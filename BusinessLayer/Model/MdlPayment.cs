﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class MdlPayment
    {
        public int manageId { get; set; }
        public string managerEmail { get; set; }
        public int invoiceId { get; set; }
        public string paymentMode { get; set; }
        public decimal amount { get; set; }
        public bool writeOff { get; set; }
        public string description { get; set; }
        public string invoiceNumber { get; set; }
        public string merchantProfileId { get; set; }
        public string payerId { get; set; }
        public string name { get; set; }
        public int propertyId { get; set; }
        public int clientId { get; set; }
        public string clientEmail { get; set; }
        public int PersonId { get; set; }

        //public int moneySenderId { get; set; }
        //public int moneyReceiverId { get; set; }

        public int statusId { get; set; }
        public int selectPaymentType { get; set; }
        public string transactionIdentifier { get; set; }
        public int RecipientType { get; set; }
        public int PayerType { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public int transactionHistoryId { get; set; }
        public string currentUrl { get; set; }

        public bool isRecurringPayment { get; set; }

        public DateTime startOnRecurring { get; set; }
        public string paymentSchedule { get; set; }
        public int paymentCategoryId { get; set; }


        //  paymentCategoryId
    }
}
