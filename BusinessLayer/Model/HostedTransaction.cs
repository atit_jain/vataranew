﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class HostedTransactionModel
    {
        public HostedTransaction HostedTransaction { get; set; }
        public Result Result { get; set; }
    }

    public class HostedTransaction
    {
        public DateTime CreationDate { get; set; }
        public string HostedTransactionIdentifier { get; set; }
        public  long PayerId { get; set; }
        public string TransactionResultMessage { get; set; }
        public string AuthCode { get; set; }
        public string TransactionHistoryId { get; set; }
        public string TransactionResult { get; set; }
        public string AvsResponse { get; set; }
        public decimal GrossAmt { get; set; }
        public  decimal NetAmt { get; set; }
        public  decimal PerTransFee { get; set; }
        public  decimal Rate { get; set; }
        public decimal GrossAmtLessNetAmt { get; set; }
        public string CVVResponseCode { get; set; }
        public decimal CurrencyConversionRate { get; set; }
        public  decimal CurrencyConvertedAmount { get; set; }
        public decimal CurrencyConvertedCurrencyCode { get; set; }
        public PaymentMethodInfo PaymentMethodInfo { get; set; }       
    }

    public class PaymentMethodInfo
    {
        public string PaymentMethodID { get; set; }
        public string PaymentMethodType { get; set; }
        public string ObfuscatedAccountNumber { get; set; }
        public  string ExpirationDate { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Protected { get; set; }

        public BillingInformation BillingInformation { get; set; }
    }

    public class BillingInformation
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }    
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
    }

    public class Result
    {
        public string ResultValue { get; set; }
        public string ResultCode { get; set; }
        public string ResultMessage { get; set; }
    }
}
