//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwPersonWithType
    {
        public int Id { get; set; }
        public string Person_Name { get; set; }
        public int PersonTypeId { get; set; }
        public string Type { get; set; }
    }
}
