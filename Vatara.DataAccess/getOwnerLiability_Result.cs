//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class getOwnerLiability_Result
    {
        public Nullable<long> PropertyID { get; set; }
        public Nullable<long> Owner { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string AmountType { get; set; }
    }
}
