//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class getPropertyTenantOwnerPersonByManagerPersonIdAndPropId_Result
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public string Email { get; set; }
        public string PersonType { get; set; }
        public Nullable<int> PersonTypeId { get; set; }
        public int StatusId { get; set; }
        public string STATUS { get; set; }
    }
}
