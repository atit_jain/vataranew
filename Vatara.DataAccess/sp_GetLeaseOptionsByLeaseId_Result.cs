//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class sp_GetLeaseOptionsByLeaseId_Result
    {
        public int LeaseId { get; set; }
        public string op_OptionType { get; set; }
        public string op_OptionDescription { get; set; }
        public string op_RequiredBy { get; set; }
        public Nullable<System.DateTime> op_EffectiveOn { get; set; }
        public Nullable<System.DateTime> op_LastExecution { get; set; }
    }
}
