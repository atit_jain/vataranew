//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class NotificationConfiguration
    {
        public int Id { get; set; }
        public string Event { get; set; }
        public string Schedule { get; set; }
        public string Message { get; set; }
    }
}
