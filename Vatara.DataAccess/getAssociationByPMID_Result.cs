//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class getAssociationByPMID_Result
    {
        public int Id { get; set; }
        public int PMID { get; set; }
        public string HOAName { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonLastName { get; set; }
        public Nullable<int> AddressId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string FbUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string Address { get; set; }
        public string Propertie { get; set; }
    }
}
