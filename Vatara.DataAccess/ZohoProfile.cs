//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZohoProfile
    {
        public int Id { get; set; }
        public int PMID { get; set; }
        public string CustomerId { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CurrencyCode { get; set; }
        public string CompanyName { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> CreationUserId { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<long> ModifierUserId { get; set; }
        public Nullable<System.DateTime> ModificationTime { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
    }
}
