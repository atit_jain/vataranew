//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class Sp_GetChatInfo_Result
    {
        public long Id { get; set; }
        public string FromMsg { get; set; }
        public string ToMsg { get; set; }
        public string Message { get; set; }
        public bool IsFile { get; set; }
        public string FileExtension { get; set; }
        public Nullable<bool> IsDelivered { get; set; }
        public System.DateTime CreationTime { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<int> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public string SenderFN { get; set; }
        public string SenderLN { get; set; }
    }
}
