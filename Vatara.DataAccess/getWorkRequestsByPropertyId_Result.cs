//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class getWorkRequestsByPropertyId_Result
    {
        public int Id { get; set; }
        public int WorkorderId { get; set; }
        public string Description { get; set; }
        public string PONumber { get; set; }
        public string InvoiceNumber { get; set; }
        public int ServiceCategoryId { get; set; }
        public string ServiceCategoryName { get; set; }
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> Status { get; set; }
    }
}
