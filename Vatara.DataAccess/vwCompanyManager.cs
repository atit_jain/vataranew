//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwCompanyManager
    {
        public int PersonId { get; set; }
        public int UserId { get; set; }
        public int PersonTypeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> AddressId { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Image { get; set; }
        public Nullable<int> PMID { get; set; }
        public string UserRole { get; set; }
        public string MailBoxNo { get; set; }
        public int AddressTypeId { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CountyId { get; set; }
        public int CityId { get; set; }
        public string Zipcode { get; set; }
        public string FullAdrs { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<bool> ManagerStatus { get; set; }
    }
}
