//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vatara.DataAccess
{
    using System;
    
    public partial class getPropertyStatusChartDataForDashboard_Result
    {
        public string MonthName { get; set; }
        public Nullable<int> MonthNumber { get; set; }
        public Nullable<int> MonthYear { get; set; }
        public Nullable<int> TotalPropertyCreated { get; set; }
        public Nullable<int> TotalLeasedProperty { get; set; }
        public Nullable<int> TotalVacantProperty { get; set; }
    }
}
