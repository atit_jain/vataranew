﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlTenantInfo
    {
        public MdlTenantInfo()
        {
            TenantLeaseData = new List<MdlTenantLeaseData>();
        }
           public List<MdlTenantLeaseData> TenantLeaseData { get; set;}
           public MdlPerson PersonInfo { get; set; }
        
    }   
}