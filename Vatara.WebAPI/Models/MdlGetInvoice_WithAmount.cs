﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlGetInvoice_WithAmount
    {
        public int id { get; set; }
        public string number { get; set; }
        public Nullable<long> propertyID { get; set; }
        public Nullable<long> from { get; set; }
        public Nullable<long> to { get; set; }
        public string wrtOff { get; set; }
        public Nullable<decimal> totalAmount { get; set; }
        public Nullable<decimal> paidAmount { get; set; }
        public Nullable<decimal> remainAmount { get; set; }

        public MdlGetInvoice_WithAmount GetMdl_GetAllInvoice_WithAmount(vmGetInvoice_WithAmount vm)
        {
            return new MdlGetInvoice_WithAmount()
            {
                id =vm.Id,
                number =vm.Number,
                propertyID = vm.PropertyID,
                from=vm.From,
                to=vm.To,
                wrtOff=vm.WrtOff,
                totalAmount = vm.TotalAmount,
                paidAmount =vm.PaidAmount,
                remainAmount = vm.TotalAmount - vm.PaidAmount
            };
        }
    }
}