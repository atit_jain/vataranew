﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlProperty
    {
        public MdlProperty()
        {
            lstMdlLeaseTenant = new List<MdlLeaseTenant>();
        }
        public int id { get; set; }
        public string name { get; set; }
        public string pAddress { get; set; }
        public string pCountry { get; set; }
        public string pCity { get; set; }
        public string pState { get; set; }
        public string pCounty { get; set; }
        public string pZipCode { get; set; }

        public string pTypeName { get; set; }
        public int pTypeId { get; set; }
        public int pCategoryId { get; set; }
        public string pCategoryName { get; set; }
        public Nullable<int> pStatusId { get; set; }
        public string pStatusName { get; set; }
        public string pNum { get; set; }
        public string pMailBoxNo { get; set; }
        public int pAddressId { get; set; }
        public Nullable<decimal> pGrossArea { get; set; }
        public Nullable<decimal> pRentableArea { get; set; }
        public string pParkingSpaces { get; set; }
        public string pUnitNo { get; set; }
        public Nullable<int> pBaseRent { get; set; }

        public List<MdlLeaseTenant> lstMdlLeaseTenant { get; set; }



        public MdlProperty GetMdl_Property(vwProperty mdl)
        {
            return new MdlProperty()
            {
                id = mdl.pId,
                name = mdl.pName,
                pNum = mdl.pNum,
                pAddress = mdl.pAddress,
                pCountry = mdl.pCountry,
                pCity = mdl.pCity,
                pState = mdl.pState,
                pCounty = mdl.pCounty,
                pZipCode = mdl.pZipCode,
                pTypeName = mdl.pTypeName,
                pTypeId = mdl.pTypeId,
                pMailBoxNo = mdl.pMailBoxNo,
                pGrossArea = mdl.pGrossArea,
                pRentableArea = mdl.pRentableArea,
                pParkingSpaces = mdl.pParkingSpaces,
                pUnitNo = mdl.pUnitNo,
                pBaseRent = mdl.pBaseRent,
               lstMdlLeaseTenant = GetLeaseTenan(mdl.pId)
            };
        }

        public MdlProperty GetMdl_PropertyUsingSP(getPropertyByOwnerPersonId_Result mdl)
        {
            return new MdlProperty()
            {
                id = mdl.pId,
                name = mdl.pName,
                pNum = mdl.pNum,
                pAddress = mdl.pAddress,
                pCountry = mdl.pCountry,
                pCity = mdl.pCity,
                pState = mdl.pState,
                pCounty = mdl.pCounty,
                pZipCode = mdl.pZipCode,
                pTypeName = mdl.pTypeName,
                pTypeId = mdl.pTypeId,
                pMailBoxNo = mdl.pMailBoxNo,
                pGrossArea = mdl.pGrossArea,
                pRentableArea = mdl.pRentableArea,
                pParkingSpaces = mdl.pParkingSpaces,
                pUnitNo = mdl.pUnitNo,
                pBaseRent = mdl.pBaseRent,
                lstMdlLeaseTenant = GetLeaseTenan(mdl.pId)
            };
        }

        public MdlProperty GetMdl_PropertyByOwnerPOID(getPropertyByOwnerPOID_Result mdl)
        {
            return new MdlProperty()
            {
                id = mdl.pId,
                name = mdl.pName,
                pNum = mdl.pNum,
                pAddress = mdl.pAddress,
                pCountry = mdl.pCountry,
                pCity = mdl.pCity,
                pState = mdl.pState,
                pCounty = mdl.pCounty,
                pZipCode = mdl.pZipCode,
                pTypeName = mdl.pTypeName,
                pTypeId = mdl.pTypeId,
                pMailBoxNo = mdl.pMailBoxNo,
                pGrossArea = mdl.pGrossArea,
                pRentableArea = mdl.pRentableArea,
                pParkingSpaces = mdl.pParkingSpaces,
                pUnitNo = mdl.pUnitNo,
                pBaseRent = mdl.pBaseRent,
                lstMdlLeaseTenant = GetLeaseTenan(mdl.pId)
            };
        }

        public List<MdlLeaseTenant> GetLeaseTenan(int PropertyId)
        {
            List<MdlLeaseTenant> lstMdlLeaseTenant = new List<MdlLeaseTenant>();
            MdlLeaseTenant MdlLeaseTenant = new MdlLeaseTenant();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.vwLeaseTenants.Where(p => p.PropertyId == PropertyId).ToList().ForEach(u =>
                    {
                    lstMdlLeaseTenant.Add(new MdlLeaseTenant().GetMdl_LeaseTenant(u));
                    });
                }
                return lstMdlLeaseTenant;
            }

            finally
            {
                lstMdlLeaseTenant = null;
            }
        }
    }
}