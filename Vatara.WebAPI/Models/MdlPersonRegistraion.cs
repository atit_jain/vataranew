﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPersonRegistraion
    {
        public int id { get; set; }
        public int personTypeId { get; set; }      

        public string personType { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<int> addressId { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phoneNo { get; set; }
        public string fax { get; set; }
        public string twitterUrl { get; set; }
        public string facebookUrl { get; set; }
        public string blogUrl { get; set; }
        public bool IsDeleted { get; set; }

        public Nullable<int> serviceCategoryId { get; set; }
        public string companyName { get; set; }

        public string mailBoxNo { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public int countryId { get; set; }
        public int stateId { get; set; }
        public int cityId { get; set; }
        public Nullable<int> countyId { get; set; }
        public string zipCode { get; set; }
        public string profileImage { get; set; }

        public Nullable<Int64> deleterUserId { get; set; }
        public DateTime creationTime { get; set; }
        public Nullable<Int64> creatorUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<Int64> lastModifierUserId { get; set; }

        // Company Info 
        public string company_Name { get; set; }
        public string company_logo { get; set; }
        public Nullable<int> company_addressId { get; set; }
        public string company_email { get; set; }
        public string company_phone { get; set; }
        public string company_fax { get; set; }
        public string company_url { get; set; }

        public int company_countryId { get; set; }
        public int company_stateId { get; set; }
        public int company_countyId { get; set; }
        public int company_cityId { get; set; }
        public string company_zipCode { get; set; }
        public string company_line2 { get; set; }
        public string company_line1 { get; set; }

        public string role { get; set; }
        public string access { get; set; }
        public int poid { get; set; }
        public bool haveRegisteredCompany { get; set; }
        public int pmid { get; set; }


        public Address GetCompany_AddressModel(MdlPersonRegistraion mdl)
        {
            return new Address()
            {
                Line1 = mdl.company_line1,
                Line2 = mdl.company_line2,
                CountryId = mdl.company_countryId,
                StateId = mdl.company_stateId,
                CityId = mdl.company_cityId,
                CountyId = mdl.company_countyId,
                Zipcode = mdl.company_zipCode,
            };
        }


        public ManagerMaster Get_Entity_ManagerMaster(MdlPersonRegistraion mdl)
        {
            ManagerMaster mgrMaster = new ManagerMaster();
           
            mgrMaster.Company_Name = mdl.company_Name;
            mgrMaster.Company_AddressId = mdl.company_addressId;
            mgrMaster.Company_Phone = mdl.company_phone;
            mgrMaster.Company_Fax = mdl.company_fax;
            mgrMaster.Company_Email = mdl.company_email;           
            mgrMaster.Logo = mdl.company_logo;
            mgrMaster.URL = mdl.company_url;

            if (mdl.haveRegisteredCompany)
            {
                mgrMaster.DisplayName = mdl.company_Name;
                mgrMaster.DisplayPhone = mdl.company_phone;
                mgrMaster.DisplayEmail = mdl.company_email;
                mgrMaster.DisplayFax = mdl.company_fax;
                mgrMaster.DisplayAddressId = mdl.company_addressId;
                mgrMaster.useCompanyInfoForDisplay = true;
            }
            else {
                mgrMaster.DisplayName = mdl.firstName +" "+ mdl.lastName;
                mgrMaster.DisplayPhone = mdl.phoneNo;
                mgrMaster.DisplayEmail = mdl.email;
                mgrMaster.DisplayFax = mdl.fax;
                mgrMaster.DisplayAddressId = mdl.addressId;
                mgrMaster.useCompanyInfoForDisplay = false;
            }
            return mgrMaster;
                             
            //IsActive = true

        }



        public Address GetAddressModel(MdlPersonRegistraion mdl)
        {
            return new Address()
            {               
                Line1 = mdl.line1,
                Line2 = mdl.line2,
                CountryId = mdl.countryId,
                StateId = mdl.stateId,
                CityId = mdl.cityId,
                CountyId = mdl.countyId,
                Zipcode = mdl.zipCode,
            };
        }

        public Address GetCompanyAddressModel(MdlPersonRegistraion mdl)
        {
            return new Address()
            {
                Line1 = mdl.company_line1,
                Line2 = mdl.company_line2,
                CountryId = mdl.company_countryId,
                StateId = mdl.company_stateId,
                CityId = mdl.company_cityId,
                CountyId = mdl.company_countyId,
                Zipcode = mdl.company_zipCode,
            };
        }


        public Person GetPersonModel(MdlPersonRegistraion mdl)
        {
            return new Person()
            {
                Id = mdl.id,
                PersonTypeId = mdl.personTypeId,
                FirstName = mdl.firstName,
                MiddleName = mdl.middleName,
                LastName = mdl.lastName,
                AddressId = mdl.addressId,
                Email = mdl.email,
                PhoneNo = mdl.phoneNo,
                Fax = mdl.fax,
              //  Image = mdl.profileImage,
            };
        }        

        public Person GetPersonModel(Person person, MdlPersonRegistraion mdl)
        {
            person.FirstName = mdl.firstName;
            person.MiddleName = mdl.middleName;
            person.LastName = mdl.lastName;
            person.Email = mdl.email;
            person.PhoneNo = mdl.phoneNo;
            person.Fax = mdl.fax;
            person.Image = mdl.profileImage;
            return person;
        }

        public Address GetAddressModel(Address address,MdlPersonRegistraion mdl)
        {
            address.Line1 = mdl.line1;
            address.Line2 = mdl.line2;
            address.CountryId = mdl.countryId;
            address.StateId = mdl.stateId;
            address.CityId = mdl.cityId;
            address.CountyId = mdl.countyId;
            address.Zipcode = mdl.zipCode;
            return address;
        }

        public MdlPersonRegistraion GetMdl_PersonRegistraion(Person person, Address address)
        {
            return new MdlPersonRegistraion()
            {
                id = person.Id,
                personTypeId = person.PersonTypeId,
                firstName = person.FirstName,
                middleName = person.MiddleName,
                lastName = person.LastName,
                phoneNo = person.PhoneNo,
                email = person.Email,
                fax = person.Fax,
                addressId = address.Id,
                line1 = address.Line1,
                line2 = address.Line2,
                countryId = address.CountryId,
                stateId = address.StateId,
                cityId = address.CityId,
                countyId = address.CountyId,
                zipCode = address.Zipcode,
                profileImage = person.Image,
            };
        }

        public MdlPersonRegistraion GetPersonModel(Person mdl)
        {
            return new MdlPersonRegistraion()
            {
                id = mdl.Id,
                personTypeId = mdl.PersonTypeId,
                personType = new clsCommon().ReturnPersonType(mdl.PersonTypeId),
                firstName = mdl.FirstName,
                middleName = mdl.MiddleName,
                lastName = mdl.LastName,
                addressId = mdl.AddressId,
                email = mdl.Email,
                phoneNo = mdl.PhoneNo,
                fax = mdl.Fax,
                profileImage = mdl.Image
            };
        }

        public MdlPersonRegistraion GetPersonModelUsingSP(getPersonByEmail_Result mdl,int POID)
        {
            if (mdl.PersonTypeId == (int)clsCommon.PersonType.Tenant)
            {
                POID = 0;
            }
            return new MdlPersonRegistraion()
            {
                id = mdl.Id,
                personTypeId = mdl.PersonTypeId,
                personType = new clsCommon().ReturnPersonType(mdl.PersonTypeId),
                firstName = mdl.FirstName,
                middleName = mdl.MiddleName,
                lastName = mdl.LastName,
                addressId = mdl.AddressId,
                email = mdl.Email,
                phoneNo = mdl.PhoneNo,
                fax = mdl.Fax,
                profileImage = mdl.Image,
                poid = POID,
                role = mdl.Role
            };
        }


    }
}