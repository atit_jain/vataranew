﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlHOA
    {
        public MdlHOA()
        {
            lstProperties = new List<string>();
        }
        public int id { get; set; }
        public int pmid { get; set; }
        public string hoaName { get; set; }
        public string personFirstName { get; set; }
        public string personLastName { get; set; }
        public Nullable<int> addressId { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string fbUrl { get; set; }
        public string twitterUrl { get; set; }
        public int creatorId { get; set; }

        public int cityId { get; set; }
        public int countryId { get; set; }
        public int stateId { get; set; }
        public int countyId { get; set; }
        public string zipcode { get; set; }
        public string address { get; set; }
        public string properties { get; set; }
        public List<string> lstProperties { get; set; }

        public HOA GetEntity_HOA(MdlHOA mdl)
        {
            return new HOA()
            {
                Id = mdl.id,
                PMID = mdl.pmid,
                HOAName = mdl.hoaName,
                PersonFirstName = mdl.personFirstName,
                PersonLastName = mdl.personLastName,
                AddressId = mdl.addressId,
                Email = mdl.email,
                Phone = mdl.phone,
                Fax = mdl.fax,
                FbUrl = mdl.fbUrl,
                TwitterUrl = mdl.twitterUrl

            };
        }

        public MdlHOA GetMdl_HOA(HOA entity)
        {
            return new MdlHOA()
            {
                id = entity.Id,
                pmid = entity.PMID,
                hoaName = entity.HOAName,
                personFirstName = entity.PersonFirstName,
                personLastName = entity.PersonLastName,
                addressId = entity.AddressId,
                email = entity.Email,
                phone = entity.Phone,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl
            };
        }


        public MdlHOA GetMdl_HOA(getAssociationByPMID_Result entity)
        {
            return new MdlHOA()
            {
                id = entity.Id,
                pmid = entity.PMID,
                hoaName = entity.HOAName,
                personFirstName = entity.PersonFirstName,
                personLastName = entity.PersonLastName,
                addressId = entity.AddressId,
                email = entity.Email,
                phone = entity.Phone,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl,
                address = entity.Address,
                lstProperties = entity.Propertie.Split(',').ToList()
            };
        }

        public MdlHOA GetMdl_HOA(getAssociationById_Result entity)
        {
            return new MdlHOA()
            {
                id = entity.Id,
                pmid = entity.PMID,
                hoaName = entity.HOAName,
                personFirstName = entity.PersonFirstName,
                personLastName = entity.PersonLastName,
                addressId = entity.AddressId,
                email = entity.Email,
                phone = entity.Phone,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl,

                cityId = entity.CityId,
                countryId = entity.CountryId,
                stateId = entity.StateId,
                countyId = entity.CountyId,
                zipcode = entity.Zipcode,
                address = entity.Address,

            };
        }

    }


    public class MdlAssociationRegistration
    {        
        public MdlHOA mdlAssociation { get; set; }
        public MdlAddress mdlAddress { get; set; }       

    }

}