﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlPropertOwnersWithManager
    {
        public string propertyName { get; set; }
        public int propertyId { get; set; }
        public int personTypeId { get; set; }
        public string ownerFullName { get; set; }
        public int ownerId { get; set; }
        public int ownerIsCompany { get; set; }
        public Nullable<int> personId { get; set; }
        public string ownerFullNameForFilter { get; set; }

        public MdlPropertOwnersWithManager GetMdl_PropertOwnersWithManager(getPropertiesByPerson_Result entity)
        {
            return new MdlPropertOwnersWithManager()
            {
                propertyName = entity.PropName + " | " + entity.PropAddress,
                propertyId = entity.PropId
            };
        }
    }
}