﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlGeneratePDF
    {
        public string[] gridHtml { get; set; }
        public string pagesize { get; set; }
        public string logo { get; set; }
        public string headerText { get; set; }
    }
}