﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPropTenantOwnerDetail
    {

        public MdlPropTenantOwnerDetail()
        {
            lstMdlTransectionInfo = new List<MdlTransectionInfo>();
            lstOwnerLiability = new List<MdlManagerLiabilityToOwner>();
        }
        public int propId { get; set; }
        public string propName { get; set; }
        public Nullable<int> ownerId { get; set; }
        public string propAddress { get; set; }
        public string ownerName { get; set; }
        public string ownerAddress { get; set; }
        public decimal rentAmt { get; set; }
        public Nullable<System.DateTime> moveInDate { get; set; }
        public Nullable<System.DateTime> renewalDate { get; set; }
        public int tenantId { get; set; }
        public string tenantName { get; set; }
        public string tenantPhone { get; set; }
        public int propManagr { get; set; }
        public List<MdlManagerLiabilityToOwner> lstOwnerLiability { get; set; }  // *Reuse of MdlManagerLiabilityToOwner for owner liability
        public List<MdlTransectionInfo> lstMdlTransectionInfo { get; set; }


        public MdlPropTenantOwnerDetail GetMdl_PropTenantOwnerDetail(getPropertyDetailByManagerOrOwner_Result mdl, int year, bool isPropIdGiven, int managerId,int pmid)
        {
            return new MdlPropTenantOwnerDetail
            {
                propId = mdl.PropId,
                propName = mdl.PropName,
                ownerId = mdl.OwnerId,
                propAddress = mdl.PropAddress,
                ownerName = mdl.OwnerName,
                ownerAddress = mdl.OwnerAddress,
                rentAmt = mdl.RentAmt,
                moveInDate = mdl.MoveInDate,
                renewalDate = mdl.RenewalDate,
                tenantId = Convert.ToInt32(mdl.TenantId),
                tenantName = mdl.TenantName == null ? "" : mdl.TenantName,
                tenantPhone = mdl.TenantPhone == null ? "" : mdl.TenantPhone,
                propManagr = Convert.ToInt32(mdl.PropManagr),
                lstMdlTransectionInfo = GetTransectionInfo(mdl, year, isPropIdGiven, managerId),
                lstOwnerLiability = GetOwnerLiability(mdl, year, isPropIdGiven, pmid)
            };
        }


        //public MdlPropTenantOwnerDetail GetMdl_PropTenantOwnerDetail1(vwPropTenantOwnerDetail mdl)
        //{
        //    return new MdlPropTenantOwnerDetail
        //    {
        //        propId = mdl.PropId,
        //        propName = mdl.PropName,
        //        ownerId = mdl.OwnerId,
        //        propAddress = mdl.PropAddress,
        //        ownerName = mdl.OwnerName,
        //        ownerAddress = mdl.OwnerAddress,
        //        rentAmt = mdl.RentAmt,
        //        moveInDate = mdl.MoveInDate,
        //        renewalDate = mdl.RenewalDate,
        //        tenantId = Convert.ToInt32(mdl.TenantId),
        //        tenantName = mdl.TenantName == null ? "" : mdl.TenantName,
        //        tenantPhone = mdl.TenantPhone == null ? "" : mdl.TenantPhone,
        //       // propManagr = Convert.ToInt32(mdl.PropManagr)      COMMENTED by ASR    2018-09-20        
        //    };
        //}       

        public List<MdlTransectionInfo> SetMonth(List<MdlTransectionInfo> lstmdl)
        {
            List<MdlTransectionInfo> lstTransectionInfo = new List<MdlTransectionInfo>();
            DataTable dtNew = new DataTable();
            DataTable dt = new DataTable();
            int lastLine = 0;
            int categoty = 0;
            int monthNum = 0;
            string incm = string.Empty;          
            
            string monthName = string.Empty;
            try
            {
                if (lstmdl.Count == 0)
                {
                    return lstTransectionInfo;
                }
                lstTransectionInfo.Add(lstmdl[0]);
                categoty = lstTransectionInfo[0].categoryId;
                monthNum = Convert.ToInt32(lstTransectionInfo[0].monthNum);
                incm = lstTransectionInfo[0].incmExpence;
                dt = ConvertListToDataTable(lstmdl);
                dtNew = dt.Clone();
                monthName = dt.Rows[0]["monthName"].ToString();
                dt.Rows[0][monthName] = lstmdl[0].amount;

                dtNew.ImportRow(dt.Rows[0]);
                lastLine = 0;
                for (int j = 1; j <= dt.Rows.Count - 1; j++)
                {
                    if (categoty == Convert.ToInt32(lstmdl[j].categoryId) && incm == lstmdl[j].incmExpence)
                    {
                        if (monthNum == Convert.ToInt32(lstmdl[j].monthNum))
                        {
                            dtNew.Rows[lastLine][monthName] = Convert.ToDecimal(dtNew.Rows[lastLine][monthName]) + Convert.ToDecimal(lstmdl[j].amount);
                        }
                        else
                        {
                            monthNum = Convert.ToInt32(lstmdl[j].monthNum);
                            monthName = lstmdl[j].monthName;
                            dtNew.Rows[lastLine][monthName] = Convert.ToDecimal(lstmdl[j].amount);
                        }
                    }
                    else
                    {
                        categoty = lstmdl[j].categoryId;
                        monthNum = Convert.ToInt32(lstmdl[j].monthNum);
                        monthName = lstmdl[j].monthName;
                        incm = lstmdl[j].incmExpence;
                        dtNew.ImportRow(dt.Rows[j]);
                        lastLine = dtNew.Rows.Count - 1;
                        dtNew.Rows[lastLine][monthName] = Convert.ToDecimal(lstmdl[j].amount);
                    }
                }

                lstTransectionInfo = (from DataRow dr in dtNew.Rows
                                      select new MdlTransectionInfo()
                                      {
                                          billId = Convert.ToInt32(dr["billId"]),
                                          invoiceNumber = Convert.ToInt32(dr["invoiceNumber"]),
                                          date = Convert.ToDateTime(Convert.ToDateTime(dr["date"]).ToString("MM/dd/yyyy")),
                                          propertyID = Convert.ToInt64(dr["propertyID"]),
                                          @from = Convert.ToInt64(dr["from"]),
                                          to = Convert.ToInt64(dr["to"]),
                                          amount = Convert.ToDecimal(dr["amount"]),
                                          paymentType = (dr["paymentType"]).ToString(),
                                          comment = (dr["comment"]).ToString(),
                                          categoryId = Convert.ToInt32(dr["categoryId"]),
                                          serviceCategory = (dr["serviceCategory"]).ToString(),
                                          monthName = (dr["monthName"]).ToString(),
                                          getYear = Convert.ToInt32(dr["getYear"]),
                                          incmExpence = (dr["incmExpence"]).ToString(),
                                          monthNum = Convert.ToInt32(dr["monthNum"]),
                                          January = Convert.ToDecimal(dr["January"]),
                                          February = Convert.ToDecimal(dr["February"]),
                                          March = Convert.ToDecimal(dr["March"]),
                                          April = Convert.ToDecimal(dr["April"]),
                                          May = Convert.ToDecimal(dr["May"]),
                                          June = Convert.ToDecimal(dr["June"]),
                                          July = Convert.ToDecimal(dr["July"]),
                                          August = Convert.ToDecimal(dr["August"]),
                                          September = Convert.ToDecimal(dr["September"]),
                                          October = Convert.ToDecimal(dr["October"]),
                                          November = Convert.ToDecimal(dr["November"]),
                                          December = Convert.ToDecimal(dr["December"])
                                      }).ToList();

                return lstTransectionInfo.OrderBy(e => e.serviceCategory).ToList();
            }          
            finally
            {
                dtNew = null;
                dt = null;
            }
        }

        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        public DataTable ConvertListToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public List<MdlTransectionInfo> GetTransectionInfo(getPropertyDetailByManagerOrOwner_Result mdl, int year, bool isPropIdGiven, int managerId)
        {
            List<MdlTransectionInfo> lstTransectionInfo = new List<MdlTransectionInfo>();
            List<MdlTransectionInfo> data = new List<MdlTransectionInfo>();
            string incmExpence = string.Empty;
            int propId = 0;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (isPropIdGiven)
                    {
                        propId = mdl.PropId;
                    }
                    entities.getTransectionInfo(managerId, mdl.OwnerId, propId, year, DateTime.Now, DateTime.Now).ToList().ForEach(u =>
                    {
                        lstTransectionInfo.Add(new MdlTransectionInfo().GetMdl_TransectionInfo(u, u.incmExpence));
                    });  
                    data = lstTransectionInfo.OrderBy(e => e.propertyID)
                       .ThenBy(e => e.categoryId).ThenBy(e => e.monthNum).ThenBy(e => e.incmExpence).ToList();
                    lstTransectionInfo = SetMonth(data);
                }
                return lstTransectionInfo;
            }
            finally
            {
                data = null;
                lstTransectionInfo = null;
                incmExpence = string.Empty;
            }
        }

        public List<MdlManagerLiabilityToOwner> GetOwnerLiability(getPropertyDetailByManagerOrOwner_Result mdl, int year, bool isPropIdGiven, int pmid)
        {
            List<MdlManagerLiabilityToOwner> lstOwnerLiability = new List<MdlManagerLiabilityToOwner>();
            int propId = 0;           
            DateTime ToDate = new DateTime(year, 12, 31);
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (isPropIdGiven)
                    {
                        propId = mdl.PropId;
                    }
                    entities.getOwnerLiability(ToDate, mdl.OwnerId, pmid, propId).ToList().ForEach(u =>
                    {
                        lstOwnerLiability.Add(new MdlManagerLiabilityToOwner().GetMdl_ManagerLiabilityToOwner(u));
                    });

                }
                return lstOwnerLiability;
            }
            finally
            {
                lstOwnerLiability = null;              
            }
        }


        public List<MdlTransectionInfo> GetTransections(int ManagerId, int OwnerId,int propertyId, DateTime fromDate, DateTime toDate)
        {
            List<MdlTransectionInfo> lstTransectionInfo = new List<MdlTransectionInfo>();
            List<MdlTransectionInfo> data = new List<MdlTransectionInfo>();
            DateTime newTodate = toDate.Date.AddDays(1);
            string incmExpence = string.Empty;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getTransectionInfo(ManagerId, OwnerId, propertyId, 0, fromDate, toDate).ToList().ForEach(u =>
                    {                       
                        lstTransectionInfo.Add(new MdlTransectionInfo().GetMdl_TransectionInfo(u, u.incmExpence));
                    });
                    #region
                    //entities.vwTransectionInfoes.Where(e => (e.From == OwnerId || e.To == OwnerId) && e.PropertyID == propertyId && e.Date >= fromDate.Date && e.Date <= newTodate.Date).ToList().ForEach(u =>
                    //{
                    //    incmExpence = string.Empty;
                    //    if (u.From == OwnerId)
                    //    {
                    //        incmExpence = "income";
                    //    }
                    //    else
                    //    {
                    //        incmExpence = "expense";
                    //    }
                    //    lstTransectionInfo.Add(new MdlTransectionInfo().GetMdl_TransectionInfo(u, incmExpence));
                    //});
                    #endregion
                }
                return lstTransectionInfo;
            }
            catch
            {
                return lstTransectionInfo;
            }
            finally
            {
                data = null;
                lstTransectionInfo = null;
                incmExpence = string.Empty;
            }
        }

    }
}