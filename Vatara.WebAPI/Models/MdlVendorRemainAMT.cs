﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlVendorRemainAMT
    {
        public int invoiceId { get; set; }
        public int invoiceDetailId { get; set; }
        public string number { get; set; }
        public Nullable<long> propertyID { get; set; }
        public string propertyName { get; set; }
        public int categoryID { get; set; }
        public string categoryName { get; set; }
        public decimal totalAmount { get; set; }
        public decimal markup { get; set; }
        public string markup_type { get; set; }
        public decimal tax { get; set; }
        public decimal paidAmount { get; set; }
        public string description { get; set; }
        public int managerId { get; set; }
        public decimal markupAmount { get; set; }
        public decimal totalAmount_Vendor { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public Nullable<long> from { get; set; }
        public Nullable<long> to { get; set; }
        public string toName { get; set; }



        public string Vendor_Notes { get; set; }



        public MdlVendorRemainAMT GetMdl_VendorRemainAMT(getVendorPaidAMTByManager_Result entity)
        {
            return new MdlVendorRemainAMT()
            {
                invoiceId = entity.InvoiceId,
                invoiceDetailId = entity.InvoiceDetailId,
                number = entity.Number,
                propertyID = entity.PropertyID,
                propertyName = entity.PropertyName,
                categoryID = entity.CategoryID,
                categoryName = entity.CategoryName,
                totalAmount = entity.TotalAmount,
                markup = entity.Markup,
                markup_type = entity.Markup_type,
                tax = entity.Tax,
                paidAmount = entity.PaidAmount,
                description = entity.Description,
                managerId = (int)entity.ManagerId,
                totalAmount_Vendor = (decimal)entity.Liabitity,
                // totalAmount_Vendor = TotalAmount_ForVendor(entity),
                date = entity.Date,
                to = entity.To,
                from = entity.From,
                toName = entity.ToName
            };
        }

        public MdlVendorRemainAMT GetMdl_VendorRemainAMT(getVendorRemainAMTByManager_Result entity)
        {
            return new MdlVendorRemainAMT()
            {
                invoiceId = entity.InvoiceId,
                invoiceDetailId = entity.InvoiceDetailId,
                number = entity.Number,
                propertyID = entity.PropertyID,
                propertyName = entity.PropertyName,
                categoryID = entity.CategoryID,
                categoryName = entity.CategoryName,
                totalAmount = entity.TotalAmount,
                markup = entity.Markup,
                markup_type = entity.Markup_type,
                tax = entity.Tax,
                paidAmount = entity.PaidAmount,
                description = entity.Description,
                managerId = (int)entity.ManagerId,
                totalAmount_Vendor = (decimal)entity.Liabitity,
                // totalAmount_Vendor = TotalAmount_ForVendor(entity),
                date = entity.Date,
                to = entity.To,
                from = entity.From,
                toName = entity.ToName
            };
        }

        //private decimal TotalAmount_ForVendor(getVendorRemainAMTByManager_Result entity)
        //{
        //    if(entity.Markup_type == "amt")
        //    {
        //        return entity.TotalAmount - entity.Markup;
        //    }
        //    else
        //    {
        //        return entity.TotalAmount - ((entity.TotalAmount * entity.Markup) / 100);
        //    }
        //}
    }
}