﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlOwnerInfo
    {
        public MdlOwnerInfo()
        {
            lstMdlProperty = new List<MdlProperty>();
            lstMdlPropertOwnersWithManager  = new List<MdlPropertOwnersWithManager>();
        }
       public MdlPerson PersonInfo { get; set; }
       public List<MdlProperty> lstMdlProperty { get; set; }
       public List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager { get; set; }
    }
}