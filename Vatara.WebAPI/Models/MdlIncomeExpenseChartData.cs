﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlIncomeExpenseChartData
    {
        public string MonthYear { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> MonthNum { get; set; }
        public decimal Liability { get; set; }
        public decimal Net_Income { get; set; }

        public MdlIncomeExpenseChartData GetMdl_IncomeExpenseChartData(GetIncomeExpenseChartData_Result entity)
        {
            return new MdlIncomeExpenseChartData()
            {
               MonthNum=entity.MonthNum,
               MonthYear=entity.MonthYear,
               Liability=entity.Liability,
               Net_Income=entity.Net_Income,
               Year=entity.Year
            };
        }

        public MdlIncomeExpenseChartData GetMdl_IncomeChartData(GetIncomeDataChart_Result entity)
        {
            return new MdlIncomeExpenseChartData()
            {
                MonthNum = entity.MonthNum,
                MonthYear = entity.MonthYear,
                Liability = entity.Liability,
                Net_Income = entity.Net_Income,
                Year = entity.Year
            };
        }
    }
}