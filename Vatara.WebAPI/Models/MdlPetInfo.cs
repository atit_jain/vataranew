﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPetInfo
    {
        public int id { get; set; }
        public string petName { get; set; }
        public int leaseId { get; set; }
        public string detail { get; set; }

        public MdlPetInfo GetMdl_PetInfo(vwPetInfo mdl)
        {
            return new MdlPetInfo()
            {
                id = mdl.id,
                petName = mdl.PetName,
                leaseId = mdl.LeaseId,
                detail = mdl.Detail
            };
        }
    }
}