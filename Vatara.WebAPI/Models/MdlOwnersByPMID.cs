﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlOwnersByPMID
    {
        public int pId { get; set; }
        public int ownerPOID { get; set; }
        public string pFullName { get; set; }
        public string pAddress { get; set; }
        public Nullable<int> pAddressId { get; set; }
        public string pPhoneNo { get; set; }
        public string pEmail { get; set; }
        public string pImage { get; set; }
        public int ownerIsCompany { get; set; }
        public string ContactNum { get; set; }

        public MdlOwnersByPMID GetMdl_OwnersByPMID(getAllOwnersByPMID_Result entity)
        {
            return new MdlOwnersByPMID()
            {
                pId = entity.pId,
                pFullName = entity.pFullName,
                pAddress = entity.pAddress,
                ContactNum = entity.pPhoneNo,
                pPhoneNo = entity.pPhoneNo,
                pEmail = entity.pEmail,
                pImage = entity.pImage,
                ownerPOID = entity.ownerPOID,
                ownerIsCompany = entity.ownerIsCompany
            };
        }
    }
}