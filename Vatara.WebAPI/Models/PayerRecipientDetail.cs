﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class PayerRecipientDetail
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string personType { get; set; }
        public string type { get; set; }


        public PayerRecipientDetail GetMdl_PayerRecipientDetail(getPayerRecipientDetail_Result mdl)
        {
            return new PayerRecipientDetail()
            {
               email = mdl.Email,
               name = mdl.Name,
               phone = mdl.Phone,
               personType = mdl.PersonType,
               type = mdl.Type
            };
        }

    }
}