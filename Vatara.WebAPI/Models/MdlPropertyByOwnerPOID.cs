﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlPropertyByOwnerPOID
    {
        public int pId { get; set; }
        public string pTypeName { get; set; }
        public string pName { get; set; }
        public int pAddressId { get; set; }
        public string pAddress { get; set; }
        public string pAddress2 { get; set; }
        public Nullable<int> pCountryId { get; set; }
        public Nullable<int> pCityId { get; set; }
        public Nullable<int> pCountyId { get; set; }
        public Nullable<int> pStateId { get; set; }
        public string pCountry { get; set; }
        public string pCity { get; set; }
        public string pState { get; set; }
        public string pCounty { get; set; }
        public string pZipCode { get; set; }
        public Nullable<int> pOwnerId { get; set; }
        public string pOwnerName { get; set; }
        public string pOwnerName1 { get; set; }
        public string pOwnerEmail { get; set; }
    }
}