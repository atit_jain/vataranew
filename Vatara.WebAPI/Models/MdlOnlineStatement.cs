﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlOnlineStatement
    {
        public MdlOnlineStatement()
        {
            lstTransectionInfo = new List<MdlOnlineStatementReport>();
        }

        public List<MdlOnlineStatementReport> lstTransectionInfo { get; set; }
        public decimal TotalInvoiceAmt { get; set; }
        public decimal PaidAmt { get; set; }
        public decimal CurrentCharges { get; set; }
        public decimal PreviousBalance { get; set; }
        public decimal BalanceDue { get; set; }
        public decimal SubTotal { get; set; }
        public Nullable<decimal> Liability { get; set; }
        public Nullable<bool> IsOwner { get; set; }
        public MdlPerson PropManager { get; set; }
        public MdlPerson PropOwner { get; set; }
        public MdlProperty Property { get; set; }
        //public Nullable<decimal> NetAmountOwnerHasToPay { get; set; }
        //public Nullable<decimal> NetAmountPmHasToPay { get; set; }
       

    }
}