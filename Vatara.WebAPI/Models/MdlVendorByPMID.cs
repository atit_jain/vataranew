﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlVendorByPMID
    {
        public int vendorId { get; set; }
        public string companyName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<int> addressId { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string fax { get; set; }
        public string fbUrl { get; set; }
        public string twitterUrl { get; set; }
        public string service { get; set; }
        public string address { get; set; }
        public int cityId { get; set; }
        public int countryId { get; set; }
        public int stateId { get; set; }
        public int countyId { get; set; }
        public string zipcode { get; set; }
        public string vendorServiceIds { get; set; }

        public MdlVendorByPMID GetMdl_VendorByPMID(getVendorByPMID_Result entity)
        {
            return new MdlVendorByPMID()
            {
                vendorId = entity.VendorId,
                companyName = entity.CompanyName,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                addressId = entity.AddressId,
                phone = entity.Phone,
                email = entity.Email,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl,
                service = entity.Service,
                address = entity.Address
            };
        }
    }
}