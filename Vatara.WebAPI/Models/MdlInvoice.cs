﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlInvoice
    {
        public int id { get; set; }
        public string number { get; set; }
        public Nullable<DateTime> date { get; set; }
        public Nullable<DateTime> dueDate { get; set; }
        public Nullable<Int64> propertyID { get; set; }
        public Nullable<Int64> from { get; set; }
        public string fromAddress { get; set; }
        public string senderName { get; set; }
        public string senderPhoneNo { get; set; }
        public Nullable<Int64> to { get; set; }
        public string toType { get; set; }
        public string receiverName { get; set; }
        public string toAddress { get; set; }
        public Nullable<int> statusId { get; set; }
        public string note { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<Int64> deleterUserId { get; set; }
        public DateTime creationTime { get; set; }
        public Nullable<Int64> creatorUserId { get; set; }

        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<Int64> lastModifierUserId { get; set; }

        public int ownerID { get; set; }
        public string ownerName { get; set; }
        public string ownerAddress { get; set; }
        public string propertyName { get; set; }
        public string propertyAddress { get; set; }
        public Nullable<int> recipientPersonType { get; set; }
        public string recipientPersonTypeName { get; set; }


        public Invoice GetEntity_Invoice(MdlInvoice mdl)
        {
            return new Invoice()
            {
                Id = mdl.id,
                Number = mdl.number,
                Date = mdl.date.Value.ToUniversalTime(),
                DueDate = mdl.dueDate.Value.ToUniversalTime(),
                PropertyID = mdl.propertyID,
                From = mdl.from,
                To = mdl.to,
                RecipientPersonType = mdl.recipientPersonType,
                StatusId = mdl.statusId,
                Note = mdl.note,
                IsDeleted = mdl.isDeleted,
                CreationTime = mdl.creationTime.ToUniversalTime(),
                CreatorUserId = mdl.creatorUserId

            };
        }


        public MdlInvoice GetMdl_Invoice(proc_GetInvoiceByID_Result mdl)
        {
            return new MdlInvoice()
            {
                id = mdl.InvoiceId,
                number = mdl.Number,
                date = mdl.Date,
                dueDate = mdl.DueDate,
                propertyID = mdl.PropertyID,
                from = mdl.From,
                fromAddress = mdl.fromAddress,
                senderName = mdl.SenderName,
                senderPhoneNo = mdl.SenderPhoneNo,
                to = mdl.To,
                receiverName = mdl.ReceiverName,
                toAddress = mdl.toAddress,
                statusId = mdl.StatusId,
                note = mdl.Note,
                isDeleted = mdl.IsDeleted,
                deleterUserId = mdl.DeleterUserId,
                creationTime = mdl.CreationTime,
                creatorUserId = mdl.CreatorUserId,
                lastModificationTime = mdl.LastModificationTime,
                lastModifierUserId = mdl.LastModifierUserId,
                ownerID = (int)mdl.OwnerID,
                ownerName = mdl.OwnerName,
                ownerAddress = mdl.OwnerAddress,
                propertyName = mdl.PropertyName,
                propertyAddress = mdl.PropertyAddress
                
            };
        }
        
    }
}