﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlCounty
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int stateId { get; set; }

        public MdlCounty GetMdl_County(vwCounty mdl)
        {
            return new MdlCounty()
            {
                id = mdl.Id,
                name = mdl.Name,
                description = mdl.Description,
                stateId = mdl.StateId
            };
        }
    }
}