﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPropertyStatusChartDataForDashboard
    {
        public string MonthName { get; set; }
        public Nullable<int> MonthNumber { get; set; }
        public string MonthYear { get; set; }
        public Nullable<int> TotalPropertyCreated { get; set; }
        public Nullable<int> TotalLeasedProperty { get; set; }
        public Nullable<int> TotalVacantProperty { get; set; }

        public MdlPropertyStatusChartDataForDashboard Get_MdlPropertyStatusChartDataForDashboard(getPropertyStatusChartDataForDashboard_Result entity)
        {
            return new MdlPropertyStatusChartDataForDashboard()
            {
                MonthName = entity.MonthName,
                MonthNumber = entity.MonthNumber,
                MonthYear = entity.MonthName.Substring(0, 3) + entity.MonthYear.ToString().Substring(2),//)entity.MonthYear,
                TotalPropertyCreated = entity.TotalPropertyCreated,
                TotalLeasedProperty = entity.TotalLeasedProperty,
                TotalVacantProperty = entity.TotalVacantProperty
            };
        }
    }   
}