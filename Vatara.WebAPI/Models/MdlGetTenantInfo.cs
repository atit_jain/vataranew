﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlGetTenantInfo
    {
        public int pId { get; set; }
        public int pTypeId { get; set; }
        public string pTypeName { get; set; }
        public string pFirstName { get; set; }
        public string pMiddleName { get; set; }
        public string pLastName { get; set; }
        public string pFullName { get; set; }
        public string pImage { get; set; }
        public Nullable<int> pServiceCategoryId { get; set; }
        public string pServiceCategoryName { get; set; }
        public string pCompanyName { get; set; }
        public Nullable<int> pStatusId { get; set; }
        public string pStatusName { get; set; }
        public string pMailBoxNo { get; set; }
        public string pAddress { get; set; }
        public Nullable<int> pCountryId { get; set; }
        public string pCountryName { get; set; }
        public Nullable<int> pStateId { get; set; }
        public string pStateName { get; set; }
        public Nullable<int> pCityId { get; set; }
        public string pCityName { get; set; }
        public Nullable<int> pCountyId { get; set; }
        public string pCountyName { get; set; }
        public string pZipcode { get; set; }
        public string pFaxNo { get; set; }
        public string pEmail { get; set; }
        public string pFBUrl { get; set; }
        public string pTwitterUrl { get; set; }
        public Nullable<int> PMID { get; set; }
        public string pPhoneNo { get; set; }

        public MdlGetTenantInfo GetTenantInfoMdl(vmGetTenantInfo mdl)
        {
            return new MdlGetTenantInfo()
            {
                pId = mdl.pId,
                pTypeId = mdl.pTypeId,
                pTypeName = mdl.pTypeName,
                pFirstName = mdl.pFirstName,
                pMiddleName = mdl.pMiddleName,
                pLastName = mdl.pLastName,
                pFullName = mdl.pFullName,
                pImage = mdl.pImage,
                pServiceCategoryId = mdl.pServiceCategoryId,
                pServiceCategoryName = mdl.pServiceCategoryName,
                pCompanyName = mdl.pCompanyName,
                pStatusId = mdl.pStatusId,
                pStatusName = mdl.pStatusName,
                pMailBoxNo = mdl.pMailBoxNo,
                pAddress = mdl.pAddress,
                pCountryId = mdl.pCountryId,
                pCountryName = mdl.pCountryName,
                pStateId = mdl.pStatusId,
                pStateName = mdl.pStatusName,
                pCityId = mdl.pCityId,
                pCityName = mdl.pCityName,
                pCountyId = mdl.pCountryId,
                pCountyName = mdl.pCountyName,
                pZipcode = mdl.pZipcode,
                pFaxNo = mdl.pFaxNo,
                pEmail = mdl.pEmail,
                pFBUrl = mdl.pFBUrl,
                pTwitterUrl = mdl.pTwitterUrl,
                PMID = mdl.PMID,
                pPhoneNo = mdl.pPhoneNo
            };
        }
    }
}