﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlGetAllInvoice
    {
        public int invoiceId { get; set; }
        public string invoiceNum { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public Nullable<System.DateTime> dueDate { get; set; }
        public Nullable<long> propertyID { get; set; }
        public string propertyName { get; set; }
        public Nullable<long> from { get; set; }
        public Nullable<long> to { get; set; }
        public string recipientName { get; set; }
        public Nullable<int> statusId { get; set; }
        public Nullable<int> deleteID { get; set; }
        public string deteteCommand { get; set; }
        public string status { get; set; }
        public string note { get; set; }
        public System.DateTime creationTime { get; set; }
        public Nullable<bool> isAutoInv { get; set; }
        public string fromEmail { get; set; }
        public string senderName { get; set; }

        public string propAdrs { get; set; }
        public string senderAdrs { get; set; }
        public string recipientAdrs { get; set; }
        public Nullable<decimal> totalInvAmount { get; set; }
        public Nullable<decimal> paidAmount { get; set; }

        public int propOwnerID { get; set; }
        public string propOwnerName { get; set; }
        public string propOwnerPhone { get; set; }
        public string propOwnerAddress { get; set; }


        //public MdlGetAllInvoice GetMdl_GetAllInvoice(vwGetAllInvoice view, bool ismanager)
        //{
        //    string status1 = "";
        //    if (!ismanager && view.Status.Trim() == clsConstants.sentToClient.Trim())
        //    {
        //        status1 = "Unpaid";
        //    }
        //    else {
        //        status1 = view.Status;
        //    }
            
        //    return new MdlGetAllInvoice()
        //    {
        //        invoiceId = view.InvoiceId,
        //        invoiceNum = view.InvoiceNum,
        //        date = view.Date,
        //        dueDate = view.DueDate,
        //        propertyID = view.PropertyID,
        //        propertyName = view.PropertyName,
        //        from = view.From,
        //        to = view.To,
        //        recipientName = view.RecipientName,             
        //        statusId = view.StatusId,
        //        status = status1,     //view.Status,
        //        deleteID = view.DeleteID,
        //        deteteCommand = view.DeteteCommand,
        //        note = view.Note,
        //        creationTime = view.CreationTime,
        //        fromEmail = view.FromEmail,
        //        senderName = view.SenderName,
        //        propAdrs = view.PropAdrs,
        //        senderAdrs = view.SenderAdrs,
        //        recipientAdrs = view.RecipientAdrs,
        //        totalInvAmount = view.TotalInvAmount,
        //        paidAmount = view.PaidAmount,
        //        propOwnerID = (int)view.PropOwnerID,
        //        propOwnerName = view.PropOwnerName,
        //        propOwnerPhone = view.PropOwnerPhone,
        //        propOwnerAddress = view.PropOwnerAddress,
        //    };
        //}

        public MdlGetAllInvoice GetMdl_GetAllInvoiceUsingproc(getInvoiceByDate_Result entity, bool ismanager)
        {
            string status1 = "";
            if (!ismanager && entity.Status.Trim() == clsConstants.sentToClient.Trim())
            {
                status1 = "Unpaid";
            }
            else
            {
                status1 = entity.Status;
            }
            return new MdlGetAllInvoice()
            {
                invoiceId = entity.InvoiceId,
                invoiceNum = entity.InvoiceNum,
                date = entity.Date,
                dueDate = entity.DueDate,
                propertyID = entity.PropertyID,
                propertyName = entity.PropertyName,
                from = entity.From,
                to = entity.To,
                recipientName = entity.RecipientName,
                statusId = entity.StatusId,
                status = status1,     //view.Status,
                deleteID = entity.DeleteID,
                deteteCommand = entity.DeteteCommand,
                note = entity.Note,
                creationTime = entity.CreationTime,
                fromEmail = entity.FromEmail,
                senderName = entity.SenderName,
                propAdrs = entity.PropAdrs,
                senderAdrs = entity.SenderAdrs,
                recipientAdrs = entity.RecipientAdrs,
                totalInvAmount = entity.TotalInvAmount,
                paidAmount = entity.PaidAmount,
                propOwnerID = entity.PropOwnerID,
                propOwnerName = entity.PropOwnerName,
                propOwnerPhone = entity.PropOwnerPhone,
                propOwnerAddress = entity.PropOwnerAddress,
                isAutoInv = entity.IsAutoInv
            };
        }
    }
}