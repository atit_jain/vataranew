﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlPropertyOwnerDetail
    {       
        public MdlPropertyOwnerDetail()
        {
            lstMdlOwner = new List<MdlOwner>();
        }
        public MdlOwnerMaster OwnerMaster { get; set; }
        public List<MdlOwner> lstMdlOwner { get; set; }
        public string Ownertype { get; set; }
       // public MdlAddress OwnerMasterAddress { get; set; }
        public int creatorId { get; set; }

    }


    public class MdlOwner
    {
        public int Id { get; set; }
        public string pFirstName { get; set; }
        public string pLastName { get; set; }
        public string pEmail { get; set; }
        public string pPhoneNo { get; set; }
        public string password { get; set; }
        public bool Isprimary { get; set; }
    }
}