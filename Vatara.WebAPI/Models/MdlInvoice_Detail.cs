﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlInvoice_Detail
    {
        public MdlInvoice_Detail()
        {
            lstInvoiceDetail = new List<MdlInvoiceDetail>();
        }
        public MdlInvoice invoice { get; set; }
        public List<MdlInvoiceDetail> lstInvoiceDetail { get; set; }
    }
}