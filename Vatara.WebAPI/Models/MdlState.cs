﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlState
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string shortName { get; set; }

        public MdlState GetMdl_State(vwState mdl)
        {
            return new MdlState()
            {
                id = mdl.Id,
                name = mdl.Name,
                description = mdl.Description,
                shortName = mdl.ShortName
            };
        }

        public MdlState GetMdl_State(State mdl)
        {
            return new MdlState()
            {
                id = mdl.Id,
                name = mdl.Name,
                description = mdl.Description,
                shortName = mdl.Abbrevation
            };
        }

    }
}