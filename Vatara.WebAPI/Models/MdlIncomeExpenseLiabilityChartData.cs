﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlIncomeExpenseLiabilityChartData
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }

        public MdlIncomeExpenseLiabilityChartData GetMdl_IncomeExpenseLiabilityChartData(getIncomeExpenseLiabilityChartData_Result entity)
        {
            return new MdlIncomeExpenseLiabilityChartData()
            {
                CategoryId = entity.CategoryId,
                CategoryName = entity.CategoryName,
                Amount = entity.Amount,
                Type = entity.Type,
                Category = entity.Category
            };
        }

        public MdlIncomeExpenseLiabilityChartData GetMdl_IncomeExpenseLiabilityChartData(getIncomeExpenseLiabilityChartDataForOwner_Result entity)
        {
            return new MdlIncomeExpenseLiabilityChartData()
            {
                CategoryId = entity.CategoryId,
                CategoryName = entity.CategoryName,
                Amount = entity.Amount,
                Type = entity.Type,
                Category = entity.Category
            };
        }

    }


    public class MdlIncomeChartData
    {
        public string IncomeType { get; set; }
        public Nullable<decimal> Paid { get; set; }
        public Nullable<decimal> Income { get; set; }


        public MdlIncomeChartData GetMdl_IncomeChartData(getIncomeChartData_Result entity)
        {
            return new MdlIncomeChartData()
            {
                IncomeType = entity.IncomeType,
                Paid = entity.Paid,
                Income = entity.Income               
            };
        }
    }


}