﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class DashboardChartModel
    {
        public DashboardChartModel()
        {
            lstIncomeExpenseLiabilityChartData = new List<MdlIncomeExpenseLiabilityDataForDashboard>();
            lstMdlPropertyStatusChartDataForDashboard = new List<MdlPropertyStatusChartDataForDashboard>();
            lstMdlWorkOrderChartDataForDashboard = new List<MdlWorkOrderChartDataForDashboard>();
            mdlLeaseChartDataForDashboard = new MdlLeaseChartDataForDashboard();
            mdlRentChartDataForDeshboard = new MdlRentChartDataForDeshboard();
            lstIncomeExpenseChartData = new List<MdlIncomeExpenseChartData>();
            lstIncomeChartData = new List<MdlIncomeExpenseChartData>();
        }
        public List<MdlPropertyStatusChartDataForDashboard> lstMdlPropertyStatusChartDataForDashboard { get; set; }
        public List<MdlIncomeExpenseLiabilityDataForDashboard> lstIncomeExpenseLiabilityChartData { get; set; }
        public List<MdlWorkOrderChartDataForDashboard> lstMdlWorkOrderChartDataForDashboard { get; set; }
        public MdlLeaseChartDataForDashboard mdlLeaseChartDataForDashboard { get; set; }
        public MdlRentChartDataForDeshboard mdlRentChartDataForDeshboard { get; set; }
        public List<MdlIncomeExpenseChartData> lstIncomeExpenseChartData { get; set; }
        public List<MdlIncomeExpenseChartData> lstIncomeChartData { get; set; }
    }
}