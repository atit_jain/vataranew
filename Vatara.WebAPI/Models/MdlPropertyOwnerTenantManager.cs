﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPropertyOwnerTenantManager
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<int> personType { get; set; }

        public MdlPropertyOwnerTenantManager GetMdl_PropertyOwnerTenantManager(getPropertyOwnerTenantManager_Result entity)
        {
            return new MdlPropertyOwnerTenantManager()
            {
                id = entity.Id,
                name = entity.Name,
                personType = entity.PersonType
            };
        }
    }
}