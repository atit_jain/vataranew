﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlStatus
    {
        public int id { get; set; }
        public string name { get; set; }        
        public string moduleName { get; set; }

        public MdlStatus GetMdl_Status(Status entity)
        {
            return new MdlStatus()
            {
                id = entity.Id,
                name = entity.Name,
                moduleName = entity.ModuleName
            };
        }
    }
}