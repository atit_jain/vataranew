﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlIncomeExpenseLiabilityDataForDashboard
    {
        public Nullable<decimal> Liability { get; set; }
        public Nullable<decimal> Net_Income { get; set; }
        public Nullable<decimal> Total_Income { get; set; }
        public Nullable<decimal> Expense { get; set; }
        public Nullable<int> Year { get; set; }       
        public Nullable<int> MonthNum { get; set; }      
        public string MonthYear { get; set; }

       
    }
}