﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlManagerLiabilityToOwner
    {
        public Nullable<long> propertyID { get; set; }
        public Nullable<long> owner { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string amountType { get; set; }
        public string ownerName { get; set; }

        public MdlManagerLiabilityToOwner GetMdl_ManagerLiabilityToOwner(getManagerLiabilityToOwner_Result entity)
        {
            return new MdlManagerLiabilityToOwner
            {
                propertyID = entity.PropertyID,
                amount = entity.Amount,
                owner = entity.Owner,
                amountType = entity.AmountType,
                ownerName = entity.OwnerName
            };
        }

        public MdlManagerLiabilityToOwner GetMdl_ManagerLiabilityToOwner(getOwnerLiability_Result entity)
        {
            return new MdlManagerLiabilityToOwner
            {
                propertyID = entity.PropertyID,
                amount = entity.Amount,
                owner = entity.Owner,
                amountType = entity.AmountType
            };
        }        
    }
}