﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlMessage
    {
        public long id { get; set; }
        public string fromMsg { get; set; }
        public string toMsg { get; set; }
        public string message { get; set; }
        public bool isFile { get; set; }
        public string fileExtension { get; set; }
        public Nullable<bool> isDelivered { get; set; }
        public string creationTime { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<int> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }

        public Nullable<bool> isRead { get; set; }
        public string senderFN { get; set; }
        public string senderLN { get; set; }

        public MdlMessage GetMdl_Message(Sp_GetChatInfo_Result entity)
        {
            return new MdlMessage()
            {
                id = entity.Id,
                fromMsg = entity.FromMsg,
                toMsg = entity.ToMsg,
                message = entity.Message,
                isFile = entity.IsFile,
                fileExtension = entity.FileExtension,
                isDelivered = entity.IsDelivered,
                creationTime = entity.CreationTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"),
                isDeleted = entity.IsDeleted,
                lastModificationTime = entity.LastModificationTime,
                isRead = entity.IsRead,
                senderFN = entity.SenderFN,
                senderLN = entity.SenderLN
            };

        }
    }
}