﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlTransactionHistory
    {
        public int id { get; set; }
        public Nullable<System.DateTime> tranDate { get; set; }
        public Nullable<int> from { get; set; }
        public Nullable<int> to { get; set; }
        public Nullable<int> payerType { get; set; }
        public Nullable<int> recipientType { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string paymentThrough { get; set; }
        public string hostedTransactionIdentifier { get; set; }
        public Nullable<int> propertyId { get; set; }
        public Nullable<int> invoiceNumber { get; set; }
        public string description { get; set; }
        public Nullable<bool> isPaid { get; set; }


        public TransactionHistory GetEntityTransactionHistory(MdlPayment mdlPayment, string HostedTransactionIdentifier)
        {
            Invoice tblInvoice = new Invoice();
            TransactionHistory TransactionHistory = new TransactionHistory();

            TransactionHistory.TranDate = DateTime.UtcNow;
            TransactionHistory.From = mdlPayment.manageId; //payer of money
            TransactionHistory.To = mdlPayment.clientId; //receiver of money
            TransactionHistory.PayerType = mdlPayment.PayerType;
            TransactionHistory.RecipientType = mdlPayment.RecipientType;
            TransactionHistory.Amount = mdlPayment.amount;

            if (mdlPayment.selectPaymentType == 0)
            {
                TransactionHistory.PaymentThrough = "Credit Card";
            }
            else
            {
                TransactionHistory.PaymentThrough = "ACH";
            }
            TransactionHistory.HostedTransactionIdentifier = HostedTransactionIdentifier;
            TransactionHistory.PropertyId = mdlPayment.propertyId;
            TransactionHistory.InvoiceNumber = mdlPayment.invoiceId;
            TransactionHistory.Description = mdlPayment.description;
            TransactionHistory.IsPaid = false;

            return TransactionHistory;

        }


        public MdlTransactionHistory GetMdl_TransactionHistory(TransactionHistory entity)
        {

            MdlTransactionHistory TransactionHistory = new MdlTransactionHistory();

            TransactionHistory.tranDate = entity.TranDate == null ? (DateTime?)null :entity.TranDate.Value.ToUniversalTime();
            TransactionHistory.from = entity.From; //payer of money
            TransactionHistory.to = entity.To; //receiver of money
            TransactionHistory.payerType = entity.PayerType;
            TransactionHistory.recipientType = entity.RecipientType;
            TransactionHistory.amount = entity.Amount;
            TransactionHistory.paymentThrough = entity.PaymentThrough;
            TransactionHistory.hostedTransactionIdentifier = entity.HostedTransactionIdentifier;
            TransactionHistory.propertyId = entity.PropertyId;
            TransactionHistory.invoiceNumber = entity.InvoiceNumber;
            TransactionHistory.description = entity.Description;
            TransactionHistory.isPaid = entity.IsPaid;

            return TransactionHistory;

        }
    }
}