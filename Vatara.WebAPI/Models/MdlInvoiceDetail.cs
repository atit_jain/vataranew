﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlInvoiceDetail
    {
        public int id { get; set; }
        public int invoiceNumber { get; set; }
        public int categoryID { get; set; }
        public string categoryName { get; set; }

        //public string wRNumber { get; set; }
        public string vendorInvRefNo { get; set; }

        public decimal amount { get; set; }
        public decimal markup { get; set; }
        public string markup_type { get; set; }
        public decimal tax { get; set; }
        public decimal totalAmount { get; set; }
        public Nullable<decimal> paidAmount { get; set; }
        public string description { get; set; }
        public bool isDeleted { get; set; }
        public DateTime creationTime { get; set; }
        public Nullable<Int64> creatorUserId { get; set; }

        public Nullable<DateTime> lastModificationTime { get; set; }
        public Nullable<Int64> lastModifierUserId { get; set; }
        public Nullable<Int64> deleterUserId { get; set; }
        public Nullable<DateTime> deletionTime { get; set; }
        public Nullable<int> workorderId { get; set; }
        public Nullable<int> wRNumber { get; set; }

        public MdlInvoiceDetail GetMdl_InvoiceDetail(Proc_GetInvoiceDetailByInvoiceID_Result mdl)
        {
            return new MdlInvoiceDetail()
            {
                id = mdl.Id,
                invoiceNumber = mdl.InvoiceNumber,
                categoryID = mdl.CategoryID,
                categoryName = mdl.categoryName,
                wRNumber = mdl.WRNumber,
                vendorInvRefNo = mdl.VendorInvRefNo,
                amount = mdl.Amount,
                markup = mdl.Markup,
                markup_type = mdl.Markup_type,
                tax = mdl.Tax,
                totalAmount = mdl.TotalAmount,
                paidAmount = mdl.PaidAmount,
                description = mdl.Description,
                isDeleted = mdl.IsDeleted,
                creationTime = mdl.CreationTime,
                creatorUserId = mdl.CreatorUserId,
                lastModificationTime = mdl.LastModificationTime,
                lastModifierUserId = mdl.LastModifierUserId,
                deleterUserId = mdl.DeleterUserId,
                deletionTime = mdl.DeletionTime,
                workorderId = mdl.WorkorderId
            };
        }


        public MdlInvoiceDetail GetMdl_InvoiceDetailFromView(vwInvoiceDetail mdl)
        {
            return new MdlInvoiceDetail()
            {
                id = mdl.Id,
                invoiceNumber = mdl.InvoiceNumber,
                categoryID = mdl.CategoryID,
                wRNumber = mdl.WRNumber,
                vendorInvRefNo = mdl.VendorInvRefNo,
                amount = mdl.Amount,
                markup = mdl.Markup,
                markup_type = mdl.Markup_type,
                tax = mdl.Tax,
                totalAmount = mdl.TotalAmount,
                description = mdl.Description
            };
        }

        public InvoiceDetail GetEntity_InvoiceDetail(MdlInvoiceDetail mdl)
        {
            return new InvoiceDetail()
            {
                Id = mdl.id,
                InvoiceNumber = mdl.invoiceNumber,
                CategoryID = mdl.categoryID,
                WRNumber = mdl.wRNumber.ToString(),
                VendorInvRefNo = mdl.vendorInvRefNo,
                Amount = mdl.amount,
                Markup = mdl.markup,
                Markup_type = mdl.markup_type,
                Tax = mdl.tax,
                TotalAmount = mdl.totalAmount,
                Description = mdl.description,
                IsDeleted = mdl.isDeleted,
                CreationTime = mdl.creationTime.ToUniversalTime(),
                CreatorUserId = mdl.creatorUserId
            };
        }
    }
}