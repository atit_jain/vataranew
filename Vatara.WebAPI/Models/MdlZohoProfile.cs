﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Models
{
    public class MdlZohoProfile
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string CustomerId { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CurrencyCode { get; set; }
        public string CompanyName { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> CreationUserId { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<long> ModifierUserId { get; set; }
        public Nullable<System.DateTime> ModificationTime { get; set; }
        public Nullable<long> DeleterUserId { get; set; }
        public Nullable<System.DateTime> DeletionTime { get; set; }

        public MdlZohoProfile getZohoProfile(ZohoProfile entities)
        {

            return new MdlZohoProfile()
            {
                Id = entities.Id,
                PersonId = entities.PMID,
                CustomerId = entities.CustomerId,
                DisplayName = entities.DisplayName,
                FirstName = entities.FirstName,
                LastName = entities.LastName,
                Email = entities.Email,
                Phone = entities.Phone,
                CurrencyCode = entities.CurrencyCode,
                CompanyName = entities.CompanyName,
                IsDeleted = entities.IsDeleted,
                CreationUserId = entities.CreationUserId,
                CreationTime = entities.CreationTime,
                ModificationTime = entities.ModificationTime,
                ModifierUserId = entities.ModifierUserId,
                DeleterUserId = entities.DeleterUserId,
                DeletionTime = entities.DeletionTime
            };
        }
    }

}