﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlWorkrequest
    {
        public int id { get; set; }
        public int workorderId { get; set; }
        public string description { get; set; }
        public string poNumber { get; set; }
        public string invoiceNumber { get; set; }
        public string equipmentId { get; set; }
        public int wrType { get; set; }
        public int serviceCategoryId { get; set; }
        public string serviceCategoryName { get; set; }       
        public Nullable<int> vendorId { get; set; }
        public Nullable<System.DateTime> expectedStartDate { get; set; }
        public Nullable<System.DateTime> expectedCompletionDate { get; set; }
        public Nullable<System.DateTime> actualStartDate { get; set; }
        public Nullable<System.DateTime> actualCompletionDate { get; set; }
        public Nullable<System.DateTime> nextSchedule { get; set; }
        public Nullable<int> status { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<long> deleterUserId { get; set; }       
        public Nullable<long> lastModifierUserId { get; set; }        
        public Nullable<long> creatorUserId { get; set; }
        public string wrStatus { get; set; }

        public Workrequest GetEntityWorkrequest(MdlWorkrequest mdl)
        {
            return new Workrequest() {
                Id = mdl.id,
                WorkorderId = mdl.workorderId,
                Description = mdl.description,
                PONumber = mdl.poNumber,
                InvoiceNumber = mdl.invoiceNumber,
                EquipmentId = mdl.equipmentId,
                //WRType = mdl.wrType,
                ServiceCategoryId = mdl.serviceCategoryId,
                VendorId = mdl.vendorId,
                ExpectedStartDate = mdl.expectedStartDate,
                ExpectedCompletionDate = mdl.expectedCompletionDate,
                ActualStartDate  = mdl.actualStartDate,
                ActualCompletionDate = mdl.actualCompletionDate,
                NextSchedule = mdl.nextSchedule,
                Status = mdl.status,
                IsDeleted = mdl.isDeleted,
                DeleterUserId = mdl.deleterUserId,
                LastModifierUserId = mdl.lastModifierUserId,
                CreatorUserId = mdl.creatorUserId
            };
        }

        public MdlWorkrequest GetMdl_Workrequest(Workrequest entity)
        {
            return new MdlWorkrequest()
            {
                id = entity.Id,
                workorderId = entity.WorkorderId,
                description = entity.Description,
                poNumber = entity.PONumber,
                invoiceNumber = entity.InvoiceNumber,
                equipmentId = entity.EquipmentId,
                //wrType = entity.WRType,
                serviceCategoryId = entity.ServiceCategoryId,
                vendorId = entity.VendorId,
                expectedStartDate = entity.ExpectedStartDate,
                expectedCompletionDate = entity.ExpectedCompletionDate,
                actualStartDate = entity.ActualStartDate,
                actualCompletionDate = entity.ActualCompletionDate,
                nextSchedule = entity.NextSchedule,
                status = entity.Status,
                isDeleted = entity.IsDeleted,
                deleterUserId = entity.DeleterUserId,
                lastModifierUserId = entity.LastModifierUserId,
                creatorUserId = entity.CreatorUserId
            };
        }


        public MdlWorkrequest GetMdl_Workrequest(addUpdateWorkRequest_Result entity)
        {
            return new MdlWorkrequest()
            {
                id = entity.Id,
                workorderId = entity.WorkorderId,
                description = entity.Description,
                poNumber = entity.PONumber,
                invoiceNumber = entity.InvoiceNumber,
                equipmentId = entity.EquipmentId,
                //wrType = entity.WRType,
                serviceCategoryId = entity.ServiceCategoryId,
                serviceCategoryName = entity.ServiceCategoryName,
                vendorId = entity.VendorId,
                expectedStartDate = entity.ExpectedStartDate,
                expectedCompletionDate = entity.ExpectedCompletionDate.Value.ToLocalTime(),
                actualStartDate = entity.ActualStartDate,
                actualCompletionDate = entity.ActualCompletionDate,
                nextSchedule = entity.NextSchedule,
                status = entity.Status,
                isDeleted = entity.IsDeleted,
                deleterUserId = entity.DeleterUserId,
                lastModifierUserId = entity.LastModifierUserId,
                creatorUserId = entity.CreatorUserId
            };
        }


        public MdlWorkrequest GetMdl_Workrequest(getWorkRequestsByWorkorderId_Result entity)
        {
            return new MdlWorkrequest()
            {
                id = entity.Id,
                workorderId = entity.WorkorderId,
                description = entity.Description,
                poNumber = entity.PONumber,
                invoiceNumber = entity.InvoiceNumber,
                equipmentId = entity.EquipmentId,
                //wrType = entity.WRType,
                serviceCategoryId = entity.ServiceCategoryId,
                serviceCategoryName = entity.ServiceCategoryName,
                vendorId = entity.VendorId,
                expectedStartDate = entity.ExpectedStartDate,
                expectedCompletionDate = entity.ExpectedCompletionDate,
                actualStartDate = entity.ActualStartDate,
                actualCompletionDate = entity.ActualCompletionDate,
                nextSchedule = entity.NextSchedule,
                status = entity.Status,
                isDeleted = entity.IsDeleted,
                deleterUserId = entity.DeleterUserId,
                lastModifierUserId = entity.LastModifierUserId,
                creatorUserId = entity.CreatorUserId
            };
        }
    }
}