﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlBill_Owner
    {
        public int billId { get; set; }
        public string number { get; set; }
        public DateTime date { get; set; }
        public long from { get; set; }
        public string payableFullName { get; set; }
        public long to { get; set; }
        public string receiverFullName { get; set; }
        public decimal amount { get; set; }
        public string paymentType { get; set; }
        public int managerId { get; set; }
        public string managerFullName { get; set; }
        public int ownerId { get; set; }
        public string ownerFullName { get; set; }
        public long propertyId { get; set; }
        public string propertyName { get; set; }
        public string OnlinePaymentType { get; set; }
        public string managerAdd { get; set; }
        public string payableAdd { get; set; }
        public string receiverAdd { get; set; }

        public string payablePhone { get; set; }
        public string receiverPhone { get; set; }
        public string propertyAdd { get; set; }
        public string managerPhone { get; set; }
        public string comment { get; set; }
        public string propertyName2 { get; set; }
        public string payerReceiver { get; set; }
        

        public MdlBill_Owner GetMdl(getAllBill_Result billOwner)
        {
            if (clsConstants.paymentTypeOnline == billOwner.PaymentType)
            {
                billOwner.PaymentType = billOwner.OnlinePaymentType;
            }
            return new MdlBill_Owner()
            {
                billId = billOwner.BillId,
                number = billOwner.Number=="0"?"": billOwner.Number,
                date = billOwner.Date,
                from = billOwner.From,
                payableFullName = billOwner.PayableFullName,
                to = billOwner.To,
                receiverFullName = billOwner.ReceiverFullName,
                amount = billOwner.Amount,
                paymentType = billOwner.PaymentType,
                managerId = billOwner.ManagerId,
                managerFullName = billOwner.ManagerFullName,
                ownerId = billOwner.OwnerId == null?0: Convert.ToInt32(billOwner.OwnerId),
                ownerFullName = billOwner.OwnerFullName,
                propertyId = billOwner.PropertyId,
                propertyName = billOwner.PropertyName,
                managerAdd = billOwner.ManagerAdd,
                payableAdd = billOwner.PayableAdd,
                receiverAdd = billOwner.ReceiverAdd,
                comment = billOwner.Comment,
                payablePhone = billOwner.PayablePhone,
                receiverPhone = billOwner.ReceiverPhone,
                propertyAdd = billOwner.PropertyAdd,
                managerPhone = billOwner.ManagerPhone,
                propertyName2 = billOwner.PropertyName2
            };
        }


        public MdlBill_Owner GetMdl(getBillById_Result billOwner)
        {
            if (clsConstants.paymentTypeOnline == billOwner.PaymentType)
            {
                billOwner.PaymentType = billOwner.OnlinePaymentType;
            }
            return new MdlBill_Owner()
            {
                billId = billOwner.BillId,
                number = billOwner.Number == "0" ? "" : billOwner.Number,
                date = billOwner.Date,
                from = billOwner.From,
                payableFullName = billOwner.PayableFullName,
                to = billOwner.To,
                receiverFullName = billOwner.ReceiverFullName,
                amount = billOwner.Amount,
                paymentType = billOwner.PaymentType,
                managerId = billOwner.ManagerId,
                managerFullName = billOwner.ManagerFullName,
                ownerId = billOwner.OwnerId == null ? 0 : Convert.ToInt32(billOwner.OwnerId),
                ownerFullName = billOwner.OwnerFullName,
                propertyId = billOwner.PropertyId,
                propertyName = billOwner.PropertyName,
                managerAdd = billOwner.ManagerAdd,
                payableAdd = billOwner.PayableAdd,
                receiverAdd = billOwner.ReceiverAdd,
                comment = billOwner.Comment,
                payablePhone = billOwner.PayablePhone,
                receiverPhone = billOwner.ReceiverPhone,
                propertyAdd = billOwner.PropertyAdd,
                managerPhone = billOwner.ManagerPhone,
                propertyName2 = billOwner.PropertyName2
            };
        }



        public MdlBill_Owner GetMdl(getBillByPersonIdAndPropertyId_Result billOwner)
        {
            if (clsConstants.paymentTypeOnline == billOwner.PaymentType)
            {
                billOwner.PaymentType = billOwner.OnlinePaymentType;
            }
            return new MdlBill_Owner()
            {
                billId = billOwner.BillId,
                number = billOwner.Number == "0" ? "" : billOwner.Number,
                date = billOwner.Date,
                from = billOwner.From,
                payableFullName = billOwner.PayableFullName,
                to = billOwner.To,
                receiverFullName = billOwner.ReceiverFullName,
                amount = billOwner.Amount,
                paymentType = billOwner.PaymentType,
                managerId = billOwner.ManagerId,
                managerFullName = billOwner.ManagerFullName,
                ownerId = billOwner.OwnerId == null ? 0 : Convert.ToInt32(billOwner.OwnerId),
                ownerFullName = billOwner.OwnerFullName,
                propertyId = billOwner.PropertyId,
                propertyName = billOwner.PropertyName,
                managerAdd = billOwner.ManagerAdd,
                payableAdd = billOwner.PayableAdd,
                receiverAdd = billOwner.ReceiverAdd,
                comment = billOwner.Comment,
                payablePhone = billOwner.PayablePhone,
                receiverPhone = billOwner.ReceiverPhone,
                propertyAdd = billOwner.PropertyAdd,
                managerPhone = billOwner.ManagerPhone,
                propertyName2 = billOwner.PropertyName2
            };
        }


    }
}