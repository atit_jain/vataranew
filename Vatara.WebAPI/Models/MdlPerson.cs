﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPerson
    {
        public int pId { get; set; }
        public int pTypeId { get; set; }
        public string pTypeName { get; set; }
        public string pFirstName { get; set; }
        public string pMiddleName { get; set; }
        public string pLastName { get; set; }
        public string Initial { get; set; }
        public string pFullName { get; set; }
        public Nullable<int> pServiceCategoryId { get; set; }
        public string pServiceCategoryName { get; set; }
        public string pCompanyName { get; set; }
        public Nullable<int> pStatusId { get; set; }
        public string pStatusName { get; set; }
        public string pMailBoxNo { get; set; }
        public string pAddress { get; set; }
        public Nullable<int> pCountryId { get; set; }
        public string pCountryName { get; set; }
        public Nullable<int> pStateId { get; set; }
        public string pStateName { get; set; }
        public Nullable<int> pCityId { get; set; }
        public string pCityName { get; set; }
        public Nullable<int> pCountyId { get; set; }
        public string pCountyName { get; set; }
        public string pZipcode { get; set; }
        public string pPhoneNo { get; set; }
        public string pFaxNo { get; set; }
        public string pEmail { get; set; }
        public string pFBUrl { get; set; }
        public string pTwitterUrl { get; set; }
        public string pImage { get; set; }
        public Nullable<int> pAddressId { get; set; }
        public string pAdrsLine1 { get; set; }
        public string pAdrsLine2 { get; set; }
        public Nullable<int> CreatorUserId { get; set; }
        public string pOwnerType { get; set; }


        public MdlPerson GetMdl_Person(vwPerson mdl)
        {
            return new MdlPerson()
            {
                pId = mdl.pId,
                pTypeId = mdl.pTypeId,
                pTypeName = mdl.pTypeName,
                pFirstName = mdl.pFirstName,
                pMiddleName = mdl.pMiddleName,
                pLastName = mdl.pLastName,
                pFullName = mdl.pFullName,
                pAddress = mdl.pAddress,
                pCountryName = mdl.pCountryName,
                pStateName = mdl.pStateName,
                pCityName = mdl.pCityName,
                pCountyName = mdl.pCountyName,
                pPhoneNo = mdl.pPhoneNo,
                pZipcode = mdl.pZipcode,
                pFaxNo = mdl.pFaxNo,
                pEmail = mdl.pEmail,
                pImage = mdl.pImage,
                pAddressId = mdl.AddressId,
                pAdrsLine1 = mdl.Line1,
                pAdrsLine2 = mdl.Line2,
                pCountryId = mdl.pCountryId,
                pStateId = mdl.pStateId,
                pCountyId = mdl.pCountyId,
                pCityId = mdl.pCityId
            };
        }


        public MdlPerson GetMdl_Person(getPersonDetailByPersonId_Result mdl)
        {
            return new MdlPerson()
            {
                pId = mdl.pId,
                pTypeId = mdl.pTypeId,
                pTypeName = mdl.pTypeName,
                pFirstName = mdl.pFirstName,
                pMiddleName = mdl.pMiddleName,
                pLastName = mdl.pLastName,
                pFullName = mdl.pFullName,
                pAddress = mdl.pAddress,
                pCountryName = mdl.pCountryName,
                pStateName = mdl.pStateName,
                pCityName = mdl.pCityName,
                pCountyName = mdl.pCountyName,
                pPhoneNo = mdl.pPhoneNo,
                pZipcode = mdl.pZipcode,
                pFaxNo = mdl.pFaxNo,
                pEmail = mdl.pEmail,
                pImage = mdl.pImage
            };
        }

        public MdlPerson GetMdl_Owner(getOwnerDetail_Result mdl)
        {
            return new MdlPerson()
            {
                pId = mdl.Id,
                pTypeId = mdl.TypeId,
                pTypeName = mdl.TypeName,
                pFirstName = mdl.Name,
                pMiddleName = mdl.Name,
                pLastName = mdl.Name,
                pFullName = mdl.Name,
                pAddress = mdl.Address,
                pCountryName = mdl.Country,
                pStateName = mdl.State,
                pCityName = mdl.City,
                pCountyName = mdl.County,
                pPhoneNo = mdl.Phone,
                pZipcode = mdl.Zipcode,
                pFaxNo = mdl.Fax,
                pEmail = mdl.Email,
                pImage = mdl.Image,
                pAddressId = mdl.AddressId,
                pAdrsLine1 = mdl.AdrsLine1,
                pAdrsLine2 = mdl.AdrsLine2,
                pCountryId = mdl.CountryId,
                pStateId = mdl.StateId,
                pCountyId = mdl.CountyId,
                pCityId = mdl.CityId
            };
        }


        public MdlPerson GetMdl_Owner(getOwnerDetailByOwnerPersonIdAndPOID_Result mdl)
        {
            return new MdlPerson()
            {
                pId = mdl.Id,
                pFirstName = mdl.FirstName,
                pMiddleName = mdl.LastName,
                pLastName = mdl.Name,
                pOwnerType=mdl.OwnerType,
                pFullName = mdl.Name,
                pAddress = mdl.Address,
                pCountryName = mdl.Country,
                pStateName = mdl.State,
                pCityName = mdl.City,
                pCountyName = mdl.County,
                pPhoneNo = mdl.Phone,
                pZipcode = mdl.Zipcode,
                pEmail = mdl.Email,
                pAddressId = mdl.AddressId,
                pAdrsLine1 = mdl.AdrsLine1,
                pAdrsLine2 = mdl.AdrsLine2,
                pCountryId = mdl.CountryId,
                pStateId = mdl.StateId,
                pCountyId = mdl.CountyId,
                pCityId = mdl.CityId,
                pImage = mdl.Image
            };
        }

        public MdlPerson GetMdl_Manager(getManagerDetail_Result mdl)
        {
            return new MdlPerson()
            {
                pId = mdl.Id,
                // pTypeId = mdl.pTypeId,
                pTypeName = mdl.TypeName,
                pFirstName = mdl.Name,
                //pMiddleName = mdl.Name,
                pLastName = mdl.Name,
                pFullName = mdl.Name,
                pAddress = mdl.Address,
                pCountryName = mdl.Country,
                pStateName = mdl.State,
                pCityName = mdl.City,
                pCountyName = mdl.County,
                pPhoneNo = mdl.Phone,
                pZipcode = mdl.Zipcode,
                pFaxNo = mdl.Fax,
                pEmail = mdl.Email,
                pImage = mdl.Image
            };
        }

    }
}