﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class CompanyManager
    {
        public int personId { get; set; }
        public int userId { get; set; }
        public int personTypeId { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<int> addressId { get; set; }
        public string email { get; set; }
        public string phoneNo { get; set; }
        public string fax { get; set; }
        public string image { get; set; }
        public Nullable<int> pmid { get; set; }
        public string userRole { get; set; }
        public string mailBoxNo { get; set; }
        public int addressTypeId { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public int countryId { get; set; }
        public int stateId { get; set; }
        public int countyId { get; set; }
        public int cityId { get; set; }
        public string zipcode { get; set; }
        public string fullAdrs { get; set; }
        public int creatorUserId { get; set; }

        public Nullable<System.DateTime> creationTime { get; set; }
        public Nullable<bool> managerStatus { get; set; }



        public CompanyManager GetMdl_CompanyManager(vwCompanyManager view)
        {
            return new CompanyManager
            {
                personId = view.PersonId,
                userId = view.UserId,
                personTypeId = view.PersonTypeId,
                firstName = view.FirstName,
                middleName = view.MiddleName,
                lastName = view.LastName,
                status = view.Status,
                addressId = view.AddressId,
                email = view.Email,
                phoneNo = view.PhoneNo,
                fax = view.Fax,
                image = view.Image,
                pmid = view.PMID,
                userRole = view.UserRole,
                mailBoxNo = view.MailBoxNo,
                addressTypeId = view.AddressTypeId,
                line1 = view.Line1,
                line2 = view.Line2,
                countryId = view.CountryId,
                stateId = view.StateId,
                countyId = view.CountyId,
                cityId = view.CityId,
                zipcode = view.Zipcode,
                fullAdrs = view.FullAdrs,
                creationTime = view.CreationTime,
                managerStatus = view.ManagerStatus
            };
        }


        public User GetEntity_User(CompanyManager mdl)
        {
            return new User()
            {
                Email = mdl.email,
            };
        }

        public Person GetEntity_Person(CompanyManager mdl)
        {
            return new Person()
            {
                Email = mdl.email,
                FirstName = mdl.firstName,
                MiddleName = mdl.middleName,
                LastName = mdl.lastName,
                PhoneNo = mdl.phoneNo,
                Fax = mdl.fax,
                Image = mdl.image
            };
        }

        public Address GetEntity_Address(CompanyManager mdl)
        {
            return new Address()
            {
                Line1 = mdl.line1,
                Line2 = mdl.line2,
                CountryId = mdl.countryId,
                StateId = mdl.stateId,
                CountyId = mdl.countyId,
                CityId = mdl.cityId,
                Zipcode = mdl.zipcode

            };
        }

    }
}