﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPendingRentInvoices
    {
        public int id { get; set; }
        public string number { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public Nullable<System.DateTime> dueDate { get; set; }
        public Nullable<long> propertyID { get; set; }
        public Nullable<long> from { get; set; }
        public Nullable<long> to { get; set; }
        public string leaseId { get; set; }
        public int intLeaseID { get; set; }       

        public MdlPendingRentInvoices GetMdl_PendingRentInvoices(vwGetPendingRentInvoice vw)
        {
            return new MdlPendingRentInvoices()
            {
                id = vw.Id,
                number = vw.Number,
                date = vw.Date,
                dueDate = vw.DueDate,
                propertyID = vw.PropertyID,
                from = vw.From,
                to = vw.To,
                leaseId = vw.LeaseId,
                intLeaseID = vw.IntLeaseID

            };
        }
    }
}