﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlEventTracker
    {
        public int eventID { get; set; }
        public Nullable<int> propertyID { get; set; }
        public string eventType { get; set; }
        public Nullable<System.DateTime> eventDate { get; set; }
        public string description { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<long> creatorUserId { get; set; }

        public EventTracker GetEntity_EventTracker(MdlEventTracker mdl)
        {
            return new EventTracker()
            {
                EventID = mdl.eventID,
                EventType = mdl.eventType,
                PropertyID = mdl.propertyID,
                EventDate = mdl.eventDate,
                Description = mdl.description,
                IsDeleted = mdl.isDeleted,
                CreatorUserId = mdl.creatorUserId
            };
        }

        public MdlEventTracker GetMdl_EventTracker(Sp_GetEventList_Result entity)
        {
            return new MdlEventTracker()
            {
                eventID = entity.EventID,
                eventType = entity.EventType,
                propertyID = entity.PropertyID,
                eventDate = entity.EventDate,
                description = entity.Description,
                isDeleted = entity.IsDeleted,
                creatorUserId = entity.CreatorUserId
            };
        }

        public MdlEventTracker GetMdl_EventTracker(EventTracker entity)
        {
            return new MdlEventTracker()
            {
                eventID = entity.EventID,
                eventType = entity.EventType,
                propertyID = entity.PropertyID,
                eventDate = entity.EventDate,
                description = entity.Description,
                isDeleted = entity.IsDeleted,
                creatorUserId = entity.CreatorUserId
            };
        }

    }

}