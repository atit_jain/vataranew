﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlFile
    {
        public int linkId { get; set; }
        public string moduleId { get; set; }       
        public string fileType { get; set; }
        public string name { get; set; }
        public int seqNo { get; set; }       
        public string description { get; set; }
        public string fileSavePath { get; set; }
        public string tags { get; set; }
        public bool isDeleted { get; set; }
    }
}