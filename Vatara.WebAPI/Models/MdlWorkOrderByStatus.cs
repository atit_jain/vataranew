﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlWorkOrderByStatus
    {
        public int woid { get; set; }
        public int wrid { get; set; }
        public int propertyId { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> requestedDate { get; set; }
        public Nullable<int> requestedBy { get; set; }
        public string status { get; set; }
        public Nullable<int> statusId { get; set; }
        public Nullable<int> requestedByPersonType { get; set; }
        public string propertyName { get; set; }
        public string requestedByName { get; set; }
        public string docName { get; set; }
        public Nullable<int> totalWorkRequests { get; set; }
        public Nullable<int> closedWorkRequests { get; set; }
        public Nullable<System.DateTime> expectedStartDate { get; set; }
        public Nullable<System.DateTime> expectedCompletionDate { get; set; }

        public MdlWorkOrderByStatus GetMdl_WorkOrderByStatus(GetWorkOrderByStatus_Result entity)
        {
            return new MdlWorkOrderByStatus()
            {
                woid = entity.WOID,               
                propertyId = entity.PropertyId,
                description = entity.Description,
                requestedDate = entity.RequestedDate,
                requestedBy = entity.RequestedBy,
                status = entity.Status,
                statusId = entity.StatusId,
                requestedByPersonType = entity.RequestedByPersonType,
                propertyName = entity.PropertyName,
                requestedByName = entity.RequestedByName,
                docName = entity.DocName,
                totalWorkRequests = entity.TotalWorkRequests,
                closedWorkRequests = entity.ClosedWorkRequests,
                expectedStartDate = entity.ExpectedStartDate,
                expectedCompletionDate = entity.ExpectedCompletionDate
            };
        }
    }
}