﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlTenantLease
    {
        public int pId { get; set; }
        public string pLeaseId { get; set; }
        public string pLeaseDesc { get; set; }
        public string pLeaseType { get; set; }
        public int pAgreementTypeId { get; set; }
        public string pAgreementTypeName { get; set; }
        public int pTerm { get; set; }
        public short pTermType { get; set; }
        public string pTermTypeName { get; set; }
        public decimal pSecurityDeposit { get; set; }
        public decimal pRentalAmount { get; set; }
        public Nullable<System.DateTime> pStartDate { get; set; }
        public Nullable<System.DateTime> pEndDate { get; set; }
        public int pPropertyId { get; set; }
        public int pTypeId { get; set; }
        public string pPropertyNumber { get; set; }
        public string pPropertyName { get; set; }
        public string pAddress { get; set; }
        public string pAddress2 { get; set; }
        public string pCountry { get; set; }
        public string pState { get; set; }
        public string pCity { get; set; }
        public string pCounty { get; set; }
        public string pZipCode { get; set; }
        public Nullable<int> pCountryId { get; set; }
        public Nullable<int> pStateId { get; set; }
        public Nullable<int> pCityId { get; set; }
        public Nullable<int> pCountyId { get; set; }
        public string pOwnerName { get; set; }
        public Nullable<decimal> pRentableArea { get; set; }
        public Nullable<decimal> pGrossArea { get; set; }
        public string pParkingSpaces { get; set; }
        public string pLeaseActive { get; set; }
        public int pPaymentFrequencyId { get; set; }
        public string pPaymentFrequencyName { get; set; }
        public decimal pLatePaymentFee { get; set; }
        public int pLatePaymentFeeChargedId { get; set; }
        public bool pIsMoveInInspectionReqd { get; set; }
        public bool pIsMoveOutInspectionReqd { get; set; }
        public int pMaxPeopleAllowed { get; set; }
        public string Tenants { get; set; }


        public MdlTenantLease GetMdl_TenantLease(vwLease mdl)
        {
            return new MdlTenantLease()
            {
                pId = mdl.pId,
                pLeaseId = mdl.pLeaseId,
                pLeaseDesc = mdl.pLeaseDesc,
                pLeaseType = mdl.pLeaseType,
                pAgreementTypeId = mdl.pAgreementTypeId,
                pTerm = mdl.pTerm,
                pTermType = mdl.pTermType,
                pTermTypeName = mdl.pTermTypeName,
                pSecurityDeposit = mdl.pSecurityDeposit,
                pRentalAmount = mdl.pRentalAmount,
                pStartDate = mdl.pStartDate,
                pEndDate = mdl.pEndDate,
                pPropertyId = mdl.pPropertyId,
                pTypeId = mdl.pTypeId,
                pPropertyNumber = mdl.pPropertyNumber,
                pPropertyName = mdl.pPropertyName,
                pAddress = mdl.pAddress,
                pAddress2 = mdl.pAddress2,
                pCountry = mdl.pCountry,
                pState = mdl.pState,
                pCity = mdl.pCity,
                pCounty = mdl.pCounty,
                pZipCode = mdl.pZipCode,
                pOwnerName = mdl.pOwnerName,
                pRentableArea = mdl.pRentableArea,
                pGrossArea = mdl.pGrossArea,
                pParkingSpaces = mdl.pParkingSpaces,
                pLeaseActive = mdl.pLeaseActive
            };
        }
    }
}