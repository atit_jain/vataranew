﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlPropertyRegistration
    {

        public MdlPropertyRegistration()
        {
            lstMdlOwner = new List<MdlOwner>();
        }
        public MdlOwnerMaster OwnerMaster { get; set; }
        public List<MdlOwner> lstMdlOwner { get; set; }
        public string Ownertype { get; set; }        
        public int creatorId { get; set; }
        public bool IsNewOwner { get; set; }      
        public MdlPropertyMap PropertyMap { get; set; }
        
    }
}