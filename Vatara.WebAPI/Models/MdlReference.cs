﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlReference
    {
        public long id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullname { get; set; }
        public string phoneNo { get; set; }
        public string email { get; set; }
        public int leaseId { get; set; }
        public int tenantId { get; set; }

        public MdlReference GetMdl_Reference(vwReference mdl)
        {
            return new MdlReference()
            {
                id = mdl.Id,
                firstName = mdl.FirstName,
                lastName = mdl.LastName,
                fullname = mdl.Fullname,
                phoneNo = mdl.PhoneNo,
                email = mdl.Email,
                leaseId = mdl.LeaseId,
                tenantId = mdl.TenantId
            };
        }
    }
}