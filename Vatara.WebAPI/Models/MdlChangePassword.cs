﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlChangePassword
    {
        public string email { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }        
    }


    public class MdlChangeEmail
    {
        public string oldemail { get; set; }
        public string newemail { get; set; }
        public int personTypeId { get; set; }
        public Nullable<int> personId { get; set; }
        public Nullable<int> pmid { get; set; }
        public Nullable<int> poid { get; set; }
    }
}