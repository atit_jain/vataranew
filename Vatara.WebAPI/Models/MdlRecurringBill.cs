﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlRecurringBill
    {
        public int billId { get; set; }
        public bool isRecurringPayment { get; set; }
        public string property { get; set; }
        public decimal amount { get; set; }
        public string email { get; set; }
        public string payer { get; set; }
        public string reciver { get; set; }
        public string reciverName { get; set; }
        public string frequency { get; set; }
        public Nullable<System.DateTime> recurringDate { get; set; }
        public string cardDrawn { get; set; }
        public string propAddress { get; set; }
        public MdlRecurringBill GetMdl_RecurringBill(RecurringBillByPersonId_Result recurring)
        {
            return new MdlRecurringBill
            {
                billId = recurring.BillId,
                isRecurringPayment = Convert.ToBoolean(recurring.IsRecurringPayment),
                propAddress = recurring.PropAddress,
                amount = recurring.Amount,
                //  email = recurring.Email,
                payer = recurring.Payer,
                reciver = recurring.Reciver,
                reciverName = recurring.ReciverName,
                frequency = recurring.frequency,
                recurringDate = recurring.RecurringDate==null? (DateTime?)null : recurring.RecurringDate.Value.ToUniversalTime(),

                cardDrawn = recurring.CardDrawn
            };
        }
        public MdlRecurringBill GetMdl_RecurringBillByProperty(RecurringBillByPersonIdAndPropertyID_Result recurring)
        {
            return new MdlRecurringBill
            {
                billId = recurring.BillId,
                isRecurringPayment = Convert.ToBoolean(recurring.IsRecurringPayment),
                propAddress = recurring.PropAddress,
                amount = recurring.Amount,
                //  email = recurring.Email,
                payer = recurring.Payer,
                reciver = recurring.Reciver,
                reciverName = recurring.ReciverName,
                frequency = recurring.frequency,
                recurringDate = recurring.RecurringDate == null ? (DateTime?)null : recurring.RecurringDate.Value.ToUniversalTime(),

                cardDrawn = recurring.CardDrawn
            };
        }
    }
}