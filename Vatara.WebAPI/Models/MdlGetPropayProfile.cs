﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlGetPropayProfile
    {
        public int personId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public System.DateTime dob { get; set; }
        public string email { get; set; }
        public int addressId { get; set; }
        public string phone { get; set; }
        public string marchantAccntNum { get; set; }
        public string marchantProfileId { get; set; }
        public string marchantPassword { get; set; }
        public string payerId { get; set; }
        public Nullable<int> LastPaymentType { get; set; }
        public Nullable<int> countryId { get; set; }
        public string countryName { get; set; }
        public Nullable<int> stateId { get; set; }
        public string stateName { get; set; }
        public Nullable<int> countyId { get; set; }
        public string countyName { get; set; }
        public Nullable<int> cityId { get; set; }
        public string cityName { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string mailBoxNo { get; set; }
        public string zipCode { get; set; }
        public long ssn { get; set; }


        public MdlGetPropayProfile GetMdl_PorpayProfile(getPropayProfile_Result entity)
        {
            return new MdlGetPropayProfile()
            {
                personId = entity.PersonId,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                dob = entity.Dob,
                email = entity.Email,
                addressId = entity.AddressId,
                phone = entity.Phone,
                marchantAccntNum = entity.MarchantAccntNum,
                marchantProfileId = entity.MarchantProfileId,
                marchantPassword = entity.MarchantPassword,
                payerId = entity.PayerId,
                LastPaymentType = entity.LastPaymentType,
                countryId = entity.CountryId,
                countryName = entity.Country,
                stateId = entity.StateId,
                stateName = entity.State,
                cityId = entity.CityId,
                cityName = entity.City,
                countyId = entity.CountyId,
                countyName = entity.County,
                address = entity.Line1,
                address2 = entity.Line2,
                zipCode = entity.Zipcode,
                mailBoxNo = entity.MailBoxNo,
                ssn=entity.ssn
            };
        }



        public MdlGetPropayProfile GetMdl_PorpayProfileForMarchantAndPayerId(getPropayProfileForMarchantAndPayerId_Result entity)
        {
            return new MdlGetPropayProfile()
            {
                personId = entity.PersonId,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                dob = entity.Dob,
                email = entity.Email,
                addressId = entity.AddressId,
                phone = entity.Phone,
                marchantAccntNum = entity.MarchantAccntNum,
                marchantProfileId = entity.MarchantProfileId,
                marchantPassword = entity.MarchantPassword,
                payerId = entity.PayerId,
                LastPaymentType = entity.LastPaymentType,
                countryId = entity.CountryId,
                countryName = entity.Country,
                stateId = entity.StateId,
                stateName = entity.State,
                cityId = entity.CityId,
                cityName = entity.City,
                countyId = entity.CountyId,
                countyName = entity.County,
                address = entity.Line1,
                address2 = entity.Line2,
                zipCode = entity.Zipcode,
                mailBoxNo = entity.MailBoxNo
            };
        }
    }
}