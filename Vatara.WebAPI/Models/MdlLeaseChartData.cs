﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlLeaseChartData
    {
        public Nullable<int> ActiveLeases { get; set; }
        public Nullable<int> LeasesExpiring { get; set; }

        public MdlLeaseChartData Get_MdlLeaseChartData(getLeaseChartDataForDashboard_Result entity)
        {
            return new MdlLeaseChartData()
            {
                ActiveLeases = entity.ActiveLeases,
                LeasesExpiring = entity.LeasesExpiring
            };
        }
    }
}