﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlRecurringPaymentInstruction
    {
        public RecurringPaymentInstruction GetEntity_RecurringPaymentInstruction(MdlPayment mdlPayment, HostedTransactionInfo hostedTransactionInfo)
        {
            return new RecurringPaymentInstruction()
            {
                frequency  = mdlPayment.paymentSchedule,
                RecurringDate = mdlPayment.startOnRecurring,
                NextTransactionDate = mdlPayment.startOnRecurring,
                Status = true,
                TransactionId = hostedTransactionInfo.TransactionId,
                TransactionHistoryId = hostedTransactionInfo.TransactionHistoryId
            };
        }
    }
}