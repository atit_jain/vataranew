﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlManagerLiabilityToTenant
    {
        public Nullable<long> propertyID { get; set; }
        public Nullable<long> tenant { get; set; }
        public string tenantName { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string amountType { get; set; }


        public MdlManagerLiabilityToTenant GetMdl_ManagerLiabilityToTenant(getManagerLiabilityToTenant_Result entity)
        {
            return new MdlManagerLiabilityToTenant
            {
                propertyID = entity.PropertyID,
                amount = entity.Amount,
                tenant = entity.Tenant,
                amountType = entity.AmountType,
                tenantName = entity.TenantName
            };
        }

    }
}