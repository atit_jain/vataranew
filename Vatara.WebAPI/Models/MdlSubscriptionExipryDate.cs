﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlSubscriptionExipryDate
    {
        public string subscriptionNumber { get; set; }
        public Nullable<int> pmid { get; set; }
        public Nullable<System.DateTime> subsStartDate { get; set; }
        public Nullable<System.DateTime> subsEndDate { get; set; }
        public Nullable<int> remainingDaysToExpire { get; set; }
        public Nullable<bool> isActive { get; set; }

        public MdlSubscriptionExipryDate GetMdl_SubscriptionExipryDate(getSubscriptionExipryDate_Result entity)
        {
            if (entity == null)
            {
                return new MdlSubscriptionExipryDate();
            }
            else
            {
                return new MdlSubscriptionExipryDate()
                {
                    subscriptionNumber = entity.SubscriptionNumber,
                    pmid = entity.PMID,
                    subsStartDate = entity.SubsStartDate == null ? (DateTime?)null : entity.SubsStartDate.Value.ToUniversalTime(),
                    subsEndDate = entity.SubsEndDate == null ? (DateTime?)null : entity.SubsEndDate.Value.ToUniversalTime(),
                    remainingDaysToExpire = entity.RemainingDaysToExpire,
                    isActive = entity.IsActive
                };
            }           
        }
    }
}