﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlBill
    {
        public int billId { get; set; }
        public int invoiceNumber { get; set; }
        public System.DateTime date { get; set; }
        public long propertyID { get; set; }
        public long from { get; set; }
        public long to { get; set; }
        public decimal amount { get; set; }
        public string paymentType { get; set; }
        public string comment { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<long> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<int> lastModifierUserId { get; set; }
        public System.DateTime creationTime { get; set; }
        public Nullable<long> creatorUserId { get; set; }

        public Bill GetEntity_Bill(MdlBill mdl)
        {
            return new Bill()
            {
                BillId = mdl.billId,
                InvoiceNumber = mdl.invoiceNumber,
                PropertyID = mdl.propertyID,
                Date = mdl.date.ToUniversalTime(),
                From = mdl.from,
                To = mdl.to,
                Amount = mdl.amount,
                PaymentType = mdl.paymentType,
                Comment = mdl.comment,
                IsDeleted = mdl.isDeleted,
                DeleterUserId = mdl.deleterUserId,
                DeletionTime = mdl.deletionTime == null ? (DateTime?)null : mdl.deletionTime.Value.ToUniversalTime(),
                LastModificationTime = mdl.lastModificationTime == null ? (DateTime?)null : mdl.lastModificationTime.Value.ToUniversalTime(),
                LastModifierUserId = mdl.lastModifierUserId,
                CreationTime = mdl.creationTime.ToUniversalTime(),
                CreatorUserId = mdl.creatorUserId
            };
        }

        public MdlBill GetMdl_Bill(vwBill mdl)
        {
            return new MdlBill()
            {
                billId = mdl.BillId,
                invoiceNumber = mdl.InvoiceNumber,
                propertyID = mdl.PropertyID == null? 0 : Convert.ToInt64(mdl.PropertyID),
                date = mdl.Date == null? new DateTime() : Convert.ToDateTime(mdl.Date),
                from = mdl.From == null ? 0 : Convert.ToInt64(mdl.From),
                to = mdl.To == null ? 0 : Convert.ToInt64(mdl.To),
                amount = mdl.Amount == null ? 0 : Convert.ToDecimal(mdl.Amount),
                paymentType = mdl.PaymentType,
                comment = mdl.Comment,               
            };
        }
    }
}