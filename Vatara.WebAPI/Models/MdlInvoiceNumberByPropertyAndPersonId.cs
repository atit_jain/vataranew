﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlInvoiceNumberByPropertyAndPersonId
    {
        public int invoiceId { get; set; }
        public string invoiceNumber { get; set; }
        public Nullable<long> propertyID { get; set; }

        public MdlInvoiceNumberByPropertyAndPersonId GetMdl_InvoiceNumberByPropertyAndPersonId(getInvoiceNumberByPropertyAndPersonId_Result entity)
        {
            return new MdlInvoiceNumberByPropertyAndPersonId
            {
                invoiceId = entity.InvoiceId,
                invoiceNumber = entity.InvoiceNumber,
                propertyID = entity.PropertyID
            };
        }
    }


   
}