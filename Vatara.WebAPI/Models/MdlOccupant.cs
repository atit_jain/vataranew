﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlOccupant
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string occFullName { get; set; }
        public string relation { get; set; }
        public int leaseId { get; set; }
        public string detail { get; set; }
        public int tenantId { get; set; }

        public bool isDeleted { get; set; }
        public MdlOccupant GetMdl_Occupant(vwOccupant mdl)
        {
            return new MdlOccupant()
            {
                id = mdl.Id,
                firstName = mdl.FirstName,
                lastName = mdl.LastName,
                occFullName = mdl.OccFullName,
                relation = mdl.Relation,
                leaseId = mdl.LeaseId,
                detail = mdl.Detail,
                tenantId = mdl.TenantId
            };

        }
    }
}