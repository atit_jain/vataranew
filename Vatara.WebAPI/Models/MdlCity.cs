﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlCity
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int stateId { get; set; }

        public MdlCity GetMdl_County(vwCity mdl)
        {
            return new MdlCity()
            {
                id = mdl.Id,
                name = mdl.Name,
                description = mdl.Description,
                stateId = mdl.StateId
            };
        }
    }
}