﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlOwnerIncomeExpenseByPropertyIdLeaseId
    {
        public string type { get; set; }
        public string category { get; set; }
        public decimal mtd { get; set; }
        public Nullable<decimal> ytd { get; set; }


        public MdlOwnerIncomeExpenseByPropertyIdLeaseId GetMdl_OwnerIncomeExpenseByPropertyIdLeaseId(getOwnerIncomeExpenseByPropertyIdLeaseId_Result entity)
        {
            return new MdlOwnerIncomeExpenseByPropertyIdLeaseId()
            {
                type = entity.Type,
                category = entity.Category,
                mtd = entity.MTD,
                ytd = entity.YTD
            };
        }

    }


    public class MdlOwnerIncomeExpenseListByPropertyIdLeaseId
    {
        public MdlOwnerIncomeExpenseListByPropertyIdLeaseId()
        {
            lstMdlOwnerIncomeByPropertyIdLeaseId = new List<MdlOwnerIncomeExpenseByPropertyIdLeaseId>();
            lstMdlOwnerExpenseByPropertyIdLeaseId = new List<MdlOwnerIncomeExpenseByPropertyIdLeaseId>();
        }
        public List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerIncomeByPropertyIdLeaseId { get; set; }
        public List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerExpenseByPropertyIdLeaseId { get; set; }
        public decimal totalIncomeMTD { get; set; }
        public decimal totalIncomeYTD { get; set; }
        public decimal totalExpenseMTD { get; set; }
        public decimal totalExpenseYTD { get; set; }
    }

}