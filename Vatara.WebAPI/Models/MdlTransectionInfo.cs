﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlTransectionInfo
    {
        /// <summary>
        /// Important :  Dont add any new property in-between these
        ///              If you want to add any new property, 
        ///              add that after last property        ///              
        ///              (i.e. public decimal December { get; set; }  )
        /// </summary>      
           
        public int billId { get; set; }
        public int invoiceNumber { get; set; }
        public System.DateTime date { get; set; }
        public long propertyID { get; set; }
        public long from { get; set; }
        public long to { get; set; }
        public decimal amount { get; set; }
        public string paymentType { get; set; }
        public string comment { get; set; }
        public int categoryId { get; set; }
        public string serviceCategory { get; set; }
        public string monthName { get; set; }
        public Nullable<int> getYear { get; set; }
        public string incmExpence { get; set; }
        public Nullable<int> monthNum { get; set; }
        public decimal January { get; set; }
        public decimal February { get; set; }
        public decimal March { get; set; }
        public decimal April { get; set; }
        public decimal May { get; set; }
        public decimal June { get; set; }
        public decimal July { get; set; }
        public decimal August { get; set; }
        public decimal September { get; set; }
        public decimal October { get; set; }
        public decimal November { get; set; }
        public decimal December { get; set; }




        public MdlTransectionInfo GetMdl_TransectionInfo(getTransectionInfo_Result mdl,string incmExpence)
        {
            return new MdlTransectionInfo
            {
                billId = mdl.BillId,
                invoiceNumber = mdl.InvoiceNumber,
                date = (DateTime)mdl.Date,
                propertyID = (int)mdl.PropertyID,
                from = (int)mdl.From,
                to = (int)mdl.To,
                amount = (int)mdl.Amount,
                paymentType = mdl.PaymentType,
                comment = mdl.Comment,
                categoryId = mdl.CategoryId,
                serviceCategory = mdl.ServiceCategory == null ? "zzz" : mdl.ServiceCategory,
                monthName = mdl.MonthName,
                getYear = mdl.GetYear,
                incmExpence = incmExpence,
                monthNum = mdl.MonthNum
            };
        }

    }
}