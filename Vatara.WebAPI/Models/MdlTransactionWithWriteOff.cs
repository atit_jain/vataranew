﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlTransactionWithWriteOff
    {
        public int billNo { get; set; }
        public int invoiceId { get; set; }
        public string number { get; set; }
        public string name { get; set; }
        public string payType { get; set; }
        public System.DateTime tranDate { get; set; }
        public decimal amount { get; set; }
        public string payMode { get; set; }
        public int categoryId { get; set; }
        public string category { get; set; }
        public Nullable<long> from { get; set; }
        public Nullable<long> to { get; set; }
        public string fromName { get; set; }
        public string pmSide { get; set; }
        public string ownerSide { get; set; }
        public string tenantSide { get; set; }
        public string toName { get; set; }
        public Nullable<int> ownerId { get; set; }
        public string ownerName { get; set; }
        public Nullable<long> propertyId { get; set; }
        public string property_Name { get; set; }
        public Nullable<decimal> wrtOffAmt { get; set; }
        public Nullable<System.DateTime> wrtOffDate { get; set; }
        public string wrtOffDesc { get; set; }


        public MdlTransactionWithWriteOff GetMdl_TransactionWithWriteOff(getTransactionWithWriteOff_Result view)
        {
            return new MdlTransactionWithWriteOff
            {
                billNo = view.BillNo,
                invoiceId = view.InvoiceId,
                number = view.Number,
                tranDate = view.TranDate,
                amount = view.Amount,
                payMode = view.PayMode,
                categoryId = view.CategoryId,
                category = view.Category,
                from = view.From,
                to = view.To,
                fromName = view.FROMNAME,
                toName = view.TONAME,
                ownerId = view.OwnerId,
                ownerName = view.OwnerName,
                propertyId = view.PropertyId,
                property_Name = view.Property_Name,
                wrtOffAmt = view.WrtOffAmt,
                wrtOffDate = view.WrtOffDate,
                wrtOffDesc = view.WrtOffDesc,
                pmSide = view.PMSide,
                ownerSide = view.OwnerSide,
                tenantSide = view.TenantSide
            };
        }
    }
}