﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlTransactionAndChartData
    {
        public MdlTransactionAndChartData()
        {
            lstIncomeExpenseLiabilityChartData = new List<MdlIncomeExpenseLiabilityChartData>();
            lstIncomeChartData = new List<MdlIncomeChartData>();
            lstTransactionWithWriteOff = new List<MdlTransactionWithWriteOff>();
            lstMdlManagerLiabilityToOwner = new List<MdlManagerLiabilityToOwner>();
            lstMdlManagerLiabilityToTenant = new List<MdlManagerLiabilityToTenant>();
            lstMdlTransactionByLedgerTable = new List<MdlTransactionByLedgerTable>();
        }
        public List<MdlTransactionWithWriteOff> lstTransactionWithWriteOff { get; set; }
        public List<MdlIncomeExpenseLiabilityChartData> lstIncomeExpenseLiabilityChartData { get; set; }
        public List<MdlIncomeChartData> lstIncomeChartData { get; set; }        
        public List<MdlManagerLiabilityToOwner> lstMdlManagerLiabilityToOwner { get; set; }
        public List<MdlManagerLiabilityToTenant> lstMdlManagerLiabilityToTenant { get; set; }
        public List<MdlTransactionByLedgerTable> lstMdlTransactionByLedgerTable { get; set; }        
    }
}