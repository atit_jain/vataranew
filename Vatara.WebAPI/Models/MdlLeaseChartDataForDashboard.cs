﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlLeaseChartDataForDashboard
    {
        public MdlLeaseChartDataForDashboard()
        {
            lstMdlExpiringLeases = new List<MdlExpiringLeases>();
        }
        public MdlLeaseChartData mdlLeaseChartData { get; set; }
        public List<MdlExpiringLeases> lstMdlExpiringLeases { get; set; }
    }
}