﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPaymentMode
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<long> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<long> lastModifierUserId { get; set; }
        public System.DateTime creationTime { get; set; }
        public Nullable<long> creatorUserId { get; set; }


        public MdlPaymentMode GetMdl_PaymentMode(vwPaymentMode mdl)
        {
            return new MdlPaymentMode()
            {
                id = mdl.Id,
                name = mdl.Name
            };
        }
    }
}