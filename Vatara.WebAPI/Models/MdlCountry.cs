﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlCountry
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public MdlCountry GetMdl_Country(vwCountry mdl)
        {
            return new MdlCountry()
            {
                id = mdl.Id,
                name = mdl.Name,
                description = mdl.Description
            };
        }

        public MdlCountry GetMdl_Country(Country mdl)
        {
            return new MdlCountry()
            {
                id = mdl.Id,
                name = mdl.Name,
                description = mdl.Description
            };
        }

    }
}