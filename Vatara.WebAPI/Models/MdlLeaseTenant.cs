﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlLeaseTenant
    {
        public int pLeaseId { get; set; }
        public Nullable<int> pSeqNo { get; set; }
        public Nullable<int> tenantid { get; set; }
        public string pTenantFirstName { get; set; }
        public string pTenantLastName { get; set; }
        public string tenantname { get; set; }
        public string isprimaryTenant { get; set; }
        public Nullable<int> isprimTenant { get; set; }
        public string tenantemail { get; set; }
        public string tenantphoneno { get; set; }
        public string pTenantFaxNo { get; set; }
        public string pTenantTwitterUrl { get; set; }
        public string pTenantFacebookUrl { get; set; }
        public int propertyId { get; set; }
        public string LeaseNo { get; set; }


        public MdlLeaseTenant GetMdl_LeaseTenant(vwLeaseTenant mdl)
        {
            return new MdlLeaseTenant()
            {
                pLeaseId = mdl.pLeaseId,
                pSeqNo = mdl.pSeqNo,
                tenantid = mdl.Tenantid,
                pTenantFirstName = mdl.pTenantFirstName,
                pTenantLastName = mdl.pTenantLastName,
                tenantname = mdl.Tenantname,
                isprimaryTenant = mdl.isprimaryTenant,
                isprimTenant = mdl.isprimTenant,
                tenantemail = mdl.Tenantemail,
                tenantphoneno = mdl.Tenantphoneno,
                pTenantFaxNo = mdl.pTenantFaxNo,
                pTenantTwitterUrl = mdl.pTenantTwitterUrl,
                pTenantFacebookUrl = mdl.pTenantFacebookUrl,
                propertyId = mdl.PropertyId,
                LeaseNo = mdl.LeaseNo
            };

        }
    }
}