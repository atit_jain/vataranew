﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlServiceCategory
    {
        public string name { get; set; }
        public string description { get; set; }
        public int id { get; set; }
        public string groupName { get; set; }
        public string pmSide { get; set; }
        public string ownerSide { get; set; }
        public string tenantSide { get; set; }
        public bool isDeleted { get; set; }
        public DateTime creationTime { get; set; }
        public Nullable<long> creatorUserId { get; set; }

        public ServiceCategory GetEntity_ServiceCategory(MdlServiceCategory mdl)
        {
            return new ServiceCategory()
            {
                Name = mdl.name,
                Description = mdl.description,
                IsDeleted = mdl.isDeleted,
                CreationTime = mdl.creationTime.ToUniversalTime(),
                CreatorUserId = mdl.creatorUserId
            };
        }

        public MdlServiceCategory GetMdl_ServiceCategory(getServiceCategory_Result entity)
        {
            return new MdlServiceCategory()
            {
                id = entity.Id,
                name = entity.Name,
                description = entity.Description,
                groupName= entity.GroupName,
            };
        }


    }
}