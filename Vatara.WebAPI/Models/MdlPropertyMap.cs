﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlPropertyMap
    {
        public int pId { get; set; }
        public string pNum { get; set; }
        public int pCategory { get; set; }
        public int pType { get; set; }
        public string pName { get; set; }
        public string pPhone { get; set; }
        public decimal pGrossArea { get; set; }
        public decimal pRentableArea { get; set; }
        public string pParkingSpaces { get; set; }
        public string pUnitNo { get; set; }
        public int pHOAId { get; set; }
        public int pBaseRent { get; set; }
        public int pPropertyTax { get; set; }

        public int pHoaDate { get; set; }
        public string HOAPaymentFrequency { get; set; }

        public decimal pHoaAmount { get; set; }
        public decimal pManagementFee { get; set; }
        public string pManagementFeeType { get; set; }
        public string pDescription { get; set; }
        public string pAirportName { get; set; }
        public string pAirportDistance { get; set; }
        public string pUOM { get; set; }
        public string pListType { get; set; }
        public string pComments { get; set; }
        public string pZoning { get; set; }
        public string pLandAcres { get; set; }
        public string pCustomField1 { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public bool pIsHomeWarrantyTaken { get; set; }
        public string pHomeWarrantyCompanyName { get; set; }
        public string pHomeWarrantyCompanyPhoneNo { get; set; }
        public string pHomeWarrantyStartDate { get; set; }
        public string pHomeWarrantyEndDate { get; set; }
        public bool pHomeWarrantySendReminder { get; set; }
        public bool pIsHomeInsuranceTaken { get; set; }
        public string pHomeInsuranceCompanyName { get; set; }
        public string pHomeInsuranceCompanyPhoneNo { get; set; }
        public string pHomeInsuranceStartDate { get; set; }
        public string pHomeInsuranceEndDate { get; set; }
        public bool pHomeInsuranceSendReminder { get; set; }
        public int pManagerId { get; set; }
        public string pAddress { get; set; }
        public int pAddressId { get; set; }
        public int pCityId { get; set; }
        public string pCity { get; set; }
        public int pCountyId { get; set; }
        public string pCounty { get; set; }
        public int pStateId { get; set; }
        public string pState { get; set; }
        public int pCountryId { get; set; }
        public string pCountry { get; set; }
        public string pMailBoxNo { get; set; }
        public string pZipCode { get; set; }
    }
}