﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlSupportTicket
    {
        public int TicketId { get; set; }
        public string TicketNo { get; set; }
        public int CompanyMgrId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public string Response { get; set; }
        public int Status { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string FromEmail { get; set; }
        
    }
}