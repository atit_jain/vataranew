﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlNotification
    {
        public int id { get; set; }
        public int pmid { get; set; }
        public Nullable<int> personId { get; set; }
        public Nullable<int> personTypeId { get; set; }
        public string module { get; set; }
        public string message { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<System.DateTime> creationTime { get; set; }

        public MdlNotification GetMdl_Notification(getNotificationData_Result mdl) {
            return new MdlNotification()
            {
                id = mdl.Id,
                pmid = mdl.PMID,
                personId = mdl.PersonId,
                personTypeId = mdl.PersonTypeId,
                module = mdl.Module,
                message = mdl.Message,
                isRead = mdl.IsRead,
                creationTime = mdl.CreationTime
            };
        }
    }
}