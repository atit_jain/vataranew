﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlSubscription
    {
        public int id { get; set; }
        public Nullable<int> managerId { get; set; }
        public Nullable<System.DateTime> subsStartDate { get; set; }
        public Nullable<System.DateTime> subsEndDate { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<int> lastSubscriptionId { get; set; }
        public Nullable<System.DateTime> creationDate { get; set; }
        public Nullable<int> creatorUserId { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<int> lastModifierUserId { get; set; }


        public MdlSubscription GetMdl_Subscription(Subscription entity)
        {
            return new MdlSubscription()
            {
                id = entity.Id,
                managerId = entity.ManagerId,
                subsStartDate = entity.SubsStartDate==null? (DateTime?)null : entity.SubsStartDate.Value.ToUniversalTime(),
                subsEndDate = entity.SubsEndDate == null ? (DateTime?)null : entity.SubsEndDate.Value.ToUniversalTime(),
                amount = entity.Amount,
                isActive = entity.IsActive,
                lastSubscriptionId = entity.LastSubscriptionId,
                creationDate = entity.CreationDate == null ? (DateTime?)null : entity.CreationDate.Value.ToUniversalTime()
            };
        }

    }
}