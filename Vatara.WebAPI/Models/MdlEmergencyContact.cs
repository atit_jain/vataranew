﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlEmergencyContact
    {
        public long id { get; set; }
        public int personId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullname { get; set; }
        public string phoneNo { get; set; }
        public string email { get; set; }
        public string detail { get; set; }

        public MdlEmergencyContact getMdl_EmergencyContact(vwEmergencyContact mdl)
        {
            return new MdlEmergencyContact()
            {
                id = mdl.Id,
                personId = mdl.PersonId,
                firstName = mdl.FirstName,
                lastName = mdl.LastName,
                fullname = mdl.Fullname,
                phoneNo = mdl.PhoneNo,
                email = mdl.Email,
                detail = mdl.Detail
            };
               
        }
    }
}