﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlSubscriptionDetail
    {
        public int subscriptionId { get; set; }
        public string subscriptionNumber { get; set; }
        public Nullable<int> managerId { get; set; }
        public Nullable<System.DateTime> subsStartDate { get; set; }
        public Nullable<System.DateTime> subsEndDate { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<int> expiredIn { get; set; }
        public string paymentMethod { get; set; }


        public MdlSubscriptionDetail GetMdl_SubscriptionDetail(getSubscriptionDetail_Result entity)
        {
            if (entity != null)
            {
                return new MdlSubscriptionDetail()
                {
                    subscriptionId = entity.SubscriptionId,
                    subscriptionNumber = entity.SubscriptionNumber,
                    managerId = entity.ManagerId,
                    subsStartDate = entity.SubsStartDate == null ? (DateTime?)null : entity.SubsStartDate.Value.ToUniversalTime(),
                    subsEndDate = entity.SubsEndDate == null ? (DateTime?)null : entity.SubsEndDate.Value.ToUniversalTime(),
                    amount = entity.Amount,
                    isActive = entity.IsActive,
                    expiredIn = entity.ExpiredIn,
                };
            }
            else
            {
                return new MdlSubscriptionDetail();
            }
        }
    }

    public class MdlGetSubscriptionDetail
    {
        public int Id { get; set; }
        public int PMID { get; set; }
        public string CustomerId { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string CurrencyCode { get; set; }
        public string SubScriptionId { get; set; }
        public string Plan_Code { get; set; }
        public string ProductId { get; set; }
        public string Interval { get; set; }
        public string IntervalUnit { get; set; }
        public Nullable<int> ExchangeRate { get; set; }
        public bool AutoCollect { get; set; }

        public MdlGetSubscriptionDetail GetMdl_SubscriptionDetail(sp_GetSubScrpitiondetail_Result entity)
        {
            return new MdlGetSubscriptionDetail()
            {
                Id = entity.Id,
                PMID = entity.PMID,
                CustomerId = entity.CustomerId,
                DisplayName = entity.DisplayName,
                Email = entity.Email,
                CurrencyCode = entity.CurrencyCode,
                SubScriptionId = entity.SubScriptionId,
                Plan_Code = entity.Plan_Code,
                ProductId = entity.ProductId,
                Interval = entity.Interval,
                IntervalUnit = entity.IntervalUnit,
                ExchangeRate = entity.ExchangeRate,
                AutoCollect = entity.AutoCollect
            };
        }
    }
}