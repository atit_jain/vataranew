﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPastDueRentInvoices
    {
        public int InvoiceId { get; set; }
        public int PropertyId { get; set; }
        public int TenantId { get; set; }
        public string PropertyName { get; set; }
        public string TenantName { get; set; }
        public string PropertyAddress { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public int LeaseId { get; set; }
        public string LeaseNumber { get; set; }

        public MdlPastDueRentInvoices GetMdl_PastDueRentInvoices(getPastDueRentInvoicesByPMID_Result entity)
        {
            return new MdlPastDueRentInvoices() {
                InvoiceId = entity.InvoiceId,
                PropertyId = entity.PropertyId,
                TenantId = entity.TenantId,
                PropertyName = entity.PropertyName,
                TenantName = entity.TenantName,
                PropertyAddress = entity.PropertyAddress,
                DueDate = entity.DueDate,
                LeaseId = entity.LeaseId,
                LeaseNumber = entity.LeaseNumber
            };
        }
    }
}