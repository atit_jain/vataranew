﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlHostedTransactionInfo
    {
        public HostedTransactionInfo GetEntity_HostedTransactionInfo(HostedTransactionModel mdl)
        {
            if (mdl.HostedTransaction.PaymentMethodInfo.BillingInformation != null)
            {
                return GetHostedTransactionInfoForNonACH(mdl);
            }
            else {
                return GetHostedTransactionInfoForACH(mdl);
            }
        }

        private HostedTransactionInfo GetHostedTransactionInfoForACH(HostedTransactionModel mdl)
        {
            return new HostedTransactionInfo()
            {
                HostTran_CreationDate = mdl.HostedTransaction.CreationDate.ToUniversalTime(),
                HostedTransactionIdentifier = mdl.HostedTransaction.HostedTransactionIdentifier,
                PayerId = mdl.HostedTransaction.PayerId.ToString(),
                TransactionResultMessage = mdl.HostedTransaction.TransactionResultMessage,
                AuthCode = mdl.HostedTransaction.AuthCode,               
                TransactionResult = mdl.HostedTransaction.TransactionResult,
                AvsResponse = mdl.HostedTransaction.AvsResponse,
                PaymentMethodID = mdl.HostedTransaction.PaymentMethodInfo.PaymentMethodID,
                PaymentMethodType = mdl.HostedTransaction.PaymentMethodInfo.PaymentMethodType,
                ObfuscatedAccountNumber = mdl.HostedTransaction.PaymentMethodInfo.ObfuscatedAccountNumber,
                ExpirationDate = mdl.HostedTransaction.PaymentMethodInfo.ExpirationDate,
                AccountName = mdl.HostedTransaction.PaymentMethodInfo.AccountName,               
                Payment_Description = mdl.HostedTransaction.PaymentMethodInfo.Description,
                Priority = mdl.HostedTransaction.PaymentMethodInfo.Priority.ToString(),
                Payment_DateCreated = mdl.HostedTransaction.PaymentMethodInfo.DateCreated.ToUniversalTime(),
                Payment_Protected = mdl.HostedTransaction.PaymentMethodInfo.Protected == null ? false : mdl.HostedTransaction.PaymentMethodInfo.Protected,
                GrossAmt = mdl.HostedTransaction.GrossAmt,
                NetAmt = mdl.HostedTransaction.NetAmt,
                PerTransFee = mdl.HostedTransaction.PerTransFee,
                Rate = mdl.HostedTransaction.Rate,
                GrossAmtLessNetAmt = mdl.HostedTransaction.GrossAmtLessNetAmt,
                CVVResponseCode = mdl.HostedTransaction.CVVResponseCode,
                CurrencyConversionRate = mdl.HostedTransaction.CurrencyConversionRate,
                CurrencyConvertedAmount = mdl.HostedTransaction.CurrencyConvertedAmount,
                CurrencyConvertedCurrencyCode = mdl.HostedTransaction.CurrencyConvertedCurrencyCode,
                ResultValue = mdl.Result.ResultValue,
                ResultCode = mdl.Result.ResultCode,
                ResultMessage = mdl.Result.ResultMessage
            };
        }

        private HostedTransactionInfo GetHostedTransactionInfoForNonACH(HostedTransactionModel mdl)
        {
            return new HostedTransactionInfo()
            {
                HostTran_CreationDate = mdl.HostedTransaction.CreationDate.ToUniversalTime(),
                HostedTransactionIdentifier = mdl.HostedTransaction.HostedTransactionIdentifier,
                PayerId = mdl.HostedTransaction.PayerId.ToString(),
                TransactionResultMessage = mdl.HostedTransaction.TransactionResultMessage,
                AuthCode = mdl.HostedTransaction.AuthCode,                
                TransactionResult = mdl.HostedTransaction.TransactionResult,
                AvsResponse = mdl.HostedTransaction.AvsResponse,
                PaymentMethodID = mdl.HostedTransaction.PaymentMethodInfo.PaymentMethodID,
                PaymentMethodType = mdl.HostedTransaction.PaymentMethodInfo.PaymentMethodType,
                ObfuscatedAccountNumber = mdl.HostedTransaction.PaymentMethodInfo.ObfuscatedAccountNumber,
                ExpirationDate = mdl.HostedTransaction.PaymentMethodInfo.ExpirationDate,
                AccountName = mdl.HostedTransaction.PaymentMethodInfo.AccountName,
                BillInfo_Address1 = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.Address1,
                BillInfo_Address2 = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.Address2,
                BillInfo_Address3 = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.Address3,
                BillInfo_City = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.City,
                BillInfo_State = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.State,
                BillInfo_ZipCode = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.ZipCode,
                BillInfo_Country = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.Country,
                BillInfo_TelephoneNumber = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.TelephoneNumber,
                BillInfo_Email = mdl.HostedTransaction.PaymentMethodInfo.BillingInformation.Email,
                Payment_Description = mdl.HostedTransaction.PaymentMethodInfo.Description,
                Priority = mdl.HostedTransaction.PaymentMethodInfo.Priority.ToString(),
                Payment_DateCreated = mdl.HostedTransaction.PaymentMethodInfo.DateCreated.ToUniversalTime(),
                Payment_Protected = mdl.HostedTransaction.PaymentMethodInfo.Protected == null ? false : mdl.HostedTransaction.PaymentMethodInfo.Protected,
                GrossAmt = mdl.HostedTransaction.GrossAmt,
                NetAmt = mdl.HostedTransaction.NetAmt,
                PerTransFee = mdl.HostedTransaction.PerTransFee,
                Rate = mdl.HostedTransaction.Rate,
                GrossAmtLessNetAmt = mdl.HostedTransaction.GrossAmtLessNetAmt,
                CVVResponseCode = mdl.HostedTransaction.CVVResponseCode,
                CurrencyConversionRate = mdl.HostedTransaction.CurrencyConversionRate,
                CurrencyConvertedAmount = mdl.HostedTransaction.CurrencyConvertedAmount,
                CurrencyConvertedCurrencyCode = mdl.HostedTransaction.CurrencyConvertedCurrencyCode,
                ResultValue = mdl.Result.ResultValue,
                ResultCode = mdl.Result.ResultCode,
                ResultMessage = mdl.Result.ResultMessage
            };
        }
    }
}