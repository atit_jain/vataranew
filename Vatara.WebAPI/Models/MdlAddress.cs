﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlAddress
    {

        public int id { get; set; }
        public int addressTypeId { get; set; }
        public string mailBoxNo { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public int countryId { get; set; }
        public string country { get; set; }
        public int stateId { get; set; }
        public string state { get; set; }
        public int cityId { get; set; }
        public string city { get; set; }
        public Nullable<int> countyId { get; set; }
        public string county { get; set; }
        public string zipcode { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<long> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<long> lastModifierUserId { get; set; }
        public System.DateTime creationTime { get; set; }
        public Nullable<long> creatorUserId { get; set; }


        public MdlAddress GetMdl_Address(vwAddress mdl)
        {
            return new MdlAddress()
            {
                id = mdl.AdrsID,
                addressTypeId = mdl.AddressTypeId,
                mailBoxNo = mdl.MailBoxNo,
                line1 = mdl.Line1,
                line2 = mdl.Line2,
                countryId = mdl.CountryId,
                country = mdl.Country,               
                stateId = mdl.StateId,
                state = mdl.State,
                cityId = mdl.CityId,
                city = mdl.City,                
                countyId = mdl.CountyId,  
                county = mdl.County,            
                zipcode = mdl.Zipcode                            
            };
        }


        public Address GetEntity_Address(MdlAddress mdl)
        {
            return new Address()
            {
                Id = mdl.id,
                AddressTypeId = mdl.addressTypeId,
                MailBoxNo = mdl.mailBoxNo,
                Line1 = mdl.line1,
                Line2 = mdl.line2,
                CountryId = mdl.countryId,
                StateId = mdl.stateId,
                CityId = mdl.cityId,
                CountyId = mdl.countyId,
                Zipcode = mdl.zipcode,
                IsDeleted = mdl.isDeleted,
                DeleterUserId = mdl.deleterUserId,
                DeletionTime = mdl.deletionTime == null ? (DateTime?)null :  mdl.deletionTime.Value.ToUniversalTime(),
                LastModificationTime = mdl.lastModificationTime == null ? (DateTime?)null : mdl.lastModificationTime.Value.ToUniversalTime(),
                LastModifierUserId = mdl.lastModifierUserId,
                CreationTime = mdl.creationTime.ToUniversalTime(),
                CreatorUserId = mdl.creatorUserId
            };
        }
    }
}