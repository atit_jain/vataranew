﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class LeaseRenewMdl
    {
        public int leaseId { get; set; }
        
        public DateTime leaseStartDate { get; set; }

        public DateTime leaseEndDate { get; set; }

        public decimal leaseAmount { get; set; }

        public int createdId { get; set; }
    }
}