﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlVendorServiceCategoriesMaster
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<int> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<int> lastModifierUserId { get; set; }
        public System.DateTime creationTime { get; set; }
        public Nullable<int> creatorUserId { get; set; }

        public MdlVendorServiceCategoriesMaster GetMdl_VendorServiceCategoriesMaster(VendorServiceCategoriesMaster entity)
        {
            return new MdlVendorServiceCategoriesMaster()
            {
                id = entity.Id,
                name = entity.Name,
                description = entity.Description
            };
        }
    }
}