﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlWorkorder
    {
        public int id { get; set; }
        public int propertyId { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> requestedDate { get; set; }
        public Nullable<int> requestedBy { get; set; }
        public Nullable<System.DateTime> expectedStartDate { get; set; }
        public Nullable<System.DateTime> expectedCompletionDate { get; set; }
        public Nullable<System.DateTime> actualStartDate { get; set; }
        public Nullable<System.DateTime> actualCompletionDate { get; set; }
        public Nullable<int> status { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<long> creatorUserId { get; set; }
        public Nullable<int> requestedByPersonType { get; set; }

        public Workorder GetEntityWorkorder(MdlWorkorder mdl)
        {
            return new Workorder() {
               PropertyId = mdl.propertyId,
               Description = mdl.description,
               RequestedDate = mdl.requestedDate,
               RequestedBy = mdl.requestedBy,
               ExpectedStartDate = mdl.expectedStartDate,
               ExpectedCompletionDate = mdl.expectedCompletionDate,
               ActualStartDate = mdl.actualStartDate,
               ActualCompletionDate = mdl.actualCompletionDate,
               Status = mdl.status,
               IsDeleted = mdl.isDeleted,
               CreatorUserId = mdl.creatorUserId,
               RequestedByPersonType = mdl.requestedByPersonType
            };
        }


        public MdlWorkorder GetMdl_Workorder(Workorder entity)
        {
            return new MdlWorkorder()
            {
                id = entity.Id,
                propertyId = entity.PropertyId,
                description = entity.Description,
                requestedDate = entity.RequestedDate.Value,
                requestedBy = entity.RequestedBy,
                expectedStartDate = entity.ExpectedStartDate,
                expectedCompletionDate = entity.ExpectedCompletionDate,
                actualStartDate = entity.ActualStartDate,
                actualCompletionDate = entity.ActualCompletionDate,
                status = entity.Status,
                isDeleted = entity.IsDeleted,
                creatorUserId = entity.CreatorUserId,
                requestedByPersonType = entity.RequestedByPersonType
            };
        }

    }
}