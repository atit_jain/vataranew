﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlSubscriptionType
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<decimal> amount { get; set; }

        public MdlSubscriptionType GetMdl_SubscriptionType(SubscriptionType entity)
        {
            return new MdlSubscriptionType()
            {
                id = entity.Id,
                name = entity.Name,
                amount = entity.Amount
            };
        }
    }
}