﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlOwnerMaster
    {
        public int POID { get; set; }
        public string OwnerType { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNum { get; set; }
        public Nullable<int> AddressId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<int> CreatorUserId { get; set; }
    }
    public class MdlOwnerDataWithType
    {
        public int Id { get; set; }
        public int POID { get; set; }
        public string OwnerType { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNum { get; set; }
        public Nullable<int> AddressId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<int> CreatorUserId { get; set; }
    }

    public class MdlOwnerData {

        public MdlOwnerDataWithType ownerData { get; set; }
        public MdlAddress mdlAddress { get; set; }
        public int personId { get; set; }
    }


}