﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlVendorServiceCategory
    {
        public int id { get; set; }
        public int vendorId { get; set; }
        public Nullable<int> serviceCategoryId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<int> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<int> lastModifierUserId { get; set; }
        public System.DateTime creationTime { get; set; }
        public Nullable<int> creatorUserId { get; set; }

        public MdlVendorServiceCategory GetMdl_VendorServiceCategory(getVendorServiceCategories_Result entity)
        {
            return new MdlVendorServiceCategory()
            {
                id = entity.Id,
                vendorId = entity.VendorId,
                name = entity.Name,
                description = entity.Description               
            };
        }


        public VendorServiceCategory GetEntity_VendorServiceCategory(MdlVendorServiceCategory mdl)
        {
            return new VendorServiceCategory()
            {
                Id = mdl.id,
                VendorId = mdl.vendorId,
                ServiceCategoryId = mdl.serviceCategoryId              
            };
        }
    }
}