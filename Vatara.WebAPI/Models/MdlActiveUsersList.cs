﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlActiveUsersList
    {
        public static List<ActiveUsers> activeUsers = new List<ActiveUsers>();

        public static List<ActiveUsers> addActiveUser(string name, string ConnectionId,int userID)
        {
            if (MdlActiveUsersList.activeUsers == null)
            {
                MdlActiveUsersList.activeUsers = new List<ActiveUsers>();
            }

            ActiveUsers mdl = new ActiveUsers();
            mdl.Name = name;
            mdl.LastActivity = DateTime.UtcNow;
            mdl.ConnectionId = ConnectionId;
            mdl.userID = userID;
            if (MdlActiveUsersList.activeUsers.Count(a => a.Name == mdl.Name && a.ConnectionId == ConnectionId) > 0)
            {
                MdlActiveUsersList.activeUsers.Remove(MdlActiveUsersList.activeUsers.First(a => a.Name == mdl.Name && a.ConnectionId == ConnectionId));
            }

            MdlActiveUsersList.activeUsers.Add(mdl);
            return MdlActiveUsersList.activeUsers;
        }

        public static List<ActiveUsers> removeActiveUser(string name, string ConnectionId)
        {
            if (MdlActiveUsersList.activeUsers == null)
            {
                MdlActiveUsersList.activeUsers = new List<ActiveUsers>();
            }

            if (MdlActiveUsersList.activeUsers.Count(a => a.Name == name && a.ConnectionId == ConnectionId) > 0)
            {
                MdlActiveUsersList.activeUsers.Remove(MdlActiveUsersList.activeUsers.First(a => a.Name == name && a.ConnectionId == ConnectionId));
            }
            return MdlActiveUsersList.activeUsers;

        }
        public static List<ActiveUsers> getActiveUsersList()
        {
            return MdlActiveUsersList.activeUsers;
        }
    }
    public class ActiveUsers
    {
        public string Name { get; set; }
        public DateTime LastActivity { get; set; }
        public string ConnectionId { get; set; }
        public int userID { get; set; }
    }
}