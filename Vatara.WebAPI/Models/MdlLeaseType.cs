﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlLeaseType
    {
        public int id { get; set; }
        public Nullable<int> propertyCategoryId { get; set; }
        public string leaseType { get; set; }
        public string description { get; set; }



        public MdlLeaseType GetMdl_LeaseType(LeaseType mdl)
        {
            return new MdlLeaseType()
            {
                id = mdl.Id,
                leaseType = mdl.LeaseType1,
                description = mdl.Description,
                propertyCategoryId = mdl.PropertyCategoryId
            };
        }

        public LeaseType GetMdl_LeaseType(MdlLeaseType mdl)
        {
            return new LeaseType()
            {
                Id = mdl.id,
                LeaseType1 = mdl.leaseType,
                Description = mdl.description,
                PropertyCategoryId = mdl.propertyCategoryId
            };
        }
    }
}