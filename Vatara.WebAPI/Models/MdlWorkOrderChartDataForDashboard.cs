﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlWorkOrderChartDataForDashboard
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        public string RequestedByName { get; set; }
        public System.DateTime Date { get; set; }
        public string PropertyName { get; set; }
        public string Status { get; set; }


        public MdlWorkOrderChartDataForDashboard Get_WorkOrderChartDataForDashboard(getWorkOrderChartDataForDashboard_Result entity)
        {
            return new MdlWorkOrderChartDataForDashboard()
            {
                Id = entity.Id,
                PropertyId = entity.PropertyId,
                RequestedByName = entity.RequestedByName,
                Date = entity.Date,
                PropertyName = entity.PropertyName,
                Status = entity.Status
            };
        }
    }
}