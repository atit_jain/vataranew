﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlTransactionByLedgerTable
    {
        public string transactionType { get; set; }
        public Nullable<int> billInvoice { get; set; }
        public System.DateTime transactionDate { get; set; }
        public string ownerTenant { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public decimal amount { get; set; }
        public string drcr { get; set; }
        public string comment { get; set; }

        public MdlTransactionByLedgerTable GetMdl_TransactionByLedgerTable(getTransactionByLedgerTable_Result entity)
        {
            return new MdlTransactionByLedgerTable()
            {
                transactionType = entity.TransactionType,
                billInvoice = entity.BillInvoice,
                transactionDate = entity.TransactionDate,
                from = entity.From,
                to = entity.To,
                amount = entity.amount,
                drcr = entity.DrCr,
                comment = entity.Comment
            };
        }

        public MdlTransactionByLedgerTable GetMdl_TransactionByLedgerTable(getTransactionByLedgerTableForOwnerTenant_Result entity)
        {
            return new MdlTransactionByLedgerTable()
            {
                transactionType = entity.TransactionType,
                billInvoice = entity.BillInvoice,
                transactionDate = entity.TransactionDate,
                ownerTenant = entity.OwnerTenant,
                amount = entity.amount,
                drcr = entity.DrCr,
                comment = entity.Comment
            };
        }

    }
}