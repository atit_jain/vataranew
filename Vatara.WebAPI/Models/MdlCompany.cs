﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlCompany
    {
        public int POID { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNum { get; set; }
        public string OwnerType { get; set; }
        public Nullable<int> PMID { get; set; }

        public MdlCompany GetMdl_Company(GetCompanyByManagerID_Result entity)
        {
            return new MdlCompany()
            {
                POID = entity.POID,
                CompanyName=entity.CompanyName,
                Email = entity.Email,
                ContactPerson = entity.ContactPerson,
                ContactNum = entity.ContactNum,
                OwnerType = entity.OwnerType,
                PMID=entity.PMID
            };
       }
    }
}