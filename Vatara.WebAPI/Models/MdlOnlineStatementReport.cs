﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlOnlineStatementReport
    {
        /// <summary>
        /// Important :  Dont add any new property in-between these
        ///              If you want to add any new property, 
        ///              add that after last property        ///              
        ///              (i.e. public decimal December { get; set; }  )
        /// </summary>      

        //public System.DateTime date { get; set; }
        //public decimal amount { get; set; }
        //public string serviceCategory { get; set; }
        //public string trantype{ get; set; }
        //public string incmExpense { get; set; }


        public string transactionType { get; set; }
        public string refNo { get; set; }
        public int billInvoiceId { get; set; }
        public Nullable<System.DateTime> transactionDate { get; set; }
        public string ownerTenant { get; set; }
        public decimal cr { get; set; }
        public decimal dr { get; set; }      
        public string comment { get; set; }        
        public Nullable<decimal> runningBalance { get; set; }

        //public Nullable<decimal> previousBalance { get; set; }
        //public Nullable<decimal> currentCharges { get; set; }
        //public Nullable<decimal> balanceDue { get; set; }
    }
}