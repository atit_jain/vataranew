using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPorpayProfile
    {
        public int id { get; set; }
        public int personId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime dob { get; set; }
        public string email { get; set; }
        public int addressId { get; set; }
        public string phone { get; set; }
        public long ssn { get; set; }
        public string marchantAccntNum { get; set; }
        public string marchantProfileId { get; set; }
        public string marchantPassword { get; set; }
        public string payerId { get; set; }


        public string bankName { get; set; }
        public string accountName { get; set; }
        public string accountNumber { get; set; }
        public string accountCountryCode { get; set; }
        public string accountOwnershipType { get; set; }
        public string accountType { get; set; }
        public string routingNumber { get; set; }


        public string address { get; set; }
        public string address2 { get; set; }
        public Nullable<int> countryId { get; set; }
        public string countryName { get; set; }
        public Nullable<int> stateId { get; set; }
        public string stateName { get; set; }
        public Nullable<int> cityId { get; set; }
        public string cityName { get; set; }
        public Nullable<int> countyId { get; set; }
        public string countyName { get; set; }
        public string zipCode { get; set; }
        public string mailBoxNo { get; set; }
        public Nullable<int> LastPaymentType { get; set; }

        public PorpayProfile GetEntity_PorpayProfile(MdlPorpayProfile mdl)
        {
            return new PorpayProfile()
            {
                PersonId = mdl.personId,
                FirstName = mdl.firstName,
                LastName = mdl.lastName,
                Dob = mdl.dob.ToUniversalTime(),
                Email = mdl.email,
                AddressId = mdl.addressId,
                Phone = mdl.phone,
               // ssn = mdl.ssn,
                MarchantAccntNum = mdl.marchantAccntNum,
                MarchantProfileId = mdl.marchantProfileId,
                MarchantPassword = mdl.marchantPassword,
                PayerId = mdl.payerId,

                //BankName = mdl.bankName,
                //AccountName = mdl.accountName,
                //AccountCountryCode = mdl.accountCountryCode,
               // AccountNumber = mdl.accountName,
               // AccountOwnershipType = mdl.accountOwnershipType,
               // AccountType = mdl.accountType,
               // RoutingNumber = mdl.routingNumber                

            };
        }

        public MdlPorpayProfile GetMdl_PorpayProfile(PorpayProfile entity)
        {
            MdlAddress mdladdress = new MdlAddress();
            using (VataraEntities ve = new VataraEntities())
            {
                mdladdress = new MdlAddress().GetMdl_Address(ve.vwAddresses.FirstOrDefault(e => e.AdrsID == entity.AddressId));
            }

            return new MdlPorpayProfile()
            {
                personId = entity.PersonId,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                dob = entity.Dob.ToUniversalTime(),
                email = entity.Email,
                addressId = entity.AddressId,
                phone = entity.Phone,
               // ssn = entity.ssn,
                marchantAccntNum = entity.MarchantAccntNum,
                marchantProfileId = entity.MarchantProfileId,
                marchantPassword = entity.MarchantPassword,
                payerId = entity.PayerId,

                //bankName = entity.BankName,
                //accountName = entity.AccountName,
                //accountNumber = entity.AccountNumber,
                //accountCountryCode = entity.AccountCountryCode,
                //accountOwnershipType = entity.AccountOwnershipType,
                //accountType = entity.AccountType,
               // routingNumber = entity.RoutingNumber,
                LastPaymentType = entity.LastPaymentType,

                countryId = mdladdress.countryId,
                countryName = mdladdress.country,
                stateId = mdladdress.stateId,
                stateName = mdladdress.state,
                cityId = mdladdress.cityId,
                cityName = mdladdress.city,
                countyId = mdladdress.countyId,
                countyName = mdladdress.county,
                address = mdladdress.line1,
                address2 = mdladdress.line2,
                zipCode = mdladdress.zipcode,
                mailBoxNo = mdladdress.mailBoxNo
                
            };
        }


        public MdlPorpayProfile GetMdl_PorpayProfileFromPerson(vwPerson entity)
        {
            return new MdlPorpayProfile()
            {
                personId = entity.pId,
                firstName = entity.pFirstName,
                lastName = entity.pLastName,
                dob = DateTime.Now,
                email = entity.pEmail,
                address = entity.pAddress,
                phone = entity.pPhoneNo,
                ssn = 0,
                marchantAccntNum = "",
                marchantProfileId = "",
                marchantPassword = "",
                payerId = "",
                countryId = entity.pCountryId,
                stateId = entity.pStateId,
                cityId = entity.pCityId,
                countyId = entity.pCountyId,
                zipCode = entity.pZipcode,
                mailBoxNo = entity.pMailBoxNo
            };
        }

    }
}