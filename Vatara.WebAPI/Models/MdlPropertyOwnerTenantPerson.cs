﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPropertyOwnerTenantPerson
    {
        public int personId { get; set; }
        public string personName { get; set; }
        public string email { get; set; }
        public string personType { get; set; }
        public Nullable<int> personTypeId { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }


        public MdlPropertyOwnerTenantPerson GetMdl_PropertyOwnerTenantPerson(getPropertyTenantOwnerPersonByManagerPersonIdAndPropId_Result mdl)
        {
            return new MdlPropertyOwnerTenantPerson()
            {
                personId = mdl.PersonId,
                personName = mdl.PersonName,
                email = mdl.Email,
                personType = mdl.PersonType,
                personTypeId = mdl.PersonTypeId,
                statusId = mdl.StatusId,
                status = mdl.STATUS
            };
        }

    }
}