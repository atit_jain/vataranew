﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlPropertyTenantOwner
    {
        public int personId { get; set; }
        public string PersonName { get; set; }
        public int PropertyId { get; set; }
        public string PersonType { get; set; }
        public int PersonTypeId { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string Email { get; set; }
    }
}