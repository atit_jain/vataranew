﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlPropertyByPMID
    {
        public string propertyName { get; set; }
        public Nullable<int> pmid { get; set; }
        public int propertyId { get; set; }
        public string propAddress { get; set; }



        public MdlPropertyByPMID GetMdl_PropertyByPMID(getPropertyByPMID_Result mdl)
        {
            return new MdlPropertyByPMID()
            {
                propertyName = mdl.PropertyName,
                pmid = mdl.PMID,
                propertyId = mdl.PropertyId,
                propAddress = mdl.PropAddress
            };
        }

    }

    public class MdlPersonStatus
    {
        public int PersonId { get; set; }
        public int ModifierPersonId { get; set; }
        public string Action { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
      
    }
}