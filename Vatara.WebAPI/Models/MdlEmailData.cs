﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlEmailData
    {
        public int id { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string subject { get; set; }
        public string purpose { get; set; }
        public Nullable<bool> isSent { get; set; }
        public Nullable<bool> resend { get; set; }


        public MdlEmailData Get_MdlEmailData(EmailData entity)
        {
            return new MdlEmailData()
            {
                id = entity.Id,
                from = entity.From,
                to = entity.To,
                subject = entity.Subject,
                purpose = entity.Purpose,
                isSent = entity.IsSent,
                resend = entity.ReSend
            };
        }


        public vwEmailData Get_vwEmailData(MdlEmailData mdl)
        {
            return new vwEmailData()
            {
                Id = mdl.id,
                From = mdl.from,
                To = mdl.to,
                Subject = mdl.subject,
                Purpose = mdl.purpose,
                IsSent = mdl.isSent,
                Resend = mdl.resend,              
            };
        }


    }
}