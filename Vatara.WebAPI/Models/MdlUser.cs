﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
namespace Vatara.WebAPI
{
    public class MdlUser
    {
        public MdlUser()
        {
            lstPerson = new List<MdlPersonRegistraion>();
            ManagerDetail = new MdlPerson();
            mdlSubscription = new MdlSubscriptionExipryDate();
        }

        public string userName { get; set; }
        public string password { get; set; }
        public int personId { get; set; }
        public string personType { get; set; }
        public int personTypeId { get; set; }
        public MdlPerson ManagerDetail { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string comProfile { get; set; }
        public int pmid { get; set; }
        public int personManageriD { get; set; }
        public int poid { get; set; }
        public string role { get; set; }
        public string access { get; set; }
        public bool isSubscriptionExpired { get; set; }
        public List<MdlPersonRegistraion> lstPerson { get; set; }
        public MdlSubscriptionExipryDate mdlSubscription { get; set; }        
    }
}