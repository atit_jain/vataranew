﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class PersonWithType
    {
        public int id { get; set; }
        public string person_Name { get; set; }
        public int personTypeId { get; set; }
        public string type { get; set; }



        public PersonWithType GetMdl_PropTenantOwnerDetail1(vwPersonWithType mdl)
        {
            return new PersonWithType
            {
               id = mdl.Id,
               person_Name = mdl.Person_Name,
               personTypeId = mdl.PersonTypeId,
               type = mdl.Type
            };
        }
    }
}