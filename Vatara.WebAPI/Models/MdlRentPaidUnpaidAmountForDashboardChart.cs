﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlRentPaidUnpaidAmountForDashboardChart
    {
        public Nullable<int> TotalInvoicedRentAmount { get; set; }
        public Nullable<int> TotalPaidRentAmount { get; set; }

        public MdlRentPaidUnpaidAmountForDashboardChart GetMdl_RentPaidUnpaidAmountForDashboardChart(getRentPaidUnpaidAmountForDashboardChart_Result entity)
        {
            return new MdlRentPaidUnpaidAmountForDashboardChart()
            {
                TotalInvoicedRentAmount = entity.TotalInvoicedRentAmount,
                TotalPaidRentAmount = entity.TotalPaidRentAmount
            };
        }
    }
}