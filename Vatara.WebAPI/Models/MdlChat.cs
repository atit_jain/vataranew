﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlChat
    {        
    }

    public class MdlChatUnreadNotificationMessage
    {
        public string fromMsg { get; set; }
        public string message { get; set; }
        public System.DateTime creationTime { get; set; }
        public string userName { get; set; }
        public string userImage { get; set; }
        public string status { get; set; }
        public bool isFile { get; set; }
        public Nullable<int> messageCount { get; set; }
        public Nullable<int> userCount { get; set; }

        public MdlChatUnreadNotificationMessage GetMdl_ChatUnreadNotificationMessage(getChatUnreadNotificationMessage_Result entity)
        {
            return new MdlChatUnreadNotificationMessage()
            {
                fromMsg = entity.fromMsg,
                message = entity.Message,
                creationTime = entity.CreationTime,
                userName = entity.UserName,
                userImage = entity.userImage,
                status = entity.status,
                isFile = entity.isFile,
                messageCount = entity.MessageCount,
                userCount = entity.UserCount
            };
        }
    }
}