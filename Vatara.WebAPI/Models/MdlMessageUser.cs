﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlMessageUser
    {
        public int id { get; set; }
        public string emailId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int personTypeId { get; set; }
        public bool isDeleted { get; set; }
        public string status { get; set; }

        public MdlMessageUser GetMdl_MessageUser(MessageUser entity)
        {
            return new MdlMessageUser()
            {
                id = entity.Id,
                emailId = entity.EmailId,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                personTypeId = entity.PersonTypeId,
                status = entity.Status
            };
        }

       
    }
}