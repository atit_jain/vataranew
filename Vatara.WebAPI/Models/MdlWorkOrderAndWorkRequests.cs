﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlWorkOrderAndWorkRequests
    {
        public MdlWorkOrderAndWorkRequests()
        {
            lstWorkrequest = new List<MdlWorkrequest>();
        }
        public MdlWorkorder Workorder { get; set; }
        public List<MdlWorkrequest> lstWorkrequest { get; set; }
        public int PMID { get; set; }
    }
}