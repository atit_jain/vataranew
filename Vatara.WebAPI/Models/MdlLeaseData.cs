﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlLeaseData
    {
        public MdlLeaseData()
        {
            lstLeaseType = new List<MdlLeaseType>();
        }
        public MdlLeaseDetail LeaseDetail { get; set; }
        public List<MdlLeaseType> lstLeaseType { get; set; }
    }
}