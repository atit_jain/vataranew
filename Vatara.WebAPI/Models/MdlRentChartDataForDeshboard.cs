﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlRentChartDataForDeshboard
    {
        public MdlRentChartDataForDeshboard()
        {
            lstMdlPastDueRentInvoices = new List<MdlPastDueRentInvoices>();
        }
        public MdlRentPaidUnpaidAmountForDashboardChart mdlRentPaidUnpaidAmountForDashboardChart { get; set; }
        public List<MdlPastDueRentInvoices> lstMdlPastDueRentInvoices { get; set; }
    }
}