﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Models
{
    public class MdlSubscriptionHistory
    {
            public int Id { get; set; }
            public int PMID { get; set; }
            public string CustomerId { get; set; }
            public string DisplayName { get; set; }
            public string Email { get; set; }
            public string CurrencyCode { get; set; }
            public string SubScriptionId { get; set; }
            public string Plan_Code { get; set; }
            public string ProductId { get; set; }
            public string Interval { get; set; }
            public string IntervalUnit { get; set; }
            public Nullable<int> ExchangeRate { get; set; }
            public bool AutoCollect { get; set; }
            public Nullable<int> Amount { get; set; }
            public Nullable<System.DateTime> ActivatedAt { get; set; }
            public Nullable<System.DateTime> ExpiresAt { get; set; }
            public string Name { get; set; }
            public bool IsDelete { get; set; }

        public MdlSubscriptionHistory GetMdl_SubscriptionHistory(GetSubscriptionHistory_Result entity)
        {
            return new MdlSubscriptionHistory()
            {
                Id = entity.Id,
                PMID = entity.PMID,
                Amount = entity.Amount,
                CustomerId=entity.CustomerId,
                DisplayName=entity.DisplayName,
                Email=entity.Email,
                CurrencyCode=entity.CurrencyCode,
                SubScriptionId=entity.SubScriptionId,
                Plan_Code=entity.Plan_Code,
                ProductId=entity.ProductId,
                Interval=entity.Interval,
                IntervalUnit=entity.IntervalUnit,
                ExchangeRate=entity.ExchangeRate,
                AutoCollect=entity.AutoCollect,
                ActivatedAt=entity.ActivatedAt,
                ExpiresAt=entity.ExpiresAt,
                Name=entity.Name,
                IsDelete=entity.IsDeleted
            };
        }
    }
}