﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlExpiringLeases
    {
        public int Id { get; set; }
        public string LeaseId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string PropertyName { get; set; }
        public string TenantName { get; set; }
        public Nullable<int> DaysToExpire { get; set; }
        public int TenantId { get; set; }
        public int PropertyId { get; set; }


        public MdlExpiringLeases GetMdl_ExpiringLeases(getExpiringLeasesByPMID_Result entity)
        {
            return new MdlExpiringLeases() {
                Id = entity.Id,
                LeaseId = entity.LeaseId,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                PropertyName = entity.PropertyName,
                TenantName = entity.TenantName,
                DaysToExpire = entity.DaysToExpire,
                TenantId = entity.TenantId,
                PropertyId = entity.PropertyId
            };
        }
    }
}