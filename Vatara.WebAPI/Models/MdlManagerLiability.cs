﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlManagerLiability
    {
        public int serviceCategoryId { get; set; }
        public string serviceCategoryName { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string liabilityType { get; set; }
        public string liabilityCategory { get; set; }

        public MdlManagerLiability GetMdl_ManagerLiability(getManagerLiability_Result entity)
        {
            return new MdlManagerLiability
            {
                serviceCategoryId = entity.ServiceCategoryId,
                serviceCategoryName = entity.ServiceCategoryName,
                amount = entity.Amount,
                liabilityType = entity.LiabilityType,
                liabilityCategory = entity.LiabilityCategory
            };
        }
    }
}