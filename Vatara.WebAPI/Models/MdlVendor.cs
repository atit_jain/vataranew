﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlVendor
    {
        public int vendorId { get; set; }
        public Nullable<int> pmid { get; set; }
        public string companyName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<int> addressId { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string fax { get; set; }
        public string fbUrl { get; set; }
        public string twitterUrl { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<int> deleterUserId { get; set; }
        public Nullable<System.DateTime> deletionTime { get; set; }
        public Nullable<System.DateTime> lastModificationTime { get; set; }
        public Nullable<int> lastModifierUserId { get; set; }
        public Nullable<System.DateTime> creationTime { get; set; }
        public Nullable<int> creatorUserId { get; set; }

        public string service { get; set; }
        public string address { get; set; }
       
        public int cityId { get; set; }
        public int countryId { get; set; }
        public int stateId { get; set; }
        public int countyId { get; set; }
        public string zipcode { get; set; }
        public string vendorServiceIds { get; set; }

        public MdlVendor GetMdl_Vendor(Vendor entity)
        {
            return new MdlVendor()
            {
                vendorId = entity.VendorId,
                pmid = entity.PMID,
                companyName = entity.CompanyName,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                addressId = entity.AddressId,
                phone = entity.Phone,
                email = entity.Email,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl,
                isDeleted = entity.IsDeleted,
                creatorUserId = entity.CreatorUserId
            };
        }

        public MdlVendor GetMdl_Vendor(getVendorByPMID_Result entity)
        {
            return new MdlVendor()
            {
                vendorId = entity.VendorId,
                companyName = entity.CompanyName,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                addressId = entity.AddressId,
                phone = entity.Phone,
                email = entity.Email,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl,
                service = entity.Service,
                address = entity.Address
            };
        }

        public MdlVendor GetMdl_Vendor(getVendorById_Result entity)
        {
            return new MdlVendor()
            {
                vendorId = entity.VendorId,
                companyName = entity.CompanyName,
                firstName = entity.FirstName,
                lastName = entity.LastName,
                addressId = entity.AddressId,
                phone = entity.Phone,
                email = entity.Email,
                fax = entity.Fax,
                fbUrl = entity.FbUrl,
                twitterUrl = entity.TwitterUrl,               
                cityId = entity.CityId,
                countryId = entity.CountryId,
                stateId = entity.StateId,
                countyId = entity.CountyId,
                zipcode = entity.Zipcode,
                vendorServiceIds = entity.VendorServiceIds,
                address = entity.Address
            };
        }

        public Vendor GetEntity_Vendor(MdlVendor mdl)
        {
            return new Vendor()
            {
                VendorId = mdl.vendorId,
                PMID = mdl.pmid,
                CompanyName = mdl.companyName,
                FirstName = mdl.firstName,
                LastName = mdl.lastName,
                AddressId = mdl.addressId,
                Phone = mdl.phone,
                Email = mdl.email,
                Fax = mdl.fax,
                FbUrl = mdl.fbUrl,
                TwitterUrl = mdl.twitterUrl,
                IsDeleted = mdl.isDeleted,
                CreatorUserId = mdl.creatorUserId
            };
        }
    }



    public class MdlVendorRegistration
    {
        public MdlVendorRegistration()
        {
            mdlServiceCategories = new List<MdlVendorServiceCategory>();
        }
        public MdlVendor mdlVendor { get; set; }
        public MdlAddress mdlAddress { get; set; }
        public List<MdlVendorServiceCategory> mdlServiceCategories { get; set; }

    }
}