﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlTenantLeaseData
    {
        public MdlTenantLeaseData()
        {
            lstTenantLease = new List<MdlTenantLease>();
            lstOccupant = new List<MdlOccupant>();
            lstVehicle = new List<MdlVehicle>();
            lstReference = new List<MdlReference>();
            lstEmergencyContact = new List<MdlEmergencyContact>();
            lstPetInfo = new List<MdlPetInfo>();
            lstPendingRentInvoices = new List<MdlPendingRentInvoices>();
        }
        public List<MdlTenantLease> lstTenantLease { get; set; }
        public List<MdlOccupant> lstOccupant { get; set; }
        public List<MdlVehicle> lstVehicle { get; set; }
        public List<MdlReference> lstReference { get; set; }
        public List<MdlPetInfo> lstPetInfo { get; set; }        
        public List<MdlEmergencyContact> lstEmergencyContact { get; set; }
        public List<MdlPendingRentInvoices> lstPendingRentInvoices { get; set; }

    }
}