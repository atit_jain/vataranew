﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlTenantsByPMID
    {
        public int pId { get; set; }
        public string pFullName { get; set; }
        public string pAddress { get; set; }
        public string pPhoneNo { get; set; }
        public string pEmail { get; set; }
        public string pFax { get; set; }
        public string pImage { get; set; }

        public MdlTenantsByPMID Get_MdlTenantsByPMID(getAllTenantsByPMID_Result entity)
        {
            return new MdlTenantsByPMID()
            {
                pId = entity.pId,
                pFullName = entity.pFullName,
                pAddress = entity.pAddress,
                pPhoneNo = entity.pPhoneNo,
                pEmail = entity.pEmail,
                pFax = entity.pFax,
                pImage = entity.pImage
            };
        }
    }
}