﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlWorkRequestsByProperty
    {
        public int id { get; set; }
        public int workorderId { get; set; }
        public string description { get; set; }
        public string poNumber { get; set; }
        public string invoiceNumber { get; set; }
        public int serviceCategoryId { get; set; }
        public string serviceCategoryName { get; set; }
        public Nullable<int> vendorId { get; set; }
        public Nullable<int> status { get; set; }

        public MdlWorkRequestsByProperty GetMdl_WorkRequestsByProperty(getWorkRequestsByPropertyId_Result entity)
        {
            return new MdlWorkRequestsByProperty()
            {
                id = entity.Id,
                workorderId = entity.WorkorderId,
                description = entity.Description,
                poNumber = entity.PONumber,
                invoiceNumber = entity.InvoiceNumber,
                serviceCategoryId = entity.ServiceCategoryId,
                serviceCategoryName = entity.ServiceCategoryName,
                vendorId = entity.VendorId,
                status = entity.Status,
            };
        }
    }
}