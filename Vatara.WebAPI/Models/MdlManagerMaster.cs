﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlManagerMaster
    {
        public int pmid { get; set; }
        public int propId { get; set; }
        public string companyName { get; set; }
        public Nullable<int> addressId { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string url { get; set; }
        public string logo { get; set; }
        public string aboutUs { get; set; }
        public string contactUs { get; set; }
        public string sliderImage1 { get; set; }
        public string sliderImage2 { get; set; }
        public string sliderImage3 { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<int> countryId { get; set; }
        public Nullable<int> stateId { get; set; }
        public Nullable<int> countyId { get; set; }
        public Nullable<int> cityId { get; set; }
        public string mailBoxNo { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string zipCode { get; set; }
        public string email { get; set; }
        public bool useCompanyInfoForDisplay { get; set; }      

        public MdlManagerMaster GetMdl_ManagerMaster(vwManagerMaster vwMdl)
        {
            return new MdlManagerMaster() {

                pmid = vwMdl.PMID,
               // propId = vwMdl.PropId,
                companyName = vwMdl.Company_Name,
                addressId = vwMdl.Company_AddressId,
                phone = vwMdl.Company_Phone,
                fax = vwMdl.Company_Fax,
                email = vwMdl.Company_Email,
                url = vwMdl.URL+clsConstants.vataraBaseUrl,
                logo = vwMdl.Logo,
                aboutUs = vwMdl.AboutUs,
                contactUs = vwMdl.ContactUs,                
                sliderImage1 = vwMdl.SliderImage1,
                sliderImage2 = vwMdl.SliderImage2,
                sliderImage3 = vwMdl.SliderImage3,               
                // isActive = vwMdl.IsActive,
                countryId = (int)(vwMdl.Company_CountryId == null ? 0 : vwMdl.Company_CountryId),
                stateId = (int)(vwMdl.Company_StateId == null ? 0 : vwMdl.Company_StateId),
                countyId = (int)(vwMdl.Company_CountyId == null ? 0 : vwMdl.Company_CountyId),
                cityId = (int)(vwMdl.Company_CityId == null ? 0 : vwMdl.Company_CityId),               
                line1 = vwMdl.Company_Line1,
                line2 = vwMdl.Company_Line2,
                zipCode = vwMdl.Company_Zipcode,
                useCompanyInfoForDisplay = (bool)vwMdl.useCompanyInfoForDisplay
            };
        }

        public ManagerMaster Get_Entity_ManagerMaster(MdlManagerMaster mdl)
        {
            return new ManagerMaster()
            {
                PMID = mdl.pmid,
                //PropId = mdl.propId,

                // Commented By asr 2018-10-03
                //CompanyName = mdl.companyName,
                //AddressId = mdl.addressId,
                //Phone = mdl.phone,
                //Email = mdl.email,
                //Fax = mdl.fax,
                //End Commented By asr 2018-10-03
                URL = mdl.url,
                Logo = mdl.logo,
                AboutUs = mdl.aboutUs,
                ContactUs = mdl. contactUs,              
                SliderImage1 = mdl.sliderImage1,
                SliderImage2 = mdl.sliderImage2,
                SliderImage3 = mdl.sliderImage3,              
                //IsActive = mdl.isActive             
            };
        }

        public Address GetAddressModel(MdlManagerMaster mdl)
        {
            return new Address()
            {
                Line1 = mdl.line1,
                Line2 = mdl.line2,
                CountryId = (int)(mdl.countryId == null ? 0: mdl.countryId),
                StateId = (int)(mdl.stateId == null? 0: mdl.stateId) ,
                CityId = (int)(mdl.cityId == null? 0: mdl.cityId),
                CountyId = mdl.countyId,
                Zipcode = mdl.zipCode,
            };
        }

        public string GetManagerCompanyURL(string stringforCompanyUrl, VataraEntities ve)
        {
            string url = string.Empty;
            if (stringforCompanyUrl.Length > 8)
            {
                url = stringforCompanyUrl.Replace(" ", "").ToLower().Substring(0, 8);
            }
            else
            {
                url = stringforCompanyUrl.Replace(" ", "").ToLower();
            }
           bool isExist = ve.ManagerMasters.Any(e => e.URL.ToLower() == url.ToLower());
            if (isExist)
            {
                url = url+new clsCommon().ReturnRandomAlphaNumericString(4);
            }         
            return url;
        }
    }
}