﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class MdlVehicle
    {
        public long id { get; set; }
        public string vehicleName { get; set; }
        public int leaseId { get; set; }
        public string detail { get; set; }
        public int tenantId { get; set; }

        public MdlVehicle GetMdl_Vehicle(vwVehicle mdl)
        {
            return new MdlVehicle()
            {
                id = mdl.Id,
                vehicleName = mdl.VehicleName,
                leaseId = mdl.LeaseId,
                detail = mdl.Detail,
                tenantId = mdl.TenantId
            };
        }
    }
}