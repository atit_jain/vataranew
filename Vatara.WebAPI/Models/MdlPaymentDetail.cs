﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class MdlPaymentDetail
    {      
        public MdlPaymentDetail()
        {
            lstBills = new List<MdlBill>();
            lstPaymentMode = new List<MdlPaymentMode>();
        }
       public List<MdlBill> lstBills { get; set; }
       public List<MdlPaymentMode> lstPaymentMode { get; set; }
       public Nullable<decimal> TotalAmount { get; set; }
       public decimal PaidAmount { get; set; }

    }
}