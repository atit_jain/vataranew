﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
using Vatara.WebAPI.Class;

namespace Vatara.WebAPI
{
    public class clsCommon
    {

        #region ConnectionString

        public static string VataraConnectionString = ConfigurationManager.ConnectionStrings["VataraConnection"].ConnectionString;

        #endregion

        private static Random random = new Random();

        public string ReturnPersonType(int personTypeId)
        {
            string  personType = new VataraEntities().PersonTypes.FirstOrDefault(e => e.Id == personTypeId).Name.ToLower();
            if (personType == clsConstants.propertyManager)
            {
                personType = clsConstants.propertyManager_Short;
            }
            return personType;
        }

        public string ReturnManagerRole(string role)
        {
            if (role.ToLower() == clsConstants.propertyManager_Short)
            {
                return "all";
            }
            else if (role.ToLower() == clsConstants.RW)
            {
                return "readwrite";
            }
            else if (role.ToLower() == clsConstants.RO)
            {
                return "readonly";
            }
            else { return "readonly"; };
        }

        public string ReturnRandomAlphaNumericString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public enum PersonType
        {
            PropertyManager = 1,
            Owner = 2,
            Tenant = 3,
            Vendor = 4,
            Associations = 5
        }

        public enum PropertyStatus
        {
            Leased = 5,
            Vacant = 6           
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = null;
            DataTable table = null;
            try
            {
                properties = TypeDescriptor.GetProperties(typeof(T));
                table = new DataTable();
                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }
            }
            finally
            {
                properties = null;
            }
            return table;
        }
     
    }
}