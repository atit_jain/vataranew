﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class clsChat
    {
        VataraEntities entity = new VataraEntities();
        public long saveChat(Message mdl)
        {
            if (mdl.Id > 0)
            {
                mdl.LastModificationTime = DateTime.UtcNow;
                entity.Entry(mdl).State = EntityState.Modified;
            }
            else
            {
                mdl.CreationTime = DateTime.UtcNow;
                mdl.IsDeleted = false;
                entity.Messages.Add(mdl);
            }
            entity.SaveChanges();
            return mdl.Id;
        }

        public void messageDelivered(long messageID)
        {
            var message = entity.Messages.First(m => m.Id == messageID);
            message.IsDelivered = true;
            message.IsRead = true;
            message.LastModificationTime = DateTime.UtcNow;
            entity.Entry(message).State = EntityState.Modified;
            entity.SaveChanges();
        }
    }
}