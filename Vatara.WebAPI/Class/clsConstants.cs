﻿using System;
using System.Configuration;

namespace Vatara.WebAPI
{ 
    public class clsConstants
    {

        #region FileTypes
        public const string FileTypeDocuments = "Documents";
        #endregion

        #region LoginConstants
        public const string EmailAlreadyExists = "Email Already Exists..";
        public const string verifyEmail = "verifyEmail";
        public const string EmailAndCompanyNotMatched = "EmailAndCompanyNotMached";
        public const string UnknownUser = "UnknownUser";
        public const string WrongUrl = "WrongUrl";
        #endregion

        #region AddressTypes

        public const string personAddress = "Person Address";
        public const string companyAddress = "Company Address";
        #endregion

        #region PersonType const

        public const string propertyManager_Short = "pm";
        public const string propertyManager = "property manager";
        public const string Tenant = "tenant";
        public const string Owner = "owner";
        public const string Receive = "Receive";
        public const string Payment = "Payment";
        public const string CashPayment = "Cash";

        #endregion

        public const string EmailVarificationSubject = "Email varification for vatara account";

        public const string PasswordForVataraLogin = "Password for login in vatara.com";
        public const string TransactionEntryintoBill = "Transaction entry into bill";
        public const string WorkorderDeletedSuccess = "Work order deleted sucessfully";
        public const string WorkrequestDeletedSuccess = "Work request deleted sucessfully";
        public const string WorkrequestDeleteError = "Error in work order deletion.";
        public const string WorkrequestUpdateError = "Error in work order updation.";
        
        #region Invoice payment frequency
        public const string Annually = "Annually";
        public const string SemiAnnually = "Semi Annually";
        public const string Quaterly = "Quaterly";
        public const string Monthly = "Monthly";
        public const string Weekly = "Weekly";
        
        #endregion


        #region Role
        public const string RW = "rw";
        public const string RO = "ro";
        #endregion

        #region ModuleName
        public const string Personmodule = "Person";
        public const string Propertymodule = "PROPERTY";
        public const string LeaseModule = "LEASE";
        public const string WorkorderModule = "WORKORDERS";
        public const string VendorModule = "VENDOR";
        public const string AssociationModule = "ASSOCIATION";
        public const string ManagerCompModule = "MANAGERCOMP";
        public const string CompSliderImageModule = "COMPSLIDERIMAGE";

        
        #endregion

        #region Liability Filert Criteria
        public const string PaidLiabilities = "Paid";
        public const string UnPaidLiabilities = "Unpaid";
        #endregion

        #region PersonStatus

        public const string PersonStatusActive = "Active";
        public const string PersonStatusInActive = "In-Active";       

        #endregion

        #region Owner Type

        public const string individualOwner = "individual";
        public const string companyOwner = "company";


        #endregion

        #region Status
        public const string Inactive = "INACTIVE";
        public const string Leased = "LEASED";
        public const string Vacant = "VACANT";
        public const string stsNew = "NEW";
        public const string Active = "Active";
        
        #endregion

        public const string vataraBaseUrl = ".vatara.com";

        public const string paymentTypeOnline = "Online";
        public const string invoiceModule = "Invoice";
        public const string savedAsDraft = "Saved as draft";
        public const string sentToClient = "Sent to client";
        public const string invoiceDeleted = "Deleted";
        public const string invoiceCancelled = "Cancelled";
        public const string invoiceDeletedSuccess = "Invoice deleted sucessfully";
        public const string WriteOff = "WriteOff";
        public const string Paid = "Paid";
        public const string PartialPymt = "PartialPymt";

        public const string errorInLeaseSaving = "Error in lease data saving, Please contact Admin for help";
        public const string somethingwrong = "Something went wrong. Please try again later";
        public const string errorInvoiceCreation = "Error creating Invoice, Please contact Admin for help";
        public const string errorPropertyUpdation = "Error updating Property, Please contact Admin for help";
        public const string errorPropertyCreation = "Error creating Property, Please contact Admin for help";
        public const string propayErrorCode_40 = "Invalid ssn.";
        public const string propayErrorCode_41 = "Invalid dob! varify that dob is valid and follows 'mm-dd-yyyy' format.";
        public const string propayErrorCode_46 = "Invalid RoutingNumber! varify that RoutingNumber is valid and not exceed 9 characters.";
        public const string propayErrorCode_47 = "Invalid Bank Account Number.";
        public const string propayErrorCode_53 = "A Propay account with this email address already exists.";
        public const string propayErrorCode_54 = "A Propay account with this social security number already exists.";
        public const string propayErrorCode_81 = "The ProPay account has expired. Contact your relationship manager.";
        public const string propayErrorCode_87 = "The username being assigned to the new account is already taken.";
        public const string wrongUserNameAndPassword = "wrongUserNameAndPassword";

        public const string rentServiceCategory = "rent";

        public const string Person = "PERSON";

        public const string Payer = "PAYER";
        public const string Receiver = "RECEIVER";

        public const string WorkOrderCancelled = "CANCELLED";
        public const string WorkOrderDeleted = "Delete";

        public const string somethingwrongDeleteEvent = "Error Deleting Event,Please contact Admin for help";
        public const string somethingwrongArchiveEvent = "Error Archive Event,Please contact Admin for help";
        public const string somethingwrongSaveEvent = "Error Creating Event,Please contact Admin for help";

        #region SupportTickets
        public const string open = "open";
        public const string closed = "closed";
        public const string onhold = "onhold";
        public const string inprogress = "inprogress";
        #endregion
        #region SupportTicketsModule
        public const string SupportTicket = "SupportTicket";
        #endregion

    }

}