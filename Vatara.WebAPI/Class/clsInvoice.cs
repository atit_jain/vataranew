﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Vatara.DataAccess;
using BusinessLayer;

namespace Vatara.WebAPI
{
    public class clsInvoice
    {      
        public string invoiceBill(string billEntry,int billNo)
        {           
                if (billEntry == string.Empty || billEntry == null)
                {
                    return billNo.ToString();
                }
                else
                {
                    return billEntry = billEntry + ',' + billNo.ToString();
                }            
        }

        public Bill FillBillModel(MdlPayment mdlPayment,string payment)
        {
            Bill bill = new Bill();
            Invoice tblInvoice = new Invoice();
            VataraEntities ve = new VataraEntities();
            try
            {
                if (mdlPayment.invoiceId != 0)
                {
                    tblInvoice = ve.Invoices.First(e => e.Id == mdlPayment.invoiceId);
                    bill.From = Convert.ToInt32(tblInvoice.From);
                    bill.To = Convert.ToInt32(tblInvoice.To);
                    bill.PropertyID = Convert.ToInt32(tblInvoice.PropertyID);
                    bill.InvoiceNumber = mdlPayment.invoiceId;
                    bill.RecipientType = (int)clsCommon.PersonType.PropertyManager;
                    bill.PayerType = (int)tblInvoice.RecipientPersonType;
                    bill.IsRecurringPayment = mdlPayment.isRecurringPayment;
                }
                else
                {
                    bill.From = mdlPayment.clientId;     
                    bill.To = mdlPayment.manageId;      
                    bill.PropertyID = mdlPayment.propertyId;
                    bill.InvoiceNumber = 0;
                    bill.RecipientType = mdlPayment.RecipientType;
                    bill.PayerType = mdlPayment.PayerType;
                    bill.IsRecurringPayment = mdlPayment.isRecurringPayment;
                }
                bill.HostedTransactionIdentifier = mdlPayment.transactionIdentifier;
                bill.Amount = mdlPayment.amount;
                bill.Date = DateTime.Now;
                
                bill.PaymentType = payment;//payMode.Name;
                if (mdlPayment.selectPaymentType == 0)
                {
                    bill.OnlinePaymentType = "Credit Card";
                }
                else
                {
                    bill.OnlinePaymentType = "ACH";
                }
                bill.IsDeleted = false;
                bill.CreationTime = DateTime.Now;
                bill.CreatorUserId = mdlPayment.manageId;
                bill.Comment = mdlPayment.description;
                return bill;
            }
            finally
            {
                 tblInvoice = null;
                 ve = null;
            }           
        }

        public Bill FillBillModel_WithoutInvoice(MdlPayment mdlPayment, string payment)
        {
            Bill bill = new Bill();

            return bill;
        }

        public Transaction FillTransactionModel(Bill bill, InvoiceDetail invoce,PaymentMode payMode,decimal paidAmountFordetail)
        {
            Transaction transaction = new Transaction();
            transaction.BillNo = bill.BillId;
            transaction.InvoiceNo = invoce.InvoiceNumber;
            transaction.TranRefNo = invoce.Id;
            transaction.TranDate = DateTime.Now.Date;
            transaction.Amount = paidAmountFordetail;
            transaction.PayMode = payMode.Id;
            transaction.Category = invoce.CategoryID;
            transaction.IsDeleted = false;
            transaction.CreationTime = DateTime.Now;
            transaction.PayMode = payMode.Id;
            return transaction;           
        }

        public Invoice FillInvoiceMdlForWriteOff(MdlPayment mdlPayment)
        {
            Invoice invoce = null;
            VataraEntities ve = new VataraEntities();
            invoce = ve.Invoices.First(e => e.Id == mdlPayment.invoiceId);
            invoce.StatusId = mdlPayment.statusId;
            invoce.WrtOffAmt = mdlPayment.amount;
            invoce.WrtOffDate = DateTime.Now;
            invoce.WrtOffDesc = mdlPayment.description;
            invoce.LastModifierUserId = mdlPayment.manageId;
            invoce.LastModificationTime = DateTime.Now;
            ve = null;
            return invoce;
        }       
    }
}