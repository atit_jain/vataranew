﻿
using System.Net.Mail;
using System.Net;
using Vatara.DataAccess;
using System;
using System.Configuration;

namespace Vatara.WebAPI.Class
{
    public class Email
    {

        public void SendMail(string FromId, string Frompassword, string ToId, string Subject, string Body)
        {

            MailMessage mailmsg = new MailMessage();
            SmtpClient smtpclient = new SmtpClient();
            mailmsg.To.Add(ToId);
            mailmsg.Subject = Subject;
            mailmsg.From = new MailAddress(FromId);
            mailmsg.Body = CreatemailBody(Body);
            smtpclient.Port = 587;
            smtpclient.Host = "smtp.gmail.com";
            mailmsg.IsBodyHtml = true;
            smtpclient.EnableSsl = true;
            smtpclient.UseDefaultCredentials = false;
            smtpclient.Credentials = new NetworkCredential(FromId, Frompassword);
            smtpclient.Send(mailmsg);

        }

        //private AlternateView Mail_Body()
        //{
        //    string path = Server.MapPath(@"Images/photo.jpg");
        //    LinkedResource Img = new LinkedResource(path, MediaTypeNames.Image.Jpeg);
        //    Img.ContentId = "MyImage";
        //    string str = @"  
        //    <table>  
        //        <tr>  
        //            <td> '" + txtmessagebody.Text + @"'  
        //            </td>  
        //        </tr>  
        //        <tr>  
        //            <td>  
        //              <img src=cid:MyImage  id='img' alt='' width='100px' height='100px'/>   
        //            </td>  
        //        </tr></table>  
        //    ";
        //    AlternateView AV =
        //    AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
        //    AV.LinkedResources.Add(Img);
        //    return AV;
        //}

        public string CreatemailBody(string link)
        {
            string htmldata = @"<html>
                                <body>
    <div>
        <u></u>
        <div>
            <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' class='' align='center'>
                <tbody>
                    <tr>
                        <td rowspan='1' colspan='1'>
                            <table cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                <tbody>
                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                <span>Please click below link to verify your email:</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:0px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:10px;line-height:20px;text-align:left;font-weight:bold;' align='left' rowspan='1' colspan='1'></td>
                                                                        </tr>
                                                                        <tr>  </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='1' colspan='1'>
                                            <table width='700px' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                                <tbody>
                                                    <tr>
                                                        <td style='color:#cccccc;padding-top:20px' valign='top' width='100%' rowspan='1' colspan='1'>
                                                            <hr color='#cccccc' size='1'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                            </div>
                                                            <u></u><u></u>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>

                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                <a href='" + link + @"'>vatara.com</a>
                                                                            </td>
                                                                        </tr>                                                                                 
                                                                                    
                                                                             </tbody>         
                                                                         </table>         
                                                                     </div>         
                                                                 </td>         
                                                             </tr>        
                                                         </tbody>         
                                                     </table>         
                                                 </td>         
                                             </tr>        
                                         </tbody>         
                                     </table>         
                                 </td>         
                             </tr>         
                         </tbody>         
                     </table>         
                 </div>         
             </div>
         </body> 
       </html>";
            return htmldata;
        }

        public string CreatemailBodyForNewAccountPassword(string password, string Email, string websiteUrl)
        {
            string htmldata = @"<html>
                                <body>
    <div>
        <u></u>
        <div>
            <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' class='' align='center'>
                <tbody>
                    <tr>
                        <td rowspan='1' colspan='1'>
                            <table cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                <tbody>
                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                <span>Please use below credentials for login in your vatara account:</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:0px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:10px;line-height:20px;text-align:left;font-weight:bold;' align='left' rowspan='1' colspan='1'></td>
                                                                        </tr>
                                                                        <tr>  </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='1' colspan='1'>
                                            <table width='700px' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                                <tbody>
                                                    <tr>
                                                        <td style='color:#cccccc;padding-top:20px' valign='top' width='100%' rowspan='1' colspan='1'>
                                                            <hr color='#cccccc' size='1'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                            </div>
                                                            <u></u><u></u>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>
                                                                            <tr>
                                                                                <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                    WebSiteUrl - " + websiteUrl + @"                                                                               
                                                                                </td>
                                                                            </tr>  

                                                                            <tr>
                                                                                <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                    EmailId - " + Email + @"
                                                                                    <br>
                                                                                     Password - " + password + @"
                                                                                </td>
                                                                            </tr>                                                                                 
                                                                             </tbody>         
                                                                         </table>         
                                                                     </div>         
                                                                 </td>         
                                                             </tr>        
                                                         </tbody>         
                                                     </table>         
                                                 </td>         
                                             </tr>        
                                         </tbody>         
                                     </table>         
                                 </td>         
                             </tr>         
                         </tbody>         
                     </table>         
                 </div>         
             </div>
         </body> 
       </html>";



            return htmldata;
        }

        public string CreatemailBodyForNewAccountPasswordForAdmin(string password, string Email)
        {
            string htmldata = @"<html>
                                <body>
    <div>
        <u></u>
        <div>
            <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' class='' align='center'>
                <tbody>
                    <tr>
                        <td rowspan='1' colspan='1'>
                            <table cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                <tbody>
                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                <span>Please use below credentials for login in your vatara account:</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:0px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:10px;line-height:20px;text-align:left;font-weight:bold;' align='left' rowspan='1' colspan='1'></td>
                                                                        </tr>
                                                                        <tr>  </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='1' colspan='1'>
                                            <table width='700px' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                                <tbody>
                                                    <tr>
                                                        <td style='color:#cccccc;padding-top:20px' valign='top' width='100%' rowspan='1' colspan='1'>
                                                            <hr color='#cccccc' size='1'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                            </div>
                                                            <u></u><u></u>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>

                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                EmailId - " + Email + @"
                                                                                <br>
                                                                                 Password - " + password + @"
                                                                            </td>
                                                                        </tr>                                                                                 
                                                                                    
                                                                             </tbody>         
                                                                         </table>         
                                                                     </div>         
                                                                 </td>         
                                                             </tr>        
                                                         </tbody>         
                                                     </table>         
                                                 </td>         
                                             </tr>        
                                         </tbody>         
                                     </table>         
                                 </td>         
                             </tr>         
                         </tbody>         
                     </table>         
                 </div>         
             </div>
         </body> 
       </html>";



            return htmldata;
        }

        public string CreatEmailBodyForNewPassword(string password, string Email)
        {
            string htmldata = @"<html>
                                <body>
    <div>
        <u></u>
        <div>
            <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' class='' align='center'>
                <tbody>
                    <tr>
                        <td rowspan='1' colspan='1'>
                            <table cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                <tbody>
                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                <span>Your password is reset successfully. Please use below password for login in your account:</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:0px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:10px;line-height:20px;text-align:left;font-weight:bold;' align='left' rowspan='1' colspan='1'></td>
                                                                        </tr>
                                                                        <tr>  </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='1' colspan='1'>
                                            <table width='700px' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                                <tbody>
                                                    <tr>
                                                        <td style='color:#cccccc;padding-top:20px' valign='top' width='100%' rowspan='1' colspan='1'>
                                                            <hr color='#cccccc' size='1'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                            </div>
                                                            <u></u><u></u>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>

                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                 Password - " + password + @"
                                                                            </td>
                                                                        </tr>                                                                                 
                                                                                    
                                                                             </tbody>         
                                                                         </table>         
                                                                     </div>         
                                                                 </td>         
                                                             </tr>        
                                                         </tbody>         
                                                     </table>         
                                                 </td>         
                                             </tr>        
                                         </tbody>         
                                     </table>         
                                 </td>         
                             </tr>         
                         </tbody>         
                     </table>         
                 </div>         
             </div>
         </body> 
       </html>";



            return htmldata;
        }

        public string CreatEmailBodyForTransactionHistory(PayerRecipientDetail PayerDetail, PayerRecipientDetail ReceiverDetail, MdlTransactionHistory TransactionHistory)
        {
            string htmldata = @"<html>
                                <body>
    <div>
        <u></u>
        <div>
            <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#ffffff' class='' align='center'>
                <tbody>
                    <tr>
                        <td rowspan='1' colspan='1'>
                            <table cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                <tbody>
                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                <span>Please enter the following transaction detail in Bill and Transaction table:</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign='top' style='padding-top:0px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:10px;line-height:20px;text-align:left;font-weight:bold;' align='left' rowspan='1' colspan='1'></td>
                                                                        </tr>
                                                                        <tr>  </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan='1' colspan='1'>
                                            <table width='700px' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff' align='center'>
                                                <tbody>
                                                    <tr>
                                                        <td style='color:#cccccc;padding-top:20px' valign='top' width='100%' rowspan='1' colspan='1'>
                                                            <hr color='#cccccc' size='1'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                            </div>
                                                            <u></u><u></u>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign='top' rowspan='1' colspan='1'>
                                            <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan='1' colspan='1'>
                                                            <div>
                                                                <table width='100%' cellpadding='0' cellspacing='0' border='0' bgcolor='#ffffff'>

                                                                    <tbody>

                                                                        <tr>
                                                                            <td valign='top' style='padding-top:20px;font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif;color:#205081;font-size:20px;line-height:32px;text-align:left;font-weight:bold' align='left' rowspan='1' colspan='1'>
                                                                                 PayerName - " + PayerDetail + @"
                                                                            </td>
                                                                        </tr>                                                                                 
                                                                                    
                                                                             </tbody>         
                                                                         </table>         
                                                                     </div>         
                                                                 </td>         
                                                             </tr>        
                                                         </tbody>         
                                                     </table>         
                                                 </td>         
                                             </tr>        
                                         </tbody>         
                                     </table>         
                                 </td>         
                             </tr>         
                         </tbody>         
                     </table>         
                 </div>         
             </div>
         </body> 
       </html>";



            return htmldata;
        }


        public void InsertEmailData(VataraEntities entity, string fromEmail, string toEmail, string subject, string body, bool IsResend)
        {
            EmailData objEmailData = new EmailData();
            fromEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
            objEmailData.From = fromEmail;
            objEmailData.To = toEmail;
            objEmailData.Subject = subject;
            objEmailData.Purpose = body;
            objEmailData.IsSent = false;
            objEmailData.ReSend = IsResend;
            objEmailData.CreationTime = DateTime.Now;
            entity.EmailDatas.Add(objEmailData);
            entity.SaveChanges();

        }

    }
}