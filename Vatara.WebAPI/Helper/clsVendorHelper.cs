﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Vatara.WebAPI.Controllers;

namespace Vatara.WebAPI
{
    public class clsVendorHelper
    {
        public int SaveVendorServiceCategories(List<MdlUpdateVendorServiceCategory> lstMdlUpdateVendorServiceCategory,int vendorId)
        {
            int result = 0;
            DataTable mdlUpdateVendorServiceCategory = null;
            clsCommon objClsCommon = new clsCommon();
            SqlConnection con = null;
            try
            {
                mdlUpdateVendorServiceCategory = new clsCommon().ConvertToDataTable(lstMdlUpdateVendorServiceCategory);
                con = new SqlConnection(clsCommon.VataraConnectionString);
                using (SqlCommand cmd = new SqlCommand("sp_SaveVendorServiceCategories", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ut_VendorServiceCategories", mdlUpdateVendorServiceCategory);
                    cmd.Parameters.AddWithValue("@vendorId", vendorId);
                    cmd.Parameters.Add("@output", SqlDbType.Int);                    
                    cmd.Parameters["@output"].Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();                 
                     result = (int)cmd.Parameters["@output"].Value;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con = null;
                objClsCommon = null;
                lstMdlUpdateVendorServiceCategory = null;
                mdlUpdateVendorServiceCategory = null;
            }
            return result;
        }
    }
}