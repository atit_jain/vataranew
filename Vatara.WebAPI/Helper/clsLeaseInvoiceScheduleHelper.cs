﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class clsLeaseInvoiceScheduleHelper
    {
        public void SaveLeaseInvoiceSchedule(DateTime StartDate, DateTime EndDate, int PaymentFrequency, decimal LeaseAmount, int? DayOfMonth, string WeekDay, int PMID, int LeaseID, VataraEntities ve)
        {
            DateTime InvoiceDate = StartDate;
            string PaymentFrequencyName;
            LeaseInvoiceSchedule LeaseInvoice = null;
            try
            {
                var LeaseInvoiceSchedules = ve.LeaseInvoiceSchedules.Where(Lease => Lease.LeaseID == LeaseID).ToList();
                if (LeaseInvoiceSchedules != null)
                {
                    foreach (var list in LeaseInvoiceSchedules)
                    {
                        ve.LeaseInvoiceSchedules.Remove(list);
                    }
                }
                PaymentFrequencyName = ve.LeasePaymentFrequencies.Where(m => m.Id == PaymentFrequency).Select(m => m.Name).SingleOrDefault();
                //weekly
                if (PaymentFrequencyName.ToLower() == clsConstants.Weekly.ToLower())
                {
                    while (InvoiceDate < EndDate)
                    {
                        LeaseInvoice = new LeaseInvoiceSchedule();
                        LeaseInvoice.PMID = PMID;
                        LeaseInvoice.IsInvoiceSend = false;
                        LeaseInvoice.LeaseID = LeaseID;
                        LeaseInvoice.Amount = LeaseAmount;

                        if (InvoiceDate.AddDays(5).DayOfWeek.ToString() == WeekDay)
                        {
                            LeaseInvoice.InvoiceDate = InvoiceDate.ToUniversalTime();
                            InvoiceDate = InvoiceDate.AddDays(7);
                            ve.LeaseInvoiceSchedules.Add(LeaseInvoice);
                        }
                        else
                        {
                            InvoiceDate = InvoiceDate.AddDays(1);
                        }
                        
                    }
                    ve.SaveChanges();
                }
                //Monthly
                else if (PaymentFrequencyName.ToLower() == clsConstants.Monthly.ToLower())
                {
                    while (InvoiceDate < EndDate)
                    {
                        LeaseInvoice = new LeaseInvoiceSchedule();
                        LeaseInvoice.PMID = PMID;
                        LeaseInvoice.IsInvoiceSend = false;
                        LeaseInvoice.LeaseID = LeaseID;
                        if (InvoiceDate == StartDate)
                        {
                            int Days = DateTime.DaysInMonth(StartDate.Year, StartDate.Month);
                            LeaseInvoice.Amount = (LeaseAmount / Days) * (Days - (Convert.ToInt32(StartDate.Day) - 1));
                            InvoiceDate = Convert.ToDateTime(InvoiceDate.Month + "-" + (DayOfMonth) + "-" + InvoiceDate.Year).AddDays(-5);
                        }
                        else
                        {
                            InvoiceDate = InvoiceDate.AddMonths(1);
                            if ((EndDate - InvoiceDate).TotalDays < 31)
                            {
                                int NoOfMonthForRent = Convert.ToInt32(12 * (EndDate.Year - StartDate.Year) + EndDate.Month - StartDate.Month);
                                int days = DateTime.DaysInMonth(EndDate.Year, EndDate.Month);
                                LeaseInvoice.Amount = (LeaseAmount / days) * (Convert.ToInt32(EndDate.Day));
                            }
                            else
                            {
                                LeaseInvoice.Amount = LeaseAmount;
                            }
                        }
                        LeaseInvoice.InvoiceDate = InvoiceDate.ToUniversalTime();
                        if (InvoiceDate < EndDate)
                        {
                            ve.LeaseInvoiceSchedules.Add(LeaseInvoice);
                        }
                    }
                    ve.SaveChanges();
                }
                //Quaterly
                else if (PaymentFrequencyName.ToLower() == clsConstants.Quaterly.ToLower())
                {
                    int NoOfMonthForRent = Convert.ToInt32(12 * (EndDate.Year - InvoiceDate.Year) + EndDate.Month - InvoiceDate.Month);
                    NoOfMonthForRent = NoOfMonthForRent / 3;
                    while (InvoiceDate < EndDate)
                    {
                        LeaseInvoice = new LeaseInvoiceSchedule();
                        LeaseInvoice.PMID = PMID;
                        LeaseInvoice.IsInvoiceSend = false;
                        LeaseInvoice.LeaseID = LeaseID;
                        LeaseInvoice.Amount = LeaseAmount;
                        if (InvoiceDate == StartDate)
                        {
                            InvoiceDate = Convert.ToDateTime(InvoiceDate.Month + "-" + (DayOfMonth) + "-" + InvoiceDate.Year).AddDays(-5);
                        }
                        else
                        {
                            InvoiceDate = InvoiceDate.AddMonths(3);
                        }
                        LeaseInvoice.InvoiceDate = InvoiceDate.ToUniversalTime();
                        if (InvoiceDate < EndDate)
                        {
                            ve.LeaseInvoiceSchedules.Add(LeaseInvoice);
                        }
                    }
                    ve.SaveChanges();
                }
                //Semi Annually
                else if (PaymentFrequencyName.ToLower() == clsConstants.SemiAnnually.ToLower())
                {
                    while (InvoiceDate < EndDate)
                    {
                        LeaseInvoice = new LeaseInvoiceSchedule();
                        LeaseInvoice.PMID = PMID;
                        LeaseInvoice.IsInvoiceSend = false;
                        LeaseInvoice.LeaseID = LeaseID;
                        LeaseInvoice.Amount = LeaseAmount;
                        if (InvoiceDate == StartDate)
                        {
                            InvoiceDate = Convert.ToDateTime(InvoiceDate.Month + "-" + (DayOfMonth) + "-" + InvoiceDate.Year).AddDays(-5);
                        }
                        else
                        {
                            InvoiceDate = InvoiceDate.AddMonths(6);
                        }
                        LeaseInvoice.InvoiceDate = InvoiceDate.ToUniversalTime();
                        if (InvoiceDate < EndDate)
                        {
                            ve.LeaseInvoiceSchedules.Add(LeaseInvoice);
                        }
                    }
                    ve.SaveChanges();
                }
                //Annually
                else if (PaymentFrequencyName.ToLower() == clsConstants.Annually.ToLower())
                {
                    while (InvoiceDate < EndDate)
                    {
                        LeaseInvoice = new LeaseInvoiceSchedule();
                        LeaseInvoice.PMID = PMID;
                        LeaseInvoice.IsInvoiceSend = false;
                        LeaseInvoice.LeaseID = LeaseID;
                        LeaseInvoice.Amount = LeaseAmount;
                        if (InvoiceDate == StartDate)
                        {
                            InvoiceDate = Convert.ToDateTime(InvoiceDate.Month + "-" + (DayOfMonth) + "-" + InvoiceDate.Year).AddDays(-5);
                        }
                        else
                        {
                            InvoiceDate = InvoiceDate.AddYears(1);
                        }
                        LeaseInvoice.InvoiceDate = InvoiceDate.ToUniversalTime();
                        if (InvoiceDate < EndDate)
                        {
                            ve.LeaseInvoiceSchedules.Add(LeaseInvoice);
                        }
                    }
                    ve.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                LeaseInvoice = null;
            }
        }

    }
}