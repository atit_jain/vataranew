﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Vatara.WebAPI
{
    public class clsDashboardHelper
    {
        
        public List<MdlIncomeExpenseLiabilityDataForDashboard> GetIncomeExpenseDataForDashboard(int PMID,string Duration)
        {
            List<MdlIncomeExpenseLiabilityDataForDashboard> lstMdlIncomeExpenseLiabilityDataForDashboard = new List<MdlIncomeExpenseLiabilityDataForDashboard>();

            using (SqlConnection conn = new SqlConnection(clsCommon.VataraConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "getIncomeExpenseLiabilityDataForDashboard";
                    cmd.Parameters.AddWithValue("@PMID", PMID);
                    cmd.Parameters.AddWithValue("@Duration", Duration);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {                       
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);
                        lstMdlIncomeExpenseLiabilityDataForDashboard = clsDashboardHelper.ConvertDataTable<MdlIncomeExpenseLiabilityDataForDashboard>(dataset.Tables[0] );
                        return lstMdlIncomeExpenseLiabilityDataForDashboard;
                    }
                }
            }
        }


        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        pro.SetValue(obj, dr[column.ColumnName], null);
                        break;
                    }
                       
                    else
                        continue;
                }
            }
            return obj;
        }


        //public List<T> ConvertTo<T>(DataTable datatable) where T : new()
        //{
        //    List<T> Temp = new List<T>();
        //    try
        //    {
        //        List<string> columnsNames = new List<string>();
        //        foreach (DataColumn DataColumn in datatable.Columns)
        //            columnsNames.Add(DataColumn.ColumnName);
        //        Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
        //        return Temp;
        //    }
        //    catch
        //    {
        //        return Temp;
        //    }

        //}

        //public T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        //{
        //    T obj = new T();
        //    try
        //    {
        //        string columnname = "";
        //        string value = "";
        //        PropertyInfo[] Properties;
        //        Properties = typeof(T).GetProperties();
        //        foreach (PropertyInfo objProperty in Properties)
        //        {
        //            columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
        //            if (!string.IsNullOrEmpty(columnname))
        //            {
        //                value = row[columnname].ToString();
        //                if (!string.IsNullOrEmpty(value))
        //                {
        //                    if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
        //                    {
        //                        value = row[columnname].ToString().Replace("$", "").Replace(",", "");
        //                        objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
        //                    }
        //                    else
        //                    {
        //                        value = row[columnname].ToString().Replace("%", "");
        //                        objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
        //                    }
        //                }
        //            }
        //        }
        //        return obj;
        //    }
        //    catch
        //    {
        //        return obj;
        //    }
        //}



    }
}