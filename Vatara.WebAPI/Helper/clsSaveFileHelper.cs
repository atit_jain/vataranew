﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Vatara.WebAPI
{
    public class clsSaveFileHelper
    {
        public int SaveFiles(List<MdlFile> lstmdlFile)
        {
            int result = 0;
            DataTable mdlFile = null;
            clsCommon objClsCommon = new clsCommon();
            SqlConnection con = null;
            try
            {
                mdlFile = new clsCommon().ConvertToDataTable(lstmdlFile);
                con = new SqlConnection(clsCommon.VataraConnectionString);
                using (SqlCommand cmd = new SqlCommand("sp_SaveFiles", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ut_File", mdlFile);                    
                    cmd.Parameters.Add("@output", SqlDbType.Int);
                    cmd.Parameters["@output"].Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    result = (int)cmd.Parameters["@output"].Value;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con = null;
                objClsCommon = null;
                lstmdlFile = null;
            }
            return result;
        }
    }
}