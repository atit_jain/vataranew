﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vatara.DataAccess;
using Vatara.WebAPI.Models;
using ZohoSubscription;
using static ZohoSubscription.CustomerApi;

namespace Vatara.WebAPI.Helper
{
    public class clszohoHelper
    {

        public ZohoProfile CreateCustomer(VataraEntities ve,MdlPersonRegistraion mdlPersonReg,int PMID)
        {
            CustomerApi customerApi = new CustomerApi();
            RequestContact Customer = new RequestContact();
            ZohoProfile mdlZohoProfile = new ZohoProfile();
            string customer_id = string.Empty;
            try
            {
                Customer.company_name = "";
                Customer.currency_code = "USD";
                Customer.display_name = mdlPersonReg.firstName + " " + mdlPersonReg.lastName;
                Customer.email = mdlPersonReg.email;
                Customer.first_name = mdlPersonReg.firstName;
                Customer.last_name = mdlPersonReg.lastName;
                Customer.mobile = mdlPersonReg.phoneNo;
                var customerdata = customerApi.CustomerExist(Customer.email);
                if (customerdata == false)
                {
                    customer_id = customerApi.Create(Customer);
                    if (!string.IsNullOrEmpty(customer_id))
                    {
                        mdlZohoProfile = new ZohoProfile();
                        mdlZohoProfile.CustomerId = customer_id;
                        mdlZohoProfile.CompanyName ="";
                        mdlZohoProfile.CreationTime =DateTime.UtcNow;
                        mdlZohoProfile.CreationUserId = PMID;
                        mdlZohoProfile.CurrencyCode = "USD";
                        mdlZohoProfile.DisplayName = mdlPersonReg.firstName + " " + mdlPersonReg.lastName;
                        mdlZohoProfile.Email = mdlPersonReg.email;
                        mdlZohoProfile.FirstName = mdlPersonReg.firstName;
                        mdlZohoProfile.IsDeleted =false;
                        mdlZohoProfile.LastName = mdlPersonReg.lastName;
                        mdlZohoProfile.PMID = PMID;
                        mdlZohoProfile.Phone =mdlPersonReg.phoneNo;
                        ve.ZohoProfiles.Add(mdlZohoProfile);
                        ve.SaveChanges();
                    }
                }
                return mdlZohoProfile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Customer = null;
                customerApi = null;
                mdlZohoProfile = null;
            }

            
        }
    }
}