﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Vatara.DataAccess;

namespace Vatara.WebAPI
{
    public class clsPropertyHelper
    {
        public int SaveProperty(MdlPropertyRegistration tt_PropertyRegistrationMdl)
        {
            int result = 0;
            DataTable dtMdlOwner = null;
            DataTable dtMdlOwnerMaster = null;
            DataTable dtMdlPropertyMap = null;
            clsCommon objClsCommon = new clsCommon();
            List<MdlOwnerMaster> lstMdlOwnerMaster = new List<MdlOwnerMaster>();
            List<MdlPropertyMap> lstMdlPropertyMap = new List<MdlPropertyMap>();
            SqlConnection con = null;
            try
            {
                lstMdlOwnerMaster.Add(tt_PropertyRegistrationMdl.OwnerMaster);
                lstMdlPropertyMap.Add(tt_PropertyRegistrationMdl.PropertyMap);

                dtMdlOwner = new clsCommon().ConvertToDataTable(tt_PropertyRegistrationMdl.lstMdlOwner);
                dtMdlOwnerMaster = new clsCommon().ConvertToDataTable(lstMdlOwnerMaster);
                dtMdlPropertyMap = new clsCommon().ConvertToDataTable(lstMdlPropertyMap);

                con = new SqlConnection(clsCommon.VataraConnectionString);

                using (SqlCommand cmd = new SqlCommand("savePropertyAndOwner", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tbl_Property", dtMdlPropertyMap);
                    cmd.Parameters.AddWithValue("@tbl_OwnerMaster", dtMdlOwnerMaster);
                    cmd.Parameters.AddWithValue("@tbl_Owner", dtMdlOwner);
                    cmd.Parameters.AddWithValue("creatorId", tt_PropertyRegistrationMdl.creatorId);
                    cmd.Parameters.Add("@output", SqlDbType.Int);
                    cmd.Parameters["@output"].Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    result = (int)cmd.Parameters["@output"].Value;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con = null;
                dtMdlOwner = null;
                dtMdlOwnerMaster = null;
                dtMdlPropertyMap = null;
                objClsCommon = null;
                lstMdlOwnerMaster = null;
                lstMdlPropertyMap = null;
            }
            return result;
        }


        public int UpdateProperty(MdlPropertyRegistration tt_PropertyRegistrationMdl)
        {
            int result = 0;
            DataTable dtMdlOwner = null;
            DataTable dtMdlOwnerMaster = null;
            DataTable dtMdlPropertyMap = null;
            clsCommon objClsCommon = new clsCommon();
            List<MdlOwnerMaster> lstMdlOwnerMaster = new List<MdlOwnerMaster>();
            List<MdlPropertyMap> lstMdlPropertyMap = new List<MdlPropertyMap>();
            SqlConnection con = null;
            try
            {
                lstMdlOwnerMaster.Add(tt_PropertyRegistrationMdl.OwnerMaster);
                lstMdlPropertyMap.Add(tt_PropertyRegistrationMdl.PropertyMap);

                dtMdlOwner = new clsCommon().ConvertToDataTable(tt_PropertyRegistrationMdl.lstMdlOwner);
                dtMdlOwnerMaster = new clsCommon().ConvertToDataTable(lstMdlOwnerMaster);
                dtMdlPropertyMap = new clsCommon().ConvertToDataTable(lstMdlPropertyMap);

                con = new SqlConnection(clsCommon.VataraConnectionString);

                using (SqlCommand cmd = new SqlCommand("SP_UpdatePropertyAndOwner", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tbl_Property", dtMdlPropertyMap);
                    cmd.Parameters.AddWithValue("@tbl_OwnerMaster", dtMdlOwnerMaster);
                    cmd.Parameters.AddWithValue("@tbl_Owner", dtMdlOwner);
                    cmd.Parameters.AddWithValue("creatorId", tt_PropertyRegistrationMdl.creatorId);
                    cmd.Parameters.Add("@output", SqlDbType.Int);
                    cmd.Parameters["@output"].Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    var v = cmd.Parameters["@output"].Value;
                     result = (int)cmd.Parameters["@output"].Value;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con = null;
                dtMdlOwner = null;
                dtMdlOwnerMaster = null;
                dtMdlPropertyMap = null;
                objClsCommon = null;
                lstMdlOwnerMaster = null;
                lstMdlPropertyMap = null;
            }
            return result;
        }



        public int UpdatePropertyAdditionalInfo(MdlPropertyAdditionalInfo MdlPropAdditionalInfo)
        {
            int result = 0;
           
            DataTable dtMdlPropertyMap = null;
            clsCommon objClsCommon = new clsCommon();           
            List<MdlPropertyMap> lstMdlPropertyMap = new List<MdlPropertyMap>();
            SqlConnection con = null;
            try
            {               
                lstMdlPropertyMap.Add(MdlPropAdditionalInfo.PropertyMap);
                dtMdlPropertyMap = new clsCommon().ConvertToDataTable(lstMdlPropertyMap);
                con = new SqlConnection(clsCommon.VataraConnectionString);

                using (SqlCommand cmd = new SqlCommand("Sp_UpdatePropertyAdditionalInfo", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tbl_Property", dtMdlPropertyMap);
                    cmd.Parameters.AddWithValue("creatorId", MdlPropAdditionalInfo.creatorId);
                    //cmd.Parameters.Add("@output", SqlDbType.Int);
                    //cmd.Parameters["@output"].Direction = ParameterDirection.Output;
                    con.Open();
                    result = cmd.ExecuteNonQuery();
                    con.Close();
                   // result = (int)cmd.Parameters["@output"].Value;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                con = null;               
                dtMdlPropertyMap = null;
                objClsCommon = null;               
                lstMdlPropertyMap = null;
            }          
        }


        public DataSet GetPropertyDetailById(int propertyId)
        {        
            using (SqlConnection conn = new SqlConnection(clsCommon.VataraConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Sp_GetPropertyWithOwnerMasterAndOwner";
                    cmd.Parameters.AddWithValue("@propertyId", propertyId);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);
                        return dataset;
                    }
                }
            }           
        }

        public DataTable GetOwnerByPOID(int POID)
        {
            using (SqlConnection conn = new SqlConnection(clsCommon.VataraConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Sp_GetOwnerByPOID";
                    cmd.Parameters.AddWithValue("@POID", POID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        // Fill the DataTable using default values for DataTable names, etc
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }

    }
}