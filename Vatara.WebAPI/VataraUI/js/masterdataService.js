var myApp = angular.module('Myapp');

myApp.service('masterdataService', function ($http, $rootScope) {

    // var _listOfStates = null;
    // var _listOfCities = null;
    // var _listOfCounty = null;
    this.getPropertyCategories = function (onComplete) {
        $http({ method: 'GET', url: '/api/propertycategory' })
            .success(function (data, status, headers, config) {
                //alert(JSON.stringify(data));
                $rootScope.PropertyCategoriesList = data;
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };



    this.getPropertyTypes = function (onComplete) {
        $http({ method: 'GET', url: '/api/propertytype' })
            .success(function (data, status, headers, config) {
                //alert(JSON.stringify(data));
                $rootScope.PropertyTypesList = data;
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getCountries = function (onComplete) {
        if ($rootScope.CountriesList == null) {
            $http({ method: 'GET', url: '/api/country/GetAllCountries' })
                .success(function (data, status, headers, config) {
                    //alert(JSON.stringify(data));
                    $rootScope.CountriesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.CountriesList);
        }
    }


    this.GetAllCountries = function (onComplete) {
       
        if ($rootScope.CountriesList == null) {
            $http({ method: 'GET', url: '/api/country/GetAllCountries' })
                .success(function (data, status, headers, config) {
                    //alert(JSON.stringify(data));
                    $rootScope.CountriesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.CountriesList);
        }
    }


    this.getStates = function (onComplete) {
        if ($rootScope.StatesList == null) {
            $http({ method: 'GET', url: '/api/state' })
                .success(function (data, status, headers, config) {
                    //alert(JSON.stringify(data));
                    $rootScope.StatesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.StatesList);
        }
    }

    this.getCities = function (onComplete) {
        if ($rootScope.CitiesList == null) {
            $http({ method: 'GET', url: '/api/city' })
                .success(function (data, status, headers, config) {
                    $rootScope.CitiesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.CitiesList);
        }
    }

    //this.getCitiesbyCounty = function (id,onComplete) {
    //    if ($rootScope.CitiesList == null) {
    //        $http({ method: 'GET', url: '/api/city/GetCity?id='+id })
    //            .success(function (data, status, headers, config) {
    //                $rootScope.CitiesList = data;
    //                onComplete(true, data);
    //            })
    //            .error(function (data, status, headers, config) {
    //                onComplete(false, status);
    //            });
    //    }
    //    else {
    //        onComplete(true, $rootScope.CitiesList);
    //    }
    //}



    this.getCitiesbyCounty = function (id) {
        var responce = $http({
            method: "Get",
            url: "/api/city/GetCityByCounty",
            params: {
                id: id
            },
            headers: { 'Content-Type': JSON }

        });
        $rootScope.CitiesList = responce;
        return responce;
    };





    this.getCounties = function (onComplete) {
        if ($rootScope.CountiesList == null) {
            $http({ method: 'GET', url: '/api/county' })
                .success(function (data, status, headers, config) {
                    $rootScope.CountiesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.CountiesList);
        }
    }

    this.getLeaseTypes = function (onComplete) {
        if ($rootScope.LeaseTypesList == null) {
            $http({ method: 'GET', url: '/api/leasetype' })
                .success(function (data, status, headers, config) {
                    $rootScope.LeaseTypesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.LeaseTypesList);
        }
    }

    this.getAgreementTypes = function (onComplete) {
        if ($rootScope.AgreementTypesList == null) {
            $http({ method: 'GET', url: '/api/agreementtype' })
                .success(function (data, status, headers, config) {
                    $rootScope.AgreementTypesList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.AgreementTypesList);
        }
    }

    this.getLeasePaymentFrequency = function (onComplete) {
        if ($rootScope.LeasePaymentFrequencyList == null) {
            $http({ method: 'GET', url: '/api/leasepaymentfrequency' })
                .success(function (data, status, headers, config) {
                    $rootScope.LeasePaymentFrequencyList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.LeasePaymentFrequencyList);
        }
    }


    this.getLeaseTerm = function (onComplete) {
        if ($rootScope.LeaseTypesList == null) {
            $http({ method: 'GET', url: '/api/leaseterm' })
                .success(function (data, status, headers, config) {
                    $rootScope.LeaseTermList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.LeaseTermList);
        }
    }

    this.getPerson = function (onComplete) {
        if (angular.isUndefined($rootScope.PersonList) || $rootScope.PersonList == null) {
            $http({ method: 'GET', url: '/api/person' })
                .success(function (data, status, headers, config) {
                    $rootScope.PersonList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.PersonList);
        }
    }

    this.getStatus = function (onComplete) {
        if (angular.isUndefined($rootScope.StatusList) || $rootScope.StatusList == null) {
            $http({ method: 'GET', url: '/api/status' })
                .success(function (data, status, headers, config) {
                    $rootScope.StatusList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.StatusList);
        }
    }
    
    this.getServiceCategory = function (onComplete) {
        if (angular.isUndefined($rootScope.ServiceCategoryList) || $rootScope.ServiceCategoryList == null) {
            $http({ method: 'GET', url: '/api/servicecategory' })
                .success(function (data, status, headers, config) {
                    $rootScope.ServiceCategoryList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.ServiceCategoryList);
        }
    }

    this.getStatus = function (onComplete) {
        if (angular.isUndefined($rootScope.StatusList) || $rootScope.StatusList == null) {
            $http({ method: 'GET', url: '/api/status' })
                .success(function (data, status, headers, config) {
                    $rootScope.StatusList = data;
                    onComplete(true, data);
                })
                .error(function (data, status, headers, config) {
                    onComplete(false, status);
                });
        }
        else {
            onComplete(true, $rootScope.StatusList);
        }
    }


    this.GetOwnerMasterByPOID = function (POID) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/GetOwnerMasterByPOID",
            params: {
                POID: POID
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };


    this.GetPersonOwnerByPMID = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/GetPersonOwnerByPMID",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    this.GetTenantByManagerId = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Person/GetTenantByManagerId",
            params: {
                managerId: PMID
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    this.GetCompanyByMangerID = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/GetCompanyByMangerID",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }

});

