//var myApp = angular.module('Myapp');

vataraApp.service('leaseService', function ($http) {

    this.leaseStartDate = new Date();
    this.leaseEndDate = new Date();

    //this.saveLease = function (leaseData, onComplete) {       
    //    $http({ method: 'post', data: leaseData, url: '/api/lease/Post_lease' })
    //        .success(function (data, status, headers, config) {               
    //            onComplete(true, data);
    //        })
    //        .error(function (data, status, headers, config) {               
    //            onComplete(false, status);
    //        });
    //};



    this.saveLease = function (leasemap) {
        
        var responce = $http.post(
            '/api/lease/Savelease',
            JSON.stringify(leasemap),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.saveFiles = function (files, url, onComplete) {

        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            //console.log(d);
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };


    this.getAllLeases = function (personId, onComplete) {

        $http({
            method: 'GET', url: '/api/lease/GetAllLease?id=' + personId
        })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getLeaseById = function (leaseId, onComplete) {
        $http({ method: 'GET', url: '/api/lease/GetLeaseById?leaseId=' + leaseId })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getLeaseByPropertyId = function (propertyId, onComplete) {
        $http({ method: 'GET', url: '/api/lease/GetLeaseByPropertyId?propertyId=' + propertyId })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };


    this.getLeaseTenants = function (pLeaseId, onComplete) {
        $http({ method: 'GET', url: '/api/leasetenants/GetLeaseById?id=' + pLeaseId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });

    };

    this.getLeaseDocuments = function (leaseId, onComplete) {
        $http({ method: 'GET', url: '/api/leasedocument/Get?id=' + leaseId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getLeaseClauses = function (leaseId, onComplete) {
        $http({ method: 'GET', url: '/api/leaseclause/GetLeaseClauseByLeaseId?id=' + leaseId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getLeaseOptions = function (leaseId, onComplete) {
        $http({ method: 'GET', url: '/api/leaseoption/GetLeaseOptionByLeaseId?id=' + leaseId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };


    this.getLeaseOccupants = function (leaseId, onComplete) {
        $http({ method: 'GET', url: '/api/lease/GetOccupantByLeaseId?leaseId=' + leaseId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.renewLease = function (data) {
        var responce = $http.post(
            '/api/Lease/RenewLease',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.terminateLease = function (data) {

        var responce = $http.post(
           '/api/Lease/TerminateLease',
           JSON.stringify(data)
        )
        return responce;      
    };


    this.getLeaseType = function (propertyCategoryId) {
        var responce = $http({
            method: "Get",
            url: "/api/Lease/GetLeaseType",
            params: {
                propertyCategoryId: propertyCategoryId                
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }

});

