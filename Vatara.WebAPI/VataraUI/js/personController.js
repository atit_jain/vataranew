
vataraApp.controller('personController', function ($scope, $http, masterdataService, personService, $routeParams, $location, $rootScope) {

    $scope.pTypeName = 'InValid Person Type';
    var countrieslist = [{}];
    var stateslist = [{}];
    var citieslist = [{}];
    var countieslist = [{}];
    var statuslist = [{}];
    var servicecategorylist = [{}];
    var personlist = [{}];
    var submitted = false;

    $scope.Photo = '';

    $scope.propertieslist = [{}];
    $scope.pId = 0;
    $scope.pServiceCategorySelected = '';
    $scope.pServiceCategory = '';
    $scope.pServiceCategoryId = 0;
    $scope.pCompanyName = '';
    $scope.pFirstName = ''
    $scope.pLastName = '';
    $scope.pMailBoxNo = ''
    $scope.pAddress = '';
    $scope.pCountry = '';
    $scope.pCountryId = 0;
    $scope.pState = '';
    $scope.pStateId = 0;
    $scope.pCounty = '';
    $scope.pCountyId = 0;
    $scope.pCity = '';
    $scope.pCityId = 0;
    $scope.pZipcode = '';
    $scope.pEmail = '';
    $scope.pPhoneNo = '';
    $scope.pFaxNo = '';
    $scope.pTwitterUrl = '';
    $scope.pFBUrl = '';
    $scope.pStatus = '';
    $scope.pStatusId = 0;


    $scope.initializePage = function () {
        //$scope.pTypeId = 2;
        $scope.pTypeId = $routeParams.PersonType;
        if ($scope.pTypeId != -1) {
            switch (parseInt($scope.pTypeId)) {
                case 1:
                    $scope.pTypeName = "Property Managers";
                    break;
                case 2:
                    $scope.pTypeName = "Owners";
                    break;
                case 3:
                    $scope.pTypeName = "Tenants";
                    break;
                //case 4:
                //    $scope.pTypeName = "Vendors";
                //    break;
                //case 5:
                //    $scope.pTypeName = "Associations";
                //break;
            
                default:
                    break;
            }
        }
        $scope.getPerson($scope.pTypeId, false);

        if (angular.isUndefined($rootScope.ServiceCategoryList) || $rootScope.ServiceCategoryList == null) {
            masterdataService.getServiceCategory(function (result, data) {
                if (result) {
                    $scope.servicecategorylist = $rootScope.ServiceCategoryList;

                    for (var i = 0; i < $scope.servicecategorylist.length; i++) {
                        if ($scope.pServiceCategoryId == $scope.servicecategorylist[i].Id) {
                            $scope.pServiceCategory = $scope.servicecategorylist[i];
                            break;
                        }
                    }                            
                }
                else {
                    swal("ERROR", 'Unable to fetch service category', "error");                    
                }
            });
        }
        else {
            $scope.servicecategorylist = $rootScope.ServiceCategoryList;
			for (var i = 0; i < $scope.servicecategorylist.length; i++) {
				if ($scope.pServiceCategoryId == $scope.servicecategorylist[i].Id) {
					$scope.pServiceCategory = $scope.servicecategorylist[i];
					break;
				}
			}                            
        }


        if (angular.isUndefined($rootScope.StatusList) || $rootScope.StatusList == null) {
            masterdataService.getStatus(function (result, data) {
                if (result) {
                    $scope.statuslist = $rootScope.StatusList.filter(function (c) {
                        return c.ModuleName == 'Person';
                    });

                    for (var i = 0; i < $scope.statuslist.length; i++) {
                        if ($scope.pStatusId == $scope.statuslist[i].Id) {
                            $scope.pStatus = $scope.statuslist[i];
                            break;
                        }
                    }                            
                }
                else {
                    swal("ERROR", 'Unable to fetch status', "error");                    
                }
            });
        } else {
                    $scope.statuslist = $rootScope.StatusList.filter(function (c) {
                        return c.ModuleName == 'Person';
                    });
			for (var i = 0; i < $scope.statuslist.length; i++) {
				if ($scope.pStatusId == $scope.statuslist[i].Id) {
					$scope.pStatus = $scope.statuslist[i];
					break;
				}
			}                            
        }


        if (angular.isUndefined($rootScope.CountriesList) || $rootScope.CountriesList == null) {
            masterdataService.getCountries(function (result, data) {
                if (result) {
                    $scope.countrieslist = data; //angular.toJson(data, true);;
                    for (var i = 0; i < $scope.countrieslist.length; i++) {
                        if ($scope.pCountryId == $scope.countrieslist[i].Id) {
                            $scope.pCountry = $scope.countrieslist[i];
                            break;
                        }
                    }                            
                }
                else {
                    swal("ERROR", "Unable to fetch countries", "error");                   
                }
            });
        } else {
            $scope.countrieslist = $rootScope.CountriesList;
			for (var i = 0; i < $scope.countrieslist.length; i++) {
				if ($scope.pCountryId == $scope.countrieslist[i].Id) {
					$scope.pCountry = $scope.countrieslist[i];
					break;
				}
			}                            
        }
        if (angular.isUndefined($rootScope.StatesList) || $rootScope.StatesList == null) {
            masterdataService.getStates(function (result, data) {
                if (result) {
                    $scope.stateslist = data; //angular.toJson(data, true);;
                    for (var i = 0; i < $rootScope.StatesList.length; i++) {
                        if ($scope.pStateId == $rootScope.StatesList[i].Id) {
                            $scope.pState = $rootScope.StatesList[i];
                            break;
                        }
                    }                            
                }
                else {
                    swal("ERROR", "Unable to fetch states", "error");                    
                }
            });
        } else {
            $scope.stateslist = $rootScope.StatesList;
            for (var i = 0; i < $rootScope.StatesList.length; i++) {
                if ($scope.pState == $rootScope.StatesList[i].Name) {
                    $scope.pState = $rootScope.StatesList[i];
                    break;
                }
            }
        }
        
        if (angular.isUndefined($rootScope.CitiesList) || $rootScope.CitiesList == null) {
            masterdataService.getCities(function (result, data) {
                if (result) {
                    $scope.citieslist = $rootScope.CitiesList.filter(function (c) {
                        return c.StateId == $scope.pStateId;
                    });
                    //alert('test city 1: ' + $rootScope.CitiesList.length + ' .... ' + $scope.citieslist.length);
                    for (var i = 0; i < $rootScope.CitiesList.length; i++) {
                        
                        if ($scope.pCityId == $rootScope.CitiesList[i].Id) {
                            $scope.pCity = $rootScope.CitiesList[i];
                            break;
                        }
                    }                    
                }
                else {
                    swal("ERROR", "Unable to fetch cities", "error");                   
                }
            });
        } else {
            // $scope.citieslist = $rootScope.CitiesList;
            $scope.citieslist = $rootScope.CitiesList.filter(function (c) {
                return c.StateId == $scope.pStateId;
            });
            //alert('test city 2: ' + $scope.citieslist.length);
            if (angular.isUndefined($scope.citieslist)) {
                for (var i = 0; i < $scope.citieslist.length; i++) {
                    
                    if ($scope.pCityId == $scope.citieslist[i].Id) {
                        $scope.pCity = $scope.citieslist[i];
                        break;
                    }
                }
            }
        }

        if (angular.isUndefined($rootScope.CountiesList) || $rootScope.CountiesList == null) {
            masterdataService.getCounties(function (result, data) {
                if (result) {
                    //$scope.countieslist = data; //angular.toJson(data, true);;
                    $scope.countieslist = $rootScope.CountiesList.filter(function (c) {
                        return c.StateId == $scope.pStateId;
                    });

                    for (var i = 0; i < $rootScope.CountiesList.length; i++) {
                        if ($scope.pCountyId == $rootScope.CountiesList[i].Id) {
                            $scope.pCounty = $rootScope.CountiesList[i];
                            break;
                        }
                    }                            
                }
                else {
                    swal("ERROR", "Unable to fetch counties", "error");                   
                }
            });
        } else {
            //$scope.countieslist = $rootScope.CountiesList;
            $scope.countieslist = $rootScope.CountiesList.filter(function (c) {
                return c.StateId == $scope.pStateId;
            });
            
            for (var i = 0; i < $rootScope.CountiesList.length; i++) {
                if ($scope.pCountyId == $rootScope.CountiesList[i].Id) {
                    $scope.pCounty = $rootScope.CountiesList[i];
                    break;
                }
            }
        }

    }

    $scope.$watch('pState', function () {
        // $scope.cities = {};
        // $scope.counties = {};
        
        if (!angular.isUndefined($scope.pState)) {
            if (!angular.isUndefined($rootScope.CitiesList)) {
                $scope.citieslist = $rootScope.CitiesList.filter(function (c) {
                    return c.StateId == $scope.pState.Id;
                });
                if (!angular.isUndefined($scope.citieslist)) {
                    //alert('cities: ' + $scope.citieslist.length);
                    for (var i = 0; i < $scope.citieslist.length; i++) {
                        
                        if ($scope.pCityId == $scope.citieslist[i].Id) {
                            $scope.pCity = $scope.citieslist[i];
                            break;
                        }
                    }
                }
            }
            if (!angular.isUndefined($rootScope.CountiesList)) {
                $scope.countieslist = $rootScope.CountiesList.filter(function (c) {
                    return c.StateId == $scope.pState.Id;
                });
                for (var i = 0; i < $scope.countieslist.length; i++) {
                    if ($scope.pCountyId == $scope.countieslist[i].Id) {
                        $scope.pCounty = $scope.countieslist[i];
                        break;
                    }
                }
                }
        }
    });

    $scope.objPerson = {
        pTypeId: '0',
        pServiceCategoryId : '',
        pCompanyName : '',
        pFirstName : '',
        pLastName : '',
        pStatusId: '',
        pMailBoxNo : '',
        pAddressId : '',
        pAddress : '',
        pCountryId : '',
        pStateId : '',
        pCountyId : '',
        pCityId : '',
        pZipcode : '',
        pEmail : '',
        pPhoneNo : '',
        pFaxNo : '',
        pTwitterUrl : '',
        pFBUrl : '',
        pVendorServiceCategoriesArray: [],
    }

    $scope.savePerson = function () {
        submitted = true;
        if (!$scope.personForm.$valid) {
            alert('Please fix the validation errors, save cancelled.');
            return;
        }
        $scope.objPerson.pId = $scope.pId;
        $scope.objPerson.pTypeId = $scope.pTypeId;
        $scope.objPerson.pServiceCategory = $scope.pServiceCategory;
        $scope.objPerson.pCompanyName = $scope.pCompanyName;
        $scope.objPerson.pFirstName = $scope.pFirstName;
        $scope.objPerson.pLastName = $scope.pLastName;
        $scope.objPerson.pStatusId = $scope.pStatus.Id;
        $scope.objPerson.pMailBoxNo = $scope.pMailBoxNo;
        $scope.objPerson.pAddressId = $scope.pAddressId;
        $scope.objPerson.pAddress = $scope.pAddress;
        $scope.objPerson.pCountryId = $scope.pCountry.Id;
        $scope.objPerson.pStateId = $scope.pState.Id;
        $scope.objPerson.pCountyId = $scope.pCounty.Id;
        $scope.objPerson.pCityId = $scope.pCity.Id;
        $scope.objPerson.pZipcode = $scope.pZipcode;
        $scope.objPerson.pEmail = $scope.pEmail;
        $scope.objPerson.pPhoneNo = $scope.pPhoneNo;
        $scope.objPerson.pFaxNo = $scope.pFaxNo;
        $scope.objPerson.pTwitterUrl = $scope.pTwitterUrl;
        $scope.objPerson.pFBUrl = $scope.pFBUrl;

        // $scope.pServiceCategory = any s in $scope.servicecategorylist;
        // $scope.selectedUser.id == (any id in $scope.users.map(function(u) { return u.id; }));
        $scope.objPerson.pVendorServiceCategoriesArray = $scope.pServiceCategory;
    

        $scope.personjson = angular.toJson($scope.objPerson, true);
            
        personService.savePerson($scope.objPerson, function (status, resp) {
            if (status) {
                $scope.pId = resp.Id;
                $scope.savePhotos();
            }
            else {
                alert('Unable to add new contact.');
            }
        });



    };


    //////////////////////////////
    $scope.getPerson = function (personTypeId, bGetSpecificInfo) {
        $scope.pTypeId = personTypeId;
        personService.getPerson(function (status, data) {
            if (status)  {
                $scope.personlist = data.filter(function (c) {
                    return c.pTypeId == parseInt(personTypeId);
                });
                if (bGetSpecificInfo)
                {
                    $scope.getOwner();
                }
            }
            else {
                alert('error in getPerson');            
            }
        });

    }

    $scope.ownerId = 0;
    $scope.getOwner = function () {
        $scope.ownerId = $routeParams.PersonId;
        $scope.owner = $scope.personlist.filter(function (c) {
            return c.pId == parseInt($scope.ownerId);
        });


        $scope.pLastName = $scope.owner[0].pLastName;
        $scope.pFirstName = $scope.owner[0].pFirstName;
        $scope.pAddress = $scope.owner[0].pAddress;
        $scope.pStateName = $scope.owner.pStateName;
        $scope.pCItyName = $scope.owner[0].pCItyName;
        $scope.pCountyName = $scope.owner[0].pCountyName;
        $scope.pZipcode = $scope.owner.pZipcode;
        $scope.pEmail = $scope.owner[0].pEmail;
        $scope.pFaxNo = $scope.owner[0].pFaxNo;
        $scope.pPhoneNo = $scope.owner[0].pPhoneNo;

        $scope.getProperties($scope.ownerId);

        if ($scope.ownerId > 0) {

            personService.getPersonPhotos($scope.ownerId, function (result, data) {
                if (result) {
        
                    for (let index = 1; index <= data.length; index++) {
                        var fd = new FormData();
        
                        fd.append("file", $scope.Photos);
                        fd.append("propid", $scope.ownerId);
                        fd.append("seqNo", index);
                        fd.append("filename", 'image'+index);                
                        fd.append("filedesc", '');
                        fd.append("filetags", 'Photo, ' + $scope.ownerId);  
                        $scope.Photos = fd;
        
                        output = document.getElementById('divimage');
                        if (output != null) {
                            output.src = '/Uploads/Person/Photos/' + $scope.ownerId + '-image'+ index.toString() +'.png';
                        }
        
                    }
                }
                else {
                    alert('Unable to fetch photos for person.')
                }
            });
        

        }
    }

    $scope.getProperties = function (personId){

        personService.getProperties(personId,  function (status, data) {
            if (status)  {

                $scope.propertieslist = data;
        
            }
            else {
                alert('error in getProperties');            
            }
        });
    };
    
    $scope.fileSelected = function (element) {
        var myFileSelected = element.files[0];
        
        $scope.Photo = myFileSelected;
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('div'+element.name);
            if (output != null)
                output.style.backgroundImage = 'url(' + reader.result +')';
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    $scope.savePhotos = function(isClose) {
        var fd = new FormData();
        fd.append("file", $scope.Photo);
        fd.append("personid", $scope.pId);
        fd.append("moduleid", 'PERSON');
        fd.append("seqNo", 0);
        fd.append("filename", 'image0');                
        fd.append("filedesc", '');
        fd.append("filetags", 'Photo, ' + $scope.pId);                    
        personService.saveFiles(fd, '/api/personphoto', function (status, resp) {
            if (status) {
                swal("SUCCESS", "Image saved successfully.", "success");                
            }
            else {
                swal("ERROR", "Unable to save image.", "error");               
            }
        });
    }
    




});	
