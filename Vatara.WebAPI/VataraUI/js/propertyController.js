//var propertyapp = angular.module('Myapp');
//var propertyapp = angular.module('Myapp', ['ngMaterial', 'ngMessages', 'ngAnimate']);

vataraApp.controller('propertiesController', function ($scope, $http, propertyService, $routeParams, $location, $rootScope,
    $filter, personService, masterdataService, mainFactoryService, ownerService, leaseService, financeService, pagingService,
    commonService, propayRegService) {
    $scope.pId = 0;
    var propertycategorieslist = [{}];
    var propertytypeslist = [{}];
    var propertytypesfilteredlist = [{}];
    var countrieslist = [{}];
    var stateslist = [{}];
    var allcitieslist = [{}];
    var citieslist = [{}];
    var allcountieslist = [{}];
    var countieslist = [{}];
    $scope.randomNumber = Math.floor(100000 + Math.random() * 90000000);
    $scope.ownerlist = [{}];
    $scope.hoalist = [{}];
    $scope.selectPropertyModel = {};
    // for lease tab.
    $scope.leasesjson = '';
    $scope.leaseslist = [{}];
    $scope.leasetenantslist = [{}];
    $scope.PropertyHasActiveLease = false;
    $scope.tenantEmail = '';
    $scope.MdlHoaDetail = {
        id: 0,
        pmid: $rootScope.userData.ManagerDetail.pId,
        hoaName: '',
        personFirstName: '',
        personLastName: '',
        email: '',
        phone: '',
        creatorId: $rootScope.userData.personId
    }

    $scope.popUp = {
        Heading: 'Delete Property',
        Message: 'Are you sure you want to archive this property?'
    };

    $scope.pAddNewOwner = false;
    $scope.pAddNewCompany = false;

    $scope.isPersonEmailExist = false;
    $scope.isOwnerMasterEmailExist = false;

    $scope.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;      //$rootScope.emailAddessPattern
    $scope.ManagementFeeType = ['Percentage', 'Amount'];

    $scope.mdlHoaPaymentFreq = ['Monthly ', 'Quarterly'];

    $scope.mdlPropertyStatus = ['Owned ', 'Leased'];

    $scope.pManagementFeeType = 'Percentage',
    $scope.pManagementFee = '';

    $scope.MdlPropertyAdditionalInfo = {
        PropertyMap: {},
        creatorId: $rootScope.userData.personId
    };

    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};
    $scope.mdlEventListByPropertyId = {}
    $scope.MdlPropertyOwnerDetail = {

        OwnerMaster: {},
        lstMdlOwner: [],
        Ownertype: '',
        creatorId: $rootScope.userData.personId
    };

    $scope.OwnerMaster = {

        POID: 0,
        CompanyName: '',
        Email: '',
        ContactPerson: '',
        ContactNum: '',
    };

    $scope.lstOwners = [];

    $scope.MdlOwner = {
        Id: 0,
        POID: 0,
        pFirstName: '',
        pLastName: '',
        pEmail: '',
        pPhoneNo: '',
        Isprimary: false,
        showRemove: true
    };

    $scope.Ownertype = 'individual';

    $scope.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    $scope.objProperty = {};
    $scope.ownerArray = [];
    $scope.pOwner = '';

    // $scope.pPropertyCategory = '';
    $scope.pPropertyCategoryFilter = [{}];
    $scope.pPropertyCategoryFilterId = 0;

    $scope.showState = true;
    $scope.showCity = true;
    $scope.showCounty = true;

    $scope.MdlPropertyRegistration = {

        OwnerMaster: {},
        lstMdlOwner: [],
        Ownertype: '',
        creatorId: $rootScope.userData.personId,
        IsNewOwner: false,
        PropertyMap: {}
    }

    // Finance tab 

    $scope.menuActive = 'Financial Summary';
    $scope.PageService = pagingService;
    $scope.selectCurrentPeriod = "";
    $scope.fromDate = new Date();
    $scope.toDate = new Date();
    $scope.ViewIncomeCategory = 'Income';
    $scope.IncmChartArray = [];
    $scope.EarnedIncmChartArray = [];
    $scope.PendingIncmChartArray = [];
    $scope.LiabitilyChartArray = [];
    $scope.IncomeCategory = [];

    $scope.pHomeWarrantyStartDate = new Date();
    $scope.pHomeWarrantyEndDate = new Date();
    $scope.pHomeInsuranceStartDate = new Date();
    $scope.pHomeInsuranceEndDate = new Date();

    $scope.fromDate1 = new Date();
    $scope.toDate1 = new Date();

    $scope.lstTransections = [];
    $scope.lstTransections_Filtered = [];

    $scope.managerid = $rootScope.userData.personId;
    $scope.pmid = $rootScope.userData.ManagerDetail.pId;
    $scope.personId = 0;
    $scope.propoertyId = 0;
    $scope.personType = 0;

    $scope.isManager = true;
    var currentDates = new Date();
    $scope.TransectionFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    }
    $scope.lstPropertOwnersByManager = [];
    $scope.CopylstPropertOwnersByManager = [];

    $scope.lstPropertByManagerAndOwner = [];
    $scope.CopylstPropertByManagerAndOwner = [];

    $scope.lstTransections_OrderBy = "";
    $scope.currentPage_lstTransections = 0;

    $scope.groupByCategory = {};
    $scope.PieChartCategoryArray = [];
    $scope.IncmExpenseChartArray = [];

    $scope.viewPropertyId = 0;
    $scope.propertyLeaseList = [];
    $scope.ShowActiveLease = true;

    $scope.personId = 0;
    $scope.propoertyId = 0;
    $scope.personType = 0;
    $scope.current_toDate = new Date();
    $scope.select_fromDate = new Date();

    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = true;

    $scope.isAllPropertySelected = true;

    $scope.CheckvalueAcc = function () {
        $("#property_additionalinfo2").accordion("disable");
    }
    //$("#disable").click(function ()
    //{
    //    //disable the accordion
    //    $("#myAccordion").accordion("disable");
    //});
    $scope.pCompany = '';
    $scope.ChangeZip = function () {

        $scope.pZipCode = '';
        $scope.pZipValid = true;
    };

    $scope.SetCityModel = function (modal) {
        $scope.pCity = modal;
        $scope.ChangeZip();
    };

    $scope.OwnerChanged = function (modal) {
        $scope.pOwner = angular.copy(modal);
    };

    $scope.FeeTypeChange = function (pManagementFeeType) {
        if (pManagementFeeType == "Percentage" && parseFloat($scope.pManagementFee) > 100) {
            $scope.pManagementFee = 0;
        }
    };

    $scope.CancelHOA = function (addHOAForm) {
        $scope.ResetMdlHoaDetail();
        addHOAForm.$setPristine();
        $('#mdlAddHOA').modal('hide');
    }
    $scope.ResetMdlHoaDetail = function () {

        $scope.MdlHoaDetail.id = 0;
        $scope.MdlHoaDetail.pmid = $rootScope.userData.ManagerDetail.pId;
        $scope.MdlHoaDetail.hoaName = '';
        $scope.MdlHoaDetail.personFirstName = '';
        $scope.MdlHoaDetail.personLastName = '';
        $scope.MdlHoaDetail.email = '';
        $scope.MdlHoaDetail.phone = '';
        $scope.MdlHoaDetail.creatorId = $rootScope.userData.personId;
    }


    $scope.GetHOAByPMID = function (callback) {

        $scope.showLoader = true;
        // $scope.lstOwners = [];
        propertyService.getHOAByPMID($rootScope.userData.ManagerDetail.pId).
          success(function (response) {
              $scope.showLoader = false;
              if (response.Status == true) {
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              $scope.showLoader = false;
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }


    $scope.CheckHOAEmailAvailability = function (email) {

        if (email == undefined || email == '' || email == null) {
            return;
        }
        $scope.showLoader = true;
        propertyService.CheckEmailAvailability(email).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.isHOAEmailExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.AddNewHOA = function (addHOAForm) {
        $scope.showLoader = true;
        propertyService.SaveHOA($scope.MdlHoaDetail).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   swal("SUCCESS", "HOA details saved successfully.", "success");
                   $scope.GetHOAByPMID(function (data) {
                       $scope.hoalist = [];
                       $scope.hoalist = data;
                   });
                   $scope.CancelHOA(addHOAForm);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    $scope.saveProperty = function (isClose, isShowTab2) {

        if (parseInt($scope.pGrossArea, 10) > 0) {
            if (parseInt($scope.pRentableArea, 10) > parseInt($scope.pGrossArea, 10)) {
                swal("Alert !", "Rentable area cannot greater than Gross Area, save cancelled", "info");
                return;
            }
        }
        $scope.showLoader = true;

        $scope.MdlPropertyRegistration.OwnerMaster = angular.copy($scope.OwnerMaster);
        $scope.MdlPropertyRegistration.lstMdlOwner = angular.copy($scope.lstOwners);
        $scope.MdlPropertyRegistration.Ownertype = $scope.Ownertype;

        if ($scope.Ownertype == "individual") {
            //************* IF owner type is individual then the primary owner data will be taken as Owner Master data ************

            angular.forEach($scope.lstOwners, function (value, key) {

                if (value.Isprimary == true) {
                    //value.Isprimary = true;
                    $scope.MdlPropertyRegistration.OwnerMaster.CompanyName = value.pLastName + " " + value.pFirstName;
                    $scope.MdlPropertyRegistration.OwnerMaster.ContactPerson = value.pLastName + " " + value.pFirstName;
                    $scope.MdlPropertyRegistration.OwnerMaster.Email = value.pEmail;
                    $scope.MdlPropertyRegistration.OwnerMaster.ContactNum = value.pPhoneNo;
                }
            });
        }
        $scope.objProperty.pManagerId = $rootScope.userData.personId;

        $scope.objProperty.pId = $scope.pId;
        $scope.objProperty.pNum = $scope.pNum;
        $scope.objProperty.pName = $scope.pName;
        $scope.objProperty.pCategory = $scope.pPropertyCategory.Id;
        $scope.objProperty.pType = $scope.pPropertyType.Id;

        $scope.objProperty.pMailBoxNo = $scope.pMailBoxNo;
        $scope.objProperty.pAddressId = $scope.pAddressId;
        $scope.objProperty.pAddress = $scope.pAddress;
        $scope.objProperty.pState = $scope.pState.Name;
        $scope.objProperty.pStateId = $scope.pState.Id;
        $scope.objProperty.pCity = $scope.pCity.Name;
        $scope.objProperty.pCityId = $scope.pCity.Id;

        if ($scope.pCounty == null) {
            $scope.objProperty.pCounty = '';
            $scope.objProperty.pCountyId = 0;
        } else {
            $scope.objProperty.pCounty = $scope.pCounty.Name;
            $scope.objProperty.pCountyId = $scope.pCounty.id;
        }
        $scope.objProperty.pZipCode = $scope.pZipCode;
        $scope.objProperty.pCountryId = $scope.pCountry.Id;
        $scope.objProperty.pCountry = $scope.pCountry.Name;
        $scope.objProperty.pPhone = $scope.pPhone;
        $scope.objProperty.pGrossArea = $scope.pGrossArea;
        $scope.objProperty.pRentableArea = $scope.pRentableArea;
        $scope.objProperty.pParkingSpaces = $scope.pParkingSpaces;
        $scope.objProperty.pPropertyTax = $scope.pPropertyTax;


        //********************************
        $scope.objProperty.pHoaAmount = $scope.pHoaAmount;

        $scope.objProperty.pHoaDate = (new Date($scope.pHoaDate)).getUTCDate();
        $scope.objProperty.HOAPaymentFrequency = $scope.HOAPaymentFrequency;

        $scope.objProperty.pManagementFee = $scope.pManagementFee;
        $scope.objProperty.pManagementFeeType = $scope.pManagementFeeType;

        //********************************

        $scope.objProperty.pUnitNo = $scope.pUnitNo;
        $scope.objProperty.pBaseRent = $scope.pBaseRent;

        if (!angular.isUndefined($scope.pHOA) && $scope.pHOA != null)
            $scope.objProperty.pHOAId = $scope.pHOA.id;
        else
            $scope.objProperty.pHOAId = 0;

        $scope.objProperty.pOwnerArray = $scope.ownerArray;
        for (let index = 0; index < $scope.ownerArray.length; index++) {
            if ($scope.ownerArray[index].isprimaryowner == "1")
                $scope.ownerArray[index].isprimaryowner = true;
        }

        $scope.objProperty.pDescription = $scope.pDescription;
        $scope.objProperty.pAirportName = $scope.pAirportName;
        $scope.objProperty.pAirportDistance = $scope.pAirportDistance;
        $scope.objProperty.pUOM = $scope.pUOM;
        $scope.objProperty.pListType = $scope.pListType;
        $scope.objProperty.pComments = $scope.pComments;
        $scope.objProperty.pZoning = $scope.pZoning;
        $scope.objProperty.pLandAcres = $scope.pLandAcres;
        $scope.objProperty.pCustomField1 = $scope.pCustomField1;
        $scope.objProperty.pIsHomeWarrantyTaken = $scope.pIsHomeWarrantyTaken;
        $scope.objProperty.pHomeWarrantyCompanyName = $scope.pHomeWarrantyCompanyName;
        $scope.objProperty.pHomeWarrantyCompanyPhoneNo = $scope.pHomeWarrantyCompanyPhoneNo;
        $scope.objProperty.pHomeWarrantyStartDate = $scope.pHomeWarrantyStartDate;
        $scope.objProperty.pHomeWarrantyEndDate = $scope.pHomeWarrantyEndDate;
        $scope.objProperty.pHomeWarrantySendReminder = $scope.pHomeWarrantySendReminder;

        if ($scope.pIsHomeInsuranceTaken == '1' || $scope.pIsHomeInsuranceTaken == 'true' || $scope.pIsHomeInsuranceTaken == true)
            $scope.objProperty.pIsHomeInsuranceTaken = true;
        else
            $scope.objProperty.pIsHomeInsuranceTaken = false;
        $scope.objProperty.pHomeInsuranceCompanyName = $scope.pHomeInsuranceCompanyName;
        $scope.objProperty.pHomeInsuranceCompanyPhoneNo = $scope.pHomeInsuranceCompanyPhoneNo;
        $scope.objProperty.pHomeInsuranceStartDate = $scope.pHomeInsuranceStartDate;
        $scope.objProperty.pHomeInsuranceEndDate = $scope.pHomeInsuranceEndDate;
        $scope.objProperty.pHomeInsuranceSendReminder = $scope.pHomeInsuranceSendReminder;

        $scope.MdlPropertyRegistration.PropertyMap = angular.copy($scope.objProperty);
        propertyService.saveProperty($scope.MdlPropertyRegistration).
               success(function (response) {
                   $scope.showLoader = false;
                   $scope.pId = response.Data;
                   $scope.PropertyEntryMode = "Edit";
                   $scope.savePhotos(isClose);
                   $scope.GetPropertyDetailByPropertyId($scope.pId, isShowTab2);
                   swal("SUCCESS", "Property data saved successfully", "success");

               }).error(function (data, status) {
                   if (status == 401) {
                       commonService.SessionOut();
                   }
                   else {
                       swal("ERROR", MessageService.serverGiveError, "error");
                   }
                   $scope.showLoader = false;
               }).finally(function (data) {
                   $scope.showLoader = false;
               });
    };

    $scope.UpdateProperty = function (isClose, isShowTab2) {
        if (parseInt($scope.pGrossArea, 10) > 0) {
            if (parseInt($scope.pRentableArea, 10) > parseInt($scope.pGrossArea, 10)) {
                swal("Alert !", "Rentable area cannot greater than Gross Area, save cancelled", "info");
                $scope.step1Show = true;
                $scope.step2Show = false;
                $scope.step3Show = false;
                return;
            }
        }

        $scope.MdlPropertyRegistration.OwnerMaster = angular.copy($scope.OwnerMaster);
        $scope.MdlPropertyRegistration.lstMdlOwner = angular.copy($scope.lstOwners);
        $scope.MdlPropertyRegistration.Ownertype = $scope.Ownertype;

        if ($scope.OwnerMaster != undefined && $scope.OwnerMaster != null) {
            $scope.MdlPropertyRegistration.OwnerMaster.CompanyName = $scope.OwnerMaster.CompanyName;
            $scope.MdlPropertyRegistration.OwnerMaster.ContactPerson = $scope.OwnerMaster.ContactPerson;
            $scope.MdlPropertyRegistration.OwnerMaster.Email = $scope.OwnerMaster.Email;
            $scope.MdlPropertyRegistration.OwnerMaster.ContactNum = $scope.OwnerMaster.ContactNum;
            $scope.MdlPropertyRegistration.OwnerMaster.POID = $scope.OwnerMaster.POID;
        }

        if ($scope.Ownertype == "individual") {
            if ($scope.lstOwners.length > 0) {
                angular.forEach($scope.lstOwners, function (value, key) {
                    if (value.Isprimary == true) {

                        $scope.MdlPropertyRegistration.OwnerMaster.CompanyName = value.pLastName + " " + value.pFirstName;
                        $scope.MdlPropertyRegistration.OwnerMaster.ContactPerson = value.pLastName + " " + value.pFirstName;
                        $scope.MdlPropertyRegistration.OwnerMaster.Email = value.pEmail;
                        $scope.MdlPropertyRegistration.OwnerMaster.ContactNum = value.pPhoneNo;
                    }
                });
            }
        }

        $scope.objProperty.pManagerId = $rootScope.userData.personId;
        $scope.objProperty.pId = $scope.pId;
        $scope.objProperty.pNum = $scope.pNum;
        $scope.objProperty.pName = $scope.pName;
        $scope.objProperty.pCategory = $scope.pPropertyCategory.Id;
        $scope.objProperty.pType = $scope.pPropertyType.Id;
        $scope.objProperty.pMailBoxNo = $scope.pMailBoxNo;
        $scope.objProperty.pAddressId = $scope.pAddressId;
        $scope.objProperty.pAddress = $scope.pAddress;
        $scope.objProperty.pState = $scope.pState.Name;
        $scope.objProperty.pStateId = $scope.pState.Id;
        $scope.objProperty.pCity = $scope.pCity.Name;
        $scope.objProperty.pCityId = $scope.pCity.Id;
        if ($scope.pCounty == null) {
            $scope.objProperty.pCounty = '';
            $scope.objProperty.pCountyId = 0;
        } else {
            $scope.objProperty.pCounty = $scope.pCounty.Name;
            $scope.objProperty.pCountyId = $scope.pCounty.id;
        }
        $scope.objProperty.pZipCode = $scope.pZipCode;
        $scope.objProperty.pCountryId = $scope.pCountry.Id;
        $scope.objProperty.pCountry = $scope.pCountry.Name;
        $scope.objProperty.pPhone = $scope.pPhone;
        $scope.objProperty.pGrossArea = $scope.pGrossArea;
        $scope.objProperty.pRentableArea = $scope.pRentableArea;
        $scope.objProperty.pParkingSpaces = $scope.pParkingSpaces;

        //********************************

        $scope.objProperty.pHoaAmount = $scope.pHoaAmount;
        $scope.objProperty.pHoaDate = (new Date($scope.pHoaDate)).getUTCDate();
        //$scope.objProperty.pHoaDate = $scope.pHoaDate;
        $scope.objProperty.HOAPaymentFrequency = $scope.HOAPaymentFrequency;
        $scope.objProperty.pManagementFee = $scope.pManagementFee;
        $scope.objProperty.pManagementFeeType = $scope.pManagementFeeType;

        //********************************

        $scope.objProperty.pUnitNo = $scope.pUnitNo;
        $scope.objProperty.pBaseRent = $scope.pBaseRent;
        $scope.objProperty.pPropertyTax = $scope.pPropertyTax;

        if (!angular.isUndefined($scope.pHOA) && $scope.pHOA != null)
            $scope.objProperty.pHOAId = $scope.pHOA.id;
        else
            $scope.objProperty.pHOAId = 0;

        $scope.objProperty.pOwnerArray = $scope.ownerArray;
        for (let index = 0; index < $scope.ownerArray.length; index++) {
            if ($scope.ownerArray[index].isprimaryowner == "1")
                $scope.ownerArray[index].isprimaryowner = true;

        }

        $scope.objProperty.pDescription = $scope.pDescription;
        $scope.objProperty.pAirportName = $scope.pAirportName;
        $scope.objProperty.pAirportDistance = $scope.pAirportDistance;
        $scope.objProperty.pUOM = $scope.pUOM;
        $scope.objProperty.pListType = $scope.pListType;
        $scope.objProperty.pComments = $scope.pComments;
        $scope.objProperty.pZoning = $scope.pZoning;
        $scope.objProperty.pLandAcres = $scope.pLandAcres;
        $scope.objProperty.pCustomField1 = $scope.pCustomField1;
        $scope.objProperty.pIsHomeWarrantyTaken = $scope.pIsHomeWarrantyTaken;
        $scope.objProperty.pHomeWarrantyCompanyName = $scope.pHomeWarrantyCompanyName;
        $scope.objProperty.pHomeWarrantyCompanyPhoneNo = $scope.pHomeWarrantyCompanyPhoneNo;
        $scope.objProperty.pHomeWarrantyStartDate = $scope.pHomeWarrantyStartDate;
        $scope.objProperty.pHomeWarrantyEndDate = $scope.pHomeWarrantyEndDate;
        $scope.objProperty.pHomeWarrantySendReminder = $scope.pHomeWarrantySendReminder;

        if ($scope.pIsHomeInsuranceTaken == '1' || $scope.pIsHomeInsuranceTaken == 'true' || $scope.pIsHomeInsuranceTaken == true)
            $scope.objProperty.pIsHomeInsuranceTaken = true;
        else
            $scope.objProperty.pIsHomeInsuranceTaken = false;
        $scope.objProperty.pHomeInsuranceCompanyName = $scope.pHomeInsuranceCompanyName;
        $scope.objProperty.pHomeInsuranceCompanyPhoneNo = $scope.pHomeInsuranceCompanyPhoneNo;
        $scope.objProperty.pHomeInsuranceStartDate = $scope.pHomeInsuranceStartDate;
        $scope.objProperty.pHomeInsuranceEndDate = $scope.pHomeInsuranceEndDate;
        $scope.objProperty.pHomeInsuranceSendReminder = $scope.pHomeInsuranceSendReminder;

        $scope.MdlPropertyRegistration.PropertyMap = angular.copy($scope.objProperty);

        // $scope.propertyjson = angular.toJson($scope.objProperty, true);
        $scope.showLoader = true;
        propertyService.UpdateProperty($scope.MdlPropertyRegistration).
               success(function (response) {
                   $scope.showLoader = false;
                   $scope.pId = response.Data;
                   $scope.PropertyEntryMode = "Edit";
                   $scope.savePhotos(isClose);
                   $scope.GetPropertyDetailByPropertyId($scope.pId, isShowTab2);
                   swal("SUCCESS", "Property updated successfully.", "success");
                   $scope.showtab2(true);
               }).error(function (data, status) {
                   if (status == 401) {
                       commonService.SessionOut();
                   }
                   else {
                       swal("ERROR", MessageService.serverGiveError, "error");
                   }
                   $scope.showLoader = false;
               }).finally(function (data) {
                   $scope.showLoader = false;
               });
    };

    $scope.UpdatePropertyAdditionalInfo = function () {

        if ($scope.pHomeWarrantySendReminder == '1' || $scope.pHomeWarrantySendReminder == 1 || $scope.pHomeWarrantySendReminder == 'true' || $scope.pHomeWarrantySendReminder == true) {
            $scope.objProperty.pHomeWarrantySendReminder = true;

            var enddate = $scope.pHomeWarrantyEndDate;
            if (new Date(enddate) <= new Date()) {
                swal("ERROR", "Can not set home warranty reminder when home warranty end date less than or equal to current date! Save discarded.", "error");
                $scope.showLoader = false;
                return;
            }
        }
        else {
            $scope.objProperty.pHomeWarrantySendReminder = false;
        }
        if ($scope.pHomeInsuranceSendReminder == '1' || $scope.pHomeInsuranceSendReminder == 1 || $scope.pHomeInsuranceSendReminder == 'true' || $scope.pHomeInsuranceSendReminder == true) {
            $scope.objProperty.pHomeInsuranceSendReminder = true;
            if ($scope.pHomeInsuranceEndDate <= new Date()) {
                swal("ERROR", "Can not set home insurance reminder when home insurance end date less than or equal to current date! Save discarded.", "error");
                $scope.showLoader = false;
                return;
            }
        }
        else {
            $scope.objProperty.pHomeInsuranceSendReminder = false;
        }

        $scope.objProperty.pId = $scope.pId;
        $scope.objProperty.pDescription = $scope.pDescription;
        $scope.objProperty.pAirportName = $scope.pAirportName;
        $scope.objProperty.pAirportDistance = $scope.pAirportDistance;
        $scope.objProperty.pUOM = $scope.pUOM;
        $scope.objProperty.pListType = $scope.pListType;
        $scope.objProperty.pComments = $scope.pComments;
        $scope.objProperty.pZoning = $scope.pZoning;
        $scope.objProperty.pLandAcres = $scope.pLandAcres;
        $scope.objProperty.pCustomField1 = $scope.pCustomField1;

        $scope.objProperty.pHomeWarrantyCompanyName = $scope.pHomeWarrantyCompanyName;
        $scope.objProperty.pHomeWarrantyCompanyPhoneNo = $scope.pHomeWarrantyCompanyPhoneNo;
        $scope.objProperty.pHomeWarrantyStartDate = $scope.pHomeWarrantyStartDate;
        $scope.objProperty.pHomeWarrantyEndDate = $scope.pHomeWarrantyEndDate;
        // $scope.objProperty.pHomeWarrantySendReminder = $scope.pHomeWarrantySendReminder;
        if ($scope.pIsHomeInsuranceTaken == '1' || $scope.pIsHomeInsuranceTaken == 'true' || $scope.pIsHomeInsuranceTaken == true)
            $scope.objProperty.pIsHomeInsuranceTaken = true;
        else
            $scope.objProperty.pIsHomeInsuranceTaken = false;

        if ($scope.pIsHomeWarrantyTaken == '1' || $scope.pIsHomeWarrantyTaken == 'true' || $scope.pIsHomeWarrantyTaken == true)
            $scope.objProperty.pIsHomeWarrantyTaken = true;
        else
            $scope.objProperty.pIsHomeWarrantyTaken = false;

        $scope.objProperty.pHomeInsuranceCompanyName = $scope.pHomeInsuranceCompanyName;
        $scope.objProperty.pHomeInsuranceCompanyPhoneNo = $scope.pHomeInsuranceCompanyPhoneNo;
        $scope.objProperty.pHomeInsuranceStartDate = $scope.pHomeInsuranceStartDate;
        $scope.objProperty.pHomeInsuranceEndDate = $scope.pHomeInsuranceEndDate;
        // $scope.objProperty.pHomeInsuranceSendReminder = $scope.pHomeInsuranceSendReminder;
        // $scope.MdlPropertyRegistration.PropertyMap = angular.copy($scope.objProperty);
        $scope.MdlPropertyAdditionalInfo.PropertyMap = angular.copy($scope.objProperty);

        $scope.showLoader = true;
        propertyService.UpdatePropertyAdditionalInfo($scope.MdlPropertyAdditionalInfo).success(function (response) {
            if (response.Status == true) {
                $scope.showLoader = false;
                swal("SUCCESS", "Additional info saved successfully", "success");
                $scope.showtab3(true)
            }
            else {
                swal("ERROR", MessageService.responseError, "error");
            }
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    $scope.GetPropertyDetailByPropertyId = function (PropertyId, isShowTab2) {
        $scope.showLoader = true;
        $scope.lstOwners = [];
        propertyService.GetPropertyDetailByPropertyId(PropertyId).
          success(function (response) {
              $scope.showLoader = false;
              if (response.Status == true) {
                  $scope.displayPropertyInfo(response.Data.tblProperty[0]);
                  var count = 0
                  angular.forEach(response.Data.tblOwner, function (value, key) {
                      count += 1;
                      var newOwner = angular.copy(value);
                      newOwner.showRemove = true;
                      newOwner.index = count;
                      $scope.lstOwners.push(newOwner);
                  });

                  if (response.Data.tblOwnerMaster.length > 0) {
                      $scope.OwnerMaster = response.Data.tblOwnerMaster[0];
                      $scope.Ownertype = $scope.OwnerMaster.OwnerType;
                      if ($scope.Ownertype == "company")
                      {
                          $scope.pCompany = $scope.OwnerMaster;
                      }
                  }
                  if (isShowTab2 == true) {
                      $scope.showtab2(true);
                  }
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.displayPropertyInfo = function (prop) {

        $scope.showState = false;
        $scope.showCity = false;
        $scope.showCounty = false;

        $scope.pId = prop.Id;
        $scope.pNum = prop.pNum;
        $scope.pPropertyCategoryId = prop.pCategory;

        $scope.pPropertyTypeId = prop.pType;
        $scope.pName = prop.pName;
        $scope.pMailBoxNo = prop.pMailBoxNo;
        $scope.pAddressId = prop.pAddressId;
        $scope.pAddress = prop.pAddress;
        $scope.pCountryId = prop.pCountryId;

        $scope.pCountyId = prop.pCountyId;
        $scope.pCityId = prop.pCityId;
        $scope.pStateId = prop.pStateId;

        $scope.pZipCode = prop.pZipCode;
        $scope.pPhoneNumber = prop.pPhoneNumber;
        $scope.pGrossArea = prop.pGrossArea;
        $scope.pRentableArea = prop.pRentableArea;
        $scope.pParkingSpaces = prop.pParkingSpaces;
        $scope.pUnitNo = prop.pUnitNo;
        $scope.pBaseRent = prop.pBaseRent;
        $scope.pPropertyTax = prop.pPropertyTax;

        $scope.pHOA = prop.pHOA;
        $scope.pHoaAmount = prop.pHoaAmount;
        $scope.pHoaDate = new Date();
        $scope.pHoaDate.setDate(prop.pHoaDate);
        $scope.HOAPaymentFrequency = prop.HOAPaymentFrequency;
        $scope.pManagementFee = prop.pManagementFee;
        $scope.pManagementFeeType = prop.pManagementFeeType == null ? 'Amount' : prop.pManagementFeeType;

        $scope.pDescription = prop.pDescription;
        $scope.pAirportName = prop.pAirportName;
        $scope.pAirportDistance = prop.pAirportDistance;
        $scope.pUOM = prop.pUOM;
        $scope.pListType = prop.pListType;
        $scope.pComments = prop.pComments;
        $scope.pZoning = prop.pZoning;
        $scope.pLandAcres = prop.pLandAcres;
        if (prop.pIsHomeWarrantyTaken == true || prop.pIsHomeWarrantyTaken == 'true')
            $scope.pIsHomeWarrantyTaken = true;
        else
            $scope.pIsHomeWarrantyTaken = false;
        $scope.pHomeWarrantyCompanyName = prop.pHomeWarrantyCompanyName;
        $scope.pHomeWarrantyCompanyPhoneNo = prop.pHomeWarrantyCompanyPhoneNo;

        $scope.pHomeWarrantyStartDate = prop.pHomeWarrantyStartDate;
        $scope.pHomeWarrantyEndDate = prop.pHomeWarrantyEndDate;

        if (prop.pHomeWarrantySendReminder == true || prop.pHomeWarrantySendReminder == 'true')
            $scope.pHomeWarrantySendReminder = true;
        else
            $scope.pHomeWarrantySendReminder = false;

        if (prop.pIsHomeInsuranceTaken == true || prop.pIsHomeInsuranceTaken == 'true')
            $scope.pIsHomeInsuranceTaken = true;
        else
            $scope.pIsHomeInsuranceTaken = false;

        $scope.pHomeInsuranceCompanyName = prop.pHomeInsuranceCompanyName;
        $scope.pHomeInsuranceCompanyPhoneNo = prop.pHomeInsuranceCompanyPhoneNo;

        $scope.pHomeInsuranceStartDate = prop.pHomeInsuranceStartDate;
        $scope.pHomeInsuranceEndDate = prop.pHomeInsuranceEndDate;

        if (prop.pHomeInsuranceSendReminder == true || prop.pHomeInsuranceSendReminder == 'true')
            $scope.pHomeInsuranceSendReminder = true;
        else
            $scope.pHomeInsuranceSendReminder = false;

        $scope.pCustomField1 = prop.pCustomField1;
        $scope.getMasterData();

        propertyService.getPropertyPhotos($scope.pId, function (result, data) {
            if (result) {

                for (let index = 1; index <= data.length; index++) {
                    var fd = new FormData();

                    fd.append("file", $scope.Photos[index - 1]);
                    fd.append("propid", $scope.pId);
                    // fd.append("seqNo", index);
                    // fd.append("filename", 'image' + index);

                    fd.append("seqNo", data[index - 1].SeqNo);
                    fd.append("filename", data[index - 1].Name);
                    fd.append("filedesc", '');
                    fd.append("filetags", 'Photo, ' + $scope.pId);
                    $scope.Photos[index - 1] = fd;

                    output = document.getElementById('divimage' + data[index - 1].SeqNo.toString());
                    if (output != null) {
                        var random = Math.floor(100000 + Math.random() * 900000);
                        output.style.backgroundImage = 'url(/Uploads/Properties/Photos/' + $scope.pId + '-' + data[index - 1].Name + '.png?cb=' + random + ')';
                    }
                }
            }
            else {
                swal("ERROR", "Unable to fetch photos for property.", "error");
            }
        });

        propertyService.getPropertyDocuments($scope.pId, function (result, data) {
            if (result) {
                $scope.documentArray = [];
                angular.forEach(data, function (value, key) {
                    counter++;
                    $scope.documentArray.push({
                        counter: counter, showRemove: true, document: value.Name, documentPath: value.FileSavePath, documentCreationDate: value.CreationTime,
                        documentName: value.Name, documentDesc: value.Description, documentTags: value.Tags, isNew: false, isDeleted: false
                    });
                });
            }
            else {
                swal("ERROR", "Unable to fetch documents for property.", "error");
            }
        });

        $scope.GetHOAByPMID(function (data) {
            $scope.hoalist = [];
            $scope.hoalist = data;

            angular.forEach(data, function (value, key) {
                if (value.id == prop.pHOAId) {
                    $scope.pHOA = value;
                }
            });
        });
    }
    $scope.SetPrimary = function (index) {
        var count = 0;
        angular.forEach($scope.lstOwners, function (value, key) {
            if (count != index) {
                $scope.lstOwners[count].Isprimary = false;
            }
            else {
                $scope.lstOwners[count].Isprimary = true;
            }
            count += 1;
        });
    }

    $scope.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    $scope.ResetMdlOwner = function () {
        $scope.MdlOwner = {
            index: 0,
            Id: 0,
            POID: 0,
            pFirstName: '',
            pLastName: '',
            pEmail: '',
            pPhoneNo: '',
            Isprimary: false,
            showRemove: true
        };
    }

    $scope.Cancel = function (addOwnerForm) {
        $scope.ResetMdlOwner();
        addOwnerForm.$setPristine();
        $('#mdlAddOwner').modal('hide');
    }

    $scope.AddNewUser = function (addOwnerForm) {
       // m.NoOftroopMemEditReq = (vm.EventAttendees.TicketAttendees.filter(function (a) { return a.Attendees.some(Attendee === Attendee.IsEditRequired === true); })).length;
        //var selectedRoles = vm.roles.filter(function (x) { return x.id === role.id; });
        //var selectedOwner = $scope.lstOwners.filter(owner => owner.pEmail == $scope.MdlOwner.pEmail);
        var selectedOwner = ($scope.lstOwners.filter(function (owner) { return owner.pEmail === $scope.MdlOwner.pEmail }));
        if (selectedOwner.length == 0) {

            var newOwner = angular.copy($scope.MdlOwner);
            if ($scope.lstOwners.length < 1) {
                newOwner.Isprimary = true;
            }
            newOwner.index = $scope.lstOwners.length + 1;
            $scope.lstOwners.push(newOwner);
            $scope.Cancel(addOwnerForm)
        }
        else {
            swal("ERROR", "Owner already exist", "info");
        }
    }

    $scope.AddExistingUser = function (modal) {
        var selectedOwner = ($scope.lstOwners.filter(function (owner) { return owner.pEmail === modal.Email }));
      //  old//var selectedOwner = $scope.lstOwners.filter(owner => owner.pEmail == modal.Email);
        if (selectedOwner.length == 0) {
            var newOwner = angular.copy($scope.MdlOwner);
            if ($scope.lstOwners.length < 1) {
                newOwner.Isprimary = true;
            }
            $scope.lstOwners.push(newOwner);
            var lastIndex = $scope.lstOwners.length - 1;
            $scope.lstOwners[lastIndex].index = lastIndex + 1;
            $scope.lstOwners[lastIndex].pFirstName = modal.FirstName;
            $scope.lstOwners[lastIndex].pLastName = modal.LastName;
            $scope.lstOwners[lastIndex].pEmail = modal.Email;
            $scope.lstOwners[lastIndex].pPhoneNo = modal.Phone;
            $scope.lstOwners[lastIndex].Id = modal.PersonId;
            $('#mdlAddOwner').modal('hide');
        }
        else {
            swal("ERROR", "Owner already exist", "info");
        }
    };

    $scope.removeRow = function (index) {
        if ($scope.lstOwners[index].Isprimary == true && $scope.lstOwners.length > 1) {

            if (index == ($scope.lstOwners.length - 1)) {
                $scope.lstOwners[index - 1].Isprimary = true;
            }
            else {
                $scope.lstOwners[index + 1].Isprimary = true;
            }
            $scope.lstOwners.splice(index, 1);
        }
        else if ($scope.lstOwners.length > 1) {
            $scope.lstOwners.splice(index, 1);
        }
        else if ($scope.lstOwners.length == 1 && $scope.PropertyEntryMode != "Edit") {
            $scope.lstOwners.splice(index, 1);
        }
    };

    $scope.savePerson = function () {

        $scope.MdlPropertyOwnerDetail.OwnerMaster = angular.copy($scope.OwnerMaster);
        $scope.MdlPropertyOwnerDetail.lstMdlOwner = angular.copy($scope.lstOwners);
        $scope.MdlPropertyOwnerDetail.Ownertype = $scope.Ownertype;
        if ($scope.Ownertype == "individual") {
            angular.forEach($scope.lstOwners, function (value, key) {

                if (value.Isprimary == true) {
                    value.Isprimary = true;
                    $scope.MdlPropertyOwnerDetail.OwnerMaster.CompanyName = value.pLastName + " " + value.pFirstName;
                    $scope.MdlPropertyOwnerDetail.OwnerMaster.ContactPerson = value.pLastName + " " + value.pFirstName;
                    $scope.MdlPropertyOwnerDetail.OwnerMaster.Email = value.pEmail;
                    $scope.MdlPropertyOwnerDetail.OwnerMaster.ContactNum = value.pPhoneNo;
                }
            });
        }

        $scope.personjson = angular.toJson($scope.MdlPropertyOwnerDetail, true);

        ownerService.SaveOwner($scope.personjson, function (status, resp) {

            if (status) {
            }
        });
    };

    $scope.getOwnerData = function () {
        $rootScope.PersonList = null;
        masterdataService.getPerson(function (result, data) {
            if (result) {
                //$scope.personlist = data; //angular.toJson(data, true);

                //*******************************************
                // ****Below Lines are commented by ASR ******

                $scope.ownerlist = $rootScope.OwnerPersonList;

                //$scope.ownerlist = $rootScope.PersonList.filter(function (c) {
                //    return c.pTypeId == 2;//For Owner
                //});

                //*******************************************
                //$scope.hoalist = $rootScope.PersonList.filter(function (c) {
                //    return c.pTypeId == 5;//For Associations
                //});
                //console.log('1019', $scope.hoalist);
            }
            else {
                swal("ERROR", "Unable to fetch Owners.", "error");
            }
        });
    }

    $scope.getCompanyData = function () {
        $rootScope.CompanyList = null;
        masterdataService.GetCompanyByMangerID($scope.pmid).
            success(function (response) {
            $scope.showLoader = false;
            if (response.Status == true) {
                $rootScope.CompanyList = response.Data;
            }
            else {
                swal("ERROR", response.Message, "error");
            }
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
            //.success() {
            //if (result) {

            //    $scope.getCompany = $rootScope.OwnerPersonList;
            //}
            //else {
            //    swal("ERROR", "Unable to fetch Company.", "error");
            //}
    }
    $scope.CompanyChanged = function (POID) {
        var POID = POID;
        $rootScope.Company = [];
        //$rootScope.Company = $rootScope.CompanyList.filter(x=>x.POID == POID)[0];
        $rootScope.Company = ($rootScope.CompanyList.filter(function (c) { return c.POID === POID }))[0];
        $scope.OwnerMaster.Email = $rootScope.Company.Email;
        $scope.OwnerMaster.ContactPerson = $rootScope.Company.ContactPerson;
        $scope.OwnerMaster.ContactNum = $rootScope.Company.ContactNum;
        $scope.OwnerMaster.CompanyName = $rootScope.Company.CompanyName;
        $scope.OwnerMaster.POID = $rootScope.Company.POID;
        $scope.getOwnerByPOID(POID);
    }
    $scope.getOwnerByPOID = function (POID) {
        propertyService.getOwnerByPOID(POID).
          success(function (response) {
              $scope.showLoader = false;
              if (response.Status == true) {
                  var count = 0;
                  $scope.lstOwners = [];
                  angular.forEach(response.Data, function (value, key) {
                      count += 1;
                      var newOwner = angular.copy(value);
                      newOwner.showRemove = true;
                      newOwner.index = count;
                      $scope.lstOwners.push(newOwner);
                  });
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }


    $scope.ClearCompany = function (pAddNewCompany) {
        if (pAddNewCompany == true)
        {
            $scope.OwnerMaster.Email = '';
            $scope.OwnerMaster.ContactPerson = '';
            $scope.OwnerMaster.ContactNum = '';
            $scope.OwnerMaster.CompanyName = '';
            $scope.OwnerMaster.POID = 0;
        }
    }
    $scope.CheckEmailAvailability = function (email, section) {

        if (email == undefined || email == '' || email == null) {
            return;
        }
        $scope.showLoader = true;
        ownerService.CheckEmailAvailability(email, section).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {

                   if (section == "company") {
                       $scope.isOwnerMasterEmailExist = response.Data;
                   }
                   else {
                       $scope.isPersonEmailExist = response.Data;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.checkCountry = function (model) {

        $scope.pCountry = model;

        if (angular.isUndefined(model) || model == null) {
            $("#ddlstate").val("");
            $("#ddlcity").val("");
            $("#ddlcounty").val("");
            $scope.showState = true;
            $scope.showCity = true;
            $scope.showCounty = true;
            $("#ddlcity").addClass('back-color');
            $("#ddlcounty").addClass('back-color');
        }
        else {
            $scope.showState = false;
            $scope.GetStatesByCountryId(model.Id, function (data) { });
        }
    }
    $scope.checkState = function (model) {

        $scope.pState = model;

        $scope.ChangeZip();
        $scope.pCounty = '';
        $scope.pCity = '';

        if (angular.isUndefined(model) || model == null) {
            //$("#ddlcity").val("");
            //$("#ddlcounty").val("");
            $scope.showCity = true;
            $scope.showCounty = true;
        }
        else {
            $scope.showCounty = false;
            $scope.showCity = false;
            //$("#ddlcounty").removeClass('back-color');
            $scope.GetCountyByStateId(model.Id, function (data) {

                $scope.pCountyId = 0;
                $scope.citieslist = [];
                $scope.allcitieslist = [];
                var countyId = 0;
                $scope.GetCityByStateOrCounty(model.Id, countyId, function (data) { });
            });
        }
    }

    $scope.checkCounty = function (model) {
        $scope.pCounty = model;

        $scope.ChangeZip();
        $scope.pCity = '';
        if (angular.isUndefined(model) || model == null) {
            $("#ddlcity").val("");
            $scope.showCity = true;
        }
        else {
            $scope.showCity = false;
            var stateId = $scope.pState.Id;
            $scope.GetCityByStateOrCounty(stateId, model.id, function (data) { });

            $scope.pCityId = 0

            ///***********Below line is commented by ASR ******************
            //**************************************************************
            //masterdataService.getCitiesbyCounty(model.Id).
            //    success(function (response) {
            //        $scope.citieslist = response.Data;
            //    }).error(function (data, status) {
            //        if (status == 401) {
            //        }
            //        $scope.showLoader = false;
            //    }).finally(function (data) {
            //        $scope.showLoader = false;
            //    });
        }
    }

    $scope.ValidateZipCode = function (model, ele) {

        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidateZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", "invalid zip", "error");
            }
        }
    }

    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };

    $scope.ValidateZipCodeUsingAPI = function (ZipCode, ele) {

        var city = '';
        var County = '';
        var Country = '';
        //  $scope.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name;
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "");
                        }

                    });
                });
                // var enteredCounty = $scope.pCounty.name.replace(" County", "").toUpperCase().trim();
                if ($scope.pState.Name.toUpperCase().trim() == state.toUpperCase().trim()) {
                    if ($scope.pCity.Name.toUpperCase().trim() == city.trim()) {
                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }
    /// Retrieve Master Data - Start ////
    $scope.getPropertyCategories = function () {

        $scope.showLoader = true;
        masterdataService.getPropertyCategories(function (result, data) {
            if (result) {
                $scope.showLoader = false;
                if (data != null) {
                    $scope.propertycategorieslist = data;
                    if (!angular.isUndefined($scope.propertycategorieslist)) {
                        $scope.pPropertyCategory = $scope.propertycategorieslist.filter(function (c) {
                            return (c.Id == $scope.pPropertyCategoryId);
                        });
                    }
                    for (var i = 0; i < $scope.propertycategorieslist.length; i++) {
                        if ($scope.pPropertyCategoryId == $scope.propertycategorieslist[i].Id) {
                            $scope.pPropertyCategory = $scope.propertycategorieslist[i];
                            break;
                        }
                    }
                    for (var i in $scope.propertycategorieslist) {
                        $scope.useCategories.push({ counter: i, id: $scope.propertycategorieslist[i].Id, name: $scope.propertycategorieslist[i].Name, checked: true });
                    }
                }
            }
            else {
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to fetch Property Categories.", "error");
                }
                $scope.showLoader = false;
            }
        });
    }

    $scope.getPropertyTypes = function () {
        $scope.showLoader = true;
        masterdataService.getPropertyTypes(function (result, data) {
            if (result) {
                $scope.showLoader = false;
                if (data != null) {

                    $scope.propertytypeslist = data;
                    $scope.pPropertyType = $scope.propertytypeslist.filter(function (c) {
                        return c.Id == $scope.pPropertyTypeId;
                    });
                    for (var i in $scope.propertytypeslist) {
                        $scope.useType.push({ counter: i, id: $scope.propertytypeslist[i].Id, name: $scope.propertytypeslist[i].Name, checked: true });
                    }
                }
            }
            else {
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to fetch Property Types.", "error");
                }
                $scope.showLoader = false;
            }
        });
    }

    $scope.$watch('pPropertyCategory', function () {
        if (!angular.isUndefined($scope.propertytypeslist)) {
            $scope.propertytypesfilteredlist = $scope.propertytypeslist.filter(function (c) {
                return c.CategoryId == $scope.pPropertyCategory.Id;
            });
        }
    });

    $scope.showpropertytypes = function (item) {
        $scope.pPropertyCategoryFilterId = 0;

        if (!angular.isUndefined($scope.propertytypeslist)) {
            if (item != undefined) {
                $scope.propertytypesfilteredlist = $scope.propertytypeslist.filter(function (c) {
                    return c.CategoryId == item.Id;
                });
                $scope.useType = [];
                for (var i in $scope.propertytypesfilteredlist) {
                    $scope.useType.push({ counter: i, id: $scope.propertytypesfilteredlist[i].Id, name: $scope.propertytypesfilteredlist[i].Name, checked: true });
                }
                $scope.pPropertyCategoryFilterId = item.Id;
                $scope.isAllPropertySelected = false;
                $scope.filterBySelection();
            }
            else {
                $scope.isAllPropertySelected = true;
                $scope.propertytypesfilteredlist = $scope.propertytypeslist;
            }
        }
    };

    /// Retrieve Master Data - Start ////
    $scope.displayPropView = false;
    $scope.ShowTabNumber = 1;

    $scope.tab1Class = "active2";
    $scope.tab2Class = "";
    $scope.tab3Class = "";
    $scope.tab4Class = "";
    $scope.tab5Class = "";
    $scope.tab6Class = "";
    $scope.setTabClass = function (tabNum) {
        $scope.tab = tabNum;
        $scope.tab1Class = "";
        $scope.tab2Class = "";
        $scope.tab3Class = "";
        $scope.tab4Class = "";
        $scope.tab5Class = "";
        $scope.tab6Class = "";
        if (tabNum == 1) {
            $scope.tab1Class = "active2";
            $scope.select_fromDate = $scope.selectCurrentPeriod.Startdate;
            $scope.current_toDate = $scope.selectCurrentPeriod.EndDate;
            $scope.showLoader = false;
            // $scope.GetTransactionByPersonAndProperty($scope.select_fromDate, $scope.current_toDate, $scope.personId, $scope.selectedProperty.pId, $scope.personType)
        }
        else if (tabNum == 2) {
            $scope.tab2Class = "active2";
        }
        else if (tabNum == 3) {
            $scope.tab3Class = "active2";
            // $scope.getLeases();
        }
        else if (tabNum == 4) {
            $scope.tab4Class = "active2";
        }
        else if (tabNum == 5) {
            $scope.tab5Class = "active2";

            $scope.FilterLeaseByStatus("Active");
            // $scope.GetLeaseByPropertyId("Active", $scope.viewPropertyId);
        }
        else if (tabNum == 6) {
            $scope.tab6Class = "active2";
        }
    };

    $scope.GetPastLeases = function (LeaseStatus) {
        $scope.FilterLeaseByStatus(LeaseStatus);
        // $scope.GetLeaseByPropertyId(LeaseStatus, $scope.viewPropertyId);
    }

    $scope.MessageToPerson = function (EmailId, PersonType) {
        openchat();
        var PersonTypeId = 0;
        if ($rootScope.PersonTyp.owner.toUpperCase() == PersonType.toUpperCase()) {
            PersonTypeId = $rootScope.PersonTypeIdEnum.owner;
        }
        else if ($rootScope.PersonTyp.tenant.toUpperCase() == PersonType.toUpperCase()) {
            PersonTypeId = $rootScope.PersonTypeIdEnum.tenant;
        }
        $rootScope.$emit("OpenPersonChatWindow", { EmailId: EmailId, PersonTypeId: PersonTypeId });
    }

    $scope.FilterLeases = function (LeaseStatus) {

        if (LeaseStatus == "Active") {
            $scope.ShowActiveLease = true;
        }
        else {
            $scope.ShowActiveLease = false;
        }
        $scope.FilterLeaseByStatus(LeaseStatus);
        // $scope.GetLeaseByPropertyId(LeaseStatus, $scope.viewPropertyId);
    }

    //$scope.MakePayment = function (pId) {
    //    $rootScope.paymentPropertyId = pId;
    //    $location.path('/payment');
    //}
    //$scope.OpenDeletePropertyConfirmationModal = function (pId) {
    //    $scope.propertyIdToDelete = pId;
    //    $("#DeletePropertyConfirmation").modal();
    //}
    //$scope.deleteProperty = function (pId) {
    //    propertyService.deleteProperty(pId, function (result, status, propList) {
    //        if (result) {
    //            $scope.propertieslist = propList;
    //            $scope.propertyjson = angular.toJson(propList, true);
    //            $scope.displayPropView = false;
    //            swal("SUCCESS", "Property deleted successfully.", "success");
    //            $("#DeletePropertyConfirmation").modal('hide');
    //        }
    //        else {
    //            $("#DeletePropertyConfirmation").modal('hide');
    //            if (status == 401) {
    //                commonService.SessionOut();
    //            }
    //            else {
    //                swal("ERROR", MessageService.serverGiveError, "error");
    //            }
    //            $scope.showLoader = false;
    //        }
    //    });
    //}

    $scope.OpenDeletePropertyConfirmationModal = function (pId) {

        $scope.propertyIdToDelete = pId;
        swal({
            title: "Are you sure?",
            text: "You want to archive this property.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then(function(willDelete) {
            if (willDelete) {
                $scope.showLoader = true;
                propertyService.deleteProperty(pId, function (result, status, propList) {
                    if (result) {
                        $scope.showLoader = false;
                        $scope.propertieslist = propList;
                        $scope.propertyjson = angular.toJson(propList, true);
                        $scope.displayPropView = false;
                        swal("SUCCESS", "Property archived successfully.", "success");
                        $("#DeletePropertyConfirmation").modal('hide');
                    }
                    else {
                        $("#DeletePropertyConfirmation").modal('hide');
                        if (status == 401) {
                            commonService.SessionOut();
                        }
                        else {
                            swal("ERROR", MessageService.serverGiveError, "error");
                        }
                        $scope.showLoader = false;
                    }
                });
            }
        });
    }

    //$scope.ConfirmMessage_Ok = function () {
    //    $scope.deleteProperty($scope.propertyIdToDelete);
    //}

    $scope.ShowPropertyView = function (pnum) {

        for (var i = 0; i < $scope.propertieslist.length; i++) {
            if ($scope.propertieslist[i].pId == pnum) {

                $scope.selectedProperty = $scope.propertieslist[i];
                // set the tab that needs to be shown
                $scope.ShowTabNumber = 1;
                $scope.setTabClass('1'); // initialy set financials as active.

                //  console.log('$scope.selectedProperty', $scope.selectedProperty);

                if (!angular.isUndefined($scope.selectedProperty.pId)) {
                    $scope.showLoader = true;
                    propertyService.getPropertyPhotos($scope.selectedProperty.pId, function (result, data) {
                        if (result) {
                            $scope.showLoader = false;
                            angular.forEach(data, function (value, key) {
                                var arr = value.FileSavePath.split("\\");
                                value.imageName = arr[arr.length - 1];
                            });
                            $scope.selectedProperty.photosArray = data;
                        }
                        else {
                            if (status == 401) {
                                commonService.SessionOut();
                            }
                            else {
                                swal("ERROR", MessageService.serverGiveError, "error");
                            }
                            $scope.showLoader = false;
                        }
                    });

                    $scope.showLoader = true;
                    propertyService.getPropertyDocuments($scope.selectedProperty.pId, function (result, data) {
                        if (result) {
                            $scope.showLoader = false;
                            angular.forEach(data, function (value, key) {
                                counter++;
                                $scope.documentArray.push({
                                    counter: counter, showRemove: true, document: value.Name, documentPath: value.FileSavePath, documentCreationDate: value.CreationTime,
                                    documentName: value.Name, documentDesc: value.Description, documentTags: value.Tags, isNew: false, isDeleted: false
                                });
                            });
                            $scope.selectedProperty.docsArray = $scope.documentArray;
                        }
                        else {
                            if (status == 401) {
                                commonService.SessionOut();
                            }
                            else {
                                swal("ERROR", MessageService.serverGiveError, "error");
                            }
                            $scope.showLoader = false;
                        }
                    });
                }
                break;
            }
        }
    }

    //$scope.ShowPropertyList = function () {
    //    $scope.displayPropView = false;
    //    $rootScope.EditedPropertyIdForShowProfile = undefined;
    //}

    $scope.Photos = [{}];

    $scope.propertieslist = [
        {
            "pId": 0,
            "pNum": 0,
            "pType": 0,
            "pTypeName": "",
            "pName": "",
            "pAddress": "",
            "pCityId": "",
            "pCity": "",
            "pCounty": "",
            "pCountyId": "",
            "pState": "",
            "pStateId": "",
            "pZipcode": "",
            "pCountryId": "",
            "pCountry": "",
            "pPhone": "",
            "pEmail": "",
            "pFax": "",
            "pGrossArea": "0",
            "pRentableArea": "0",
            "pParkingSpaces": "0",
            "pUnitNo": "",
            "pHOA": "",
            "HOAPaymentFrequency": "",
            "pDescription": "",
            "pAirportName": "",
            "pAirportDistance": "",
            "pUOM": "",
            "pListType": "",
            "pComments": "",
            "pCustomField1": "",
            "photosArray": [],
            "docsArray": []
        }
    ]

    $scope.propertyjson = '';

    $scope.getProperties = function (callback) {

        $scope.getStatusData();
        $scope.getPropertyCategories();
        $scope.getPropertyTypes();

        $scope.showLoader = true;
        $scope.displayPropView = false;
        var personId = $rootScope.userData.personId;
        propertyService.getAllProperties(personId, function (status, data) {
            if (status) {
                $scope.showLoader = false;
                $scope.propertieslist = data;
                $scope.allpropertieslist = data;
                $scope.propertyjson = angular.toJson(data, true);
                callback(data);
            }
            else {
                $scope.showLoader = false;
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", "error in getAllProperties", "error");
                }
            }
        });
    };


    // });

    $scope.showLeaseResidential = function (prop) {
        if (!angular.isUndefined(prop)) {
            if (!angular.isUndefined(prop.pStatusId)) {
                return ((prop.pStatusId == 5 || prop.pStatusId == 8) && prop.pCategoryId == 1);
            }
        }
        return false;
    }
    $scope.showLeaseCommercial = function (prop) {
        if (!angular.isUndefined(prop)) {
            if (!angular.isUndefined(prop.pStatusId)) {
                return ((prop.pStatusId == 5 || prop.pStatusId == 8) && prop.pCategoryId == 2);
            }
        }
        return false;
    }

    // propertyapp.controller('AddProperyController', function ($scope, $http, propertyService, $routeParams) {

    //var prop = null;
    $scope.PropertyEntryMode = "Add";
    $scope.documentArray = [];
    var counter = 0;
    //$scope.documentArray = [{ counter: counter, showRemove: false, document: '', documentPath: '', documentName: '', documentTags: '', isNew: true },];


    $scope.initializeNewEntry = function () {

        $scope.pId = 0;
        $scope.pNum = '';
        $scope.pPropertyTypeId = 0;
        $scope.pPropertyType = '';
        //$scope.pTypeName = '';
        $scope.pName = '';
        $scope.pMailBoxNo = '';
        $scope.pAddressId = 0;
        $scope.pAddress = '';
        $scope.pCountry = '1';
        $scope.pState = '';
        $scope.pStateId = '0';
        $scope.pCity = '';
        $scope.pCityId = '0'
        $scope.pCounty = '';
        $scope.pCountyId = '0';
        $scope.pZipCode = '';
        $scope.pPhoneNumber = '';
        $scope.pGrossArea = '';
        $scope.pRentableArea = '';
        $scope.pParkingSpaces = '';
        $scope.pUnitNo = "";
        $scope.pHOA = '';
        $scope.pHoaAmount = '';
        $scope.pHoaDate = new Date();
        $scope.HOAPaymentFrequency = '';

        $scope.pManagementFee = 0;
        $scope.pManagementFeeType = '';
        // $scope.pOwnerId = 0;
        // $scope.pOwnerName = '';
        // $scope.pOwnerPhone = '';
        // $scope.pOwnerFax = '';
        // $scope.pOwnerEmail = '';

        $scope.pPersonId = 0;
        $scope.pFirstName = ''
        $scope.pLastName = '';
        $scope.pEmail = '';
        $scope.pPhoneNo = '';
        $scope.personjson = '';

        // additional details
        $scope.pDescription = '';
        $scope.pAirportName = '';
        $scope.pAirportDistance = '';
        $scope.pUOM = '';
        $scope.pListType = '';
        $scope.pComments = '';
        $scope.pZoning = '';
        $scope.pLandAcres = '';
        $scope.pIsHomeWarrantyTaken = false;
        $scope.pHomeWarrantyCompanyName = '';
        $scope.pHomeWarrantyCompanyPhoneNo = '';
        $scope.pHomeWarrantyStartDate = new Date();
        $scope.pHomeWarrantyEndDate = new Date();
        $scope.pHomeWarrantySendReminder = false;
        $scope.pIsHomeInsuranceTaken = false;
        $scope.pHomeInsuranceCompanyName = '';
        $scope.pHomeInsuranceCompanyPhoneNo = '';
        $scope.pHomeInsuranceStartDate = new Date();
        $scope.pHomeInsuranceEndDate = new Date();
        $scope.pHomeInsuranceSendReminder = false;

        $scope.pCustomField1 = '';

    }

    $scope.displayExistingInfo = function (prop) {
        $scope.showState = false;
        $scope.showCity = false;
        $scope.showCounty = false;

        $scope.pId = prop.pId;
        $scope.pNum = prop.pNum;
        $scope.pPropertyCategoryId = prop.pCategoryId;
        $scope.pPropertyTypeId = prop.pTypeId;
        $scope.pName = prop.pName;
        $scope.pMailBoxNo = prop.pMailBoxNo;
        $scope.pAddressId = prop.pAddressId;
        $scope.pAddress = prop.pAddress;
        $scope.pCountryId = prop.pCountryId;

        $scope.pCountyId = prop.pCountyId;
        $scope.pCityId = prop.pCityId;
        $scope.pStateId = prop.pStateId;
        $scope.pZipCode = prop.pZipCode;
        $scope.pPhoneNumber = prop.pPhoneNumber;
        $scope.pGrossArea = prop.pGrossArea;
        $scope.pRentableArea = prop.pRentableArea;
        $scope.pParkingSpaces = prop.pParkingSpaces;
        $scope.pUnitNo = prop.pUnitNo;
        $scope.pBaseRent = prop.pBaseRent;
        $scope.pHOA = prop.pHOA;

        $scope.pHoaAmount = prop.pHoaAmount;
        $scope.pHoaDate = new Date();
        $scope.pHoaDate.setDate(prop.pHoaDate);
        $scope.HOAPaymentFrequency = prop.HOAPaymentFrequency;
        $scope.pManagementFee = prop.pManagementFee;
        $scope.pDescription = prop.pDescription;
        $scope.pAirportName = prop.pAirportName;
        $scope.pAirportDistance = prop.pAirportDistance;
        $scope.pUOM = prop.pUOM;
        $scope.pListType = prop.pListType;
        $scope.pComments = prop.pComments;
        $scope.pZoning = prop.pZoning;
        $scope.pLandAcres = prop.pLandAcres;
        if (prop.pIsHomeWarrantyTaken == true || prop.pIsHomeWarrantyTaken == 'true')
            $scope.pIsHomeWarrantyTaken = true;
        else
            $scope.pIsHomeWarrantyTaken = false;
        $scope.pHomeWarrantyCompanyName = prop.pHomeWarrantyCompanyName;
        $scope.pHomeWarrantyCompanyPhoneNo = prop.pHomeWarrantyCompanyPhoneNo;
        $scope.pHomeWarrantyStartDate = prop.pHomeWarrantyStartDate;
        $scope.pHomeWarrantyEndDate = prop.pHomeWarrantyEndDate;
        //$scope.pHomeWarrantySendReminder = prop.pHomeWarrantySendReminder;
        if (prop.pHomeWarrantySendReminder == true || prop.pHomeWarrantySendReminder == 'true')
            $scope.pHomeWarrantySendReminder = true;
        else
            $scope.pHomeWarrantySendReminder = false;

        //$scope.pIsHomeInsuranceTaken = prop.pIsHomeInsuranceTaken;
        if (prop.pIsHomeInsuranceTaken == true || prop.pIsHomeInsuranceTaken == 'true')
            $scope.pIsHomeInsuranceTaken = true;
        else
            $scope.pIsHomeInsuranceTaken = false;

        $scope.pHomeInsuranceCompanyName = prop.pHomeInsuranceCompanyName;
        $scope.pHomeInsuranceCompanyPhoneNo = prop.pHomeInsuranceCompanyPhoneNo;
        $scope.pHomeInsuranceStartDate = prop.pHomeInsuranceStartDate;
        $scope.pHomeInsuranceEndDate = prop.pHomeInsuranceEndDate;
        // $scope.pHomeInsuranceSendReminder = prop.pHomeInsuranceSendReminder;
        if (prop.pHomeInsuranceSendReminder == true || prop.pHomeInsuranceSendReminder == 'true')
            $scope.pHomeInsuranceSendReminder = true;
        else
            $scope.pHomeInsuranceSendReminder = false;

        $scope.pCustomField1 = prop.pCustomField1;

        if ($scope.pId > 0) {
            $scope.showLoader = true;
            propertyService.getPropertyOwners($scope.pId, function (result, data) {
                if (result) {
                    $scope.showLoader = false;
                    $scope.ownerArray = data;
                    for (var index = 0; index < data.length; index++) {
                        if (data[index].isprimaryowner == "true") {
                            $scope.ownerArray[index].isprimaryowner = "1";
                        }
                    }
                }
                else {
                    $scope.showLoader = false;
                    if (data == 401) {
                        commonService.SessionOut();
                    }
                    else {
                        swal("ERROR", MessageService.serverGiveError, "error");
                    }
                }
            });
            $scope.showLoader = true;
            propertyService.getPropertyPhotos($scope.pId, function (result, data) {
                if (result) {
                    $scope.showLoader = false;
                    for (let index = 1; index <= data.length; index++) {
                        var fd = new FormData();
                        fd.append("file", $scope.Photos[index - 1]);
                        fd.append("propid", $scope.pId);
                        fd.append("seqNo", index);
                        fd.append("filename", 'image' + index);
                        fd.append("filedesc", '');
                        fd.append("filetags", 'Photo, ' + $scope.pId);
                        $scope.Photos[index - 1] = fd;
                        output = document.getElementById('divimage' + index.toString());
                        if (output != null) {
                            var random = Math.floor(100000 + Math.random() * 900000);
                            output.style.backgroundImage = 'url(/Uploads/Properties/Photos/' + prop.pId + '-image' + index.toString() + '.png?cb=' + random + ')';
                        }
                    }
                }
                else {
                    $scope.showLoader = false;
                    if (data == 401) {
                        commonService.SessionOut();
                    }
                    else {
                        swal("ERROR", "Unable to fetch photos for property.", "error");
                    }
                }
            });
        }


        // prop documents
        $scope.showLoader = true;
        propertyService.getPropertyDocuments(prop.pId, function (result, data) {
            if (result) {
                $scope.showLoader = false;
                pDocs = data;
                if (pDocs.length > 0) {
                    $scope.documentArray = [];
                    counter = 0;
                    for (var i = 0; i < pDocs.length; i++) {
                        $scope.documentArray.push({ counter: counter, showRemove: true, document: '', documentPath: pDocs[i].DocOrgFileName, documentName: pDocs[i].Name, documentDesc: pDocs[i].Description, documentTags: pDocs[i].Tags, documentCreationDate: pDocs[i].CreationTime, isNew: false, isDeleted: false });
                        counter++;
                    }
                    //$scope.selectedProperty.docsArray = $scope.documentArray;
                }
            }
            else {
                $scope.showLoader = false;
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }
        });
    }

    $scope.openUrl = function (url) {
        window.open("/Uploads/Properties/Documents/" + url, '_blank', 'height=800,width=600');
    }

    //#region Documents Tab        
    $scope.addDocument = function ($event) {
        counter++;
        $scope.documentArray.push({ counter: counter, showRemove: true, document: '', documentPath: '', documentName: '', documentTags: '', documentCreationDate: '', isNew: true, isDeleted: false });
        $event.preventDefault();
    };

    $scope.removeDocument = function (index) {

        for (var i = 0; i < $scope.documentArray.length; i++) {
            if ($scope.documentArray[i].counter == index) {
                if ($scope.documentArray[i].isNew) {
                    $scope.documentArray.splice(i, 1);
                    counter = counter - 1;
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.
                    $scope.documentArray[i].isDeleted = true;
                }
            }
        }
    }


    $scope.documentSelected = function (element) {
        for (var i = 0; i < element.files.length; i++) {
            var index = counter;
            $scope.documentArray.push({ counter: counter++, showRemove: true, document: element.files[i], documentPath: '', documentName: element.files[i].name, documentDesc: '', documentTags: '', isNew: true, isDeleted: false });
            var reader = new FileReader();
            reader.onload = function () {
                // $scope.documentArray[index].document = reader.result;
            }
            reader.readAsDataURL(event.target.files[i]);
            $scope.$apply();
        }
    };
    //#endregion


    //#region - Show / Hide functions - Start
    $scope.step1Show = true;
    $scope.step2Show = false;
    $scope.step3Show = false;

    $scope.showtab1 = function () {

        $scope.step1Show = true;
        $scope.step2Show = false;
        $scope.step3Show = false;

        $("#liBasicInfo").removeClass("active");
        $("#liAddlInfo").removeClass("active");
        $("#liDocuments").removeClass("active");
        $("#liBasicInfo").addClass("active");

        $("#step1tab").addClass("tab-pane fade in active");
        $("#step2tab").addClass("tab-pane fade");
        $("#step3tab").addClass("tab-pane fade");

        $("#steps1").css({ display: "block" });
        $("#steps2").css({ display: "none" });
        $("#steps3").css({ display: "none" });
    };

    $scope.showtab2 = function (boolCheck) {
        if (boolCheck) {
            boolCheck = $scope.propForm.$valid;
        }
        else
            boolCheck = true;
        if (boolCheck && $scope.pId > 0) {

            $scope.step1Show = false;
            $scope.step2Show = true;
            $scope.step3Show = false;
            $("#step1tab").addClass("tab-pane fade");
            $("#step2tab").addClass("tab-pane fade in active");
            $("#step3tab").addClass("tab-pane fade");

            $("#liBasicInfo").removeClass("active");
            $("#liAddlInfo").removeClass("active");
            $("#liDocuments").removeClass("active");
            $("#liAddlInfo").addClass("active");

            $("#steps1").css({ display: "none" });
            $("#steps2").css({ display: "block" });
            $("#steps3").css({ display: "none" });
        }
    };

    $scope.showtab3 = function (boolCheck) {
        if (boolCheck) {
            boolCheck = $scope.propForm.$valid;
        }
        else
            boolCheck = true;

        if (boolCheck && $scope.pId > 0) {

            $scope.step1Show = false;
            $scope.step2Show = false;
            $scope.step3Show = true;

            $("#liBasicInfo").removeClass("active");
            $("#liAddlInfo").removeClass("active");
            $("#liDocuments").removeClass("active");
            $("#liDocuments").addClass("active");

            $("#step1tab").addClass("tab-pane fade");
            $("#step2tab").addClass("tab-pane fade");
            $("#step3tab").addClass("tab-pane fade in active");

            $("#steps1").css({ display: "none" });
            $("#steps2").css({ display: "none" });
            $("#steps3").css({ display: "block" });
        }
    };
    //#endregion

    //#region  Photos - Loading & Saving
    $scope.fileSelected = function (element) {

        var myFileSelected = element.files[0];

        switch (element.name) {
            case 'image1':
                var fname = ("1_" + myFileSelected.name);
                $scope.Photos[0] = myFileSelected;
                $scope.Photos[0].seqNo = 1;
                $scope.Photos[0].name = fname;
                $scope.$apply();
                break;
            case 'image2':
                var fname = ("2_" + myFileSelected.name);
                $scope.Photos[1] = myFileSelected;
                $scope.Photos[1].seqNo = 2;
                $scope.Photos[1].name = fname;
                $scope.$apply();
                break;
            case 'image3':
                var fname = ("3_" + myFileSelected.name);
                $scope.Photos[2] = myFileSelected;
                $scope.Photos[2].seqNo = 3;
                $scope.Photos[2].name = fname;
                $scope.$apply();
                break;
            case 'image4':
                var fname = ("4_" + myFileSelected.name);
                $scope.Photos[3] = myFileSelected;
                $scope.Photos[3].seqNo = 4;
                $scope.Photos[3].name = fname;
                $scope.$apply();
                break;
            case 'image5':
                //$scope.Photos[4] = myFileSelected;
                //$scope.Photos[4].seqNo = 5;               
                //$scope.$apply();

                var fname = ("5_" + myFileSelected.name);
                $scope.Photos[4] = myFileSelected;
                $scope.Photos[4].seqNo = 5;
                $scope.Photos[4].name = fname;
                $scope.$apply();
                break;
        }

        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('div' + element.name);
            if (output != null)
                output.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    $scope.savePhotos = function (isClose) {
        var fd = new FormData();
        var isUndefined = false;
        if ($scope.PropertyEntryMode == "Add" && $scope.Photos[0] == undefined) {
            isUndefined = true;
        }

        if ($scope.Photos.length > 0) {

            for (let index = 1; index <= $scope.Photos.length; index++) {
                if ($scope.Photos[index - 1] != undefined) {
                    if ($scope.Photos[index - 1].seqNo != undefined) {
                        var seq = $scope.Photos[index - 1].seqNo;
                        fd.append("file", $scope.Photos[index - 1]);
                        fd.append("propid", $scope.pId);
                        fd.append("moduleid", 'PROPERTY');
                        fd.append("seqNo", seq);
                        fd.append("imageindex", index);
                        fd.append("filename", 'image' + index);
                        fd.append("filedesc", '');
                        fd.append("filetags", 'Photo, ' + $scope.pId);
                    }
                }
            }
            $scope.showLoader = true;
            propertyService.saveFiles(fd, '/api/propertyphoto', function (status, resp) {
                if (status) {
                    $scope.showLoader = false;

                    if ($scope.documentArray.length > 0) {
                        $scope.saveDocuments(isClose);
                    }
                    else {
                        if (isClose) {
                            if ($scope.PropertyEntryMode == "Edit") {
                                $location.path('/propertyProfile/' + $scope.pId);
                            }
                            else {
                                $location.path('/properties');
                            }
                        }
                    }
                }
                else {

                    if (status == 401) {
                        commonService.SessionOut();
                    }
                    else {
                        swal("ERROR", "Unable to save photos.", "error");
                    }
                    $scope.showLoader = false;
                }
            });
        }
        else {
            if ($scope.documentArray.length > 0) {
                $scope.saveDocuments(isClose);
            }
            else {
                if (isClose) {
                    if ($scope.PropertyEntryMode == "Edit") {
                        $location.path('/propertyProfile/' + $scope.pId);
                    }
                    else {
                        $location.path('/properties');
                    }
                }
            }
        }
    }


    $scope.CancelProp = function (isClose) {
        if ($scope.PropertyEntryMode == "Edit") {
            $scope.ShowPropertyView($scope.pId);
        }
        else {
            if (!isClose) {
                $location.path('/properties');
            }
        }
    }
    //#endregion

    //#region Save Documents 
    $scope.saveDocuments = function (isClose) {
        var fd = new FormData();

        for (var i = 0; i < $scope.documentArray.length; i++) {

            fd.append("file", $scope.documentArray[i].document);
            fd.append("filename", $scope.documentArray[i].documentName);
            fd.append("propid", $scope.pId);
            fd.append("moduleid", 'PROPERTY');
            fd.append("fileType", 'document');
            fd.append("seqNo", $scope.documentArray[i].counter);
            fd.append("filedesc", $scope.documentArray[i].documentDesc);
            fd.append("filetags", $scope.documentArray[i].documentTags);
            fd.append("idDeleted", $scope.documentArray[i].isDeleted);
        }
        $scope.showLoader = true;
        propertyService.saveFiles(fd, '/api/propertydocument', function (status, resp) {
            if (status) {
                $scope.showLoader = false;
                swal("SUCCESS", "Property Documents saved successfully.", "success");
                if (isClose) {
                    //Below code is added by ASR 
                    // this checks if the property is edited then it
                    //redirects to property view page instead of property listing page

                    if ($scope.PropertyEntryMode == "Edit") {
                        $location.path('/propertyProfile/' + $scope.pId);
                    }
                    else {
                        $location.path('/properties');
                    }
                    //Below code is Commented by ASR  AS it redirects to property listing page in all cases
                    //$location.path('/properties');
                }
            }
            else {

                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to add documents.", "error");
                }
                $scope.showLoader = false;
            }
        });

    }
    //#endregion


    //#region upload Documents
    $scope.uploadDocuments = function (pId) {
        var fd = new FormData();
        for (var i = 0; i < $scope.documentArray.length; i++) {
            fd.append("file", $scope.documentArray[i].document);
            fd.append("filename", $scope.documentArray[i].documentName);
            fd.append("propid", pId);
            fd.append("moduleid", 'PROPERTY');
            fd.append("fileType", 'document');
            fd.append("seqNo", $scope.documentArray[i].counter);
            fd.append("filedesc", $scope.documentArray[i].documentDesc);
            fd.append("filetags", $scope.documentArray[i].documentTags);
            fd.append("idDeleted", $scope.documentArray[i].isDeleted);
        }

        propertyService.saveFiles(fd, '/api/propertydocument', function (status, resp) {
            if (status) {
                swal("SUCCESS", "Document uploaded successfully.", "success");
                $scope.closeuploadDocument();
            }
            else {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to upload documents.", "error");
                }
                $scope.showLoader = false;
            }
        });
    }

    //#endregion
    //#region "Owner Events"
    $scope.objPerson = {
        pTypeId: 0,
        pFirstName: '',
        pLastName: '',
        pEmail: '',
        pPhoneNo: ''
    }

    $scope.getStatusData = function () {
        $scope.showLoader = true;
        masterdataService.getStatus(function (result, data) {
            if (result) {
                $scope.showLoader = false;
                $scope.statuslist = $rootScope.StatusList.filter(function (c) {
                    return c.ModuleName == 'Property';//For Property Status
                });
                for (var i in $scope.statuslist) {
                    $scope.useStatus.push({ counter: i, id: $scope.statuslist[i].Id, name: $scope.statuslist[i].Name, checked: true });
                }
            }
            else {
                $scope.showLoader = false;
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", "Unable to fetch Status.", "error");
                }
            }
        });
    }

    var ownerCounter = 0;

    $scope.isChecked = function () {
        return $scope.selected.length === $scope.items.length;
    };

    $scope.addOwner = function ($event) {
        if (!angular.isUndefined($scope.pOwner) && $scope.pOwner != null) {
            //ownerCounter++;
            ownerCounter = $scope.ownerArray.length + 1;
            var ispriowner = '0';
            ispriowner = $scope.pOwner.isprimaryowner;
            if ($scope.ownerArray.length == 0)
                ispriowner = '1';
            $scope.ownerArray.push({ counter: ownerCounter, showRemove: true, ownerid: $scope.pOwner.pId, ownername: $scope.pOwner.pFullName, ownerphoneno: $scope.pOwner.pPhoneNo, owneremail: $scope.pOwner.pEmail, isprimaryowner: ispriowner, isNew: true, isDeleted: false });
            //$event.preventDefault();
        }
    };

    $scope.removeOwner = function (index) {
        for (var i = 0; i < $scope.ownerArray.length; i++) {
            if ($scope.ownerArray[i].counter == index) {
                if ($scope.ownerArray[i].isNew) {
                    $scope.ownerArray.splice(i, 1);
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.
                    $scope.ownerArray[i].isDeleted = true;
                }
            }
        }
    }
    //#endregion


    $scope.showfilter = function () {
        if ($(".vt-filters").css('right') == '-186px') {
            $(".vt-filters").animate({ right: '0' }, 500);
            $(".vt-filterdiv").animate({ width: "185px" });
        }
    };

    $scope.closefilter = function () {
        $(".vt-filters").animate({ right: '-186px' }, 500);
        $(".vt-filterdiv").animate({ width: "140px" });
    };

    $scope.resetHomeWarrantyData = function () {
        $scope.pHomeWarrantyCompanyName = '';
        $scope.pHomeWarrantyCompanyPhoneNo = '';
        $scope.pHomeWarrantyStartDate = '';
        $scope.pHomeWarrantyEndDate = '';
        $scope.pHomeWarrantySendReminder = false;
    }

    $scope.resetHomeInsuranceData = function () {
        $scope.pHomeInsuranceCompanyName = '';
        $scope.pHomeInsuranceCompanyPhoneNo = '';
        $scope.pHomeInsuranceStartDate = new Date();
        $scope.pHomeInsuranceEndDate = new Date();
        $scope.pHomeInsuranceSendReminder = false;
    }

    $scope.useStatus = [];
    $scope.useCategories = [];
    $scope.useType = [];
    var isStatusSelected = true;
    var isCategorySelected = true;
    var isTypeSelected = true;

    $scope.filterBySelection = function () {

        var isStatusSelected = true;
        var isCategorySelected = true;
        var isTypeSelected = true;
        return function (p) {

            if (!angular.isUndefined(p)) {

                isStatusSelected = true;
                isTypeSelected = true;
                if (!angular.isUndefined($scope.statuslist) && !angular.isUndefined($scope.useStatus) && $scope.useStatus.length > 0) {
                    for (var i in $scope.statuslist) {
                        if (p.pStatusId == $scope.useStatus[i].id) {
                            isStatusSelected = $scope.useStatus[i].checked;
                            break;
                        }
                    }
                }
                if (!angular.isUndefined($scope.pPropertyCategoryFilterId) && $scope.pPropertyCategoryFilterId > 0) {
                    if (p.pCategoryId == $scope.pPropertyCategoryFilterId) {
                        isCategorySelected = true;
                    }
                    else
                        isCategorySelected = false;
                }
                if (!angular.isUndefined(p.pTypeId) && !angular.isUndefined($scope.useType) && $scope.useType.length > 0) {

                    for (var i in $scope.useType) {
                        if (p.pTypeId == $scope.useType[i].id) {
                            isTypeSelected = $scope.useType[i].checked;
                            break;
                        }
                    }
                }
                return isStatusSelected && isCategorySelected && isTypeSelected; //|| ($scope.isAllPropertySelected && item != 'UI');
            }
            else
                return true;
        };
    };

    $scope.disableCreateLease = function () {
        var isDisable = !($scope.selectedProperty.pStatusId == 4 || $scope.selectedProperty.pStatusId == 6 || $scope.selectedProperty.pStatusId == 7);

        if (isDisable) {
            $("#btnCreateLease").removeClass("vt-btn1");
            $("#btnCreateLease").addClass("vt-btn2");
        }
        return isDisable;
    }

    $scope.GetLeaseByPropertyId = function (callback) {
        $scope.showLoader = true;
        leaseService.getLeaseByPropertyId($scope.viewPropertyId, function (status, data) {
            if (status) {
                $scope.showLoader = false;
                $scope.propertyLeaseList = data;
                callback(data);
            }
            else {
                $scope.showLoader = false;
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "error in getAllLeases", "error");
                }
            }
        });
    };

    $scope.FilterLeaseByStatus = function (LeaseStatus) {
        $scope.lstLease = [];
        $scope.leaseslist = null;
        if ($scope.propertyLeaseList.length > 0) {

            //$scope.lstLease = $scope.propertyLeaseList.filter(lease => lease.pLeaseActive == LeaseStatus);
            $scope.lstLease = $scope.propertyLeaseList.filter(function (lease) { return lease.pLeaseActive === LeaseStatus });
           
            $scope.leaseslist = $scope.lstLease;
            $scope.leasesjson = angular.toJson($scope.leaseslist, true);
        }
        else {
            $scope.GetLeaseByPropertyId(function (data) {


               // $scope.lstLease = $scope.propertyLeaseList.filter(lease => lease.pLeaseActive == LeaseStatus);
                $scope.lstLease = $scope.propertyLeaseList.filter(function (lease) { return lease.pLeaseActive === LeaseStatus });
                $scope.leaseslist = $scope.lstLease;
                $scope.leasesjson = angular.toJson($scope.leaseslist, true);
            });
        }
    }


    $scope.split = function (input, splitChar, splitIndex) {
        if (!angular.isUndefined(input) && input != null && input.length > 0) {
            return input.split(splitChar)[splitIndex];
        }
    };
    $scope.uploadDocument = function () {
        $("#uploadDocument").modal('show');
    }
    $scope.closeuploadDocument = function () {
        $("#uploadDocument").modal('hide');
    }
    $scope.GetTransactionByPersonAndProperty = function (fromDate, toDate, personId, propoertyId, personType) {
        $scope.showLoader = true;
        financeService.getTransactionByPersonAndProperty($scope.managerid, $scope.isManager, $scope.pmid, fromDate, toDate, personId, propoertyId, personType).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    //console.log('getTransactionByPersonAndProperty Data', response.Data);

                    if (propoertyId == 0) {
                        $scope.lstPropertByManagerAndOwner = response.Data.MdlPropertOwnersWithManager;
                        var addNewOption_All = angular.copy($scope.lstPropertByManagerAndOwner[0]);
                        addNewOption_All.ownerId = 0;
                        addNewOption_All.propertyId = 0;
                        addNewOption_All.propertyName = " All";
                        $scope.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                        $scope.TransectionFilterParameters.property = addNewOption_All;
                    }
                    $scope.lstTransections_Filtered = response.Data.lstMdlTransactionWithWriteOff;
                    // $scope.ChartArray($scope.lstTransections_Filtered);
                    // $scope.ChartArrayForManager($scope.lstTransections_Filtered);  

                    $scope.ChartArrayForManager(response.Data.lstIncomeExpenseLiabilityChartData);
                    $scope.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    $scope.LoadPropertyListFunction = function () {

        $scope.getProperties(function (data) { });
        $scope.randomNumber = Math.floor(100000 + Math.random() * 900000);
    }


    $scope.LoadPropertyProfile = function () {

        $scope.propertyLeaseList = [];
        $scope.tenantEmail = '';
        $scope.PropertyHasActiveLease = false;

        $scope.randomNumber = Math.floor(100000 + Math.random() * 900000);

        if (!angular.isUndefined($routeParams.PropertyNum)) {

            $scope.lstCurrentPeriod = angular.copy(Dateranges);
            $scope.selectCurrentPeriod = $scope.lstCurrentPeriod[0];

            $scope.viewPropertyId = $routeParams.PropertyNum;
            $scope.getProperties(function (data) {
                $scope.ShowPropertyView($routeParams.PropertyNum);
            });
            $scope.GetLeaseByPropertyId(function (data) {
              //  var ActiveLeases = data.filter(l => l.pLeaseActive == "Active");
                var ActiveLeases = data.filter(function (l) { return l.pLeaseActive == "Active" });
                if (ActiveLeases.length > 0) {
                    $scope.PropertyHasActiveLease = true;
                    $scope.tenantEmail = ActiveLeases[0].TenantEmail;
                }
            });
        }
        else {
            $location.path("/properties");
        }
    }

    $scope.LoadAddEdit = function () {

        if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum != -1) {
            $scope.GetPropertyDetailByPropertyId($routeParams.PropertyNum);
            $scope.PropertyEntryMode = "Edit";
            $rootScope.EditedPropertyIdForShowProfile = $routeParams.PropertyNum;   // Here Set the property ID In global variable so if user saves
        }
        else {
            $scope.PropertyEntryMode = "Add";
            $scope.getMasterData();
            //$scope.GetHOAByPMID(function (data) {
            //    console.log('LoadAddEdit data 222', data);
            //    $scope.hoalist = [];
            //    $scope.hoalist = angular.copy(data);
            //});
        }
        $scope.getCompanyData();
    }


    $scope.getMasterData = function () {

        $scope.getPropertyCategories();
        $scope.showLoader = true;
        masterdataService.getPropertyTypes(function (result, data) {
            if (result) {
                $scope.showLoader = false;
                $scope.propertytypeslist = data;
                if (!angular.isUndefined($scope.propertytypeslist)) {

                    $scope.propertytypesfilteredlist = $scope.propertytypeslist.filter(function (c) {
                        return (c.CategoryId == $scope.pPropertyCategoryId);
                    });
                }
                for (var i = 0; i < $scope.propertytypesfilteredlist.length; i++) {
                    if ($scope.pPropertyTypeId == $scope.propertytypesfilteredlist[i].Id) {
                        $scope.pPropertyType = $scope.propertytypesfilteredlist[i];
                        break;
                    }
                }
                isCall1 = true;
            }
            else {
                $scope.showLoader = false;
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to fetch Property Types", "error");
                }
            }
        });
        $scope.showLoader = true;
        masterdataService.GetPersonOwnerByPMID($rootScope.userData.ManagerDetail.pId).
               success(function (response) {
                   $scope.showLoader = false;
                   $rootScope.OwnerPersonList = response.Data;
                   $scope.ownerlist = $rootScope.OwnerPersonList;

               }).error(function (data, status) {
                   $scope.showLoader = false;
                   if (status == 401) {
                       commonService.SessionOut();
                   }
               }).finally(function (data) {
                   $scope.showLoader = false;
               });
        $scope.showLoader = true;
        masterdataService.getPerson(function (result, data) {
            if (result) {
                $scope.showLoader = false;
                $scope.GetHOAByPMID(function (data) {
                    $scope.hoalist = [];
                    $scope.hoalist = data;
                });
            }
            else {
                $scope.showLoader = false;
                if (data == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to fetch Owners & Associations.", "error");
                }
            }
        });
        if (angular.isUndefined($rootScope.CountriesList) || $rootScope.CountriesList == null) {

            $scope.GetCountry(function (data) {
                if ($scope.PropertyEntryMode == "Edit") {
                    $scope.GetStatesByCountryId($scope.pCountryId, function (data) {
                        $scope.GetCountyByStateId($scope.pStateId, function (data) {
                            $scope.GetCityByStateOrCounty($scope.pStateId, $scope.pCountyId, function (data) { });
                        });
                    });
                }
            });
        } else {
            $scope.countrieslist = $rootScope.CountriesList;
            for (var i = 0; i < $scope.countrieslist.length; i++) {
                if ($scope.pCountryId == $scope.countrieslist[i].Id) {
                    $scope.pCountry = $scope.countrieslist[i];

                    if ($scope.PropertyEntryMode == "Edit") {
                        $scope.GetStatesByCountryId($scope.pCountryId, function (data) {
                            $scope.GetCountyByStateId($scope.pStateId, function (data) {
                                $scope.GetCityByStateOrCounty($scope.pStateId, $scope.pCountyId, function (data) { });
                            });
                        });
                    }
                    else
                        break;
                }
            }
            isCall3 = true;
        }
        $scope.showLoader = false;
    }

    $scope.GetCountry = function (callback) {
        $scope.showLoader = true;
        propertyService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.countrieslist = response;
                  $rootScope.CountriesList = response;

                  if ($scope.PropertyEntryMode == "Edit") {
                      for (var i = 0; i < $scope.countrieslist.length; i++) {
                          if ($scope.pCountryId == $scope.countrieslist[i].Id) {
                              $scope.pCountry = $scope.countrieslist[i];
                              break;
                          }
                      }
                  }
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              swal("ERROR", MessageService.serverGiveError, "error");
              $scope.showLoader = false;
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetStatesByCountryId = function (CountryId, callback) {

        $scope.showLoader = true;
        propertyService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.stateslist = response;
                  if ($scope.PropertyEntryMode == "Edit") {
                      for (var i = 0; i < $scope.stateslist.length; i++) {
                          if ($scope.pStateId == $scope.stateslist[i].Id) {
                              $scope.pState = $scope.stateslist[i];
                              break;
                          }
                      }
                  }
                  callback(response);
              }
              else {
                  $scope.showLoader = false;
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              $scope.showLoader = false;
              swal("ERROR", MessageService.serverGiveError, "error");
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCountyByStateId = function (stateId, callback) {

        $scope.showLoader = true;
        propertyService.GetCountyByState(stateId).
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.allcountieslist = response.Data;
                  $scope.countieslist = response.Data;
                  if ($scope.PropertyEntryMode == "Edit") {

                      for (var i = 0; i < $scope.countieslist.length; i++) {
                          if ($scope.pCountyId == $scope.countieslist[i].id) {
                              $scope.pCounty = $scope.countieslist[i];
                              break;
                          }
                      }
                  }
                  callback(response);
              }
              else {
                  $scope.showLoader = false;
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              $scope.showLoader = false;
              swal("ERROR", MessageService.serverGiveError, "error");
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCityByStateOrCounty = function (StateId, CountyId, callback) {

        $scope.showLoader = true;
        propertyService.getCityByStateOrCounty(StateId, CountyId).
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.allcitieslist = response.Data;
                  $scope.citieslist = response.Data;
                  if ($scope.PropertyEntryMode == "Edit") {
                      if (!angular.isUndefined($scope.citieslist)) {
                          for (var i = 0; i < $scope.citieslist.length; i++) {

                              if ($scope.pCityId == $scope.citieslist[i].Id) {
                                  $scope.pCity = $scope.citieslist[i];
                                  break;
                              }
                          }
                      }
                  }
                  callback(response.Data);
              }
              else {
                  $scope.showLoader = false;
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              $scope.showLoader = false;
              swal("ERROR", MessageService.serverGiveError, "error");
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.FilterTransections = function (checkValue, selectedPeriod) {

        if (selectedPeriod != null) {
            if (selectedPeriod.date == 'custom' && checkValue == undefined) {

                $scope.lstTransections = [];
                $scope.lstTransections_Filtered = [];
                $scope.IncmExpenseChartArray = [];
                $scope.PieChartCategoryArray = [];

                if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                    //$scope.PieChartLoad();
                    $scope.IncmChartLoad();
                }
                return;
            }
            $scope.current_toDate = new Date();
            $scope.select_fromDate = new Date();
            if (selectedPeriod.date == 'custom' && checkValue == 'custom') {

                $scope.current_toDate = $scope.toDate;
                $scope.select_fromDate = $scope.fromDate;
            }
            else {

                if (selectedPeriod.Period == 'Annual') {
                    $scope.select_fromDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {
                    $scope.select_fromDate = selectedPeriod.Startdate;
                    $scope.current_toDate = selectedPeriod.EndDate;
                }
            }
            $scope.lstTransections_Filtered = [];
            $scope.IncmExpenseChartArray = [];
            $scope.PieChartCategoryArray = [];

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                //$scope.PieChartLoad();
                $scope.IncmChartLoad();
            }

            $scope.TransectionFilterParameters.owner = undefined;
            $scope.TransectionFilterParameters.property = undefined;
            $scope.GetAllTransections($scope.select_fromDate, $scope.current_toDate);
        }
        else {
            $scope.lstTransections = [];
            $scope.IncmExpenseChartArray = [];
            $scope.PieChartCategoryArray = [];
            $scope.lstTransections_Filtered = [];

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                //$scope.PieChartLoad();
                $scope.IncmChartLoad();
            }
            $scope.TransectionFilterParameters.owner = undefined;
            $scope.TransectionFilterParameters.property = undefined;
        }
    };

    $scope.GetAllTransections = function (fromDate, toDate) {

        $scope.showLoader = true;
        //financeService.getAllTransections($scope.managerid, fromDate, toDate).
        if (PersonTypeEnum.owner == $rootScope.userData.personType) {
            $scope.personId = $rootScope.userData.poid;
        }

        financeService.getAllTransections($scope.managerid, $scope.isManager, fromDate, toDate, $scope.pmid, 0, $scope.selectedProperty.pId, $scope.personType).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    $scope.lstTransections = response.Data;
                    $scope.lstTransections_Filtered = angular.copy($scope.lstTransections);
                    //  $scope.ChartArray($scope.lstTransections_Filtered);
                    $scope.ChartArrayForManager($scope.lstTransections_Filtered);
                    $scope.personId = $rootScope.userData.personId;
                    if (PersonTypeEnum.tenant == $rootScope.userData.personType) {
                       // $scope.lstTransections_Filtered = $scope.lstTransections.filter(transections => transections.propertyId === parseInt($rootScope.selectedPropertyId));
                        $scope.lstTransections_Filtered = $scope.lstTransections.filter(function (transections) { return transections.propertyId === parseInt($rootScope.selectedPropertyId)});
                    }
                    $scope.TransectionFilterParameters.owner = $scope.CopylstPropertOwnersByManager[0];
                    $scope.TransectionFilterParameters.property = $scope.CopylstPropertByManagerAndOwner[0];
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", response.Message, "error");
                    //$scope.personId = $rootScope.userData.personId;
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $scope.showLoader = false;
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }).finally(function (data) {
                $scope.showLoader = false;
                $scope.personId = $rootScope.userData.personId;
            });
    };

    $scope.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    $scope.ChangeCustomToDate = function () {
        $scope.toDate1 = $scope.fromDate1;
    }

    $scope.ButtonClick_Excel = function (id, excelName) {
        $(id).table2excel({
            filename: excelName
        });
    }
    $scope.ButtonClick_Pdf = function (id, pdfName, headerName) {
        HtmlTableToPdf(id, pdfName, headerName, '', '');
    }
    $scope.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        $scope.pCounty = undefined;
        var countyId = 0;
        $scope.GetCityByStateOrCounty($scope.pState.Id, countyId, function (data) { });
    };

    $scope.sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop];
        }, 0);
    };

    $scope.ChartArrayForManager = function (modal) {
        $scope.ViewIncomeCategory = 'Income';
        $scope.IncmChartArray = [];
        $scope.TotalIncome = 0;
        $scope.RealisedIncome = 0;
        $scope.UnrealisedIncome = 0;

        $scope.groupByCategory = $filter('groupBy')(modal, 'Category');        // Category => Income,Expense
        angular.forEach($scope.groupByCategory, function (value, key) {
            var innergroup = $filter('groupBy')(value, 'Type');            // Type => Earned, Pending
            if (key == 'Income') {
                $scope.TotalIncome = $scope.sum(value, 'Amount');
                //---------------------------------------------------------------------------------------------------
                // GET total income (Realised + Unrealised)

                var IncomeGroup = $filter('groupBy')(value, 'CategoryName');
                angular.forEach(IncomeGroup, function (IncomeGroupValue, key3) {

                    var IncomeGroupTotal = $scope.sum(IncomeGroupValue, 'Amount');
                    if (parseFloat(IncomeGroupTotal) > 0) {
                        var IncmArr = [];
                        IncmArr.push(key3, IncomeGroupTotal);
                        $scope.IncmChartArray.push(IncmArr);
                    }
                });
                //----------------------------------------------------------------------------------------------------
                angular.forEach(innergroup, function (innerValue, key2) {
                    if (key2 == 'Earned') {
                        $scope.RealisedIncome = $scope.sum(innerValue, 'Amount');
                    }
                    else {
                        $scope.UnrealisedIncome = $scope.sum(innerValue, 'Amount');
                    }
                });
            }
        });
        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            $scope.IncmChartLoad();
            //$scope.EarnedIncmChartLoad();
            //$scope.PendingIncmChartLoad();
            //$scope.ExpenseChartLoad();
            //$scope.LiabitilyChartLoad();
        }
    };

    $scope.ExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawExpenseChart);

        function drawExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.ExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('ExpenseChart'));
            chart.draw(data, options);
        }
    }

    $scope.RealisedExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawRealisedExpenseChart);

        function drawRealisedExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.RealisedExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart (Realised)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('RealisedExpenseChart'));
            chart.draw(data, options);
        }
    }

    $scope.UnRealisedExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawUnRealisedExpenseChart);

        function drawUnRealisedExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.UnRealisedExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart (Unrealised)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('UnrealisedExpenseChart'));
            chart.draw(data, options);
        }
    }


    $scope.IncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawIncmChart);

        function drawIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.IncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'My Income ($)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('IncmChart'));
            chart.draw(data, options);
        }
    }

    $scope.EarnedIncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawEarnedIncmChart);

        function drawEarnedIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.EarnedIncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart (Earned)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('EarnedIncmChart'));
            chart.draw(data, options);
        }
    }

    $scope.PendingIncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawPendingIncmChart);

        function drawPendingIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.PendingIncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart (Pending)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('PendingIncmChart'));
            chart.draw(data, options);
        }
    }


    $scope.CreateLiabilityChart = function (model) {
        //console.log('CreateLiabilityChart model', model);

        //var ArrTotalInvoicedAmountToOwner = model.filter(transections => transections.amountType == 'TotalInvoicedAmountToOwner');
        //var ArrTotalPaidAmountByOwner = model.filter(transections => transections.amountType == 'TotalPaidAmountByOwner');
        //var ArrTotalAmountPMHasToPayToOwner = model.filter(transections => transections.amountType == 'TotalAmountPMHasToPayToOwner');
        //var ArrTotalAmountPMPaidToOwner = model.filter(transections => transections.amountType == 'TotalAmountPMPaidToOwner');

        var ArrTotalInvoicedAmountToOwner = model.filter(function (transections) { return transections.amountType == 'TotalInvoicedAmountToOwner' });
        var ArrTotalPaidAmountByOwner = model.filter(function (transections) { return transections.amountType == 'TotalPaidAmountByOwner' });
        var ArrTotalAmountPMHasToPayToOwner = model.filter(function (transections) { return  transections.amountType == 'TotalAmountPMHasToPayToOwner'});
        var ArrTotalAmountPMPaidToOwner = model.filter(function (transections) { return  transections.amountType == 'TotalAmountPMPaidToOwner'});
       
        var TotalInvoicedAmountToOwner = $scope.sum(ArrTotalInvoicedAmountToOwner, 'amount');
        var TotalPaidAmountByOwner = $scope.sum(ArrTotalPaidAmountByOwner, 'amount');
        var TotalAmountPMHasToPayToOwner = $scope.sum(ArrTotalAmountPMHasToPayToOwner, 'amount');
        var TotalAmountPMPaidToOwner = $scope.sum(ArrTotalAmountPMPaidToOwner, 'amount');

        $scope.LiabitilyChartArray = [];
        var LiabilityArr = [];
        var NetAmountOwnerHasToPay = TotalInvoicedAmountToOwner - TotalPaidAmountByOwner;
        var NetAmountPMHasToPayToOwner = TotalAmountPMHasToPayToOwner - TotalAmountPMPaidToOwner;
        LiabilityArr.push('Paid Liability', TotalAmountPMPaidToOwner);
        $scope.LiabitilyChartArray.push(LiabilityArr);
        var Liability = NetAmountPMHasToPayToOwner - NetAmountOwnerHasToPay;
        LiabilityArr = [];
        LiabilityArr.push('Liability', Liability);
        $scope.LiabitilyChartArray.push(LiabilityArr);
        if (Liability > 0) {
            $scope.LiabitilyChartLoad();
        }
        else {
            $scope.LiabitilyChartArray = [];
        }
    }

    $scope.LiabitilyChartLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawLiabilityChart);

        function drawLiabilityChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows($scope.LiabitilyChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'My Liability ($)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('LiabitilyChart'));
            chart.draw(data, options);
        }
    }

    $scope.GrossAreaChange = function (event) {
        if (event.keyCode == 8 || event.keyCode == 46) {
            if (isNaN(parseInt($scope.pGrossArea)) || (parseInt($scope.pGrossArea) <= 0)) {
                $scope.pRentableArea = 0;
            }
        }
        //else {
        //    if (!isNaN(parseInt($scope.pGrossArea)) && (parseInt($scope.pGrossArea) > 0) && !isNaN(parseInt($scope.pRentableArea)))
        //    {
        //        if (parseInt($scope.pGrossArea) < parseInt($scope.pRentableArea))
        //        {
        //            $scope.pRentableArea = $scope.pGrossArea;
        //        }
        //    }
        //}
    }

    $scope.LoadPropertyFinancial = function (PropoertyId) {
        $scope.showLoader = true;
        $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};
        var LeaseId = 0;            // LeaseId will always be 0 for property finance
        propertyService.getOwnerIncomeExpenseByPropertyIdLeaseId(PropoertyId, LeaseId).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = angular.copy(response.Data);
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $scope.showLoader = false;
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }).finally(function (data) {
                $scope.showLoader = false;
                $scope.personId = $rootScope.userData.personId;
            });
    }

    var delayInMilliseconds = 2000; //2 second
    setTimeout(function () {

        $scope.showLoader = false;
    }, delayInMilliseconds);

});

