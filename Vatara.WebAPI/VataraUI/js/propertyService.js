var myApp = angular.module('Myapp');

myApp.service('propertyService', function ($http) {
    var propertieslist = [];
    var documentsOfselectedProperty = [];

    this.deleteProperty = function (pId, onComplete) {

        $http({ method: 'DELETE', url: '/api/properties/DeleteProperty?id=' + pId })
            .success(function (data, status, headers, config) {

                // remove the id from the list
                for (var i = 0; i < propertieslist.length; i++) {
                    if (propertieslist[i].pId == pId) {
                        propertieslist.splice(i, 1);
                    }
                }
                onComplete(true, status, propertieslist);
            })
            .error(function (data, status, headers, config) {
                //alert(status);
                onComplete(false, status, propertieslist);
            });
    };
    this.getProperty = function (pId, onComplete) {

        // for (var i = 0; i < propertieslist.length; i++) {
        //     if (propertieslist[i].pId == pId) {
        //         return propertieslist[i];
        //     }
        // }
        // return null;

        $http({ method: 'GET', url: '/api/properties/Get?id=' + pId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getDocumentsOfSelectedProperty = function () {
        return documentsOfselectedProperty;
    };
    this.getAllProperties = function (personId, onComplete) {
        $http({ method: 'GET', url: '/api/properties/GetAllProperty?id=' + personId })
            .success(function (data, status, headers, config) {
                propertieslist = data;
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };
    this.getPropertyOwners = function (propId, onComplete) {
        $http({ method: 'GET', url: '/api/propertyowners/Get?id=' + propId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getPropertyPhotos = function (propId, onComplete) {
        $http({ method: 'GET', url: '/api/propertyphoto/Get?id=' + propId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };
    this.getPropertyDocuments = function (propId, onComplete) {
        $http({ method: 'GET', url: '/api/propertydocument/Get?id=' + propId })
            .success(function (data, status, headers, config) {
                documentsOfselectedProperty = data;
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.addNewProperty = function (propData, onComplete) {
        $http({ method: 'post', data: propData, url: '/api/properties' })
            .success(function (data, status, headers, config) {
                propertieslist.push(propData);
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };
    this.saveFiles = function (files, url, onComplete) {

        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.saveProperty = function (data) {
        var responce = $http.post(
            '/api/Properties/SaveProperty',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.UpdateProperty = function (data) {
        var responce = $http.post(
            '/api/Properties/UpdateProperty',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.UpdatePropertyAdditionalInfo = function (data) {
        var responce = $http.post(
            '/api/Properties/UpdatePropertyAdditionalInfo',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.GetPropertyDetailByPropertyId = function (PropertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/Properties/GetPropertyDetailByPropertyId",
            params: {
                PropertyId: PropertyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.GetCityByCounty = function (id) {
        var responce = $http({
            method: "Get",
            url: "/api/City/GetCityByCounty",
            params: {
                id: id
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getCityByStateOrCounty = function (stateId, countyId) {
        var responce = $http({
            method: "Get",
            url: "/api/City/GetCityByStateOrCounty",
            params: {
                StateId: stateId,
                CountyId: countyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.GetCountyByState = function (stateId) {
        var responce = $http({
            method: "Get",
            url: "/api/County/GetCountyByState",
            params: {
                stateId: stateId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getStates = function () {
        var responce = $http({
            method: "Get",
            url: "/api/State/Get",
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getCountries = function () {

        var responce = $http({
            method: "Get",
            url: "/api/Country/Get",
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.SaveHOA = function (data) {
        var responce = $http.post(
            '/api/Properties/SaveHOA',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.CheckEmailAvailability = function (EmailId) {
        var responce = $http({
            method: "Get",
            url: "/api/Properties/CheckEmailAvailability",
            params: { email: EmailId },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.getHOAByPMID = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Properties/GetHOAByPMID",
            params: { PMID: PMID },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.getPropertiesByPMID = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Properties/GetPropertiesByPMID",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getOwnerIncomeExpenseByPropertyIdLeaseId = function (PropoertyId, LeaseId) {
        var responce = $http({
            method: "Get",
            url: "/api/Transaction/GetOwnerIncomeExpenseByPropertyIdLeaseId",
            params: {
                PropoertyId: PropoertyId,
                LeaseId: LeaseId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getOwnerByPOID = function (POID) {
        var responce = $http({
            method: "Get",
            url: "/api/Properties/GetOwnerByPOID",
            params: {
                POID: POID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }
});

