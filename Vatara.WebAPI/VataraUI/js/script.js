$(document).ready(function () {
    google.charts.load('current', { 'packages': ['corechart'] });
    // google.charts.setOnLoadCallback(drawChart);
    //function drawChart() {
    //    var data = google.visualization.arrayToDataTable([
    //      ['Task', 'Hours per Day'],
    //      ['Lawn Maintenance', 6],
    //      ['Gas', 4],
    //      ['Heat', 3],
    //      ['Other Maintenance', 5],
    //      ['Repairing', 1]
    //    ]);

    //    var options = {
    //        title: 'Total Expenditure'
    //    };
    //    //var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
    //    chart2.draw(data, options);

    //    var data = google.visualization.arrayToDataTable([
    //      ['Task', 'Hours per Day'],
    //      ['Income', 11],
    //      ['Expenditure', 2]
    //    ]);
    //    var options = {
    //        title: 'Cash Flow'
    //    };
    //    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    //    chart.draw(data, options);
    //}

    //google.charts.load('current', { 'packages': ['corechart'] });
    //google.charts.setOnLoadCallback(drawChart2);
    //function drawChart2() {
    //    var data = google.visualization.arrayToDataTable([
    //      ['Task', 'Hours per Day'],
    //      ['Invoice Amount in Draft', 6],
    //      ['Invoice Amount Sent', 4],
    //      ['Invoice Amount Received', 3],
    //      ['Invoice Amount to be paid', 5],
    //      ['Invoice Amount Paid', 1]
    //    ]);

    //    var options = {
    //        title: 'Total Expenditure'
    //    };
    //    //var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
    //    chart2.draw(data, options);
    //}


    /*$("md-sidenav").mouseenter(function(){
			$(".top-bar, .vt-breadcrumb").css({
				padding: "0 0 0 110px"
			});			
		}).mouseleave(function(){
			$(".top-bar, .vt-breadcrumb").css({
				padding: "0 0 0 0px"
			});			
		});
	*/
    var window_hgt = $(window).height() - 185;
    var slides = $(".msg_div").css({ "height": window_hgt });

    //var screen_width = $(window).width();
    //alert(screen_width);


    var regex = /^(.+?)(\d+)$/i;
    var cloneIndex = $(".clonedInput").length;

    function clone() {

        $(this).parents(".clonedInput").clone()
			.appendTo(".unit-div")
			.attr("id", "clonedInput" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone', clone)
			.on('click', 'button.remove', remove);
        cloneIndex++;
    }
    function remove() {
        $(this).parents(".clonedInput").remove();
    }

    //Script for Unit Details
    function clone() {
        $(this).parents(".clonedInput1").clone()
			.appendTo(".unit_details")
			.attr("id", "clonedInput1" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone', clone)
			.on('click', 'button.remove', remove);
        cloneIndex++;
    }
    function remove() {
        $(this).parents(".clonedInput1").remove();
    }
    $("a.clone").on("click", clone);
    $("button.remove").on("click", remove);

    //Script for Documents Details
    function clone1() {
        $(this).parents(".clonedInput2").clone()
			.appendTo(".doc_details")
			.attr("id", "clonedInput2" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone1', clone1)
			.on('click', 'button.remove1', remove1);
        cloneIndex++;
    }
    function remove1() {
        $(this).parents(".clonedInput2").remove();
    }
    $("a.clone1").on("click", clone1);
    $("button.remove1").on("click", remove1);

    //Script for Tenant Details
    function clone2() {
        $(this).parents(".clonedInput3").clone()
			.appendTo(".tenant_details")
			.attr("id", "clonedInput3" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone2', clone2)
			.on('click', 'button.remove2', remove2);
        cloneIndex++;
    }
    function remove2() {
        $(this).parents(".clonedInput3").remove();
    }
    $("a.clone2").on("click", clone2);
    $("button.remove2").on("click", remove2);

    //Script for Cluses options Details
    function clone3() {
        $(this).parents(".clonedInput4").clone()
			.appendTo(".clause_details")
			.attr("id", "clonedInput4" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone3', clone3)
			.on('click', 'button.remove3', remove3);
        cloneIndex++;
    }
    function remove3() {
        $(this).parents(".clonedInput4").remove();
    }
    $("a.clone3").on("click", clone3);
    $("button.remove3").on("click", remove3);

    //Script for options Details
    function clone4() {
        $(this).parents(".clonedInput5").clone()
			.appendTo(".options_details")
			.attr("id", "clonedInput5" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone4', clone4)
			.on('click', 'button.remove4', remove4);
        cloneIndex++;
    }
    function remove4() {
        $(this).parents(".clonedInput5").remove();
    }
    $("a.clone4").on("click", clone4);
    $("button.remove4").on("click", remove4);

    //Script for create work order
    function clone5() {
        $(this).parents(".clonedInput6").clone()
			.appendTo(".creatework")
			.attr("id", "clonedInput6" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone5', clone5)
			.on('click', 'button.remove5', remove5);
        cloneIndex++;
    }
    function remove5() {
        $(this).parents(".clonedInput6").remove();
    }
    $("a.clone5").on("click", clone5);
    $("button.remove5").on("click", remove5);


    //Script for create property
    function clone6() {
        $(this).parents(".clonedInput7").clone()
			.appendTo(".createpty")
			.attr("id", "clonedInput7" + cloneIndex)
			.find("*")
			.each(function () {
			    var id = this.id || "";
			    var match = id.match(regex) || [];
			    if (match.length == 3) {
			        this.id = match[1] + (cloneIndex);
			    }
			})
			.on('click', 'a.clone6', clone6)
			.on('click', 'button.remove6', remove6);
        cloneIndex++;
    }
    function remove6() {
        $(this).parents(".clonedInput7").remove();
    }
    $("a.clone6").on("click", clone6);
    $("button.remove6").on("click", remove6);


    //alert(window_width);


    $('.vt-srch-icon').click(function (e) {
        //e.stopPropagation();
        //$(".sb-search").css({ width: "390px" });

        //var window_width = $(window).width();
        //if (window_width < 1024) {
        //    e.stopPropagation();
        //    $(".sb-search").css({ width: "260px" });
        //}

    });
    $('.custom-tab').click(function (e) {
        $(".menu2 li").removeClass("active");
        $(".menu2 li:last-Child").addClass("active");
    });

    $('.image-upload input').on('change', function () {

        //var classes = $(this).parent().closest('div').attr('class').split(' '); // this gets the parent classes.
        //alert(classes);
        $(this).parent().closest('div').addClass("inptactive");
    });


    $('ul.term-list').each(function () {

        var LiN = $(this).find('li').length;
        if (LiN > 3) {
            $('li', this).eq(2).nextAll().hide().addClass('toggleable');
            $(this).append('<li class="more">+</li>');
        }
        else {
            $(".less").css({ display: "none" });
        }
    });

    $('ul.term-list').on('click', '.more', function () {

        if ($(this).hasClass('less')) {
            $(this).text('+').removeClass('less');
        } else {
            $(this).text('-').addClass('less');
        }

        $(this).siblings('li.toggleable').slideToggle();

    });


    $('.collapse.in').prev('.panel-heading').addClass('active');
    $('#accordion, #bs-collapse')
      .on('show.bs.collapse', function (a) {
          $(a.target).prev('.panel-heading').addClass('active');
      })
      .on('hide.bs.collapse', function (a) {
          $(a.target).prev('.panel-heading').removeClass('active');
      });


    //Mobile menu Script  
    $("#toggle").click(function () {
        if ($("md-sidenav").css('width') == '210px') {
            $("md-sidenav").css({ width: '56px' });
            $("md-sidenav").css({ left: '0' });
        }
        else {
            $("md-sidenav").css({ width: '210px' });
            $("md-sidenav").css({ left: '0' });
        }
    });



    $("#openchat").click(function () {
        $(".openchat").animate({ right: '0' }, 500);
        $(".chat").animate({ width: '310px' }, 500);
        $(".vt-srch-icon").css({ "pointer-events": "none" });
        $(".chat_title").css({ display: "inline-block" });
        $("#chat_close").css({ display: "inline-block" });
        $(".sb-search").animate({ margin: '0 310px 0 0' }, 500);
        $(".chat_content").animate({ right: '0px' }, 500);
    });

    $("#chat_close").click(function () {
        $(".chat").animate({ width: '200px' }, 500);
        $("#chat_close").css({ display: "none" });
        $(".vt-srch-icon").css({ "pointer-events": "inherit" });
        $(".overlay").css({ display: "none" });
        $(".chat_content").animate({ right: '-310px' }, 500);
        $(".sb-search").animate({ margin: '0 200px 0 0' }, 500);
    });
    //$(".chat_row").click(function(){
    //    var anchorId = $(this).find('div:first').attr('id');
    //    console.log('anchorId', anchorId);
    //	$('.chat_row').find('#' + anchorId).css({display: "block"});
    //	$('.chat_row').find('#' + anchorId).css({right: "0"});			
    //});
    $(".notify2").click(function () {
        $(".description").animate({ right: '-310px' }, 500);
        //$(".notify2").animate({right:'-360px'},500);
    });

    $(".chat_row span").text(function (index, currentText) {
        return currentText.substr(0, 40) + '...';
    });


    $('input[type=checkbox]').on('change', function (e) {
        if ($(this).prop('checked')) {
            var musicianTypes = $(this).next().addClass("chckbxbold");
        } else {
            var musicianTypes = $(this).next().removeClass("chckbxbold");
        };
    });
    $("#openchat").click(function () {
        if ($(this).parent().hasClass("popped")) {
            //$(this).parent().animate({left:'-400px'}, {queue: false, duration: 500}).removeClass("popped");
            $(".overlay").fadeOut(500);
        } else {
            //$(this).parent().animate({right: "0px" }, {queue: false, duration: 500}).addClass("popped");
            $(".overlay").fadeIn(500);
        }
    });

});
