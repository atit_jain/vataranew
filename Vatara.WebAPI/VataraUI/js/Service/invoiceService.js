﻿vataraApp.service("invoiceService", invoiceService);
invoiceService.$inject = ['$http', '$location'];
function invoiceService($http, $location)
{


    //*****************************************************
    //            Invoice Methods
    //*****************************************************

    this.getInvoicesByPersonAndProperty = function (managerId, fromDate, toDate, ownerPOID, personId, propertyId, personType) {

        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetInvoicesByPersonAndProperty",
            params: {
                managerId: managerId,
                fromDate: fromDate,
                toDate: toDate,
                ownerPOID: ownerPOID,
                personId: personId,
                propertyId: propertyId,
                personType: personType
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getInvoicePaymentHistory = function (invoiceId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetInvoicePaymentHistory",
            params: {
                invoiceId: invoiceId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertOwnersByManager = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/PropertOwnersByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    //this.getPropertByManagerAndOwner = function (managerId) {
    //    var responce = $http({
    //        method: "Get",
    //        url: "/api/Invoice/PropertByManagerAndOwner",
    //        params: {
    //            managerId: managerId
    //        },
    //        headers: { 'Content-Type': JSON }
    //    });
    //    return responce;
    //}

    this.getPropertByManagerAndOwner = function (managerId, isManager, pmid, personId, propertyId, personType) {
        
        var params = {};
        var URL = "";
        if (isManager == true)
        {
            URL = "/api/Invoice/PropertByManagerAndOwner";
            params.managerId = managerId;
        }
        else
        {
            URL = "/api/Invoice/PropertByTenant_Owner";
            params.PMID = pmid;
            params.PersonId = personId;
            params.PropoertyId = propertyId;
            params.PersonType = personType;
        }
        var responce = $http({
            method: "Get",
            url: URL,
            params: params,
            //params: {
            //    managerId: managerId
            //},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertByTenant_Owner = function (personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/PropertByTenant_Owner",
            params: {
                managerId: personId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }



    this.getPropertByManager = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/PropertByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getAllInvoicesByManager = function (StartDate, EndDate, ManagerId, PersonId, PropertyId, PersonType, ownerPOID) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetAllInvoicesByManager",
            params: {
                StartDate: StartDate,
                EndDate: EndDate,
                ManagerId: ManagerId,
                PersonId: PersonId,
                PropertyId: PropertyId,
                PersonType: PersonType,
                ownerPOID: ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getInvoiceWithDetail = function (invoiceId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetInvoiceWithDetail",
            params: {
                invoiceId: invoiceId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    //*****************************************************
    //              End Invoice Methods
    //*****************************************************


    this.getPropertiesByType = function (id) {        
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/PropertyWithPropertyType",
            params: {
                id: id
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }

    this.getPropertyTenantOwner = function (id, isManager, propertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/PropertyTenantOwner",
            params: {
                id: id,
                isManager: isManager,
                propertyId: propertyId
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }

    this.getCurrentInvoiceNumberByManager = function (mangerID) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetCurrentInvoiceNumber",
            params: {
                managerId: mangerID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    } 

 
    this.saveInvoice = function (data) {
        var responce =  $http.post(
            '/api/Invoice/SaveInvoice',
            JSON.stringify(data),
            {
                headers: {'Content-Type': 'application/json'}
            }
        )
        return responce;
    }

    this.validInvoiceNumber = function (invoceNumber, mangerID) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/ValidInvoiceNumber",
            params: {
                invoceNumber: invoceNumber,
                mangerID: mangerID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.deleteInvoice = function (InvoiceData) {

        var responce = $http.post(
            '/api/Invoice/DeleteInvoice',
            JSON.stringify(InvoiceData),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;     
    }


    this.savePayment = function (data) {
       
        var responce = $http.post(
            '/api/Invoice/Payment',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.payment_WithoutInvoice = function (data) {

        var responce = $http.post(
            '/api/Invoice/Payment_WithoutInvoice',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }
    
    this.propayPayment_GetUrl = function (MdlPayment) {
        MdlPayment.currentUrl = $location.absUrl();
        var responce = $http.post(
            '/api/Invoice/PropayPayment_GetUrl',
            JSON.stringify(MdlPayment),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.getAllInvoiceWithTotalAmt = function (propertyId, personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetAllInvoiceWithTotalAmt",
            params: {
                clientID: personId,
                propertyId: propertyId                
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropayProfile = function (emailId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetPropayProfile",
            params: {
                EmailId: emailId,
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPersonInfo = function (id) {
        var responce = $http({
            method: "Get",
            url: "/api/Person/Get",
            params: {
                id: id
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getInvoiceNumberByPropertyAndPerson = function (propertyId, personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetInvoiceNumberByPropertyAndPerson",
            params: {
                propertyId: propertyId,
                personId: personId,
                personType: personType
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    


    this.Printpdf = function (gridHtml, pagesize, headerText) {
       // console.log('headerText', headerText);
        var data = { gridHtml: gridHtml, pagesize: pagesize, headerText: headerText };
       // console.log('Input Data', data);
        var responce = $http.post(
            '/api/Report/GeneratePDF',
            JSON.stringify(data),
            {
                headers: { 'accept': 'application/pdf' },
                responseType: 'arraybuffer',
                dataType: 'json'
            }
        )
        return responce;
    }

    //this.testmethodwithGet = function (mdlCity) {      
    //    console.log('mdlCity', mdlCity);
    //    var responce = $http({
    //        method: "Get",
    //        url: "/api/Invoice/testmethod",
    //        params: {
    //            mdlCity: mdlCity,
    //        },
    //        headers: { 'Content-Type': JSON }
    //    });
    //    return responce;        
    //}




    //----------------------test payment
    this.PaymentMethodId = function (value, datavalue)
    {
       
        var en = "Basic " + "MTkxNjI3MTAxMzA2NzM0MjowNjcxNDU0Yy1jYmE2LTRjZmEtOTBhOS1hYjRiM2FmMTU5NzU=";
        var urlLink = "https://xmltestapi.propay.com/protectpay/Payers/" + value + "/PaymentMethods/";
         var responce = $http({
            method: "PUT",
            url: urlLink,
            data: datavalue,
            headers: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': '*', 
                            'Access-Control-Allow-Headers': '*',
                            'Access-Control-Allow-Credentials': true,
                            'Authorization': en,
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
            }
           
        });

        return responce;
    }

    //----------------------test end


}


