﻿vataraApp.service("indexService", indexService);
indexService.$inject = ['$http'];
function indexService($http) {


    this.getChartDataForDashboard = function (PMID, Duration) {
        var responce = $http({
            method: "Get",
            url: "/api/Dashboard/GetChartDataForDashboard",
            params: {
                PMID: PMID,
                Duration: Duration               
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    this.getIncomeExpenseChartData = function (PMID, Duration) {
        var responce = $http({
            method: "Get",
            url: "/api/Dashboard/GetIncomeExpenseChartData",
            params: {
                PMID: PMID,
                Duration: Duration
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    this.getPropertyChartData = function (PMID, Duration) {
        var responce = $http({
            method: "Get",
            url: "/api/Dashboard/GetPropertyChartData",
            params: {
                PMID: PMID,
                Duration: Duration
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };


    this.GetlstIncomeChartData = function (PMID, Duration) {
        var responce = $http({
            method: "Get",
            url: "/api/Dashboard/GetIncomeChartData",
            params: {
                PMID: PMID,
                Duration: Duration
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };


    this.GetIncomeandExpenseChartData = function (PMID, Duration) {
        var responce = $http({
            method: "Get",
            url: "/api/Dashboard/GetIncomeandExpenseChartData",
            params: {
                PMID: PMID,
                Duration: Duration
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    //this.saveInvoice = function (data) {
    //    var responce = $http.post(
    //        '/api/Invoice/SaveInvoice',
    //        JSON.stringify(data),
    //        {
    //            headers: { 'Content-Type': 'application/json' }
    //        }
    //    )
    //    return responce;
    //}



}