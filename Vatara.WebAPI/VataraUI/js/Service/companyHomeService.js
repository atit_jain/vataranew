﻿vataraApp.service("companyHomeService", companyHomeService);
companyHomeService.$inject = ['$http', '$rootScope'];
function companyHomeService($http, $rootScope) {



    this.getCompanyProfile = function (CompanyDomain) {
       
        var responce = $http({
            method: "Get",
            url: "/api/Company/GetCompanyProfile",
            params: {
                CompanyDomain: CompanyDomain
            },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }


}



