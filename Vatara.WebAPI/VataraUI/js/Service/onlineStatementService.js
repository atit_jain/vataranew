﻿vataraApp.service("onlineStatementService", onlineStatementService);
onlineStatementService.$inject = ['$http'];
function onlineStatementService($http)
{
    this.getOnlineStatement = function (managerId,personId, personType, propId, fromDate, toDate,ownerPOID) {
        var responce = $http({
            method: "Get",
            url: "/api/Report/OnlineStatement",
            params: {
                managerId:managerId,
                personId: personId,
                personType:personType,
                propId: propId,
                startDate:fromDate,
                EndDate: toDate,
                ownerPOID: ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getpropertyByPersonAndPersonType = function (managerId, personId, personType, ownerPOID) {
        var responce = $http({
            method: "Get",
            url: "/api/Report/GetpropertyByPersonAndPersonType",
            params: {
                managerId: managerId,
                personId: personId,
                personType: personType,
                ownerPOID: ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getPropertOwnersByManager = function (managerId) {
          var responce = $http({
            method: "Get",
            url: "/api/Report/PropertOwnersByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getPropertByManagerAndOwner = function (managerId, isManager, pmid, personId, propoertyId, personType) {

        var URL = "";
        var params = {};
        if (isManager == true) {
            URL = "/api/Report/PropertByManagerAndOwner";
            params.managerId = managerId;
        }
        else {           
            
            URL = "/api/Report/PropertByTenant_Owner";
            params.PMID = pmid;
            params.personId = personId;
            params.propoertyId = propoertyId;
            params.personType = personType;
            params.managerId = managerId;
        }
        var responce = $http({
            method: "Get",
            url: URL,
            params: params,
            //params: {
            //    managerId: managerId
            //},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.Printpdf = function (gridHtml, pagesize) {

        var data = { gridHtml: gridHtml, pagesize: pagesize };
        var responce = $http.post(
            '/api/Report/GeneratePDF',
            JSON.stringify(data),
            {
                headers: { 'accept': 'application/pdf' },
                responseType: 'arraybuffer',                
                dataType: 'json'
            }
        )
        return responce;       
    }

  
}