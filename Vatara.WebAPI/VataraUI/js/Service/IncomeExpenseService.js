﻿vataraApp.service("IncomeExpenseService", IncomeExpenseService);
IncomeExpenseService.$inject = ['$http'];
function IncomeExpenseService($http)
{

    //this.getPropTenantOwnerDetail = function (managerId,ownerId,propId) {
    //    var responce = $http({
    //        method: "Get",
    //        url: "/api/Report/PropTenantOwnerDetail",
    //        params: {
    //            managerId: managerId,
    //            ownerId:ownerId,
    //            propId: propId

    //        },
    //        headers: { 'Content-Type': JSON }
    //    });
    //    return responce;
    //}


    this.getOwnerStatement = function (managerId, pmid, ownerPOID, ownerPersonId, propId, year) {

        //console.log('managerId', managerId);
        //console.log('pmid', pmid);
        //console.log('ownerPOID', ownerPOID);
        //console.log('ownerPersonId', ownerPersonId);
        //console.log('propId', propId);
        //console.log('year', year);

        var responce = $http({
            method: "Get",
            url: "/api/Report/GetOwnerStatement",
            params: {
                managerId: managerId,
                pmid:pmid,
                ownerPOID: ownerPOID,
                ownerPersonId:ownerPersonId,
                propId: propId,
                year: year
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getPropertByManagerAndOwner = function (managerId, isManager, pmid, personId, propoertyId, personType, ownerPOID) {

        var URL = "";
        var params = {};
        if (isManager == true) {
            URL = "/api/Report/PropertByManagerAndOwner";
            params.managerId = managerId;
        }
        else {
            URL = "/api/Report/PropertByTenant_Owner";
            params.PMID = pmid;
            params.PersonId = personId;
            params.PropoertyId = propoertyId;
            params.PersonType = personType;
            params.managerId = managerId;
            params.ownerPOID = ownerPOID
        }
        var responce = $http({
            method: "Get",
            url: URL,
            params: params,
            //params: {
            //    managerId: managerId
            //},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getPropertOwnersByManager = function (managerId) {
         var responce = $http({
            method: "Get",
            url: "/api/Report/PropertOwnersByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getpropertyByPersonAndPersonType = function (managerId, personId, personType, ownerPOID) {
        var responce = $http({
            method: "Get",
            url: "/api/Report/GetpropertyByPersonAndPersonType",
            params: {
                managerId: managerId,
                personId: personId,
                personType: personType,
                ownerPOID: ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }
}