﻿vataraApp.service("CompanyService", CompanyService);
CompanyService.$inject = ['$http'];
function CompanyService($http) {


    this.saveCompany = function (data) {
        var responce = $http.post(
            '/api/Company/SaveCompany',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.getCompanyData = function (EmailId, personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Company/GetCompanyData",
            params: {
                EmailId: EmailId,
                personId: personId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    this.companyEmailExist = function (EmailId) {        
        var responce = $http({
            method: "Get",
            url: "/api/Login/CompanyEmailExist",
            params: { email: EmailId },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.validateCompanyURL = function (URL,PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Company/validateCompanyURL",
            params: { url: URL, PMID: PMID },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }


    //this.savePhotos = function (files, url, onComplete) {
    //    $http.post(url, files, {
    //        transformRequest: angular.identity,
    //        headers: { 'Content-Type': undefined }
    //    }).success(function (data, status, headers, config) {
    //        onComplete(true, status);
    //    })
    //        .error(function (data, status, headers, config) {
    //            onComplete(false, status);
    //        });
    //};

    this.savePhotos = function (files) {
        var responce = $http.post(
            '/api/Company/SaveCompanySliderImages',
           files,
            {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }
        )
        return responce;
    };

    this.getCompanyphoto = function (companyId, onComplete) {
        $http({ method: 'GET', url: '/api/companyphoto/Get?id=' + companyId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getCompanysliderimage = function (companyId, onComplete) {
        $http({ method: 'GET', url: '/api/companysliderimage/Get?id=' + companyId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.saveFiles = function (files, url, onComplete) {
        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.RemoveFile = function (propId, onComplete) {
        $http({ method: 'DELETE', url: '/api/companyphoto/Delete?id=' + propId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

}