﻿
vataraApp.service("AssociationService", AssociationService);
AssociationService.$inject = ['$http', '$rootScope'];
function AssociationService($http, $rootScope) {


    this.getAllAssocians = function (PMID) {

        var responce = $http({
            method: "Get",
            url: "/api/Association/GetAllAssocians",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getAssociansDetailByAssociansId = function (associansId) {

        var responce = $http({
            method: "Get",
            url: "/api/Association/GetAssociansDetailByAssociansId",
            params: {
                associansId: associansId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.createNewAssociation = function (mdlAssociationRegistration) {
        var responce = $http.post(
            '/api/Association/CreateNewAssociation',
            JSON.stringify(mdlAssociationRegistration),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.updateAssociation = function (mdlAssociationRegistration) {
        var responce = $http.post(
            '/api/Association/UpdateAssociation',
            JSON.stringify(mdlAssociationRegistration),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.saveFiles = function (files, url, onComplete) {
        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getAssociationPhotos = function (associationId, onComplete) {
        $http({ method: 'GET', url: '/api/associationphoto/Get?id=' + associationId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.EmailExist = function (email, associationId) {      
        var responce = $http({
            method: "Get",
            url: "/api/Association/CheckEmailExist",
            params: {
                email: email,
                associationId: associationId
            },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }
    
    this.RemoveFile = function (propId, onComplete) {
        $http({ method: 'DELETE', url: '/api/associationphoto/Delete?id=' + propId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

};

//AssociationService