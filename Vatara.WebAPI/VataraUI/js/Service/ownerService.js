﻿vataraApp.service("ownerService", ownerService);
ownerService.$inject = ['$http'];
function ownerService($http) {


    this.getOwnerDetails = function (POID, PersonId) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/GetOwnerDetail",
            params: {
                POID: POID,
                PersonId: PersonId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    this.getOwnersByPMID = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/GetOwnersByPMID",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    

    //this.SaveOwner = function (data) {
    //    var responce = $http.post(
    //        '/api/Owner/SaveOwner',
    //        JSON.stringify(data),
    //        {
    //            headers: { 'Content-Type': 'application/json' }
    //        }
    //    )
    //    return responce;
    //};


    this.SaveOwner = function (personData, onComplete) {
        $http({ method: 'post', data: personData, url: '/api/Owner/SaveOwner' })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };


    this.CheckEmailAvailability = function (EmailId, section) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/CheckEmailAvailability",
            params: { email: EmailId, OwnerType: section },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.getPropertyDoc = function (propId, onComplete) {
        $http({ method: 'GET', url: '/api/propertydocument/Get?id=' + propId })
            .success(function (data, status, headers, config) {
                documentsOfselectedProperty = data;
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getPropertByOwner = function (personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Owner/PropertByOwner",
            params: {
                ownerId: personId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getPropertByOwnerPOID = function (POID, PersonId) {   // *** PersonId is mendatory here ****
        var responce = $http({
            method: "Get",
            url: "/api/Owner/PropertByOwnerPOID",
            params: {
                POID: POID,
                PersonId: PersonId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.updateOwnerDetail = function (MdlOwnerData) {
        var responce = $http.post(
            '/api/Owner/UpdateOwnerDetail',
            JSON.stringify(MdlOwnerData),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    



};