﻿
vataraApp.service("propayRegService", propayRegService);
propayRegService.$inject = ['$http'];
function propayRegService($http) {


    this.getUserProfile = function (id, personTypeId) {
        var responce = $http({
            method: "Get",
            url: "/api/MarchantPropayRegistration/GetPropayProfile",
            params: {
                personId: id,
                personTypeId: personTypeId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getCountries = function () {
       
        var responce = $http({
            method: "Get",
            url: "/api/Country/Get",
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getStates = function () {
        var responce = $http({
            method: "Get",
            url: "/api/State/Get",
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }
    this.getCityByState = function (stateId) {
        var responce = $http({
            method: "Get",
            url: "/api/City/GetCityByState",
            params: {
                stateId: stateId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getCountyByState = function (stateId) {
        var responce = $http({
            method: "Get",
            url: "/api/County/GetCountyByState",
            params: {
                stateId: stateId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.registerMarchant = function (data) {

        var responce = $http.post(
            '/api/MarchantPropayRegistration/RegisterMarchant',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.GetCityByCounty = function (id) {
        var responce = $http({
            method: "Get",
            url: "/api/City/GetCityByCounty",
            params: {
                id: id
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getCityByStateOrCounty = function (stateId,countyId) {
        var responce = $http({
            method: "Get",
            url: "/api/City/GetCityByStateOrCounty",
            params: {
                StateId: stateId,
                CountyId: countyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    

}