﻿vataraApp.service("SupportTicketService", SupportTicketService);
SupportTicketService.$inject = ['$http'];
function SupportTicketService($http)
{

    this.GetAllSupportTicket = function (UserId, includeClosedStatus)
    {
        var responce = $http({
            method: "Get",
            url: "/api/SupportTicket/GetAllSupportTicket",
            params: { UserId: UserId, IncludeClosedStatus: includeClosedStatus },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    };
    this.SaveSupportTicket = function (data)
    {
        var responce = $http.post(
            '/api/SupportTicket/SaveSupportTicket',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }
    this.UpdateTicketStatus = function (TicketId) {
        var responce = $http({
            method: "Get",
            url: "/api/SupportTicket/UpdateTicketStatus",
            params: { TicketId: TicketId },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    };

    this.GetAllFaq = function(section)
    {
        var responce = $http({
            method: "Get",
            url: "/api/SupportTicket/GetAllFaq",
            params: { section: section },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }
};