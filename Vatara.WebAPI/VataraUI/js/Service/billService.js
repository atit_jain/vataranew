﻿vataraApp.service("billService", billService);
billService.$inject = ['$http'];
function billService($http) {

    this.getOwnerListByManager_Bill = function (id) {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/GetOwnerListByManager_Bill",
            params: {
                managerId: id
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    this.getPropertyListByManager_Bill = function (id, isManager, personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/GetPropertyListByManager_Bill",
            params: {
                managerId: id,
                isManager: isManager,
                personId: personId
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }

    this.getBill_DateWise = function (managerID, personID, startDate, endDate, isManager, propertyID, personTypeId, ownerPOID)
    {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/GetBill_DateWise",
            params: {
                managerId: managerID,
                personId: personID,               
                startDate: startDate,
                endDate: endDate,
                isManager: isManager,
                propertyID: propertyID,
                personTypeId: personTypeId,
                ownerPOID: ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getBillById = function (BillId, personTypeId) {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/GetBillById",
            params: {
                BillId: BillId,
                personTypeId: personTypeId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertOwnersByManager = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/PropertOwnersByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.Printpdf = function (gridHtml, pagesize, headerText) {
        var data = { gridHtml: gridHtml, pagesize: pagesize, headerText: headerText };
        var responce = $http.post(
            '/api/Report/GeneratePDF',
            JSON.stringify(data),
            {
                headers: { 'accept': 'application/pdf' },
                responseType: 'arraybuffer',
                dataType: 'json'
            }
        )
        return responce;
    }

    this.getBillByPersonAndProperty = function (managerID, startDate, endDate,ownerPOID, personID,  propertyID, personTypeId) {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/GetBillByPersonAndProperty",
            params: {
                managerId: managerID,               
                startDate: startDate,
                endDate: endDate,
                ownerPOID:ownerPOID,
                personId: personID,
                propoertyId: propertyID,
                personType: personTypeId                
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getBillForOwnerTenantByPersonIdAndPropertyId = function (managerID, startDate, endDate, ownerPOID, personID, propertyID, personTypeId) {
        var responce = $http({
            method: "Get",
            url: "/api/Bill/GetBillForOwnerTenantByPersonIdAndPropertyId",
            params: {
                managerId: managerID,
                startDate: startDate,
                endDate: endDate,
                ownerPOID: ownerPOID,
                personId: personID,
                propoertyId: propertyID,
                personType: personTypeId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

}