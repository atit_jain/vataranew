﻿vataraApp.service("AddNewUserService", AddNewUserService);
AddNewUserService.$inject = ['$http'];
function AddNewUserService($http) {


    this.SaveAdmin = function (data) {
        var responce = $http.post(
            '/api/AddNewUser/RegisterNewManager',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.EmailExist = function (email) {      
        var responce = $http({
            method: "Get",
            url: "/api/Login/EmailExist",
            params: { email: email, id: 0, personType: "" },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }


    this.getAllManagers = function (managerId) {

        var responce = $http({
            method: "Get",
            url: "/api/AddNewUser/GetAllManagers",
            params: { managerId: managerId },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.DeleteUser = function (data) {
        var responce = $http.post(
            '/api/AddNewUser/DeleteUser',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

}



