﻿vataraApp.service("outstandingPaymentService", outstandingPaymentService);
outstandingPaymentService.$inject = ['$http'];
function outstandingPaymentService($http) {



    //this.getVendorRemainPayment = function (managerId, fromDate, toDate) {
    //    var responce = $http({
    //        method: "Get",
    //        url: "/api/OutStandingPay/VendorRemainPayment",
    //        params: {
    //            managerId: managerId,
    //            fromDate: fromDate,
    //            toDate: toDate
    //        },
    //        headers: { 'Content-Type': JSON }
    //    });
    //    return responce;
    //}

    this.getVendorRemainPayment = function (managerId,PMID, fromDate, toDate, personType, personId, filterCategory) {

        var responce = $http({
            method: "Get",
            url: "/api/OutStandingPay/VendorRemainPayment",
            params: {
                managerId: managerId,
                PMID:PMID,
                fromDate: fromDate,
                toDate: toDate,
                filterCategory:filterCategory,
                personType:personType,
                personId: personId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.savePayment = function (data) {
        var responce = $http.post(
            '/api/OutStandingPay/VendorPaid',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.getPropertOwnersByManager = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/OutStandingPay/PropertOwnersByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertByManagerAndOwner = function (managerId, isManager) {

        var URL = "";
        if (isManager == true) {
            URL = "/api/OutStandingPay/PropertByManagerAndOwner";
        }
        else {
            URL = "/api/OutStandingPay/PropertByTenant_Owner";
        }
        var responce = $http({
            method: "Get",
            url: URL,
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


}