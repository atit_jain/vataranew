﻿vataraApp.service("tenantService", tenantService);
tenantService.$inject = ['$http'];
function tenantService($http) {

    this.getTenantDetails = function (tenantId) {     
        var responce = $http({
            method: "Get",
            url: "/api/Tenant/TenantDetail",
            params: {
                tenantId: tenantId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };


    this.getTenantsByPMID = function (PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Tenant/GetTenantsByPMID",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };
    

    this.getInvoicePaymentHistory = function (invoiceId) {
        var responce = $http({
            method: "Get",
            url: "/api/Tenant/GetInvoicePaymentHistory",
            params: {
                invoiceId: invoiceId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.updateTenantDetail = function (mdlPersonReg) {
        var responce = $http.post(
            '/api/Tenant/UpdateTenantDetail',
            JSON.stringify(mdlPersonReg),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }
    
  
    //this.getLeaseDocuments = function (leaseId, onComplete) {
    //    $http({ method: 'GET', url: '/api/Lease/GetLeaseDocuments?leaseId=' + leaseId })
    //        .success(function (data, status, headers, config) {
    //            onComplete(true, data)
    //        })
    //        .error(function (data, status, headers, config) {
    //            onComplete(false, status);
    //        });
    //};
};