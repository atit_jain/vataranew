﻿vataraApp.service("recurringBillService", recurringBillService);
recurringBillService.$inject = ['$http'];
function recurringBillService($http) {

    this.recurringBill = function (personTypeId, personId, propertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/RecurringBill/GetRecurringBill",
            params: {
                personTypeId: personTypeId,
                personId: personId,
                propertyId:propertyId
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    this.stopRecurringpayment = function (data) {
        var responce = $http.post(
            '/api/RecurringBill/StopRecurringPayment',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    };

    this.GetRecurringBillByPropertyID = function (personTypeId, personId,PropertyID) {
        var responce = $http({
            method: "Get",
            url: "/api/RecurringBill/GetRecurringBill",
            params: {
                personTypeId: personTypeId,
                personId: personId
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }
}