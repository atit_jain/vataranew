﻿
vataraApp.service("chatService", chatService);
chatService.$inject = ['$http'];
function chatService($http) {

    this.Isminimise = false;
    this.MinimiseUser = {};
    this.isUserOnline = false;

    this.getAllUserListForChat = function (PMID, loggedInPersonType, loggedInPersonEmail) {
        var responce = $http({
            method: "Get",
            url: "/api/Chat/GetAllUserListForChat",
            params: {
                PMID: PMID,
                loggedInPersonType: loggedInPersonType,
                loggedInPersonEmail: loggedInPersonEmail
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    this.getChatUnreadNotificationMessage = function (toMsg) {
        var responce = $http({
            method: "Get",
            url: "/api/Chat/GetChatUnreadNotificationMessage",
            params: {
                toMsg: toMsg
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    this.getChatInfo = function (LoggedInUser,LoggedInUserPersonTypeId, SelectedUser, SelectedUserPersonTypeId) {
        var responce = $http({
            method: "Get",
            url: "/api/Chat/GetChatInfo",
            params: {
                LoggedInUser: LoggedInUser,
                LoggedInUserPersonTypeId:LoggedInUserPersonTypeId,
                SelectedUser: SelectedUser,
                SelectedUserPersonTypeId: SelectedUserPersonTypeId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };    

    this.getNotification = function (personId, personTypeId, PMID) {
        var responce = $http({
            method: "Get",
            url: "/api/Chat/GetNotification",
            params: {
                personId: personId,
                personTypeId: personTypeId,
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    this.updateNotificationStatus = function (notificationId) {
        var responce = $http({
            method: "Get",
            url: "/api/Chat/UpdateNotificationStatus",
            params: {
                notificationId: notificationId                
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    

    this.clearNotification = function (mdl) {
        var responce = $http.post(
            '/api/Chat/ClearNotification',
            JSON.stringify(mdl),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.deleteNOtification = function (notificationId) {
        var responce = $http.post(
            '/api/Chat/DeleteNOtification',
            JSON.stringify(notificationId),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    


}