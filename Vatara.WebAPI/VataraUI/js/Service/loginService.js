﻿vataraApp.service("loginService", loginService);
loginService.$inject = ['$http', '$rootScope'];
function loginService($http, $rootScope) {

    this.SignIn = function (user) {

        var responce = $http.post(
            '/api/Login/SignIn',
            JSON.stringify(user),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.SignOut = function (user) {
        var responce = $http.post(
            '/api/Login/SignOut',
            JSON.stringify(user),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.PerformAction = function (user) {
        var responce = $http.post(
            '/api/Login/PerformAction',
            JSON.stringify(user),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.PersonRegistration = function (mdlPersonReg) {

        mdlPersonReg.Image = '';
        mdlPersonReg.company_logo = '';
        // console.log('mdlPersonReg 1122', mdlPersonReg);
        var responce = $http.post(
            '/api/Login/PersonRegistraion',
            JSON.stringify(mdlPersonReg),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.EmailExist = function (mdlPersonReg) {
        var id = 0;
        if (mdlPersonReg.id != "") {
            id = mdlPersonReg.id;
        }

        var responce = $http({
            method: "Get",
            url: "/api/Login/EmailExist",
            params: { email: mdlPersonReg.email, id: id, personType: mdlPersonReg.personType },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.GetRegistrationData = function (personId) {

        var responce = $http({
            method: "Get",
            url: "/api/Login/GetRegistrationData",
            params: {
                personId: personId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.ChangePassword = function (mdlPersonReg) {
        var responce = $http.post(
            '/api/Login/ChangePassword',
            JSON.stringify(mdlPersonReg),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.ChangeEmail = function (mdlChangeEmail) {
        var responce = $http.post(
            '/api/Login/ChangeEmail',
            JSON.stringify(mdlChangeEmail),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.CheckEmailExist = function (EmailId) {
        var responce = $http({
            method: "Get",
            url: "/api/Login/CheckEmailExist",
            params: { email: EmailId },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }



    this.VerifyEmail = function (email, Guid, comProfile) {
        var responce = $http({
            method: "Get",
            url: "/api/Login/VerifyEmail",
            params: {
                email: email,
                Guid: Guid,
                comProfile: comProfile
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.SaveAdmin = function (mdlPersonReg) {
        var responce = $http.post(
            '/api/Login/SaveAdmin',
            JSON.stringify(mdlPersonReg),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.ForgotPasswordReset = function (mdlforgotPasswordReset) {

        var responce = $http.post(
            '/api/Login/ForgotPasswordReset',
            JSON.stringify(mdlforgotPasswordReset),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.saveFiles = function (files, url, onComplete) {
        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getCompanyphoto = function (companyId, onComplete) {
        $http({ method: 'GET', url: '/api/companyphoto/Get?id=' + companyId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.GetSubscriptionPlan = function () {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/GetSubscriptionPlan",
            params: {},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.SaveSubScription = function (subscription) {

        var responce = $http.post(
            '/api/Subscription/SaveSubScription_withTrial',
            JSON.stringify(subscription),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.SaveSubScription_withCard = function (subscription) {

        var responce = $http.post(
            '/api/Subscription/SaveSubScription_withCard',
            JSON.stringify(subscription),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.SavePayment = function (SubScriptionId, CustomerId, Amount, PaymentMode) {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/SavePayment",
            params: {
                SubScriptionId: SubScriptionId,
                CustomerId: CustomerId,
                Amount: Amount,
                PaymentMode: PaymentMode
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.GetZohoProfile = function (Email) {
        var responce = $http({
            method: "Get",
            url: "/api/Login/GetZohoProfile",
            params: {
                Email: Email
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.GetUserInfo = function (PMID, PersonId, PersonTypeId) {

        var responce = $http({
            method: "Get",
            url: "/api/Login/GetUserInfo",
            params: {
                PMID: PMID,
                PersonId:PersonId,
                PersonTypeId:PersonTypeId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }
}