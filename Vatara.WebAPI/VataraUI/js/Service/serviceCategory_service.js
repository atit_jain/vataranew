﻿vataraApp.service("serviceCategory_service", serviceCategory_service);
serviceCategory_service.$inject = ['$http'];
function serviceCategory_service($http)
{

    this.saveServiceCategory = function (data) {
        var returnData = $http.post(
              '/api/ServiceCategory/SaveCategory',
              JSON.stringify(data),
              {
                  headers: { 'Content-Type': 'application/json' }
              }
          );        
        return returnData;
    }

    this.getServiceCategory = function (personTypeId) {
        var responce = $http({
            method: "Get",
            url: "/api/ServiceCategory/GetServiceCategory",
            params: { personTypeId: personTypeId ,},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

}


