﻿vataraApp.service("paymentService", paymentService);
paymentService.$inject = ['$http', '$location'];
function paymentService($http, $location) {

    this.getPersonInfo = function (id) {
        var responce = $http({
            method: "Get",
            url: "/api/Person/Get",
            params: {
                id: id
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropayProfile = function (emailId, personId, personTypeId) {

        var responce = $http({
            method: "Get",
            url: "/api/Payment/GetPropayProfile",
            params: {
                EmailId : emailId,
                PersonId : personId,
                PersonTypeId : personTypeId,
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.savePayment = function (data) {

        var responce = $http.post(
            '/api/Invoice/Payment',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.payment_WithoutInvoice = function (data) {

        var responce = $http.post(
            '/api/Invoice/Payment_WithoutInvoice',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.getPropertByManager = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/Payment/PropertByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertByOwner = function (personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Payment/PropertByOwner",
            params: {
                ownerId: personId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertByTenant = function (TenantId) {
        var responce = $http({
            method: "Get",
            url: "/api/Payment/PropertByTenant",
            params: {
                TenantId: TenantId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertyTenantOwner = function (id, isManager, propertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/Payment/PropertyTenantOwner",
            params: {
                id: id,
                isManager: isManager,
                propertyId: propertyId
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    }


    this.propayPayment_GetUrl = function (MdlPayment) {
        MdlPayment.currentUrl = $location.absUrl();
        var responce = $http.post(
            '/api/Invoice/PropayPayment_GetUrl',
            JSON.stringify(MdlPayment),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.getAllInvoiceWithTotalAmt = function (propertyId, personId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/GetAllInvoiceWithTotalAmt",
            params: {
                clientID: personId,
                propertyId: propertyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.PaymentMethodId = function (value, datavalue) {

        var en = "Basic " + "MTkxNjI3MTAxMzA2NzM0MjowNjcxNDU0Yy1jYmE2LTRjZmEtOTBhOS1hYjRiM2FmMTU5NzU=";
        var urlLink = "https://xmltestapi.propay.com/protectpay/Payers/" + value + "/PaymentMethods/";
         var responce = $http({
            method: "PUT",
            url: urlLink,
            data: datavalue,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Credentials': true,
                'Authorization': en,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        });
        return responce;
    }

    this.RedirectToMobileSchema = function (Result, Message) {
        var responce = $http({
            method: "Get",
            url: "/Home/RedirectToMobileSchema",
            params: {
                Result: Result,
                Message: Message
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

}