﻿vataraApp.service("eventTrackerService", eventTrackerService);
eventTrackerService.$inject = ['$http', '$location'];
function eventTrackerService($http, $location) {
    
    this.getPropertyByPerson = function (ownerPOID , personId,  personType) {

        var responce = $http({
            method: "Get",
            url: "/api/EventTracker/GetPropertyByPerson",
            params: {
                ownerPOID: ownerPOID,
                personId: personId,
                personType: personType
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.saveNewEvent = function (eventData) {
        var responce = $http.post(
            '/api/EventTracker/SaveNewEvent',
            JSON.stringify(eventData),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.getEventList = function (propertyID, showArchive) {
        var responce = $http({
            method: "Get",
            url: "/api/EventTracker/GetEventList",
            params: {
                PropertyID: propertyID,
                ShowArchive: showArchive
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.deleteEvent = function (eventData) {
        var responce = $http.post(
            '/api/EventTracker/DeleteEvent',
            JSON.stringify(eventData),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.archiveEvent = function (eventData) {
        var responce = $http.post(
            '/api/EventTracker/ArchiveEvent',
            JSON.stringify(eventData),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }
}