﻿
vataraApp.service("subscriptionService", subscriptionService);
subscriptionService.$inject = ['$http'];
function subscriptionService($http) {


    this.getSubscriptionInfo = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/GetSubscriptionInfo",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    this.getSubscriptionTypes = function () {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/GetSubscriptionTypes",
            params: {},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };


    this.propayPayment_GetUrl = function (MdlPayment) {
        MdlPayment.currentUrl = $location.absUrl();
        var responce = $http.post(
            '/api/Invoice/PropayPayment_GetUrl',
            JSON.stringify(MdlPayment),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.GetSubscriptionPlan = function () {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/GetSubscriptionPlan",
            params: {},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.GetCustomerProfile = function (userName) {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/GetCustomerProfile",
            params: {
                userName: userName
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.GetCustomerSubscriptionHistory = function (pmid) {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/GetCustomerSubscriptionHistory",
            params: {
                pmid: pmid
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.UpdateSubscription = function (Payment) {
        var responce = $http.post(
          '/api/Subscription/UpdateSubscription',
          JSON.stringify(Payment),
          {
              headers: { 'Content-Type': 'application/json' }
          }
      )
        return responce;
    }
    this.CancelSubscription = function (SubscriptionId, PlanCode) {
        var responce = $http({
            method: "Get",
            url: "/api/Subscription/CancelSubscription",
            params: {
                SubscriptionId: SubscriptionId,
                PlanCode: PlanCode
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }
}