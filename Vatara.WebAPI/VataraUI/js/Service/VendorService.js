﻿//VendorService
vataraApp.service("VendorService", VendorService);
VendorService.$inject = ['$http', '$rootScope'];
function VendorService($http, $rootScope) {

    this.getAllVendors = function (PMID) {

        var responce = $http({
            method: "Get",
            url: "/api/Vendor/GetAllVendors",
            params: {
                PMID: PMID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getVendorDetailByVendorId = function (vendorId) {

        var responce = $http({
            method: "Get",
            url: "/api/Vendor/GetVendorDetailByVendorId",
            params: {
                vendorId: vendorId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getVendorServiceCategories = function (VendorId) {

        var responce = $http({
            method: "Get",
            url: "/api/Vendor/GetVendorServiceCategories",
            params: {
                VendorId: VendorId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getVendorServiceCategoryMaster = function () {

        var responce = $http({
            method: "Get",
            url: "/api/Vendor/GetVendorServiceCategoryMaster",
            params: {},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.createNewVendor = function (mdlVendorRegistration) {
        var responce = $http.post(
            '/api/Vendor/CreateNewVendor',
            JSON.stringify(mdlVendorRegistration),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.updateVendor = function (mdlVendorRegistration) {
        var responce = $http.post(
            '/api/Vendor/UpdateVendor',
            JSON.stringify(mdlVendorRegistration),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.EmailExist = function (email, vendorId) {
        var responce = $http({
            method: "Get",
            url: "/api/Vendor/CheckEmailExist",
            params: {
                email: email,
                vendorId: vendorId
            },
            headers: { 'Content-Type': 'application/json' }
        });
        return responce;
    }

    this.saveFiles = function (files, url, onComplete) {
        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getVendorphotos = function (vendorId, onComplete) {
        $http({ method: 'GET', url: '/api/vendorphoto/Get?id=' + vendorId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.RemoveFile = function (propId, onComplete) {
        $http({ method: 'DELETE', url: '/api/vendorphoto/Delete?id=' + propId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };
    



    //this.getVendorphotos = function (vendorId, onComplete) {
    //    $http({ method: 'GET', url: '/api/vendorphoto/' + vendorId })
    //        .success(function (data, status, headers, config) {
    //            //documentsOfselectedProperty = data;
    //            onComplete(true, data)
    //        })
    //        .error(function (data, status, headers, config) {
    //            onComplete(false, status);
    //        });
    //};
}