﻿
vataraApp.service("manageUsersService", manageUsersService);
manageUsersService.$inject = ['$http'];
function manageUsersService($http) {

    //this.GetAllPropertyByPMID = function (pmid) {
    //    var responce = $http({
    //        method: "Get",
    //        url: "/api/ManagerUser/GetAllPropertyByPMID",
    //        params: {
    //            PMID: pmid
    //        },
    //        headers: { 'Content-Type': JSON }

    //    });
    //    return responce;
    //};


    this.GetPropertyTenantOwner = function (managerId, propertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/ManagerUser/GetPropertyTenantOwner",
            params: {
                managerId: managerId,
                propertyId: propertyId
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };

    
    this.UpdateUserStatus = function (data) {
        var responce = $http.post(
            '/api/ManagerUser/UpdateUserStatus',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.resendLoginCredential = function (data) {
        var responce = $http.post(
            '/api/ManagerUser/ResendLoginCredential',
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


}