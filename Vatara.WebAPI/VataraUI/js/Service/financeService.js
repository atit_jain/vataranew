﻿vataraApp.service("financeService", financeService);
financeService.$inject = ['$http'];
function financeService($http) {

    this.getAllTransections = function (managerId, isManager, fromDate, toDate, pmid, personId, propoertyId, personType) {

        var responce = $http({
            method: "Get",
            url: "/api/Transaction/GetTransactionByManager",
            params: {
                managerId: managerId,
                isManager:isManager,
                fromDate: fromDate,
                toDate: toDate,
                pmid: pmid,
                personId: personId,
                propoertyId: propoertyId,
                personType: personType
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getOwnerTransaction = function (managerId, isManager, fromDate, toDate, pmid, personId, propoertyId, personType, ownerPOID) {
        var responce = $http({
            method: "Get",
            url: "/api/Transaction/GetOwnerTransaction",
            params: {
                managerId: managerId,
                isManager: isManager,
                fromDate: fromDate,
                toDate: toDate,
                pmid: pmid,
                personId: personId,
                propoertyId: propoertyId,
                personType: personType,
                ownerPOID: ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertByManagerAndOwner = function (managerId, isManager,pmid, personId, propoertyId, personType) {

        var params = {};
        var URL = "";
        if (isManager == true) {
            URL = "/api/Transaction/PropertByManagerAndOwner";
            params.managerId = managerId;
        }
        else {
            URL = "/api/Transaction/PropertByTenant_Owner";
            params.PMID = pmid;
            params.PersonId = personId;
            params.PropoertyId = propoertyId;
            params.PersonType = personType;
        }
        var responce = $http({
            method: "Get",
            url: URL,
            params:params,
            //params: {
            //    managerId: managerId
            //},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getPropertOwnersByManager = function (managerId) {
        var responce = $http({
            method: "Get",
            url: "/api/Invoice/PropertOwnersByManager",
            params: {
                managerId: managerId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getTransactionByPersonAndProperty = function (managerId,isManager,pmid, fromDate, toDate, personId, propoertyId, personType,ownerPOID) {

         var responce = $http({
            method: "Get",
            url: "/api/Transaction/GetTransactionByPersonAndProperty",
            params: {
                managerId: managerId,
                isManager:isManager,
                pmid:pmid,
                fromDate: fromDate,
                toDate: toDate,
                personId: personId,
                propoertyId: propoertyId,
                personType: personType,
                ownerPOID:ownerPOID
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }


    this.getPropertyByPerson = function (ownerPOID, personId, managerId, personType) {

        var responce = $http({
            method: "Get",
            url: "/api/Transaction/GetPropertyByPersonId",
            params: {                
                ownerPOID: ownerPOID,
                personId: personId,
                managerId: managerId,
                personType: personType
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

}