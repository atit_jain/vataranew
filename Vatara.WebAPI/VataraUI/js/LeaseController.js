//var leaseapp = angular.module('Myapp');
//var propertyapp = angular.module('Myapp', ['ngMaterial', 'ngMessages', 'ngAnimate']);
vataraApp.config(function ($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function (date) {
        // Requires Moment.js OR enter your own formatting code here....
        return moment(date).format('l');
    };
});
vataraApp.controller('leaseController', function ($scope, $http, propertyService, $routeParams, $location, masterdataService, $rootScope, leaseService, personService, AddNewUserService, storageService) {

    var propertytypeslist = [{}];
    $scope.isPersonEmailExist = false;
    $scope.activeTap = "Financials";
    // $scope.activeTap = "Docs";    
    $scope.documents = [];
    $scope.showLoader = false;// Pm 26-9-2018
    $scope.popUp = {
        Heading: 'Terminate Lease',
        Message: 'Are you sure terminate lease?'
    };

    $scope.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    $scope.lstOccupant = [];
    $scope.propertieslist = [
        {
            "pId": 0,
            "pNum": 0,
            "pType": 0,
            "pTypeName": "",
            "pName": "",
            "pAddress": "",
            "pCityId": "",
            "pCity": "",
            "pCounty": "",
            "pCountyId": "",
            "pState": "",
            "pStateId": "",
            "pZipcode": "",
            "pCountryId": "",
            "pCountry": "",
            "pPhone": "",
            "pEmail": "",
            "pFax": "",
            "pGrossArea": "0",
            "pRentableArea": "0",
            "pParkingSpaces": "0",
            "pUnitNo": "",
            "pHOA": "",
            "pDescription": "",
            "pAirportName": "",
            "pAirportDistance": "",
            "pUOM": "",
            "pListType": "",
            "pComments": "",
            "pCustomField1": "",
            "photosArray": [],
            "docsArray": []
        }
    ];
    $scope.photos = [];
    $scope.pPropertyId = 0;
    $scope.pLeaseDesc = '';
    $scope.pLeaseType = '';
    $scope.pAgreementTypeId = 0;
    $scope.pAgreementType = '';
    $scope.pLeaseTermId = 0;
    $scope.pLeaseTerm = '';
    $scope.pLeaseTermTypeId = 0;
    $scope.pLeaseTermType = '';
    $scope.pLeaseStartDate = '';
    $scope.pLeaseEndDate = '';
    $scope.pComments = '';
    $scope.pSecurityDeposit = '';
    $scope.pRentalAmount = 0;
    $scope.pLeaseActive = "1"; //PM 27-9-2018

    $scope.pPersonId = 0;
    $scope.pFirstName = ''
    $scope.pLastName = '';
    $scope.pEmail = '';
    $scope.pPhoneNo = '';
    $scope.personjson = '';
    //Pm 27-9-2018
    $scope.pStartDate = new Date();
    $scope.pEndDate = new Date();

    //End Pm 27-9-2018
    $scope.propertyjson = '';

    $scope.pSearchType = 0;
    $scope.pSearchValue = '';
    $scope.createlease_ByProperty = false;
    $scope.propertiessearchresultslist = [{}];

    $scope.leasesjson = '';
    $scope.leaseslist = [{}];
    $scope.leasetenantslist = [{}];

    $scope.pAddNewTenant = '0';
    $scope.pIsMoveInInspectionReqd = 0;
    $scope.pIsMoveOutInspectionReqd = '0';
    $scope.leaseTypes = [];
    $scope.selectLeaseType = '';
    //PM 17-9-2018
    $scope.sortByDate = 'pStartDate';
    $scope.selectSortByDate = 'startDate';//PM 17-9-2018
    $scope.mdlLeaseType = [];
    $scope.pParkingSpaces = 0;
    $scope.pRentableArea = 0;
    $scope.pTerminationDesc = '';
    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};
    $scope.leaseInfo = {};
    $scope.propertyInfo = {};
    $scope.weekDayArray = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    $scope.monthDayArray = [];
    $scope.monthNumberArray = [];
    $scope.frequency = '';
    $scope.checkfrequency = true;

    $scope.mdlWeekDay = '';
    $scope.mdlMonthDay = '';
    $scope.mdlMonthNumber = '';    

    $scope.ShowSelectFrequency = '';
    $scope.PaymentFrequency = '';
    $scope.SelectDay = '';
    $scope.suffix = '';

    $scope.CreateMonthDayArray = function () {

        $scope.monthDayArray = [];
        for (var i = 1 ; i <= 31 ; i++) {
            $scope.monthDayArray.push(i);
        }
    }

    $scope.CreateMonthNumberArray = function (FrequencyId) {
        if (FrequencyId === undefined) {
            $scope.checkfrequency = false;
            $("#mdlRentPaymentFrequency").modal('hide');
            $scope.mdlMonthDay = '';
            $scope.PaymentFrequency = '';
            $scope.pPaymentFrequencyId = '';
            $scope.ShowSelectFrequency = '';
        }
        else {
            $scope.monthNumberArray = [];
            $scope.mdlMonthDay = '';
            $scope.PaymentFrequency = FrequencyId;
            $scope.pPaymentFrequencyId = FrequencyId;
            $scope.SelectDay = '';
            if (FrequencyId == 1) {
                $scope.mdlWeekDay = 'Monday';
                $scope.SelectDay = 'Monday';
                $scope.ShowSelectFrequency = 'Monday';
            }
            else {
                $scope.mdlMonthDay = 1;
                $scope.ShowSelectFrequency = 1;
                $scope.suffix = 'st';
            }
            var freq = $scope.leasepaymentfrequencylist.filter(function (f) { return f.Id == FrequencyId })[0];

            if (freq != undefined) {
                $scope.frequency = freq.Name;
                var count = 0;
                if ($scope.frequency.toUpperCase() == "QUATERLY") {
                    count = 3;
                }
                if ($scope.frequency.toUpperCase() == "SEMI ANNUALLY") {
                    count = 6;
                }
                if ($scope.frequency.toUpperCase() == "ANNUALLY") {
                    count = 12;
                }
                for (var i = 1; i <= count; i++) {
                    $scope.monthNumberArray.push(i);
                }
            }
            $("#mdlRentPaymentFrequency").modal('show');
        }
    }

    $scope.setDay = function (day) {

        $scope.mdlMonthDay = day;
        $scope.ordinal_suffix_of($scope.mdlMonthDay);
        $scope.ShowSelectFrequency = $scope.mdlMonthDay;
    }

    //$scope.setmMonth = function (month) {

    //    $scope.mdlMonthNumber = month;
    //}  
   
    $scope.sortByDate_Event = function (sortBy) {
        $scope.selectSortByDate = sortBy;
        if (sortBy == 'startDate') {
            $scope.sortByDate = 'pStartDate';
        }
        else {
            $scope.sortByDate = 'pEndDate';
        }
    }
    $scope.leasePropertyId = 0;
    //End PM 17-9-2018
    $scope.showOccupants = false;

    $scope.ViewOccupants = function () {
        if ($scope.showOccupants)
        { $scope.showOccupants = false; }
        else { $scope.showOccupants = true; }
    };

    $scope.split = function (input, splitChar, splitIndex) {
        if (!angular.isUndefined(input) && input != null && input.length > 0) {
            return input.split(splitChar)[splitIndex];
        }
    };

    $scope.getLeases = function () {
        storageService.remove('mdlLeaseDuration');
        var personId = 0;
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
            personId = $rootScope.userData.personId;
        }
        else {
            personId = $rootScope.userData.personManageriD;
        }

        $scope.getPropertyTypes();
        leaseService.getAllLeases(personId, function (status, data) {
            if (status) {
                $scope.leaseslist = data;
                //PM 01-10-2018
                $scope.leaseTypes = [];
                for (i = 0; i < data.length; i++) {

                    if ($scope.leaseslist[i].DayOfMonth != null && $scope.leaseslist[i].DayOfMonth != undefined) {

                        var dayOfmonth = $scope.leaseslist[i].DayOfMonth;                       
                        var suffix = $scope.ordinal_suffix_of(dayOfmonth);
                        $scope.leaseslist[i].suffix = suffix;
                        $scope.suffix = '';
                    }                  
                    if ($scope.leaseTypes.indexOf(data[i].pLeaseType) == -1 && data[i].pLeaseType != null) {
                        $scope.leaseTypes.push(data[i].pLeaseType);
                    }
                }
                $scope.leasesjson = angular.toJson(data, true);
            }
            else {
                swal("ERROR", "Error in get lease list", "error");
                //alert('error in getAllLeases');
            }
        });
    };

    $scope.LoadFunction = function () {
        if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum != -1) {
            $scope.leasePropertyId = $routeParams.PropertyNum;

            if ($location.url().indexOf("create_lease") != -1) {
                $scope.createlease_ByProperty = true;
            }
        }
        $scope.CreateMonthDayArray();
        $scope.getProperties();
        $scope.getMasterData();
        //PM 26-9-2018
        //$scope.showtab1();
        $scope.showtab2();
        //End PM 26-9-2018
    }

    $scope.CancelLease = function () {
        if ($scope.leasePropertyId > 0) {
            $location.path('/propertyProfile/' + $scope.leasePropertyId);
        }
        else if ($routeParams.LeaseNum > 0) {
            $location.path('/leaseview/' + $routeParams.LeaseNum); // NP 8-Oct-2018
        }
        else {
            $location.path('/leases');
        }
    }

    $scope.getProperties = function () {
        var personId = 0;
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
            personId = $rootScope.userData.personId;
        }
        else {
            personId = $rootScope.userData.personManageriD;
        }

        propertyService.getAllProperties(personId, function (status, data) {
            if (status) {
                $scope.propertieslist = data;

                if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum != -1) {

                    var prop = $scope.propertieslist.filter(function (c) {
                        return c.pId == $routeParams.PropertyNum;
                    });
                    $scope.selectncontinue(prop);
                }
                $scope.propertyjson = angular.toJson($scope.propertieslist, true);
            }
            else {
                swal("ERROR", "Error in get Property list", "error");
                //alert('error in getAllProperties');
            }
        });
    };

    $scope.selectedProperty = function (property) {
        $scope.selected_property = property;
    };

    $scope.selectncontinue = function (property) {
        $scope.selected_property = property[0];
        $scope.pLeaseAmount = $scope.selected_property.pBaseRent;
        $scope.GetLeaseType($scope.selected_property.pCategoryId);
        $scope.showtab2();
    };

    $scope.disable_btnTenants = false;
    $scope.getLeaseTenants = function (leaseId) {
        leaseService.getLeaseTenants(leaseId, function (status, data) {
            $scope.tenantArray = [];
            if (status) {
                $scope.leasetenantslist = data;
                if (data != null && data != undefined) {
                    tenantCounter = 1;
                    $scope.disable_btnTenants = true;
                    $scope.tenantArray.push({
                        counter: tenantCounter, showRemove: true, tenantid: data.Tenantid, tenantname: data.Tenantname,
                        tenantphoneno: data.Tenantphoneno, tenantemail: data.Tenantemail, isNew: false, isDeleted: false
                    });
                }
            }
            else {
                swal("ERROR", "Error in get lease tenants", "error");
                // alert('error in getLeaseTenants');
            }
        });
    }

    //#region "Tab Click events"
    $scope.step1Show = true;
    $scope.step2Show = false;
    $scope.step3Show = false;

    $scope.showtab1 = function () {
        $scope.step1Show = true;
        $scope.step2Show = false;
        $scope.step3Show = false;

        $("#step1").addClass("tab-pane fade in active");
        $("#step2").addClass("tab-pane fade");
        $("#step3").addClass("tab-pane fade");

        $("#liSelectProperty").removeClass("active");
        $("#liLeaseInfo").removeClass("active");
        $("#liTenantInfo").removeClass("active");
        $("#liSelectProperty").addClass("active");

        $("#steps11").css({ display: "block" });
        $("#steps12").css({ display: "none" });
        $("#steps13").css({ display: "none" });
    };

    $scope.showtab2 = function () {
        $scope.step1Show = false;
        $scope.step2Show = true;
        $scope.step3Show = false;

        $("#step1").addClass("tab-pane fade");
        $("#step2").addClass("tab-pane fade in active");
        $("#step3").addClass("tab-pane fade");

        $("#liSelectProperty").removeClass("active");
        $("#liLeaseInfo").removeClass("active");
        $("#liTenantInfo").removeClass("active");
        $("#liLeaseInfo").addClass("active");

        $("#steps11").css({ display: "none" });
        $("#steps12").css({ display: "block" });
        $("#steps13").css({ display: "none" });

    };

    $scope.showtab3 = function () {
        $scope.step1Show = false;
        $scope.step2Show = false;
        $scope.step3Show = true;

        $("#step1").addClass("tab-pane fade");
        $("#step2").addClass("tab-pane fade");
        $("#step3").addClass("tab-pane fade in active");

        $("#liSelectProperty").removeClass("active");
        $("#liLeaseInfo").removeClass("active");
        $("#liTenantInfo").removeClass("active");
        $("#liTenantInfo").addClass("active");

        $("#steps11").css({ display: "none" });
        $("#steps12").css({ display: "none" });
        $("#steps13").css({ display: "block" });

    };
    //#endregion

    $scope.getTenantData = function () {
        $rootScope.PersonList = null;
        masterdataService.getPerson(function (result, data) {
            if (result) {
                $scope.GetTenantByManagerId($rootScope.userData.ManagerDetail.pId);
            }
            else {
                swal("ERROR", "Unable to fetch Tenants", "error");
                //alert('Unable to fetch Tenants.');
            }
        });
    }

    //#region "Retrieve Master Data"
    $scope.getMasterData = function () {
        $scope.getTenantData();

        masterdataService.getAgreementTypes(function (result, data) {
            if (result) {
                $scope.agreementtypeslist = data; //angular.toJson(data, true);;
            }
            else {
                swal("ERROR", "Unable to fetch agreement types", "error");
                // alert('Unable to fetch Agreement Types.');
            }
        });
        masterdataService.getLeasePaymentFrequency(function (result, data) {
            if (result) {
                $scope.leasepaymentfrequencylist = data;
            }
            else {
                swal("ERROR", "Unable to fetch lease payment frequency list", "error");
                //alert('Unable to fetch Lease Payment Frequency List.');
            }
        });
    }
    //#endregion

    //#region "Tenant Events"
    $scope.tenantArray = [];
    $scope.pTenant = '';
    var tenantCounter = 0;

    $scope.addTenant = function ($event, frmAddTenant) {
        if (!angular.isUndefined($scope.pTenant) && $scope.pTenant != undefined) {
            tenantCounter++;
            $scope.disable_btnTenants = true;
            $scope.tenantArray.push({ counter: tenantCounter, showRemove: true, tenantid: $scope.pTenant.pId, tenantname: $scope.pTenant.pFullName, tenantphoneno: $scope.pTenant.pPhoneNo, tenantemail: $scope.pTenant.pEmail, isNew: true, isDeleted: false });
            $("#mdlAddTanents").modal('hide'); //PM 27-9-2018
            frmAddTenant.$setPristine();
            //$event.preventDefault();
        }
    };

    $scope.removeTenant = function (index) {

        for (var i = 0; i < $scope.tenantArray.length; i++) {
            if ($scope.tenantArray[i].counter == index) {
                if ($scope.tenantArray[i].isNew) {
                    $scope.disable_btnTenants = false;
                    $scope.tenantArray.splice(i, 1);
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.   
                    $scope.disable_btnTenants = false;
                    $scope.tenantArray[i].isDeleted = true;
                }
            }
        }
    }
    //#endregion
    //#region "Clause Events"
    $scope.clauseArray = [];
    $scope.pClauseId = '';
    $scope.pClauseType = '';
    $scope.pClauseDesc = '';
    var clauseCounter = 0;

    $scope.addClause = function ($event, frmClauses) {
        if (!angular.isUndefined($scope.pClauseType) && $scope.pClauseType != '') {
            clauseCounter++;
            $scope.clauseArray.push({
                counter: clauseCounter, showRemove: true,
                clauseid: $scope.pClauseId, clausetype: $scope.pClauseType, clausedesc: $scope.pClauseDesc, isNew: true, isDeleted: false
            });
            //$event.preventDefault();
            $scope.pClauseType = '';
            $scope.pClauseDesc = '';
            frmClauses.$setPristine();
        }
    };

    $scope.removeClause = function (index) {
        for (var i = 0; i < $scope.clauseArray.length; i++) {
            if ($scope.clauseArray[i].counter == index) {
                if ($scope.clauseArray[i].isNew) {
                    $scope.clauseArray.splice(i, 1);
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.
                    $scope.clauseArray[i].isDeleted = true;
                }
            }
        }
    };
    //#endregion
    //#region "Option Events"
    $scope.optionArray = [];
    $scope.pOptionId = '';
    $scope.pOptionType = '';
    $scope.pOptionDesc = '';
    $scope.pOptionReqdBy = '';
    $scope.pOptionEffOn = new Date();
    $scope.pOptionLastExec = new Date();

    var optionCounter = 0;

    $scope.addOption = function ($event, frmOptions) {
        if (!angular.isUndefined($scope.pOptionType) && $scope.pOptionType != '') {
            optionCounter++;
            $scope.pOptionEffOn = moment($scope.pOptionEffOn).format('l');;
            $scope.pOptionLastExec = moment($scope.pOptionLastExec).format('l');;
            $scope.optionArray.push({ counter: optionCounter, showRemove: true, optionid: $scope.pOptionId, optiontype: $scope.pOptionType, optiondesc: $scope.pOptionDesc, optionreqdby: $scope.pOptionReqdBy, optioneffon: $scope.pOptionEffOn, optionlastexec: $scope.pOptionLastExec, isNew: true, isDeleted: false });
            //$event.preventDefault();
            $scope.pOptionId = '';
            $scope.pOptionType = '';
            $scope.pOptionDesc = '';
            frmOptions.$setPristine();
        }
    };

    $scope.removeOption = function (index) {
        for (var i = 0; i < $scope.optionArray.length; i++) {
            if ($scope.optionArray[i].counter == index) {
                if ($scope.optionArray[i].isNew) {
                    $scope.optionArray.splice(i, 1);
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.
                    $scope.optionArray[i].isDeleted = true;
                }
            }
        }
    }
    //#endregion

    $scope.objLease = {
        pId: 0,
        pMID:0,
        pPropertyId: 0,
        pLeaseDesc: '',
        pLeaseStartDate: '',
        pLeaseEndDate: '',
        pAgreementTypeId: '',
        pLeaseType: '',
        pPaymentFrequency: 0,
        pSecurityDeposit: 0,
        pLeaseAmount: 0,
        pLeaseActive: 0,
        pLeaseTermId: 0,
        pLeaseTermTypeId: 0,
        pComments: '',
        pParkingSpaces: 0,
        pRentableArea: 0,
        pLatePayamentFee: 0,
        pLatePayamentChargedId: 0,
        pIsMoveInInspectionReqd: 0,
        pIsMoveOutInspectionReqd: 0,
        pMaxPeopleAllowedToLive: 0,

        pClauseArray: [],
        pOptionArray: [],
        pTenantArray: []
    }

    $scope.saveLease = function (isClose) {
        $scope.showLoader = true; //Pm 27-9-2018
        $scope.objLease.pId = $scope.pId;
        $scope.objLease.pPropertyId = $scope.selected_property.pId;
        $scope.objLease.pLeaseDesc = $scope.pLeaseDesc;

        $scope.objLease.pLeaseStartDate = $scope.pStartDate;
        $scope.objLease.pLeaseEndDate = $scope.pEndDate;
        

        $scope.objLease.pAgreementTypeId = $scope.pAgreementType;
        $scope.objLease.pLeaseType = $scope.pLeaseType;
        $scope.objLease.pPaymentFrequency = $scope.pPaymentFrequencyId;
        $scope.objLease.pSecurityDeposit = $scope.pSecurityDeposit;
        $scope.objLease.pLeaseAmount = $scope.pLeaseAmount;
        if ($scope.pLeaseActive == '1')
            $scope.objLease.pLeaseActive = true;
        else
            $scope.objLease.pLeaseActive = false;
        $scope.objLease.pLeaseTermId = $scope.pLeaseTerm;
        $scope.objLease.pLeaseTermTypeId = $scope.pLeaseTermType;
        $scope.objLease.pComments = $scope.pComments;
        $scope.objLease.pLatePayamentFee = $scope.pLatePayamentFee;
        $scope.objLease.pLatePayamentChargedId = $scope.pLatePayamentCharged;
        $scope.objLease.pIsMoveInInspectionReqd = parseInt($scope.pIsMoveInInspectionReqd);
        $scope.objLease.pIsMoveOutInspectionReqd = parseInt($scope.pIsMoveOutInspectionReqd);
        $scope.objLease.pMaxPeopleAllowedToLive = $scope.pMaxPeopleAllowedToLive;

        $scope.objLease.pClauseArray = $scope.clauseArray;
        $scope.objLease.pOptionArray = $scope.optionArray;
        $scope.objLease.pTenantArray = $scope.tenantArray;
        $scope.objLease.occupantArray = $scope.lstOccupant;
        $scope.objLease.createdId = $rootScope.userData.personId;
        $scope.objLease.pParkingSpaces = $scope.pParkingSpaces;
        $scope.objLease.pRentableArea = $scope.pRentableArea;
        $scope.objLease.pMID = $rootScope.userData.ManagerDetail.pId;

        if ($scope.pPaymentFrequencyId == 1) {
            $scope.objLease.pWeekDay = $scope.ShowSelectFrequency;
        }
        else {
            $scope.objLease.pWeekDay = $scope.mdlWeekDay;
        }
        $scope.objLease.pDayOfMonth = $scope.ShowSelectFrequency;
        $scope.objLease.pMonthNumber = $scope.mdlMonthNumber;

        //$scope.mdlWeekDay = '';
        //$scope.mdlMonthDay = '';
        //$scope.mdlMonthNumber = '';



        // $scope.leasejson = angular.toJson($scope.objLease, true);

        $scope.showLoader = true;
        leaseService.saveLease($scope.objLease).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   if (isClose == false) {
                       $scope.showtab3();
                   }
                   $scope.pId = response.Data;
                   if ($scope.documentArray.length > 0) {
                       $scope.saveDocuments(isClose);
                   }
                   else {
                       swal("SUCCESS", "Lease data saved successfully.", "success").then(function(value) {
                           window.location.href = "#/leases";
                       });
                       //swal({
                       //    title: "SUCCESS!",
                       //    text: "Lease data saved successfully.",
                       //    type: "success"
                       //}, function () {
                       //    $location.path('/leases');
                       //});
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });

        //leaseService.saveLease($scope.leasejson, function (status, resp) {
        //    $scope.showLoader = false; //Pm 27-9-2018
        //    if (status) {               
        //        if (isClose == false)
        //        {
        //            $scope.showtab3();
        //        }               
        //        $scope.pId = resp.Id;
        //        if ($scope.documentArray.length > 0)
        //        {
        //            $scope.saveDocuments(isClose);
        //        }                
        //    }
        //    else {
        //        swal("ERROR", "Error in save lease.", "error");
        //        //alert('Unable to save Lease.');
        //    }
        //});
    };

    $scope.objPerson = {
        pTypeId: 0,
        pFirstName: '',
        pLastName: '',
        pEmail: '',
        pPhoneNo: ''
    }

    $scope.savePerson = function (frmAddTenant) {

        $scope.objPerson.pId = 0;
        $scope.objPerson.pTypeId = 3; //Tenant
        $scope.objPerson.pFirstName = $scope.pFirstName;
        $scope.objPerson.pLastName = $scope.pLastName;
        $scope.objPerson.pEmail = $scope.pEmail;
        $scope.objPerson.pPhoneNo = $scope.pPhoneNo;
        $scope.objPerson.pPropertyId = $scope.selected_property.pId;
        $scope.objPerson.pAddressId = $scope.selected_property.pAddressId;

        $scope.personjson = angular.toJson($scope.objPerson, true);
        $scope.showLoader = true;
        personService.savePerson($scope.objPerson, function (status, resp) {
            $scope.showLoader = false;
            if (status) {
                $("#mdlAddTanents").modal('hide');
                $scope.getTenantData();
                swal("SUCCESS", "Tenant saved successfully.", "success");
                //alert("Tenant Saved Successfully");
                tenantCounter = $scope.tenantArray.length + 1;
                $scope.tenantArray.push({ counter: tenantCounter, showRemove: true, tenantid: resp.Id, tenantname: $scope.objPerson.pLastName + ' ' + $scope.pFirstName, tenantphoneno: $scope.objPerson.pPhoneNo, tenantemail: $scope.objPerson.pEmail, isNew: true, isDeleted: false });
                $scope.disable_btnTenants = true;
                $scope.TenantFieldEmpty(frmAddTenant);
                frmAddTenant.$setPristine(frmAddTenant);
            }
            else {
                swal("ERROR", "Unable to add new tenant.", "error");
                // alert('Unable to add new tenant.');
            }
        });
    };

    $scope.TenantFieldEmpty = function (frmAddTenant) {
        $scope.objPerson.pFirstName = '';
        $scope.objPerson.pLastName = '';
        $scope.objPerson.pEmail = '';
        $scope.objPerson.pPhoneNo = '';
        $scope.pFirstName = '';
        $scope.pLastName = '';
        $scope.pEmail = '';
        $scope.pPhoneNo = '';
        frmAddTenant.$setPristine();
    }

    //#region Documents Tab 
    $scope.documentArray = [];
    var docCounter = 0;
    $scope.addDocument = function ($event) {
        docCounter++;
        $scope.documentArray.push({ counter: docCounter, showRemove: true, document: '', documentPath: '', documentName: '', documentTags: '', isNew: true, isDeleted: false });
        $event.preventDefault();
    };

    $scope.removeDocument = function (index) {
        for (var i = 0; i < $scope.documentArray.length; i++) {
            if ($scope.documentArray[i].counter == index) {
                if ($scope.documentArray[i].isNew) {
                    $scope.documentArray.splice(i, 1);
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.
                    $scope.documentArray[i].isDeleted = true;
                }
            }
        }
    }

    $scope.documentSelected = function (element) {
        for (var i = 0; i < element.files.length; i++) {
            var index = docCounter;
            $scope.documentArray.push({ counter: docCounter++, showRemove: true, document: element.files[i], documentPath: '', documentName: element.files[i].name, documentDesc: '', documentTags: '', isNew: true, isDeleted: false });
            var reader = new FileReader();
            $scope.$apply();
        }
    };

    //#region Save Documents 
    $scope.saveDocuments = function (isClose) {
        var fd = new FormData();

        for (var i = 0; i < $scope.documentArray.length; i++) {
            fd.append("file", $scope.documentArray[i].document);
            fd.append("filename", $scope.documentArray[i].documentName);
            fd.append("propid", $scope.pId);
            fd.append("moduleid", 'LEASE');
            fd.append("fileType", 'document');
            fd.append("seqNo", $scope.documentArray[i].counter);
            fd.append("filedesc", $scope.documentArray[i].documentDesc);
            fd.append("filetags", $scope.documentArray[i].documentTags);
            fd.append("idDeleted", $scope.documentArray[i].isDeleted);
        }
        propertyService.saveFiles(fd, '/api/leasedocument', function (status, resp) {
            if (status) {
                if (isClose) {
                    $location.path('/leases');
                }
                swal("SUCCESS", "Lease data saved successfully.", "success");
            }
            else {
                swal("ERROR", "Unable to add documents.", "error");
                //  alert('Unable to add documents.');
            }
        });
    }
    //#endregion

    //#endregion

    $scope.getPropertyTypes = function () {

        masterdataService.getPropertyTypes(function (result, data) {
            if (result) {
                $scope.propertytypeslist = data;
                // $scope.pPropertyType = $scope.propertytypeslist.filter(function (c) {
                //     return c.Id == $scope.pPropertyTypeId;
                // });

                for (var i = 0; i < $scope.propertytypeslist.length; i++) {
                    if ($scope.pPropertyTypeId == $scope.propertytypeslist[i].Id) {
                        $scope.pPropertyType = $scope.propertytypeslist[i];
                        break;
                    }
                }
                for (var i in $scope.propertytypeslist) {
                    $scope.useType.push({ counter: i, id: $scope.propertytypeslist[i].Id, name: $scope.propertytypeslist[i].Name, checked: true });
                }

            }
            else {
                swal("ERROR", "Unable to get property types.", "error");
                //alert('Unable to fetch Property Types.');
            }
        });
    }

    //#region "Filters"
    $scope.showfilter = function () {
        if ($(".vt-filters").css('right') == '-186px') {
            $(".vt-filters").animate({ right: '0' }, 500);
            $(".vt-filterdiv").animate({ width: "185px" });
        }

    };

    $scope.closefilter = function () {
        $(".vt-filters").animate({ right: '-186px' }, 500);
        $(".vt-filterdiv").animate({ width: "140px" });
    };

    $scope.statuslist = [{ Id: 1, Name: 'Active' }, { Id: 2, Name: 'InActive' }];
    $scope.useStatus = [{ Id: 1, Name: 'Active', checked: true }, { Id: 2, Name: 'InActive', checked: true }];
    $scope.useType = [];
    var isStatusSelected = false;
    var isTypeSelected = false;

    $scope.filterBySelection = function () {
        return function (p) {
            if (!angular.isUndefined(p)) {
                isStatusSelected = true;
                isTypeSelected = true;

                if (!angular.isUndefined($scope.statuslist) && !angular.isUndefined($scope.useStatus) && $scope.useStatus.length > 0) {
                    for (var i in $scope.statuslist) {
                        if (p.pLeaseActive == $scope.useStatus[i].Name) {
                            isStatusSelected = $scope.useStatus[i].checked;
                            break;
                        }
                    }
                }
                if (!angular.isUndefined($scope.propertytypeslist) && !angular.isUndefined($scope.useType) && $scope.useStatus.length > 0) {
                    for (var i in $scope.propertytypeslist) {
                        if (p.pTypeId == $scope.useType[i].id) {
                            isTypeSelected = $scope.useType[i].checked;
                            break;
                        }
                    }
                }
                return isStatusSelected && isTypeSelected;
            }
            else
                return true;
        };
    };

    //#endregion

    $scope.showDocs = function () {
        $("#leaseDocs").css({ display: "block" });
        $("#leaseFinancials").css({ display: "none" });
        $("#leaseCnO").css({ display: "none" });
        $scope.activeTap = "Docs";
    }

    $scope.showLeaseFin = function () {
        $("#leaseDocs").css({ display: "none" });
        $("#leaseFinancials").css({ display: "block" });
        $("#leaseCnO").css({ display: "none" });
        $scope.activeTap = "Financials";
    }

    $scope.showCnO = function () {
        $("#leaseDocs").css({ display: "none" });
        $("#leaseFinancials").css({ display: "none" });
        $("#leaseCnO").css({ display: "block" });
        $scope.activeTap = "Clauses";
    }
    $scope.leaseInfo = {};
    $scope.propertyInfo = {};
    $scope.pEntryMode = "Create";
    $scope.editlease_ByLeaseView = false;
    if (!angular.isUndefined($routeParams.LeaseNum) && $routeParams.LeaseNum != -1) {
        $scope.showLoader = true;
        $scope.editlease_ByLeaseView = true;
        leaseService.getLeaseById($routeParams.LeaseNum, function (status, data) {
            $scope.showLoader = false;
            if (status) {
                if (data != null) {
                    $scope.mdlLeaseType = [];
                    $scope.mdlLeaseType = data.lstLeaseType;

                    $scope.leaseInfo = data.LeaseDetail;
                    $scope.leaseInfo.pPaymentFrequencyId = data.LeaseDetail.pPaymentFrequencyId.toString();
                    if ($scope.leaseInfo.pPaymentFrequencyId == 1) {
                        $scope.frequency = 'Weekly';
                        $scope.ShowSelectFrequency = data.LeaseDetail.pWeekDay;
                    }
                    else {
                        $scope.ordinal_suffix_of(data.LeaseDetail.pDayOfMonth);
                        $scope.ShowSelectFrequency = data.LeaseDetail.pDayOfMonth;
                    }
                    $scope.pLeaseAmount = data.LeaseDetail.pRentalAmount;
                    $scope.pAgreementType = data.LeaseDetail.pAgreementTypeId.toString();
                    $scope.pIsMoveInInspectionReqd = data.LeaseDetail.pIsMoveInInspectionReqd;
                    $scope.pIsMoveOutInspectionReqd = data.LeaseDetail.pIsMoveOutInspectionReqd;
                    $scope.objLease.pIsMoveInInspectionReqd = data.LeaseDetail.pIsMoveOutInspectionReqd;
                    $scope.pLatePayamentFee = data.LeaseDetail.pLatePaymentFee;
                    $scope.pRentableArea = data.LeaseDetail.pRentableArea;

                    //**********************
                    $scope.pStartDate = new Date(data.LeaseDetail.pStartDate);                    
                    $scope.pEndDate = new Date(data.LeaseDetail.pEndDate);
                                       
                    //**********************

                    $scope.pLatePayamentCharged = data.LeaseDetail.pLatePaymentFeeChargedId.toString();
                    $scope.pMaxPeopleAllowedToLive = data.LeaseDetail.pMaxPeopleAllowed;
                    $scope.pEntryMode = "Edit";
                    $scope.getLeaseTenants($scope.leaseInfo.pId);
                    $scope.GetDocumentByLeaseId($scope.leaseInfo.pId);
                    $scope.GetPhotos($scope.leaseInfo.pPropertyId);
                    $scope.GetPropertyByPropertyId($scope.leaseInfo.pPropertyId, function (data2) {
                        if (data2.pParkingSpaces == null) {
                            $scope.selected_property.pParkingSpaces = 0;
                        }
                        else {
                            $scope.selected_property.pParkingSpaces = parseInt($scope.selected_property.pParkingSpaces);
                        }
                        if (data.LeaseDetail.pParkingSpaces != null) {
                            $scope.pParkingSpaces = parseInt(data.LeaseDetail.pParkingSpaces);
                        }
                        //Get financial data for leased property
                        $scope.LoadLeaseFinancial($scope.leaseInfo.pPropertyId, $scope.leaseInfo.pId);
                    });
                    $scope.getMasterData();
                    $scope.GetLeaseClausesByLeaseId($scope.leaseInfo.pId);
                    $scope.GetLeaseOptions($scope.leaseInfo.pId);
                    $scope.GetLeaseOccupantsByLeaseId($scope.leaseInfo.pId);
                }
            }
            else {
                swal("ERROR", "Unable to get lease details.", "error");
                //alert('error in getProperty');
            }
        });
    }

    //PM 26-09-2018
    $scope.GetLeaseOptions = function (leaseId) {
        leaseService.getLeaseOptions(leaseId, function (result, data) {
            if (result) {
                $scope.leaseInfo.optionsArray = data;
                //PM 28-9-2018
                angular.forEach(data, function (value, key) {
                    if (value.IsDeleted == false) {
                        optionCounter++;
                        $scope.optionArray.push({
                            counter: optionCounter, showRemove: true, optionid: value.Id, optiontype: value.OptionType,
                            optiondesc: value.OptionDesc, optionreqdby: value.RequiredBy, optioneffon: value.EffectiveOn,
                            optionlastexec: value.LastExecution, isNew: false, isDeleted: false
                        });
                    }

                });

                //End PM 28-9-2018
            }
            else {
                swal("ERROR", "Unable to fetch options for Lease.", "error");
                //alert('Unable to fetch options for Lease.')
            }
        });
    }
    $scope.GetLeaseClausesByLeaseId = function (leaseId) {
        $scope.showLoader = true;
        leaseService.getLeaseClauses(leaseId, function (result, data) {
            $scope.showLoader = false;
            if (result) {
                $scope.leaseInfo.clausesArray = data;
                //PM 28-09-2018
                angular.forEach(data, function (value, key) {
                    if (value.IsDeleted == false) {
                        clauseCounter++;
                        $scope.clauseArray.push({
                            counter: clauseCounter, showRemove: true, clauseid: value.Id, clausetype: value.ClauseType,
                            clausedesc: value.ClauseDesc, isNew: false, isDeleted: false
                        });
                    }
                });
                //End PM 28-09-2018
            }
            else {
                swal("ERROR", "Unable to fetch Clauses for Lease.", "error");
                //alert('Unable to fetch Clauses for Lease.')
            }
        });
    }
    $scope.GetPropertyByPropertyId = function (propertyId, callback) {
        $scope.showLoader = true;
        propertyService.getProperty(propertyId, function (status, data) {
            $scope.showLoader = false;
            if (status) {
                if (data != null) {
                    $scope.propertyInfo = data;
                    $scope.selected_property = $scope.propertyInfo;
                    $scope.pLeaseDesc = $scope.leaseInfo.pLeaseDesc;
                    $scope.pParkingSpaces = $scope.selected_property.pParkingSpaces;
                    $scope.pLeaseId = $scope.leaseInfo.pLeaseId;
                    $scope.pLeaseType = $scope.leaseInfo.pLeaseType;
                    $scope.pAgreementTypeId = $scope.leaseInfo.pAgreementTypeId;
                    $scope.pPaymentFrequencyId = $scope.leaseInfo.pPaymentFrequencyId;
                    $scope.pSecurityDeposit = $scope.leaseInfo.pSecurityDeposit;
                    $scope.pId = $scope.leaseInfo.pId;
                    callback(data);
                }
            }
            else {
                swal("ERROR", "Error in get property.", "error");
                //alert('error in getProperty');
            }
        });
    }
    $scope.openUrl = function (url) {
        window.open("/Uploads/Leases/Documents/" + url, '_blank', 'height=800,width=600');
    }

    $scope.GetDocumentByLeaseId = function (leaseId) {
        $scope.showLoader = true;
        leaseService.getLeaseDocuments(leaseId, function (result, data) {
            $scope.showLoader = false;
            if (result) {
                $scope.leaseInfo.docsArray = data;
                angular.forEach(data, function (value, key) {
                    docCounter++;
                    $scope.documentArray.push({
                        counter: docCounter, showRemove: true, document: value.Name, documentPath: value.FileSavePath,
                        documentName: value.Name, documentTags: value.Tags, isNew: false, isDeleted: false,
                        documentDesc: value.Description
                    });
                });
            }
            else {
                swal("ERROR", "Unable to fetch documents for Lease.", "error");
                //alert('Unable to fetch documents for Lease.')
            }
        });

    }
    $scope.GetPhotos = function (propertyId) {
        $scope.showLoader = true;
        propertyService.getPropertyPhotos(propertyId, function (result, data) {
            $scope.showLoader = false;
            if (result) {
                angular.forEach(data, function (value, key) {
                    var arr = value.FileSavePath.split("\\");
                    value.imageName = arr[arr.length - 1];
                });
                $scope.photos = data;
            }
            else {
                swal("ERROR", "Unable to fetch photos for property.", "error");
                //alert('Unable to fetch photos for property.')
            }
        });
    }
    $scope.GetPropertyByDropdown = function (propertyModel) {
        $scope.selectPropertyModel = propertyModel;
        $scope.GetPropertyByPropertyId($scope.selectPropertyModel.pId, function (data) {
        });
        $scope.GetLeaseType(propertyModel.pCategoryId);

    }

    $scope.GetLeaseType = function (propertyCategoryId) {
        $scope.showLoader = true;

        leaseService.getLeaseType(propertyCategoryId).
          success(function (response) {
              if (response.Status == true) {
                  $scope.showLoader = false;
                  if (response.Data.length > 0) {
                      $scope.mdlLeaseType = response.Data;
                  }
              }
              else {
                  swal("ERROR", "Unable to get lease types.", "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.ShowAddTenetsMdl = function () {
        $("#mdlAddTanents").modal();
    }
    $scope.ShowAddClausesMdl = function () {
        $("#mdlClauses").modal();
    }
    $scope.ShowAddOptionsMdl = function () {
        $("#mdlOptions").modal();
    }
    //End PM 26-09-2018

    //PM 01-10-2018

    $scope.occupantsMdl = {
        id: 0,
        firstName: '',
        lastName: '',
        relation: '',
        detail: '',
        leaseId: '',
        isDeleted: false
    }

    $scope.RenewLeaseMdl = {
        leaseId: '',
        leaseStartDate: new Date(),
        leaseEndDate: new Date(),
        leaseAmount: 0
    }

    $scope.ShowRenewLeasePopUp = function () {
        $("#renewLease_PopUp").modal();
    }

    $scope.ShowAddOccupantsMdl = function () {
        $("#mdlOccupants").modal();
    }

    $scope.AddOccupants = function (occupant, frmOccupants) {
        $scope.lstOccupant.push(occupant);

        $scope.occupantsMdl = {
            id: 0,
            firstName: '',
            lastName: '',
            relation: '',
            detail: '',
            leaseId: '',
            isDeleted: false
        }
        frmOccupants.$setPristine();
    }

    $scope.CloseMdl_Occupants = function (frmOccupants) {
        $scope.occupantsMdl = {
            id: 0,
            firstName: '',
            lastName: '',
            relation: '',
            detail: '',
            leaseId: '',
            isDeleted: false
        }
        frmOccupants.$setPristine();
        $("#mdlOccupants").modal("hide");
    }

    $scope.RemoveOccupants = function (index, occupant) {
        if (occupant.id == 0) {
            $scope.lstOccupant.splice(index, 1);
        }
        else {
            occupant.isDeleted = true;
        }
    }

    $scope.ShowLeaseInfo = function () {
        $scope.showLeaseFin();
    }
    
    $scope.GetLeaseOccupantsByLeaseId = function (leaseId) {
        $scope.showLoader = true;
        leaseService.getLeaseOccupants(leaseId, function (result, data) {
            $scope.showLoader = false;
            if (result) {
                angular.forEach(data, function (value, key) {
                    $scope.lstOccupant.push(value);
                });
            }
            else {
                swal("ERROR", "Unable to fetch clauses for lease.", "error");
                //alert('Unable to fetch Clauses for Lease.')
            }
        });
    }

    $scope.startDateChanged = function () {
      //  $scope.pStartDate = moment($scope.pStartDate).format('YYYY/MM/DD');
        $scope.pEndDate = $scope.pStartDate;

    }

    $scope.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    //$scope.endDateChanged = function () {
    //    $scope.pEndDate = moment($scope.pEndDate).format('YYYY/MM/DD');
    //    //$scope.pStartDate = moment($scope.pStartDate).format('YYYY/MM/DD');
    //    //if($scope.pEndDate<$scope.pStartDate)
    //    //{
    //    //    $scope.pEndDate = $scope.pStartDate;
    //    //}
    //}

    $scope.RenewLease = function () {
        $scope.RenewLeaseMdl = {
            leaseId: $routeParams.LeaseNum,
            leaseStartDate: $scope.pStartDate,
            leaseEndDate: $scope.pEndDate,
            leaseAmount: $scope.pLeaseAmount,
            createdId: $rootScope.userData.personId
        }
        $scope.showLoader = true;
        leaseService.renewLease($scope.RenewLeaseMdl).success(function (response) {
            $scope.showLoader = false;
            if (response.Status == true) {
                $location.path('/leases');
            }
            else {
                swal("ERROR", MessageService.responseError, "error");
            }
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    $scope.ConfirmMessage_Ok = function () {
        $scope.pTerminationDesc = "";
        $("#deActiveLease").modal('hide');
        $("#LeaseTerminationDesc").modal('show');
    }

    $scope.ConfirmTermination = function () {

        $scope.leaseInfo.pTerminationDesc = $scope.pTerminationDesc;
        $scope.TerminateLease($scope.leaseInfo);
    }

    $scope.TerminateLease = function (lease) {
        $scope.showLoader = true;

        leaseService.terminateLease(lease).success(function (response) {
            $("#LeaseTerminationDesc").modal('hide');
            $scope.showLoader = false;
            if (response.Status == true && response.Data != null) {
                $location.path('/leases');
            }
            else {
                swal("ERROR", MessageService.responseError, "error");
            }
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    $scope.ShowPopUpTerminateLease = function () {
        $("#deActiveLease").modal();
    }

    $scope.EmailExist = function () {
        if ($scope.pEmail == undefined || $scope.pEmail == '') {
            return;
        }

        $scope.showLoader = true;
        AddNewUserService.EmailExist($scope.pEmail).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.isPersonEmailExist = response.Data;
               }
               else {
                   swal("ERROR", "", "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    //End 01-10-2018 

    $scope.GetTenantByManagerId = function (managerId) {

        $scope.showLoader = true;
        masterdataService.GetTenantByManagerId(managerId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.tenantlist = response.Data;
               }
               else {
                   swal("ERROR", "Error in get tenant", "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.CloseMdl_Clause = function (frmClauses) {
        $scope.pClauseType = '';
        $scope.pClauseDesc = '';
        frmClauses.$setPristine();
        $("#mdlClauses").modal("hide");
    }

    $scope.CloseMdl_Options = function (frmOptions) {
        $scope.pOptionId = '';
        $scope.pOptionType = '';
        $scope.pOptionDesc = '';
        frmOptions.$setPristine();
        $("#mdlOptions").modal("hide");
    }

    $scope.CloseMdl_Tenant = function (frmAddTenant) {
        $scope.TenantFieldEmpty(frmAddTenant);
        $("#mdlAddTanents").modal("hide");
    }

    $scope.MessageToPerson = function (EmailId, PersonType) {
        openchat();
        var PersonTypeId = 0;
        if ($rootScope.PersonTyp.owner.toUpperCase() == PersonType.toUpperCase()) {
            PersonTypeId = $rootScope.PersonTypeIdEnum.owner;
        }
        else if ($rootScope.PersonTyp.tenant.toUpperCase() == PersonType.toUpperCase()) {
            PersonTypeId = $rootScope.PersonTypeIdEnum.tenant;
        }
        $rootScope.$emit("OpenPersonChatWindow", { EmailId: EmailId, PersonTypeId: PersonTypeId });
    }

    $scope.ViewLeaseDetails = function (leaseId, propertyId, leaseStartDate, leaseEndDate) {
        //leaseService.leaseStartDate = leaseStartDate;
        //leaseService.leaseEndDate = leaseEndDate;
        var mdlLeaseDuration = {
            leaseStartDate: leaseStartDate,
            leaseEndDate: leaseEndDate
        };
        storageService.save('mdlLeaseDuration', mdlLeaseDuration);
        $location.path("/leaseview/" + leaseId + "/" + propertyId);
    }

    $scope.LoadLeaseFinancial = function (propertyId, leaseId) {
        $scope.showLoader = true;
        $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};

        propertyService.getOwnerIncomeExpenseByPropertyIdLeaseId(propertyId, leaseId).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = angular.copy(response.Data);
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $scope.showLoader = false;
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }).finally(function (data) {
                $scope.showLoader = false;
                $scope.personId = $rootScope.userData.personId;
            });
    }

    $scope.ShowSelectDayDate = function () {
        if ($scope.SelectDay!='') {
            $scope.ShowSelectFrequency = $scope.SelectDay;
        }
        else {
            $scope.ordinal_suffix_of($scope.mdlMonthDay);
            $scope.ShowSelectFrequency = $scope.mdlMonthDay;
        }
        $('#mdlRentPaymentFrequency').modal('hide');
    }

    $scope.showSelectWeekDay = function (WeekDay) {
        $scope.SelectDay = WeekDay;
        $scope.ShowSelectFrequency = $scope.SelectDay;
    }

    $scope.ShowFrequencyModel = function () {
        if ($scope.pPaymentFrequencyId == 1) {
            $scope.mdlWeekDay = $scope.ShowSelectFrequency;
        }
        else {
            $scope.mdlMonthDay = $scope.ShowSelectFrequency;
        }
        $('#mdlRentPaymentFrequency').modal('show');
    }

    $scope.ordinal_suffix_of = function (i) {


        var suffixes = ["th", "st", "nd", "rd"];
        var dtfilter =  i;
        var day = parseInt(dtfilter, 10);
        var relevantDigits = (day < 30) ? day % 20 : day % 30;
        var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
        $scope.suffix = suffix;
        return suffix;
    }
});
