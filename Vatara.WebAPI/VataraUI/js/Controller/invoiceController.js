﻿vataraApp.controller('invoiceController', invoiceController);
invoiceController.$inject = ['$scope', '$window', 'invoiceService', 'billService', 'serviceCategory_service',
    'masterdataService', 'pagingService', '$filter', 'onlineStatementService', '$routeParams', '$location', '$rootScope', 'commonService', 'workorderService', 'financeService'];
function invoiceController($scope, $window, invoiceService, billService, serviceCategory_service,
    masterdataService, pagingService, $filter, onlineStatementService, $routeParams, $location, $rootScope, commonService, workorderService, financeService) {
    var IC = this;

    IC.pageShowOnOwner_TenantContact = false;
    IC.personTypeForOwner_TenantContact = 0;
    IC.owner_TenantIdForOwner_TenantContact = 0;
    IC.pageShowOnTenantContact = false;
    IC.managerid = $rootScope.userData.personId;
    IC.isManager = false;
    $scope.menuActive = 'Invoices';
    $scope.menuUrl = 'invoices';
    IC.PropertyId = 1;
    IC.lstProperty = [];
    IC.lstPropertyOwnerTenant = [];
    IC.lstPropertyOwnerTenantByPropertyId = [];
    IC.newinvoiceNumber = true;
    $scope.showLoader = false;
//    $rootScope.ActiveTabName = '';

    IC.lstServiceCategory = [];
    IC.lstInvoiceDetail = [];
    IC.invoice_Text = '';
    IC.invoice_Number = '';
    IC.lstWorkRequests = [];
    IC.MdlServiceCategory = {
        name: '',
        description: '',
        creatorUserId: IC.managerid
    }
    IC.lstPropertOwnersByManager = [];
    IC.CopylstPropertOwnersByManager = [];
    IC.lstPropertByManagerAndOwner = [];
    IC.CopylstPropertByManagerAndOwner = [];
    IC.filterPropertyList = [];
    var currentDates = new Date();
    IC.selectCurrentPeriod = "";
    IC.lstInvoices = [];
    IC.lstInvoices_Filtered = [];
    IC.fromDate = new Date();
    IC.toDate = new Date();
    IC.PageService = pagingService;
    // IC.itemsPerPage = IC.PageService.itemsPerPage;

    IC.itemsPerPage = _ItemsPerPage;
    IC.itemsPerPageArr = IC.PageService.itemsPerPageArr;
    IC.ItemCountPerPage = IC.itemsPerPage;

    IC.currentPage_lstInvoices = 0;
    IC.lstInvoices_OrderBy = "";
    IC.IsEdit = false;
    IC.createEdit = false;
    IC.isSentToClient = false;
    IC.InvoiceFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    }

    IC.lstMarkupType = ['pct', 'amt'];
    IC.invoiceDetail = {
        Id: 0,
        category: "",
        VendorInvRefNo: '',
        WRNumber: '',
        amount: 0,
        description: '',
        statusId: 0,
        showRemove: true,
        markup: 0,
        markup_type: 'pct',
        tax: 0,
        totalAmount: 0,
        creatorUserId: IC.managerid,        
        woid:0
    }

    IC.arrInvoiceFilterCategory = ['All', 'Auto', 'Manual'];
    IC.invoiceFilterCategory = IC.arrInvoiceFilterCategory[0];

    IC.invoice = {
        id: 0,
        invoiceDate: new Date(),
        dueDate: new Date(),
        invoiceNumber: '',
        description: '',
        property: "",
        client: "",
        clientType: "",
        creatorUserId: IC.managerid,
        propertyModel: {},
        from: 0,
        fromAddress: '',
        clientName: '',
        senderName: '',
        senderPhoneNo: '',
        toAddress: '',
        ownerID: 0,
        ownerName: '',
        ownerAddress: '',
        propertyName: '',
        propertyAddress: '',
        recipientEmail: $rootScope.userData.ManagerDetail.pEmail
    }

    IC.mdlPayment = {
        amount: 0,
        invoiceId: 0,
        description: '',
        manageId: IC.managerid,
        payMode: '',
        paymentMode: '',
        writeOff: false
    }
    IC.invoiceData = {
        payableAmt: 0,
        invoiceNo: '',
        invoiceDate: '',
        recipientName: '',
        propertyName: ''
    }

    IC.PaymentDetail = [];
    IC.TotalAmount = 0;
    IC.TotalPaid = 0;
    IC.AmountDue = 0;
    IC.PieChartArray = [];
    IC.InvoiceStatusArray = [];
    IC.GraphArray = [];
    IC.groupByStatus = {};
    IC.savedInDraft = 0;
    IC.sendToClient = 0
    IC.ShowMdlInvPymt = false;
    IC.ShowWriteOff = false;
    IC.EnableSave = false;

    IC.mdlCity = {

        id: 0,
        name: '',
        description: '',
        stateId: 0
    };
    $rootScope.PaymentAgainstInvoice = false;

    $rootScope.InvoicePaymentDetails = {

        intId: 0,
        invNumber: "",
        from: 0,
        fromEmail: 0,
        to: 0,
        propertyId: 0,
        propertyName: "",
        invDate: "",
        invDueDate: "",
        amount: 0,
        paid: 0,
        dueAmt: 0
    }

    IC.Selected_fromDate = new Date();
    IC.Selected_toDate = new Date();
    IC.pmid = $rootScope.userData.ManagerDetail.pId;
    IC.personId = $rootScope.userData.personId;
    IC.propertyId = 0;
    IC.personType = $rootScope.userData.personTypeId;
    IC.POID = 0;
    IC.ownerIsCompany = undefined;

    IC.SetItemsPerPage = function (count) {

        IC.currentPage_lstInvoices = 0;
    }

    IC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    }

    IC.LoadDataForCreateInvoiceForRequestingPayment = function (propertyId, personId) {

        //var invoiceDetailMdl = angular.copy(IC.invoiceDetail);
        //invoiceDetailMdl.showRemove = false;
        //IC.lstInvoiceDetail.push(invoiceDetailMdl);
        IC.GetPropertiesByType(function (data) {
            IC.selectedProperty = data.filter(function (p) { return p.id == propertyId })[0];
        });
        IC.GetPropertyTenantOwner(function (data) {
            IC.GetPropertyTenantOwnerBy_PropId(IC.selectedProperty, function (TenantOwnerData) {

                IC.lstPropertyOwnerTenantByPropertyId = IC.lstPropertyOwnerTenant.filter(function (Property) { return Property.PropertyId === IC.invoice.property });  //&& Property.personId != $rootScope.userData.personId
                IC.GetWorkRequestsByProperty(IC.invoice.property, function (WorkRequestData) {
                    IC.lstWorkRequests = angular.copy(WorkRequestData);
                    IC.invoice.client = IC.lstPropertyOwnerTenantByPropertyId.filter(function (p) { return p.personId == personId && p.PersonType.toLowerCase() == PersonTypeEnum.tenant.toLowerCase() && p.PropertyId == propertyId })[0];
                });
            });
        });
        IC.GetServiceCategory($rootScope.userData.personTypeId);
    }


    IC.LoadInvoices = function () {
        PaymentAgainstInvoice = false;
        IC.lstCurrentPeriod = angular.copy(Dateranges);
        IC.selectCurrentPeriod = IC.lstCurrentPeriod[0];

        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("createInvoice/") && !angular.isUndefined($routeParams.LeaseNum)) {

                IC.IsEdit = false;
                IC.createEdit = true;
                IC.isManager = true;
                IC.Clear_SaveInvoice();
                IC.GetCurrentInvoiceNumberByManager();
                IC.LeaseNum = $routeParams.LeaseNum;
                IC.LoadDataForCreateInvoiceForRequestingPayment($routeParams.PropertyId, $routeParams.TenantId);

                //  IC.LoadFunction();  // used in Create Invoice
            }
            else {
                if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                    var propertyId = 0;
                    IC.LoadDataForOwner(propertyId);

                } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                    var propertyId = 0;
                    IC.LoadDataForTenant(propertyId);
                }
                else {
                    $rootScope.ActiveTabName = '';
                    IC.LoadDataForPropertyManager();
                }
            }
        }
        else {
            IC.LoadDataForOwnerTenant();
           // $rootScope.ActiveTabName = '';
        }
    }

    IC.LoadDataForPropertyManager = function () {

        IC.isManager = true;
        IC.GetPropertOwnersByManager();
        IC.LoadFunction();  // used in Create Invoice
        IC.GetPropertByManagerAndOwner();
        IC.FilterInvoices();
    }

    IC.LoadDataForOwnerTenant = function () {
        IC.isManager = false;
        IC.managerid = $rootScope.userData.personId;
        IC.pmid = $rootScope.userData.ManagerDetail.pId;
        IC.personId = $rootScope.userData.personId;
        IC.propertyId = 0;
        IC.personType = $rootScope.userData.personTypeId;

        if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
            var propertyId = 0;
            IC.LoadDataForOwner(propertyId);
        }
        else {    // for tenant
            //1 get property
            //2 get invoices
            var propertyId = 0;
            IC.LoadDataForTenant(propertyId);
        }
    }

    IC.LoadDataForOwner = function (propertyId) {

        //IC.Selected_fromDate = new Date(new Date().getFullYear(), 0, 1);
        //IC.Selected_toDate = new Date();

        IC.Selected_fromDate = IC.selectCurrentPeriod.Startdate === undefined ? IC.fromDate : IC.selectCurrentPeriod.Startdate;
        IC.Selected_toDate = IC.selectCurrentPeriod.EndDate === undefined ? IC.toDate : IC.selectCurrentPeriod.EndDate;

        if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
            IC.pageShowOnOwner_TenantContact = true;
            IC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
            IC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
            if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {

                IC.personId = $routeParams.ownerId;
                IC.POID = $routeParams.ownerPOID;
                IC.ownerIsCompany = $routeParams.ownerIsCompany;
                IC.personType = $rootScope.PersonTypeIdEnum.owner;
            }
            IC.propertyId = propertyId;
        }
        else {

            IC.POID = $rootScope.userData.poid;
            IC.managerid = $rootScope.userData.personManageriD;
        }
        IC.GetInvoicesByPersonAndProperty(IC.Selected_fromDate, IC.Selected_toDate, IC.POID, IC.personId, IC.propertyId, IC.personType);
    }

    IC.LoadDataForTenant = function (propertyId) {

        IC.Selected_fromDate = IC.selectCurrentPeriod.Startdate === undefined ? IC.fromDate : IC.selectCurrentPeriod.Startdate;
        IC.Selected_toDate = IC.selectCurrentPeriod.EndDate === undefined ? IC.toDate : IC.selectCurrentPeriod.EndDate; 

        if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
            IC.pageShowOnOwner_TenantContact = true;
            IC.pageShowOnTenantContact = true;
            IC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
            IC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;

            IC.POID = 0;
            IC.personId = $routeParams.tenantId;
            IC.propertyId = propertyId;
            IC.personType = $rootScope.PersonTypeIdEnum.tenant;
        }
        else {
            IC.POID = $rootScope.userData.poid;
            IC.managerid = $rootScope.userData.personManageriD;
            IC.propertyId = $rootScope.selectedPropertyId;
        }
        if (IC.propertyId > 0 && IC.Selected_fromDate != undefined && IC.Selected_toDate != undefined) {
            IC.GetInvoicesByPersonAndProperty(IC.Selected_fromDate, IC.Selected_toDate, IC.POID, IC.personId, IC.propertyId, IC.personType);
        }
    }

    IC.GetPropertyByPerson = function (ownerPOID, personId, managerId, personType, callback) {

        $scope.showLoader = true;
        financeService.getPropertyByPerson(ownerPOID, personId, managerId, personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   //FC.lstPropertByManagerAndOwner = response.Data;
                   //var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                   //addNewOption_All.ownerId = 0;
                   //addNewOption_All.propertyId = 0;
                   //addNewOption_All.propertyName = " All";
                   //FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                   //FC.TransectionFilterParameters.property = addNewOption_All;
                   callback(response.Data);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
               if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                   FC.personId = $rootScope.userData.personId;
               }
           });
    };

    IC.SetVendorInvoiceRef = function (model, index) {

        IC.lstInvoiceDetail[index].VendorInvRefNo = model.invoiceNumber;
    }

    IC.PrintPdf = function (divname) {
        IC.HTMLArr = [];
        var gridHtml = $("#" + divname).html();
        IC.HTMLArr.push(gridHtml);
        invoiceService.Printpdf(IC.HTMLArr, '', 'Invoices').
        success(function (data) {

            var pdfFile = new Blob([data], { type: 'application/pdf' });
            //var pdfFileUrl = URL.createObjectURL(pdfFile);
            //window.open(pdfFileUrl);
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(pdfFile, 'Invoices.pdf');
            } else {
                var objectUrl = URL.createObjectURL(pdfFile);
                window.open(objectUrl);
            }

        }).error(function (data) {
            $scope.showLoader = false;
            swal("ERROR", "", "error");
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    IC.BindInvoice = function (modal) {

        IC.TotalAmount = 0;
        IC.TotalPaid = 0;
        IC.AmountDue = 0;
        var invoice = angular.copy(modal.invoice);
        var invoiceDetail = angular.copy(modal.lstInvoiceDetail);
        IC.invoice.id = invoice.id;
        IC.invoice.invoiceNumber = invoice.number;
        IC.invoice.invoiceDate = invoice.date; // date to parse
        IC.invoice.dueDate = invoice.dueDate; // date to parse
        IC.invoice.creatorUserId = invoice.creatorUserId;
        IC.invoice.from = invoice.from;
        IC.invoice.client = invoice.to;
        IC.invoice.clientName = invoice.receiverName;
        IC.invoice.description = invoice.note;
        IC.invoice.property = invoice.propertyID;
        IC.invoice.statusId = invoice.statusId;
        IC.invoice_Text = IC.invoice.invoiceNumber;
        IC.invoice.fromAddress = invoice.fromAddress;
        IC.invoice.senderName = invoice.senderName;
        IC.invoice.senderPhoneNo = invoice.senderPhoneNo;
        IC.invoice.toAddress = invoice.toAddress;
        IC.invoice.ownerID = invoice.ownerID;
        IC.invoice.ownerName = invoice.ownerName;
        IC.invoice.ownerAddress = invoice.ownerAddress;
        IC.invoice.propertyName = invoice.propertyName;
        IC.invoice.propertyAddress = invoice.propertyAddress;
        var selectedProperty = IC.lstProperty.filter(function (Property) { return Property.id === IC.invoice.property });
        if (0 < selectedProperty.length) {
            IC.selectedProperty = selectedProperty[0];
            IC.invoice.property = IC.selectedProperty.id;

            IC.lstPropertyOwnerTenantByPropertyId = IC.lstPropertyOwnerTenant.filter(function (Property) { return Property.PropertyId == IC.invoice.property });
            var person = IC.lstPropertyOwnerTenantByPropertyId.filter(function (person) { return person.personId == invoice.to });

            if (0 < person.length) {
                IC.invoice.client = person[0];
            }
        }
        IC.lstInvoiceDetail = [];
        var counter = 0;
        angular.forEach(invoiceDetail, function (value, key) {
            var invoiceDetailMdl = angular.copy(IC.invoiceDetail);
            IC.lstInvoiceDetail.push(invoiceDetailMdl);
            var category = IC.lstServiceCategory.filter(function (category) { return category.id === value.categoryID });
            IC.lstInvoiceDetail[counter].Id = value.id;
            IC.lstInvoiceDetail[counter].categoryName = value.categoryName;
            IC.lstInvoiceDetail[counter].VendorInvRefNo = value.vendorInvRefNo;
            var WR = IC.lstWorkRequests.filter(function (wr) { return wr.id == value.wRNumber })[0];
            if (WR != undefined) {
                IC.lstInvoiceDetail[counter].WRNumber = WR;
            }
            else {
                IC.lstInvoiceDetail[counter].WRNumber = value.wRNumber;
            }
            IC.lstInvoiceDetail[counter].wr_Number = value.wRNumber;
            IC.lstInvoiceDetail[counter].woid = value.workorderId;
            IC.lstInvoiceDetail[counter].category = category[0];
            IC.lstInvoiceDetail[counter].amount = value.amount;
            IC.lstInvoiceDetail[counter].markup = value.markup;
            IC.lstInvoiceDetail[counter].markup_type = value.markup_type;
            IC.lstInvoiceDetail[counter].tax = value.tax;
            IC.lstInvoiceDetail[counter].totalAmount = value.totalAmount;
            IC.lstInvoiceDetail[counter].paidAmount = value.paidAmount;
            IC.lstInvoiceDetail[counter].description = value.description;
            IC.TotalAmount += parseFloat(value.totalAmount);
            IC.TotalPaid += parseFloat(value.paidAmount);
            if (counter == 0) {
                IC.lstInvoiceDetail[counter].showRemove = false;
            }
            else {
                IC.lstInvoiceDetail[counter].showRemove = true;
            }
            counter = counter + 1;
        });
    }

    IC.SaveInvoice = function (statusCode, frmInvoice) {

        $scope.showLoader = true;
        var MdlInvoice_Detail = {
            invoice: {},
            lstInvoiceDetail: []
        };
        var lstInvoiceDetail = [];
        IC.invoice.invoiceNumber = IC.invoice_Text;

        var invoiceId = 0;
        if (IC.invoice.id > 0) {
            invoiceId = IC.invoice.id;
        }

        var invoice = {
            id: invoiceId,
            number: IC.invoice.invoiceNumber,
            date: IC.invoice.invoiceDate,
            dueDate: IC.invoice.dueDate,
            propertyID: IC.invoice.property,
            creatorUserId: IC.managerid,
            from: $rootScope.userData.ManagerDetail.pId,
            to: IC.invoice.client.personId,
            recipientPersonTypeName: IC.invoice.client.PersonType,
            statusId: statusCode,
            note: IC.invoice.description,
            creatorUserId: IC.managerid,
            lastModifierUserId: IC.managerid,
            lastModificationTime: new Date()
        }
        MdlInvoice_Detail.invoice = invoice;
        var detailId = 0;
        for (var i = 0; i <= IC.lstInvoiceDetail.length - 1; i++) {
            detailId = 0;
            if (IC.lstInvoiceDetail[i].Id > 0) {
                detailId = IC.lstInvoiceDetail[i].Id;
            }
            var MdlInvoiceDetail = {
                id: detailId,
                categoryID: IC.lstInvoiceDetail[i].category.id,
                vendorInvRefNo: IC.lstInvoiceDetail[i].VendorInvRefNo,
                wRNumber: IC.lstInvoiceDetail[i].WRNumber.id,
                amount: IC.lstInvoiceDetail[i].amount,
                markup: IC.lstInvoiceDetail[i].markup,
                markup_type: IC.lstInvoiceDetail[i].markup_type,
                tax: IC.lstInvoiceDetail[i].tax,
                totalAmount: IC.lstInvoiceDetail[i].totalAmount,
                description: IC.lstInvoiceDetail[i].description,
                creatorUserId: IC.managerid
            }
            lstInvoiceDetail.push(MdlInvoiceDetail);
        }

        MdlInvoice_Detail.lstInvoiceDetail = lstInvoiceDetail;
        invoiceService.saveInvoice(MdlInvoice_Detail).success(function (response) {
            if (response.Status == true) {
                $scope.showLoader = false;
                if (statusCode == 1) {
                    swal("SUCCESS", "invoice saved as draft", "success");
                }
                else {
                    swal("SUCCESS", "invoice sent to client successfully", "success");
                }
                IC.Clear_SaveInvoice();
                IC.GetCurrentInvoiceNumberByManager();
               // IC.Cancle(frmInvoice);
                frmInvoice.$setPristine();
                frmInvoice.$setUntouched();
            }
            else {
                swal("ERROR", MessageService.responseError, "error");
            }
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    //*****************************************************
    // Invoice Payment
    //*****************************************************

    IC.toggle = function () {

        if (IC.ShowMdlInvPymt == false && IC.ShowWriteOff == true) {
            IC.ShowWriteOff = !IC.ShowWriteOff;
        }
        IC.ShowMdlInvPymt = !IC.ShowMdlInvPymt;
        IC.mdlPayment.writeOff = false;
    }

    IC.toggleWriteOff = function () {

        IC.mdlPayment.description = '';
        IC.mdlPayment.amount = IC.invoiceData.payableAmt;

        if (IC.ShowWriteOff == false && IC.ShowMdlInvPymt == true) {
            IC.ShowMdlInvPymt = !IC.ShowMdlInvPymt;
        }
        IC.ShowWriteOff = !IC.ShowWriteOff;
        IC.mdlPayment.writeOff = true;
    }


    IC.ChangePaybleAmt = function () {

        if (parseFloat(IC.mdlPayment.amount) > parseFloat(IC.invoiceData.payableAmt)) {
            IC.mdlPayment.amount = IC.invoiceData.payableAmt;
        }
    }

    IC.OpenConfirmation = function () {
        $("#writeOffConfirmation").modal('show');
    }

    IC.WriteOffConfirm = function () {
        $("#writeOffConfirmation").modal('hide');
        IC.SavePayment();
    }

    IC.InvoicePayment = function (invoiceId, invoiceNo, recipientName, propertyName, invoiceDate, propertyId, from, dueDate, fromEmail) {
        IC.invoiceData.invoiceNo = invoiceNo;
        IC.invoiceData.recipientName = recipientName;
        IC.invoiceData.propertyName = propertyName;
        IC.invoiceData.invoiceDate = invoiceDate;
        IC.mdlPayment.writeOff = false;
        IC.mdlPayment.paymentMode = '';
        IC.ShowWriteOff = false;
        IC.ShowMdlInvPymt = false;
        // IC.FillmdlPayment(invoiceId);
        invoiceService.getInvoicePaymentHistory(invoiceId).
           success(function (response) {
               if (response.Status == true) {

                   IC.PaymentDetail = angular.copy(response.Data);
                   IC.invoiceData.payableAmt = (IC.PaymentDetail.TotalAmount - IC.PaymentDetail.PaidAmount);
                   IC.mdlPayment.amount = IC.invoiceData.payableAmt;
                   IC.mdlPayment.invoiceId = invoiceId;
                   IC.mdlPayment.payMode = IC.PaymentDetail.lstPaymentMode[0];
                   var lstBills = IC.PaymentDetail.lstBills[0];
                   if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
                       $("#mdlInvoicePmt").modal('show');
                   }
                   else {
                       $rootScope.PaymentAgainstInvoice = true;
                       $rootScope.InvoicePaymentDetails.intId = invoiceId;
                       $rootScope.InvoicePaymentDetails.invNumber = invoiceNo === undefined ? lstBills.invoiceNumber : invoiceNo;
                       $rootScope.InvoicePaymentDetails.from = from == 0 ? lstBills.from : from;
                       $rootScope.InvoicePaymentDetails.fromEmail = fromEmail;
                       $rootScope.InvoicePaymentDetails.to = IC.mdlPayment.manageId;
                       $rootScope.InvoicePaymentDetails.propertyId = propertyId;
                       $rootScope.InvoicePaymentDetails.propertyName = propertyName;
                       $rootScope.InvoicePaymentDetails.invDate = IC.invoiceData.invoiceDate;
                       $rootScope.InvoicePaymentDetails.invDueDate = dueDate;
                       $rootScope.InvoicePaymentDetails.amount = IC.PaymentDetail.TotalAmount;
                       $rootScope.InvoicePaymentDetails.paid = IC.PaymentDetail.PaidAmount;
                       $rootScope.InvoicePaymentDetails.dueAmt = IC.invoiceData.payableAmt;
                       $window.localStorage.removeItem("PaymentAgainstInvoice");
                       $window.localStorage.setItem("PaymentAgainstInvoice", JSON.stringify($rootScope.PaymentAgainstInvoice));
                       $window.localStorage.removeItem("InvoicePaymentDetails");
                       $window.localStorage.setItem("InvoicePaymentDetails", JSON.stringify($rootScope.InvoicePaymentDetails));
                       $location.path("/payment");
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               // $scope.showLoader = false;
           });
    }


    IC.SavePayment = function () {

        IC.mdlPayment.paymentMode = IC.mdlPayment.payMode.name;
        $scope.showLoader = true;
        invoiceService.savePayment(IC.mdlPayment).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   $("#mdlInvoicePmt").modal('hide');
                   IC.mdlPayment.description = '';
                   swal("SUCCESS", 'Payment saved successfully', "success");
                   IC.FilterInvoices();
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    //*****************************************************
    // End Invoice Payment
    //*****************************************************

    //*****************************************************
    //Chart Methods
    //*****************************************************

    IC.CreateChartArray = function (modal) {
        IC.InvoiceStatusArray = [];
        IC.InvoiceAmountStatusArray = [];
        var Invoicecategory = "";
        IC.paidAmount = 0;
        IC.dueAmount = 0;
        IC.totalAmount = 0;
        IC.writeOffAmount = 0;

        IC.groupByStatus = $filter('groupBy')(modal, 'status');
        IC.savedInDraft = 0;
        IC.sendToClient = 0
        angular.forEach(IC.groupByStatus, function (item, key) {
            var statusname = key;
            var count = item.length;
            var arr = [];
            arr.push(statusname, count);
            IC.InvoiceStatusArray.push(arr);

            var InvoiceAmountBycatagory = 0;
            var arrInvoiceAmountStatus = [];
            if (key == "Saved as draft") {
                Invoicecategory = "Total Amount As Draft"
                angular.forEach(item, function (item1, key1) {

                    InvoiceAmountBycatagory += parseFloat(item1.totalInvAmount);

                });
                arrInvoiceAmountStatus.push(Invoicecategory, InvoiceAmountBycatagory);
                IC.InvoiceAmountStatusArray.push(arrInvoiceAmountStatus);
            }
            else if (key == "Sent to client" || key == "PartialPymt" || key == "Paid" || key == "PartialWriteOff") {

                angular.forEach(item, function (item1, key1) {

                    IC.paidAmount += item1.paidAmount;
                    IC.totalAmount += item1.totalInvAmount;
                    if (key == "PartialWriteOff") {
                        IC.writeOffAmount += (item1.totalInvAmount - item1.paidAmount)
                    }
                });
            }
            else if (key == "WriteOff") {

                angular.forEach(item, function (item1, key1) {
                    IC.writeOffAmount += parseFloat(item1.totalInvAmount);
                });
                arrInvoiceAmountStatus.push(Invoicecategory, InvoiceAmountBycatagory);
                IC.InvoiceAmountStatusArray.push(arrInvoiceAmountStatus);
            }
        });

        if (IC.totalAmount > 0) {
            var arr = [];
            var remain = parseFloat(IC.totalAmount - (parseFloat(IC.paidAmount) + parseFloat(IC.writeOffAmount)));
            arr.push("Total Due Amount", remain);
            IC.InvoiceAmountStatusArray.push(arr);
        }

        if (IC.paidAmount > 0) {
            var arrInvoiceAmountStatus = [];
            arrInvoiceAmountStatus.push("Total Paid Amount ", IC.paidAmount);
            IC.InvoiceAmountStatusArray.push(arrInvoiceAmountStatus);
        }
        if (IC.writeOffAmount > 0) {
            var arrInvoiceAmountStatus = [];
            arrInvoiceAmountStatus.push("Total WriteOff Amount ", IC.writeOffAmount);
            IC.InvoiceAmountStatusArray.push(arrInvoiceAmountStatus);
        }
        if (IC.InvoiceStatusArray.length > 0) {
            IC.PieChartLoad();
        }
        if (IC.InvoiceAmountStatusArray.length > 0) {
            IC.PieChartInvoiceAmountLoad();
        }
    }

    IC.PieChartLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(IC.InvoiceStatusArray);
            var options = {
                title: 'Invoice status by count',
                pieSliceText: 'value',
                sliceVisibilityThreshold: 0,
            };
            
            if ($rootScope.userData.personType == $rootScope.PersonTyp.property_manager && IC.lstInvoices_Filtered.length > 0 && IC.pageShowOnOwner_TenantContact == false)
            {
                var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
                chart2.draw(data, options);
            }           
        }
    }


    IC.PieChartInvoiceAmountLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.

        function drawChart() {

            var dataArray = IC.InvoiceAmountStatusArray;

            var total = IC.getTotal(dataArray);

            // Adding tooltip column  
            for (var i = 0; i < dataArray.length; i++) {
                dataArray[i].push(IC.customTooltip(dataArray[i][0], dataArray[i][1], total));
            }

            // Changing legend  
            for (var i = 0; i < dataArray.length; i++) {
                dataArray[i][0] = dataArray[i][0] + " " +
                         "  $" + dataArray[i][1] + ", " + ((dataArray[i][1] / total) * 100).toFixed(1) + "%";
            }
            // Column names
            dataArray.unshift(['Invoice Status', 'Invoice Amount', 'Tooltip']);
            var data = google.visualization.arrayToDataTable(dataArray);

            // Setting role tooltip
            data.setColumnProperty(2, 'role', 'tooltip');
            data.setColumnProperty(2, 'html', true);

            var options = {
                title: 'Amount by status',
                pieSliceText: 'value',
                tooltip: { isHtml: true }
            };

            if ($rootScope.userData.personType == $rootScope.PersonTyp.property_manager && IC.lstInvoices_Filtered.length > 0 && IC.pageShowOnOwner_TenantContact == false) {
                var chart = new google.visualization.PieChart(document.getElementById('piechart11'));
                chart.draw(data, options);
            }           
        }
    }


    IC.customTooltip = function (name, value, total) {
        return '<div style="padding:5%!important;">' + name + '<br/><b> $ ' +
            value + ' (' + ((value / total) * 100).toFixed(1) + '%)</b>'
        '</div>'

        // name + '<br/><b> $ ' + value + ' (' + ((value / total) * 100).toFixed(1) + '%)</b>';
    }

    IC.getTotal = function (dataArray) {
        var total = 0;
        for (var i = 0; i < dataArray.length; i++) {
            total += dataArray[i][1];
        }
        return total;
    }

    //*****************************************************
    //End Chart Methods
    //*****************************************************

    //*****************************************************
    //Invoice Methods
    //*****************************************************

    IC.EditAmount = function (index) {
        var markupType = IC.lstInvoiceDetail[index].markup_type;

        if (markupType == 'pct') {

            if (IC.lstInvoiceDetail[index].amount != undefined) {
                IC.CheckValue(index);
                IC.lstInvoiceDetail[index].totalAmount = ((parseFloat(IC.lstInvoiceDetail[index].amount) * (1 + (parseFloat(IC.lstInvoiceDetail[index].markup) / 100))) * (1 + (parseFloat(IC.lstInvoiceDetail[index].tax) / 100))).toFixed(2);
            }
        } else {
            if (IC.lstInvoiceDetail[index].amount != undefined) {
                IC.CheckValue(index);
                var total = parseFloat(IC.lstInvoiceDetail[index].amount) + parseFloat(IC.lstInvoiceDetail[index].markup)
                if (parseFloat(IC.lstInvoiceDetail[index].tax) < 1) {
                    IC.lstInvoiceDetail[index].totalAmount = total
                }
                else {
                    IC.lstInvoiceDetail[index].totalAmount = (parseFloat(IC.lstInvoiceDetail[index].amount) + ((((parseFloat(IC.lstInvoiceDetail[index].amount) + (parseFloat(IC.lstInvoiceDetail[index].markup))) * (parseFloat(IC.lstInvoiceDetail[index].tax))) / 100))).toFixed(2);
                }
            }
        }
    }

    IC.CheckValue = function (index) {
        if (IC.lstInvoiceDetail[index].markup == undefined || IC.lstInvoiceDetail[index].markup == '') {
            IC.lstInvoiceDetail[index].markup = 0;
        } else {
            // IC.lstInvoiceDetail[index].markup = parseFloat(IC.lstInvoiceDetail[index].markup);
            IC.lstInvoiceDetail[index].markup = IC.lstInvoiceDetail[index].markup;
        }
        if (IC.lstInvoiceDetail[index].tax == undefined || IC.lstInvoiceDetail[index].tax == '') {
            IC.lstInvoiceDetail[index].tax = 0;
        }
        else {
            //IC.lstInvoiceDetail[index].tax = parseFloat(IC.lstInvoiceDetail[index].tax);
            IC.lstInvoiceDetail[index].tax = IC.lstInvoiceDetail[index].tax;
        }
    }

    IC.DeleteInvoice = function (invoiceId, statusId, deleteCommand, currentStatus, status) {

        swal({
            title: "Are you sure?",
            text: "You want to " + status + " this invoice.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then(function(willDelete)  {
            if (willDelete) {
                $scope.showLoader = true;
                var array = {
                    invoiceId: invoiceId,
                    statusId: statusId,
                    deleteCommand: deleteCommand,
                    currentStatus: currentStatus
                };
                invoiceService.deleteInvoice(array).
                   success(function (response) {
                       if (response.Status == true) {
                           IC.FilterInvoices();
                           $scope.showLoader = false;
                           swal("SUCCESS", "Invoice " + deleteCommand + " successfully !", "success");
                       }
                       else {
                           swal("ERROR", response.Message, "error");
                       }
                   }).error(function (data, status) {
                       if (status == 401) {
                           commonService.SessionOut();
                       }
                       else {
                           swal("ERROR", MessageService.serverGiveError, "error");
                       }
                       $scope.showLoader = false;
                   }).finally(function (data) {
                       $scope.showLoader = false;
                   });
            } else {
                //swal("Your imaginary file is safe!");
            }
        });
    }

    IC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    IC.ChangeCustomToDate = function () {
        IC.toDate = IC.fromDate;
    }

    IC.FilterProperty = function (model) {

        IC.currentPage_lstInvoices = 0;
        //IC.personId = model.ownerId;
        IC.personId = model.personId;
        IC.propertyId = 0;
        IC.personType = model.personTypeId;

        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {

            if (model.ownerId == 0) {
                IC.personId = $rootScope.userData.personId;
                IC.propertyId = 0;
                IC.personType = $rootScope.userData.personTypeId;
            }
            var ownerPOID = model.ownerId;            
            IC.GetInvoicesByPersonAndProperty(IC.Selected_fromDate, IC.Selected_toDate, ownerPOID, IC.personId, IC.propertyId, IC.personType);
        }
        else {
            if (model.ownerId == 0) {
                IC.lstInvoices_Filtered = angular.copy(IC.lstInvoices);
                IC.InvoiceFilterParameters.property = IC.lstPropertByManagerAndOwner[0];
                IC.lstPropertByManagerAndOwner = IC.CopylstPropertByManagerAndOwner;
            }
            else {
                IC.lstInvoices_Filtered = IC.lstInvoices.filter(function (invoices) { return invoices.to === model.ownerId });
                if (IC.InvoiceFilterParameters.owner != null) {
                    IC.lstPropertByManagerAndOwner = IC.Filter(IC.CopylstPropertByManagerAndOwner, IC.InvoiceFilterParameters.owner.ownerId);
                    var addNewOption_All = angular.copy(IC.lstPropertByManagerAndOwner[0]);
                    addNewOption_All.ownerId = 0;
                    addNewOption_All.propertyId = 0;
                    addNewOption_All.propertyName = " All";
                    IC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                    IC.InvoiceFilterParameters.property = addNewOption_All;
                }
                else {
                    IC.lstPropertByManagerAndOwner = IC.CopylstPropertByManagerAndOwner;
                    IC.InvoiceFilterParameters.property = IC.lstPropertByManagerAndOwner[0];
                }
            }
        }
        IC.CreateChartArray(IC.lstInvoices_Filtered);
    }


    IC.FilterInvoiceByProperty = function (model) {

        IC.currentPage_lstInvoices = 0;
        //WHEN logged-in user is a PM
        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                var propertyId = model.propertyId;
                IC.LoadDataForOwner(propertyId);

            } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                var propertyId = model.propertyId;
                IC.LoadDataForTenant(propertyId);
            }
            else {
                IC.personId = IC.InvoiceFilterParameters.owner.personId;
                IC.propertyId = model.propertyId;
                IC.personType = IC.InvoiceFilterParameters.owner.personTypeId;
                var ownerPOID = IC.InvoiceFilterParameters.owner.ownerId;              
                IC.GetInvoicesByPersonAndProperty(IC.Selected_fromDate, IC.Selected_toDate, ownerPOID, IC.personId, IC.propertyId, IC.personType);
            }
        }
        else {   //WHEN logged-in user is Owner Or Tenant

            IC.propertyId = model.propertyId;
            IC.GetInvoicesByPersonAndProperty(IC.Selected_fromDate, IC.Selected_toDate, IC.POID, IC.personId, IC.propertyId, IC.personType);
        }        
    }

    IC.Filter = function (items, ownerId) {
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.ownerId == ownerId) {
                retArray.push(obj);
            }
        });
        return retArray;
    }

    IC.GetPropertOwnersByManager = function () {
        $scope.showLoader = true;
        invoiceService.getPropertOwnersByManager(IC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       IC.CreateDataForPersonDropdown(response.Data, function (data) {

                       });
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    IC.CreateDataForPersonDropdown = function (model, callback) {
        var companyTypeOwner = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 1 });
        var individualOwner = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 0 });
        var Tenants = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.tenant });
        // var list = [];

        var companyTypeOwnerData = $filter('groupBy')(companyTypeOwner, 'ownerId');


        angular.forEach(companyTypeOwnerData, function (value, key) {
            IC.lstPropertOwnersByManager.push(value[0])
        });

        var TenantsData = $filter('groupBy')(Tenants, 'personId');


        angular.forEach(TenantsData, function (value, key) {
            IC.lstPropertOwnersByManager.push(value[0])
        });

        var individualOwnerData = $filter('groupBy')(individualOwner, 'personId');

        angular.forEach(individualOwnerData, function (value, key) {
            IC.lstPropertOwnersByManager.push(value[0]);
        });
        //IC.lstPropertOwnersByManager = angular.copy(data);

        var addAll = angular.copy(IC.lstPropertOwnersByManager[0]);
        addAll.ownerFullName = "All";
        addAll.ownerId = 0;
        addAll.personId = 0;
        addAll.personTypeId = 0;
        addAll.propertyId = 0;
        addAll.propertyName = '';
        IC.lstPropertOwnersByManager.unshift(addAll);
        IC.InvoiceFilterParameters.owner = addAll;
        IC.CopylstPropertOwnersByManager = angular.copy(IC.lstPropertOwnersByManager);


    }

    IC.GetPropertByManagerAndOwner = function () {
        $scope.showLoader = true;

        invoiceService.getPropertByManagerAndOwner(IC.managerid, IC.isManager, IC.pmid, IC.personId, IC.propertyId, IC.personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {
                           IC.lstPropertByManagerAndOwner.push(value[0])
                       });

                       var addNewOption_All = angular.copy(IC.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerFullName = " All";
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       IC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       IC.InvoiceFilterParameters.property = addNewOption_All;
                       IC.CopylstPropertByManagerAndOwner = IC.lstPropertByManagerAndOwner;

                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    IC.FilterInvoices = function (checkValue) {

        if (IC.selectCurrentPeriod != null) {
            if (IC.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {
                IC.lstInvoices = [];
                return;
            }
            IC.Selected_fromDate = new Date();
            IC.Selected_toDate = new Date();

            if (IC.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {
                IC.Selected_toDate = IC.toDate;
                IC.Selected_fromDate = IC.fromDate;
            }
            else {
                if (IC.selectCurrentPeriod.Period == 'Annual') {
                    IC.Selected_fromDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {
                    IC.Selected_fromDate = IC.selectCurrentPeriod.Startdate === undefined ? IC.fromDate : IC.selectCurrentPeriod.Startdate;
                    IC.Selected_toDate = IC.selectCurrentPeriod.EndDate===undefined?IC.toDate:IC.selectCurrentPeriod.EndDate;
                }
            }
            IC.InvoiceFilterParameters.owner = undefined;
            IC.InvoiceFilterParameters.property = undefined;

            IC.pmid = $rootScope.userData.ManagerDetail.pId;
            IC.personId = $rootScope.userData.personId;
            IC.personType = $rootScope.userData.personTypeId;

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

                if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                    var propertyId = 0;
                    IC.LoadDataForOwner(propertyId);

                } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                    var propertyId = 0;
                    if ($rootScope.selectedPropertyId != 0 && $rootScope.selectedPropertyId != undefined)
                    {
                        propertyId = $rootScope.selectedPropertyId;
                    }
                    IC.LoadDataForTenant(propertyId);
                }
                else {
                    IC.GetAllInvoicesByManager(IC.Selected_fromDate, IC.Selected_toDate, IC.managerid, IC.personId, IC.propertyId, IC.personType);
                }
            }
            else {
                IC.LoadDataForOwnerTenant();
            }

            //if ($location.url().match("tenants/")) {
            //    IC.GetAllInvoicesByManager(IC.Selected_fromDate, IC.Selected_toDate, IC.managerid, IC.personId, $rootScope.selectedPropertyId, IC.personType);
            //}
            //else {
            //    IC.GetAllInvoicesByManager(IC.Selected_fromDate, IC.Selected_toDate, IC.managerid, IC.personId, IC.propertyId, IC.personType);
            //}
        }
        else {

            IC.lstInvoices = [];
            IC.lstInvoices_Filtered = [];
            IC.InvoiceFilterParameters.owner = undefined;
            IC.InvoiceFilterParameters.property = undefined;
        }
        $scope.showLoader = false;
    }

    $rootScope.$on("FilterInvoiceOnLeaseChange", function (event, MdlParameters) {

        //var propertyId = 0;
        //if ($rootScope.selectedPropertyId != 0 && $rootScope.selectedPropertyId != undefined) {
        //    propertyId = $rootScope.selectedPropertyId;
        //}       
        IC.LoadDataForTenant(MdlParameters.propertyId);
       
    });


    IC.GetAllInvoicesByManager = function (StartDate, EndDate, ManagerId, PersonId, PropertyId, PersonType) {

        var ownerPOID = 0;
        if ($rootScope.userData.personType == "owner") {
            ManagerId = $rootScope.userData.personManageriD;
            PersonId = $rootScope.userData.personId;
            ownerPOID = $rootScope.userData.poid;
        }
        if ($rootScope.userData.personType == "tenant") {
            ManagerId = $rootScope.userData.personManageriD;
        }

        $scope.showLoader = true;
        invoiceService.getAllInvoicesByManager(StartDate, EndDate, ManagerId, PersonId, PropertyId, PersonType, ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   IC.lstInvoices = response.Data;
                   IC.lstInvoices_Filtered = angular.copy(IC.lstInvoices);
                   if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                       IC.CreateChartArray(IC.lstInvoices);
                   }
                   if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
                       IC.lstInvoices_Filtered = IC.lstInvoices.filter(function (invoices) { return invoices.propertyID === parseInt($rootScope.selectedPropertyId) });
                   }
                   IC.InvoiceFilterParameters.property = IC.CopylstPropertByManagerAndOwner[0];
                   if (IC.pageShowOnOwner_TenantContact == false) {
                       IC.InvoiceFilterParameters.owner = angular.copy(IC.CopylstPropertOwnersByManager[0]);
                   }
                   else {
                       IC.InvoiceFilterParameters.owner = IC.lstPropertOwnersByManager.filter(function (t) { return t.ownerId == IC.owner_TenantIdForOwner_TenantContact && t.personTypeId == IC.personTypeForOwner_TenantContact })[0];

                       var delayInMilliseconds = 2000; //2 second
                       setTimeout(function () {
                           IC.FilterProperty(IC.InvoiceFilterParameters.owner);
                       }, delayInMilliseconds);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    IC.GetInvoiceData = function (invoiceId, callback) {
        invoiceService.getInvoiceWithDetail(invoiceId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data != null) {
                      // console.log('response.Data', response.Data);
                       callback(response.Data);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.ViewInvoice = function (invoiceId) {
        IC.GetInvoiceData(invoiceId, function (data) {
            IC.BindInvoice(data);
            //alert($rootScope.userData.personType)
            //alert($rootScope.PersonTyp.property_manager)
            //alert(IC.pageShowOnOwner_TenantContact)
            //alert($rootScope.ActiveTabName)
            $("#mdl_ViewInvoice").modal('show');
            //var mdl = $("#mdl_ViewInvoice");
            //console.log('mdl', mdl);
            //IC.GetWorkRequestsByProperty(data.invoice.propertyID, function (data2) {
            //    IC.lstWorkRequests = angular.copy(data2);
            //    IC.BindInvoice(data);
            //    $("#mdl_ViewInvoice").modal('show');
            //});
        });
    }
    IC.CloseModel = function () {
        $("#mdl_ViewInvoice").modal('hide');
    }

    IC.EditInvoice = function (invoiceId, status) {
        IC.isSentToClient = false;
        IC.createEdit = true;
        IC.GetInvoiceData(invoiceId, function (data) {
            IC.GetWorkRequestsByProperty(data.invoice.propertyID, function (data2) {
                IC.lstWorkRequests = angular.copy(data2);
                IC.IsEdit = true;
                IC.BindInvoice(data);
                if (status == 'Sent to client') {
                    IC.isSentToClient = true;
                }
            });
        });
    }

    if (!angular.isUndefined($routeParams.invoiceId) && $routeParams.invoiceId != -1) {

        // IC.EditInvoice($routeParams.invoiceId);
    }

    //*****************************************************
    // End Invoice Methods
    //*****************************************************      


    IC.Cancle = function (frmInvoice) {
        if ($location.url().match("createInvoice/") && !angular.isUndefined($routeParams.LeaseNum)) {
            if (!angular.isUndefined($routeParams.PropertyId) && !angular.isUndefined($routeParams.TenantId)) {
                window.location.href = "#leaseview/" + $routeParams.LeaseNum + "/" + $routeParams.PropertyId;
            }
        }
        else {
            IC.createEdit = false;
            IC.isSentToClient = false;
            IC.Clear_SaveInvoice();
            frmInvoice.$setPristine();
            $window.location.reload();
        }
    }

    IC.LoadFunction = function () {
        // used in Create Invoice
        var invoiceDetailMdl = angular.copy(IC.invoiceDetail);
        invoiceDetailMdl.showRemove = false;
        IC.lstInvoiceDetail.push(invoiceDetailMdl);
        IC.GetPropertiesByType(function (data) { });
        IC.GetPropertyTenantOwner(function (data) { });
        IC.GetServiceCategory($rootScope.userData.personTypeId);
    };

    IC.CreateInvoice = function () {
        IC.createEdit = true;
        IC.IsEdit = false;
        IC.Clear_SaveInvoice();
        IC.GetCurrentInvoiceNumberByManager();
    }

    IC.Clear_SaveInvoice = function () {

        IC.selectedProperty = "";
        IC.invoice = {
            invoiceDate: new Date(),
            dueDate: new Date(),
            invoiceNumber: '',
            description: '',
            property: "",
            client: "",
            creatorUserId: IC.managerid,
            propertyModel: {},
        }
        IC.lstInvoiceDetail = [];
        var invoiceDetailMdl = angular.copy(IC.invoiceDetail);
        invoiceDetailMdl.showRemove = false;
        IC.lstInvoiceDetail.push(invoiceDetailMdl);
    }

    IC.GetCurrentInvoiceNumberByManager = function () {
        //IC.managerid
        invoiceService.getCurrentInvoiceNumberByManager(IC.managerid).
            success(function (response) {
                if (response.Status == true) {
                    if (response.Data[0] != null && response.Data[0] != '') {
                        IC.invoice_Text = response.Data[0] + '-' + response.Data[1];
                    }
                    else {
                        IC.invoice_Text = 'Inv-00001';
                    }
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    IC.StopCharInText = function (e) {

        if (IC.invoice_Text == undefined || IC.invoice_Text.length < 3) {
            if ((e.keyCode < 65 || e.keyCode > 90) && (e.keyCode < 97 || e.keyCode > 123) && e.keyCode != 8 && e.keyCode != 46) {
                return e.preventDefault();
            }
        }
        else if (IC.invoice_Text.length == 3 && e.keyCode != 8 && e.keyCode != 46) {

            IC.invoice_Text = IC.invoice_Text.toString() + '-';
            return e.preventDefault();
        }

        else if (IC.invoice_Text.length > 3 && ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39))) {
            // let it happen, don't do anything

            return;
        }
        else if (IC.invoice_Text.length > 3 && (e.keyCode < 48 || e.keyCode > 57)) {

            return e.preventDefault();
        }
    }

    IC.CheckCategory = function () {
        var counter = 0;
        angular.forEach(IC.lstServiceCategory, function (value, key) {
            if (IC.MdlServiceCategory.name.trim() == value.name) {
                counter += 1;
            }
        });

        if (counter == 0) {
            IC.EnableSave = true;
        } else {
            IC.EnableSave = false;
            swal("ERROR", 'Category already exists.', "error");
        }
    }

    IC.SaveServiceCategory = function () {
        $scope.showLoader = true;
        serviceCategory_service.saveServiceCategory(IC.MdlServiceCategory).
            success(function (data) {
                $scope.showLoader = false;
                IC.lstServiceCategory.push(data);
                swal("SUCCESS", 'Category has been saved.', "success");
                // IC.GetServiceCategory();              
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
        // }       
    }

    IC.GetPropertiesByType = function (callback) {
        $scope.showLoader = true;
        invoiceService.getPropertiesByType(IC.managerid).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    IC.lstProperty = response.Data;
                    callback(IC.lstProperty);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    IC.GetPropertyTenantOwner = function (callback) {

        $scope.showLoader = true;
        var isManager = true;
        invoiceService.getPropertyTenantOwner(IC.managerid, isManager, 0).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    IC.lstPropertyOwnerTenant = response.Data;
                    callback(IC.lstPropertyOwnerTenant);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };
    IC.selectedProperty = "";
    IC.GetPropertyTenantOwnerByPropId = function (selected) {

        IC.GetPropertyTenantOwnerBy_PropId(selected, function (data) { });

        //if (selected.id != undefined) {
        //    IC.invoice.property = selected.id;
        //    IC.lstPropertyOwnerTenantByPropertyId = IC.lstPropertyOwnerTenant.filter(Property => Property.PropertyId === IC.invoice.property);  //&& Property.personId != $rootScope.userData.personId
        //    IC.GetWorkRequestsByProperty(IC.invoice.property, function (data) {
        //        IC.lstWorkRequests = angular.copy(data);
        //    });
        //}
        //else {
        //    IC.validationProperty = selected.id;
        //    IC.lstPropertyOwnerTenantByPropertyId = 0;
        //}
    };

    IC.GetPropertyTenantOwnerBy_PropId = function (selectedProperty, callback) {
        if (selectedProperty.id != undefined) {
            IC.invoice.property = selectedProperty.id;
            IC.lstPropertyOwnerTenantByPropertyId = IC.lstPropertyOwnerTenant.filter(function (Property) { return Property.PropertyId === IC.invoice.property });  //&& Property.personId != $rootScope.userData.personId
            IC.GetWorkRequestsByProperty(IC.invoice.property, function (data) {
                IC.lstWorkRequests = angular.copy(data);
                callback(IC.lstPropertyOwnerTenantByPropertyId);
            });
        }
        else {
            IC.validationProperty = selected.id;
            IC.lstPropertyOwnerTenantByPropertyId = 0;
        }
    };

    IC.ChangeInvoiceDate = function () {
        IC.invoice.dueDate = IC.invoice.invoiceDate;
    }

    IC.AddRow = function () {
        var invoiceDetailMdl = angular.copy(IC.invoiceDetail);
        IC.lstInvoiceDetail.push(invoiceDetailMdl);
    }

    IC.removeInvoiceDetail = function (index, detailId) {
        IC.lstInvoiceDetail.splice(index, 1);
    }

    IC.ClientChange = function (model) {
        var length = IC.lstInvoiceDetail.length;
        if (length > 0) {
            IC.lstInvoiceDetail.splice(1);
        }
        IC.lstInvoiceDetail[0].category = undefined;

        if (model != undefined) {
            if (model.PersonType.toUpperCase() == PersonTypeEnum.owner.toUpperCase()) {
                IC.GetServiceCategory(PersonTypeIdEnum.owner)
            }
            else {
                IC.GetServiceCategory(PersonTypeIdEnum.tenant)
            }
        }
        else {
            IC.GetServiceCategory($rootScope.userData.personTypeId)
        }
    };

    IC.GetServiceCategory = function (personTypeId) {
        $scope.showLoader = true;
        serviceCategory_service.getServiceCategory(personTypeId).
            success(function (data) {
                $scope.showLoader = false;
                IC.lstServiceCategory = data.Data;

            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });

    };

    IC.AddCategory = function (mdl) {
        if (mdl == null) {
            return;
        }
        if (mdl.Id == 0) {
            $("#otherCategory").modal();
        }
    }

    IC.CreateInvoiceNumber = function () {
        IC.invoice.invoiceNumber = IC.invoice_Text;

        if (IC.invoice_Text != '' && IC.invoice_Text != null || IC.invoice_Text != undefined) {
            $scope.showLoader = true;
            invoiceService.validInvoiceNumber(IC.invoice.invoiceNumber, IC.managerid).
                success(function (data) {
                    $scope.showLoader = false;
                    IC.newinvoiceNumber = !data;
                }).error(function (data, status) {
                    if (status == 401) {
                        commonService.SessionOut();
                    }
                    else {
                        swal("ERROR", MessageService.serverGiveError, "error");
                    }
                    $scope.showLoader = false;

                }).finally(function (data) {
                    $scope.showLoader = false;
                });
        }
    }

    IC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    IC.ButtonClick_Excel = function (id, excelName) {
        $(id).table2excel({
            filename: excelName
        });
    }

    IC.ButtonClick_Pdf = function (id, pdfName, headerName) {
        HtmlTableToPdf(id, pdfName, headerName, '', '');
    }

    IC.GetInvoicesByPersonAndProperty = function (fromDate, toDate, ownerPOID, personId, propertyId, personType) {
        $scope.showLoader = true;
        invoiceService.getInvoicesByPersonAndProperty(IC.managerid, fromDate, toDate, ownerPOID, personId, propertyId, personType).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    //if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                    //    IC.lstInvoices_Filtered = response.Data.lstMdlGetAllInvoice;
                    //}
                    //else {
                    //    IC.lstInvoices_Filtered = response.Data.lstMdlGetAllInvoice.filter(i => i.isAutoInv == 0);
                    //}   
                    IC.lstInvoices_Filtered = response.Data.lstMdlGetAllInvoice;
                    IC.lstInvoices = angular.copy(IC.lstInvoices_Filtered);
                    if (propertyId == 0) {

                        if (response.Data.MdlPropertOwnersWithManager.length > 0) {
                            IC.lstPropertByManagerAndOwner = response.Data.MdlPropertOwnersWithManager;
                            var addNewOption_All = angular.copy(IC.lstPropertByManagerAndOwner[0]);

                            if (addNewOption_All != undefined) {
                                addNewOption_All.ownerId = 0;
                                addNewOption_All.propertyId = 0;
                                addNewOption_All.personType = 0;
                                addNewOption_All.propertyName = " All";
                                IC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                                IC.InvoiceFilterParameters.property = addNewOption_All;
                            }
                        }
                        else {
                            IC.lstPropertByManagerAndOwner = IC.CopylstPropertByManagerAndOwner;
                        }
                    }
                    IC.CreateChartArray(IC.lstInvoices_Filtered);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    IC.GetWorkRequestsByProperty = function (PropertyId, callback) {
        $scope.showLoader = true;
        IC.lstWorkRequests = [];
        workorderService.getWorkRequestsByProperty(PropertyId).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    $rootScope.$on("ViewInvoiceForFinance", function (event, MdlParameters) {
        var invoiceId = MdlParameters.invoiceId;
        IC.ViewInvoice(invoiceId);
    });

    IC.FilterInvoiceByEntryType = function () {

        IC.lstInvoices_Filtered = [];
        if (IC.invoiceFilterCategory == 'Auto') {
            IC.lstInvoices_Filtered = IC.lstInvoices.filter(function (invoices) { return invoices.isAutoInv == true });
        }
        else if (IC.invoiceFilterCategory == 'Manual') {
            IC.lstInvoices_Filtered = IC.lstInvoices.filter(function (invoices) { return invoices.isAutoInv == false });
        }
        else {
            IC.lstInvoices_Filtered = angular.copy(IC.lstInvoices);
        }
        IC.currentPage_lstInvoices = 0;

        IC.CreateChartArray(IC.lstInvoices_Filtered);
    }

    IC.InvoicePaymentByInvoiceId = function (InvoiceId) {
        IC.GetInvoiceData(InvoiceId, function (data) {
            IC.BindInvoice(data);
            IC.InvoicePayment(IC.invoice.id, IC.invoice.invoiceNumber, IC.invoice.clientName, IC.invoice.propertyName, IC.invoice.invoiceDate, IC.invoice.property, IC.invoice.from, IC.invoice.dueDate, IC.invoice.recipientEmail);
        });
    }
}
