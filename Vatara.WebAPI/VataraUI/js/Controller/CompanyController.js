﻿vataraApp.controller("CompanyController", CompanyController);
CompanyController.$inject = ['$scope', 'CompanyService', 'pagingService', '$filter', '$location', '$rootScope', 'commonService', 'propayRegService', '$sce'];
function CompanyController($scope, CompanyService, pagingService, $filter, $location, $rootScope, commonService, propayRegService, $sce) {

    var CC = this;

    $scope.Country = [];
    $scope.State = [];
    $scope.County = [];
    $scope.City = [];
    CC.EditCompanyInfo = false;
    CC.EditCompanyView = false;
    $scope.vataradomain = '.vatara.com';
    $scope.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    }
    $scope.CopyMdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    }

    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = true;

    $scope.isURLEdit = false;
    $scope.CopymdlCompany = {};

    $scope.mdlCompany = {

        pmid: 0,
        // propId:0,
        companyName: null,
        addressId: 0,
        email: null,
        phone: null,
        fax: null,
        url: null,
        logo: null,
        aboutUs: '',
        contactUs: '',
        sliderImage1: null,
        sliderImage2: null,
        sliderImage3: null,
        //isActive:1,
        countryId: 0,
        stateId: 0,
        countyId: 0,
        cityId: 0,
        profileImage: null,
        zipCode: null,
        line2: '',
        line1: '',
        useCompanyInfoForDisplay: false
    }
    $scope.logo = ''
    $scope.sliderImage1 = '',
    $scope.sliderImage2 = '',
    $scope.sliderImage3 = '',
    $scope.Photos = [{}],
    $scope.pId = 0;
    $scope.CompanyPhoto = [];
    $scope.CompanySliderImages = [];

    $scope.LoadFunction = function () {
        $scope.GetCountry(function (data) {
            $scope.GetStates(function (data) {
                $scope.GetCompanyData($rootScope.userData.userName, $rootScope.userData.personId);
            });
        });
    }

    $scope.validateCompanyURL = function () {
        $scope.showLoader = true;
        if ($scope.mdlPerson.url != undefined && $scope.mdlPerson.url != null && $scope.mdlPerson.url != '') {
            CompanyService.validateCompanyURL($scope.mdlPerson.url, $rootScope.userData.ManagerDetail.pId).
               success(function (response) {
                   $scope.showLoader = false;
                   if (response.Status == true) {
                       $scope.isCompanyURLExist = response.Data;
                   }
                   else {
                       swal("ERROR", response.Message, "error");
                   }
               }).error(function (data) {
                   $scope.showLoader = false;
                   swal("ERROR", MessageService.responseError, "error");
               }).finally(function (data) {
                   $scope.showLoader = false;
               });
        }
    }

    $scope.SaveData = function (frmCompanyProfile) {

        $scope.mdlCompany.countryId = $scope.mdlCountryState.selectedCountry.Id;
        $scope.mdlCompany.stateId = $scope.mdlCountryState.selectedState.Id;
        $scope.mdlCompany.cityId = $scope.mdlCountryState.selectedCity.Id;

        if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != '') {
            $scope.mdlCompany.countyId = $scope.mdlCountryState.selectedCounty.id;
        } else {
            $scope.mdlCompany.countyId = 0;
        }
        $scope.mdlCompany.logo = JSON.stringify($scope.logo);
        $scope.mdlCompany.sliderImage1 = JSON.stringify($scope.sliderImage1);
        $scope.mdlCompany.sliderImage2 = JSON.stringify($scope.sliderImage2);
        $scope.mdlCompany.sliderImage3 = JSON.stringify($scope.sliderImage3);

        $scope.showLoader = true;
        CompanyService.saveCompany($scope.mdlCompany).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.pId = $scope.mdlCompany.pmid
                   if ($scope.CompanyPhoto.length > 0)
                   {
                       $scope.SaveCompanyPhoto();
                   }                   
                   if ($scope.CompanySliderImages.length > 0)
                   {
                       $scope.SaveCompanySliderImage();
                   }
                   $scope.showMessage = true;
                   frmCompanyProfile.$setPristine();
                   swal("SUCCESS", "Company info saved successfully", "success");
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.GetCompanyData = function (Email, personId) {

        $scope.showLoader = true;
        CompanyService.getCompanyData(Email, personId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       CC.GetComapnyDataModal(response.Data);
                       CC.EditCompanyInfo = true;
                       CC.EditCompanyView = true;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.GetCompanyPhoto = function () {
        CompanyService.getCompanyphoto($scope.pId, function (result, data) {
            if (result) {

                for (let index = 0; index < data.length; index++) {
                    var fd = new FormData();
                    fd.append("file", $scope.CompanyPhoto);
                    fd.append("companyId", $scope.pId);
                    fd.append("seqNo", index);
                    fd.append("filename", 'image' + index);
                    fd.append("filedesc", '');
                    fd.append("filetags", 'Photo, ' + $scope.pId);
                    $scope.CompanyPhoto[index] = fd;
                    output = document.getElementById('divimage');
                    if (output != null) {
                        var random = Math.floor(100000 + Math.random() * 900000);
                        output.style.backgroundImage = 'url(../Uploads/Manager/Company/Photos/' + $scope.pId + '-image' + index.toString() + '.png?cb=' + random + ')';
                    }
                }
            }
            else {
                swal("ERROR", "Unable to fetch photo.", "error");
            }
        });
    };

    $scope.GetCompanySliderImage = function () {
        CompanyService.getCompanysliderimage($scope.pId, function (result, data) {
            if (result) {
                for (let index = 0; index < data.length; index++) {
                    var fd = new FormData();
                    fd.append("file", $scope.CompanyPhoto);
                    fd.append("companyId", $scope.pId);
                    fd.append("seqNo", index);
                    fd.append("filename", 'image' + index);
                    fd.append("filedesc", '');
                    fd.append("filetags", 'Photo, ' + $scope.pId);
                    $scope.CompanyPhoto[index] = fd;
                    output = document.getElementById('divimage' + (index + 1).toString());                   
                    if (output != null) {
                        var random = Math.floor(100000 + Math.random() * 900000);

                        output.style.backgroundImage = 'url(../Uploads/Manager/Company/SliderImages/' + $scope.pId + '-image' + (index + 1).toString() + '.png?cb=' + random + ')';
                        var slider = $('#myCarousel .carousel-inner #sliderimage' + (index + 1).toString()).find("img");                       
                        slider.attr('src', '../Uploads/Manager/Company/SliderImages/' + $scope.pId + '-image' + (index + 1).toString() + '.png?cb='+random);
                    }
                }
            }
            else {
                swal("ERROR", "Unable to fetch photo.", "error");
            }
        });
    };

    CC.GetComapnyDataModal = function (mdl) {
      
        $scope.pId = mdl.pmid;
        $scope.mdlCompany.pmid = mdl.pmid;
        // $scope.mdlCompany.propId = mdl.propId;
        $scope.mdlCompany.companyName = mdl.companyName;
        $scope.mdlCompany.addressId = mdl.addressId;
        $scope.mdlCompany.email = mdl.email;
        $scope.mdlCompany.phone = mdl.phone;
        $scope.mdlCompany.fax = mdl.fax;
        $scope.mdlCompany.url = mdl.url;
        $scope.mdlCompany.aboutUs = mdl.aboutUs;
        $scope.mdlCompany.contactUs = mdl.contactUs;

        // var country = $scope.Country.filter(country => country.Id === mdl.countryId);
        var country = ($scope.Country.filter(function (c) { return c.Id === mdl.countryId }));
        if (country.length > 0) {
            $scope.mdlCountryState.selectedCountry = country[0];
        }
        var state = $scope.State.filter(function (state) { return state.Id === mdl.stateId });
        if (state.length > 0) {
            $scope.mdlCountryState.selectedState = state[0];
        }
        $scope.mdlCompany.countyId = mdl.countyId;
        $scope.mdlCompany.cityId = mdl.cityId;
        $scope.GetCountyByState(mdl.stateId, function (data) {
            $scope.GetCityByStateOrCounty(mdl.stateId, mdl.countyId, function (data) { });
        });
        $scope.mdlCompany.profileImage = null;
        $scope.mdlCompany.zipCode = mdl.zipCode;
        $scope.mdlCompany.line2 = mdl.line2;
        $scope.mdlCompany.line1 = mdl.line1;
        $scope.mdlCompany.useCompanyInfoForDisplay = mdl.useCompanyInfoForDisplay;
        $scope.CopymdlCompany = angular.copy($scope.mdlCompany);
        $scope.GetCompanyPhoto();
        $scope.GetCompanySliderImage();
    }

    $scope.fileSelected = function (element) {
        var myFileSelected = element.files[0];

        $scope.CompanyPhoto[0] = myFileSelected;
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('divimage');
            if (output != null)
                output.style.backgroundImage = 'url(' + reader.result + ')';
            $scope.$apply();
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    $scope.SaveCompanyPhoto = function () {
        var fd = new FormData();
        fd.append("file", $scope.CompanyPhoto[0]);
        fd.append("companyId", $scope.pId);
        fd.append("moduleid", 'MANAGERCOMP');
        fd.append("seqNo", 0);
        fd.append("filename", 'image0');
        fd.append("filedesc", '');
        fd.append("filetags", 'Photo, ' + $scope.pId);
        CompanyService.saveFiles(fd, '/api/companyphoto', function (status, resp) {
            if (status) {
                swal("SUCCESS", "Photo saved successfully", "success");
            }
            else {
                swal("ERROR", "Unable to save photos.", "error");
            }
        });
    }

    //$scope.SliderImageSelected = function (element) {      
    //    var myFileSelected = element.files[0];
    //    $scope.CompanySliderImages = myFileSelected;
    //    var reader = new FileReader();
    //    reader.onload = function () {
    //        var output = document.getElementById('div' + element.name);            
    //        if (output != null)
    //            output.style.backgroundImage = 'url(' + reader.result + ')';
    //        $scope.$apply();
    //        console.log('reader.result', reader.result);
    //    }
    //    reader.readAsDataURL(event.target.files[0]);
    //    $scope.$apply();
    //};

    $scope.SliderImageSelected = function (element) {

        var myFileSelected = element.files[0];
        switch (element.name) {
            case 'image1':
                var fname = ("1_" + myFileSelected.name);
                $scope.CompanySliderImages[0] = myFileSelected;
                $scope.CompanySliderImages[0].seqNo = 1;
                $scope.CompanySliderImages[0].name = fname;
                $scope.$apply();
                break;
            case 'image2':
                var fname = ("2_" + myFileSelected.name);
                $scope.CompanySliderImages[1] = myFileSelected;
                $scope.CompanySliderImages[1].seqNo = 2;
                $scope.CompanySliderImages[1].name = fname;
                $scope.$apply();
                break;
            case 'image3':
                var fname = ("3_" + myFileSelected.name);
                $scope.CompanySliderImages[2] = myFileSelected;
                $scope.CompanySliderImages[2].seqNo = 3;
                $scope.CompanySliderImages[2].name = fname;
                $scope.$apply();
                break;
        }
        var reader = new FileReader();
        reader.onload = function (e) {
            var output = document.getElementById('div' + element.name);           
            if (output != null)
            {
                output.style.backgroundImage = 'url(' + reader.result + ')';
                var slider = $('#myCarousel .carousel-inner #slider' + element.name).find("img");
                slider.attr('src', e.target.result);
            }               
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    $scope.SaveCompanySliderImage = function () {
        var fd = new FormData();
        //fd.append("file", $scope.CompanySliderImages);
        //fd.append("companyId", $scope.pId);
        //fd.append("moduleid", 'MANAGERCOMP');
        //fd.append("seqNo", 0);
        //fd.append("filename", 'image0');
        //fd.append("filedesc", '');
        //fd.append("filetags", 'Photo, ' + $scope.pId);

        for (let index = 1; index <= $scope.CompanySliderImages.length; index++) {
            if ($scope.CompanySliderImages[index - 1] != undefined) {
                if ($scope.CompanySliderImages[index - 1].seqNo != undefined) {
                  
                    var seq = $scope.CompanySliderImages[index - 1].seqNo;
                    fd.append("file", $scope.CompanySliderImages[index - 1]);
                    fd.append("companyId", $scope.pId);
                    fd.append("moduleid", 'COMPSLIDERIMAGE');
                    fd.append("seqNo", seq);
                    fd.append("imageindex", index);
                    fd.append("filename", 'image' + index);
                    fd.append("filedesc", '');
                    fd.append("filetags", 'Photo, ' + $scope.pId);
                }
            }
        }
        CompanyService.saveFiles(fd, '/api/companysliderimage', function (status, resp) {
            if (status) {
                //swal("SUCCESS", "Photo saved successfully", "success");
            }
            else {
                swal("ERROR", "Unable to save photos.", "error");
            }
        });
    }

    $scope.GetCountry = function (callback) {

        $scope.showLoader = true;
        propayRegService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.Country = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }


          }).error(function (data, status) {
              swal("ERROR", MessageService.serverGiveError, "error");
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetStates = function (callback) {
        $scope.showLoader = true;
        propayRegService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.State = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              swal("ERROR", MessageService.serverGiveError, "error");
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCountyByStateId = function (modal) {

        $scope.mdlCountryState.selectedCounty = undefined;
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();
        $scope.GetCountyByState(modal.Id, function (data) {
            var countyId = 0;
            $scope.GetCityByStateOrCounty(modal.Id, countyId, function (data) {

            });
        });
    }

    $scope.ChangeCity = function (modal) {
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();
        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) { });
    }

    $scope.GetCountyByState = function (stateId, callback) {
        $scope.showLoader = true;
        propayRegService.getCountyByState(stateId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.County = angular.copy(response.Data);
                  var county = $scope.County.filter(function (county) { return county.id == $scope.mdlCompany.countyId });
                  if (county.length > 0) {
                      $scope.mdlCountryState.selectedCounty = county[0];
                      $scope.CopyMdlCountryState.selectedCounty = $scope.mdlCountryState.selectedCounty;
                  }
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        propayRegService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.City = angular.copy(response.Data);
                  var city = $scope.City.filter(function(city ){return city.Id == $scope.mdlCompany.cityId});
                  if (city.length > 0) {
                      $scope.mdlCountryState.selectedCity = city[0];
                      $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
                  }
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.ChangeZip = function () {
        $scope.mdlCompany.zipCode = '';
        $scope.pZipValid = true;
    }

    $scope.CompanyEmailExist = function () {

        if ($scope.mdlCompany.email == null || $scope.mdlCompany.email == '' || $scope.mdlCompany.email == undefined) {
            return;
        }
        $scope.showLoader = true;
        CompanyService.companyEmailExist($scope.mdlCompany.email).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.isCompanyEmailExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.Close = function (frmCompanyProfile) {
        frmCompanyProfile.$setPristine();
        $location.path("/home");
    }

    $scope.Edit = function (section) {
        if (section == "EditCompanyInfo") {
            CC.EditCompanyInfo = false;
        }
    }

    $scope.Cancel = function () {
        $scope.mdlCompany = angular.copy($scope.CopymdlCompany);
        CC.EditCompanyInfo = true;
    }

    $scope.Preview = function () {

        $("#mdlCompanyProfilePreview").modal('show');
    }

    $scope.toTrustedHTML = function (html) {
        return $sce.trustAsHtml(html);
    }

    $scope.ValidateZipCode = function (model, ele) {
        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidateZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");
            }
        }
    }
    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };

    $scope.ValidateZipCodeUsingAPI = function (ZipCode, ele) {
        var city = '';
        var County = '';
        var Country = '';
        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name;
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "");
                        }
                    });
                });
                //var enteredCounty = $scope.mdlCountryState.selectedCounty.name.replace(" County", "").toUpperCase().trim();
                if ($scope.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.toUpperCase().trim()) {
                    if ($scope.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {
                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        $scope.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;
        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, countyId, function (data) { });
    };

    $scope.saveSliderImages = function () {

        $scope.showLoader = true;
        var fd = new FormData();
        var isUndefined = false;      
        for (let index = 1; index <= $scope.Photos.length; index++) {
            if ($scope.Photos[index - 1] != undefined) {
                if ($scope.Photos[index - 1].seqNo != undefined) {
                    var seq = $scope.Photos[index - 1].seqNo;
                    fd.append("file", $scope.Photos[index - 1]);
                    fd.append("companyId", $scope.mdlCompany.pmid);
                    fd.append("moduleid", 'managerMaster');
                    fd.append("seqNo", seq);
                    fd.append("imageindex", index);
                    fd.append("filename", 'image' + index);
                    fd.append("filedesc", '');
                    fd.append("filetags", 'Photo, ' + $scope.pId);
                }
            }
        }
        CompanyService.savePhotos(fd).
   success(function (response) {
       $scope.showLoader = false;
       if (response.Status == true) {
           $scope.isCompanyEmailExist = response.Data;
       }
       else {
           swal("ERROR", response.Message, "error");
       }
   }).error(function (data) {
       $scope.showLoader = false;
       swal("ERROR", MessageService.responseError, "error");
   }).finally(function (data) {
       $scope.showLoader = false;
   });
    }

    $scope.removeProfileImage = function () {
        CompanyService.RemoveFile($scope.pId, function (result, data) {
            if (result) {
                $window.location.reload();
            }
            else {
                swal("ERROR", "Unable to save image.", "error");
            }
        });
    }
}
