﻿
vataraApp.controller('IncomeExpenseController', IncomeExpenseController);
IncomeExpenseController.$inject = ['$scope', 'IncomeExpenseService', 'onlineStatementService', 'serviceCategory_service',
    'masterdataService', 'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService'];
function IncomeExpenseController($scope, IncomeExpenseService, onlineStatementService, serviceCategory_service,
    masterdataService, pagingService, $filter, $routeParams, $location, $rootScope, commonService) {

    var IEC = this;
    IEC.managerid = $rootScope.userData.personId;
    IEC.isManager = true;

    IEC.pageShowOnOwner_TenantContact = false;
    IEC.personTypeForOwner_TenantContact = 0;
    IEC.owner_TenantIdForOwner_TenantContact = 0;

    if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
        IEC.pageShowOnOwner_TenantContact = true;
        IEC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
        IEC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
    }
    else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
        IEC.pageShowOnOwner_TenantContact = true;
        IEC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
        IEC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;
    }

    // $scope.menuActive = 'Income Expense Report';

    IEC.pmid = $rootScope.userData.ManagerDetail.pId;
    //IEC.ownerPersonId = $rootScope.userData.personId;
    IEC.personType = $rootScope.userData.personTypeId;
    IEC.propId = 0;   
    IEC.ownerPersonId = 0;
    IEC.ownerPOID = 0;

    IEC.selectCurrentPeriod = "";
    IEC.fromDate = new Date();
    IEC.toDate = new Date();
    IEC.lstPropertByManagerAndOwner = [];
    IEC.CopylstPropertByManagerAndOwner = [];

    IEC.lstPropertByManagerAndOwnerGrouped = [];

    IEC.lstPropertOwnersByManager = [];
    IEC.CopylstPropertOwnersByManager = [];
    var currentDates = new Date();
    IEC.DetailFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    };
    IEC.IncomeExpenceRecord = '';
    IEC.CopyIncomeExpenceRecord = '';
    IEC.StartYear = '';
    IEC.lstYears = [];

    IEC.CreatelstYears = function () {
       // IEC.StartYear = new Date('1970-01-01').getFullYear();
        IEC.lstYears = [];
        for (var i = new Date('1990-01-01').getFullYear() ; i <= new Date().getFullYear() ; i++) {

            IEC.lstYears.push({ Year: IEC.StartYear + i });
        }
    }

    IEC.LoadFunction = function () {
       
        IEC.CreatelstYears();     // common function for all
        IEC.selectCurrentPeriod = IEC.lstYears[IEC.lstYears.length - 1]

        //if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
        //    IEC.isManager = true;
        //}
        //else {
        //    IEC.isManager = false;
        //   // IEC.managerid = $rootScope.userData.personId;
        //    IEC.pmid = $rootScope.userData.ManagerDetail.pId;
        //    IEC.ownerPersonId = $rootScope.userData.personId;
        //    IEC.personType = $rootScope.userData.personTypeId;
        //    IEC.propId = 0;
        //}

        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {              
                var propoertyId = 0;
                IEC.LoadDataForOwner(propoertyId);
            }
            else {               
                IEC.LoadDataForPropertyManager();
            }
        }
        else {           
            var propoertyId = 0;
            IEC.LoadDataForOwner(propoertyId);
        }
    };

    IEC.LoadDataForPropertyManager = function () {
 
        IEC.GetPropertOwnersByManager();
        IEC.GetPropertByManagerAndOwner();
    }

    IEC.LoadDataForOwner = function (propoertyId) {

        IEC.isManager = false;
        IEC.pmid = $rootScope.userData.ManagerDetail.pId;
        IEC.ownerPersonId = $rootScope.userData.personId;
        IEC.personType = $rootScope.userData.personTypeId;
        IEC.propId = 0;

        if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

            IEC.pageShowOnOwner_TenantContact = true;
            IEC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
            IEC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
            if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {

                IEC.ownerPersonId = $routeParams.ownerId;
                IEC.ownerPOID = $routeParams.ownerPOID;
                IEC.ownerIsCompany = $routeParams.ownerIsCompany;
                IEC.personType = $rootScope.PersonTypeIdEnum.owner;

                if ($routeParams.propertyId != undefined) {
                    propoertyId = $routeParams.propertyId;
                }
            }
        }
        else {

            IEC.managerid = $rootScope.userData.personManageriD;           
            IEC.ownerPersonId = $rootScope.userData.personId;
            IEC.ownerPOID = $rootScope.userData.poid;
        }
        IEC.propId = propoertyId;
        IEC.pmid = $rootScope.userData.ManagerDetail.pId;

        // ***** SET this in case on owner login 
         // IEC.managerid = $rootScope.userData.personManageriD;

        IEC.GetpropertyByPersonAndPersonType(IEC.managerid, IEC.ownerPersonId, IEC.personType, IEC.ownerPOID, function (data) {
            IEC.DetailFilterParameters.property = data.filter(function (p) { return p.propertyId == IEC.propId })[0];
            IEC.GetOwnerStatement(IEC.managerid, IEC.ownerPOID, IEC.ownerPersonId, IEC.propId, IEC.selectCurrentPeriod.Year);
        });       
    }


    IEC.MdlMonth = function () {

        return (
            mdl = {
                January: 0,
                February: 0,
                March: 0,
                April: 0,
                May: 0,
                June: 0,
                July: 0,
                August: 0,
                September: 0,
                October: 0,
                November: 0,
                December: 0,
                serviceCategory: ''
            });
    };

    IEC.GetReport = function () {

        IEC.IncomeExpenceRecord = '';
        IEC.CopyIncomeExpenceRecord = '';

        IEC.propId = IEC.DetailFilterParameters.property.propertyId;
        IEC.pmid = $rootScope.userData.ManagerDetail.pId;

        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if (!$location.url().match("owners/") && !$location.url().match("propertyOwners/")) {
                IEC.ownerPOID = IEC.DetailFilterParameters.owner.ownerId;
                IEC.ownerPersonId = IEC.DetailFilterParameters.owner.personId;
            }
        }
        else {
            IEC.ownerPOID = $rootScope.userData.poid;
            IEC.ownerPersonId = $rootScope.userData.personId;
            IEC.managerid = $rootScope.userData.personManageriD;            
        }

        if (IEC.ownerPersonId == 0 || IEC.ownerPersonId ===undefined) {
            swal("Error", "Please select Owner", "info");
        }
        else if (IEC.propId === undefined) {
            swal("Error", "Please select Property", "info");
        }
        else {
            IEC.GetOwnerStatement(IEC.managerid, IEC.ownerPOID, IEC.ownerPersonId, IEC.propId, IEC.selectCurrentPeriod.Year);
        }
    }

    IEC.PrintPdf = function () {

        IEC.HTMLArr = [];
        var gridHtml = $("#divReport").html();
        var children = $("#divReport").children();
        for (var i = 0; i < children.length; i++) {
            var id = children[i].id;
            var childHtml = $("#" + id).html();
            IEC.HTMLArr.push(childHtml);            
        }
        onlineStatementService.Printpdf(IEC.HTMLArr,'').
        success(function (data) {

            var pdfFile = new Blob([data], { type: 'application/pdf' });
            //var pdfFileUrl = URL.createObjectURL(pdfFile);
            //window.open(pdfFileUrl);
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(pdfFile, 'IncomeExpense.pdf');
            } else {
                var objectUrl = URL.createObjectURL(pdfFile);
                window.open(objectUrl);
            }

        }).error(function (data) {
            $scope.showLoader = false;
            swal("ERROR", "", "error");
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    IEC.ButtonClick_Pdf = function (id, pdfName, headerName) {
        HtmlTableToPdf(id, pdfName, headerName, '', '');
    }

    IEC.CalculateAnnualIncomeExpence = function (iExpence) {
        var AnnualIncomeExpence = '';
        AnnualIncomeExpence = IEC.MdlMonth();
        angular.forEach(iExpence, function (xpence, xpenceKey) {

            AnnualIncomeExpence.January += xpence.January
            AnnualIncomeExpence.February += xpence.February
            AnnualIncomeExpence.March += xpence.March
            AnnualIncomeExpence.April += xpence.April
            AnnualIncomeExpence.May += xpence.May
            AnnualIncomeExpence.June += xpence.June
            AnnualIncomeExpence.July += xpence.July
            AnnualIncomeExpence.August += xpence.August
            AnnualIncomeExpence.September += xpence.September
            AnnualIncomeExpence.October += xpence.October
            AnnualIncomeExpence.November += xpence.November
            AnnualIncomeExpence.December += xpence.December
        });
        return AnnualIncomeExpence;
    }

    IEC.GetOwnerStatement = function (managerid,ownerPOID, ownerPersonId, propertyId, Year) {
     
        //IEC.propId = IEC.DetailFilterParameters.property.propertyId;
        //if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
        //    IEC.managerid = $rootScope.userData.personManageriD;
        //    IEC.ownerPersonId = $rootScope.userData.personId;
        //    IEC.ownerPOID = $rootScope.userData.poid;
        //}


        //if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
        //    IEC.managerid = $rootScope.userData.personManageriD;
        //    IEC.ownerPersonId = $rootScope.userData.personId;
        //    IEC.ownerPOID = $rootScope.userData.poid;
        //}
       
        $scope.showLoader = true;
       
        IncomeExpenseService.getOwnerStatement(managerid, IEC.pmid, ownerPOID, ownerPersonId, propertyId, Year).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       var data = angular.copy($filter('groupBy')(response.Data, 'ownerName'));
                       angular.forEach(data, function (value, key) {
                           angular.forEach(value, function (value2, key2) {
                               var Liability = IEC.GetOwnerLiability(value2.lstOwnerLiability);
                               value2.Liability = Liability;
                               var valueabc = angular.copy($filter('groupBy')(value2.lstMdlTransectionInfo, 'incmExpence'));
                               if (valueabc.income == undefined && valueabc.expense!= undefined) {
                                   valueabc.income = [];
                                   valueabc.income[0] = angular.copy(valueabc.expense[0]);
                                   IEC.ResetFields(valueabc.income[0], 'income');
                               }
                               var netIncome = IEC.MdlMonth();
                               var grossIncm = '';
                               var toatlExpnse = '';
                               angular.forEach(valueabc, function (iExpence, incmExpencekey) {
                                   if (incmExpencekey == 'income') {
                                       grossIncm = IEC.CalculateAnnualIncomeExpence(iExpence);
                                       grossIncm.serviceCategory = 'zzzz';
                                       valueabc.income.push(grossIncm);
                                   }
                                   else if (incmExpencekey == 'expense') {
                                       toatlExpnse = IEC.CalculateAnnualIncomeExpence(iExpence);
                                       toatlExpnse.serviceCategory = 'zzzz';
                                       valueabc.expense.push(toatlExpnse);
                                   }
                               });
                               valueabc.netIncome = IEC.CalculateNetIncome(grossIncm, toatlExpnse, valueabc);
                               value2.lstMdlTransectionInfo = angular.copy(valueabc);
                               value2.FinalNetIncome = IEC.GetSumOfAll(valueabc.netIncome);
                               value2.FinalGrossIncome = IEC.GetSumOfAll(grossIncm);
                               value2.FinalTotalExpenses = IEC.GetSumOfAll(toatlExpnse);
                           });
                       });
                       IEC.IncomeExpenceRecord = data;
                       IEC.CopyIncomeExpenceRecord = IEC.IncomeExpenceRecord;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
              
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    $scope.sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop];
        }, 0);
    };
    IEC.GetOwnerLiability = function (model)
    {
        var ArrTotalInvoicedAmountToOwner = model.filter(function(transections){return   transections.amountType == 'TotalInvoicedAmountToOwner'});
        var ArrTotalPaidAmountByOwner = model.filter(function(transections){return  transections.amountType == 'TotalPaidAmountByOwner'});
        var ArrTotalAmountPMHasToPayToOwner = model.filter(function(transections){return  transections.amountType == 'TotalAmountPMHasToPayToOwner'});
        var ArrTotalAmountPMPaidToOwner = model.filter(function (transections) { return transections.amountType == 'TotalAmountPMPaidToOwner' });

        var TotalInvoicedAmountToOwner = $scope.sum(ArrTotalInvoicedAmountToOwner, 'amount');
        var TotalPaidAmountByOwner = $scope.sum(ArrTotalPaidAmountByOwner, 'amount');
        var TotalAmountPMHasToPayToOwner = $scope.sum(ArrTotalAmountPMHasToPayToOwner, 'amount');
        var TotalAmountPMPaidToOwner = $scope.sum(ArrTotalAmountPMPaidToOwner, 'amount');
        var NetAmountOwnerHasToPay = TotalInvoicedAmountToOwner - TotalPaidAmountByOwner;
        var NetAmountPMHasToPayToOwner = TotalAmountPMHasToPayToOwner - TotalAmountPMPaidToOwner;
        var Liability = NetAmountOwnerHasToPay - NetAmountPMHasToPayToOwner;

        return Liability;
    }

    IEC.GetSumOfAll = function (modal) {
        return modal.January + modal.February + modal.March + modal.April + modal.May + modal.June + modal.July + modal.August + modal.September + modal.October + modal.November + modal.December;
    };

    IEC.ResetFields = function (modal, incmExpence) {
        modal.January = 0;
        modal.February = 0;
        modal.March = 0;
        modal.April = 0;
        modal.May = 0;
        modal.June = 0;
        modal.July = 0;
        modal.August = 0;
        modal.September = 0;
        modal.October = 0;
        modal.November = 0;
        modal.December = 0;
        modal.serviceCategory = 'zzzz';
        modal.incmExpence = incmExpence;
        modal.amount = 0;
        modal.billId = 0;
        modal.categoryId = 0;
        modal.date = '';
        modal.from = '';
        modal.to = '';
        modal.getYear = '';
        modal.invoiceNumber = '';
        modal.monthName = '';
        modal.monthNum = '';
        modal.paymentType = '';
        modal.propertyID = '';
        modal.serviceCategory = '';
        return modal;
    }

    IEC.CalculateNetIncome = function (mdlIncome, mdlExpense, MdlTransInfo) {

        var netIncome = IEC.MdlMonth();
        netIncome.January = (!isNaN(mdlIncome.January - mdlExpense.January)) ? mdlIncome.January - mdlExpense.January : 0;
        netIncome.February = (!isNaN(mdlIncome.February - mdlExpense.February)) ? mdlIncome.February - mdlExpense.February : 0;
        netIncome.March = (!isNaN(mdlIncome.March - mdlExpense.March) ) ? mdlIncome.March - mdlExpense.March : 0;
        netIncome.April = (!isNaN(mdlIncome.April - mdlExpense.April)) ? mdlIncome.April - mdlExpense.April : 0;
        netIncome.May = (!isNaN(mdlIncome.May - mdlExpense.May)) ? mdlIncome.May - mdlExpense.May : 0;
        netIncome.June = (!isNaN(mdlIncome.June - mdlExpense.June)) ? mdlIncome.June - mdlExpense.June : 0;
        netIncome.July = (!isNaN(mdlIncome.July - mdlExpense.July)) ? mdlIncome.July - mdlExpense.July : 0;
        netIncome.August = (!isNaN(mdlIncome.August - mdlExpense.August)) ? mdlIncome.August - mdlExpense.August : 0;
        netIncome.September = (!isNaN(mdlIncome.September - mdlExpense.September)) ? mdlIncome.September - mdlExpense.September : 0;
        netIncome.October = (!isNaN(mdlIncome.October - mdlExpense.October)) ? mdlIncome.October - mdlExpense.October : 0;
        netIncome.November = (!isNaN(mdlIncome.November - mdlExpense.November)) ? mdlIncome.November - mdlExpense.November : 0;
        netIncome.December = (!isNaN(mdlIncome.December - mdlExpense.December)) ? mdlIncome.December - mdlExpense.December : 0;
        return netIncome;
    }

    IEC.FilterProperty = function (model) {
       
        if (model.ownerId == 0) {         
            IEC.DetailFilterParameters.property = IEC.CopylstPropertByManagerAndOwner[0];
            IEC.lstPropertByManagerAndOwner = IEC.CopylstPropertByManagerAndOwner;
            //IEC.lstPropertByManagerAndOwner = IEC.lstPropertByManagerAndOwnerGrouped
        }
        else {          
            IEC.ownerPersonId = model.personId;
            IEC.personType = $rootScope.PersonTypeIdEnum.owner;
            IEC.ownerPOID = model.ownerId;
            IEC.GetpropertyByPersonAndPersonType(IEC.managerid, IEC.ownerPersonId, IEC.personType, IEC.ownerPOID, function (data) { });

            //if (IEC.DetailFilterParameters.owner != null) {
            //    IEC.lstPropertByManagerAndOwner = IEC.Filter(IEC.CopylstPropertByManagerAndOwner, IEC.DetailFilterParameters.owner.ownerId);
            //    var addNewOption_All = angular.copy(IEC.lstPropertByManagerAndOwner[0]);
            //    addNewOption_All.ownerId = 0;
            //    addNewOption_All.propertyId = 0;
            //    addNewOption_All.propertyName = " All";
            //    IEC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
            //    IEC.DetailFilterParameters.property = addNewOption_All;
            //}
            //else {
            //    //IEC.lstPropertByManagerAndOwner = IEC.CopylstPropertByManagerAndOwner;
            //    IEC.lstPropertByManagerAndOwner = IEC.lstPropertByManagerAndOwnerGrouped;
            //    IEC.DetailFilterParameters.property = IEC.lstPropertByManagerAndOwner[0];
            //}
        }
    };


    IEC.GetpropertyByPersonAndPersonType = function (managerid, personId, personType, ownerPOID,callback) {
        $scope.showLoader = true;
        IncomeExpenseService.getpropertyByPersonAndPersonType(managerid, personId, personType, ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   // console.log('GetpropertyByPersonAndPersonType response.Data', response.Data);
                   IEC.lstPropertByManagerAndOwner = response.Data;
                   //var addNewOption_All = angular.copy(IEC.lstPropertByManagerAndOwner[0]);
                   //addNewOption_All.ownerId = 0;
                   //addNewOption_All.propertyId = 0;
                   //addNewOption_All.propertyName = "All";
                   //IEC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                   //IEC.DetailFilterParameters.property = addNewOption_All;

                   callback(response.Data);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    IEC.FilterInvoiceDetailsByProperty = function (model) {
      
    }

    IEC.Filter = function (items, ownerId) {
       
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.ownerId == ownerId) {
                retArray.push(obj);
            }
        });
        return retArray;
    };

    IEC.FilterDetailsByDate = function (checkValue) {

        if (IEC.selectCurrentPeriod != null) {

            if (IEC.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {
                return;
            }
            var currentDate = new Date();
            var selectDate;
            if (IEC.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {
                currentDate = IEC.toDate;
                selectDate = IEC.fromDate;
            }
            else {
                if (IEC.selectCurrentPeriod.Period == 'Annual') {
                    selectDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {
                    selectDate = IEC.selectCurrentPeriod.date;
                }
            }
            IEC.DetailFilterParameters.owner = undefined;
            IEC.DetailFilterParameters.property = undefined;
        }
        else {

            IEC.DetailFilterParameters.owner = undefined;
            IEC.DetailFilterParameters.property = undefined;
        }
    };

    IEC.ChangeCustomToDate = function () {
        IEC.toDate = IEC.fromDate;
    }

    IEC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    IEC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    };

    IEC.GetPropertOwnersByManager = function () {
        $scope.showLoader = true;
        IncomeExpenseService.getPropertOwnersByManager(IEC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       IEC.CreateDataForPersonDropdown(response.Data, function (data) { });
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
               
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };


    IEC.CreateDataForPersonDropdown = function (model, callback) {

        IEC.ownerPersonId = 0;
        IEC.personType = 0;
        IEC.ownerPOID = 0;

        var companyTypeOwner = model.filter(function(o){return  o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 1});
        var individualOwner = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 0 });

        var companyTypeOwnerData = $filter('groupBy')(companyTypeOwner, 'ownerId');
        angular.forEach(companyTypeOwnerData, function (value, key) {
            IEC.lstPropertOwnersByManager.push(value[0])
        });

        var individualOwnerData = $filter('groupBy')(individualOwner, 'personId');
        angular.forEach(individualOwnerData, function (value, key) {
            IEC.lstPropertOwnersByManager.push(value[0]);
        });
        //var addAll = angular.copy(IEC.lstPropertOwnersByManager[0]);
        //addAll.ownerFullName = "All";
        //addAll.ownerId = 0;
        //addAll.personId = 0;
        //addAll.personTypeId = 0;
        //addAll.propertyId = 0;
        //addAll.propertyName = '';
        //IEC.lstPropertOwnersByManager.unshift(addAll);
        IEC.CopylstPropertOwnersByManager = angular.copy(IEC.lstPropertOwnersByManager);

        //console.log('IEC.lstPropertOwnersByManager', IEC.lstPropertOwnersByManager);
        if (IEC.pageShowOnOwner_TenantContact == false) {
            if ($routeParams.OwnerId && $routeParams.OwnerPOID && $routeParams.PropertyId) {
                
                IEC.ownerPersonId = $routeParams.OwnerId;
                IEC.personType = $rootScope.PersonTypeIdEnum.owner;
                IEC.ownerPOID = $routeParams.OwnerPOID;

                if ($routeParams.ownerIsCompany == 1) {
                    IEC.DetailFilterParameters.owner = IEC.lstPropertOwnersByManager.filter(function (o) { return o.ownerId == $routeParams.OwnerPOID && o.ownerIsCompany == $routeParams.ownerIsCompany })[0];
                }
                else {
                    IEC.DetailFilterParameters.owner = IEC.lstPropertOwnersByManager.filter(function (o) { return o.personId == $routeParams.OwnerId && o.ownerIsCompany == $routeParams.ownerIsCompany })[0];
                }
                IEC.GetpropertyByPersonAndPersonType(IEC.managerid, IEC.ownerPersonId, IEC.personType, IEC.ownerPOID, function (data) {

                    IEC.DetailFilterParameters.property = data.filter(function (p) { return p.propertyId == $routeParams.PropertyId })[0];
                    IEC.GetOwnerStatement(IEC.managerid, IEC.ownerPOID, IEC.ownerPersonId, $routeParams.PropertyId, IEC.selectCurrentPeriod.Year);
                });              
            }
            //else {
            //    IEC.DetailFilterParameters.owner = addAll;
            //}
        }
        else {
            var ownerModel = IEC.lstPropertOwnersByManager.filter(function (t) {
                return t.ownerId == IEC.owner_TenantIdForOwner_TenantContact
                    && t.personTypeId == IEC.personTypeForOwner_TenantContact
            })[0];
            IEC.DetailFilterParameters.owner = ownerModel;
            var delayInMilliseconds = 3000; //3 second
            setTimeout(function () {
                if (ownerModel != undefined) {
                    IEC.FilterProperty(ownerModel);
                }
            }, delayInMilliseconds);
        }
    }


    IEC.GetPropertByManagerAndOwner = function () {

        $scope.showLoader = true;
        IncomeExpenseService.getPropertByManagerAndOwner(IEC.managerid, IEC.isManager, IEC.pmid, IEC.ownerPersonId, IEC.propId, IEC.personType, IEC.ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                      
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {
                           IEC.lstPropertByManagerAndOwner.push(value[0])
                       });                      
                       //var addNewOption_All = angular.copy(IEC.lstPropertByManagerAndOwner[0]);
                       //addNewOption_All.ownerFullName = " All";
                       //addNewOption_All.ownerId = 0;
                       //addNewOption_All.propertyId = 0;
                       //addNewOption_All.propertyName = " All";
                       //IEC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       //IEC.DetailFilterParameters.property = addNewOption_All;     

                       //IEC.lstPropertByManagerAndOwnerGrouped = IEC.lstPropertByManagerAndOwner;
                       IEC.CopylstPropertByManagerAndOwner = angular.copy(IEC.lstPropertByManagerAndOwner);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
              
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };
}



