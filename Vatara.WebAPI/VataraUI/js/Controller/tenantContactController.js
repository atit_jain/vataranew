﻿
vataraApp.controller('tenantContactController', tenantContactController);
tenantContactController.$inject = ['$scope', '$routeParams', 'pagingService', 'tenantService', 'leaseService', '$filter', '$rootScope', 'commonService', '$timeout', 'propayRegService', '$location', 'personService'];
function tenantContactController($scope, $routeParams, pagingService, tenantService, leaseService, $filter, $rootScope, commonService, $timeout, propayRegService, $location, personService) {

    $scope.pTypeName = 'Tenant';
    $scope.pTypeId = $rootScope.PersonTypeIdEnum.tenant;
    $scope.personlist = [];
    $scope.tenantData = [];
    $scope.selectedLeaseData = [];
    $scope.selectLease = [];
    $scope.LeaseId = [];
    $scope.DocsArray = [];
    $scope.intLeaseId = 0;
    $scope.propertyId = 0;

    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};
  
    $scope.mdlTenatData = {
        pId :0,
        pFirstName : '',
        pMiddleName : '',
        pLastName: '',
        pPhoneNo: '',
        pZipcode: '',
        pAddressId: 0,
        pAdrsLine1 : '',
        pAdrsLine2 : '',
        pCountryId : 0,
        pStateId: 0,
        pCountyId : 0,
        pCityId: 0,
        CreatorUserId: $rootScope.userData.personId
    };

    $scope.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    $scope.CopyMdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    $scope.Country = [];
    $scope.State = [];
    $scope.County = [];
    $scope.City = [];
    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = true;
    $scope.Photos = [];

    $scope.LoadViewTenant = function ()
    {
        $scope.intLeaseId = 0;
        $scope.propertyId = 0;
        if ($location.url().match("leaseTenant/") && !angular.isUndefined($routeParams.leaseId)) {
            $scope.intLeaseId = $routeParams.leaseId;
            $scope.propertyId = $routeParams.propertyId;
        }
        if (!angular.isUndefined($routeParams.tenantId)) {
            $scope.TenantDetails($routeParams.tenantId);
        }
    }

    $scope.LoadAllTenants = function () {

        $scope.GetTenantsByPMID();
    };

    $scope.GetTenantsByPMID = function () {
        $scope.showLoader = true;
        tenantService.getTenantsByPMID($rootScope.userData.ManagerDetail.pId).
         success(function (response) {
             if (response.Status == true) {

                 $scope.personlist = response.Data;
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    };

    $scope.GetLeaseModel = function (data) {
       
        angular.forEach(data, function (value, key) {
            $scope.LeaseId.push(value.lstTenantLease[0].pLeaseId)
        });
        $scope.selectLease = $scope.LeaseId[0];
        $scope.GetLeaseData();
    }

    $scope.TenantDetails = function (tenantId) {
        $scope.showLoader = true;
        tenantService.getTenantDetails(tenantId).
         success(function (response) {
             if (response.Status == true) {
                 $scope.tenantData = angular.copy(response.Data);               
                // console.log('$scope.tenantData', $scope.tenantData);
                 $scope.GetLeaseModel($scope.tenantData.TenantLeaseData);
                 $scope.showTab = true;
                 output = document.getElementById('divimage2');
                 if (output != null) {
                     var random = Math.floor(100000 + Math.random() * 900000);
                     output.style.backgroundImage = 'url(../Uploads/Person/Photos/' + $scope.tenantData.PersonInfo.pId + '-imageProfileImagePersonId_' + $scope.tenantData.PersonInfo.pId + '.png?cb=' + random + ')';
                 }
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }


    $scope.ShowModel = function (EmailId) {

        var personTypeId = $rootScope.PersonTypeIdEnum.tenant;

        $rootScope.$emit("ChangeContactEmail", { EmailId: EmailId, personTypeId: personTypeId });
    }

    $scope.GetTenantDataForEdit = function (mdl) {
        $scope.mdlTenatData.pId = mdl.pId,
        $scope.mdlTenatData.pFirstName = mdl.pFirstName,
        $scope.mdlTenatData.pMiddleName = mdl.pMiddleName,
        $scope.mdlTenatData.pLastName = mdl.pLastName,
        $scope.mdlTenatData.pPhoneNo = mdl.pPhoneNo,
        $scope.mdlTenatData.pZipcode = mdl.pZipcode,
        $scope.mdlTenatData.pAddressId = mdl.pAddressId,
        $scope.mdlTenatData.pAdrsLine1 = mdl.pAdrsLine1,
        $scope.mdlTenatData.pAdrsLine2 = mdl.pAdrsLine2,
        $scope.mdlTenatData.pCountryId = mdl.pCountryId,
        $scope.mdlTenatData.pStateId = mdl.pStateId,
        $scope.mdlTenatData.pCountyId = mdl.pCountyId,
        $scope.mdlTenatData.pCityId = mdl.pCityId
    }

    $scope.GetLeaseData = function () {
        $scope.showLoader = true;
        $scope.showTab = false;
        angular.forEach($scope.tenantData.TenantLeaseData, function (value, key) {

            if (value.lstTenantLease[0].pLeaseId == $scope.selectLease) {
                $scope.selectedLeaseData = angular.copy(value);
                $rootScope.selectedPropertyId = $scope.selectedLeaseData.lstTenantLease[0].pPropertyId;

                $rootScope.$emit("FilterTransactionOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
                $rootScope.$emit("FilterInvoiceOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
                $rootScope.$emit("FilterBillsOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });

                $scope.GetLeaseDocuments(value.lstTenantLease[0].pId, function (data) {
                    $timeout(function () {
                        $scope.showTab = true;
                        $scope.showLoader = false;
                    }, 1000);
                });
            }
        });
    };

    $scope.GetLeaseDocuments = function (leaseId, callback) {
        $scope.showLoader = true;
        leaseService.getLeaseDocuments(leaseId, function (result, data) {
            if (result) {
                $scope.showLoader = false;
                $scope.DocsArray = angular.copy(data);
                callback(data);
            }
            else if (data == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
        });
    }

    $scope.OpenUrl = function (url) {

        window.open("/Uploads/Leases/Documents/" + url, '_blank', 'height=800,width=600');
    }



    $scope.ViewTenantProfile = function () {

        $scope.GetTenantDataForEdit($scope.tenantData.PersonInfo);

        $scope.GetCountry(function (CountryData) {

            var country = CountryData.filter(function (country) { return country.Id == $scope.mdlTenatData.pCountryId });
            if (country.length > 0) {
                $scope.mdlCountryState.selectedCountry = country[0];
            }
            $scope.GetStates(function (StatesData) {
                var state = StatesData.filter(function (state) { return state.Id == $scope.mdlTenatData.pStateId });
                if (state.length > 0) {
                    $scope.mdlCountryState.selectedState = state[0];
                }
                $scope.GetCountyByState($scope.mdlTenatData.pStateId, function (data) {
                    $scope.County = angular.copy(data);
                    var county = $scope.County.filter(function (county) { return county.id == $scope.mdlTenatData.pCountyId });
                    if (county.length > 0) {
                        $scope.mdlCountryState.selectedCounty = county[0];
                    }

                    $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, $scope.mdlTenatData.pCountyId, function (data) {

                        $scope.City = angular.copy(data);
                        var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlTenatData.pCityId });
                        if (city.length > 0) {
                            $scope.mdlCountryState.selectedCity = city[0];
                        }
                    });
                });
            });
        });
        $('#mdlTenantProfile').modal('show');
    }


    $scope.UpdateTenantDetail = function (mdlTenantProfile) {

        $scope.mdlAddress = {
            line1: $scope.mdlTenatData.pAdrsLine1,
            line2: $scope.mdlTenatData.pAdrsLine2,
            cityId: $scope.mdlCountryState.selectedCity.Id,
            countyId: 0,
            stateId: $scope.mdlCountryState.selectedState.Id,
            countryId: $scope.mdlCountryState.selectedCountry.Id,
            zipcode: $scope.mdlTenatData.pZipcode,
        }

        if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
            $scope.mdlAddress.countyId = $scope.mdlCountryState.selectedCounty.id;
        }
       var MdlTenantData = {
            mdlTenatData: $scope.mdlTenatData,
            mdlAddress: $scope.mdlAddress
        }
        $scope.showLoader = true;
        tenantService.updateTenantDetail(MdlTenantData).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.TenantDetails($scope.mdlTenatData.pId, function (data) { });
                  mdlTenantProfile.$setPristine();
                  $('#mdlTenantProfile').modal('hide');
                  swal("SUCCESS", "Data updated successfully", "success");
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCountry = function (callback) {

        $scope.showLoader = true;
        propayRegService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.Country = angular.copy(response);

                  // console.log('$scope.Country', $scope.Country);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetStates = function (callback) {
        //console.log('State called');
        $scope.showLoader = true;
        propayRegService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.State = angular.copy(response);
                  // console.log('$scope.State', $scope.State);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCountyByStateId = function (modal) {

        $scope.mdlCountryState.selectedCounty = undefined;
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();

        $scope.GetCountyByState(modal.Id, function (data) {
            $scope.County = angular.copy(data);
            var county = $scope.County.filter(function(county){return county.id == $scope.mdlTenatData.CountyId});
            if (county.length > 0) {
                $scope.mdlCountryState.selectedCounty = county[0];
                $scope.CopyMdlCountryState.selectedCounty = $scope.mdlCountryState.selectedCounty;
            }
            var countyId = 0
            if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
                countyId = $scope.mdlCountryState.selectedCounty.id;
            }
            $scope.GetCityByStateOrCounty(modal.Id, countyId, function (data) {

                $scope.City = angular.copy(data);
                var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlTenatData.pCountryId });
                // console.log('city', city);

                if (city.length > 0) {
                    $scope.mdlCountryState.selectedCity = city[0];
                    $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
                }
            });
        });
    }

    $scope.ChangeCity = function (modal) {
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();

        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) {

            $scope.City = angular.copy(data);
            var city = $scope.City.filter(function(city){return city.Id == $scope.mdlTenatData.pCityId});
            // console.log('city', city);

            if (city.length > 0) {
                $scope.mdlCountryState.selectedCity = city[0];
                $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
            }
        });
    }

    $scope.GetCountyByState = function (stateId, callback) {

        $scope.showLoader = true;
        propayRegService.getCountyByState(stateId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        propayRegService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {

              if (response != undefined) {
                  $scope.showLoader = false;
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }


    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        $scope.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;

        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, countyId, function (data) {
            $scope.City = angular.copy(data);
            var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlPerson.cityId });
            if (city.length > 0) {
                $scope.mdlCountryState.selectedCity = city[0];
                $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
            }
        });
    };

    $scope.ChangeZip = function () {
        $scope.mdlTenatData.pZipcode = '';
        $scope.pZipValid = true;
    }

    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };

    $scope.ValidatePersonInfoZipCode = function (model, ele) {

        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidatePersonInfoZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", "invalid zip", "error");                
            }
        }
    }

    $scope.ValidatePersonInfoZipCodeUsingAPI = function (ZipCode, ele) {

        var city = '';
        var County = '';
        var Country = '';
 
        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {

                var address_components = response.results[0].address_components;
                $.each(address_components, function (index, component) {

                    var types = component.types;
                    // console.log('types ', types);

                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            //console.log('administrative_area_level_1 ', component.long_name);

                            state = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_2') {
                            //   console.log('administrative_area_level_2 ', component.long_name);
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "").toUpperCase();
                        }

                    });
                });

                if ($scope.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.trim()) {
                    if ($scope.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {

                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {

                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    $scope.LoadLeaseFinancial = function (propertyId, leaseId) {

        $scope.showLoader = true;
        $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};

        propertyService.getOwnerIncomeExpenseByPropertyIdLeaseId(propertyId, leaseId).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = angular.copy(response.Data);
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $scope.showLoader = false;
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }).finally(function (data) {
                $scope.showLoader = false;
                $scope.personId = $rootScope.userData.personId;
            });
    }

    $scope.ActiveTab = function (tabName) {

        if (tabName.toUpperCase() == 'TRANSACTIONS') {            
            $rootScope.ShowInvoiceBillModel = false;            
        }
        else {
            $rootScope.ShowInvoiceBillModel = true;           
        }
        $rootScope.ActiveTabName = tabName;

    }



    $scope.tab1Class = "active2";
    $scope.tab2Class = "";
    $scope.tab3Class = "";
    $scope.tab4Class = "";
    $scope.tab5Class = "";

    $scope.setTabClass = function (tabNum) {
        $scope.tab = tabNum;
        $scope.tab1Class = "";
        $scope.tab2Class = "";
        $scope.tab3Class = "";
        $scope.tab4Class = "";
        $scope.tab5Class = "";
        if (tabNum == 1) {
            $scope.tab1Class = "active2";            
            $scope.showLoader = false;            
        }
        else if (tabNum == 2) {
            $scope.tab2Class = "active2";
        }
        else if (tabNum == 3) {
            $scope.tab3Class = "active2";           
        }
        else if (tabNum == 4) {
            $scope.tab4Class = "active2";
        }
        else if (tabNum == 5) {
            $scope.tab5Class = "active2";
        }
    };

    $rootScope.$on("ChangeTenantContactEmailSuccess", function (event, MdlParameters) {

        $scope.tenantData.PersonInfo.pEmail = MdlParameters.EmailId;
    });

}