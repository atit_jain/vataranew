﻿vataraApp.controller('financeController', financeController);
financeController.$inject = ['$scope', 'financeService', 'serviceCategory_service', 'masterdataService',
                            'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService', 'ownerService', 'storageService', 'billService'];
function financeController($scope, financeService, serviceCategory_service, masterdataService, pagingService, $filter,
                             $routeParams, $location, $rootScope, commonService, ownerService, storageService, billService) {

    var FC = this;
    FC.pageShowOnOwner_TenantContact = false;
    FC.personTypeForOwner_TenantContact = 0;
    FC.owner_TenantIdForOwner_TenantContact = 0;
    FC.pageShowOnTenantContact = false;
    if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
        FC.pageShowOnOwner_TenantContact = true;
        FC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
        FC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
    }
    else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
        FC.pageShowOnOwner_TenantContact = true;
        FC.pageShowOnTenantContact = true;
        FC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
        FC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;
    }
    $scope.menuActive = 'Financial Summary';
    $scope.menuUrl = 'summary';


    FC.PageService = pagingService;
    FC.itemsPerPageArr = FC.PageService.itemsPerPageArr;
    FC.ItemCountPerPage = _ItemsPerPage;

    FC.selectCurrentPeriod = "";
    FC.fromDate = new Date();
    FC.toDate = new Date();
    FC.lstTransections = [];
    FC.lstTransections_Filtered = [];
    $rootScope.ActiveTabName = 'Transactions';

    FC.managerid = $rootScope.userData.personId;
    FC.pmid = $rootScope.userData.ManagerDetail.pId;
    FC.personId = 0;
    FC.propertyId = 0;
    FC.ownerPOID = 0;
    FC.personType = $rootScope.userData.personTypeId;
    FC.isLease = false;
    FC.isManager = true;
    var currentDates = new Date();
    FC.lstLiabilityDetail = [];
    FC.IncmChartArray = [];

    FC.TotalLiability = 0;
    FC.OwnerLiability = 0;
    FC.TenantLiability = 0;
    FC.mdlBill = {};
    //PM 24-9-2018
    FC.CheckComeFromLease = function () {
        var lease = $location.url();
        if (lease.match("leaseview/")) {
            var urlSplit = lease.split("/");
            FC.propertyId = urlSplit[3];
            FC.isLease = true;
            //  console.log('urlSplit', urlSplit)
        }
    }
    //End PM 24-9-2018

    FC.TransectionFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    }
    FC.lstPropertOwnersByManager = [];
    FC.CopylstPropertOwnersByManager = [];
    FC.lstPropertByManagerAndOwner = [];
    FC.CopylstPropertByManagerAndOwner = [];
    FC.lstTransections_OrderBy = "";
    FC.currentPage_lstTransections = 0;
    FC.groupByCategory = {};
    FC.PieChartCategoryArray = [];
    FC.IncmExpenseChartArray = [];
    FC.personId = 0;
    FC.propertyId = 0;
    // FC.personType = 0;
    FC.current_toDate = new Date();
    FC.select_fromDate = new Date();

    FC.LoadFunction = function () {
        FC.CheckComeFromLease(); //PM 24-9-2018
        FC.lstCurrentPeriod = angular.copy(Dateranges);
        FC.selectCurrentPeriod = FC.lstCurrentPeriod[0];        
        //if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
        //    FC.GetPropertOwnersByManager();
        //}
        //else {
        //    //IF LOGGED IN person is not manager
        //    FC.isManager = false;
        //    FC.managerid = $rootScope.userData.personId;
        //    FC.pmid = $rootScope.userData.ManagerDetail.pId;
        //    FC.personId = $rootScope.userData.personId;
        //    FC.propertyId = 0;
        //    FC.personType = $rootScope.userData.personTypeId;
        //}
        //if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
        //    FC.personId = $rootScope.userData.poid;
        //    FC.GetPropertByOwnerPOID();
        //}
        //else {
        //    FC.GetPropertByManagerAndOwner();
        //}
        //FC.FilterTransections();
       
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
                var propertyId = 0;
                FC.LoadDataForOwner(propertyId);
            } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                var propertyId = 0;
                FC.LoadDataForTenant(propertyId);
            }
            else {  // FOR PM
                FC.GetPropertOwnersByManager();
                FC.GetPropertByManagerAndOwner();
                FC.FilterTransections();
            }
        }
        else {
            FC.LoadDataForOwnerTenant();
        }
        //if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
        //    FC.GetPropertOwnersByManager();
        //    FC.GetPropertByManagerAndOwner();
        //    FC.FilterTransections();
        //}
        //else {
        //    FC.LoadDataForOwnerTenant();
        //}
    }

    FC.LoadDataForOwnerTenant = function () {

        FC.isManager = false;
        FC.managerid = $rootScope.userData.personId;
        FC.pmid = $rootScope.userData.ManagerDetail.pId;
        FC.personId = $rootScope.userData.personId;
        FC.propertyId = 0;
        FC.personType = $rootScope.userData.personTypeId;
        FC.ownerPOID = $rootScope.userData.poid;

        if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
            FC.LoadDataForOwner(propertyId);
        }
        else {
            var propertyId = 0;
            FC.LoadDataForTenant(propertyId);
        }
    }

    FC.LoadDataForOwner = function (propertyId) {
        FC.select_fromDate = FC.selectCurrentPeriod.Startdate;
        FC.current_toDate = FC.selectCurrentPeriod.EndDate;
        
        if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
            FC.pageShowOnOwner_TenantContact = true;
            FC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
            FC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
            if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {
                FC.personId = $routeParams.ownerId;
                FC.ownerPOID = $routeParams.ownerPOID;
                FC.ownerIsCompany = $routeParams.ownerIsCompany;
                FC.personType = $rootScope.PersonTypeIdEnum.owner;
            }
            FC.propertyId = propertyId;          

            FC.GetPropertyByPerson(FC.ownerPOID, FC.personId, FC.managerid, FC.personType, function (data) {
                FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
            })
        }
        else {
            FC.ownerPOID = $rootScope.userData.poid;
            FC.managerid = $rootScope.userData.personManageriD;
            FC.personId = $rootScope.userData.personId;
            FC.personType = $rootScope.userData.personTypeId;
            FC.GetPropertyByPerson(FC.ownerPOID, FC.personId, FC.managerid, FC.personType, function (data) {
                FC.CopylstPropertByManagerAndOwner = FC.lstPropertByManagerAndOwner;
                FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
            })
        }   
    }

    FC.LoadDataForTenant = function (propertyId) {

        FC.select_fromDate = FC.selectCurrentPeriod.Startdate === undefined ? FC.fromDate : FC.selectCurrentPeriod.Startdate;
        FC.current_toDate = FC.selectCurrentPeriod.EndDate === undefined ? FC.toDate : FC.selectCurrentPeriod.EndDate;

        if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
            FC.pageShowOnOwner_TenantContact = true;
            FC.pageShowOnTenantContact = true;
            FC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
            FC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;

           FC.ownerPOID = 0;
           FC.personId = $routeParams.tenantId;
           FC.propertyId = propertyId;
           FC.personType = $rootScope.PersonTypeIdEnum.tenant;
           FC.GetPropertyByPerson(FC.ownerPOID, FC.personId, FC.managerid, FC.personType, function (data) {
               FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
           })
        }
        else {
            FC.ownerPOID = $rootScope.userData.poid;
            FC.managerid = $rootScope.userData.personManageriD;
            FC.personId = $rootScope.userData.personId;
            FC.personType = $rootScope.userData.personTypeId;
            FC.propertyId = $rootScope.selectedPropertyId;
            FC.GetPropertyByPerson(FC.ownerPOID, FC.personId, FC.managerid, FC.personType, function (data) {
                FC.CopylstPropertByManagerAndOwner = FC.lstPropertByManagerAndOwner;
                FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
            })
        }              
    }

    FC.GetPropertyByPerson = function (ownerPOID, personId, managerId, personType, callback) {
        $scope.showLoader = true;
        financeService.getPropertyByPerson(ownerPOID, personId, managerId, personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   FC.lstPropertByManagerAndOwner = response.Data;
                   var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                   addNewOption_All.ownerId = 0;
                   addNewOption_All.propertyId = 0;
                   addNewOption_All.propertyName = " All";
                   FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                   FC.TransectionFilterParameters.property = addNewOption_All;
                   callback(response.Data);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
               if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                   FC.personId = $rootScope.userData.personId;
               }
           });
    };

    FC.TotalExpenditure = 0;
    FC.MgrExpence = 0;
    FC.MgrIncome = 0;

    FC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    }

    FC.FilterTransections = function (checkValue) {

        if (FC.selectCurrentPeriod != null) {
            if (FC.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {
                FC.lstTransections = [];
                FC.lstTransections_Filtered = [];
                FC.IncmExpenseChartArray = [];
                FC.PieChartCategoryArray = [];
                if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                    FC.IncmChartLoad();
                }
                return;
            }
            FC.current_toDate = new Date();
            FC.select_fromDate = new Date();

            if (FC.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {

                FC.current_toDate = FC.toDate;
                FC.select_fromDate = FC.fromDate;
            }
            else {
                if (FC.selectCurrentPeriod.Period == 'Annual') {
                    FC.select_fromDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {
                    FC.select_fromDate = FC.selectCurrentPeriod.Startdate;
                    FC.current_toDate = FC.selectCurrentPeriod.EndDate;
                }
            }
            FC.lstTransections_Filtered = [];
            FC.IncmExpenseChartArray = [];
            FC.PieChartCategoryArray = [];

            FC.TransectionFilterParameters.owner = undefined;
            FC.TransectionFilterParameters.property = undefined;

            var ownerPOID = 0;
            FC.propertyId = 0;

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

                if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                    var propertyId = 0;
                    FC.LoadDataForOwner(propertyId);

                } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                    var propertyId = 0;
                    if ($rootScope.selectedPropertyId != 0 && $rootScope.selectedPropertyId != undefined) {
                        propertyId = $rootScope.selectedPropertyId;
                    }
                    FC.LoadDataForTenant(propertyId);
                }
                else {  // FOR PM
                    FC.GetAllTransections(FC.select_fromDate, FC.current_toDate, function (data) {
                        FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
                    });
                }
            }
            else {
                FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
            }

            //if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
            //    FC.personId = 0;
            //    FC.personType = $rootScope.userData.personTypeId;
            //    FC.GetAllTransections(FC.select_fromDate, FC.current_toDate);
            //}
            //else {
            //    FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
            //}
        }
        else {
            FC.lstTransections = [];
            FC.IncmExpenseChartArray = [];
            FC.PieChartCategoryArray = [];
            FC.lstTransections_Filtered = [];

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                FC.IncmChartLoad();
            }
            FC.TransectionFilterParameters.owner = undefined;
            FC.TransectionFilterParameters.property = undefined;
        }
    };

    //FC.GetOwnerTransections = function (fromDate, toDate) {
    //    $scope.showLoader = true;
    //    FC.personId = $rootScope.userData.poid;
    //    financeService.getOwnerTransaction(FC.managerid, FC.isManager, fromDate, toDate, FC.pmid, FC.personId, FC.propertyId, FC.personType, ownerPOID).
    //       success(function (response) {
    //           if (response.Status == true) {
    //               $scope.showLoader = false;
    //               // FC.lstTransections = response.Data.lstTransactionWithWriteOff;
    //               FC.lstTransections = response.Data.lstMdlTransactionByLedgerTable;
    //               FC.lstTransections_Filtered = angular.copy(FC.lstTransections);
    //               console.log('FC.lstTransections_Filtered', FC.lstTransections_Filtered);
    //               console.log('response.Data', response.Data);
    //               var lstMdlManagerLiabilityToTenant = [];
    //               FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner, lstMdlManagerLiabilityToTenant);
    //               //FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner);
    //               FC.ChartArrayForOwner(response.Data.lstIncomeExpenseLiabilityChartData);
    //               FC.personId = $rootScope.userData.personId;
    //               FC.TransectionFilterParameters.owner = FC.CopylstPropertOwnersByManager[0];
    //               FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
    //           }
    //           else {
    //               swal("ERROR", response.Message, "error");
    //               //FC.personId = $rootScope.userData.personId;
    //           }
    //       }).error(function (data, status) {
    //           if (status == 401) {
    //               commonService.SessionOut();
    //           }
    //           else {
    //               swal("ERROR", MessageService.serverGiveError, "error");
    //           }
    //           $scope.showLoader = false;
    //       }).finally(function (data) {
    //           $scope.showLoader = false;
    //           if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
    //               FC.personId = $rootScope.userData.personId;
    //           }
    //       });
    //};

    FC.GetOwnerTenantTransections = function (fromDate, toDate) {
        $scope.showLoader = true;
        //FC.managerid = $rootScope.userData.personManageriD;
        //FC.isManager = false;
        //FC.pmid = $rootScope.userData.ManagerDetail.pId;
        //FC.personId = $rootScope.userData.personId;
        //FC.personType = $rootScope.userData.personTypeId;
        //var ownerPOID = $rootScope.userData.poid;

        //if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
        //    ownerPOID = $rootScope.userData.poid;
        //    FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
        //}
        //else if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
        //    FC.GetOwnerTenantTransections(FC.select_fromDate, FC.current_toDate);
        //}

        financeService.getOwnerTransaction(FC.managerid, FC.isManager, fromDate, toDate, FC.pmid, FC.personId, FC.propertyId, FC.personType, FC.ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   FC.lstTransections = response.Data.lstMdlTransactionByLedgerTable;
                   FC.lstTransections_Filtered = angular.copy(FC.lstTransections);

                   var lstMdlManagerLiabilityToTenant = [];
                   //No need to show chart in case of owner / Tenant
                   //FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner, lstMdlManagerLiabilityToTenant);
                   //FC.ChartArrayForOwner(response.Data.lstIncomeExpenseLiabilityChartData);
                   FC.personId = $rootScope.userData.personId;
                   FC.TransectionFilterParameters.owner = FC.CopylstPropertOwnersByManager[0];
                   FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
               if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                   FC.personId = $rootScope.userData.personId;
               }
           });
    };

    FC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    FC.ChangeCustomToDate = function () {
        FC.toDate = FC.fromDate;
    }

    FC.GetAllTransections = function (fromDate, toDate,callback) {

        $scope.showLoader = true;
        if (PersonTypeEnum.owner == $rootScope.userData.personType) {
            FC.personId = $rootScope.userData.poid;
        }
        if (FC.isLease == true)    // if it comes from leaseView then set the fromDate and Todate to 2000 to 2050 to ignore the filter in stored procedure
        {
            var mdlLeaseDuration = storageService.get('mdlLeaseDuration');
            if (mdlLeaseDuration != null && mdlLeaseDuration.leaseStartDate != undefined) {
                //fromDate = mdlLeaseDuration.leaseStartDate;
                //toDate = mdlLeaseDuration.leaseEndDate;                
                fromDate = new Date(2000, 0, 1);
                toDate = new Date(2050, 11, 31);
            }
        }

        //console.log('FC.managerid', FC.managerid);
        //console.log('FC.isManager', FC.isManager);
        //console.log('fromDate', fromDate);
        //console.log('toDate', toDate);
        //console.log('FC.pmid', FC.pmid);
        //console.log('FC.personId', FC.personId);
        //console.log('FC.propertyId', FC.propertyId);
        //console.log('FC.personType', FC.personType);

        financeService.getAllTransections(FC.managerid, FC.isManager, fromDate, toDate, FC.pmid, FC.personId, FC.propertyId, FC.personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   FC.lstTransections = response.Data.lstMdlTransactionByLedgerTable;
                   FC.lstTransections_Filtered = angular.copy(FC.lstTransections);
                   FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner, response.Data.lstMdlManagerLiabilityToTenant);
                   FC.ChartArrayForManager(response.Data.lstIncomeChartData);
                  // FC.ChartArrayForManager(response.Data.lstIncomeExpenseLiabilityChartData);                 
                   if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                       FC.personId = $rootScope.userData.personId;
                   }
                   if (PersonTypeEnum.tenant == $rootScope.userData.personType) {
                       FC.lstTransections_Filtered = FC.lstTransections.filter(function (transections) { return transections.propertyId === parseInt($rootScope.selectedPropertyId) });
                   }
                   if (FC.pageShowOnOwner_TenantContact == false) {
                       FC.TransectionFilterParameters.owner = FC.CopylstPropertOwnersByManager[0];
                   }
                   else {
                       FC.TransectionFilterParameters.owner = FC.lstPropertOwnersByManager.filter(function (t) { return t.ownerId == FC.owner_TenantIdForOwner_TenantContact && t.personTypeId == FC.personTypeForOwner_TenantContact })[0];

                       if ($location.url().match("tenants/")) {
                           var Prop = FC.lstPropertByManagerAndOwner.filter(function (prop) { return prop.propertyId == $rootScope.selectedPropertyId })[0];
                           FC.TransectionFilterParameters.property = angular.copy(Prop);
                       }
                   }
                   callback(response.Data);
               }
               else {
                   swal("ERROR", response.Message, "error");
                   //FC.personId = $rootScope.userData.personId;
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
               if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                   FC.personId = $rootScope.userData.personId;
               }
           });
    };

    FC.CreateLiabilityChart = function (ownerLiabilityModel, tenantLiabilityModel) {
        
        FC.TotalLiability = 0;
        FC.OwnerLiability = 0;
        FC.TenantLiability = 0;

       // var ArrTotalInvoicedAmountToOwner = ownerLiabilityModel.filter(transections => transections.amountType == 'TotalInvoicedAmountToOwner');
       // var ArrTotalPaidAmountByOwner = ownerLiabilityModel.filter(transections => transections.amountType == 'TotalPaidAmountByOwner');
       // var ArrTotalAmountPMHasToPayToOwner = ownerLiabilityModel.filter(transections => transections.amountType == 'TotalAmountPMHasToPayToOwner');
       // var ArrTotalAmountPMPaidToOwner = ownerLiabilityModel.filter(transections => transections.amountType == 'TotalAmountPMPaidToOwner');

        var ArrTotalInvoicedAmountToOwner = ownerLiabilityModel.filter(function (transections) { return transections.amountType == 'TotalInvoicedAmountToOwner' });
        var ArrTotalPaidAmountByOwner = ownerLiabilityModel.filter(function (transections) { return  transections.amountType == 'TotalPaidAmountByOwner'});
        var ArrTotalAmountPMHasToPayToOwner = ownerLiabilityModel.filter(function (transections) { return  transections.amountType == 'TotalAmountPMHasToPayToOwner'});
        var ArrTotalAmountPMPaidToOwner = ownerLiabilityModel.filter(function (transections) { return transections.amountType == 'TotalAmountPMPaidToOwner' });


        var TotalInvoicedAmountToOwner = $scope.sum(ArrTotalInvoicedAmountToOwner, 'amount');
        var TotalPaidAmountByOwner = $scope.sum(ArrTotalPaidAmountByOwner, 'amount');
        var TotalAmountPMHasToPayToOwner = $scope.sum(ArrTotalAmountPMHasToPayToOwner, 'amount');
        var TotalAmountPMPaidToOwner = $scope.sum(ArrTotalAmountPMPaidToOwner, 'amount');

        FC.LiabitilyChartArray = [];
        var LiabilityArr = [];
        var NetAmountOwnerHasToPay = TotalInvoicedAmountToOwner - TotalPaidAmountByOwner;
        var NetAmountPMHasToPayToOwner = TotalAmountPMHasToPayToOwner - TotalAmountPMPaidToOwner;
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if (TotalAmountPMPaidToOwner < 0) {
                TotalAmountPMPaidToOwner = 0;
            }
            LiabilityArr.push('Paid Liability', TotalAmountPMPaidToOwner);
            FC.LiabitilyChartArray.push(LiabilityArr);
            var Liability = NetAmountPMHasToPayToOwner - NetAmountOwnerHasToPay;
            LiabilityArr = [];
            if (Liability < 0) {
                LiabilityArr.push('To Owner', 0);
            } else {
                LiabilityArr.push('To Owner', Liability);
            }            
            FC.LiabitilyChartArray.push(LiabilityArr);
            FC.TotalLiability += Liability;
            FC.OwnerLiability = Liability;
            if (Liability > 0) {
                if (tenantLiabilityModel.length > 0) {
                    FC.CreateTenantLiabilityChart(tenantLiabilityModel, function (data) {
                        angular.forEach(data, function (value, key) {
                            FC.LiabitilyChartArray.push(value);
                        });
                        FC.LiabitilyChartLoad();
                    })
                }
                else {
                    FC.LiabitilyChartLoad();
                }
            }
            else {

                if (tenantLiabilityModel.length > 0) {
                    FC.CreateTenantLiabilityChart(tenantLiabilityModel, function (data) {
                        if (data.length > 0) {
                            angular.forEach(data, function (value, key) {
                                FC.LiabitilyChartArray.push(value);
                            });
                            FC.LiabitilyChartLoad();
                        }
                        else {
                            FC.LiabitilyChartArray = [];
                        }
                    })
                }
                else {
                    FC.LiabitilyChartArray = [];
                }
            }
        }
        else if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {

            if (TotalPaidAmountByOwner < 0) {
                TotalPaidAmountByOwner = 0;
            }

            LiabilityArr.push('Paid Liability', TotalPaidAmountByOwner);
            FC.LiabitilyChartArray.push(LiabilityArr);
            var Liability = NetAmountOwnerHasToPay - NetAmountPMHasToPayToOwner;
            LiabilityArr = [];
            if (Liability < 0) {
                Liability = 0;
            }
            LiabilityArr.push('Liability', Liability);
            FC.LiabitilyChartArray.push(LiabilityArr);
            if (Liability > 0) {
                FC.LiabitilyChartLoad();
            }
            else {
                FC.LiabitilyChartArray = [];
            }
        }
    }

    FC.CreateTenantLiabilityChart = function (tenantLiabilityModel, callback) {

        var ArrTotalInvoicedAmountToTenant = tenantLiabilityModel.filter(function (transections) { return  transections.amountType == 'TotalInvoicedAmountToTenant'});
        var ArrTotalPaidAmountByTenant = tenantLiabilityModel.filter(function (transections) { return  transections.amountType == 'TotalPaidAmountByTenant'});
        var ArrTotalAmountPMHasToPayToTenant = tenantLiabilityModel.filter(function (transections) { return  transections.amountType == 'TotalAmountPMHasToPayToTenant'});
        var ArrTotalAmountPMPaidToTenant = tenantLiabilityModel.filter(function (transections) { return transections.amountType == 'TotalAmountPMPaidToTenant' });

        var TotalInvoicedAmountToTenant = $scope.sum(ArrTotalInvoicedAmountToTenant, 'amount');
        var TotalPaidAmountByTenant = $scope.sum(ArrTotalPaidAmountByTenant, 'amount');
        var TotalAmountPMHasToPayToTenant = $scope.sum(ArrTotalAmountPMHasToPayToTenant, 'amount');
        var TotalAmountPMPaidToTenant = $scope.sum(ArrTotalAmountPMPaidToTenant, 'amount');

        FC.TenantLiabitilyChartArray = [];
        var LiabilityArr = [];
        var NetAmountTenantHasToPay = TotalInvoicedAmountToTenant - TotalPaidAmountByTenant;
        var NetAmountPMHasToPayToTenant = TotalAmountPMHasToPayToTenant - TotalAmountPMPaidToTenant;
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if (TotalAmountPMPaidToTenant < 0) {
                TotalAmountPMPaidToTenant = 0
            }
            LiabilityArr.push('Paid Liability', TotalAmountPMPaidToTenant);
            FC.TenantLiabitilyChartArray.push(LiabilityArr);
            var Liability = NetAmountPMHasToPayToTenant - NetAmountTenantHasToPay;
            FC.TenantLiability = Liability;
            if (FC.TenantLiability > 0)
            {
                FC.TenantLiability = Liability;
               
            }
            else
            {
                FC.TenantLiability = 0;
                //FC.TotalLiability += 0;
            }
            FC.TotalLiability += Liability;
            LiabilityArr = [];

            if (Liability < 0)
            {
                Liability = 0
            }
            LiabilityArr.push('To Tenant', Liability);
            FC.TenantLiabitilyChartArray.push(LiabilityArr);
            if (Liability == 0) {
                FC.TenantLiabitilyChartArray = [];
            }

            callback(FC.TenantLiabitilyChartArray);
            //else {
            //    FC.TenantLiabitilyChartArray = [];
            //}
        }
        //else if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
        //    LiabilityArr.push('Paid Liability', TotalPaidAmountByTenant);
        //    FC.TenantLiabitilyChartArray.push(LiabilityArr);
        //    var Liability = NetAmountTenantHasToPay - NetAmountPMHasToPayToTenant;
        //    LiabilityArr = [];
        //    LiabilityArr.push('Liability', Liability);
        //    FC.TenantLiabitilyChartArray.push(LiabilityArr);
        //    if (Liability > 0) {
        //        FC.LiabitilyChartLoad();
        //    }
        //    else {
        //        FC.TenantLiabitilyChartArray = [];
        //    }
        //}
    }

    FC.CreateDataToShowLiabilityDetail = function (model) {
        var LiabilityDetail = $filter('groupBy')(model, 'owner');
        FC.lstLiabilityDetail = [];
        angular.forEach(LiabilityDetail, function (value, key) {

            var ArrTotalInvoicedAmountToOwner = value.filter(function (transections) { return  transections.amountType == 'TotalInvoicedAmountToOwner'});
            var ArrTotalPaidAmountByOwner = value.filter(function (transections) { return  transections.amountType == 'TotalPaidAmountByOwner'});
            var ArrTotalAmountPMHasToPayToOwner = value.filter(function (transections) { return  transections.amountType == 'TotalAmountPMHasToPayToOwner'});
            var ArrTotalAmountPMPaidToOwner = value.filter(function (transections) { return transections.amountType == 'TotalAmountPMPaidToOwner' });

            var TotalInvoicedAmountToOwner = $scope.sum(ArrTotalInvoicedAmountToOwner, 'amount');
            var TotalPaidAmountByOwner = $scope.sum(ArrTotalPaidAmountByOwner, 'amount');
            var TotalAmountPMHasToPayToOwner = $scope.sum(ArrTotalAmountPMHasToPayToOwner, 'amount');
            var TotalAmountPMPaidToOwner = $scope.sum(ArrTotalAmountPMPaidToOwner, 'amount');

            var LiabilityArr = {
                OwnerId: 0,
                OwnerName: '',
                PMLiabilityToOwner: 0,
                OwnerLiabilityToPM: 0,
                NetPMLiabilityToOwner: 0
            };
            var NetAmountOwnerHasToPay = TotalInvoicedAmountToOwner - TotalPaidAmountByOwner;
            var NetAmountPMHasToPayToOwner = TotalAmountPMHasToPayToOwner - TotalAmountPMPaidToOwner;
            var Liability = NetAmountPMHasToPayToOwner - NetAmountOwnerHasToPay;
                                   
            LiabilityArr.OwnerId = key;
            LiabilityArr.OwnerName = value[0].ownerName;
            LiabilityArr.PMLiabilityToOwner = NetAmountPMHasToPayToOwner;
            LiabilityArr.OwnerLiabilityToPM = NetAmountOwnerHasToPay;
            LiabilityArr.NetPMLiabilityToOwner = Liability;
            FC.lstLiabilityDetail.push(LiabilityArr);
        });
    }

    FC.GetPropertByManagerAndOwner = function () {
        $scope.showLoader = true;

        FC.lstPropertByManagerAndOwner = [];
        FC.CopylstPropertByManagerAndOwner = [];

        financeService.getPropertByManagerAndOwner(FC.managerid, FC.isManager, FC.pmid, FC.personId, FC.propertyId, FC.personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       FC.CopylstPropertByManagerAndOwner = response.Data;
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {

                           FC.lstPropertByManagerAndOwner.push(value[0])
                       });
                       var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerFullName = " All";
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       // FC.TransectionFilterParameters.property = addNewOption_All;

                       FC.TransectionFilterParameters.property = FC.lstPropertByManagerAndOwner[0];
                       FC.CopylstPropertByManagerAndOwner = FC.lstPropertByManagerAndOwner;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.FilterProperty = function (mdl) {

        //FC.personId = mdl.ownerId;
        FC.personId = mdl.personId;
        FC.ownerPOID = mdl.ownerId;
        FC.propertyId = 0;
        FC.personType = mdl.personTypeId;
        FC.currentPage_lstTransections = 0;

        if (mdl.personId == 0) {
            FC.GetPropertByManagerAndOwner();
            FC.FilterTransections();
        }
        else {
            FC.GetTransactionByPersonAndProperty(FC.select_fromDate, FC.current_toDate, FC.personId, FC.propertyId, FC.personType, FC.ownerPOID);
        }        
    }

    FC.GetTransactionByPersonAndProperty = function (fromDate, toDate, personId, propertyId, personType, ownerPOID) {

        $scope.showLoader = true;
        financeService.getTransactionByPersonAndProperty(FC.managerid, FC.isManager, FC.pmid, fromDate, toDate, personId, propertyId, personType, ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (propertyId == 0) {
                       FC.lstPropertByManagerAndOwner = response.Data.MdlPropertOwnersWithManager;
                       var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       if (!$location.url().match("tenants/")) {
                           FC.TransectionFilterParameters.property = addNewOption_All;
                       }
                   }
                   FC.lstTransections_Filtered = response.Data.lstMdlTransactionByLedgerTable;
                   FC.ChartArrayForManager(response.Data.lstIncomeChartData);
                   //FC.ChartArrayForManager(response.Data.lstIncomeExpenseLiabilityChartData);
                  
                   //FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner);
                   FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner, response.Data.lstMdlManagerLiabilityToTenant);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.Filter = function (items, propertyOwnerId) {

        var retArray = [];
        angular.forEach(items, function (obj) {

            if (obj.ownerId == propertyOwnerId) {
                retArray.push(obj);
            }
        });

        return retArray;
    }

    FC.FilterTenantProp = function (items, propertyId) {
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.propertyId == propertyId) {
                retArray.push(obj);
            }
        });

        return retArray;
    }

    $rootScope.$on("FilterTransactionOnLeaseChange", function (event, MdlParameters) {

        var Prop = FC.lstPropertByManagerAndOwner.filter(function (prop) { return prop.propertyId == MdlParameters.propertyId })[0];
        FC.TransectionFilterParameters.property = angular.copy(Prop);
        var ownerPOID = 0;                    // no need to poid as here we are viewing report for tenant
        FC.LoadDataForTenant(MdlParameters.propertyId);
    });

    FC.FilterTransectionByProperty = function (model) {

        // FC.personId = FC.TransectionFilterParameters.owner.ownerId;      // ownerId is poid

        FC.propertyId = model.propertyId;
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            FC.personId = FC.TransectionFilterParameters.owner.personId;
            FC.ownerPOID = FC.TransectionFilterParameters.owner.ownerId;
            FC.personType = FC.TransectionFilterParameters.owner.personTypeId;            
            if (FC.personId == 0 && FC.personType == 0) {
                FC.personType = $rootScope.userData.personTypeId;
                FC.GetAllTransections(FC.select_fromDate, FC.current_toDate, function (data) { });
            }
            else {                
                FC.GetTransactionByPersonAndProperty(FC.select_fromDate, FC.current_toDate, FC.personId, FC.propertyId, FC.personType, FC.ownerPOID);
            }
        }
        else {
            FC.ownerPOID = $rootScope.userData.poid;
            FC.managerid = $rootScope.userData.personManageriD;
            FC.isManager = false;            
            FC.GetTransactionByPersonAndProperty(FC.select_fromDate, FC.current_toDate, FC.personId, FC.propertyId, FC.personType, FC.ownerPOID);
        }       
    }

    FC.GetPropertOwnersByManager = function () {

        $scope.showLoader = true;
        financeService.getPropertOwnersByManager(FC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       FC.CreateDataForPersonDropdown(response.Data, function (data) { });

                       //var data = $filter('groupBy')(response.Data, 'personId');
                       //angular.forEach(data, function (value, key) {
                       //    FC.lstPropertOwnersByManager.push(value[0])
                       //});
                       //var addNewOption_All = angular.copy(FC.lstPropertOwnersByManager[0]);
                       //addNewOption_All.ownerFullName = " All";
                       //addNewOption_All.ownerId = 0;
                       //FC.lstPropertOwnersByManager.unshift(addNewOption_All);
                       //if (FC.pageShowOnOwner_TenantContact == false) {
                       //    FC.TransectionFilterParameters.owner = addNewOption_All;
                       //}
                       //else {
                       //    FC.TransectionFilterParameters.owner = FC.lstPropertOwnersByManager.filter(t => t.ownerId == FC.owner_TenantIdForOwner_TenantContact && t.personTypeId == FC.personTypeForOwner_TenantContact)[0];
                       //    FC.FilterProperty(FC.TransectionFilterParameters.owner);
                       //}
                       //FC.CopylstPropertOwnersByManager = FC.lstPropertOwnersByManager;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.CreateDataForPersonDropdown = function (model, callback) {
        var companyTypeOwner = model.filter(function (o) { return  o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 1});
        var individualOwner = model.filter(function (o) { return  o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 0});
        var Tenants = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.tenant });
        // var list = [];

        var companyTypeOwnerData = $filter('groupBy')(companyTypeOwner, 'ownerId');
        angular.forEach(companyTypeOwnerData, function (value, key) {
            FC.lstPropertOwnersByManager.push(value[0])
        });

        var TenantsData = $filter('groupBy')(Tenants, 'personId');
        angular.forEach(TenantsData, function (value, key) {
            FC.lstPropertOwnersByManager.push(value[0])
        });

        var individualOwnerData = $filter('groupBy')(individualOwner, 'personId');

        angular.forEach(individualOwnerData, function (value, key) {
            FC.lstPropertOwnersByManager.push(value[0]);
        });

        var addAll = angular.copy(FC.lstPropertOwnersByManager[0]);
        addAll.ownerFullName = "All";
        addAll.ownerFullNameForFilter = "Aaa";
        addAll.ownerId = 0;
        addAll.personId = 0;
        addAll.personTypeId = 0;
        addAll.propertyId = 0;
        addAll.propertyName = '';
        FC.lstPropertOwnersByManager.unshift(addAll);
        FC.TransectionFilterParameters.owner = addAll;

        if (FC.pageShowOnOwner_TenantContact == false) {
            FC.TransectionFilterParameters.owner = addAll;
        }
        else {
            FC.TransectionFilterParameters.owner = FC.lstPropertOwnersByManager.filter(function (t) { return t.ownerId == FC.owner_TenantIdForOwner_TenantContact && t.personTypeId == FC.personTypeForOwner_TenantContact })[0];
            FC.FilterProperty(FC.TransectionFilterParameters.owner);
        }
        FC.CopylstPropertOwnersByManager = FC.lstPropertOwnersByManager;
    }

    FC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    //*****************************************************
    //Chart Methods
    //*****************************************************

    $scope.sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop];
        }, 0);
    };

    FC.ChartArrayForManager = function (modal) {



        FC.ViewIncomeCategory = 'Income';
        FC.IncmChartArray = [];
        FC.TotalIncome = 0;
        FC.RealisedIncome = 0;
        FC.UnrealisedIncome = 0;

     //   FC.IncmChartArray = modal;


        angular.forEach(modal, function (value, key) {
            var IncmArr = [];
            IncmArr.push(value.IncomeType, value.Income);           
            FC.IncmChartArray.push(IncmArr);

            FC.TotalIncome += value.Income;
        });

        //FC.groupByCategory = $filter('groupBy')(modal, 'Category');        // Category => Income,Expense
        //angular.forEach(FC.groupByCategory, function (value, key) {
        //    var innergroup = $filter('groupBy')(value, 'Type');            // Type => Earned, Pending
        //    if (key == 'Income') {
        //        FC.TotalIncome = $scope.sum(value, 'Amount');
        //        //---------------------------------------------------------------------------------------------------
        //        // GET total income (Realised + Unrealised)
        //        var IncomeGroup = $filter('groupBy')(value, 'CategoryName');
        //        angular.forEach(IncomeGroup, function (IncomeGroupValue, key3) {
        //            var IncomeGroupTotal = $scope.sum(IncomeGroupValue, 'Amount');
        //            if (parseFloat(IncomeGroupTotal) > 0) {
        //                var IncmArr = [];
        //                IncmArr.push(key3, IncomeGroupTotal);
        //                FC.IncmChartArray.push(IncmArr);
        //            }
        //        });
        //        //----------------------------------------------------------------------------------------------------
        //        angular.forEach(innergroup, function (innerValue, key2) {
        //            if (key2 == 'Earned') {
        //                FC.RealisedIncome = $scope.sum(innerValue, 'Amount');
        //            }
        //            else {
        //                FC.UnrealisedIncome = $scope.sum(innerValue, 'Amount');
        //            }
        //        });
        //    }
        //});

        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            FC.IncmChartLoad();
        }
    };

    FC.ChartArrayForOwner = function (modal) {

        FC.ViewIncomeCategory = 'Income';
        FC.IncomeCategory = [];
        FC.IncmChartArray = [];
        FC.EarnedIncmChartArray = [];
        FC.PendingIncmChartArray = [];

        FC.ViewExpenseCategory = 'Expense';
        FC.ExpenseCategory = [];
        FC.ExpenseChartArray = [];
        FC.RealisedExpenseChartArray = [];
        FC.UnRealisedExpenseChartArray = [];

        FC.groupByCategory = $filter('groupBy')(modal, 'Category');
        angular.forEach(FC.groupByCategory, function (value, key) {

            var innergroup = $filter('groupBy')(value, 'Type');
            if (key == 'Income') {
                FC.IncomeCategory.push(key);
                angular.forEach(innergroup, function (innerValue, key2) {
                    FC.IncomeCategory.push(key2);
                    var IncomeGroup = $filter('groupBy')(innerValue, 'CategoryName');
                    angular.forEach(IncomeGroup, function (IncomeGroupValue, key3) {
                        var IncomeGroupTotal = $scope.sum(IncomeGroupValue, 'Amount');
                        if (parseFloat(IncomeGroupTotal) > 0) {
                            var IncmArr = [];
                            IncmArr.push(key3, IncomeGroupTotal);
                            if (key2 == 'Earned') {
                                FC.EarnedIncmChartArray.push(IncmArr);
                            }
                            else {
                                FC.PendingIncmChartArray.push(IncmArr);
                            }
                        }
                    });
                    var total = $scope.sum(innerValue, 'Amount');
                    var IncmArr = [];
                    IncmArr.push(key2, total);
                    FC.IncmChartArray.push(IncmArr);
                });
            }

            if (key == 'Expense') {
                FC.ExpenseCategory.push(key);
                angular.forEach(innergroup, function (innerValue, key2) {
                    FC.ExpenseCategory.push(key2);
                    var ExpenseGroup = $filter('groupBy')(innerValue, 'CategoryName');
                    angular.forEach(ExpenseGroup, function (ExpenseGroupValue, key3) {
                        var ExpenseGroupTotal = $scope.sum(ExpenseGroupValue, 'Amount');
                        if (parseFloat(ExpenseGroupTotal) > 0) {
                            var ExpenseArr = [];
                            ExpenseArr.push(key3, ExpenseGroupTotal);
                            if (key2 == 'Realised') {
                                FC.RealisedExpenseChartArray.push(ExpenseArr);
                            }
                            else {
                                FC.UnRealisedExpenseChartArray.push(ExpenseArr);
                            }
                        }
                    });
                    var total = $scope.sum(innerValue, 'Amount');
                    var ExpenseArr = [];
                    ExpenseArr.push(key2, total);
                    FC.ExpenseChartArray.push(ExpenseArr);
                });
            }
        });

        FC.IncmChartLoad();
        FC.EarnedIncmChartLoad();
        FC.PendingIncmChartLoad();
        FC.ExpenseChartLoad();
        FC.RealisedExpenseChartLoad();
        FC.UnRealisedExpenseChartLoad();
    };

    FC.ExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawExpenseChart);

        function drawExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.ExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('ExpenseChart'));
            chart.draw(data, options);
        }
    }

    FC.RealisedExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawRealisedExpenseChart);

        function drawRealisedExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.RealisedExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart (Realised)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('RealisedExpenseChart'));
            chart.draw(data, options);
        }
    }

    FC.UnRealisedExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawUnRealisedExpenseChart);

        function drawUnRealisedExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.UnRealisedExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart (Unrealised)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('UnrealisedExpenseChart'));
            chart.draw(data, options);
        }
    }

    FC.IncmChartLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.

        if (FC.IncmChartArray.length > 0) {
            google.charts.setOnLoadCallback(drawIncmChart);
        }

        function drawIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.IncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'My Income ($)'
            };
            // Instantiate and draw our chart, passing in some options.
            if ($rootScope.userData.personType == $rootScope.PersonTyp.property_manager && FC.pageShowOnOwner_TenantContact == false) {
                var chart = new google.visualization.PieChart(document.getElementById('IncmChart'));
                chart.draw(data, options);
            }
        }
    }

    FC.EarnedIncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawEarnedIncmChart);

        function drawEarnedIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.EarnedIncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart (Earned)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('EarnedIncmChart'));
            chart.draw(data, options);
        }
    }

    FC.PendingIncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawPendingIncmChart);

        function drawPendingIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.PendingIncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart (Pending)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('PendingIncmChart'));
            chart.draw(data, options);
        }
    }

    FC.LiabitilyChartLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawLiabilityChart);

        function drawLiabilityChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.LiabitilyChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'My Liabilities ($)'
            };
            // Instantiate and draw our chart, passing in some options.


            if ($rootScope.userData.personType == $rootScope.PersonTyp.property_manager && FC.LiabitilyChartArray.length > 0 && FC.pageShowOnOwner_TenantContact == false) {
                var chart = new google.visualization.PieChart(document.getElementById('LiabitilyChart'));
                chart.draw(data, options);
            }
        }
    }

    //*****************************************************
    //End Chart Methods
    //*****************************************************

    FC.ButtonClick_Excel = function (id, excelName) {
        $(id).table2excel({
            filename: excelName
        });
    }

    FC.ButtonClick_Pdf = function (id, pdfName, headerName) {
        HtmlTableToPdf(id, pdfName, headerName, '', '');
    }

    FC.GetPropertByOwnerPOID = function () {
        $scope.showLoader = true;
        ownerService.getPropertByOwnerPOID(FC.personId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       FC.CopylstPropertByManagerAndOwner = response.Data;
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {

                           FC.lstPropertByManagerAndOwner.push(value[0])
                       });
                       var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerFullName = " All";
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       FC.TransectionFilterParameters.property = FC.lstPropertByManagerAndOwner[0];
                       FC.CopylstPropertByManagerAndOwner = FC.lstPropertByManagerAndOwner;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.GetBillData = function (BillId) {

        $scope.showLoader = true;
        billService.getBillById(BillId, $rootScope.userData.personTypeId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   FC.mdlBill = angular.copy(response.Data);
                   $scope.billReport = FC.mdlBill;
                   $scope.billReport.billType = FC.mdlBill.payerReceiver;
                   $scope.billReport.companylogo = DefaultLogo;
                   $('#generateBill').modal();
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.ViewInvoice = function (invoiceId) {
        $rootScope.$emit("ViewInvoiceForFinance", { invoiceId: invoiceId });
    }
}