﻿
vataraApp.controller('manageUsersController', manageUsersController);
manageUsersController.$inject = ['$scope', 'manageUsersService', 'pagingService', '$filter', '$rootScope', 'commonService', 'propertyService'];
function manageUsersController($scope, manageUsersService, pagingService, $filter, $rootScope, commonService, propertyService) {

    var MU = this;

    MU.managerId = $rootScope.userData.personId;
    MU.pmid = $rootScope.userData.ManagerDetail.pId;
    MU.selectProperty = {};
    MU.PropertyList = [];
    MU.FilterType = 'All';
    MU.UserList = [];
    MU.UserList_Filtered = [];
    MU.propertyId = 0;

    MU.PageService = pagingService;
    MU.itemsPerPageArr = MU.PageService.itemsPerPageArr;
    MU.ItemCountPerPage = _ItemsPerPage;

    MU.currentPage = 0;
    MU.OrderBy = "";

    MU.MdlAction = {
        PersonId : 0,
        Action: '',
        ModifierPersonId: $rootScope.userData.personId,
        FromEmail: $rootScope.userData.ManagerDetail.pEmail,
        ToEmail:''
    }

    MU.UserMdl = {
        Email: "",
        PersonId: "",
        PMID:$rootScope.userData.ManagerDetail.pId
    }

    MU.LoadFunction = function () {


        MU.GetAllPropertyByPMID();
        MU.GetOwnerTenantByManagerIdAndPropertyId();
    }


    MU.GetOwnerTenant = function (selectProperty) {

        MU.FilterType = 'All';
        MU.propertyId = selectProperty.propertyId

        MU.GetOwnerTenantByManagerIdAndPropertyId();
    };


    MU.GetOwnerTenantByManagerIdAndPropertyId = function () {
        $scope.showLoader = true;
        manageUsersService.GetPropertyTenantOwner(MU.managerId, MU.propertyId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       MU.UserList = angular.copy(response.Data);
                       MU.UserList_Filtered = MU.UserList;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    MU.GetAllPropertyByPMID = function () {
        $scope.showLoader = true;
        propertyService.getPropertiesByPMID(MU.pmid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       MU.PropertyList = angular.copy(response.Data);

                       var addNewOption_All = angular.copy(MU.PropertyList[0]);
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = "All";
                       MU.PropertyList.unshift(addNewOption_All);
                       MU.selectProperty = addNewOption_All;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };


    MU.filterUser = function () {

        if (MU.FilterType == 'All') {
            MU.UserList_Filtered = MU.UserList;
        }
        else {
            MU.UserList_Filtered = MU.UserList.filter(function (person) { return person.personType == MU.FilterType });
        }   
    }


    MU.ChangeUserStatus = function (PersonId,PersonEmail, Action) {

        $scope.showLoader = true;

        MU.MdlAction.PersonId = PersonId;
        MU.MdlAction.Action = Action;
        MU.MdlAction.ToEmail = PersonEmail;

        manageUsersService.UpdateUserStatus(MU.MdlAction).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   swal("SUCCESS", "User status updated successfully", "success");
                   MU.ClearMdlAction();
                   MU.GetOwnerTenantByManagerIdAndPropertyId();
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }


    MU.ClearMdlAction = function () {

        MU.MdlAction = {
            PersonId : 0,
            Action: '',
            ModifierPersonId: $rootScope.userData.personId,
            FromEmail: $rootScope.userData.ManagerDetail.pEmail,
            ToEmail: ''
    }
    }

    MU.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }


    MU.ResendLoginCredential = function (PersonId, Email) {

        MU.UserMdl.Email = Email;
        MU.UserMdl.PersonId = PersonId;           
        manageUsersService.resendLoginCredential(MU.UserMdl).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   swal("SUCCESS", "Login Credential Sent successfully", "success");                  
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }
}