﻿vataraApp.controller('changePasswordController', changePasswordController);
changePasswordController.$inject = ['$scope', 'loginService', '$window', '$location', '$rootScope', 'commonService','$routeParams'];
function changePasswordController($scope, loginService, $window, $location, $rootScope, commonService, $routeParams) {
    var CP = this;

    CP.mdlUser = {
        email: "",
        oldPassword: "",
        newPassword: "",
        confirmPassword: ""
    };

    CP.mdlEmail = {
        oldemail: $rootScope.userData == null ? '' : $rootScope.userData.userName,
        newemail: "",
        confirmemail: "",
        personTypeId: $rootScope.userData == null ? '' : $rootScope.userData.personTypeId,
        personId: 0,
        poid: 0,
        pmid: $rootScope.userData == null ? '' : $rootScope.userData.ManagerDetail.pId
    };

    CP.isExist = false;
    CP.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    CP.PasswordError = '';

    CP.Clear = function (changePassword) {
        CP.mdlUser = {
            email: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: ""
        };
        changePassword.$setPristine();
        $("#mdlChangePassword").modal('hide');
    }

    CP.UpdatePassword = function (changePasswordForm) {
        $scope.showLoader = true;
        CP.mdlUser.email = $rootScope.userData.userName
        loginService.ChangePassword(CP.mdlUser).
         success(function (response) {
             $scope.showLoader = false;
             if (response.Status == true) {
                 CP.Clear(changePasswordForm);
                 CP.SignOut();
                 swal("SUCCESS", "Password has been updated.", "success");
             }
             else {
                 if (response.Data != null) {
                     swal("ERROR", "wrong old Password", "error");
                 }
                 else {
                     swal("ERROR", MessageService.responseError, "error");
                 }
             }

         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    CP.ClearEmailForm = function (frmChangeEmail) {
        CP.mdlEmail = {
            oldemail: $rootScope.userData.userName,
            newemail: "",
            confirmemail: "",
            personTypeId: $rootScope.userData.personTypeId,
            personId: 0,
            poid: 0,
            pmid: $rootScope.userData.ManagerDetail.pId
        };
        frmChangeEmail.$setPristine();
        $("#mdlChangeEmail").modal('hide');
    }

    $rootScope.$on("ChangeContactEmail", function (event, MdlParameters) {

        CP.mdlEmail.oldemail = MdlParameters.EmailId;
        CP.mdlEmail.personTypeId = MdlParameters.personTypeId;
    });

    CP.UpdateEmail = function (frmChangeEmail) {

        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {

                    CP.mdlEmail.personId = $routeParams.ownerId;
                    CP.mdlEmail.poid = $routeParams.ownerPOID;
                    CP.mdlEmail.personTypeId = $rootScope.PersonTypeIdEnum.owner;
                }
                CP.UpdateEmailForOwner(frmChangeEmail);

            } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
                //tenant     
                CP.UpdateEmailForTenant(frmChangeEmail);
            }
            else {
                CP.UpdateEmailForPropertyManager(frmChangeEmail);
            }
        }
        else {
            // Owner/Tenant 
            if ($rootScope.userData.personTypeId == $rootScope.PersonTypeIdEnum.tenant) {

                CP.UpdateEmailForTenant(frmChangeEmail);
            }
            else {
                CP.UpdateEmailForOwner(frmChangeEmail);
            }            
        }

        //console.log('CP.mdlEmail', CP.mdlEmail);
        //$scope.showLoader = true;
        //loginService.ChangeEmail(CP.mdlEmail).
        // success(function (response) {
        //     $scope.showLoader = false;           
        //     if (response.Status == true) {
        //         if ($rootScope.userData.personTypeId != $rootScope.PersonTypeIdEnum.owner)
        //         {                    
        //             $rootScope.userData.userName = CP.mdlEmail.newemail;
        //         }                 
        //         var Usermdl = $rootScope.userData;
        //         $window.localStorage.removeItem("loginUser");
        //         $window.localStorage.setItem("loginUser", JSON.stringify(Usermdl));             
        //         $rootScope.$emit("ChangeContactEmailSuccess", { EmailId: CP.mdlEmail.newemail });
        //         swal("SUCCESS", "Email updated successfully.", "success");
        //         CP.ClearEmailForm(frmChangeEmail);
        //     }
        //     else {
        //         if (response.Data != null) {                     
        //         }
        //         else {
        //             swal("ERROR", MessageService.responseError, "error");
        //         }
        //     }
        // }).error(function (data, status) {
        //     if (status == 401) {
        //         commonService.SessionOut();
        //     }
        //     else {
        //         swal("ERROR", MessageService.serverGiveError, "error");
        //     }
        //     $scope.showLoader = false;
        // }).finally(function (data) {
        //     $scope.showLoader = false;
        // });
    }

    CP.UpdateEmailForPropertyManager = function (frmChangeEmail) {

        $scope.showLoader = true;
        loginService.ChangeEmail(CP.mdlEmail).
         success(function (response) {
             $scope.showLoader = false;
             if (response.Status == true) {
                 var Usermdl = $rootScope.userData;
                 $window.localStorage.removeItem("loginUser");
                 $window.localStorage.setItem("loginUser", JSON.stringify(Usermdl));
                 swal("SUCCESS", "Email updated successfully.", "success");
                 if (CP.mdlEmail.oldemail == $rootScope.userData.userName) {
                     CP.SignOut();
                 }
                 CP.ClearEmailForm(frmChangeEmail);
             }
             else {
                 if (response.Data != null) {
                 }
                 else {
                     swal("ERROR", MessageService.responseError, "error");
                 }
             }
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    CP.SignOut = function () {
        $scope.showLoader = true;
        CloseHubConnection();
        CP.mdlUser = JSON.parse($window.localStorage.getItem("loginUser"));
        loginService.SignOut(CP.mdlUser).
           success(function (response) {

               commonService.SessionOut();
               // CloseHubConnection();

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");;
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    CP.UpdateEmailForTenant = function (frmChangeEmail) {

        $scope.showLoader = true;
        loginService.ChangeEmail(CP.mdlEmail).
         success(function (response) {
             $scope.showLoader = false;
             if (response.Status == true) {

                 if ($rootScope.userData.personTypeId == $rootScope.PersonTypeIdEnum.tenant) {
                     $rootScope.userData.userName = CP.mdlEmail.newemail;
                     var Usermdl = $rootScope.userData;
                     $window.localStorage.removeItem("loginUser");
                     $window.localStorage.setItem("loginUser", JSON.stringify(Usermdl));
                 }
                 else {
                     $rootScope.$emit("ChangeTenantContactEmailSuccess", { EmailId: CP.mdlEmail.newemail });
                 }
                 swal("SUCCESS", "Email updated successfully.", "success");
                 if (CP.mdlEmail.oldemail == $rootScope.userData.userName) {
                     CP.SignOut();
                 }
                 CP.ClearEmailForm(frmChangeEmail);
             }
             else {
                 if (response.Data != null) {
                 }
                 else {
                     swal("ERROR", MessageService.responseError, "error");
                 }
             }
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    CP.UpdateEmailForOwner = function (frmChangeEmail) {
      
        $scope.showLoader = true;
        loginService.ChangeEmail(CP.mdlEmail).
         success(function (response) {
             $scope.showLoader = false;
             if (response.Status == true) {
                 
                 if ($rootScope.userData.personTypeId == $rootScope.PersonTypeIdEnum.owner) {
                     //$rootScope.userData.userName = CP.mdlEmail.newemail;
                     //var Usermdl = $rootScope.userData;
                     //$window.localStorage.removeItem("loginUser");
                     //$window.localStorage.setItem("loginUser", JSON.stringify(Usermdl));
                 }
                 $rootScope.$emit("ChangeContactEmailSuccess", { EmailId: CP.mdlEmail.newemail });

                 swal("SUCCESS", "Email updated successfully.", "success");
                 if (CP.mdlEmail.oldemail == $rootScope.userData.userName) {
                     CP.SignOut();
                 }
                 CP.ClearEmailForm(frmChangeEmail);
                 //
             }
             else {
                 if (response.Data != null) {
                 }
                 else {
                     swal("ERROR", MessageService.responseError, "error");
                 }
             }
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });

    }

    CP.CheckEmailExist = function () {

        if (CP.mdlEmail.newemail == null || CP.mdlEmail.newemail == '') {
            return;
        }
        $scope.showLoader = true;
        loginService.CheckEmailExist(CP.mdlEmail.newemail).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   CP.isExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    CP.BackspaceKeyPress = function (event) {
        if (event.keyCode === 8) {
            CP.isExist = false;
        }
    };


    CP.CheckPassword = function (pwd) {
        // Validate lowercase letters
        CP.PasswordError = '';
        var lowerCaseLetters = /[a-z]/g;
        if (!pwd.match(lowerCaseLetters)) {
            CP.PasswordError = CP.PasswordError + 'One Lower Case';
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (!pwd.match(upperCaseLetters)) {
            if (CP.PasswordError != '')
                CP.PasswordError = CP.PasswordError + ',One Upper Case';
            else
                CP.PasswordError = CP.PasswordError + 'One Upper Case';
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (!pwd.match(numbers)) {
            if (CP.PasswordError != '')
                CP.PasswordError = CP.PasswordError + ',One Number';
            else
                CP.PasswordError = CP.PasswordError + 'One Number';
        }

        //Special Character
        var SpecialCharacter = /[!@#$%^&*]/g;
        if (!pwd.match(SpecialCharacter)) {
            if (CP.PasswordError != '')
                CP.PasswordError = CP.PasswordError + ',One Special Character';
            else
                CP.PasswordError = CP.PasswordError + 'One Special Character';
        }

    }

}