﻿vataraApp.controller('tenantController', tenantController);
tenantController.$inject = ['$scope', 'pagingService', 'tenantService', 'leaseService', '$filter', '$rootScope', 'commonService', '$timeout', '$window', '$location'];
function tenantController($scope, pagingService,  tenantService,leaseService, $filter, $rootScope, commonService, $timeout, $window, $location) {

    var TC = this;
    TC.managerid = $rootScope.userData.personId;
    $rootScope.selectedPropertyId = 0;
    TC.tenantData = [];
    TC.selectLease = [];
    TC.LeaseId = [];
    TC.selectedLeaseData = [];
    $scope.ActiveList = "London";
    TC.showTab = false;

    TC.DocsArray = [];
    $rootScope.ShowInvoiceBillModel = false;
    $rootScope.ActiveTabName = 'TRANSACTIONS';

    TC.PaymentDetail = [];
    TC.invoiceData = {
        payableAmt: 0,
        invoiceNo: '',
        invoiceDate: '',
        recipientName: '',
        propertyName: ''
    }
    TC.mdlPayment = {
        amount: 0,
        invoiceId: 0,
        description: '',
        manageId: TC.managerid,
        payMode: '',
        paymentMode: '',
        writeOff: false
    }

    $rootScope.InvoicePaymentDetails = {

        intId: 0,
        invNumber: "",
        from: 0,
        fromEmail: '',
        to: 0,
        propertyId: 0,
        propertyName: "",
        invDate: "",
        invDueDate: "",
        amount: 0,
        paid: 0,
        dueAmt: 0
    }

    TC.PageLoad = function () {

        commonService.RemovePaymentData();
        if (TC.managerid == undefined) {
            if ($window.localStorage.getItem("loginUser") != null) {
                $rootScope.userData = JSON.parse($window.localStorage.getItem("loginUser"));
                TC.managerid = $rootScope.userData.personId;
                $scope.TenantDetails();
                TC.setTabClass(1);
            }
            else {
                commonService.SessionOut();
            }
        }
        else {
            $scope.TenantDetails();
            TC.setTabClass(1);
        }
    };

    $scope.TenantDetails = function () {
        $scope.showLoader = true;
        tenantService.getTenantDetails(TC.managerid).
         success(function (response) {
             if (response.Status == true) {
                 TC.tenantData = angular.copy(response.Data);
                 TC.GetLeaseModel(TC.tenantData.TenantLeaseData);
                 $("#personImage").attr('src', '../Uploads/Person/Photos/' + TC.tenantData.PersonInfo.pId + '-imageProfileImagePersonId_' + TC.tenantData.PersonInfo.pId + '.png');
                
                output = document.getElementById('divimage2');
                if (output != null) {
                    var random = Math.floor(100000 + Math.random() * 900000);
                              output.style.backgroundImage = 'url(../Uploads/Person/Photos/' + TC.tenantData.PersonInfo.pId + '-imageProfileImagePersonId_' + TC.tenantData.PersonInfo.pId + '.png?cb=' + random + ')';
                          }                
              //  $("#divimage2").attr('src', '../Uploads/Person/Photos/' + TC.tenantData.PersonInfo.pId + '-imageProfileImagePersonId_' + TC.tenantData.PersonInfo.pId + '.png');
                 TC.showTab = true;
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    TC.PayInvoice = function (invoiceId, invoiceNo, recipientName, propertyName, invoiceDate, propertyId, from, dueDate, fromEmail) {
        
        TC.invoiceData.invoiceNo = invoiceNo;
        TC.invoiceData.recipientName = recipientName;
        TC.invoiceData.propertyName = propertyName;
        TC.invoiceData.invoiceDate = invoiceDate;
        TC.mdlPayment.paymentMode = '';

        tenantService.getInvoicePaymentHistory(invoiceId).
           success(function (response) {
               if (response.Status == true) {

                   TC.PaymentDetail = angular.copy(response.Data);
                   TC.invoiceData.payableAmt = (TC.PaymentDetail.TotalAmount - TC.PaymentDetail.PaidAmount);
                   TC.mdlPayment.amount = TC.invoiceData.payableAmt;
                   TC.mdlPayment.invoiceId = invoiceId;
                   TC.mdlPayment.payMode = TC.PaymentDetail.lstPaymentMode[0];
                   $rootScope.PaymentAgainstInvoice = true;
                   $rootScope.InvoicePaymentDetails.intId = invoiceId;
                   $rootScope.InvoicePaymentDetails.invNumber = invoiceNo;
                   $rootScope.InvoicePaymentDetails.from = from;
                   $rootScope.InvoicePaymentDetails.fromEmail = fromEmail;
                   $rootScope.InvoicePaymentDetails.to = TC.managerid;
                   $rootScope.InvoicePaymentDetails.propertyId = propertyId;
                   $rootScope.InvoicePaymentDetails.propertyName = propertyName;
                   $rootScope.InvoicePaymentDetails.invDate = TC.invoiceData.invoiceDate;
                   $rootScope.InvoicePaymentDetails.invDueDate = dueDate;
                   $rootScope.InvoicePaymentDetails.amount = TC.PaymentDetail.TotalAmount;
                   $rootScope.InvoicePaymentDetails.paid = TC.PaymentDetail.PaidAmount;
                   $rootScope.InvoicePaymentDetails.dueAmt = TC.invoiceData.payableAmt;
                   $window.localStorage.removeItem("PaymentAgainstInvoice");
                   $window.localStorage.setItem("PaymentAgainstInvoice", JSON.stringify($rootScope.PaymentAgainstInvoice));
                   $window.localStorage.removeItem("InvoicePaymentDetails");
                   $window.localStorage.setItem("InvoicePaymentDetails", JSON.stringify($rootScope.InvoicePaymentDetails));
                   $location.path("/payment");
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
           });
    }

    TC.GetLeaseModel = function (data) {
       
        angular.forEach(data, function (value, key) {
            TC.LeaseId.push(value.lstTenantLease[0].pLeaseId)                     
        });
        TC.selectLease = TC.LeaseId[0];
        TC.GetLeaseData();

        $rootScope.selectedPropertyId = data[0].lstTenantLease[0].pPropertyId;
        $rootScope.$emit("FilterTransactionOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
        $rootScope.$emit("FilterInvoiceOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
        $rootScope.$emit("FilterBillsOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
    }

    TC.GetLeaseData = function () {
        $scope.showLoader = true;
        TC.showTab = false;
        
        angular.forEach(TC.tenantData.TenantLeaseData, function (value, key) {

            if (value.lstTenantLease[0].pLeaseId == TC.selectLease) {
                TC.selectedLeaseData = angular.copy(value);
                $rootScope.selectedPropertyId = TC.selectedLeaseData.lstTenantLease[0].pPropertyId;

                $rootScope.$emit("FilterTransactionOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
                $rootScope.$emit("FilterInvoiceOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });
                $rootScope.$emit("FilterBillsOnLeaseChange", { propertyId: $rootScope.selectedPropertyId });

                TC.GetLeaseDocuments(value.lstTenantLease[0].pId, function (data) {
                    $timeout(function () {
                        TC.showTab = true;
                        $scope.showLoader = false;
                    }, 1000);
                });
                TC.setTabClass(1);
            }
        });
    }

    TC.RegistrationModel = function () {
        $('#mdlTenantProfile').modal('show');
    }

    TC.MerchantRegistrationModel = function () {
        $('#mdlTenantMerchantProfile').modal('show');
    }

    TC.GetLeaseDocuments = function (leaseId,callback) {
        $scope.showLoader = true;
        leaseService.getLeaseDocuments(leaseId, function (result, data) {
            if (result) {
                $scope.showLoader = false;
                TC.DocsArray = angular.copy(data);
                callback(data);
            }
            else if (data == 401) {
                alert()
                commonService.SessionOut();
            }
            else {
                alert()
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
        });
    }

    TC.OpenUrl = function (url) {      
        window.open("/Uploads/Leases/Documents/" + url, '_blank', 'height=800,width=600');
    }

    TC.ActiveTab = function (tabName) {

        if (tabName.toUpperCase() == 'TRANSACTIONS') {           
            $rootScope.ShowInvoiceBillModel = false;          
        }
        else {
            $rootScope.ShowInvoiceBillModel = true;           
        }
        $rootScope.ActiveTabName = tabName;
    }


    $scope.tab1Class = "active2";
    $scope.tab2Class = "";
    $scope.tab3Class = "";
    $scope.tab4Class = "";
    $scope.tab5Class = "";  

    TC.setTabClass = function (tabNum) {
        $scope.tab = tabNum;
        $scope.tab1Class = "";
        $scope.tab2Class = "";
        $scope.tab3Class = "";
        $scope.tab4Class = "";
        $scope.tab5Class = "";       

        if (tabNum == 1) {
            $scope.tab1Class = "active2";
            $scope.showLoader = false;
        }
        else if (tabNum == 2) {
            $scope.tab2Class = "active2";
        }
        else if (tabNum == 3) {
            $scope.tab3Class = "active2";
        }
        else if (tabNum == 4) {
            $scope.tab4Class = "active2";
        }
        else if (tabNum == 5) {
            $scope.tab5Class = "active2";
        }        
    };

};