﻿
vataraApp.controller('propayRegController', propayRegController);
propayRegController.$inject = ['$scope', 'propayRegService', 'masterdataService', 'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService', '$sce'];
function propayRegController($scope, propayRegService, masterdataService, pagingService, $filter, $routeParams, $location, $rootScope, commonService,$sce) {
    var PRC = this;

    PRC.managerid = $rootScope.userData.personId;
    PRC.mdlPerson = {};
    PRC.PersonTypeId = 0;
    // PRC.MaxDate = moment().subtract(18, 'years').calendar();

    PRC.TodayDate = new Date();

    PRC.MaxDate = new Date(PRC.TodayDate.getFullYear() - 18, PRC.TodayDate.getMonth(), PRC.TodayDate.getDate());

    PRC.MdlPropay_Profile = {

        PorpayProfile: {
            id: 0,
            personId: PRC.managerid,
            firstName: '',
            lastName: '',
            dob: PRC.MaxDate,
            email: '',
            addressId: 0,
            phone: '',
            ssn: '',
            zip: '',
            marchantAccntNum: '',
            bankName:'',
            accountName :'',
            accountNumber:'',
            accountCountryCode:'',
            accountOwnershipType:'',
            accountType:'',
            routingNumber: '',
            marchantProfileId: ''
        },
        Address: {
            addressTypeId: 0,
            mailBoxNo: '',
            line1: '',
            countryId: 0,
            stateId: 0,
            state: '',
            countyId: 0,
            cityId: 0,
            city: '',
            zipcode: '',
        }
    };

    PRC.Country = [];
    PRC.State = [];
    PRC.County = [];
    PRC.City = [];

    PRC.selectedCountry = undefined;
    PRC.selectedState = undefined;
    PRC.selectedCounty = undefined;
    PRC.selectedCity = undefined;   
    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = true;

    PRC.TermConditionUrl = ''
    PRC.TermCondition = undefined;

    PRC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    PRC.RegisterMarchant = function (frmPropay) {

        PRC.MdlPropay_Profile.Address.countryId = PRC.selectedCountry.Id;
        PRC.MdlPropay_Profile.Address.stateId = PRC.selectedState.Id;
        PRC.MdlPropay_Profile.Address.state = PRC.selectedState.Abbrevation;
        PRC.MdlPropay_Profile.Address.cityId = PRC.selectedCity.Id;
        PRC.MdlPropay_Profile.Address.city = PRC.selectedCity.name;

        if (PRC.selectedCounty != undefined && PRC.selectedCounty != null && PRC.selectedCounty != '') {
            PRC.MdlPropay_Profile.Address.countyId = PRC.selectedCounty.id;
        }
        else {
            PRC.MdlPropay_Profile.Address.countyId = 0;
        }      
        $scope.showLoader = true;
        propayRegService.registerMarchant(PRC.MdlPropay_Profile).success(function (response) {

            if (response.Status == true) {
                $scope.showLoader = false;
                $('#mdlTenantMerchantProfile').modal('hide');
                swal("SUCCESS", "Merchant registered successfully", "success");

                PRC.Clear();
                frmPropay.$setPristine();
                frmPropay.$setUntouched();
                if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
                    $location.path("/home");
                }
                else if (PersonTypeEnum.owner == $rootScope.userData.personType) {
                    $location.path("/OwnerHome");
                }
                else {
                    $location.path("/TenantHome");
                }               
            }
            else {
                swal("ERROR", response.Message, "error");
            }
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
           
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    PRC.LoadFunction = function () {

        PRC.GetCountry(function (data) {
            PRC.GetStates(function (data) {
                PRC.LoadProfile(function (data) {
                });
            });
        });
    }

    PRC.LoadProfile = function (callback) {
        $scope.showLoader = true;
        PRC.PersonTypeId = $rootScope.userData.personTypeId;
          if (PersonTypeEnum.owner == $rootScope.userData.personType) {
              PRC.managerid = $rootScope.userData.poid;
          }

        propayRegService.getUserProfile(PRC.managerid, $rootScope.userData.personTypeId).
           success(function (response) {
               if (response != undefined) {
                   $scope.showLoader = false;
                   PRC.mdlPerson = angular.copy(response);
                   PRC.ModalMap(PRC.mdlPerson, function (data) {
                       callback(response);
                   });
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;              
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    PRC.ModalMap = function (modal, callback) {
 
        PRC.MdlPropay_Profile.PorpayProfile.firstName = modal.Data.firstName;
        PRC.MdlPropay_Profile.PorpayProfile.lastName = modal.Data.lastName;
        PRC.MdlPropay_Profile.PorpayProfile.ssn = modal.Data.ssn;
        PRC.MdlPropay_Profile.PorpayProfile.zip = modal.Data.zipCode;
        PRC.MdlPropay_Profile.PorpayProfile.marchantProfileId = modal.Data.marchantProfileId;
        PRC.MdlPropay_Profile.PorpayProfile.marchantAccntNum = modal.Data.marchantAccntNum;

        if ((new Date(modal.Data.dob).getFullYear()) != PRC.TodayDate.getFullYear()) {
            PRC.MdlPropay_Profile.PorpayProfile.dob = modal.Data.dob;
        }
        PRC.MdlPropay_Profile.PorpayProfile.email = modal.Data.email;
        PRC.MdlPropay_Profile.PorpayProfile.phone = modal.Data.phone;

        PRC.MdlPropay_Profile.Address.mailBoxNo = modal.Data.mailBoxNo;
        PRC.MdlPropay_Profile.Address.line1 = modal.Data.address;
        PRC.MdlPropay_Profile.Address.countryId = modal.Data.countryId;
        PRC.MdlPropay_Profile.Address.stateId = modal.Data.stateId;
        PRC.MdlPropay_Profile.Address.cityId = modal.Data.cityId;
        PRC.MdlPropay_Profile.Address.countyId = modal.Data.countyId;
        PRC.MdlPropay_Profile.Address.zipcode = modal.Data.zipCode;
        PRC.MdlPropay_Profile.Address.marchantAccntNum = modal.Data.marchantAccntNum;

        PRC.MdlPropay_Profile.PorpayProfile.bankName = modal.Data.bankName;
        PRC.MdlPropay_Profile.PorpayProfile.accountCountryCode = modal.Data.accountCountryCode;
        PRC.MdlPropay_Profile.PorpayProfile.accountName = modal.Data.accountName;
        PRC.MdlPropay_Profile.PorpayProfile.accountNumber = modal.Data.accountNumber;
        PRC.MdlPropay_Profile.PorpayProfile.accountOwnershipType = modal.Data.accountOwnershipType;
        PRC.MdlPropay_Profile.PorpayProfile.accountType = modal.Data.accountType;
        PRC.MdlPropay_Profile.PorpayProfile.routingNumber = modal.Data.routingNumber;

        var country = PRC.Country.filter(function (country) { return country.Id == modal.Data.countryId });
        if (country.length > 0) {
            PRC.selectedCountry = country[0];
        }
        var state = PRC.State.filter(function (country) { return country.Id == modal.Data.stateId });
        if (state.length > 0) {
            PRC.selectedState = state[0];
            PRC.GetCountyByState(state[0].Id, 'new', function (data) {
                PRC.GetCityByStateOrCounty(state[0].Id, modal.Data.countyId, function (data) {
                    PRC.City = angular.copy(data);
                    var city = PRC.City.filter(function (city) { return city.Id == PRC.MdlPropay_Profile.Address.cityId });
                    if (city.length > 0) {
                        PRC.selectedCity = city[0];
                    }
                })
            });
            callback();
        }
    }

    PRC.GetCountry = function (callback) {
        $scope.showLoader = true;
        propayRegService.getCountries().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  PRC.Country = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
              
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    PRC.GetStates = function (callback) {

        $scope.showLoader = true;
        propayRegService.getStates().
          success(function (response) {

              if (response != undefined) {
                  $scope.showLoader = false;
                  PRC.State = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
             
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    PRC.GetCountyAndCityByStateId = function (modal, firstLoad) {
        PRC.GetCountyByState(modal.Id, firstLoad, function (data) { });
        PRC.GetCityByState(modal.Id, firstLoad);
    }

    PRC.GetCountyByStateId = function (modal) {

        PRC.selectedCounty = undefined;
        PRC.selectedCity = undefined;        
        PRC.County = [];
        PRC.City = [];        
        PRC.GetCountyByState(modal.Id, undefined, function (data) {
            PRC.GetCityByStateOrCounty(modal.Id, 0, function (data) {
                PRC.City = angular.copy(data);
            });
        });
    }

    PRC.GetCountyByState = function (stateId, firstLoad,callback) {

        $scope.showLoader = true;
        propayRegService.getCountyByState(stateId).
          success(function (response) {

              if (response != undefined) {
                  $scope.showLoader = false;                 
                  PRC.County = angular.copy(response.Data);                  
               
                  var county = PRC.County.filter(function (county) { return county.id == PRC.MdlPropay_Profile.Address.countyId });
                      if (county.length > 0) {
                          PRC.selectedCounty = county[0];
                      }
                      callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
              
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }
  

    PRC.GetCityByCounty = function (countyId, firstLoad) {
       
        $scope.showLoader = true;
        propayRegService.GetCityByCounty(countyId).
          success(function (response) {

              if (response != undefined) {
                  $scope.showLoader = false;
                  PRC.City = angular.copy(response.Data);
                  var city = PRC.City.filter(function (city) { return city.Id == PRC.MdlPropay_Profile.Address.cityId });
                  if (city.length > 0) {
                      PRC.selectedCity = city[0];
                  }
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
              
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    PRC.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        propayRegService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }




    PRC.ChangeCity = function (modal) {
       
        PRC.selectedCity = undefined;
        PRC.City = [];
        PRC.GetCityByStateOrCounty(PRC.selectedState.Id, modal.id, function (data) {
          PRC.City = angular.copy(data);
        });
    }


    PRC.Cancle = function (frmPropay) {
     
        frmPropay.$setPristine();
        //if (PersonType.tenant == $rootScope.userData.personType) {         
        //    $('#mdlTenantMerchantProfile').modal('hide');
        //}
     
        //if (PersonType.owner == $rootScope.userData.personType) {           
        //    $('#mdlOwnerMerchantProfile').modal('hide');
        //}          
    }

    PRC.Clear = function () {

        PRC.MdlPropay_Profile.PorpayProfile.id = 0;
        PRC.MdlPropay_Profile.PorpayProfile.personId = PRC.managerid;
        PRC.MdlPropay_Profile.PorpayProfile.firstName = '';
        PRC.MdlPropay_Profile.PorpayProfile.lastName = '';
        PRC.MdlPropay_Profile.PorpayProfile.dob = new Date();
        PRC.MdlPropay_Profile.PorpayProfile.email = '';
        PRC.MdlPropay_Profile.PorpayProfile.addressId = 0;
        PRC.MdlPropay_Profile.PorpayProfile.phone = '';
        PRC.MdlPropay_Profile.PorpayProfile.ssn = '';

        PRC.MdlPropay_Profile.Address.addressTypeId = 0;
        PRC.MdlPropay_Profile.Address.mailBoxNo = '';
        PRC.MdlPropay_Profile.Address.line1 = '';
        PRC.MdlPropay_Profile.Address.countryId = 0;
        PRC.MdlPropay_Profile.Address.stateId = 0;
        PRC.MdlPropay_Profile.Address.state = '';
        PRC.MdlPropay_Profile.Address.countyId = 0;
        PRC.MdlPropay_Profile.Address.cityId = 0;
        PRC.MdlPropay_Profile.Address.city = '';
        PRC.MdlPropay_Profile.Address.zipcode = '';

        PRC.selectedCountry = undefined;
        PRC.selectedState = undefined;
        PRC.selectedCounty = undefined;
        PRC.selectedCity = undefined;

    }


    $scope.ValidateZipCode = function (model, ele) {
        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidateZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");
            }
        }
    }
    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };

    $scope.ValidateZipCodeUsingAPI = function (ZipCode, ele) {
        var city = '';
        var County = '';
        var Country = '';
        //  $scope.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name;
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "");
                        }

                    });
                });

                var enteredCounty = $scope.mdlCountryState.selectedCounty.name.replace(" County", "").toUpperCase().trim();

                if ($scope.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.toUpperCase().trim()) {
                    if ( $scope.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {
                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    PRC.ShowTC = function (TermCondition) {
        if (TermCondition)
        {
            window.open("https://www.propay.com/en-US/Legal/ProFac-Sub-merchant-Terms-and-Conditions", "_system");
        }
    }
    //$scope.clearddl = function ($event) {
    //    $event.stopPropagation();
    //    PRC.selectedCounty = undefined;
    //    var countyId = 0;
    //    $scope.GetCityByStateOrCounty($scope.pState.Id, countyId, function (data) { });
    //};

}