﻿
vataraApp.controller('onlineStatementController', onlineStatementController);
onlineStatementController.$inject = ['$scope', 'onlineStatementService', 'IncomeExpenseService', 'serviceCategory_service',
    'masterdataService', 'pagingService', '$filter', '$routeParams', '$location', 'commonService', '$rootScope', '$window', 'billService'];
function onlineStatementController($scope, onlineStatementService, IncomeExpenseService, serviceCategory_service, masterdataService, pagingService, $filter, $routeParams, $location, commonService, $rootScope, $window, billService) {

    var OSC = this;

    OSC.pageShowOnOwner_TenantContact = false;
    OSC.personTypeForOwner_TenantContact = 0;
    OSC.owner_TenantIdForOwner_TenantContact = 0;
    OSC.pageShowOnTenantContact = false;
    OSC.ownerPOID = 0;

    if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
        OSC.pageShowOnOwner_TenantContact = true;
        OSC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
        OSC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
    }
    // this report will not show on tenant portal
    //*****************************************************
    //else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/") || $location.path() == "/TenantHome") {
    //    OSC.pageShowOnOwner_TenantContact = true;
    //    OSC.pageShowOnTenantContact = true;
    //    OSC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
    //    OSC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;
    //}


    OSC.managerid = $rootScope.userData.personId;
    OSC.isManager = true;
    OSC.Currentdate = new Date();
    OSC.selectCurrentPeriod = "";
    OSC.fromDate = new Date();
    OSC.toDate = new Date();
    var currentDates = new Date();
    OSC.lstTransections = [];
    OSC.lstTransections_Filtered = [];
    OSC.lstPropertOwnersByManager = [];
    OSC.CopylstPropertOwnersByManager = [];
    OSC.lstPropertByManagerAndOwner = [];
    OSC.CopylstPropertByManagerAndOwner = [];
    OSC.statementBeginingDate = new Date();
    OSC.statementEndingDate = new Date();

    OSC.StatementData = {};
    OSC.Comment = '';

    OSC.mdlBill = {};
    $scope.billReport = "";

    OSC.DetailFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    };

    //OSC.personId = 0;
    //OSC.personType = 0;
    OSC.pmid = $rootScope.userData.ManagerDetail.pId;
    OSC.personId = $rootScope.userData.personId;
    OSC.personType = $rootScope.userData.personTypeId;
    OSC.propId = 0;
    OSC.ownerPOID = 0;
    OSC.PrintPdf = function (divname) {

        OSC.HTMLArr = [];
        var gridHtml = $("#" + divname).html();
        OSC.HTMLArr.push(gridHtml);
        onlineStatementService.Printpdf(OSC.HTMLArr, '').
        success(function (data) {
            var pdfFile = new Blob([data], { type: 'application/pdf' });
            //var pdfFileUrl = URL.createObjectURL(pdfFile);
            //window.open(pdfFileUrl);
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(pdfFile, 'OnlineStatement.pdf');
            } else {
                var objectUrl = URL.createObjectURL(pdfFile);
                window.open(objectUrl);
            }
        }).error(function (data, config, status, headers) {
            $scope.showLoader = false;
            swal("ERROR", data, "error");
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    OSC.LoadFunction = function () {

        //Common function
        OSC.lstCurrentPeriod = angular.copy(Dateranges);
        OSC.selectCurrentPeriod = OSC.lstCurrentPeriod[0];

        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                var propertyId = 0;
                OSC.LoadDataForOwner(propertyId);

            }
                //else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
                //    var propertyId = 0;
                //    OSC.LoadDataForTenant(propertyId);
                //}
            else {
                OSC.LoadDataForPropertyManager();
            }
        }
        else {
            OSC.LoadDataForOwnerTenant();
        }
    }

    OSC.LoadDataForPropertyManager = function () {
        OSC.isManager = true;
        OSC.GetPropertOwnersByManager();
        OSC.GetPropertByManagerAndOwner(function (data) {
            OSC.FilterTransections();
        });
    }

    OSC.LoadDataForOwnerTenant = function () {
        OSC.isManager = false;
        OSC.managerid = $rootScope.userData.personId;
        OSC.pmid = $rootScope.userData.ManagerDetail.pId;
        OSC.personId = $rootScope.userData.personId;
        OSC.propId = 0;
        OSC.personType = $rootScope.userData.personTypeId;

        if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
            var propertyId = 0;
            OSC.LoadDataForOwner(propertyId);
        }
        //else {    // for tenant
        //    //1 get property
        //    //2 get invoices
        //    var propertyId = 0;
        //    OSC.LoadDataForTenant(propertyId);
        //}
    }

    OSC.LoadDataForOwner = function (propertyId) {

        OSC.DetailFilterParameters.fromDate = OSC.selectCurrentPeriod.Startdate;
        OSC.DetailFilterParameters.toDate = OSC.selectCurrentPeriod.EndDate;

        if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

            OSC.pageShowOnOwner_TenantContact = true;
            OSC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
            OSC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
            if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {

                OSC.personId = $routeParams.ownerId;
                OSC.ownerPOID = $routeParams.ownerPOID;
                OSC.ownerIsCompany = $routeParams.ownerIsCompany;
                OSC.personType = $rootScope.PersonTypeIdEnum.owner;
            }
        }
        else {
            OSC.managerid = $rootScope.userData.personManageriD;
            OSC.pmid = $rootScope.userData.ManagerDetail.pId;
            OSC.personId = $rootScope.userData.personId;
            OSC.ownerPOID = $rootScope.userData.poid;
        }
        if ($routeParams.propertyId != undefined) {
            propertyId = $routeParams.propertyId;
        }
        OSC.propId = propertyId;

        OSC.GetpropertyByPersonAndPersonType(OSC.managerid, OSC.personId, OSC.personType, OSC.ownerPOID, function (data) {

            OSC.DetailFilterParameters.property = data.filter(function (p) { return p.propertyId == propertyId })[0];
            OSC.GetgetOnlineStatement(OSC.personId, OSC.personType, OSC.propId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
        });
        // IC.GetInvoicesByPersonAndProperty(IC.Selected_fromDate, IC.Selected_toDate, IC.POID, IC.personId, IC.propertyId, IC.personType);
    }

    OSC.LoadDataForTenant = function (propertyId) {

        OSC.Selected_fromDate = OSC.selectCurrentPeriod.Startdate;
        OSC.Selected_toDate = OSC.selectCurrentPeriod.EndDate;
        if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
            OSC.pageShowOnOwner_TenantContact = true;
            OSC.pageShowOnTenantContact = true;
            OSC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
            OSC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;

            OSC.POID = 0;
            OSC.personId = $routeParams.tenantId;
            OSC.propId = propertyId;
            OSC.personType = $rootScope.PersonTypeIdEnum.tenant;
        }
        else {
            OSC.managerid = $rootScope.userData.personManageriD;
            OSC.pmid = $rootScope.userData.ManagerDetail.pId;
            OSC.personId = $rootScope.userData.personId;
            OSC.ownerPOID = $rootScope.userData.poid;
        }
        OSC.GetpropertyByPersonAndPersonType(OSC.managerid, OSC.personId, OSC.personType, OSC.ownerPOID, function (data) {

            OSC.DetailFilterParameters.property = data.filter(function (p) { return p.propertyId == propertyId })[0];
            OSC.GetgetOnlineStatement(OSC.personId, OSC.personType, OSC.DetailFilterParameters.property.propertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
        });
    }

    //OSC.GetStatement = function () {
    //    var personId = 0;
    //    var personType = 0;
    //    OSC.ownerPOID = 0;
    //    if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
    //        if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
    //            if (OSC.DetailFilterParameters.property.propertyId == undefined || OSC.DetailFilterParameters.property.propertyId == 0) {
    //                swal("ERROR", "Please select a property", "error");
    //                return;
    //            }
    //            //else {
    //            //    OSC.GetgetOnlineStatement(personId, personType, OSC.DetailFilterParameters.property.propertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
    //            //}
    //        } else if ($location.url().match("tenants/") || $location.path() == "/TenantHome") {
    //            if ($rootScope.selectedPropertyId == undefined) {
    //                swal("ERROR", "Something went wrong. Please try after some time.", "error");
    //                return;
    //            }
    //            //else {
    //            //    OSC.GetgetOnlineStatement(personId, personType, $rootScope.selectedPropertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
    //            //}
    //        }
    //        else {
    //            if (OSC.DetailFilterParameters.owner.ownerId == undefined || OSC.DetailFilterParameters.owner.ownerId == 0) {
    //                swal("ERROR", "Please select Owner/Tenant", "error");
    //                return;
    //            }
    //            personId = OSC.DetailFilterParameters.owner.personId;
    //            OSC.ownerPOID = OSC.DetailFilterParameters.owner.ownerId;
    //            personType = OSC.DetailFilterParameters.owner.personTypeId;
    //            OSC.GetgetOnlineStatement(personId, personType, $rootScope.selectedPropertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
    //        }
    //    } else {
    //        personId = $rootScope.userData.personId;
    //        personType = $rootScope.userData.personTypeId;
    //        OSC.ownerPOID = $rootScope.userData.poid;
    //    }
    // ************************************************************************************************
    //    if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
    //        if (OSC.DetailFilterParameters.owner.ownerId == undefined || OSC.DetailFilterParameters.owner.ownerId == 0) {
    //            swal("ERROR", "Please select Owner/Tenant", "error");
    //            return;
    //        }            
    //        personId = OSC.DetailFilterParameters.owner.personId;
    //        OSC.ownerPOID = OSC.DetailFilterParameters.owner.ownerId;
    //        personType = OSC.DetailFilterParameters.owner.personTypeId;
    //    }
    //    else {
    //        personId = $rootScope.userData.personId;
    //        personType = $rootScope.userData.personTypeId;
    //        OSC.ownerPOID = $rootScope.userData.poid;
    //    }
    //    if ($location.url().match("tenants/") || $location.path() == "/TenantHome") {
    //        if ($rootScope.selectedPropertyId == undefined) {
    //            swal("ERROR", "Something went wrong. Please try after some time.", "error");
    //            return;
    //        }
    //        else {
    //            OSC.GetgetOnlineStatement(personId, personType, $rootScope.selectedPropertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
    //        }
    //    }
    //    else {
    //        if (OSC.DetailFilterParameters.property.propertyId == undefined || OSC.DetailFilterParameters.property.propertyId == 0) {
    //            swal("ERROR", "Please select a property", "error");
    //            return;
    //        }
    //        else {
    //            OSC.GetgetOnlineStatement(personId, personType, OSC.DetailFilterParameters.property.propertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
    //        }
    //    }
    //}

    OSC.GetStatement = function () {
        var personId = 0;
        var personType = 0;
        OSC.ownerPOID = 0;
        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {
              
                if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {

                    OSC.personId = $routeParams.ownerId;
                    OSC.ownerPOID = $routeParams.ownerPOID;
                    OSC.ownerIsCompany = $routeParams.ownerIsCompany;
                    OSC.personType = $rootScope.PersonTypeIdEnum.owner;
                    personType = OSC.personType;
                }
            }
            else if (OSC.DetailFilterParameters.owner.ownerId == undefined || OSC.DetailFilterParameters.owner.ownerId == 0) {
                swal("ERROR", "Please select Owner/Tenant", "info");
                return;
            }
            else {
                personId = OSC.DetailFilterParameters.owner.ownerId;
                personId = OSC.DetailFilterParameters.owner.personId;
                OSC.ownerPOID = OSC.DetailFilterParameters.owner.ownerId;
                personType = OSC.DetailFilterParameters.owner.personTypeId;
            }           
        }
        else {
            personId = $rootScope.userData.personId;
            personType = $rootScope.userData.personTypeId;
            OSC.ownerPOID = $rootScope.userData.poid;
        }
        if (OSC.DetailFilterParameters.property.propertyId == undefined || OSC.DetailFilterParameters.property.propertyId == 0) {
            swal("ERROR", "Please select a property", "info");
            return;
        }
        else {
            OSC.GetgetOnlineStatement(personId, personType, OSC.DetailFilterParameters.property.propertyId, OSC.DetailFilterParameters.fromDate, OSC.DetailFilterParameters.toDate);
        }
    }

    OSC.GetgetOnlineStatement = function (personId, personType, propertyId, fromDate, toDate) {
        $scope.showLoader = true;

        if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
            OSC.managerid = $rootScope.userData.personManageriD;
            //OSC.personId = $rootScope.userData.poid;
            OSC.personId = $rootScope.userData.personId;
            OSC.ownerPOID = $rootScope.userData.poid;
        }

        onlineStatementService.getOnlineStatement(OSC.managerid, OSC.personId, personType, propertyId, fromDate, toDate, OSC.ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   OSC.statementBeginingDate = fromDate;
                   OSC.statementEndingDate = toDate;

                   OSC.StatementData = angular.copy(response.Data);

               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    OSC.GetPropertOwnersByManager = function () {
        if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
            OSC.managerid = $rootScope.userData.personManageriD;
        }
        $scope.showLoader = true;
        onlineStatementService.getPropertOwnersByManager(OSC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       OSC.CreateDataForPersonDropdown(response.Data, function (data) { });
                       //var data = $filter('groupBy')(response.Data, 'personId');                       
                       //angular.forEach(data, function (value, key) {
                       //    OSC.lstPropertOwnersByManager.push(value[0])
                       //});
                       //var addNewOption_All = angular.copy(OSC.lstPropertOwnersByManager[0]);
                       //addNewOption_All.ownerFullName = " All";
                       //addNewOption_All.ownerId = 0;
                       //OSC.lstPropertOwnersByManager.unshift(addNewOption_All);
                       //OSC.CopylstPropertOwnersByManager = angular.copy(OSC.lstPropertOwnersByManager);
                       //if (OSC.pageShowOnOwner_TenantContact == false) {
                       //    OSC.DetailFilterParameters.owner = addNewOption_All;                          
                       //}
                       //else
                       //{
                       //    var ownerModel = OSC.lstPropertOwnersByManager.filter(t => t.ownerId == OSC.owner_TenantIdForOwner_TenantContact
                       //        && t.personTypeId == OSC.personTypeForOwner_TenantContact)[0];
                       //    OSC.DetailFilterParameters.owner = ownerModel;                          
                       //    var delayInMilliseconds = 3000; //3 second
                       //    setTimeout(function () {
                       //        if (ownerModel != undefined)
                       //        {
                       //            OSC.FilterProperty(ownerModel);
                       //        }                              
                       //    }, delayInMilliseconds);                          
                       //}    
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };


    OSC.ViewInvoice = function (invoiceId) {
        $rootScope.$emit("ViewInvoiceForFinance", { invoiceId: invoiceId });
    }

    OSC.GetBillData = function (BillId) {

        $scope.showLoader = true;
        billService.getBillById(BillId, $rootScope.userData.personTypeId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   OSC.mdlBill = angular.copy(response.Data);
                   $scope.billReport = OSC.mdlBill;
                   $scope.billReport.billType = OSC.mdlBill.payerReceiver;
                   $scope.billReport.companylogo = DefaultLogo;
                   $('#generateBill').modal();
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };




    OSC.CreateDataForPersonDropdown = function (model, callback) {
        var companyTypeOwner = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 1 });
        var individualOwner = model.filter(function(o){return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 0});
        var Tenants = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.tenant });

        var companyTypeOwnerData = $filter('groupBy')(companyTypeOwner, 'ownerId');
        angular.forEach(companyTypeOwnerData, function (value, key) {
            OSC.lstPropertOwnersByManager.push(value[0])
        });

        var TenantsData = $filter('groupBy')(Tenants, 'personId');
        angular.forEach(TenantsData, function (value, key) {
            OSC.lstPropertOwnersByManager.push(value[0])
        });

        var individualOwnerData = $filter('groupBy')(individualOwner, 'personId');
        angular.forEach(individualOwnerData, function (value, key) {
            OSC.lstPropertOwnersByManager.push(value[0]);
        });
        //var addAll = angular.copy(OSC.lstPropertOwnersByManager[0]);
        //addAll.ownerFullName = "All";
        //addAll.ownerId = 0;
        //addAll.personId = 0;
        //addAll.personTypeId = 0;
        //addAll.propertyId = 0;
        //addAll.propertyName = '';
        //OSC.lstPropertOwnersByManager.unshift(addAll);
        OSC.CopylstPropertOwnersByManager = angular.copy(OSC.lstPropertOwnersByManager);
        if (OSC.pageShowOnOwner_TenantContact == false) {
            OSC.DetailFilterParameters.owner = addAll;
        }
        else {
            var ownerModel = OSC.lstPropertOwnersByManager.filter(function (t) {
                return t.ownerId == OSC.owner_TenantIdForOwner_TenantContact
                    && t.personTypeId == OSC.personTypeForOwner_TenantContact
            })[0];
            OSC.DetailFilterParameters.owner = ownerModel;

            var delayInMilliseconds = 3000; //3 second
            setTimeout(function () {
                if (ownerModel != undefined) {
                    OSC.FilterProperty(ownerModel);
                }
            }, delayInMilliseconds);
        }
    }



    OSC.GetPropertByManagerAndOwner = function (callback) {

        $scope.showLoader = true;
        onlineStatementService.getPropertByManagerAndOwner(OSC.managerid, OSC.isManager, OSC.pmid, OSC.personId, OSC.propId, OSC.personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {
                           OSC.lstPropertByManagerAndOwner.push(value[0])
                       });
                       //var addNewOption_All = angular.copy(OSC.lstPropertByManagerAndOwner[0]);
                       //addNewOption_All.ownerFullName = "All";
                       //addNewOption_All.ownerId = 0;
                       //addNewOption_All.propertyId = 0;
                       //addNewOption_All.propertyName = "All";
                       //OSC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       //OSC.DetailFilterParameters.property = addNewOption_All;
                       OSC.CopylstPropertByManagerAndOwner = OSC.lstPropertByManagerAndOwner;

                       callback(OSC.lstPropertByManagerAndOwner);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };


    OSC.GetpropertyByPersonAndPersonType = function (managerid, personId, personType, ownerPOID, callback) {
        $scope.showLoader = true;
        onlineStatementService.getpropertyByPersonAndPersonType(managerid, personId, personType, ownerPOID).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   // console.log('GetpropertyByPersonAndPersonType response.Data', response.Data);

                   OSC.lstPropertByManagerAndOwner = response.Data;
                   //var addNewOption_All = angular.copy(OSC.lstPropertByManagerAndOwner[0]);
                   //addNewOption_All.ownerId = 0;
                   //addNewOption_All.propertyId = 0;
                   //addNewOption_All.propertyName = "All";
                   //OSC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                   //OSC.DetailFilterParameters.property = addNewOption_All;

                   callback(OSC.lstPropertByManagerAndOwner);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    OSC.FilterProperty = function (mdl) {
        if (mdl.ownerId == 0) {
            // OSC.lstTransections_Filtered = angular.copy(OSC.lstTransections);
            OSC.DetailFilterParameters.property = OSC.CopylstPropertByManagerAndOwner[0];
            OSC.lstPropertByManagerAndOwner = angular.copy(OSC.CopylstPropertByManagerAndOwner);
            OSC.personId = OSC.managerid;
            OSC.personType = 0;
        }
        else {
            // OSC.personId = mdl.ownerId;
            OSC.personId = mdl.personId;
            OSC.ownerPOID = mdl.ownerId;
            OSC.personType = mdl.personTypeId;

            OSC.GetpropertyByPersonAndPersonType(OSC.managerid, OSC.personId, OSC.personType, OSC.ownerPOID, function (data) { });
        }
    }

    OSC.FilterTransections = function (checkValue) {

        if (OSC.selectCurrentPeriod != null) {
            if (OSC.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {
                OSC.DetailFilterParameters.fromDate = OSC.fromDate;
                OSC.DetailFilterParameters.toDate = OSC.toDate;
                return;
            }
            var currentDate = new Date();
            OSC.DetailFilterParameters.toDate = currentDate;
            var selectDate;
            if (OSC.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {

                currentDate = OSC.toDate;
                selectDate = OSC.fromDate;
                OSC.DetailFilterParameters.fromDate = selectDate;
                OSC.DetailFilterParameters.toDate = currentDate;
            }
            else {
                if (OSC.selectCurrentPeriod.Period == 'Annual') {
                    selectDate = new Date(new Date().getFullYear(), 0, 1);
                    OSC.DetailFilterParameters.fromDate = selectDate;
                }
                else {
                    //selectDate = OSC.selectCurrentPeriod.date;
                    selectDate = OSC.selectCurrentPeriod.Startdate;
                    currentDate = OSC.selectCurrentPeriod.EndDate;
                    OSC.DetailFilterParameters.fromDate = selectDate;
                    OSC.DetailFilterParameters.toDate = currentDate;
                }
            }
        }
        // else {}
    };

    OSC.Filter = function (items, propertyOwnerId) {
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.ownerId == propertyOwnerId) {
                retArray.push(obj);
            }
        });
        return retArray;
    }

    OSC.FilterTenantProp = function (items, propertyId) {
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.propertyId == propertyId) {
                retArray.push(obj);
            }
        });
        return retArray;
    }

    OSC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    }

    OSC.ChangeCustomToDate = function () {
        OSC.toDate = OSC.fromDate;
        OSC.DetailFilterParameters.fromDate = OSC.fromDate;
        OSC.DetailFilterParameters.toDate = OSC.toDate;
    }

    OSC.SetToDate = function () {
        OSC.DetailFilterParameters.toDate = OSC.toDate;
    }
}