﻿vataraApp.controller('subscriptionController', subscriptionController);
subscriptionController.$inject = ['$scope', 'subscriptionService', 'pagingService', '$filter', '$rootScope', 'commonService'];
function subscriptionController($scope,subscriptionService, pagingService,  $filter, $rootScope, commonService) {

    var SC = this;
    SC.managerId = $rootScope.userData.ManagerDetail.pId;
    SC.MdlSubscriptionDetail = [];
    SC.SubscriptionPackage = [];
    SC.SelectedPackage = '';
    $scope.PaymentUrl = '';
    SC.ShowSubscription = true;
    SC.lstSubScriptionHistory = [];
    SC.SubscriptionAction = false;
    SC.PlanCode = '';
    SC.CustomerId = 0;
    SC.SubscriptionExpire = false;
    SC.SubscriptionId = "";
    SC.PlanCode = "";

    SC.MdlPayment = {
        manageId: $rootScope.userData.ManagerDetail.pId,
        managerEmail: "",
        invoiceNumber: "",
        invoiceId: 0,
        amount: 0,
        payerId: 0,
        merchantProfileId: 0,
        name: "",
        paymentMode: 'Online',
        transactionIdentifier: '',
        description: "",
        withInvoice: "",
        propertyId: 0,
        clientId: 0,
        clientEmail: '',
        selectPaymentType: 0,
        PayerType: 0,
        RecipientType: 0,
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipCode: '',
        transactionHistoryId: 0,
    };


    SC.Payment = {
        PaymentMode: '',
        FirstName: '',
        CVV: '',
        ExpireDate: '',
        LastDigits: '',
        AutoRenewal: false,
        SubscriptionId: '',
        PlanCode:''
    }

    SC.LoadFunction = function () {
        SC.GetSubscriptionPlan();
        SC.GetSubscriptionInfo();
        SC.GetSubscriptionTypes();
    }


    SC.GetSubscriptionInfo = function () {
        $scope.showLoader = true;
        subscriptionService.getSubscriptionInfo(SC.managerId).
         success(function (response) {
             if (response.Status == true) {

                 SC.MdlSubscriptionDetail = response.Data;
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    SC.GetSubscriptionTypes = function () {
        $scope.showLoader = true;
        subscriptionService.getSubscriptionTypes().
         success(function (response) {
             if (response.Status == true) {
                 SC.SubscriptionPackage = response.Data;
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;
         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }


    SC.Pay = function () {
        $scope.showLoader = true;
        subscriptionService.paySubscription().
         success(function (response) {
             if (response.Status == true) {

                // $scope.PaymentUrl
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;
         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }


    SC.ShowMdlRenewSubscription = function () {
        $("#mdlRenewSubscription").modal();
    }

    SC.CloseMdlRenewSubscription = function (frmRenewSubscription) {
        SC.SelectedPackage = '';
        frmRenewSubscription.$setPristine();
        $("#mdlRenewSubscription").modal("hide");
    }

    SC.GetSubscriptionPlan = function () {
        debugger;
        subscriptionService.GetSubscriptionPlan().
         success(function (response) {
             if (response.Status == true) {

                 // $scope.PaymentUrl
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;
         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }
    SC.GetCustomerProfile = function () {
        subscriptionService.GetCustomerProfile($rootScope.userData.userName).
            success(function (response) {
                if (response.Status == true) {
                    if (response.data != null) {
                        SC.ZohoCustomerProfile = response.data;
                    }
                    else { SC.ZohoCustomerProfile = null; }
                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
                $scope.showLoader = false;
            }).error(function (daya, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            })
    }

    SC.OnLoadSubScriptionHistory = function () {
        SC.SubscriptionExpire = JSON.parse(window.localStorage.getItem("SubscriptionExpire"));
        subscriptionService.GetCustomerSubscriptionHistory($rootScope.userData.ManagerDetail.pId).
           success(function (response) {
               if (response.Status == true) {
                   SC.lstSubScriptionHistory = response.Data;

                   if (SC.lstSubScriptionHistory.length != 0) {
                       var Subscriptondelete = SC.lstSubScriptionHistory.filter(function (s) { return s.IsDelete == false });
                       if (Subscriptondelete.length != 0)
                       { SC.SubscriptionAction = true; }
                       var index = SC.lstSubScriptionHistory.length - 1;
                       var Name = SC.lstSubScriptionHistory.filter(function (s) { return s.SubScriptionId })[index].Name;
                       if (Name == 'Trial') {

                       }
                       else {

                           SC.PlanCode = SC.lstSubScriptionHistory.filter(function (s) { return s.SubScriptionId })[index].Plan_Code;
                           SC.CustomerId = SC.lstSubScriptionHistory.filter(function (s) { return s.SubScriptionId })[index].CustomerId;
                       }
                   }
                   else {
                       SC.SubscriptionAction = false;
                   }
               }
               else {
                   swal("ERROR", MessageService.responseError, "error");
               }
               $scope.showLoader = false;
           }).error(function (daya, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           })
    }

    SC.ViewSubscription = function () {
        if (SC.lstSubScriptionHistory.length > 0) {
            if (SC.PlanCode != '' && SC.CustomerId != '') {
                window.location = '#/SubscriptionPlan/' + SC.CustomerId + '/' + SC.PlanCode;
                window.location.reload();
            }
            else {
                window.location = '#/SubscriptionPlan';
                window.location.reload();
            }
        }
        else {
            window.location = '#/SubscriptionPlan/-1';
            window.location.reload();
        }
    }

    SC.UpdateSubscription = function (Subscriptionpayment) {
        $scope.showLoader = true;
        SC.Payment.PaymentMode= Subscriptionpayment.PaymentMode;
        SC.Payment.FirstName= Subscriptionpayment.FirstName;
        SC.Payment.CVV= Subscriptionpayment.CVV;
        SC.Payment.ExpireDate= Subscriptionpayment.ExpireDate;
        SC.Payment.LastDigits= Subscriptionpayment.LastDigits;
        SC.Payment.AutoRenewal= Subscriptionpayment.AutoRenewal;
        SC.Payment.SubscriptionId= SC.SubscriptionId;
        SC.Payment.PlanCode=SC.PlanCode;
        subscriptionService.UpdateSubscription(SC.Payment).
           success(function (response) {
               if (response.Status == true) {
                   SC.OnLoadSubScriptionHistory();
                   $scope.showLoader = false;
               }
           }).error(function (daya, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           })
    }

    SC.CancelSubscription = function (SubscriptionId, PlanCode) {
        $scope.showLoader = true;
        subscriptionService.CancelSubscription(SubscriptionId, PlanCode).
           success(function (response) {
               if (response.Status == true) {
                   window.location.reload();
                   $scope.showLoader = false;
               }
           }).error(function (daya, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           })
    }

    SC.changepymntMethod = function (SubScription)
    {
        SC.SubscriptionId = SubScription.SubscriptionId;
        SC.PlanCode = SubScription.Plan_Code;
        SC.Payment.FirstName = $rootScope.userData.Name;
        SC.Payment.AutoRenewal = SubScription.AutoCollect;
        $('#mdl_AddCardDetail').modal("show");
    }


    SC.changeExpireDate = function (ExpireDate) {
        var CurrentMonth = new Date().getMonth() + 1;
        var Currentyear = new Date().getFullYear();

        var year = Currentyear.toString().substring(2, 4);;
        var ExpireYear = ExpireDate.toString().substring(2, 4);
        var ExpireMonth = ExpireDate.toString().substring(0, 2);
        $scope.ExpiryMonth = ExpireMonth;
        $scope.ExpiryYear = "20" + ExpireYear;

        if (parseInt(ExpireYear) > parseInt(year)) {
            if (ExpireMonth > 12) {
                $scope.ExpireDateError = false;
                $scope.Payment.ExpireDate = '';
            }
            else {
                $scope.ExpireDateError = true;
            }
        }
        else if (parseInt(ExpireYear) < parseInt(year) && parseInt(ExpireMonth) > parseInt(CurrentMonth)) {
            $scope.ExpireDateError = false;
            $scope.Payment.ExpireDate = '';
        }
        else {
            if (parseInt(ExpireMonth) > parseInt(CurrentMonth)) {
                if (ExpireMonth > 12) {
                    $scope.ExpireDateError = false;
                    $scope.Payment.ExpireDate = '';
                }
                else {
                    $scope.ExpireDateError = true;
                }
                // $scope.ExpireDateError = true;
            } else {
                if (ExpireMonth > 12) {
                    $scope.ExpireDateError = false;
                    $scope.Payment.ExpireDate = '';
                }
                else {
                    $scope.ExpireDateError = true;
                }
            }
        }
    }
}