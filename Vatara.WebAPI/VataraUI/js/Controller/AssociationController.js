﻿
vataraApp.controller('AssociationController', AssociationController);
AssociationController.$inject = ['$scope', '$window', 'masterdataService', 'AssociationService', 'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService'];
function AssociationController($scope, $window, masterdataService, AssociationService, pagingService, $filter, $routeParams, $location, $rootScope, commonService) {
    var ASSC = this;

    ASSC.associationEntryMode = 'Create';
    ASSC.lstAssociation = [];
    ASSC.country = [];
    ASSC.state = [];
    ASSC.county = [];
    ASSC.city = [];
    ASSC.Photos = [];

    ASSC.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    ASSC.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    ASSC.zipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    ASSC.pZipValid = true;
    ASSC.isEmailExist = false;

    ASSC.mdlAssociation = {
        id: 0,
        pmid: $rootScope.userData.ManagerDetail.pId,
        hoaName: '',
        personFirstName: '',
        personLastName: '',
        addressId: 0,
        phone: '',
        email: '',
        fax: '',
        fbUrl: '',
        twitterUrl: '',
        creatorId: $rootScope.userData.personId
    };

    ASSC.mdlAddress = {

        id: 0,
        mailBoxNo: '',
        line1: '',
        line2: '',
        countryId: 0,
        country: '',
        stateId: 0,
        state: '',
        cityId: 0,
        city: '',
        countyId: 0,
        county: '',
        zipcode: ''
    };

    ASSC.mdlAssociationRegistration = {
        mdlAssociation: ASSC.mdlAssociation,
        mdlAddress: ASSC.mdlAddress
    }


    ASSC.LoadAssociation = function () {
        ASSC.GetAllAssocians(function (data) { });
    };

    ASSC.LoadCreateAssociation = function () {

        if (!angular.isUndefined($routeParams.associationId)) {
            ASSC.associationEntryMode = 'Edit';
            $scope.pId = $routeParams.associationId;
            ASSC.GetAssociansDetailByAssociansId($routeParams.associationId, function (data) {
                ASSC.BindAssociationData(data[0]);
                ASSC.GetAssociationPhoto();
            });
        }
        else {

            ASSC.GetCountry(function (data) {
                ASSC.GetStates(function (data) {

                });
            });
        }

    };

    ASSC.BindAssociationData = function (data) {
        ASSC.mdlAssociationRegistration.mdlAssociation.id = data.id;
        ASSC.mdlAssociationRegistration.mdlAssociation.pmid = $rootScope.userData.ManagerDetail.pId;
        ASSC.mdlAssociationRegistration.mdlAssociation.hoaName = data.hoaName;
        ASSC.mdlAssociationRegistration.mdlAssociation.personFirstName = data.personFirstName;
        ASSC.mdlAssociationRegistration.mdlAssociation.personLastName = data.personLastName;
        ASSC.mdlAssociationRegistration.mdlAssociation.addressId = data.addressId;
        ASSC.mdlAssociationRegistration.mdlAssociation.phone = data.phone;
        ASSC.mdlAssociationRegistration.mdlAssociation.email = data.email;
        ASSC.mdlAssociationRegistration.mdlAssociation.fax = data.fax;
        ASSC.mdlAssociationRegistration.mdlAssociation.fbUrl = data.fbUrl;
        ASSC.mdlAssociationRegistration.mdlAssociation.twitterUrl = data.twitterUrl;

        ASSC.mdlAssociationRegistration.mdlAddress.zipcode = data.zipcode;
        ASSC.mdlAssociationRegistration.mdlAddress.line1 = data.address;
        ASSC.mdlAssociationRegistration.mdlAddress.id = data.addressId;
        ASSC.GetCountry(function (CountryData) {
            ASSC.GetStates(function (StateData) {

                ASSC.mdlCountryState.selectedCountry = CountryData.filter(function (c) { return c.Id == data.countryId })[0];
                ASSC.mdlCountryState.selectedState = StateData.filter(function (s) { return s.Id == data.stateId })[0];
                ASSC.GetCountyByState(data.stateId, data.countyId, function (CountyData) {

                    ASSC.GetCityByStateOrCounty(data.stateId, data.countyId, function (CityData) {
                        ASSC.mdlCountryState.selectedCity = CityData.filter(function (c) { return c.Id == data.cityId })[0];
                    });

                });
            });
        });
    };


    ASSC.GetAllAssocians = function (callback) {
        $scope.showLoader = true;
        var PMID = $rootScope.userData.ManagerDetail.pId;
        AssociationService.getAllAssocians(PMID).
                    success(function (data) {
                        $scope.showLoader = false;                      
                        ASSC.lstAssociation = data.Data;
                        callback(data.Data);
                    }).error(function (data, status) {
                        if (status == 401) {
                            commonService.SessionOut();
                        }
                        else {
                            swal("ERROR", MessageService.serverGiveError, "error");
                        }
                        $scope.showLoader = false;

                    }).finally(function (data) {
                        $scope.showLoader = false;
                    });
    };

    ASSC.GetAssociansDetailByAssociansId = function (associansId, callback) {
        $scope.showLoader = true;
        AssociationService.getAssociansDetailByAssociansId(associansId).
                    success(function (data) {
                        $scope.showLoader = false;
                        callback(data.Data);
                    }).error(function (data, status) {
                        if (status == 401) {
                            commonService.SessionOut();
                        }
                        else {
                            swal("ERROR", MessageService.serverGiveError, "error");
                        }
                        $scope.showLoader = false;

                    }).finally(function (data) {
                        $scope.showLoader = false;
                    });
    };

  
    ASSC.SaveAssociation = function (frmCreateAssociation) {
        ASSC.mdlAssociationRegistration.mdlAddress.countryId = ASSC.mdlCountryState.selectedCountry.Id;
        ASSC.mdlAssociationRegistration.mdlAddress.stateId = ASSC.mdlCountryState.selectedState.Id;
        if (ASSC.mdlCountryState.selectedCounty != undefined) {
            ASSC.mdlAssociationRegistration.mdlAddress.countyId = ASSC.mdlCountryState.selectedCounty.id;
        }
        ASSC.mdlAssociationRegistration.mdlAddress.cityId = ASSC.mdlCountryState.selectedCity.Id;
        $scope.showLoader = true;
        AssociationService.createNewAssociation(ASSC.mdlAssociationRegistration).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    $scope.pId = response.Message;

                    if (!angular.isUndefined(ASSC.Photos.name)) {
                        ASSC.savePhotos(function (data) {
                            swal("SUCCESS", "Data saved successfully.", "success");
                        });
                    }
                    else {
                        swal("SUCCESS", "Data saved successfully.", "success");
                    }
                    frmCreateAssociation.$setPristine();
                    frmCreateAssociation.$setUntouched();
                    $location.path("/association");                    
                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    ASSC.UpdateAssociation = function (frmCreateAssociation) {

        ASSC.mdlAssociationRegistration.mdlAddress.countryId = ASSC.mdlCountryState.selectedCountry.Id;
        ASSC.mdlAssociationRegistration.mdlAddress.stateId = ASSC.mdlCountryState.selectedState.Id;
        if (ASSC.mdlCountryState.selectedCounty != undefined) {
            ASSC.mdlAssociationRegistration.mdlAddress.countyId = ASSC.mdlCountryState.selectedCounty.id;
        }
        ASSC.mdlAssociationRegistration.mdlAddress.cityId = ASSC.mdlCountryState.selectedCity.Id;
        ASSC.mdlAssociationRegistration.mdlAssociation.creatorId = $rootScope.userData.personId;
        
        $scope.showLoader = true;
        AssociationService.updateAssociation(ASSC.mdlAssociationRegistration).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {

                    if (!angular.isUndefined(ASSC.Photos.name)) {
                        ASSC.savePhotos(function (data) {
                            swal("SUCCESS", "Data saved successfully.", "success");
                        });
                    }
                    else {
                        swal("SUCCESS", "Data saved successfully.", "success");
                    }
                   // swal("SUCCESS", "Data updated successfully.", "success");
                    frmCreateAssociation.$setPristine();
                    frmCreateAssociation.$setUntouched();
                    $location.path("/association");                   
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    $scope.fileSelected = function (element) {
        var myFileSelected = element.files[0];

        ASSC.Photos = myFileSelected;
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('divimage');
            if (output != null)
                output.style.backgroundImage = 'url(' + reader.result + ')';
            $scope.$apply();
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    ASSC.savePhotos = function (callback) {
        var fd = new FormData();
        fd.append("file", ASSC.Photos);
        fd.append("associationId", $scope.pId);
        fd.append("moduleid", 'ASSOCIATION');
        fd.append("seqNo", 0);
        fd.append("filename", 'image0');
        fd.append("filedesc", '');
        fd.append("filetags", 'Photo, ' + $scope.pId);
        AssociationService.saveFiles(fd, '/api/associationphoto', function (status, resp) {
            if (status) {
                callback(status)
                //swal("SUCCESS", "Photo saved successfully", "success");
            }
            else {
                swal("ERROR", "Unable to save photos.", "error");
            }
        });

    }

    ASSC.GetAssociationPhoto = function () {

        AssociationService.getAssociationPhotos($scope.pId, function (result, data) {
            if (result) {
                for (let index = 0; index < data.length; index++) {
                    var fd = new FormData();
                    fd.append("file", ASSC.Photos);
                    fd.append("associationId", $scope.pId);
                    fd.append("seqNo", index);
                    fd.append("filename", 'image' + index);
                    fd.append("filedesc", '');
                    fd.append("filetags", 'Photo, ' + $scope.pId);
                    ASSC.Photos = fd;                   
                    output = document.getElementById('divimage');
                    if (output != null) {
                        var random = Math.floor(100000 + Math.random() * 900000);
                        output.style.backgroundImage = 'url(/Uploads/Contacts/Association/Photos/' + $scope.pId + '-image' + index.toString() + '.png?cb=' + random + ')';
                    }                   
                }
            }
            else {
                swal("ERROR", "Unable to fetch photo.", "error");
            }
        });
    };

    ASSC.EmailExist = function (email) {
        if (email == undefined || email == '') {
            return;
        }
        $scope.showLoader = true;
        var associationId = 0;
        if (ASSC.associationEntryMode == 'Edit') {
            associationId = $routeParams.associationId;
        }
        AssociationService.EmailExist(email, associationId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   ASSC.isEmailExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    ASSC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    ASSC.GetCountry = function (callback) {

        $scope.showLoader = true;
        commonService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  ASSC.country = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    ASSC.GetStates = function (callback) {
        $scope.showLoader = true;
        commonService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  ASSC.state = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    ASSC.GetCountyByState = function (stateId, editId, callback) {
        $scope.showLoader = true;
        commonService.getCountyByState(stateId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  ASSC.county = angular.copy(response.Data);

                  if (editId != undefined) {

                      var conty = ASSC.county.filter(function (c) { return c.id == editId });
                      ASSC.mdlCountryState.selectedCounty = conty[0];
                  }
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    ASSC.GetCountyByStateId = function (modal) {

        ASSC.mdlCountryState.selectedCounty = undefined;
        ASSC.mdlCountryState.selectedCity = undefined;
        ASSC.ChangeZip();
        ASSC.GetCountyByState(modal.Id, undefined, function (data) {

            var countyId = 0;
            ASSC.GetCityByStateOrCounty(modal.Id, countyId, function (data) {
                ASSC.city = angular.copy(data);
            });
        });
    }

    ASSC.ChangeCity = function (modal) {
        ASSC.mdlCountryState.selectedCity = undefined;
        ASSC.ChangeZip();
        ASSC.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) {

            ASSC.city = angular.copy(data);
        });
    };

    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        ASSC.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;
        ASSC.GetCityByStateOrCounty(ASSC.mdlCountryState.selectedState.Id, countyId, function (data) {
            ASSC.city = angular.copy(data);
        });
    };

    ASSC.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        commonService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;

                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    ASSC.ChangeZip = function () {
        ASSC.mdlAssociationRegistration.mdlAddress.zipcode = '';
        ASSC.pZipValid = true;
    };

    ASSC.ValidateZipCode = function (model, ele) {

        if (model.length == 5 || model.length == 10) {
            if (ASSC.checkZip(model)) {
                ASSC.ValidateZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");              
            }
        }
    }

    ASSC.checkZip = function (value) {
        return (ASSC.zipPattern).test(value);
    };

    ASSC.ValidateZipCodeUsingAPI = function (ZipCode, ele) {
        var city = '';
        var County = '';
        var Country = '';
        //  ASSC.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name;
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "");
                        }

                    });
                });
                // var enteredCounty = ASSC.mdlCountryState.selectedCounty.name.replace(" County", "").toUpperCase().trim();

                if (ASSC.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.toUpperCase().trim()) {
                    if (ASSC.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {
                        ASSC.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        ASSC.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    ASSC.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    ASSC.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    ASSC.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }


    $scope.removeProfileImage = function () {
        AssociationService.RemoveFile($routeParams.associationId, function (result, data) {
            if (result) {
                $window.location.reload();
            }
            else {
                swal("ERROR", "Unable to save image.", "error");
            }
        });
    }
}

//AssociationController