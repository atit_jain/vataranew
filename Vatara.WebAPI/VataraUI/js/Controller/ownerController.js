﻿vataraApp.controller('ownerController', ownerController);
ownerController.$inject = ['$scope', 'pagingService', 'ownerService', '$filter', '$rootScope', 'commonService', '$timeout'];
function ownerController($scope, pagingService, ownerService, $filter, $rootScope, commonService, $timeout) {

    var OC = this;
    OC.personId = $rootScope.userData.personId;
    OC.ownerPOID = $rootScope.userData.poid;

    OC.ownerData = [];
    OC.DocsArray = [];
    OC.Property = '';
    OC.lstPropertByOwner = [];

    $scope.tab1Class = "";
    $scope.tab2Class = "active2";
    $scope.tab3Class = "";
    $scope.tab4Class = "";
    $scope.tab5Class = "";
    $scope.tab6Class = "";
    $scope.tab7Class = "";

   // $rootScope.ShowInvoiceBillModel = false;
    $rootScope.ActiveTabName = 'Invoices';
    $rootScope.ShowInvoiceBillModel = true;
    
    OC.PageLoad = function()
    {
        OC.OwnerDetails();
        OC.GetPropertByOwner();
    }

    OC.OwnerDetails = function () {
        $scope.showLoader = true;
        ownerService.getOwnerDetails(OC.ownerPOID, OC.personId).
         success(function (response) {
             if (response.Status == true) {
                 OC.ownerData = angular.copy(response.Data);
                 output = document.getElementById('divimage2');
                 if (output != null) {
                     var random = Math.floor(100000 + Math.random() * 900000);
                     output.style.backgroundImage = 'url(../Uploads/Person/Photos/' + OC.ownerData.PersonInfo.pImage + '?cb=' + random + ')';
                 }
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }


    OC.ActiveTab = function (tabName) {

        if (tabName.toUpperCase() == 'TRANSACTIONS' || tabName.toUpperCase() == 'ONLINESTATEMENT') {
            $rootScope.ShowInvoiceBillModel = false;
        }
        else {
            $rootScope.ShowInvoiceBillModel = true;
        }
        $rootScope.ActiveTabName = tabName;

        //if (tabName.toUpperCase() == 'ONLINESTATEMENT') {
        //    $rootScope.ShowInvoiceBillModel = false;            
        //    $rootScope.ActiveTabName = 'ONLINESTATEMENT';
        //}
        //else if (tabName.toUpperCase() == 'TRANSACTIONS')
        //{
        //    $rootScope.ShowInvoiceBillModel = false;
        //    $rootScope.ActiveTabName = 'TRANSACTIONS';
        //}
        //else {
        //    $rootScope.ShowInvoiceBillModel = true;
        //    $rootScope.ActiveTabName = '';
        //}        
    }


    OC.GetPropertByOwner = function () {

        $scope.showLoader = true;
        ownerService.getPropertByOwner(OC.personId).
           success(function (response) {
               if (response.Status == true) {
                   // console.log('response.Data', response.Data);
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       OC.lstPropertByOwner = response.Data;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    OC.GetPropertyDoc = function (propertyId) {
        $scope.showLoader = true;
        ownerService.getPropertyDoc(propertyId, function (result, data) {
            if (result) {
                $scope.showLoader = false;
                OC.DocsArray = angular.copy(data);              
            }
            else if (data == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
        });
    }

    OC.OpenUrl = function (url) {
        window.open("/Uploads/Properties/Documents/" + url, '_blank', 'height=800,width=600');
    }

    OC.RegistrationModel = function () {
        $('#mdlOwnerProfile').modal('show');
    }

    OC.MerchantRegistrationModel = function () {
        $('#mdlOwnerMerchantProfile').modal('show');
    }

    OC.setTabClass = function (tabNum) {
        $scope.tab = tabNum;
        $scope.tab1Class = "";
        $scope.tab2Class = "";
        $scope.tab3Class = "";
        $scope.tab4Class = "";
        $scope.tab5Class = "";
        $scope.tab6Class = "";
        $scope.tab7Class = "";

        if (tabNum == 1) {
            $scope.tab1Class = "active2";
            $scope.showLoader = false;
        }
        else if (tabNum == 2) {
            $scope.tab2Class = "active2";
        }
        else if (tabNum == 3) {
            $scope.tab3Class = "active2";
        }
        else if (tabNum == 4) {
            $scope.tab4Class = "active2";
        }
        else if (tabNum == 5) {
            $scope.tab5Class = "active2";
        }
        else if (tabNum == 6) {
            $scope.tab6Class = "active2";
        }
        else if (tabNum == 7) {
            $scope.tab7Class = "active2";
        }
    };

}