﻿vataraApp.controller('eventTrackerController', eventTrackerController);
eventTrackerController.$inject = ['$scope', 'eventTrackerService', '$routeParams', 'pagingService', '$location', '$filter', '$rootScope', 'commonService'];
function eventTrackerController($scope, eventTrackerService, $routeParams, pagingService, $location, $filter, $rootScope, commonService) {

    var ETC = this;
    ETC.lstPropertByManager = [];

    ETC.personType = $rootScope.userData.personTypeId;
    ETC.managerid = $rootScope.userData.personId;
    ETC.PMID = $rootScope.userData.ManagerDetail.pId;
    ETC.ownerPOID = $rootScope.userData.poid;
    ETC.personId = ETC.managerid;

    ETC.mdlSelectedProperty = '';
    ETC.mdlSelectedPropertyForAddNewEvent = '';
    ETC.property = [];
    ETC.event_OrderBy = "";

    ETC.lstEvent = [];
    ETC.lstEvent_Filtered = [];
    ETC.pageService = pagingService;
  
    ETC.itemsPerPageArr = ETC.pageService.itemsPerPageArr;
    ETC.ItemCountPerPage = _ItemsPerPage;  

    ETC.currentPage_lstEvent = 0;
    ETC.lstEventType = [];
    ETC.arrEventType = ['All', 'Violation', 'Incident', 'Other'];
    ETC.mdlEventType = ['Violation', 'Incident', 'Other'];
    ETC.eventDate = new Date();
    ETC.showOtherEvent = false;
    ETC.selectedEventType = '';
    ETC.isArchive = 0;
    ETC.showArchive = false;
    ETC.EventType = ETC.arrEventType[0];
    ETC.isSubTab = false;
   

    ETC.MdlEvent = {
        eventID: 0,
        eventType: '',
        eventDate: new Date(),
        propertyID: 0,
        description: '',
        creatorUserId: ETC.managerid,
        isDeleted: false
    }

    ETC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    };

    ETC.CloseOtherTypeInputBox = function () {

        ETC.MdlEvent.eventType = '';
        ETC.showOtherEvent = false;
        ETC.selectedEventType = ''
    };

    ETC.EventTypeChanged = function (EventType) {

        if (EventType == "Other") {
            ETC.showOtherEvent = true;
            ETC.MdlEvent.eventType = '';
        }
        else {
            $(".showOtherEvent").hide();
            ETC.MdlEvent.eventType = EventType;
            ETC.showOtherEvent = false;
        }
    };

    //Get Property in load Event
    ETC.LoadProperty = function () {

        $scope.showLoader = true;
        eventTrackerService.getPropertyByPerson(ETC.ownerPOID, ETC.personId, ETC.personType).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    ETC.lstPropertByManager = response.Data;

                    
                    if ($location.url().match("propertyProfile/")) {
                        if (!angular.isUndefined($routeParams.PropertyNum)) {
                            ETC.isSubTab = true;
                            ETC.mdlSelectedProperty = parseInt($routeParams.PropertyNum);
                            var showArchive = false;
                            ETC.GetEventList(ETC.mdlSelectedProperty, showArchive, function (data) {
                                ETC.GetEventListByEventType(ETC.EventType);
                            });
                        }
                    }
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    ETC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    ETC.AddEvent = function () {
        ETC.MdlEvent = {
            eventID: 0,
            eventType: '',
            eventDate: new Date(),
            propertyID: 0,
            description: '',
            creatorUserId: ETC.managerid,
            isDeleted: false
        }
        ETC.selectedEventType = '';
        ETC.showOtherEvent = false;
        ETC.mdlSelectedPropertyForAddNewEvent = '';
        $("#mdl_AddEvent").modal('show');
    }

    ETC.SaveNewEvent = function (frmAddNewEvent) {
        ETC.MdlEvent.propertyID = ETC.mdlSelectedPropertyForAddNewEvent.propertyId;
        $scope.showLoader = true;
        eventTrackerService.saveNewEvent(ETC.MdlEvent).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    var returnModel = response.Data;
                    if (ETC.MdlEvent.eventID > 0) {
                        swal("SUCCESS", "Event Updated successfully.", "success");

                        var objIndexlstEvent = ETC.lstEvent.findIndex(function (obj) { return obj.eventID == returnModel.eventID });
                        ETC.lstEvent[objIndexlstEvent].description = returnModel.description;
                        ETC.lstEvent[objIndexlstEvent].eventDate = returnModel.eventDate;
                        ETC.lstEvent[objIndexlstEvent].eventType = returnModel.eventType;

                        var objIndexlstEvent_Filtered = ETC.lstEvent_Filtered.findIndex(function (obj) { return obj.eventID == returnModel.eventID });
                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].description = returnModel.description;
                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].eventDate = returnModel.eventDate;
                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].eventType = returnModel.eventType;

                        ETC.GetEventListByEventType(ETC.EventType);

                    } else {
                        swal("SUCCESS", "Event saved successfully.", "success");
                        if (ETC.mdlSelectedProperty.propertyId == returnModel.propertyID) {
                            if (ETC.EventType == returnModel.eventType) {
                                ETC.lstEvent_Filtered.push(returnModel);
                            }
                            ETC.lstEvent.push(returnModel);
                            ETC.lstEvent_Filtered.push(returnModel);
                        }
                    }
                    ETC.CloseMdl_AddNewEvent(frmAddNewEvent);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    ETC.CloseMdl_AddNewEvent = function (frmAddNewEvent) {

        ETC.MdlEvent = {
            eventID: 0,
            eventType: '',
            eventDate: new Date(),
            propertyID: 0,
            description: '',
            creatorUserId: ETC.managerid,
            isDeleted: false
        }
        ETC.selectedEventType = '';
        ETC.showOtherEvent = false;
        ETC.mdlSelectedPropertyForAddNewEvent = '';
        frmAddNewEvent.$setPristine();
        $("#mdl_AddEvent").modal("hide");
    }

    ETC.GetEventListByArchiveStatus = function (showArchive, eventType) {
        if (ETC.mdlSelectedProperty.propertyId === undefined) {
            var PropertyId = ETC.mdlSelectedProperty;
        } else {
            var PropertyId = ETC.mdlSelectedProperty.propertyId;
        }
        ETC.GetEventList(PropertyId, showArchive, function (data) {
            ETC.GetEventListByEventType(eventType);
        });
    };

    ETC.GetEventListByProperty = function (propertyId, eventType) {
        
        if (ETC.isArchive == true) {
            ETC.showArchive = true;
        }
        else {
            ETC.showArchive = false;
        }
        ETC.GetEventList(propertyId, ETC.showArchive, function (data) {
            ETC.GetEventListByEventType(eventType);
        });
    };

    ETC.GetEventList = function (propertyId, showArchive, callback) {
        $scope.showLoader = true;
        eventTrackerService.getEventList(propertyId, showArchive).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    ETC.lstEvent = angular.copy(response.Data);
                    ETC.lstEvent_Filtered = angular.copy(response.Data);
                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                $scope.showLoader = false;
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    ETC.GetEventListByEventType = function (eventType) {
        if (eventType == "All") {
            ETC.lstEvent_Filtered = angular.copy(ETC.lstEvent);
        }
        else if (eventType == "Other") {
            ETC.lstEvent_Filtered = angular.copy(ETC.lstEvent.filter(function (event) { return event.eventType != "Violation" && event.eventType != "Incident"}));
        }
        else {
            ETC.lstEvent_Filtered = angular.copy(ETC.lstEvent.filter(function (event) { return event.eventType == eventType}));
        }
    };

    ETC.EditEventTracker = function (eventID) {
        ETC.MdlEvent = angular.copy(ETC.lstEvent.filter(function (event) { return event.eventID == eventID})[0]);
        ETC.selectedEventType = ETC.MdlEvent.eventType;
        if (ETC.selectedEventType != "Violation" && ETC.selectedEventType != "Incident") {
            ETC.showOtherEvent = true;
        }
        ETC.mdlSelectedPropertyForAddNewEvent = ETC.lstPropertByManager.filter(function (prop) { return prop.propertyId == ETC.MdlEvent.propertyID})[0];
        $("#mdl_AddEvent").modal('show');
    };

    ETC.ArchiveEvent = function (eventID, eventType, eventDate) {
        swal({
            title: "Are you sure?",
            text: "You want to Archive this Event.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(function(willArchive) {
                if (willArchive) {
                    $scope.showLoader = true;
                    ETC.MdlEvent = {
                        eventID: eventID,
                        eventType: eventType,
                        eventDate: eventDate,
                        propertyID: 0,
                        description: '',
                        creatorUserId: ETC.managerid,
                        isDeleted: false
                    };
                    $scope.showLoader = true;
                    eventTrackerService.archiveEvent(ETC.MdlEvent).
                        success(function (response) {
                            if (response.Status == true) {
                                $scope.showLoader = false;
                                var returnModel = response.Data;
                                if (ETC.MdlEvent.eventID > 0) {
                                    swal("SUCCESS", "Event Archive successfully !", "success");
                                    if (ETC.isArchive == true) {
                                        var objIndexlstEvent = ETC.lstEvent.findIndex(function (obj) { return obj.eventID == returnModel.eventID});
                                        ETC.lstEvent[objIndexlstEvent].description = returnModel.description;
                                        ETC.lstEvent[objIndexlstEvent].eventDate = returnModel.eventDate;
                                        ETC.lstEvent[objIndexlstEvent].eventType = returnModel.eventType;
                                        ETC.lstEvent[objIndexlstEvent].isDeleted = true;

                                        var objIndexlstEvent_Filtered = ETC.lstEvent_Filtered.findIndex(function (obj) { return  obj.eventID == returnModel.eventID});
                                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].description = returnModel.description;
                                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].eventDate = returnModel.eventDate;
                                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].eventType = returnModel.eventType;
                                        ETC.lstEvent_Filtered[objIndexlstEvent_Filtered].isDeleted = true;
                                    }
                                    else {
                                        var objIndexlstEvent = ETC.lstEvent.findIndex(function (obj) { return obj.eventID == returnModel.eventID});
                                        ETC.lstEvent.splice(objIndexlstEvent);

                                        var objIndexlstEvent_Filtered = ETC.lstEvent_Filtered.findIndex(function (obj) { return obj.eventID == returnModel.eventID});
                                        ETC.lstEvent_Filtered.splice(objIndexlstEvent_Filtered);
                                    }
                                }
                            }
                            else {
                                swal("ERROR", response.Message, "error");
                            }
                        }).error(function (data, status) {
                            if (status == 401) {
                                commonService.SessionOut();
                            }
                            else {
                                swal("ERROR", MessageService.serverGiveError, "error");
                            }
                            $scope.showLoader = false;
                        }).finally(function (data) {
                            $scope.showLoader = false;
                        });
                } else {
                }
            });
    }

    ETC.DeleteEvent = function (eventID, eventType, eventDate) {
        swal({
            title: "Are you sure?",
            text: "You want to Delete this Event.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(function(willDelete) {
                if (willDelete) {
                    $scope.showLoader = true;
                    ETC.MdlEvent = {
                        eventID: eventID,
                        eventType: eventType,
                        eventDate: eventDate,
                        propertyID: 0,
                        description: '',
                        creatorUserId: ETC.managerid,
                        isDeleted: false
                    };
                    $scope.showLoader = true;
                    eventTrackerService.deleteEvent(ETC.MdlEvent).
                        success(function (response) {
                            if (response.Status == true) {
                                var returnModel = angular.copy(response.Data);

                                var objIndexlstEvent = ETC.lstEvent.findIndex(function (obj) { return obj.eventID == returnModel.eventID});
                                ETC.lstEvent.splice(objIndexlstEvent);

                                var objIndexlstEvent_Filtered = ETC.lstEvent_Filtered.findIndex(function (obj) { return obj.eventID == returnModel.eventID});
                                ETC.lstEvent_Filtered.splice(objIndexlstEvent_Filtered);

                                $scope.showLoader = false;
                                swal("SUCCESS", "Event Delete successfully !", "success");
                            }
                            else {
                                swal("ERROR", response.Message, "error");
                            }
                        }).error(function (data, status) {
                            if (status == 401) {
                                commonService.SessionOut();
                            }
                            else {
                                swal("ERROR", MessageService.serverGiveError, "error");
                            }
                            $scope.showLoader = false;
                        }).finally(function (data) {
                            $scope.showLoader = false;
                        });
                } else {
                }
            });
    }


}