﻿vataraApp.controller('VendorController', VendorController);
VendorController.$inject = ['$scope', '$window', 'VendorService', 'masterdataService', 'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService'];
function VendorController($scope, $window, VendorService, masterdataService, pagingService, $filter, $routeParams, $location, $rootScope, commonService) {
    var VC = this;
    VC.selection = [];
    VC.country = [];
    VC.state = [];
    VC.county = [];
    VC.city = [];
    VC.vendorServiceCategory = [];
    VC.lstVendor = [];
    VC.Photos = [];
    VC.showthis = true;
    VC.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    VC.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    VC.zipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    VC.pZipValid = true;
    VC.isEmailExist = false;
    VC.vendorEntryMode = 'Create';

    VC.mdlVendor = {
        vendorId: 0,
        pmid: $rootScope.userData.ManagerDetail.pId,
        companyName: '',
        firstName: '',
        lastName: '',
        addressId: 0,
        phone: '',
        email: '',
        fax: '',
        fbUrl: '',
        twitterUrl: '',
        creatorUserId: $rootScope.userData.personId
    };
    
    VC.mdlAddress = {

        id: 0,
        mailBoxNo: '',
        line1: '',
        line2: '',
        countryId: 0,
        country: '',
        stateId: 0,
        state: '',
        cityId: 0,
        city: '',
        countyId: 0,
        county: '',
        zipcode: ''
    };

    VC.mdlVendorRegistration = {
        mdlVendor: VC.mdlVendor,
        mdlAddress: VC.mdlAddress,
        mdlServiceCategories: []
    }

    VC.LoadVendor = function () {
        VC.GetAllVendors(function (data) { });
    }

    VC.GetAllVendors = function (callback) {
        $scope.showLoader = true;
        var PMID = $rootScope.userData.ManagerDetail.pId;
        VendorService.getAllVendors(PMID).
                    success(function (data) {
                        $scope.showLoader = false;
                        VC.lstVendor = data.Data;
                        callback(data.Data);
                    }).error(function (data, status) {
                        if (status == 401) {
                            commonService.SessionOut();
                        }
                        else {
                            swal("ERROR", MessageService.serverGiveError, "error");
                        }
                        $scope.showLoader = false;

                    }).finally(function (data) {
                        $scope.showLoader = false;
                    });
    };

    VC.LoadCreateVendor = function () {

        if (!angular.isUndefined($routeParams.vendorId)) {
            VC.vendorEntryMode = 'Edit';
            $scope.pId = $routeParams.vendorId;
            VC.GetVendorDetailByVendorId($routeParams.vendorId, function (data) {
                VC.BindVendorData(data[0]);
                VC.GetVendorPhoto();
            });
        }
        else {

            VC.mdl = {};
            if ($window.localStorage.getItem("AddNewVendorFromWorkOrder") != null) {               
                var mdl = JSON.parse($window.localStorage.getItem("AddNewVendorFromWorkOrder"));
                VC.mdl = angular.copy(mdl);
            }
            VC.GetVendorServiceCategoryMaster(function (data) { });
            VC.GetCountry(function (data) {
                VC.GetStates(function (data) {

                });
            });
        }
    };


    VC.CancelCreateVendor = function () {

        if ($window.localStorage.getItem("AddNewVendorFromWorkOrder") != null) {
            $location.path("/" + VC.mdl.returnUrl);
            VC.mdl = {};
            $window.localStorage.removeItem("AddNewVendorFromWorkOrder");
        }
        else {
            $location.path("/vendor");
        }
    }

    VC.BindVendorData = function (data) {      

        VC.mdlVendorRegistration.mdlVendor.vendorId = data.vendorId;
        VC.mdlVendorRegistration.mdlVendor.pmid = $rootScope.userData.ManagerDetail.pId;
        VC.mdlVendorRegistration.mdlVendor.companyName = data.companyName;
        VC.mdlVendorRegistration.mdlVendor.firstName = data.firstName;
        VC.mdlVendorRegistration.mdlVendor.lastName = data.lastName;
        VC.mdlVendorRegistration.mdlVendor.addressId = data.addressId;
        VC.mdlVendorRegistration.mdlVendor.phone = data.phone;
        VC.mdlVendorRegistration.mdlVendor.email = data.email;
        VC.mdlVendorRegistration.mdlVendor.fax = data.fax;
        VC.mdlVendorRegistration.mdlVendor.fbUrl = data.fbUrl;
        VC.mdlVendorRegistration.mdlVendor.twitterUrl = data.twitterUrl;
        VC.mdlVendorRegistration.mdlAddress.zipcode = data.zipcode;
        VC.mdlVendorRegistration.mdlAddress.line1 = data.address;
        VC.mdlVendorRegistration.mdlAddress.id = data.addressId;

        VC.GetVendorServiceCategoryMaster(function (data1) {
            var arr = data.vendorServiceIds.split(",");
            var arr2 = [];
            angular.forEach(arr, function (value, key) {
                var id = parseInt(value.trim());
                arr2.push(id);
            });
            VC.mdlVendorRegistration.mdlServiceCategories = arr2;
        });
        VC.GetCountry(function (CountryData) {
            VC.GetStates(function (StateData) {

                VC.mdlCountryState.selectedCountry = CountryData.filter(function (c) { return c.Id == data.countryId })[0];
                VC.mdlCountryState.selectedState = StateData.filter(function (s) { return s.Id == data.stateId })[0];
                //VC.mdlCountryState.selectedCounty = VC.state.filter(s => s.Id == data.stateId);
                //VC.mdlCountryState.selectedCity = VC.state.filter(s => s.Id == data.stateId);

                VC.GetCountyByState(data.stateId, data.countyId, function (CountyData) {

                    VC.GetCityByStateOrCounty(data.stateId, data.countyId, function (CityData) {
                        VC.mdlCountryState.selectedCity = CityData.filter(function (c) { return c.Id == data.cityId })[0];
                    });
                });
            });
        });
    };

    VC.GetVendorDetailByVendorId = function (vendorId, callback) {
        $scope.showLoader = true;
        VendorService.getVendorDetailByVendorId(vendorId).
                    success(function (data) {
                        $scope.showLoader = false;
                        callback(data.Data);
                    }).error(function (data, status) {
                        if (status == 401) {
                            commonService.SessionOut();
                        }
                        else {
                            swal("ERROR", MessageService.serverGiveError, "error");
                        }
                        $scope.showLoader = false;

                    }).finally(function (data) {
                        $scope.showLoader = false;
                    });
    };

    VC.ShowServicecategory = function (model) {
    };

    $scope.fileSelected = function (element) {

        var myFileSelected = element.files[0];
        VC.Photos = myFileSelected;
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('divimage');
            if (output != null)               
                output.style.backgroundImage = 'url(' + reader.result + ')';
            $scope.$apply();
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    VC.savePhotos = function (callback) {
       
        var fd = new FormData();
        fd.append("file", VC.Photos);
        fd.append("vendorId", $scope.pId);
        fd.append("moduleid", 'VENDOR');
        fd.append("seqNo", 0);
        fd.append("filename", 'image0');
        fd.append("filedesc", '');
        fd.append("filetags", 'Photo, ' + $scope.pId);
        VendorService.saveFiles(fd, '/api/vendorphoto', function (status, resp) {
            if (status) {
                callback(status);
               // swal("SUCCESS", "Photo saved successfully", "success");
            }
            else {
                swal("ERROR", "Unable to save image.", "error");
            }
        });
    }
    //VC.bgUrl = '';
    VC.GetVendorPhoto = function () {
        
            VendorService.getVendorphotos($scope.pId, function (result, data) {
                if (result) {

                    for (let index = 0; index < data.length; index++) {
                        var fd = new FormData();
                        fd.append("file", VC.Photos);
                        fd.append("vendorId", $scope.pId);
                        fd.append("seqNo", index);
                        fd.append("filename", 'image' + index);
                        fd.append("filedesc", '');
                        fd.append("filetags", 'Photo, ' + $scope.pId);
                        VC.Photos = fd;
                        //VC.bgUrl = '13-image0.png';
                       
                       // VC.bgUrl = '../Uploads/Contacts/Vendors/Photos/' + $scope.pId + '-image' + index.toString() + '.png';
                       // $("#divimage1").css('background-image', 'url(/Uploads/Contacts/Vendors/Photos/' + $scope.pId + '-image' + index.toString() + '.png)');                        
                        output = document.getElementById('divimage');
                        if (output != null) {                          
                            var random = Math.floor(100000 + Math.random() * 900000); //(new Date()).toString();
                            document.getElementById('divimage').style.backgroundImage = 'url(/Uploads/Contacts/Vendors/Photos/' + $scope.pId + '-image' + index.toString() + '.png?cb='+random+')';
                           
                        }                    
                    }
                }
                else {
                    swal("ERROR", "Unable to fetch photo.", "error");
                }
            });      
    };

    VC.SaveVendor = function (frmCreateVendor) {

        VC.mdlVendorRegistration.mdlAddress.countryId = VC.mdlCountryState.selectedCountry.Id;
        VC.mdlVendorRegistration.mdlAddress.stateId = VC.mdlCountryState.selectedState.Id;
        if (VC.mdlCountryState.selectedCounty != undefined) {
            VC.mdlVendorRegistration.mdlAddress.countyId = VC.mdlCountryState.selectedCounty.id;
        }
        VC.mdlVendorRegistration.mdlAddress.cityId = VC.mdlCountryState.selectedCity.Id;
        var arr = [];       
        angular.forEach(VC.mdlVendorRegistration.mdlServiceCategories, function (value, key) {
            var serCat = {
                serviceCategoryId: value
            }
            arr.push(serCat);
        });
        VC.mdlVendorRegistration.mdlServiceCategories = arr;     
        $scope.showLoader = true;
        VendorService.createNewVendor(VC.mdlVendorRegistration).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    $scope.pId = response.Message;                  

                    if (!angular.isUndefined(VC.Photos.name)) {
                        VC.savePhotos(function (data) {
                            swal("SUCCESS", "Data saved successfully.", "success");
                        });
                    }
                    else {
                        swal("SUCCESS", "Data saved successfully.", "success");
                    }                   
                    frmCreateVendor.$setPristine();
                    frmCreateVendor.$setUntouched();
                    VC.CancelCreateVendor();                    
                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };


    VC.UpdateVendor = function (frmCreateVendor) {
        VC.mdlVendorRegistration.mdlAddress.countryId = VC.mdlCountryState.selectedCountry.Id;
        VC.mdlVendorRegistration.mdlAddress.stateId = VC.mdlCountryState.selectedState.Id;
        if (VC.mdlCountryState.selectedCounty != undefined) {
            VC.mdlVendorRegistration.mdlAddress.countyId = VC.mdlCountryState.selectedCounty.id;
        }
        VC.mdlVendorRegistration.mdlAddress.cityId = VC.mdlCountryState.selectedCity.Id;

        var arr = [];
        arr = angular.copy(VC.mdlVendorRegistration.mdlServiceCategories);

        VC.mdlVendorRegistration.mdlServiceCategories = [];
        angular.forEach(arr, function (value, key) {

            var serCat = VC.vendorServiceCategory.filter(function (c) { return c.id == value });
            VC.mdlVendorRegistration.mdlServiceCategories.push(serCat[0]);          
        });
        // VC.mdlVendorRegistration.mdlServiceCategories = arr;
        VC.mdlVendorRegistration.mdlVendor.creatorUserId = $rootScope.userData.personId;    
        $scope.showLoader = true;
        VendorService.updateVendor(VC.mdlVendorRegistration).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {                    

                    if (!angular.isUndefined(VC.Photos.name)) {
                        VC.savePhotos(function (data) {
                            swal("SUCCESS", "Data saved successfully.", "success");
                        });
                    }
                    else {
                        swal("SUCCESS", "Data updated successfully.", "success");
                    }                   
                    frmCreateVendor.$setPristine();
                    frmCreateVendor.$setUntouched();                   
                    $location.path("/vendor");
                    // VC.ClearfrmCreateVendor();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };


    VC.GetVendorServiceCategoryMaster = function (callback) {
        $scope.showLoader = true;
        VendorService.getVendorServiceCategoryMaster().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  VC.vendorServiceCategory = response.Data;
                  callback(response.Data);
               //   console.log('VC.vendorServiceCategory', response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    };

    VC.EmailExist = function (email) {
        if (email == undefined || email == '') {
            return;
        }
        $scope.showLoader = true;
        var vendorId = 0;
        if (VC.vendorEntryMode == 'Edit') {
            vendorId = $routeParams.vendorId;
        }

        VendorService.EmailExist(email, vendorId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   VC.isEmailExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }


    VC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    VC.GetCountry = function (callback) {

        $scope.showLoader = true;
        commonService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  VC.country = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    VC.GetStates = function (callback) {
        $scope.showLoader = true;
        commonService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  VC.state = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    VC.GetCountyByState = function (stateId, editId, callback) {
        $scope.showLoader = true;
        commonService.getCountyByState(stateId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  VC.county = angular.copy(response.Data);

                  if (editId != undefined) {

                      var conty = VC.county.filter(function (c) { return c.id == editId });
                      VC.mdlCountryState.selectedCounty = conty[0];
                  }
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    VC.GetCountyByStateId = function (modal) {

        VC.mdlCountryState.selectedCounty = undefined;
        VC.mdlCountryState.selectedCity = undefined;
        VC.ChangeZip();
        VC.GetCountyByState(modal.Id, undefined, function (data) {

            var countyId = 0;
            VC.GetCityByStateOrCounty(modal.Id, countyId, function (data) {
                VC.city = angular.copy(data);
            });
        });
    }

    VC.ChangeCity = function (modal) {
        VC.mdlCountryState.selectedCity = undefined;
        VC.ChangeZip();
        VC.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) {

            VC.city = angular.copy(data);
        });
    };

    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        VC.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;
        VC.GetCityByStateOrCounty(VC.mdlCountryState.selectedState.Id, countyId, function (data) {
            VC.city = angular.copy(data);
        });
    };

    VC.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        commonService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;

                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    VC.ChangeZip = function () {
        VC.mdlVendorRegistration.mdlAddress.zipcode = '';
        VC.pZipValid = true;
    };

    VC.ValidateZipCode = function (model, ele) {
      //  console.log('ValidateZipCode model', model);

        if (model.length == 5 || model.length == 10) {
            if (VC.checkZip(model)) {
                VC.ValidateZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");
            }
        }
    }

    VC.checkZip = function (value) {
      //  console.log('zipPattern', VC.zipPattern);
        return (VC.zipPattern).test(value);
    };

    VC.ValidateZipCodeUsingAPI = function (ZipCode, ele) {

        var city = '';
        var County = '';
        var Country = '';
        //  VC.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name;
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "");
                        }
                    });
                });
                // var enteredCounty = VC.mdlCountryState.selectedCounty.name.replace(" County", "").toUpperCase().trim();

                if (VC.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.toUpperCase().trim()) {
                    if (VC.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {
                        VC.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        VC.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    VC.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    VC.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    VC.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    $scope.CheckFax_IsEmpty = function () {
        if (VC.mdlVendorRegistration.mdlVendor.fax == undefined) {
            $scope.Users.fax = "0";
        }
    }

    $scope.mdClass = '';
    VC.HideContainer = function (value) {
        //value.stopPropagation();
        var mdselectResult = angular.element(document.getElementsByTagName("md-select"));
        $(mdselectResult).triggerHandler('click');
       // var ContainerResult = angular.element(document.getElementsByClassName("md-select-menu-container"));
       // ContainerResult.removeClass('md-active md-clickable');
       // ContainerResult.addClass('md-leave');
       // ContainerResult.addClass('md-input-focused');
       // //ContainerResult.css('md-z-index', 0);
       // //ContainerResult.css('md-no-asterisk', 'true');
       // ContainerResult.css("display", "none");
       // ContainerResult.attr("aria-expanded", "false");
       // ContainerResult.attr("aria-hidden", "false");

       // //md-backdrop.md-sidenav-backdrop{
       // //    display:none
       // //}md-input-focused

       // var mdselectResult = angular.element(document.getElementsByTagName("md-select"));
       // mdselectResult.attr("aria-expanded", "false");
       // mdselectResult.css("clicked", "true");

       // var mdselectvalueResult = angular.element(document.getElementsByClassName("md-select-value"));
       // mdselectvalueResult.attr("aria-hidden", "");

       // var mdselectbackdropResult = angular.element(document.getElementsByTagName("md-backdrop"));
       // mdselectbackdropResult.css("position", "");
       //// mdselectbackdropResult.css("display", 'none');
       // mdselectbackdropResult.empty();
       // angular.element('md-select-menu-container').triggerHandler('click');
        
      //  md - select - backdrop
    }
    VC.Hide = function () {

    }
    
    //$mdDialog.show({
    //    clickOutsideToClose: true,
    //    onRemoving: function () {
    //        $scope.beforeClose();
    //    },
    //    scope: $scope,
    //    preserveScope: true
    //});
    //$scope.beforeClose = function () {
    //    $mdSelect.hide();
    //}

    //VC.ClearfrmCreateVendor = function ()
    //{
    //    VC.mdlVendorRegistration = {
    //        mdlVendor: VC.mdlVendor,
    //        mdlAddress: VC.mdlAddress,
    //        mdlServiceCategories: [],
    //    }
    //    VC.mdlCountryState = {
    //        selectedCountry: undefined,
    //        selectedState: undefined,
    //        selectedCounty: undefined,
    //        selectedCity: undefined
    //    };
    //};

    $scope.removeProfileImage = function () {
        VendorService.RemoveFile($scope.pId, function (result, data) {
            if (result) {
                $window.location.reload();
            }
            else {
                swal("ERROR", "Unable to save image.", "error");
            }
        });
    }



}
