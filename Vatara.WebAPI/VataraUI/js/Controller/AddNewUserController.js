﻿vataraApp.controller('AddNewUserController', AddNewUserController);
AddNewUserController.$inject = ['$scope', 'AddNewUserService', 'loginService', '$window', '$location', '$rootScope', 'commonService', 'propayRegService', 'CompanyService', 'manageUsersService', '$route'];
function AddNewUserController($scope, AddNewUserService, loginService, $window, $location, $rootScope, commonService, propayRegService, CompanyService,manageUsersService, $route) {

    $scope.showLoader = false;
    $scope.Country = [];
    $scope.State = [];
    $scope.County = [];
    $scope.City = [];
    $scope.LstManagers = [];

    $scope.isEmailExist = false;
    $scope.lstManagers_OrderBy = '';
    $scope.IsManagerAction = "new";

    $scope.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    }

    $scope.UserMdl = {
        Email: "",
        PersonId: "",
        PMID:''
    }

    $scope.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = false;

    $scope.Users = {

        personId: 0,
        userId: 0,
        personTypeId: 0,
        firstName: '',
        middleName: '',
        lastName: '',
        addressId: 0,
        email: '',
        phoneNo: '',
        fax: '',
        image: '',
        pmid: 0,
        userRole: 'RO',
        mailBoxNo: '',
        line1: '',
        line2: '',
        countryId: 0,
        stateId: 0,
        countyId: 0,
        cityId: 0,
        zipcode: '',
        fullAdrs: '',
        creatorUserId: $rootScope.userData.personId
    }

    $scope.DeleteUserModel = {
        personId: 0,
        deleterId: $rootScope.userData.personId
    };

    $scope.SaveAdmin = function (frmRegister) {

        $scope.Users.countryId = $scope.mdlCountryState.selectedCountry.Id;
        $scope.Users.stateId = $scope.mdlCountryState.selectedState.Id;
        $scope.Users.cityId = $scope.mdlCountryState.selectedCity.Id;
        if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
            $scope.Users.countyId = $scope.mdlCountryState.selectedCounty.id;
        }
        else {
            $scope.Users.countyId = 0;
        }
       
        $scope.Users.image = JSON.stringify($scope.Users.image);
        $scope.Users.creatorUserId = $rootScope.userData.personId;

        $scope.showLoader = true;
        AddNewUserService.SaveAdmin($scope.Users).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {

                   $scope.GetAllManagers();
                   $scope.CloseAddNewAdminMdl(frmRegister);
                   swal("SUCCESS", "Data Saved Successfully.", "success");                   
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.AddNewAdmin = function () {
        $scope.IsManagerAction = "new";
        $("#mdlAddNewAdmin").modal('show');
    }

    $scope.CloseAddNewAdminMdl = function (frmRegister) {

        $scope.Cancel();
        frmRegister.$setPristine();
        $("#mdlAddNewAdmin").modal('hide');
    }

    $scope.LoadManagers = function () {
        //$scope.newAdmin = true;
        $scope.IsManagerAction = "new";
        $scope.GetAllManagers();

        $scope.GetCountry(function (data) {
            $scope.GetStates(function (data) {

                $scope.Users.image = defaultProfileImage;
            });
        });
    }

    $scope.GetCountry = function (callback) {

        $scope.showLoader = true;
        propayRegService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.Country = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetStates = function (callback) {
        $scope.showLoader = true;
        propayRegService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.State = angular.copy(response);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCountyByStateId = function (modal) {

        $scope.mdlCountryState.selectedCounty = undefined;
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();
        $scope.GetCountyByState(modal.Id, undefined, function (data) {
            
            var countyId = 0;
            $scope.GetCityByStateOrCounty(modal.Id, countyId, function (data) {
                $scope.City = angular.copy(data);
            });
        });
    }

    $scope.ChangeCity = function (modal) {
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();
        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) {

            $scope.City = angular.copy(data);
        });
    }

    $scope.GetCountyByState = function (stateId, editId,callback) {
        $scope.showLoader = true;
        propayRegService.getCountyByState(stateId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.County = angular.copy(response.Data);

                  if (editId != undefined) {

                      //var conty = $scope.County.filter(c =>c.id == editId);
                      var conty = ($scope.County.filter(function (c) { return c.id === editId }));
                      $scope.mdlCountryState.selectedCounty = conty[0];
                  }
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        propayRegService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.EmailExist = function () {
        if ($scope.Users.email == undefined || $scope.Users.email == '') {
            return;
        }

        $scope.showLoader = true;
        AddNewUserService.EmailExist($scope.Users.email).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.isEmailExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.Cancel = function () {

        $scope.Users.personId = 0;
        $scope.Users.userId = 0;
        $scope.Users.personTypeId = 0;
        $scope.Users.firstName = '';
        $scope.Users.middleName = '';
        $scope.Users.lastName = '';
        $scope.Users.addressId = 0;
        $scope.Users.email = '';
        $scope.Users.phoneNo = '';
        $scope.Users.fax = '';
        $scope.Users.image = defaultProfileImage;
        $scope.Users.pmid = 0;
        $scope.Users.userRole = 'RO';
        $scope.Users.mailBoxNo = '';
        $scope.Users.line1 = '';
        $scope.Users.line2 = '';
        $scope.Users.countryId = 0;
        $scope.Users.stateId = 0;
        $scope.Users.countyId = 0;
        $scope.Users.cityId = 0;
        $scope.Users.zipcode = '';
        $scope.Users.fullAdrs = '';
        $scope.mdlCountryState.selectedCountry = undefined;
        $scope.mdlCountryState.selectedState = undefined;
        $scope.mdlCountryState.selectedCounty = undefined;
        $scope.mdlCountryState.selectedCity = undefined;
    }


    $scope.GetAllManagers = function () {

        $scope.showLoader = true;
        AddNewUserService.getAllManagers($rootScope.userData.personId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.LstManagers = angular.copy(response.Data);
                   angular.forEach($scope.LstManagers, function (manager, key) {
                       manager.image = JSON.parse(manager.image);
                   });
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
              
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.GetManagerDetail = function (personId, ActionName) {

        if (ActionName == "view") {
            $scope.IsManagerAction = "view";
        }
        else {
            $scope.IsManagerAction = "edit";
        }
        var person = $scope.LstManagers.filter(function (m) { return m.personId == personId });
        $scope.Users = angular.copy(person[0]);
        $scope.GetCountyByState($scope.Users.stateId, $scope.Users.countyId, function (data) { });
        $scope.GetCityByStateOrCounty($scope.Users.stateId, $scope.Users.countyId, function (data) {

            $scope.City = angular.copy(data);
            var cty = $scope.City.filter(function (c) { return c.Id == $scope.Users.cityId });
                $scope.mdlCountryState.selectedCity = cty[0];         
        });
        var cntry = $scope.Country.filter(function (c) { return c.Id == $scope.Users.countryId });
        var stat = $scope.State.filter(function (s) { return s.Id == $scope.Users.stateId });
        $scope.mdlCountryState.selectedCountry = cntry[0];
        $scope.mdlCountryState.selectedState = stat[0];
        $("#mdlAddNewAdmin").modal('show');
    }

    $scope.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    $scope.ChangeZip = function () {
        $scope.Users.zipcode = '';
        $scope.pZipValid = false;
    }

    $scope.ValidateZipCode = function (model,ele)
    {
        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidateZipCodeUsingAPI(model,ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");               
            }
        }
    }

    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };
    
    $scope.ValidateZipCodeUsingAPI = function (ZipCode, ele) {

        var city = '';
        var County = '';
        var Country = '';
        //  $scope.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name;
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "");
                        }

                    });
                });
               // var enteredCounty = $scope.mdlCountryState.selectedCounty.name.replace(" County", "").toUpperCase().trim();

                if ($scope.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.toUpperCase().trim()) {
                    if ($scope.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {
                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    $scope.ResendLoginCredential = function (PersonId, Email) {

        $scope.UserMdl.Email = Email;
        $scope.UserMdl.PersonId = PersonId;
        $scope.UserMdl.PMID = $rootScope.userData.ManagerDetail.pId;

        manageUsersService.resendLoginCredential($scope.UserMdl).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;                  
                   swal("SUCCESS", "Login credential sent successfully", "success");
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });

    }

    $scope.ZipCodeTextChange = function (zipCode, zipType) {
        if (zipCode.length >= 5) {           
            $scope.ValidateZipCode(zipCode, zipType)
        }
    }


    $scope.DeleteUser = function (personId) {

        $scope.showLoader = true;
        $scope.DeleteUserModel.personId = personId;         
        AddNewUserService.DeleteUser($scope.DeleteUserModel).
          success(function (response) {
              $scope.showLoader = false;
              if (response.Status == true) {
                  $scope.showLoader = false;
                  $scope.DeleteUserModel.personId = 0;
                  swal("SUCCESS", "User deleted successfully.", "success");
                  $scope.GetAllManagers();
              }
              else {
                  swal("ERROR", response.Message, "error");
              }

          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.CheckFax_IsEmpty = function()
    {
        if($scope.Users.fax == undefined)
        {
            $scope.Users.fax = "0";
        }
    }

    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        $scope.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;
        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, countyId, function (data) {
            $scope.City = angular.copy(data);
        });
    };

    $scope.fileSelected = function (element) {
        var myFileSelected = element.files[0];
        $scope.CompanyPhoto[0] = myFileSelected;
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('divimage1');
            if (output != null)
                output.style.backgroundImage = 'url(' + reader.result + ')';
            $scope.$apply();
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };
}



