﻿vataraApp.controller('loginController', loginController);
loginController.$inject = ['$scope', 'loginService', '$window', '$location', '$rootScope', 'commonService', 'propayRegService'];
function loginController($scope, loginService, $window, $location, $rootScope, commonService, propayRegService) {
    var LC = this;    
    $scope.showLoader = false;
    //$scope.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    LC.eml_add = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    LC.mdlUser = {
        userName: "",
        password: "",
        personId: 0,
        personType: "",
        comProfile: "",
        pmid: 0
    };
    LC.mdlforgotPasswordReset = {
        userEmail : ""
    }

    //LC.showSubscriptionPlan = false;

    LC.isForgotEmailExists = null;

    LC.PageLoad = function (loginFrm) {

        var url = $location.search(); // Used to verify Email        

        //  if ($location.path().indexOf('/chat/') > -1)

        var stringPath = '';
        if ($location.absUrl().indexOf("localhost") > -1)
        {
            stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("localhost"));
        }
        else {
            stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("vatara.biz"));
        }      
        var arr2 = stringPath.split('/');
        var comProfile = (arr2[(arr2.length - 1)]).replace('.', '');
        if (comProfile == '' || comProfile == undefined) {
            comProfile = null
        }
        //***********************************************************************************************
        //**** comment below line in normal cases only in owner/tenant login case uncomment this line. **
        //***********************************************************************************************

       // $rootScope.CompanyDomain = 'pawanman';   // ********* for testing of owner/tenant profile use this. **************

        if ($location.search().isverification != undefined) {

            if ($rootScope.userData == null || $rootScope.userData == '' || $rootScope.userData == undefined) {
                if (url.email != undefined && url.id != undefined) {
                    LC.VerifyEmail(url.email, url.id, comProfile, loginFrm)
                }
            }
            else {
                if (url.email != undefined && url.id != undefined) {
                    commonService.SessionOut();
                    LC.VerifyEmail(url.email, url.id, comProfile, loginFrm)
                }                
            }            
        }
        else {
            if (comProfile != null) {                
                $rootScope.CompanyDomain = comProfile;
                $location.path("/homepage");     //Company home page for tenant / owner
            }
        }
    }

    LC.LoginUser = function (userDataRow) {   // function used in Logged in page 

        $rootScope.userData.personId = userDataRow.id;
        $rootScope.userData.personType = userDataRow.personType;
        $rootScope.userData.personTypeId = userDataRow.personTypeId;
        $rootScope.userData.poid = userDataRow.poid

        //if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
        //    $rootScope.userData.poid = 0;
        //}
        $window.localStorage.removeItem("loginUser");
        $window.localStorage.setItem("loginUser", JSON.stringify($rootScope.userData));

        if ($rootScope.userData.personType == PersonTypeEnum.tenant) {
            $location.path("/TenantHome");
        }
        else {
            $location.path("/OwnerHome");
        }
    }

    LC.SignIn = function () {
        if ($rootScope.userData != undefined) {
            if ($rootScope.userData.CompanyData != null && $rootScope.userData.CompanyData != undefined) {
                LC.mdlUser.comProfile = $rootScope.userData.CompanyData.url;
                LC.mdlUser.pmid = $rootScope.userData.CompanyData.pmid;
            }
        }
        else if ($rootScope.CompanyDomain != undefined && $rootScope.CompanyDomain != null && $rootScope.CompanyDomain != '')
        {
            LC.mdlUser.comProfile = $rootScope.CompanyDomain;
        }

        $scope.showLoader = true;
        loginService.SignIn(LC.mdlUser).
            success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   LC.mdlUser = {};
                   LC.mdlUser = angular.copy(response.Data);
                   LC.mdlUser.Date = new Date();
                   LC.mdlUser.ManagerDetail.pImage = JSON.parse(LC.mdlUser.ManagerDetail.pImage);
                   $window.localStorage.removeItem("loginUser");
                   $window.localStorage.setItem("loginUser", JSON.stringify(LC.mdlUser));
                   $window.localStorage.removeItem("currentUserData");
                   $window.localStorage.removeItem("customer");
                   $rootScope.checkLogin = true;
                   $rootScope.userData = angular.copy(LC.mdlUser);
                   // document.getElementById("loggedInUserEmail").value = LC.mdlUser.userName;

                   $("#mdllogin").modal('hide');
                   if ($rootScope.PersonTyp.property_manager == LC.mdlUser.personType) {
                       if ($rootScope.userData.isSubscriptionExpired == true)
                       {
                           $window.localStorage.setItem("SubscriptionExpire", JSON.stringify(true));
                           window.location = '#/SubscriptionHistory';
                           window.location.reload();
                       }
                       else
                       {
                           $window.localStorage.setItem("SubscriptionExpire", JSON.stringify(false));
                           window.location = '#/home';
                           window.location.reload();
                       }
                      // $location.path("/home");
                       
                       
                   }
                   else {
                       if (LC.mdlUser.isSubscriptionExpired == false) {
                           if (LC.mdlUser.lstPerson != undefined && LC.mdlUser.lstPerson.length > 1) {
                             //  $location.path("/LoggedIn");
                               window.location = '#/LoggedIn';
                               window.location.reload();
                           }
                           else {
                               if ($rootScope.PersonTyp.owner == LC.mdlUser.personType) {
                                   //  $location.path("/OwnerHome");
                                   window.location = '#/OwnerHome';
                                   window.location.reload();
                               }
                               else {
                                 //  $location.path("/TenantHome");
                                   window.location = '#/TenantHome';
                                   window.location.reload();
                               }
                           }
                       } else {

                           swal("ERROR", "Error in login.Please contact to your manager", "error");
                           window.location = '#/homepage';
                           window.location.reload();
                       }                       
                   }
               }
               else {
                   if (response.Data != null) {

                       if (response.Message == "verifyEmail") {
                           swal("ERROR", MessageService.verifyEmail, "error");
                       }
                       else if (response.Message == "EmailAndCompanyNotMached") {
                           swal("ERROR", MessageService.EmailAndCompanyNotMached, "error");
                       }
                       else if (response.Message == "UnknownUser") {
                           swal("ERROR", MessageService.UnknownUser, "error");
                       }
                       else if (response.Message == "WrongUrl") {
                           swal("ERROR", MessageService.WrongUrl, "error");
                       }
                       else {
                           swal("ERROR", MessageService.worngUserAndPassword, "error");
                       }
                       $window.localStorage.removeItem("loginUser");
                   }
                   else {
                       swal("ERROR", MessageService.responseError, "error");
                       $window.localStorage.removeItem("loginUser");
                   }
               }
           }).error(function (data) {

               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    // **********    Logout function is implemented in indexController ****************
    //*********************************************************************************
    //LC.SignOut = function () {
    //    $scope.showLoader = true;
    //    loginService.SignOut(LC.mdlUser).
    //       success(function (response) {
    //           $scope.showLoader = false;              
    //          // $window.localStorage.removeItem("InvoicePaymentDetails");              
    //       }).error(function (data, status) {
    //           if (status == 401) {                   
    //               commonService.SessionOut();
    //           }
    //           $scope.showLoader = false;
    //       }).finally(function (data) {
    //           $scope.showLoader = false;
    //       });
    //}

    LC.PerformAction = function () {
        $scope.showLoader = true;
        loginService.PerformAction(LC.mdlUser).
           success(function (response) {

           }).error(function (data, status) {
               if (status == 401) {
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    LC.Clear = function (frmLogin) {

        LC.mdlUser = {
            userName: '',
            password: ''
        };
        frmLogin.$setPristine();
    }

    LC.RegistrationModel = function () {
        //mdlPerson.email = '';
        //mdlPerson.password = '';
        $location.path("/registration");
        //$('#personRegistration').modal();
    }

    LC.VerifyEmail = function (email, Guid, comProfile, loginFrm) {
        $scope.showLoader = true;
        //LC.mdlUser.password = null;
        //loginFrm.$setPristine();
        
        if (comProfile == null)
        {
            comProfile = '';
        }

        loginService.VerifyEmail(email, Guid, comProfile).
           success(function (response) {
               if (response.Status == true) {

                   LC.mdlUser.userName = email;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
               }

               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    LC.ClearForgotPassword = function (frmForgotPassword)
    {
        $("#mdlForgotPassword").modal('hide');
        LC.mdlforgotPasswordReset.userEmail = "";
        LC.isForgotEmailExists = null;
        frmForgotPassword.$setPristine();       
    }

    LC.ForgotPasswordReset = function (frmForgotPassword)
    {
        $scope.showLoader = true;
        loginService.ForgotPasswordReset(LC.mdlforgotPasswordReset).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {                  
                   swal("SUCCESS", "Your password is reset successfully.Please check your mail for new password", "success");
                   LC.ClearForgotPassword(frmForgotPassword);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }
    LC.CheckEmail = function (Email)
    {
        LC.CheckEmailExist(Email, function (data) {

            LC.isForgotEmailExists = data;
        });
    }

    LC.CheckEmailExist = function (Email, Callback) {

        if (Email == null || Email == '') {
            return;
        }
        $scope.showLoader = true;
        loginService.CheckEmailExist(Email).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   Callback(response.Data);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

}