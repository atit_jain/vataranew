﻿vataraApp.controller('indexController', indexController);
indexController.$inject = ['$scope', 'loginService', '$window', '$location', '$rootScope', 'commonService', '$timeout', 'chatService', '$filter', 'indexService', 'storageService'];
function indexController($scope, loginService, $window, $location, $rootScope, commonService, $timeout, chatService, $filter, indexService, storageService) {
    var IC = this;
    IC.UserData = [];
    IC.ChatData = [];
    IC.today = $filter('date')(new Date(), 'yyyy-MM-dd');
    IC.selectedEmail = null;
    IC.RowIndex = 0;
    IC.SelectedChatIndex = 0;
    IC.PMID = 0;
    IC.lstNotification = [];
    IC.TotalUnreadNotification = '';
    IC.IncmChartArray = [];
    IC.PropertyStatusChartArray = [];
    IC.selectPeriodForIncome = 'Last 12 months';
    IC.selectPeriodForProperty = 'Last 12 months';
    IC.selectPeriodForIncomendExpense = 'Last 12 months';
    IC.lstPeriod = ['Last 12 months', 'Last 6 months', 'YTD']

    IC.WorkOrderChartModel = {
        OpenWorkOrderCount: 0,
        PastDueWorkOrderCount: 0,
        lstPastDueWorkOrders: []
    };
    IC.LeaseChartModel = {
        ActiveLeaseCount: 0,
        LeaseExpiring: 0,
        lstExpiringLeases: []
    };
    IC.RentChartModel = {
        TotalInvoicedRentAmount: 0,
        TotalPaidRentAmount: 0,
        lstPastDueRentInvoices: []
    };
    IC.RentalChartData = [];
    IC.PaidRentalAmount = 0;
    IC.SubscriptionExpire = false;

    IC.SignOut = function () {

        $scope.showLoader = true;
        window.localStorage.removeItem('SubscriptionExpire');
        CloseHubConnection();
        
        IC.mdlUser = JSON.parse($window.localStorage.getItem("loginUser"));
        loginService.SignOut(IC.mdlUser).
           success(function (response) {
               commonService.SessionOut();
               // CloseHubConnection();

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");;
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.OpenUserPortal = function (userData1) {

        $rootScope.userData.personId = userData1.id;
        $rootScope.userData.personType = userData1.personType;
        $rootScope.userData.personTypeId = userData1.personTypeId;
        $rootScope.userData.poid = userData1.poid
        $rootScope.currentUserData = $rootScope.userData;

        $window.localStorage.setItem("currentUserData", JSON.stringify($rootScope.currentUserData));
        $window.localStorage.setItem("loginUser", JSON.stringify($rootScope.userData));
        //var r = confirm("Are you sure? You want to login as " + $rootScope.userData.personType);
        //if (r == true) { }

        if ($rootScope.userData.personType == PersonTypeEnum.property_manager) {
            $location.path("/home");
        }
        else if ($rootScope.userData.personType == PersonTypeEnum.owner) {

            $location.path("/OwnerHome");
        }
        else {
            $location.path("/TenantHome");
        }

        $timeout(function () {
            $rootScope.userData.personId = userData1.id;
            $rootScope.userData.personType = userData1.personType;
        }, 1000);
    }

    $scope.ShowLoginModal = function () {

        $("#mdllogin").modal('show');
    }

    IC.OpenChat = function (toMsg) {
        var divId = toMsg.split('@')[0];
        var DivWithIdAsEmail = $('div[id*="' + divId + '"]');
        chatService.isUserOnline = $(DivWithIdAsEmail).find("div.cursorpointer h4.cursorpointer  i#status").hasClass("online");
        $window.localStorage.setItem("requestedChatPage", JSON.stringify("/chat/" + toMsg));
        $location.path("/chat/" + toMsg);
        $(".notify2").click();
        $("#chat_close").click();
    }


    IC.LoadChatData = function () {

        ChatPageLoad(); // jquery Function to start the hub
        IC.selectedEmail = null;
        if ($rootScope.userData != null && $rootScope.userData != undefined) {
            IC.Currentuser = $rootScope.userData.userName;
            IC.PMID = $rootScope.userData.ManagerDetail.pId;
            IC.GetAllUserListForChat(function (data) { });
            document.getElementById("loggedInUser").value = IC.Currentuser;
            document.getElementById("loggedInUserName").value = $rootScope.userData.Name;
            IC.GetNotification(function (data) { });
        }
    }

    IC.GetAllUserListForChat = function (callback) {

        $scope.showLoader = true;
        chatService.getAllUserListForChat(IC.PMID, $rootScope.userData.personTypeId, $rootScope.userData.userName).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.UserData = angular.copy(response.Data);
                       callback(response.Data);
                       var total = 0;
                       $.each(response.Data, function () {
                           total += parseInt(this.UnreadMessage, 10);
                       });
                       if (total > 0) {
                           $("#openchat span#messageCount").html(total);
                       }
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $rootScope.$on("OpenPersonChatWindow", function (event, MdlParameters) {

        var index = 0;
        var Name = MdlParameters.EmailId.split('@')[0];
        var DivWithIdAsEmail = $('div[id*="' + Name + '"]');
        index = $(DivWithIdAsEmail).attr("index");
        if (index != undefined) {
            IC.GetSelectedUserChat(MdlParameters.EmailId, index, MdlParameters.PersonTypeId);
        }
    });

    IC.GetSelectedUserChat = function (SelectedUser, index, SelectedUserPersonTypeId) {  //(IC.Currentuser, $routeParams.user)

        document.getElementById("selectedUser").value = SelectedUser;
        IC.selectedEmail = SelectedUser;
        $scope.showLoader = true;
        chatService.getChatInfo($rootScope.userData.userName, $rootScope.userData.personTypeId, SelectedUser, SelectedUserPersonTypeId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.ChatData = angular.copy(response.Data);
                       for (var i = 0; i < IC.ChatData.length; i++) {
                           IC.ChatData[i]['creationTime'] = new Date(IC.ChatData[i]['creationTime']);
                           if (IC.ChatData[i]['isFile'] == true) {
                               IC.ChatData[i]['messageLink'] = IC.ChatData[i]['message'];
                               IC.ChatData[i]['message'] = IC.ChatData[i]['message'].substring(IC.ChatData[i]['message'].indexOf("-") + 1, IC.ChatData[i]['message'].length);
                           }
                       }
                       var Name = SelectedUser.split('@')[0];
                       var DivWithIdAsEmail = $('div[id*="' + Name + '"]');
                       var sideMessageShow = $(DivWithIdAsEmail).find("div.description div.msg_div.msg_sec div#sideMessageShow");
                       $(sideMessageShow).text('');
                       IC.DisplayDiv(index);
                       IC.SelectedChatIndex = index;
                       IC.SetScrollPosition(index);
                       IC.UpdateUnreadMessageCount(SelectedUser);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.UpdateUnreadMessageCount = function (SelectedUserEmail) {

        var intUnreadMessageCount = 0;
        var date = moment().format("MMM D, YYYY h:mm a");
        var divId = SelectedUserEmail.split('@')[0];
        var DivWithIdAsEmail = $('div[id*="' + divId + '"]');
        var unreadMessage1 = $(DivWithIdAsEmail).find("div.cursorpointer span#unreadMessageCount");   // right side user message count
        $(DivWithIdAsEmail).find(".about span#UnreadMessage").text('');
        var liWithIdAsEmail = $('li[id*="' + divId + '"]');
        if (liWithIdAsEmail.length > 0) {
            $(liWithIdAsEmail).find(".about span#UnreadMessage").text('');
        }

        //****** Set the unread message count for chat page user list***********                
        //var unreadMessage1 = $(divAbout).find("span#UnreadMessage");

        var unreadMessage = $(unreadMessage1)[0];
        var unreadMessageCount = $(unreadMessage).text();
        if (unreadMessageCount != '' && unreadMessageCount != undefined && unreadMessageCount != null) {
            intUnreadMessageCount = parseInt(unreadMessageCount);
            $(unreadMessage).text(null);
        }

        //******End Set the unread message count  for chat page user list ********
        //***********************************************


        //***** Update the total unread message count on top nav bar*******
        //*****************************************************************
        var span_Text = document.getElementById("messageCount").innerText;
        if (span_Text != '' && span_Text != undefined && span_Text != null) {
            var unreadMessageCount1 = parseInt(span_Text);
            if (unreadMessageCount1 > 0) {
                unreadMessageCount1 = unreadMessageCount1 - intUnreadMessageCount;
                if (unreadMessageCount1 <= 0) {
                    unreadMessageCount1 = null;
                }
                //console.log('unreadMessageCount1', unreadMessageCount1);
                document.getElementById('messageCount').innerHTML = unreadMessageCount1;
            }
        }
    }

    IC.DisplayDiv = function (index) {

        $('#chatrow_' + index).css({ display: "block" });
        $('#chatrow_' + index).css({ right: "0" });
    }

    IC.BackToChatList = function (index) {
        IC.selectedEmail = null;
        IC.SelectedChatIndex = 0;
        IC.RowIndex = 0;
        $('#chatrow_' + index).animate({ right: '-310px' }, 500);
        if ($location.path().indexOf('/chat/') > -1) {
        }
        else {
            $("#selectedUser").val('');
        }
    }

    IC.sendBtnClick = function () {
        document.getElementById("loggedInUser").value = IC.Currentuser;
        document.getElementById("sendmessageIndex").click();
    }
    IC.SetScrollPosition = function (index) {

        var scrollHeight1 = $('div#chatrow_' + index + ' .msg_div.msg_sec')[0].scrollHeight;
        $('div#chatrow_' + index + ' .msg_div.msg_sec').scrollTop(scrollHeight1);
    }
    $scope.$watch("IC.RowIndex", function (newVal, oldVal) {
        if (newVal == IC.ChatData.length - 1) {
            IC.SetScrollPosition(IC.SelectedChatIndex);
        }
    });

    IC.SendImage = function () {
        document.getElementById('FileUpload1').click();
    };

    IC.EmojiInit = function () {
        EmojiInit();
    }

    $rootScope.$on("CallGetNotification", function () {
        IC.GetNotification(function (data) { });
    });

    IC.ImgClick = function () {
        IC.GetNotification(function (data) { });
        $("#mdlNotification").modal('hide');
    }

    IC.GetNotification = function (callback) {

        $scope.showLoader = true;
        var personId = $rootScope.userData.personId;
        //if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
        //    personId = $rootScope.userData.poid;
        //}
        chatService.getNotification(personId, $rootScope.userData.personTypeId, IC.PMID).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.lstNotification = angular.copy(response.Data.lstNotification);
                       IC.TotalUnreadNotification = response.Data.unreadNotificationCount;
                       if (IC.TotalUnreadNotification <= 0) {
                           IC.TotalUnreadNotification = '';
                       }
                       callback(response.Data);
                   }
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
                   // swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
                   // swal("ERROR", MessageService.serverGiveError, "error");;
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.OpenImage = function (path) {
        window.open(path, '_blank', 'height=800,width=600');
    }

    IC.UpdateNotificationStatus = function (notificationId, index) {
        $scope.showLoader = true;
        chatService.updateNotificationStatus(notificationId).
         success(function (response) {
             $scope.showLoader = false;
             if (response.Status == true) {
                 IC.lstNotification[index].isRead = true;
                 if (IC.TotalUnreadNotification > 1) {
                     IC.TotalUnreadNotification = IC.TotalUnreadNotification - 1;
                 }
                 else {
                     IC.TotalUnreadNotification = '';
                 }
             }
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");;
             }
             $scope.showLoader = false;
         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    IC.OpenPage = function (module, isRead, notificationId, index) {

        $("#mdlNotification").modal('hide');
        if (!isRead) {
            IC.UpdateNotificationStatus(notificationId, index);
        }

        if (module.toUpperCase() == "WORKORDER") {
            $location.path("/workorders");
        }
        else if (module.toUpperCase() == "PROPERTY") {
            $location.path("/properties");
        }
        else if (module.toUpperCase() == "LEASE") {
            $location.path("/leases");
        }
        else if (module.toUpperCase() == "INVOICE") {
            $location.path("/invoices");
        }
    }

    $scope.ClearNotification = function () {

        var personId = $rootScope.userData.personId;
        //if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
        //    personId = $rootScope.userData.poid;
        //}
        var mdl = {
            personId: personId,
            personTypeId: $rootScope.userData.personTypeId
        };
        $scope.showLoader = true;
        chatService.clearNotification(mdl).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   swal("SUCCESS", "Data cleared successfully.", "success");
                   $("#mdlNotification").modal('hide');
                   IC.lstNotification = [];
                   IC.TotalUnreadNotification = '';
               }
               else {
                   swal("ERROR", "Something went wrong, please try after some time", "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.DeleteNOtification = function (notificationId) {

        chatService.deleteNOtification(notificationId).
          success(function (response) {
              $scope.showLoader = false;
              if (response.Status == true) {
                  $('#mdlNotification').modal('hide');
                  var index = IC.lstNotification.map(function (item) {
                      return item.id;
                  }).indexOf(notificationId);
                  IC.lstNotification.splice(index, 1);

                  if (IC.TotalUnreadNotification > 1) {
                      IC.TotalUnreadNotification = IC.TotalUnreadNotification - 1;
                  }
                  else {
                      IC.TotalUnreadNotification = '';
                  }
                  swal("SUCCESS", "Data deleted successfully.", "success");;
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  $('#mdlNotification').modal('hide');
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");;
              }
              $scope.showLoader = false;
          }).finally(function (data) {
              $('#mdlNotification').modal('hide');
              $scope.showLoader = false;
          });

    }

    IC.HomePageLoad = function () {
        if (chatService.Isminimise) {
            openchat();
            chatService.Isminimise = false;
        }
        document.getElementById("loggedInUserEmail").value = $rootScope.userData.userName;
        IC.GetChartDataForDashboard(IC.selectPeriodForIncome);

    }

    IC.GetIncomeChartData = function (Duration) {
        $scope.showLoader = true;
        IC.PMID = $rootScope.userData.ManagerDetail.pId;
        indexService.getIncomeExpenseChartData(IC.PMID, Duration).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.CreateIncomeExpenseChartData(response.Data);
                   }
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.GetlstIncomeChartData = function (Duration) {
        $scope.showLoader = true;
        IC.PMID = $rootScope.userData.ManagerDetail.pId;
        indexService.GetlstIncomeChartData(IC.PMID, Duration).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.CreateIncomeChart(response.Data);
                   }
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }
    IC.GetIncomeandExpenseChartData = function (Duration) {
        $scope.showLoader = true;
        IC.PMID = $rootScope.userData.ManagerDetail.pId;
        indexService.GetIncomeandExpenseChartData(IC.PMID, Duration).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.CreateIncomeExpenseChart(response.Data);
                   }
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.GetPropertyChartData = function (Duration) {

        $scope.showLoader = true;
        IC.PMID = $rootScope.userData.ManagerDetail.pId;
        indexService.getPropertyChartData(IC.PMID, Duration).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       IC.CreatePropertyStatusChartData(response.Data);
                   }
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    IC.GetChartDataForDashboard = function (Duration) {
        $scope.showLoader = true;
        IC.ActiveLeaseCount = 0;
        IC.LeaseExpiring = 0;
        IC.WorkOrderChartModel.OpenWorkOrderCount = 0;
        IC.WorkOrderChartModel.PastDueWorkOrderCount = 0;
        IC.WorkOrderChartModel.lstPastDueWorkOrders = [];

        IC.PMID = $rootScope.userData.ManagerDetail.pId;
        indexService.getChartDataForDashboard(IC.PMID, Duration).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    if (response.Data != null) {
                        IC.CreateIncomeExpenseChartData(response.Data.lstIncomeExpenseLiabilityChartData);
                        IC.CreatePropertyStatusChartData(response.Data.lstMdlPropertyStatusChartDataForDashboard);
                        IC.CreateWorkOrderChartData(response.Data.lstMdlWorkOrderChartDataForDashboard);
                        IC.CreateLeaseChartData(response.Data.mdlLeaseChartDataForDashboard);
                        IC.CreateRentChartData(response.Data.mdlRentChartDataForDeshboard);
                        //IC.CreateIncomeChart(response.Data.lstIncomeChartData);
                        //IC.CreateIncomeExpenseChart(response.Data.lstIncomeExpenseChartData);
                    }
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    IC.CreateLeaseChartData = function (model) {

        IC.LeaseChartModel.ActiveLeaseCount = 0;
        IC.LeaseChartModel.LeaseExpiring = 0;
        IC.LeaseChartModel.lstExpiringLeases = [];

        IC.LeaseChartModel.ActiveLeaseCount = model.mdlLeaseChartData.ActiveLeases;
        IC.LeaseChartModel.LeaseExpiring = model.mdlLeaseChartData.LeasesExpiring;
        IC.LeaseChartModel.lstExpiringLeases = angular.copy(model.lstMdlExpiringLeases);
    }

    IC.CreateRentChartData = function (model) {
        IC.RentalChartData = [];
        IC.RentChartModel.TotalInvoicedRentAmount = 0;
        IC.RentChartModel.TotalPaidRentAmount = 0;
        IC.RentChartModel.lstPastDueRentInvoices = [];

        IC.RentChartModel.TotalInvoicedRentAmount = model.mdlRentPaidUnpaidAmountForDashboardChart.TotalInvoicedRentAmount;
        IC.RentChartModel.TotalPaidRentAmount = model.mdlRentPaidUnpaidAmountForDashboardChart.TotalPaidRentAmount;
        IC.RentChartModel.lstPastDueRentInvoices = angular.copy(model.lstMdlPastDueRentInvoices);

        var paid = Math.ceil(IC.Percentage(IC.RentChartModel.TotalPaidRentAmount, IC.RentChartModel.TotalInvoicedRentAmount));
        if (!isNaN(paid)) {
            IC.PaidRentalAmount = paid;
        }
        else {
            paid = 0;
            IC.PaidRentalAmount = paid; 
        }
        var unpaid = (100 - paid);
        var paidArr = ['Paid', paid];
        var unpaidArr = ['Unpaid', unpaid];

        IC.RentalChartData.push(['Content', 'Size']);
        IC.RentalChartData.push(paidArr);
        IC.RentalChartData.push(unpaidArr);
        IC.RentalChart();
    }

    IC.Percentage = function (partialValue, totalValue) {
        return (100 * partialValue) / totalValue;
    }

    IC.CreateWorkOrderChartData = function (model) {
        var lstOpenWorkOrder = model.filter(function (WorkOrder) { return WorkOrder.Status == 'Open' });
        var lstPastDueWorkOrder = model.filter(function (WorkOrder) { return WorkOrder.Status == 'PastDue' });

        IC.WorkOrderChartModel.OpenWorkOrderCount = lstOpenWorkOrder.length;
        IC.WorkOrderChartModel.PastDueWorkOrderCount = lstPastDueWorkOrder.length;
        IC.WorkOrderChartModel.lstPastDueWorkOrders = angular.copy(lstPastDueWorkOrder);
    }


    IC.CreatePropertyStatusChartData = function (data) {
        IC.PropertyStatusChartArray = [];
        var Headingarry = ['Genre', 'Leased', 'Vacant', { role: 'annotation' }];
        IC.PropertyStatusChartArray.push(Headingarry);
        angular.forEach(data, function (Value, key) {
            var DataArray = [];
            DataArray.push(Value.MonthYear, Value.TotalLeasedProperty, Value.TotalVacantProperty, '')
            IC.PropertyStatusChartArray.push(DataArray);
        });
        IC.PropertyStatusChartLoad();
    };

    IC.CreateIncomeExpenseChartData = function (data) {
        IC.IncmChartArray = [];
        var Headingarry = ['Genre', 'Liability', 'Net Income', { role: 'annotation' }];
        IC.IncmChartArray.push(Headingarry);
        angular.forEach(data, function (Value, key) {
            var DataArray = [];
            DataArray.push(Value.MonthYear, Value.Liability, Value.Net_Income, '')
            IC.IncmChartArray.push(DataArray);
        });
        IC.IncmChartLoad();
    };

    IC.RentalChart = function () {

        //google.load("visualization", "1", {
        //    packages: ["corechart"]
        //});
        //google.setOnLoadCallback(drawChart);
        //function drawChart() {
        //    var data = new google.visualization.DataTable();
        //    data.addColumn('string', 'Volume');
        //    data.addColumn('number', 'Mortgage Volume');
        //    data.addRows([
        //        ['A', 10000],
        //        ['B', 0, ]
        //    ]);
        //    var options = {
        //        title: 'Mortgage Volume',
        //        width: 400,
        //        height: 400,
        //        colors: ['silver', '#f5f5f5'],
        //        pieSliceText: 'none',
        //        pieHole: 0.70,
        //        legend: 'none',
        //        //legend: { position: 'labeled' },
        //        slices: {
        //            1: { offset: 0.05 },
        //        },
        //        tooltip: { trigger: 'none' },
        //        animation: { duration: 800, easing: 'in' }
        //    };
        //    var chart = new google.visualization.PieChart(document.getElementById('RentalChart'));
        //    chart.draw(data, options);
        //}

        google.load("visualization", "1", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            //var data = google.visualization.arrayToDataTable([
            //    ['Content', 'Size'],
            //    ['Photos', 22],
            //    ['Videos', 9]

            //]);

            var data = google.visualization.arrayToDataTable(IC.RentalChartData);


            var options = {
                title: "",
                pieHole: 0.9,
                pieSliceBorderColor: "none",
                colors: ['#46b5a6', '#ce4953'],
                legend: {
                    position: "none"
                },
                pieSliceText: "none",
                tooltip: {
                    trigger: "none"
                },
                chartArea: {
                    left: "3%",
                    top: "3%",
                    height: "94%",
                    width: "94%"
                }
                // chartArea: { left: 0, top: 0, width: "100%", height: "100%" }
                //width: '100%',
                //height: 300,
            };
            var chart = new google.visualization
                    .PieChart(document.getElementById('RentalChart'));
            chart.draw(data, options);
        }
    };

    IC.IncmChartLoad = function () {
        debugger;
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });
        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawIncmChart);
        function drawIncmChart() {
            var data = new google.visualization.arrayToDataTable(IC.IncmChartArray);
            var group_width = '50%';
            if (IC.IncmChartArray.length <= 3) {
                group_width = '10%';
            }
            var options = {
                // title: 'Income And Liability, in ($)',
                colors: ['#05c0a7', '#979899', '#008bff'],
                width: '100%',
                height: 400,
                legend: { position: 'bottom', maxLines: 3 },
                bar: { groupWidth: group_width },
                isStacked: true,
                vAxis: {
                    title: 'Income',
                    format: 'currency'
                }
                //hAxis: {
                //    title: 'Month'
                //}
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('IncmChart'));
           chart.draw(data, options);
        }
    }

    IC.CreateIncomeChart = function (data) {
        IC.IncmChartDataArray = [];
        var Headingarry = ['Genre', 'Income', { role: 'annotation' }];
        IC.IncmChartDataArray.push(Headingarry);
        angular.forEach(data, function (Value, key) {
            var DataArray = [];
            DataArray.push(Value.MonthYear, Value.Net_Income, '')
            IC.IncmChartDataArray.push(DataArray);
        });
        IC.IncmChartDataLoad();
    }

    IC.IncmChartDataLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });
        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawIncomeChart);
        function drawIncomeChart() {
            var data = new google.visualization.arrayToDataTable(IC.IncmChartDataArray);

            var group_width = '50%';
            if (IC.IncmChartDataArray.length <= 3) {
                group_width = '10%';
            }
            var options = {
                // title: 'Income And Liability, in ($)',
                colors: ['#05c0a7', '#979899', '#008bff'],
                width: '100%',
                height: 400,
                legend: { position: 'bottom', maxLines: 3 },
                bar: { groupWidth: group_width },
                isStacked: true,
                vAxis: {
                    title: 'Income',
                    format: 'currency'
                }
                //hAxis: {
                //    title: 'Month'
                //}
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.LineChart(document.getElementById('IncmChart'));
            chart.draw(data, options);
        }
    }

    IC.CreateIncomeExpenseChart = function (data)
    {
        IC.IncmExpenseChartDataArray = [];
        var Headingarry = ['Genre', 'Income','Expense', { role: 'annotation' }];
        IC.IncmExpenseChartDataArray.push(Headingarry);
        angular.forEach(data, function (Value, key) {
            var DataArray = [];
            DataArray.push(Value.MonthYear, Value.Net_Income,Value.Liability, '')
            IC.IncmExpenseChartDataArray.push(DataArray);
        });
        if (IC.IncmExpenseChartDataArray.length > 1) {
            IC.IncmExpnsChartDataLoad();
        }
    }

    IC.IncmExpnsChartDataLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });
        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawIncomeExpenseChart);
        function drawIncomeExpenseChart() {
            var data = new google.visualization.arrayToDataTable(IC.IncmExpenseChartDataArray);

            var group_width = '50%';
            if (IC.IncmExpenseChartDataArray.length <= 3) {
                group_width = '10%';
            }
            var options = {
                // title: 'Income And Liability, in ($)',
                colors: ['#05c0a7', '#979899', '#008bff'],
                width: '100%',
                height: 400,
                legend: { position: 'bottom', maxLines: 3 },
                bar: { groupWidth: group_width },
                isStacked: true,
                vAxis: {
                    title: 'Income',
                    format: 'currency'
                }
                //hAxis: {
                //    title: 'Month'
                //}
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.LineChart(document.getElementById('IncmExpenseChart'));
            chart.draw(data, options);
        }
    }

    IC.PropertyStatusChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });
        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawIncmChart);
        function drawIncmChart() {
            var data = new google.visualization.arrayToDataTable(IC.PropertyStatusChartArray);
            var group_width = '50%';
            if (IC.PropertyStatusChartArray.length <= 3) {
                group_width = '10%';
            }
            var options = {
                // title: 'Month-wise property status',
                colors: ['#05c0a7', '#b96167'],
                width: '100%',
                height: 400,
                legend: { position: 'bottom' },
                bar: { groupWidth: group_width },
                isStacked: true,
                vAxis: {
                    title: 'Number of properties',
                    format: '0'
                }
                //hAxis: {
                //    title: 'Month'
                //}
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('PropertyStatusChart'));
            chart.draw(data, options);
        }
    }

    IC.ViewLeaseDetails = function (leaseId, propertyId, leaseStartDate, leaseEndDate) {
        //leaseService.leaseStartDate = leaseStartDate;
        //leaseService.leaseEndDate = leaseEndDate;
        var mdlLeaseDuration = {
            leaseStartDate: leaseStartDate,
            leaseEndDate: leaseEndDate
        };
        storageService.remove('mdlLeaseDuration');
        storageService.save('mdlLeaseDuration', mdlLeaseDuration);
        $location.path("/leaseview/" + leaseId + "/" + propertyId);
    }
   
}