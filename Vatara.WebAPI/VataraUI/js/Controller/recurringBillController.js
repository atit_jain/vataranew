﻿vataraApp.controller('recurringBillController', recurringBillController);
recurringBillController.$inject = ['$scope', 'recurringBillService', 'commonService', '$rootScope', 'pagingService', '$filter', '$location'];
function recurringBillController($scope, recurringBillService, commonService, $rootScope, pagingService, $filter, $location) {
    var RC = this;    
    $scope.showLoader = false;
    RC.recurringBill = [];
    $scope.popUp = {
        Heading: 'Stop Recurring',
        Message: ''
    };

    RC.selectBill = {};
    RC.isSubTab = false;
    RC.propertyId = 0;

    RC.GetRecurringBill = function () {
        $scope.showLoader = true;
        recurringBillService.recurringBill($rootScope.userData.personTypeId, $rootScope.userData.personId, RC.propertyId).
        success(function (data) {
            RC.recurringBill = data.Data;
            $scope.showLoader = false;
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    RC.LoadRecurringBill = function()
    {
        if ($location.url().match("payment")) {
            RC.isSubTab = true;
        }
        RC.GetRecurringBill();
    }

    RC.ShowPopUpRecurring = function (recurringBill) {
        RC.selectBill = recurringBill;
        $scope.popUp = {
            Heading: 'Stop Payment',
            Message: 'Do you want to stop this Recurring Payment?'
            };

        $("#stopRecurring").modal();
    }

    $scope.ConfirmMessage_Ok = function()
    {
        $scope.showLoader = true;
        $("#stopRecurring").modal('hide');
       recurringBillService.stopRecurringpayment(RC.selectBill.billId).
       success(function (data) {
           RC.LoadRecurringBill();
           $scope.showLoader = false;
       }).error(function (data, status) {
           if (status == 401) {
               commonService.SessionOut();
           }
           else {
               swal("ERROR", MessageService.serverGiveError, "error");
           }
           $scope.showLoader = false;

       }).finally(function (data) {
           $scope.showLoader = false;
       });
    }

    RC.GetRecurringBillByPropertyID = function (PropertyID)
    {
        recurringBillService.recurringBill($rootScope.userData.personTypeId, $rootScope.userData.personId, PropertyID).
        success(function (data) {
            RC.recurringBill = data.Data;
            $scope.showLoader = false;
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }
}