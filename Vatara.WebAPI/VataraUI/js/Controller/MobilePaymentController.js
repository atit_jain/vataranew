﻿vataraApp.controller('MobilePaymentController', MobilePaymentController);
MobilePaymentController.$inject = ['$scope', 'paymentService', 'loginService', '$filter', '$window', '$location', '$rootScope', 'commonService', '$route', '$sce', '$routeParams', 'serviceCategory_service', 'invoiceService', 'recurringBillService'];
function MobilePaymentController($scope, paymentService, loginService, $filter, $window, $location, $rootScope, commonService, $route, $sce, $routeParams, serviceCategory_service, invoiceService, recurringBillService) {

    var PC = this;
    PC.StartOnRecurring = new Date();
    PC.MinDate = new Date();
    PC.PaymentSchedule = ["Monthly", "Quarterly"];
    PC.SelectedSchedule = "Monthly";
    PC.PersonTypeManager = 1;
    PC.PersonTypeEnum = {
        Manager: 1,
        Owner: 2,
        Tenant: 3
    }
    if ($rootScope.userData != undefined) {
        PC.PersonTypeId = $rootScope.userData.personTypeId;
        PC.managerid = $rootScope.userData.personId;
    }
    else {
        PC.PersonTypeId = 0;
        PC.managerid = 0;
    }
    $scope.showLoader = false;
    PC.withInvoice = "withoutinvoice";
    PC.selectPaymentType = null;
    PC.lstProperty = [];
    PC.lstPropertByManagerAndOwner = [];
    PC.lstPropertyOwnerTenant = [];
    PC.selectProperty = [];
    PC.selectClient = "";
    PC.selectInvoice = "";
    PC.lstPropertyOwnerTenantByPropertyId = [];
    PC.lstInvoice_WithAmount = [];
    PC.amount = "";
    PC.propayProfile_Payer = "";
    PC.propayProfile_Merchant = "";
    PC.showTransctionMessage = false;
    PC.ShowPayment = true;
    PC.showSuccessMessage = false;
    PC.remainAmount = 0;
    PC.ShowPropayMsg = false;
    PC.SearchResult = undefined;
    $rootScope.PaymentAgainstInvoice = false;
    $rootScope.InvoicePaymentDetails = '';
    PC.IsPayWithCard = false;
    PC.IsRecurringPayment = false;
    PC.cVV = '';
    PC.expiryDate = '';
    PC.cardNum = '';
    PC.paymentCategory = '';
    PC.PropertyID = 0;

    PC.lstServiceCategory = [];
    PC.copylstServiceCategory = [];
    PC.lstPaymentType = [{ "id": 0, "paymentMethod": "Credit Card" }, { "id": 1, "paymentMethod": "ACH" }, { "id": 2, "paymentMethod": "Cash/Cheque" }];
    PC.recurringBill = [];
    PC.Message = '';
    PC.mdlUser = {
        userName: "",
        password: "",
        personId: 0,
        personType: "",
        comProfile: "",
        pmid: 0
    };
    PC.MdlPayment = {
        manageId: 0,//$rootScope.userData.ManagerDetail.pId,
        managerEmail: "",
        invoiceNumber: "",
        invoiceId: 0,
        amount: 0,
        payerId: 0,
        merchantProfileId: 0,
        name: "",
        paymentMode: 'Online',
        transactionIdentifier: '',
        description: "",
        withInvoice: PC.withInvoice,
        propertyId: 0,
        clientId: 0,
        clientEmail: '',
        selectPaymentType: 0,
        PayerType: 0,
        RecipientType: 0,
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipCode: '',
        transactionHistoryId: 0,
        isRecurringPayment: false,
        startOnRecurring: new Date(),
        paymentSchedule: 'Monthly',
        paymentCategoryId: 0,
        PersonId: 0
    };

    if ($rootScope.userData != undefined) {
        PC.MdlPayment.manageId = $rootScope.userData.ManagerDetail.pId;
    }
    
   
    PC.GetUserInfo = function (PMID, PersonId, PersonTypeId, callback) {
        loginService.GetUserInfo(PMID, PersonId, PersonTypeId).
           success(function (response) {
               if (response.Status == true)
               {
                   $window.localStorage.setItem("loginUser", JSON.stringify(response.Data));
                  // $rootScope.checkLogin = true;
                   $rootScope.userData = angular.copy(response.Data);
                   callback(response.Data);

                   PC.mdlUser = {};
                   PC.mdlUser = angular.copy(response.Data);
                   PC.mdlUser.Date = new Date();
                   PC.mdlUser.ManagerDetail.pImage = JSON.parse(PC.mdlUser.ManagerDetail.pImage);
                   $window.localStorage.removeItem("loginUser");
                   $window.localStorage.setItem("loginUser", JSON.stringify(PC.mdlUser));
                   $window.localStorage.removeItem("currentUserData");
                   $window.localStorage.removeItem("customer");
                   $rootScope.userData = angular.copy(PC.mdlUser);
                   //$location.path("/homepage");
                   window.location = '#/MobilePayment';
                   window.location.reload();
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });

    }

    PC.ChoosePaymentType = function (value) {
        //PC.selectPaymentType = value;
        PC.MdlPayment.selectPaymentType = PC.selectPaymentType;
        if (PC.selectPaymentType != 2) {
            PC.GetMerchantId();
        }
        else {
            PC.propayProfile_Merchant = "";
        }
    }

    PC.PayWithInvoice = function () {
        PC.MdlPayment = {
            manageId: $rootScope.userData.ManagerDetail.pId,
            managerEmail: "",
            invoiceNumber: "",
            invoiceId: 0,
            amount: 0,
            payerId: 0,
            merchantProfileId: 0,
            name: "",
            paymentMode: 'Online',
            transactionIdentifier: '',
            description: "",
            withInvoice: PC.withInvoice,
            propertyId: 0,
            clientId: 0,
            clientEmail: '',
            selectPaymentType: 0,
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipCode: '',
            moneySenderId: 0,
            moneyReceiverId: 0,
            transactionHistoryId: 0,
            paymentCategoryId: 0,
        };
        PC.amount = "";
        PC.selectClient = '';
        PC.selectInvoice = '';
        PC.lstInvoice_WithAmount = [];
        PC.lstInvoiceNumber = [];
    }


    PC.GetServiceCategory = function (personTypeId) {
        $scope.showLoader = true;
        serviceCategory_service.getServiceCategory(personTypeId).
                    success(function (data) {
                        $scope.showLoader = false;
                        PC.lstServiceCategory = data.Data;
                        PC.copylstServiceCategory = angular.copy(PC.lstServiceCategory);

                    }).error(function (data, status) {
                        if (status == 401) {
                            commonService.SessionOut();
                        }
                        else {
                            swal("ERROR", MessageService.serverGiveError, "error");
                        }
                        $scope.showLoader = false;

                    }).finally(function (data) {
                        $scope.showLoader = false;
                    });
    };

    ///******************************************************//////////////////
    ///************Load function runs on page load ********************/////////////

    //PC.LoadMobile = function () {
    //    debugger;
    //    if ($location.url().split('?')[0].match("MobilePayment")) {
    //        $rootScope.userData = JSON.parse($window.localStorage.getItem("loginUser"));
    //        if ($rootScope.userData === undefined || $rootScope.userData==null) {
    //            var routeParam = $location.search();
    //            var PMID = routeParam.PMID;
    //            var PersonId = routeParam.PersonId;
    //            var PersonTypeId = routeParam.PersonTypeId;
    //            PC.GetUserInfo(PMID, PersonId, PersonTypeId, function (data) {
    //            });
    //        }
    //    }
    //}
    PC.PageLoad = function () {
        debugger;
        if ($location.url().split('?')[0].match("MobilePayment")) {
            $rootScope.userData = JSON.parse($window.localStorage.getItem("loginUser"));
            var routeParam = $location.search();
            if (routeParam.PMID != null || routeParam.PMID!=undefined) {
                // var routeParam = $location.search();
                    var PMID = routeParam.PMID;
                    var PersonId = routeParam.PersonId;
                    var PersonTypeId = routeParam.PersonTypeId;
                    PC.GetUserInfo(PMID, PersonId, PersonTypeId, function (data) {
                    });
            }
            else {

                 var showPropayMessage = $window.localStorage.getItem("ShowPropayMsg");
                $rootScope.userData = JSON.parse($window.localStorage.getItem("loginUser"));
                if ($rootScope.userData != undefined) {
                    PC.managerid = $rootScope.userData.personId;
                    PC.PersonTypeId = $rootScope.userData.personTypeId;
                    PC.MdlPayment.manageId = $rootScope.userData.ManagerDetail.pId;
                }
                if ($location.search().result != undefined) {
                    $scope.showLoader = true;
                    PC.GetValue();
                    //PC.ShowPropayMsg = JSON.parse($window.localStorage.getItem("ShowPropayMsg"));
                    //if (PC.ShowPropayMsg) {
                    //    PC.SearchResult = JSON.parse($window.localStorage.getItem("paymentResult"));
                    //   // PC.RedirctSamepage();
                    //    PC.GetValueFromUrl();
                    //}

                    // alert();
                }
                else if (showPropayMessage != 'undefined' && showPropayMessage != null) {
                    PC.showSuccessMessage = true;
                    PC.ShowPropayMsg = JSON.parse($window.localStorage.getItem("ShowPropayMsg"));
                    if (PC.ShowPropayMsg) {
                        PC.SearchResult = JSON.parse($window.localStorage.getItem("paymentResult"));
                        PC.GetValueFromUrl();
                    }
                }
                else {
                    PC.showTransctionMessage = false;
                    // alert(1);
                    //document.getElementsByClassName("iFramTabClass").style.display = "block";
                    // $(".iFramTabClass").show();           
                    if ($window.localStorage.getItem("paymentResult") != "undefined") {
                        PC.SearchResult = JSON.parse($window.localStorage.getItem("paymentResult"));
                    }
                    else {
                        PC.SearchResult = undefined;
                    }
                    if (PC.SearchResult != undefined && PC.SearchResult != "") {
                        PC.GetValueFromUrl();
                    }
                    else {
                        if ($window.localStorage.getItem("PaymentAgainstInvoice") != null) {
                            $rootScope.PaymentAgainstInvoice = JSON.parse($window.localStorage.getItem("PaymentAgainstInvoice"));
                        }
                        if ($rootScope.PaymentAgainstInvoice) {

                            $rootScope.InvoicePaymentDetails = JSON.parse($window.localStorage.getItem("InvoicePaymentDetails"));
                            PC.amount = $rootScope.InvoicePaymentDetails.dueAmt;
                            PC.remainAmount = $rootScope.InvoicePaymentDetails.dueAmt;
                            PC.MdlPayment.manageId = PC.managerid;
                            PC.MdlPayment.invoiceNumber = $rootScope.InvoicePaymentDetails.invNumber;
                            PC.MdlPayment.invoiceId = $rootScope.InvoicePaymentDetails.intId;
                            PC.MdlPayment.amount = $rootScope.InvoicePaymentDetails.amount;
                            PC.MdlPayment.paymentMode = 'Online';
                            PC.MdlPayment.propertyId = $rootScope.InvoicePaymentDetails.propertyId;
                            PC.MdlPayment.withInvoice = 'invoice';
                            PC.MdlPayment.clientId = $rootScope.InvoicePaymentDetails.from;
                            PC.MdlPayment.clientEmail = $rootScope.InvoicePaymentDetails.fromEmail;
                            //moneySenderId: 0,
                            //moneyReceiverId: 0,
                            if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
                                //Temporary condition to identify PersonTypeId only for case where receipient is owner or tenant
                                if (PC.selectClient.PersonType == "Owner") {
                                    PC.MdlPayment.RecipientType = PC.PersonTypeEnum.Owner;
                                }
                                else {
                                    PC.MdlPayment.RecipientType = PC.PersonTypeEnum.Tenant;
                                }
                                PC.MdlPayment.PayerType = PC.PersonTypeEnum.Manager; // Person type is manager
                            }
                            else {
                                PC.MdlPayment.RecipientType = PC.PersonTypeEnum.Manager; // receiver of payment
                                PC.MdlPayment.PayerType = $rootScope.userData.personTypeId; // Person type Owner/Tenant
                            }
                            PC.GetMerchantId();
                        }
                        $scope.showLoader = false;
                    }
                }
                if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {

                    PC.GetPropertByManagerAndOwner();
                    PC.GetPropertyTenantOwner(true, 0);
                }
                else if (PersonTypeEnum.owner == $rootScope.userData.personType) {
                    PC.GetPropertByOwner();
                }
                else {
                    PC.GetPropertByTenant();
                }
                PC.GetPayerId();
                var personTypeId = $rootScope.userData.personTypeId;
                PC.GetServiceCategory(personTypeId);
                // $scope.showLoader = false;
            }
        }

    }

    PC.CancelPaymentAgainstInvoice = function () {

        PC.showTransctionMessage = false;
        $window.localStorage.removeItem("PaymentAgainstInvoice")
        $location.path('/OwnerHome');
    }

    ///******************************************************
    ///************ Function to get the payer Propay data ****

    PC.GetPayerId = function () {
        var personId = 0;

        if (PersonTypeEnum.owner == $rootScope.userData.personType) {
            personId = $rootScope.userData.poid;
        }
        else if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            personId = $rootScope.userData.ManagerDetail.pId;
        }
        else {
            personId = $rootScope.userData.personId;
        }
        PC.GetPropayProfile($rootScope.userData.userName, personId, $rootScope.userData.personTypeId, function (data) {
            PC.propayProfile_Payer = data;
            if (PC.propayProfile_Payer != null) {
                if (PC.propayProfile_Payer.LastPaymentType != null) {
                    PC.selectPaymentType = PC.propayProfile_Payer.LastPaymentType;
                }
                else {
                    PC.selectPaymentType = 0; //Credit Card
                }
                PC.MdlPayment.payerId = PC.propayProfile_Payer.payerId;
                PC.MdlPayment.name = PC.propayProfile_Payer.firstName + ' ' + PC.propayProfile_Payer.lastName;
                PC.MdlPayment.city = PC.propayProfile_Payer.cityName;
                PC.MdlPayment.state = PC.propayProfile_Payer.stateName;
                PC.MdlPayment.address1 = PC.propayProfile_Payer.address;
                PC.MdlPayment.zipCode = PC.propayProfile_Payer.zipCode;
                //address1: '',
                //address2: '',
                //city: '',
                //state: '',
                //zipCode: '',
            }
        })

    }

    ///*****************************************************************************************
    ///***************    Method for getting the propay merchant id for selected people    *****   


    PC.GetMerchantId = function () {
        var Email1 = '';
        var PersonId = 0;
        var PersonTypeId = 0;

        if (PC.selectPaymentType != 2) {     // 2 => Cash/Cheque

            if (!$rootScope.PaymentAgainstInvoice && (PC.selectClient == undefined || PC.selectClient == '')) {
                return;
            }
            else {
                if ($rootScope.PaymentAgainstInvoice) {

                    //PC.GetPersonInfo(PC.MdlPayment.clientEmail);
                    Email1 = angular.copy(PC.MdlPayment.clientEmail);
                    PersonId = $rootScope.userData.ManagerDetail.pId;
                    PersonTypeId = $rootScope.userData.ManagerDetail.pTypeId;
                }
                else {
                    Email1 = PC.selectClient.Email;
                    PersonId = PC.selectClient.personId; // if selectClient is owner than PersonId will be poid
                    PersonTypeId = PC.selectClient.PersonTypeId;
                }
                PC.GetPropayProfile(Email1, PersonId, PersonTypeId, function (data) {

                    PC.propayProfile_Merchant = data;
                    if (PC.propayProfile_Merchant != null) {
                        PC.MdlPayment.merchantProfileId = PC.propayProfile_Merchant.marchantProfileId;
                    }
                    PC.paymentCategory = '';
                    if (PC.selectClient.PersonTypeId == $rootScope.PersonTypeIdEnum.owner) {
                        PC.lstServiceCategory = PC.lstServiceCategory.filter(function (item) { return item.name.toUpperCase() !== "SECURITY DEPOSIT" })
                    }
                    else {
                        PC.lstServiceCategory = [];
                        PC.lstServiceCategory = angular.copy(PC.copylstServiceCategory);
                    }
                })
            }
        }
    }

    PC.GetPersonInfo = function (Id) {

        paymentService.getPersonInfo(Id).
           success(function (response) {
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    PC.GetPropayProfile = function (Email, PersonId, PersonTypeId, callback) {

        if (Email === undefined) {
            Emai = $rootScope.userData.userName;
        }
        $scope.showLoader = true;
        paymentService.getPropayProfile(Email, PersonId, PersonTypeId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data != null)
                       PC.MdlPayment.PersonId = response.Data.personId;
                   callback(response.Data);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {

               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    ///////****************************************************************************/////////////
    ////////////////*****End Method for getting the propay merchant id for selected people******///////////
    ////////////////****************************************************************************/////////


    //*****************************************************************************************
    //************ Function to determine wether payment is against any invoice or not *********

    PC.GetValue = function () {
        if ($location.search().result != undefined) {
            PC.ShowPropayMsg = true;
            $window.localStorage.setItem("paymentResult", JSON.stringify($location.search().result));
            $window.localStorage.setItem("ShowPropayMsg", JSON.stringify(PC.ShowPropayMsg));
            $window.localStorage.setItem("GetErrorPropayMsg", JSON.stringify($location.search().message));
            $scope.PaymentUrl = $sce.trustAsResourceUrl('');
            $("#MainHtml").remove();
            parent.location.reload();
            // PC.RedirctSamepage();
        }
    }

    PC.GetValueFromUrl = function () {
        if (PC.SearchResult != undefined) {
            console.log("PC.SearchResult", PC.SearchResult);

            if (PC.SearchResult == 'Failure') {
                PC.RedirctSamepage();
                PC.ShowPayment = true;
                PC.showTransctionMessage = true;
                PC.showSuccessMessage = false;
                //$location.search().message
                var message = $window.localStorage.getItem("GetErrorPropayMsg");
                swal("ERROR", message, "error");
            }
            else if (PC.SearchResult == 'Cancel') {
                PC.ShowPayment = true;
                PC.showTransctionMessage = true;
                // PC.RedirctSamepage();

                // ****** IF cancled a payment then redirect to home page of user *******
                //if (PersonTypeEnum.tenant == $rootScope.userData.personType) {
                //    $location.path("/TenantHome");
                //}
                //if (PersonTypeEnum.owner == $rootScope.userData.personType) {
                //    $location.path("/OwnerHome");
                //}
                //************************************************************************
            }
            else {
                PC.MdlPayment = JSON.parse($window.localStorage.getItem("MdlPayment"));
                PC.showTransctionMessage = true;
                if (PC.MdlPayment != null) {
                    if (PC.MdlPayment.RecipientType == 1) {
                        PC.MdlPayment.PersonId = $rootScope.userData.personManageriD;
                    }
                    if (PC.MdlPayment.withInvoice == 'invoice') {
                        PC.SavePayment();
                        PC.ShowPayment = true;
                        PC.showTransctionMessage = true;
                        PC.showSuccessMessage = true;
                    }
                    else {
                        PC.SavePayment_WithoutInvoice();
                    }

                }
            }
            $window.localStorage.setItem("GetErrorPropayMsg", undefined);
            $window.localStorage.setItem("paymentResult", undefined);
            $window.localStorage.setItem("ShowPropayMsg", undefined);
            $window.localStorage.removeItem("PaymentAgainstInvoice");
            $window.localStorage.removeItem("PaymentAgainstInvoice");
        }
    }

    PC.SavePayment = function () {
        $scope.showLoader = true;
        paymentService.savePayment(PC.MdlPayment).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   //swal("SUCCESS", 'Payment saved successfully', "success");
                   PC.showSuccessMessage = true;
                   PC.showTransctionMessage = true;
                   $window.localStorage.removeItem("MdlPayment");
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    PC.SavePayment_WithoutInvoice = function () {
        $scope.showLoader = true;
        paymentService.payment_WithoutInvoice(PC.MdlPayment).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   PC.SearchResult = 'Success';
                   // swal("SUCCESS", 'Payment saved successfully', "success");
                   PC.ShowPayment = true;
                   PC.showTransctionMessage = true;
                   PC.showSuccessMessage = true;
                   $window.localStorage.removeItem("MdlPayment");
               }
               else {
                   swal("ERROR", "Something went wrong please contact to administrator.", "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    //******************************************************************************
    //*************   function to get list of all property for the logged in manager

    PC.GetPropertByManagerAndOwner = function () {
        $scope.showLoader = true;
        paymentService.getPropertByManager(PC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {
                           PC.lstPropertByManagerAndOwner.push(value[0])
                       });
                       // PC.lstPropertByManagerAndOwner = response.Data;

                       //Below line is for if this make-payment page is requested from any property's profile then check the property and get it selected
                       if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum != -1) {

                           var selectProperty = PC.lstPropertByManagerAndOwner.filter(function (prop) { return prop.propertyId == parseInt($routeParams.PropertyNum) });
                           PC.selectProperty = selectProperty[0];

                           PC.GetPropertyTenantOwnerByPropId(PC.selectProperty);
                       }
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    //******************************************************************************
    //*************   function to get list of all property for owner
    PC.GetPropertByOwner = function () {
        $scope.showLoader = true;
        paymentService.getPropertByOwner(PC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   // console.log('response.Data', response.Data);
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       PC.lstPropertByManagerAndOwner = response.Data;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    PC.GetPropertByTenant = function () {
        $scope.showLoader = true;
        paymentService.getPropertByTenant(PC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       PC.lstPropertByManagerAndOwner = response.Data;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    PC.GetPropertyTenantOwner = function (isManager, propertyId) {
        $scope.showLoader = true;
        
        //Below line is for if this make payment page is requested from any property's profile then get the owner/Tenant of selected property
        if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum != -1) {
            propertyId = $routeParams.PropertyNum;
        }

        paymentService.getPropertyTenantOwner(PC.managerid, isManager, propertyId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   PC.lstPropertyOwnerTenant = response.Data;


                   if (PersonTypeEnum.property_manager != $rootScope.userData.personType) {
                       PC.lstPropertyOwnerTenantByPropertyId = PC.lstPropertyOwnerTenant;

                       var modalArr = PC.lstPropertyOwnerTenantByPropertyId.filter(function (person) { return person.PersonType == "Manager" });
                       PC.selectClient = modalArr[0];
                   }

                   //Below line is for if this make payment page is requested from any property's profile then get the property owner/tenants
                   if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum != -1) {
                       PC.GetPropertyTenantOwnerByPropId(PC.selectProperty);
                   }

               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    PC.GetPropertyTenantOwnerByPropId = function (selected) {
        PC.selectClient = '';
        PC.selectInvoice = '';
        PC.lstInvoice_WithAmount = [];

        PC.propayProfile_Merchant = "";

        if (selected.propertyId != undefined) {
            if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {

                PC.lstPropertyOwnerTenantByPropertyId = PC.lstPropertyOwnerTenant.filter(function (Property) { return Property.PropertyId == selected.propertyId });
            }
            else {
                PC.GetPropertyTenantOwner(false, selected.propertyId);
            }
            PC.PropertyID = selected.propertyId;
            PC.GetRecurringData(selected.propertyId);
        }
        else {
            PC.lstPropertyOwnerTenantByPropertyId = [];
        }

    };

    PC.PayWithCard = function () {

        PC.IsPayWithCard = true;
        PC.MdlPayment.IsPayWithCard = true;
    }

    PC.GetPaymentUrl = function () {
        $scope.showLoader = true;
        PC.MdlPayment.amount = PC.amount;
        PC.MdlPayment.isRecurringPayment = PC.IsRecurringPayment;
        PC.MdlPayment.startOnRecurring = PC.StartOnRecurring;
        PC.MdlPayment.paymentSchedule = PC.SelectedSchedule;
        PC.MdlPayment.selectPaymentType = PC.selectPaymentType;
        if (PC.paymentCategory != '' && PC.paymentCategory != undefined) {
            PC.MdlPayment.paymentCategoryId = PC.paymentCategory.id;
        }
        if (!$rootScope.PaymentAgainstInvoice) {
            PC.MdlPayment.propertyId = PC.selectProperty.propertyId;

            if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {

                PC.MdlPayment.clientId = PC.selectClient.personId;
                PC.MdlPayment.managerEmail = $rootScope.userData.userName;

                if (PC.selectClient.PersonType == "Owner") {
                    PC.MdlPayment.RecipientType = PC.PersonTypeEnum.Owner;
                }
                else {
                    PC.MdlPayment.RecipientType = PC.PersonTypeEnum.Tenant;
                }
                PC.MdlPayment.PayerType = PC.PersonTypeEnum.Manager; // Person type is manager
            }
            else {

                //20190420 date comment
                if ($rootScope.userData.personTypeId == 3) {
                    PC.MdlPayment.manageId = $rootScope.userData.personId;
                    PC.MdlPayment.PersonId = $rootScope.userData.personId;
                }
                else if ($rootScope.userData.personTypeId == 2) {
                    PC.MdlPayment.manageId = $rootScope.userData.poid;
                    PC.MdlPayment.PersonId = $rootScope.userData.personId;
                }

                ///
                PC.MdlPayment.clientId = $rootScope.userData.ManagerDetail.pId;
                PC.MdlPayment.managerEmail = $rootScope.userData.userName;
                PC.MdlPayment.RecipientType = PC.PersonTypeEnum.Manager; // receiver of payment
                PC.MdlPayment.PayerType = $rootScope.userData.personTypeId; // Person type Owner/Tenant
            }
        }
        else {
            PC.MdlPayment.managerEmail = $rootScope.userData.userName;
        }
        if (PC.selectPaymentType == 2) {
            if (PC.MdlPayment.RecipientType == 1) {
                PC.MdlPayment.PersonId = $rootScope.userData.personManageriD;
            }
            PC.CashPayment(PC.MdlPayment);
        }
        else {    // pay using propay
            PC.PaymentUsingPropay(PC.MdlPayment);

        }
    };

    PC.PaymentUsingPropay = function (MdlPayment) {
        paymentService.propayPayment_GetUrl(MdlPayment).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.transctionResult[1] == 'success') {
                       PC.MdlPayment.transactionIdentifier = response.Data.transctionResult[2];
                       PC.MdlPayment.transactionHistoryId = response.Data.TransactionHistoryId;
                       $window.localStorage.setItem("MdlPayment", JSON.stringify(PC.MdlPayment));
                       PC.ShowPayment = false;
                       $scope.PaymentUrl = $sce.trustAsResourceUrl(response.Data.transctionResult[0]);
                       console.log(" $scope.PaymentUrl", $scope.PaymentUrl);
                   }
                   else {
                       swal("ERROR", response.Data.transctionResult[0], "error");
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {

               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });

    }
    PC.CashPayment = function () {

        PC.MdlPayment.paymentMode = 'Cash';
        if (PC.MdlPayment.withInvoice == 'invoice')
        { PC.SavePayment(); }
        else {
            PC.SavePayment_WithoutInvoice();
        }
    }

    PC.GetAllInvoiceWithTotalAmt = function () {
        if (PC.selectClient == undefined || PC.selectClient == '') {
            return;
        }
        $scope.showLoader = true;
        paymentService.getAllInvoiceWithTotalAmt(PC.selectProperty.propertyId, PC.selectClient.personId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   PC.lstInvoice_WithAmount = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    PC.GetInvoiceNumberByPropertyAndPerson = function () {

        $scope.showLoader = true;
        PC.lstInvoiceNumber = [];

        paymentService.getInvoiceNumberByPropertyAndPerson(PC.selectProperty.propertyId, PC.selectClient.personId, PC.selectClient.personId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   PC.lstInvoiceNumber = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    PC.ChangePaybleAmt = function () {

        if ($rootScope.PaymentAgainstInvoice) {
        }
        else {
            PC.remainAmount = parseFloat(PC.selectInvoice.remainAmount);
        }
        if (parseFloat(PC.amount) > parseFloat(PC.remainAmount)) {
            var str = PC.amount.toString();
            str = str.slice(0, -1);
            PC.amount = parseInt(str);
        }
    }

    PC.ChangeInvoice = function (invoice) {
        if (invoice == "") {
            PC.amount = "";
            PC.MdlPayment.invoiceNumber = "";
            PC.MdlPayment.amount = "";
            PC.MdlPayment.invoiceId = "";
        }
        else {
            PC.amount = invoice.remainAmount;
            PC.MdlPayment.amount = invoice.remainAmount;
            PC.MdlPayment.invoiceNumber = invoice.number;
            PC.MdlPayment.invoiceId = invoice.id;
        }
    }

    PC.RedirctSamepage = function () {
        PC.ShowPayment = true;
    }

    PC.test = function () {
        var data = {
            "PayerAccountId": "4485110779140380",
            "PaymentMethodType": "Visa",
            "AccountNumber": "4747474747474747",
            "ExpirationDate": "0820",
            "AccountCountryCode": "USA",
            "AccountName": "Janis Joplin",
            "BillingInformation":
             {
                 "Address1": "123 ABC St",
                 "Address2": "Apt. A",
                 "Address3": null,
                 "City": "Some Place",
                 "Country": "USA",
                 "Email": null,
                 "State": "AK",
                 "TelephoneNumber": null,
                 "ZipCode": "12345"
             },
            "Description": "MyVisaCard",
            "Priority": 0,
            "DuplicateAction": null,
            "Protected": false
        };

        paymentService.PaymentMethodId(data.PayerAccountId, data).success(function (response) {
        }).error(function (data, status) {
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    PC.CancelPayment = function () {

        if (!angular.isUndefined($routeParams.PropertyNum) && $routeParams.PropertyNum > 0) {
            $location.path('/propertyProfile/' + $routeParams.PropertyNum);
        }
        else if (!angular.isUndefined($routeParams.page)) {
            $location.path('/' + $routeParams.page + '/');
        }
        else {
            $location.path('/summary');
        }

    }

    PC.ShowScheduledPayment = function () {
        if (PC.PropertyID == 0) {
            PC.GetRecurringData(0);
            $("#mdl_ShowRecurringPayment").modal('show');
            // swal("ERROR", "Please select Property", "info");
        }
        else {
            $("#mdl_ShowRecurringPayment").modal('show');
        }
    }

    PC.CloseMdl_ScheduledPayment = function () {
        $("#mdl_ShowRecurringPayment").hide();
    }

    PC.GetRecurringData = function (propertyId) {
        $scope.showLoader = true;
        recurringBillService.recurringBill($rootScope.userData.personTypeId, $rootScope.userData.personId, propertyId).
        success(function (data) {
            PC.recurringBill = data.Data;
            $scope.showLoader = false;
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;

        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    PC.ChangePaymentMode = function (paymentCategory) {
        if (paymentCategory.id == 8 || paymentCategory.id == 34) {
            PC.selectPaymentType = 1;
        }
    }

    PC.LoadMobilePayment = function () {
        // location.href = "/Home/RedirectToMobileSchema?Result=" + "result" + "&Message=" + "mesg";
        if ($location.search().result != undefined) {
            var arr = JSON.stringify($location.search());
            console.log("arr", arr);
            var Result = JSON.stringify($location.search().result);
            var Message = JSON.stringify($location.search().message);
            alert(Result);
         
        }
    }
}
