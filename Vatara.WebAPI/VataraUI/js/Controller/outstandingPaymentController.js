﻿vataraApp.controller('outstandingPaymentController', outstandingPaymentController);
outstandingPaymentController.$inject = ['$scope', 'outstandingPaymentService', 'serviceCategory_service', 'masterdataService', 'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService'];
function outstandingPaymentController($scope, outstandingPaymentService, serviceCategory_service, masterdataService, pagingService, $filter, $routeParams, $location, $rootScope, commonService) {
    var OPC = this;

    OPC.managerid = $rootScope.userData.personId;
    OPC.isManager = true;
    $scope.menuActive = 'liabilities';
    $scope.menuUrl = 'liabilities';
    OPC.lstVendorRemainPayment = [];
    OPC.CopylstVendorRemainPayment = [];
    OPC.isPaid = false;
    OPC.vendorNotes = '';
    OPC.invoiceDetail = '';
    OPC.currentPage = 0;
    OPC.PageService = pagingService;

    OPC.ItemCountPerPage = _ItemsPerPage;
    OPC.itemsPerPageArr = OPC.PageService.itemsPerPageArr;

    OPC.OrderByFeild = "";
    OPC.FilterFeild = "";
    
    OPC.lstDetails = [];
    $scope.selectCurrentPeriod = "";
    $scope.fromDate = new Date();
    $scope.toDate = new Date();
    OPC.currentDate = new Date();
    OPC.selectDate = new Date();
    OPC.filterCategory = 'unpaid';

    $scope.lstPropertByManagerAndOwner = [];
    $scope.lstPropertOwnersByManager = [];
    var currentDates = new Date();
    OPC.CopylstPropertOwnersByManager = [];    
    OPC.CopylstPropertByManagerAndOwner = [];
    OPC.personType = $rootScope.userData.personTypeId;
    OPC.personId = $rootScope.userData.personId;
    OPC.ThiredPartyLiabitilyChartArray = [];
    
    $scope.DetailFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    }

    OPC.LoadFunction = function () {
        OPC.isPaid = false;
        OPC.vendorNotes = '';

        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            OPC.isManager = true;            
        }
        else {
            OPC.isManager = false;
        }
        OPC.getPersonIdAndPersonType();
        $scope.lstCurrentPeriod = angular.copy(Dateranges);
        $scope.selectCurrentPeriod = $scope.lstCurrentPeriod[0];
        OPC.GetPropertOwnersByManager();
        OPC.GetPropertByManagerAndOwner();
        $scope.FilterDetailsByDate();
    };

    OPC.filterCategoryChanged = function()
    {        
        OPC.filterCategory = OPC.filterCategory;
        OPC.GetVendorRemainPayment(OPC.selectDate, OPC.currentDate);
    }

    OPC.GetVendorRemainPayment = function (fromDate, toDate) {
        $scope.showLoader = true;
        var PMID = $rootScope.userData.ManagerDetail.pId;
        outstandingPaymentService.getVendorRemainPayment(OPC.managerid, PMID,fromDate, toDate, OPC.personType, OPC.personId, OPC.filterCategory).
         success(function (response) {
             if (response.Status == true) {
                 var property = $scope.DetailFilterParameters.property;

                 OPC.CopylstVendorRemainPayment = response.Data.vendorRemainAMT;

                 OPC.lstVendorRemainPayment = OPC.CopylstVendorRemainPayment.filter(function (invoiceDetails) { return invoiceDetails.propertyID == property.propertyId });
                
                 if ((OPC.managerid == OPC.personId && OPC.personType == $rootScope.PersonTypeIdEnum.property_manager) || OPC.personId == 0) {
                     $scope.DetailFilterParameters.owner = OPC.CopylstPropertOwnersByManager[0];
                   //  $scope.DetailFilterParameters.property = OPC.CopylstPropertByManagerAndOwner[0];
                    // $scope.lstPropertByManagerAndOwner = OPC.CopylstPropertByManagerAndOwner;

                     OPC.CreateThiredPartyLiabitilyChart(response.Data.lstMdlManagerLiability);
                 }
                 else {

                    // $scope.lstPropertByManagerAndOwner = response.Data.MdlPropTenantOwnerDetail;
                     //var addAll = angular.copy(response.Data.MdlPropTenantOwnerDetail[0]);
                     //addAll.propertyName = "All";
                     //addAll.propertyId = 0;
                     //$scope.lstPropertByManagerAndOwner.unshift(addAll);
                     //$scope.DetailFilterParameters.property = $scope.lstPropertByManagerAndOwner[0];
                 }            
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    };

    $scope.sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop];
        }, 0);
    };

    OPC.CreateThiredPartyLiabitilyChart = function (modal) {

       // console.log('modal 12222', modal);       
        OPC.ThiredPartyLiabitilyChartArray = [];
        OPC.groupByCategory = $filter('groupBy')(modal, 'liabilityType');
        angular.forEach(OPC.groupByCategory, function (value, key) {

            var innergroup = $filter('groupBy')(value, 'serviceCategoryName');
            angular.forEach(innergroup, function (innervalue, key2) {
                var serviceCategoryTotal = $scope.sum(innervalue, 'amount');
                var Arr = [];
                Arr.push(key2, serviceCategoryTotal);
                OPC.ThiredPartyLiabitilyChartArray.push(Arr);
            });           
            //else if (key == 'Liability') {
            //    var innergroup = $filter('groupBy')(value, 'Type');
            //    //console.log('Expense innergroup', innergroup);
            //    angular.forEach(innergroup, function (innerValue, key2) {
            //        //console.log('innerValue', innerValue);
            //        var LiabitilyInnergroup = $filter('groupBy')(innerValue, 'CategoryName');
            //        angular.forEach(LiabitilyInnergroup, function (innerValue2, key3) {
            //            var total = $scope.sum(innerValue2, 'Amount');
            //            //console.log('total', total);
            //            var ExpenseArr = [];
            //            ExpenseArr.push(key3, total);
            //            FC.LiabitilyChartArray.push(ExpenseArr);
            //        });
            //    });
            //    // console.log('FC.ExpenseChartArray', FC.ExpenseChartArray);
            //}
        });
        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            OPC.ThiredPartyLiabitilyChartLoad();
        }
    };

    OPC.ThiredPartyLiabitilyChartLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });
        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawThiredPartyLiabilityChart);

        function drawThiredPartyLiabilityChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(OPC.ThiredPartyLiabitilyChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Thired Party Liability'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('ThiredPartyLiabilityChart'));
            chart.draw(data, options);
        }
    }

    OPC.Filter = function (items, propertyId) {
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.propertyId == propertyId) {
                retArray.push(obj);
            }
        });
        return retArray;
    }
    
    $scope.FilterDetailsByDate = function (checkValue) {

        OPC.currentPage = 0;
        if ($scope.selectCurrentPeriod != null) {
           
            if ($scope.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {
                OPC.lstDetails = [];
                return;
            }
            OPC.currentDate = new Date();
          
            if ($scope.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {
                OPC.currentDate = $scope.toDate;
                OPC.selectDate = $scope.fromDate;
            }
            else {
                if ($scope.selectCurrentPeriod.Period == 'Annual') {
                    OPC.selectDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {
                    //selectDate =  $scope.selectCurrentPeriod.date;
                    OPC.selectDate = $scope.selectCurrentPeriod.Startdate;
                    OPC.currentDate = $scope.selectCurrentPeriod.EndDate;
                }
            }
            $scope.DetailFilterParameters.owner = undefined;
            $scope.DetailFilterParameters.property = undefined;

           // OPC.personId = OPC.managerid;
            //OPC.personType = $rootScope.userData.personTypeId;
            OPC.getPersonIdAndPersonType();
            OPC.GetVendorRemainPayment(OPC.selectDate, OPC.currentDate);
        }
        else {           
            OPC.lstVendorRemainPayment = [];
            OPC.CopylstVendorRemainPayment = [];
            $scope.DetailFilterParameters.owner = undefined;
            $scope.DetailFilterParameters.property = undefined;
        }
    };

    OPC.GetPropertOwnersByManager = function () {
        $scope.showLoader = true;
        outstandingPaymentService.getPropertOwnersByManager(OPC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                      // console.log('response.Data 112233', response.Data);
                       $scope.lstPropertOwnersByManager = [];
                       OPC.CopylstPropertOwnersByManager = [];
                       var data = $filter('groupBy')(response.Data, 'ownerId');
                       angular.forEach(data, function (value, key) {
                           $scope.lstPropertOwnersByManager.push(value[0])
                       });
                       var addAll = angular.copy($scope.lstPropertOwnersByManager[0]);
                       addAll.ownerFullName = "All";
                       addAll.ownerId = 0;
                       $scope.lstPropertOwnersByManager.unshift(addAll);
                       $scope.DetailFilterParameters.owner = addAll;
                       OPC.CopylstPropertOwnersByManager = $scope.lstPropertOwnersByManager;
                     // console.log('$scope.lstPropertOwnersByManager', $scope.lstPropertOwnersByManager);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;              
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    OPC.GetPropertByManagerAndOwner = function () {
        $scope.showLoader = true;
        outstandingPaymentService.getPropertByManagerAndOwner(OPC.managerid, OPC.isManager).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {
                       $scope.lstPropertByManagerAndOwner = [];
                       OPC.CopylstPropertByManagerAndOwner = [];
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {
                           $scope.lstPropertByManagerAndOwner.push(value[0])
                       });
                       var addNewOption_All = angular.copy($scope.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerFullName = " All";
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       $scope.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       $scope.DetailFilterParameters.property = addNewOption_All;
                       OPC.CopylstPropertByManagerAndOwner = $scope.lstPropertByManagerAndOwner;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;              
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    OPC.getPersonIdAndPersonType = function () {

        OPC.personType = $rootScope.userData.personTypeId;

        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            OPC.personId = 0;
        }
        else {
            OPC.personId = $rootScope.userData.personId;
        }        
    }

    $scope.FilterProperty = function (model) {

        //console.log('model 44', model);
        OPC.currentPage = 0;      
        OPC.personType = model.personTypeId;
        OPC.personId = model.ownerId;
        if (model.ownerId == 0)
        {
            OPC.getPersonIdAndPersonType();
        }
        OPC.GetVendorRemainPayment(OPC.selectDate, OPC.currentDate);
        if (model.ownerId == 0) {
            OPC.lstVendorRemainPayment = angular.copy(OPC.CopylstVendorRemainPayment);
            $scope.DetailFilterParameters.property = $scope.lstPropertByManagerAndOwner[0];
            $scope.lstPropertByManagerAndOwner = OPC.CopylstPropertByManagerAndOwner;
        }
        else {

            OPC.lstVendorRemainPayment = OPC.CopylstVendorRemainPayment.filter(function (invoiceDetails) { return invoiceDetails.to === model.ownerId });
            if ($scope.DetailFilterParameters.owner != null) {
                $scope.lstPropertByManagerAndOwner = OPC.Filter(OPC.CopylstPropertByManagerAndOwner, $scope.DetailFilterParameters.owner.propertyId);
                var addNewOption_All = angular.copy($scope.lstPropertByManagerAndOwner[0]);
                addNewOption_All.ownerId = 0;
                addNewOption_All.propertyId = 0;
                addNewOption_All.propertyName = " All";
                $scope.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                $scope.DetailFilterParameters.property = addNewOption_All;
            }
            else {
                $scope.lstPropertByManagerAndOwner = OPC.CopylstPropertByManagerAndOwner;
                $scope.DetailFilterParameters.property = $scope.lstPropertByManagerAndOwner[0];
            }
        }
    };

    $scope.FilterLiabilitiesByProperty = function (model) {
        OPC.currentPage = 0;       
        if (model.propertyId == 0 && $scope.DetailFilterParameters.owner.ownerId != 0) {            
            OPC.lstVendorRemainPayment = OPC.CopylstVendorRemainPayment.filter(function (invoiceDetails) { return invoiceDetails.to === $scope.DetailFilterParameters.owner.ownerId });
        }
        else if (model.propertyId == 0 && $scope.DetailFilterParameters.owner.ownerId == 0) {            
            OPC.lstVendorRemainPayment = OPC.CopylstVendorRemainPayment;
        }
        else if (model.propertyId != 0 && $scope.DetailFilterParameters.owner.ownerId == 0) {           
            OPC.lstVendorRemainPayment = OPC.CopylstVendorRemainPayment.filter(function (invoiceDetails) { return invoiceDetails.propertyID == model.propertyId});
        }
        else {           
            OPC.lstVendorRemainPayment = OPC.CopylstVendorRemainPayment.filter(function (invoiceDetails) { return invoiceDetails.propertyID === model.propertyId && invoiceDetails.to == $scope.DetailFilterParameters.owner.ownerId });
        }
    }
     
    OPC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    };

    $scope.ChangeCustomToDate = function () {
        $scope.toDate = $scope.fromDate;
    }

    $scope.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    OPC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    };   

    OPC.OpenModal = function (invoiceDetail) {
        OPC.invoiceDetail = invoiceDetail;
        $("#mdlOutstandingPay").modal('show');
    };

    OPC.hideModal = function () {
        OPC.isPaid = false;
        OPC.vendorNotes = '';
        $("#mdlOutstandingPay").modal('hide');
    };

    OPC.SavePayment = function () {
        OPC.invoiceDetail.Vendor_Notes = OPC.vendorNotes;

        $scope.showLoader = true;
        outstandingPaymentService.savePayment(OPC.invoiceDetail).
         success(function (response) {
             if (response.Status == true) {
                 swal("SUCCESS", "Payment saved successfully", "success");
                 OPC.hideModal();
                 OPC.LoadFunction();
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;
            
         }).finally(function (data) {
             $scope.showLoader = false;
         });
    };

}