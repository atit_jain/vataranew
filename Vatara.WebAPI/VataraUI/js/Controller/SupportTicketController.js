﻿vataraApp.controller("SupportTicketController", SupportTicketController);
SupportTicketController.$inject = ['$scope', 'SupportTicketService', 'pagingService', '$filter', '$location', '$rootScope', 'commonService'];
function SupportTicketController($scope, SupportTicketService, pagingService, $filter, $location, $rootScope, commonService) 
{

    $scope.PMID = 1;
    includeClosedStatus = false;
    section = "";
    $scope.ShowDiv = true;
    $scope.ticket =
        {
            CompanyMgrId: 1,
            userid: $rootScope.userData.personId,
            subject: '',
            description: '',
            FromEmail:'',
        }

    $scope.toggleOpen = function ()
    {
        $scope.isseventhOpen = !$scope.isseventhOpen;
    }

    $scope.AddNewTicket = function ()
    {
        
        $("#addNewTicket").modal('show');
    }

    $scope.SaveSupportTicket = function (frmSupportTicket)
    {
        $scope.showLoader = true;
        $scope.ticket.FromEmail = $rootScope.userData.userName;
            SupportTicketService.SaveSupportTicket($scope.ticket).
             success(function (response)
             {
                 if (response.Status == true)
                 {
                     $scope.CloseSupporttickets(frmSupportTicket);
                     $scope.GetAllSupportTicket();
                     
                 }
                 else
                 {
                     swal("ERROR", MessageService.responseError, "error");
                 }
                 $scope.showLoader = false;
             }).error(function (data, status) {
                 if (status == 401) {
                     commonService.SessionOut();
                 }
                 else {
                     swal("ERROR", MessageService.serverGiveError, "error");
                 }
                 $scope.showLoader = false;

             }).finally(function (data) {
                 $scope.showLoader = false;
             });
    }

    $scope.GetAllSupportTicket = function (IncludeClosedStatus)
    {
        $scope.showLoader = true;
        SupportTicketService.GetAllSupportTicket($rootScope.userData.personId, IncludeClosedStatus).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true)
               {
                   $scope.LstTickets = angular.copy(response.Data);
                   $scope.GetAllFaq(section);
               }
               else
               {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status)
           {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.UpdateTicketStatus = function(TicketId)
    {
        $scope.showLoader = true;
        SupportTicketService.UpdateTicketStatus(TicketId).
           success(function (response)
           {
               $scope.showLoader = false;
               if (response.Status == true)
               {
                   swal("SUCCESS", "Status has been updated", "success");
                   $scope.GetAllSupportTicket();
               }
               else
               {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }
    $scope.CloseSupporttickets = function (frmSupportTicket)
    {
        $scope.ticket.subject = '';
        $scope.ticket.description = '';
        frmSupportTicket.$setPristine();
        $("#addNewTicket").modal('hide');        
    }


    $scope.ToggalAccordion = function ()
    {
        $scope.ShowDiv = !$scope.ShowDiv;
    }

    $scope.ShowAllTickets = function()
    {
        if(!$scope.include)
        {
            $scope.GetAllSupportTicket(true)
        }
        else
        { $scope.GetAllSupportTicket(false) }
    }

    $scope.GetAllFaq = function (section)
    {
        $scope.showLoader = true;
        SupportTicketService.GetAllFaq(section).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.LstFAQ = angular.copy(response.Data);

                   $scope.status = { isItemOpen: new Array($scope.LstFAQ.length), isFirstDisabled: false };
                   $scope.status.isItemOpen[0] = true;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }
    }
