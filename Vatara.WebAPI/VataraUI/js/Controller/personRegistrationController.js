﻿vataraApp.controller('personRegistrationController', personRegistrationController);
personRegistrationController.$inject = ['$scope', '$routeParams', 'loginService', '$window', '$location', '$rootScope', 'commonService', 'propayRegService', 'CompanyService', '$route', 'personService'];
function personRegistrationController($scope, $routeParams, loginService, $window, $location, $rootScope, commonService, propayRegService, CompanyService, $route, personService) {
    $scope.showLoader = false;
    $scope.Country = [];
    $scope.State = [];
    $scope.County = [];
    $scope.City = [];
    $scope.isURLEdit = false;
    $scope.vataradomain = '.vatara.com';

    $scope.CompanyCounty = [];
    $scope.CompanyCity = [];
    $scope.newAdmin = false;
    $scope.CompanyPhoto = [];
    $scope.pId = 0;
    $scope.PasswordError = '';
    //$scope.haveRegisteredCompany = false;

    $scope.isExist = false;
    $scope.isCompanyURLExist = false;
    $scope.isCompanyEmailExist = false;
    $scope.emailDisable = false;
    $scope.showMessage = false;
    $scope.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    }
    $scope.CopyMdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    }

    $scope.mdlCompanyCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    }

    $scope.eml_addess = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    $scope.selectPersonType = PersonTypeEnum.property_manager;

    $scope.Image = '';
    $scope.mdlPerson = {
        id: "",
        personType: $scope.selectPersonType,
        firstName: "",
        middleName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
        phoneNo: "",
        fax: "",
        line1: "",
        line2: "",
        countryId: "",
        stateId: "",
        cityId: "",
        countyId: "",
        zipCode: "",
        profileImage: null,
        Image: null,

        company_Name: null,
        company_logo: DefaultCompanyLogo,
        company_addressId: 0,
        company_email: null,
        company_phone: null,
        company_fax: null,
        company_url: null,
        company_countryId: 0,
        company_stateId: 0,
        company_countyId: 0,
        company_cityId: 0,
        company_zipCode: null,
        company_line2: '',
        company_line1: '',
        haveRegisteredCompany: false,
        pmid: 0,
    };

    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = false;
    $scope.compZipValid = false;
    $scope.disableSaveButton = false;
    $scope.Photo = [];
    $scope.isSub = false;

    $scope.showSubscriptionPlan = false;
    $scope.lstplans = [];
    $scope.lstplans_Filtered = [];
    $scope.customer = [];
    $scope.selectedplan = '';
    $scope.selectedcustomerId = 0;
    $scope.mdlPerson.email = '';
    $scope.mdlPerson.password = '';
    $scope.NewSubscription = false;

    $scope.ExpiryMonth = 0;
    $scope.ExpiryYear = 0;
    $scope.subscription = {
        subscription_id: 0,
        name: '',
        customerId: 0,
        amount: 0,
        product_id: '',
        planCode: '',
        currency_code: '',
        currency_symbol: '',
        CardID: '',
        CurrencyCode: '',
        IntervalUnit: '',
        plan: [],
        customer: [],
        mdlCard: []
    }

    $scope.Payment = {
        PaymentMode: '',
        FirstName: '',
        CVV: '',
        ExpireDate: '',
        LastDight: '',
        AccountNumber:'',
        AutoRenewal: false
    }

    $scope.plan = {
        billing_cycles: 0,
        created_time: "",
        interval: 0,
        interval_unit: "",
        name: "",
        plan_code: "",
        product_id: "",
        recurring_price: 0,
        setup_fee: 0,
        status: "",
        trial_period: 0,
        updated_time: ""
    }

    $scope.mdlCard = {
        customer_id: '',
        status: '',
        gateway: '',
        first_name: '',
        last_name: '',
        iin: '',
        last4: '',
        card_type: '',
        expiry_month: '',
        expiry_year: 0,
        masked_number: '',
        card_number: '',
        cvv_number: '',
        street: '',
        city: '',
        state: '',
        zip: 0,
        country: ''
    }

    $scope.LoadFunction = function () {
        $scope.newAdmin = false;
        $scope.GetCountry(function (data) {
            $scope.GetStates(function (data) {
                //if ($scope.mdlPerson.profileImage != null && $scope.mdlPerson.profileImage != undefined) {
                //    $scope.mdlPerson.Image.base64 = $scope.mdlPerson.profileImage;
                //}
                $('#showmsg').hide();
                if ($rootScope.userData != null) {

                    $scope.pZipValid = true;
                    if ($location.path().indexOf('/owners/') > -1) {
                        if (!angular.isUndefined($routeParams.ownerId)) {
                            $scope.GetRegistrationData($routeParams.ownerId);   //ownerId
                        }
                        else {
                            $window.location.reload();
                        }
                    }
                    else {
                        $scope.GetRegistrationData($rootScope.userData.personId);
                    }

                }
            });
        });
    }

    //-----------------Country State City

    $scope.ModelClose = function (frmRegister) {
        frmRegister.$setPristine();
        $('#personRegistration').modal('hide');

        if ($rootScope.userData != undefined) {
            if (PersonTypeEnum.tenant == $rootScope.userData.personType) {
                $('#mdlTenantProfile').modal('hide');
            }
            else if (PersonTypeEnum.owner == $rootScope.userData.personType) {
                $('#mdlOwnerProfile').modal('hide');
            }
            else {
                $location.path("/home");
            }
        }
        $scope.CreateModel();
    }

    $scope.CreateModel = function () {

        if ($rootScope.userData != undefined) {
            $scope.mdlCountryState = {
                selectedCountry: $scope.CopyMdlCountryState.selectedCountry,
                selectedState: $scope.CopyMdlCountryState.selectedState,
                selectedCounty: $scope.CopyMdlCountryState.selectedCounty,
                selectedCity: $scope.CopyMdlCountryState.selectedCity
            }
            $scope.GetRegistrationData($rootScope.userData.personId);
        }
        else {

            //$scope.mdlCountryState = {
            //    selectedCountry: undefined,
            //    selectedState: undefined,
            //    selectedCounty: undefined,
            //    selectedCity: undefined
            //}

            $scope.mdlCountryState = {};
            $scope.mdlPerson = {
                id: "",
                personType: PersonTypeEnum.property_manager,
                firstName: "",
                middleName: "",
                lastName: "",
                email: "",
                password: "",
                confirmPassword: "",
                phoneNo: "",
                fax: "",
                line1: "",
                line2: "",
                countryId: "",
                stateId: "",
                cityId: "",
                countyId: "",
                zipCode: "",
                profileImage: null,
                //Image: defaultProfileImage,
                Image: "",
                company_Name: null,
                company_logo: DefaultCompanyLogo,
                company_addressId: 0,
                company_email: null,
                company_phone: null,
                company_fax: null,
                company_url: null,
                company_countryId: 0,
                company_stateId: 0,
                company_countyId: 0,
                company_cityId: 0,
                company_zipCode: null,
                company_line2: '',
                company_line1: '',
                haveRegisteredCompany: false,
                pmid: 0
            };
        }
    }

    $scope.ImageSelected = function () {
        //console.log('$scope.mdlPerson', $scope.mdlPerson);
        //console.log('$scope.mdlPerson111', $scope.mdlPerson);
    }

    $scope.GetRegistrationData = function (personId) {

        if ($rootScope.userData.personId > 0) {
            $scope.showLoader = true;
            loginService.GetRegistrationData(personId).    //$rootScope.userData.personId
              success(function (response) {
                  $scope.showLoader = false;
                  if (response.Status == true) {
                      $scope.mdlPerson = angular.copy(response.Data);
                      $scope.emailDisable = true;

                      if ($scope.mdlPerson.profileImage != null && $scope.mdlPerson.profileImage != undefined) {
                          output = document.getElementById('divimage1');
                          if (output != null) {
                              var random = Math.floor(100000 + Math.random() * 900000);
                              output.style.backgroundImage = 'url(../Uploads/Person/Photos/' + $scope.mdlPerson.id + '-imageProfileImagePersonId_' + $scope.mdlPerson.id + '.png?cb=' + random + ')';
                          }
                      }
                      var country = $scope.Country.filter(function (country) { return country.Id == $scope.mdlPerson.countryId });

                      if (country.length > 0) {
                          $scope.mdlCountryState.selectedCountry = country[0];
                          $scope.CopyMdlCountryState.selectedCountry = $scope.mdlCountryState.selectedCountry;
                      }
                      var state = $scope.State.filter(function (state) { return state.Id == $scope.mdlPerson.stateId });

                      if (state.length > 0) {
                          $scope.mdlCountryState.selectedState = state[0];
                          $scope.CopyMdlCountryState.selectedState = $scope.mdlCountryState.selectedState;
                      }

                      $scope.GetCountyByState($scope.mdlPerson.stateId, function (data) {

                          $scope.County = angular.copy(data);
                          var county = $scope.County.filter(function (county) { return county.id == $scope.mdlPerson.countyId });
                          if (county.length > 0) {
                              $scope.mdlCountryState.selectedCounty = county[0];
                              $scope.CopyMdlCountryState.selectedCounty = $scope.mdlCountryState.selectedCounty;
                          }
                          $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, $scope.mdlPerson.countyId, function (data) {
                              $scope.City = angular.copy(data);
                              var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlPerson.cityId });
                              if (city.length > 0) {
                                  $scope.mdlCountryState.selectedCity = city[0];
                                  $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
                              }
                          });
                      });
                  }
                  else {
                      swal("ERROR", "Error in get person profile.", "error");
                  }
              }).error(function (data) {
                  $scope.showLoader = false;
                  swal("ERROR", MessageService.responseError, "error");
              }).finally(function (data) {
                  $scope.showLoader = false;
              });
        }
    }

    $scope.GetCountry = function (callback) {
        //console.log('GetCountry called');
        $scope.showLoader = true;
        propayRegService.getCountries().
          success(function (response) {
              $scope.showLoader = false;
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.Country = angular.copy(response);

                  // console.log('$scope.Country', $scope.Country);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }


          }).error(function (data, status) {
              swal("ERROR", MessageService.serverGiveError, "error");
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetStates = function (callback) {
        //console.log('State called');
        $scope.showLoader = true;
        propayRegService.getStates().
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;
                  $scope.State = angular.copy(response);
                  // console.log('$scope.State', $scope.State);
                  callback(response);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              swal("ERROR", MessageService.serverGiveError, "error");
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCountyByStateId = function (modal) {

        $scope.mdlCountryState.selectedCounty = undefined;
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();

        $scope.GetCountyByState(modal.Id, function (data) {

            $scope.County = angular.copy(data);
            var county = $scope.County.filter(function (county) { return county.id == $scope.mdlPerson.countyId });
            if (county.length > 0) {
                $scope.mdlCountryState.selectedCounty = county[0];
                $scope.CopyMdlCountryState.selectedCounty = $scope.mdlCountryState.selectedCounty;
            }
            var countyId = 0
            if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
                countyId = $scope.mdlCountryState.selectedCounty.id;
            }
            $scope.GetCityByStateOrCounty(modal.Id, countyId, function (data) {

                $scope.City = angular.copy(data);
                var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlPerson.cityId });
                // console.log('city', city);

                if (city.length > 0) {
                    $scope.mdlCountryState.selectedCity = city[0];
                    $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
                }
            });
        });
    }

    $scope.ChangeCity = function (modal) {
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();

        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) {

            $scope.City = angular.copy(data);
            var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlPerson.cityId });
            // console.log('city', city);

            if (city.length > 0) {
                $scope.mdlCountryState.selectedCity = city[0];
                $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
            }

        });
    }

    $scope.GetCompanyCountyByStateId = function (modal, firstLoad) {

        $scope.mdlCompanyCountryState.selectedCounty = undefined;
        $scope.mdlCompanyCountryState.selectedCity = undefined;
        $scope.ChangeCompnyZip();

        $scope.GetCountyByState(modal.Id, function (data) {

            $scope.CompanyCounty = angular.copy(data);
            var countyId = 0; // Because 
            $scope.GetCityByStateOrCounty(modal.Id, countyId, function (data) {
                $scope.CompanyCity = angular.copy(data);
            });

        });
    }

    $scope.ChangeCompanyCity = function (modal) {
        $scope.mdlCompanyCountryState.selectedCity = undefined;
        $scope.ChangeCompnyZip();
        $scope.GetCityByStateOrCounty($scope.mdlCompanyCountryState.selectedState.Id, modal.id, function (data) {

            $scope.CompanyCity = angular.copy(data);
        });
    }

    $scope.GetCountyByState = function (stateId, callback) {

        $scope.showLoader = true;
        propayRegService.getCountyByState(stateId).
          success(function (response) {
              if (response != undefined) {
                  $scope.showLoader = false;

                  //if (profile == "company") {
                  //    $scope.CompanyCounty = angular.copy(response.Data);
                  //}
                  //else {
                  //**** Below code is written in   $scope.GetCountyByStateId method because callback function will return the data *****
                  // *** to the place from where this function is called.  ***********
                  //*********************************************************************************************************************
                  //$scope.County = angular.copy(response.Data);
                  //var county = $scope.County.filter(county => county.id == $scope.mdlPerson.countyId);
                  //if (county.length > 0) {
                  //    $scope.mdlCountryState.selectedCounty = county[0];
                  //    $scope.CopyMdlCountryState.selectedCounty = $scope.mdlCountryState.selectedCounty;
                  //}
                  //}
                  callback(response.Data);
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;

          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        propayRegService.getCityByStateOrCounty(stateId, countyId).
          success(function (response) {

              if (response != undefined) {
                  $scope.showLoader = false;
                  callback(response.Data);

                  //if (profile == "company") {
                  //    $scope.CompanyCity = angular.copy(response.Data);
                  //}
                  //else {
                  //    console.log('$scope.mdlPerson.cityId', $scope.mdlPerson.cityId);
                  //    $scope.City = angular.copy(response.Data);
                  //    var city = $scope.City.filter(city => city.Id == $scope.mdlPerson.cityId);
                  //    console.log('city', city);
                  //    if (city.length > 0) {
                  //        $scope.mdlCountryState.selectedCity = city[0];
                  //        $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
                  //    }
                  //}
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              $scope.showLoader = false;
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    $scope.ChangeZip = function () {
        $scope.mdlPerson.zipCode = '';
        $scope.pZipValid = false;
    }

    $scope.ChangeCompnyZip = function () {
        $scope.mdlPerson.company_zipCode = '';
        $scope.compZipValid = false;
    }

    $scope.PersonRegistration = function (frmRegister) {

        $scope.mdlPerson.countryId = $scope.mdlCountryState.selectedCountry.Id;
        $scope.mdlPerson.stateId = $scope.mdlCountryState.selectedState.Id;
        $scope.mdlPerson.cityId = $scope.mdlCountryState.selectedCity.Id;
        if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
            $scope.mdlPerson.countyId = $scope.mdlCountryState.selectedCounty.id;
        }
        else {
            $scope.mdlPerson.countyId = 0;
        }
        // $scope.mdlPerson.profileImage = JSON.stringify($scope.mdlPerson.Image);
        // $scope.mdlPerson.profileImage = '';

        if ($rootScope.userData == null && $scope.mdlPerson.haveRegisteredCompany == true) {
            $scope.mdlPerson.company_countryId = $scope.mdlCompanyCountryState.selectedCountry.Id;
            $scope.mdlPerson.company_stateId = $scope.mdlCompanyCountryState.selectedState.Id;
            $scope.mdlPerson.company_cityId = $scope.mdlCompanyCountryState.selectedCity.Id;

            if ($scope.mdlCompanyCountryState.selectedCounty != undefined && $scope.mdlCompanyCountryState.selectedCounty != null && $scope.mdlCompanyCountryState.selectedCounty != '') {
                $scope.mdlPerson.company_countyId = $scope.mdlCompanyCountryState.selectedCounty.id;
            }
            else {
                $scope.mdlPerson.company_countyId = 0;
            }
            //  $scope.mdlPerson.company_logo = JSON.stringify($scope.mdlPerson.company_logo);
            $scope.mdlPerson.company_logo = '';
        }
        if ($rootScope.userData != null) {
            $scope.mdlPerson.pmid = $rootScope.userData.ManagerDetail.pId;
        }
        $scope.showLoader = true;

        loginService.PersonRegistration($scope.mdlPerson).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.pId = response.Message;
                   $scope.customer = response.Data;
                   $window.localStorage.setItem("customer", JSON.stringify($scope.customer));
                   if ($rootScope.userData == null) {
                       if ($scope.CompanyPhoto.length > 0) {
                           $scope.SaveCompanyPhoto();
                       }
                       $scope.showSubscriptionPlan = true;
                       //$scope.showMessage = true;
                       $scope.NewSubscription = true;
                       $scope.OnLoadSubScription();

                       $scope.userId = $scope.mdlPerson.email;
                       frmRegister.$setPristine();
                       $scope.CreateModel();
                   }
                   else {
                       //console.log('$scope.Photo', $scope.Photo);
                       //console.log('$scope.Photo.length', $scope.Photo.length);
                       swal("SUCCESS", "Person Info has been Saved", "success");
                       if ($scope.Photo.length > 0) {
                           $scope.pId = $rootScope.userData.personId;
                           $scope.savePhotos();
                       }
                       if (PersonTypeEnum.tenant == $rootScope.userData.personType) {
                           $('#mdlTenantProfile').modal('hide');
                           $route.reload();
                       }
                       else if (PersonTypeEnum.owner == $rootScope.userData.personType) {
                           $('#mdlOwnerProfile').modal('hide');
                           $route.reload();
                       }
                       else if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
                        //   $location.path("/home");
                       }
                   }
                   
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.personfileSelected = function (element) {

        var myFileSelected = element.files[0];
        if (myFileSelected.type == "image/jpeg" || myFileSelected.type == "image/jpg" || myFileSelected.type == "image/gif" || myFileSelected.type == "image/png") {
            $scope.Photo[0] = myFileSelected;
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('div' + element.name);
                if (output != null)
                    output.style.backgroundImage = 'url(' + reader.result + ')';
            }
            reader.readAsDataURL(event.target.files[0]);
            $scope.$apply();
        }
        else {
            $(element).val('');
            swal("Invalid file type", " Allowed file types : jpeg, jpg, gif, png", "info");
        }
    };

    $scope.savePhotos = function () {
        var fd = new FormData();
        fd.append("file", $scope.Photo[0]);
        fd.append("personid", $scope.pId);
        fd.append("moduleid", 'PERSON');
        fd.append("seqNo", 'ProfileImagePersonId_' + $scope.pId);
        fd.append("filename", 'ProfileImagePersonId_' + $scope.pId);
        fd.append("filedesc", '');
        fd.append("filetags", 'Photo, ' + $scope.pId);
        personService.saveFiles(fd, '/api/personphoto', function (status, resp) {
            if (status) {
                // swal("SUCCESS", "Image saved successfully.", "success");                
            }
            else {
                swal("ERROR", "Unable to save image.", "error");
            }
        });
    }

    $scope.RegistrationCancel = function () {
        $location.path("/login");
    }

    $scope.EmailExist = function () {

        if ($scope.mdlPerson.email == undefined || $scope.mdlPerson.email == '') {
            return;
        }
        $scope.showLoader = true;
        if ($scope.mdlPerson.email != null && $scope.mdlPerson.email != "" && $scope.mdlPerson.email != undefined) {
            loginService.EmailExist($scope.mdlPerson).
               success(function (response) {
                   $scope.showLoader = false;
                   if (response.Status == true) {
                       $scope.isExist = response.Data;
                   }
                   else {
                       swal("ERROR", response.Message, "error");
                   }
               }).error(function (data) {
                   $scope.showLoader = false;
                   swal("ERROR", MessageService.responseError, "error");
               }).finally(function (data) {
                   $scope.showLoader = false;
               });
        }
    }

    $scope.validateCompanyURL = function () {

        if ($scope.mdlPerson.company_url != undefined && $scope.mdlPerson.company_url != null && $scope.mdlPerson.company_url != '') {
            $scope.showLoader = true;
            CompanyService.validateCompanyURL($scope.mdlPerson.company_url, $rootScope.userData.ManagerDetail.pId).
               success(function (response) {
                   $scope.showLoader = false;
                   if (response.Status == true) {
                       $scope.isCompanyURLExist = response.Data;
                       $scope.disableSaveButton = response.Data;
                   }
                   else {
                       swal("ERROR", response.Message, "error");
                   }
               }).error(function (data) {
                   $scope.showLoader = false;
                   swal("ERROR", MessageService.responseError, "error");
               }).finally(function (data) {
                   $scope.showLoader = false;
               });
        }
    }

    $scope.CompanyEmailExist = function () {

        if ($scope.mdlPerson.company_email == null || $scope.mdlPerson.company_email == "") {
            return;
        }
        $scope.showLoader = true;
        CompanyService.companyEmailExist($scope.mdlPerson.company_email).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   $scope.isCompanyEmailExist = response.Data;
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    //***************************** ADD NEW ADMIN SECTION ****************

    $scope.SaveAdmin = function (frmRegister) {

        $scope.mdlPerson.countryId = $scope.mdlCountryState.selectedCountry.Id;
        $scope.mdlPerson.stateId = $scope.mdlCountryState.selectedState.Id;
        $scope.mdlPerson.cityId = $scope.mdlCountryState.selectedCity.id;
        $scope.mdlPerson.countyId = $scope.mdlCountryState.selectedCounty.id;
        $scope.mdlPerson.profileImage = JSON.stringify($scope.mdlPerson.Image);
        $scope.showLoader = true;
        loginService.SaveAdmin($scope.mdlPerson).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {

                   swal("SUCCESS", "Data Saved Successfully.", "success");
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data) {
               $scope.showLoader = false;
               swal("ERROR", MessageService.responseError, "error");
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.AddNewAdmin = function () {
        $scope.newAdmin = true;
        $("#mdlAddNewAdmin").modal('show');
    }

    $scope.CloseAddNewAdminMdl = function () {
        $("#mdlAddNewAdmin").modal('hide');
    }

    $scope.LoadManagers = function () {
        $scope.newAdmin = true;
    }

    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };

    $scope.ValidatePersonInfoZipCode = function (model, ele) {

        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidatePersonInfoZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");
            }
        }
    }

    $scope.ValidatePersonInfoZipCodeUsingAPI = function (ZipCode, ele) {

        var city = '';
        var County = '';
        var Country = '';
        //  $scope.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;
                $.each(address_components, function (index, component) {
                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "").toUpperCase();
                        }
                    });
                });

                //var enteredState = $scope.mdlCountryState.selectedState.Name.toUpperCase();
                // var enteredCounty = $scope.mdlCountryState.selectedCounty.name.replace(" County", "").toUpperCase().trim();
                //var apiState = state.toUpperCase();
                //var apiCounty = County.toUpperCase();

                if ($scope.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.trim()) {
                    if ($scope.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {

                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {

                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    $scope.ValidateCompanyInfoZipCode = function (model, ele) {
        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidateCompanyInfoZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");
            }
        }
    }

    $scope.ValidateCompanyInfoZipCodeUsingAPI = function (ZipCode, ele) {
        var city = '';
        var County = '';
        var Country = '';

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {
                var address_components = response.results[0].address_components;
                $.each(address_components, function (index, component) {

                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_2') {
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "").toUpperCase();
                        }
                    });
                });
                if ($scope.mdlCompanyCountryState.selectedState.Name.toUpperCase().trim() == state.trim()) {
                    if ($scope.mdlCompanyCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {
                        $scope.compZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {
                        //$("#zip_code").focus();                       
                        $scope.compZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.compZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.compZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.compZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }

    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        $scope.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;

        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, countyId, function (data) {
            $scope.City = angular.copy(data);
            var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlPerson.cityId });
            if (city.length > 0) {
                $scope.mdlCountryState.selectedCity = city[0];
                $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
            }
        });
    };

    $scope.clearCompanyddl = function ($event) {
        $event.stopPropagation();
        $scope.mdlCompanyCountryState.selectedCounty = undefined;
        var countyId = 0;
        $scope.GetCityByStateOrCounty($scope.mdlCompanyCountryState.selectedState.Id, countyId, function (data) {
            $scope.CompanyCity = angular.copy(data);
        });
    };

    $scope.fileSelected = function (element) {
        var myFileSelected = element.files[0];
        $scope.CompanyPhoto[0] = myFileSelected;
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('divimage');
            if (output != null)
                output.style.backgroundImage = 'url(' + reader.result + ')';
            $scope.$apply();
        }
        reader.readAsDataURL(event.target.files[0]);
        $scope.$apply();
    };

    $scope.SaveCompanyPhoto = function () {
        var fd = new FormData();
        fd.append("file", $scope.CompanyPhoto[0]);
        fd.append("companyId", $scope.pId);
        fd.append("moduleid", 'MANAGERCOMP');
        fd.append("seqNo", 0);
        fd.append("filename", 'image0');
        fd.append("filedesc", '');
        fd.append("filetags", 'Photo, ' + $scope.pId);
        loginService.saveFiles(fd, '/api/companyphoto', function (status, resp) {
            if (status) {
                swal("SUCCESS", "Photo saved successfully", "success");
            }
            else {
                swal("ERROR", "Unable to save photos.", "error");
            }
        });
    }

    $scope.ZipCodeTextChange = function (zipCode, zipType) {
        if (zipCode != undefined) {
            if (zipCode.length >= 5) {
                if (zipType == 'PersonInfoZip') {
                    $scope.ValidatePersonInfoZipCode(zipCode, zipType)
                }
                else if (zipType == 'CompInfoZip') {
                    $scope.ValidateCompanyInfoZipCode(zipCode, zipType)
                }
            }
        }
    }

    $scope.CheckPassword = function (pwd) {
        // Validate lowercase letters
        $scope.PasswordError = '';
        var lowerCaseLetters = /[a-z]/g;
        if (!pwd.match(lowerCaseLetters)) {
            $scope.PasswordError = $scope.PasswordError + 'One Lower Case';
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (!pwd.match(upperCaseLetters)) {
            if ($scope.PasswordError != '')
                $scope.PasswordError = $scope.PasswordError + ',One Upper Case';
            else
                $scope.PasswordError = $scope.PasswordError + 'One Upper Case';
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (!pwd.match(numbers)) {
            if ($scope.PasswordError != '')
                $scope.PasswordError = $scope.PasswordError + ',One Number';
            else
                $scope.PasswordError = $scope.PasswordError + 'One Number';
        }

        //Special Character
        var SpecialCharacter = /[!@#$%^&*]/g;
        if (!pwd.match(SpecialCharacter)) {
            if ($scope.PasswordError != '')
                $scope.PasswordError = $scope.PasswordError + ',One Special Character'
            else
                $scope.PasswordError = $scope.PasswordError + 'One Special Character';
        }

    }

    $scope.removeProfileImage = function () {
        personService.RemoveFile($rootScope.userData.personId, function (result, data) {
            if (result) {
                $window.location.reload();
            }
            else {
                swal("ERROR", "Unable to save image.", "error");
            }
        });
    }

    $scope.OnLoadSubScription = function () {
        $scope.showLoader = true;
        var customerdata = JSON.parse($window.localStorage.getItem("customer"));
        if (customerdata != null)
        {
            $scope.customer = customerdata;
            $scope.NewSubscription = true;
        }

        if (!angular.isUndefined($routeParams.CustomerId) && !angular.isUndefined($routeParams.PlanCode)) {
            $scope.selectedplan = $routeParams.PlanCode;
            $scope.selectedcustomerId = $routeParams.CustomerId;
        }
        else if (!angular.isUndefined($routeParams.NewSubscription)) {
            $scope.NewSubscription = true;
            $scope.GetZohoProfile();
        }
        loginService.GetSubscriptionPlan().
        success(function (response) {
            if (response.Status == true) {
                $scope.lstplans = response.Data;
                $scope.lstplans_Filtered = angular.copy($scope.lstplans);
                if ($scope.selectedplan != '') {
                    $scope.subscription.plan = $scope.lstplans.filter(function (plan) { return plan.plan_code == $scope.selectedplan })[0];
                }
                if ($scope.NewSubscription != true) {
                    $scope.lstplans_Filtered = angular.copy($scope.lstplans.filter(function (Plan) { return Plan.name != "Trial" }));
                }
            }
            else {
                swal("ERROR", MessageService.responseError, "error");
            }
            $scope.showLoader = false;
        }).error(function (data, status) {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    $scope.SaveSubScripe = function () {
        $scope.subscription.planCode = $scope.subscription.plan.plan_code;
        $scope.subscription.subscription_id = 0;
        $scope.subscription.name = '';
        $scope.subscription.amount = $scope.subscription.plan.recurring_price;
        $scope.subscription.product_id = $scope.subscription.plan.product_id;
        $scope.subscription.CardID = '';
        $scope.subscription.plan = $scope.subscription.plan;
        $scope.subscription.IntervalUnit = $scope.subscription.plan.interval_unit;
        $scope.ClearPaymentModel();
        var dj = $scope.customer;
            if ($scope.customer.length == 0) {
                $scope.GetZohoProfile();
            }
            else {
                $scope.subscription.customerId = $scope.customer.CustomerId;
                $scope.subscription.CurrencyCode = $scope.customer.CurrencyCode;
                $scope.subscription.customer = $scope.customer;
                $scope.Payment.FirstName = $scope.customer.DisplayName;
               // $('#mdl_AddCardDetail').modal("show");
            }
            if ($scope.subscription.plan.name == 'Trial') {
                $scope.SaveSubScriptionservice($scope.subscription);
            }
            else {
                $('#mdl_AddCardDetail').modal("show");
            }
    };

    $scope.SavePayment = function (Payment) {
        $scope.showLoader = true;
        $scope.subscription.name = Payment.FirstName;
        $scope.subscription.CardID = Payment.CVV;

        var namearr = Payment.FirstName.split(' ');

        $scope.subscription.auto_collect = Payment.AutoRenewal;
        $scope.mdlCard.expiry_month = $scope.ExpiryMonth;
        $scope.mdlCard.expiry_year = $scope.ExpiryYear;
        $scope.mdlCard.last4 = Payment.AccountNumber.toString().substr(12, 4);
        $scope.mdlCard.cvv_number = Payment.CVV;
        $scope.mdlCard.card_number = Payment.AccountNumber;
        $scope.mdlCard.first_name = namearr[0];
        $scope.mdlCard.last_name = namearr[1];
        $scope.subscription.mdlCard = $scope.mdlCard;

        $scope.SaveSubScriptionservice_withCard($scope.subscription);

    }
    $scope.SaveSubScriptionservice = function (subscription) {
        $scope.showLoader = true;
        $scope.subscription.auto_collect = false;

        loginService.SaveSubScription($scope.subscription).
           success(function (Response) {
               // $scope.showLoader = false;
               if (Response.Status == true) {
                   $scope.showLoader = false;
                   if ($scope.subscription.plan.name == 'Trial') {
                       if ($rootScope.userData != null) {
                           swal("SUCCESS", "Subscription has been Saved", "success").then((willDelete) => {
                               if (willDelete) {
                                   window.location = '#/home';
                                   window.location.reload();
                               } 
                           });;
                          
                       } else {
                           $scope.showSubscriptionPlan = false;
                           $scope.showMessage = true;
                           $('#subscriptionplan').hide();
                           $('#showmsg').show();
                           swal("SUCCESS", "Person Info has been Saved", "success");
                       }
                   }
                   else {
                       var SubScriptionId = Response.Data;
                       $scope.SaveSubscriptionPayment(SubScriptionId, $scope.customer.CustomerId, $scope.subscription.amount, "creditcard");
                   }
                   $('#mdl_AddCardDetail').modal("hide");
               }
               else {

               }

           }).error(function () {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.SaveSubScriptionservice_withCard = function (subscription) {
        // $scope.subscription.auto_collect = true;

        loginService.SaveSubScription_withCard($scope.subscription).
           success(function (Response) {
               // $scope.showLoader = false;
               if (Response.Status == true) {
                   var SubScriptionId = Response.Data;
                   if (SubScriptionId == null)
                       swal("ERROR", "try again!", "error");
                   else {
                       if ($rootScope.userData != null) {
                           $rootScope.userData.isSubscriptionExpired = false;
                           $window.localStorage.setItem("SubscriptionExpire", JSON.stringify(false));
                           $window.localStorage.setItem("loginUser", JSON.stringify($rootScope.userData));
                           swal("SUCCESS", "Subscription has been Saved", "success").then((willDelete) => {
                               if (willDelete) {
                                   window.location = '#/home';
                                   window.location.reload();
                               }
                           });;
                       }
                       else {
                           //window.location = '#/login';
                           //window.location.reload();
                           $('#subscriptionplan').hide();
                           $('#showmsg').show();
                       }
                   }
                   // $scope.SaveSubscriptionPayment(SubScriptionId, $scope.customer.CustomerId, $scope.subscription.amount, "creditcard");
                   $('#mdl_AddCardDetail').modal("hide");
               }
               else {

               }

           }).error(function () {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.SaveSubscriptionPayment = function (SubScriptionId, CustomerId, Amount, PaymentMode) {
        $scope.showLoader = true;
        loginService.SavePayment(SubScriptionId, CustomerId, Amount, PaymentMode).
         success(function (Response) {
             if (Response.Status == true) {
                 $scope.showLoader = false;
                 if ($rootScope.userData != null) {
                     $rootScope.userData.isSubscriptionExpired = false;
                     $window.localStorage.setItem("SubscriptionExpire", JSON.stringify(false));
                     $window.localStorage.setItem("loginUser", JSON.stringify($rootScope.userData));
                     window.location = '#/home';
                     window.location.reload();
                 }
                 else {
                     window.location = '#/login';
                     window.location.reload();
                 }
                 // swal("Successfully SubScripe", "Success");
             } else {

             }

         }).error(function () {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;
         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    $scope.ClearPaymentModel = function () {
        $scope.Payment = {
            FirstName: '',
            CVV: '',
            ExpireDate: '',
            LastDight: '',
            AccountNumber:''
        }
    }

   
    $scope.GetZohoProfile = function () {
        loginService.GetZohoProfile($rootScope.userData.userName).
        success(function (Response) {
            if (Response.Status == true) {
                $scope.customer = Response.Data;
                $scope.subscription.customerId = $scope.customer.CustomerId;
                $scope.subscription.CurrencyCode = $scope.customer.CurrencyCode;
                $scope.subscription.customer = $scope.customer;
                $scope.Payment.FirstName = $scope.customer.DisplayName;
            } 
        }).error(function () {
            if (status == 401) {
                commonService.SessionOut();
            }
            else {
                swal("ERROR", MessageService.serverGiveError, "error");
            }
            $scope.showLoader = false;
        }).finally(function (data) {
            $scope.showLoader = false;
        });

    }

    $scope.ChangePlan = function (plan, event) {
        $('.selectdiv').removeClass('selectdiv');
        $(event.target).closest('div[name=divplan]').addClass('selectdiv');
        $scope.subscription.plan = plan;
        var target = $(event.target);
    }

    $scope.changeExpireDate = function (ExpireDate) {
        var CurrentMonth = new Date().getMonth() + 1;
        var Currentyear = new Date().getFullYear();

        var year = Currentyear.toString().substring(2, 4);;
        var ExpireYear = ExpireDate.toString().substring(2, 4);
        var ExpireMonth = ExpireDate.toString().substring(0, 2);
        $scope.ExpiryMonth = ExpireMonth;
        $scope.ExpiryYear = "20" + ExpireYear;

        if (parseInt(ExpireYear) > parseInt(year)) {
            if (ExpireMonth > 12) {
                $scope.ExpireDateError = false;
                $scope.Payment.ExpireDate = '';
            }
            else {
                $scope.ExpireDateError = true;
            }
        }
        else if (parseInt(ExpireYear) < parseInt(year) && parseInt(ExpireMonth) > parseInt(CurrentMonth)) {
            $scope.ExpireDateError = false;
            $scope.Payment.ExpireDate = '';
        }
        else {
            if (parseInt(ExpireMonth) > parseInt(CurrentMonth)) {
                if (ExpireMonth > 12) {
                    $scope.ExpireDateError = false;
                    $scope.Payment.ExpireDate = '';
                }
                else {
                    $scope.ExpireDateError = true;
                }
                // $scope.ExpireDateError = true;
            } else {
                if (ExpireMonth > 12) {
                    $scope.ExpireDateError = false;
                    $scope.Payment.ExpireDate = '';
                }
                else {
                    $scope.ExpireDateError = true;
                }
            }
        }
    }
}