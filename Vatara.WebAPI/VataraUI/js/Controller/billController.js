﻿vataraApp.controller('billController', billController);
billController.$inject = ['$scope', 'billService', '$routeParams', 'pagingService', 'onlineStatementService',
    '$location', '$filter', '$rootScope', 'commonService', 'tenantService'];
function billController($scope, billService, $routeParams, pagingService, onlineStatementService, $location, $filter,
    $rootScope, commonService, tenantService) {

    var BC = this;

    BC.pageShowOnOwner_TenantContact = false;
    BC.personTypeForOwner_TenantContact = 0;
    BC.owner_TenantIdForOwner_TenantContact = 0;
    BC.pageShowOnTenantContact = false;
   
    $scope.menuActive = 'Payments';
    $scope.menuUrl = 'payments';

    var currentDates = new Date();
    BC.selectCurrentPeriod = "";
    BC.selectOwner = "";
    BC.lstCurrentPeriod = [];
    BC.filterOwnerList = [];
    BC.selectProperty = "";
    BC.filterPropertyList = [];
    BC.filterPropertyList_dropdown = [];
    BC.startDate = new Date();
    BC.endDate = new Date();

    BC.selectDate = new Date();
    BC.currentDate = new Date();

    BC.PageService = pagingService;
    BC.itemsPerPageArr = BC.PageService.itemsPerPageArr;    

    BC.receivableItemsPerPage = _ItemsPerPage;
    BC.payableItemsPerPage = _ItemsPerPage;
    //BC.mdlReceivableItemCountPerPage = _ItemsPerPage;
    //BC.mdlPayableItemCountPerPage = _ItemsPerPage;

    BC.currentPage_Receiver = 0;
    BC.currentPage_Payable = 0;
    BC.Receiver_OrderBy = "";
    BC.Payable_OrderBy = "";
    $scope.showLoader = false;
    BC.managerid = $rootScope.userData.personId;
    BC.personID = 0;
    BC.isManager = false;
    BC.personTypeId = 0;
    BC.lstOwner = [];
    BC.lstProperty = [];
    BC.lstReceiver = [];
    BC.lstPayable = [];
    BC.lstReceiver_Filter = [];
    BC.lstPayable_Filter = [];
    $scope.billReport = "";
    BC.ownerPOID = 0;  

    BC.SetItemsPerPageForPayable = function (count) {      
        BC.currentPage_Payable = 0;        
    }

    BC.SetItemsPerPageForReceivable = function (count) {       
        BC.currentPage_Receiver = 0;      
    }

    BC.PrintPdf = function (divname, pagesize, headerText) {
        //'Receiver.pdf', 'Receiver'
        BC.HTMLArr = [];

        var gridHtml = $("#" + divname).html();

        BC.HTMLArr.push(gridHtml);

        billService.Printpdf(BC.HTMLArr, pagesize, headerText).
        success(function (data) {

            var pdfFile = new Blob([data], { type: 'application/pdf' });
            //var pdfFileUrl = URL.createObjectURL(pdfFile);
            //window.open(pdfFileUrl);

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(pdfFile, 'Financial.pdf');
            } else {
                var objectUrl = URL.createObjectURL(pdfFile);
                window.open(objectUrl);
            }

        }).error(function (data) {
            $scope.showLoader = false;
            swal("ERROR", "", "error");
        }).finally(function (data) {
            $scope.showLoader = false;
        });
    }

    BC.Filter = function (items, startDate, endDate) {
        var retArray = [];

        if (!startDate && !endDate) {
            return items;
        }
        angular.forEach(items, function (obj) {

            var receivedDate = obj.date;
            if (moment(receivedDate).isAfter(startDate) && moment(receivedDate).isBefore(endDate)) {
                retArray.push(obj);
            }
        });
        return retArray;
    }
    //----------------------------------------------------------------
    BC.ChangeDate = function () {
        BC.endDate = BC.startDate;
    }

    BC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    }

    BC.LoadBill = function () {

        //BC.GetListProperty();
        BC.GetDate();
        BC.lstCurrentPeriod = angular.copy(Dateranges);
        BC.selectCurrentPeriod = BC.lstCurrentPeriod[0];


        if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                var propertyId = 0;
                BC.LoadDataForOwner(propertyId);

            } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                var propertyId = 0;

                var selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
                var currentDate = BC.selectCurrentPeriod.EndDate === undefined ? BC.endDate : BC.selectCurrentPeriod.EndDate;

                BC.LoadDataForTenant(propertyId, selectDate, currentDate);
            }
            else {
                BC.LoadDataForPropertyManager();
            }
        }
        else {           

            BC.LoadDataForOwnerTenant();
        }     
    }

    BC.LoadDataForPropertyManager = function () {
        BC.isManager = true;       
        BC.GetListProperty(function (data) {
            BC.GetListOwner();
        });
    }

    BC.LoadDataForOwnerTenant = function () {
        BC.isManager = false;
        BC.managerid = $rootScope.userData.personId;
        BC.pmid = $rootScope.userData.ManagerDetail.pId;
        BC.personID = $rootScope.userData.personId;
        BC.propertyId = 0;
        BC.personTypeId = $rootScope.userData.personTypeId;
        BC.POID = $rootScope.userData.poid;

        if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {

            BC.LoadDataForOwner(BC.propertyId);
        }
        else {  
            var selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
            var currentDate = BC.selectCurrentPeriod.EndDate === undefined ? BC.endDate : BC.selectCurrentPeriod.EndDate;
            var propertyId = 0;
            if ($rootScope.selectedPropertyId != undefined)
            {
                propertyId = $rootScope.selectedPropertyId;
            }
            BC.LoadDataForTenant(propertyId, selectDate, currentDate);
        }
       // BC.GetBillData(); // ***********   this creates infinite loop check it
    }

    BC.LoadDataForOwner = function (propertyId) {     

        var selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
        var currentDate = BC.selectCurrentPeriod.EndDate===undefined?BC.endDate:BC.selectCurrentPeriod.EndDate;
        if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

            BC.pageShowOnOwner_TenantContact = true;
            BC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.owner;
            BC.owner_TenantIdForOwner_TenantContact = $routeParams.ownerId;
            if ($routeParams.ownerId != undefined && $routeParams.ownerPOID != undefined && $routeParams.ownerIsCompany != undefined) {

                BC.personID = $routeParams.ownerId;
                BC.POID = $routeParams.ownerPOID;
                BC.ownerIsCompany = $routeParams.ownerIsCompany;
                BC.personTypeId = $rootScope.PersonTypeIdEnum.owner;

                if ($routeParams.propertyId != undefined && propertyId == 0) // view owner profile by clicking on owner name in property view
                {
                    propertyId = $routeParams.propertyId;
                }
            }           
            BC.GetBillByPersonAndProperty(selectDate, currentDate, propertyId, BC.POID);
        }
        else {
            BC.managerid = $rootScope.userData.personManageriD;
            BC.POID = $rootScope.userData.poid;
            BC.GetBillForOwnerTenantByPersonIdAndPropertyId(selectDate, currentDate, propertyId, BC.POID);
        }      
    }

    BC.LoadDataForTenant = function (propertyId, selectDate, currentDate) {

        if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {
            BC.pageShowOnOwner_TenantContact = true;
            BC.pageShowOnTenantContact = true;
            BC.personTypeForOwner_TenantContact = $rootScope.PersonTypeIdEnum.tenant;
            BC.owner_TenantIdForOwner_TenantContact = $routeParams.tenantId;

            if ($location.url().match("tenants/")) {              
                propertyId = $rootScope.selectedPropertyId;
                if (propertyId == undefined) {
                    propertyId = 0;
                }
            }
            if ($routeParams.propertyId != undefined && (propertyId == 0 || propertyId == undefined)) {
                propertyId = $routeParams.propertyId;
            }
            BC.personID = $routeParams.tenantId;
            BC.personTypeId = $rootScope.PersonTypeIdEnum.tenant;
            var ownerPOID = 0;
            BC.GetBillByPersonAndProperty(selectDate, currentDate, propertyId, ownerPOID);
        }
        else {
            BC.managerid = $rootScope.userData.personManageriD;
            BC.POID = $rootScope.userData.poid;
            if (propertyId != 0)
            {
                BC.GetBillForOwnerTenantByPersonIdAndPropertyId(selectDate, currentDate, propertyId, BC.POID);
            }            
        }
    }

    $rootScope.$on("FilterBillsOnLeaseChange", function (event, MdlParameters) {

        var selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
        var currentDate = BC.selectCurrentPeriod.EndDate === undefined ? BC.endDate : BC.selectCurrentPeriod.EndDate;
      //  alert('MdlParameters.propertyId ' + MdlParameters.propertyId);
        BC.LoadDataForTenant(MdlParameters.propertyId, selectDate, currentDate);
    });


    BC.GetBillData = function (checkValue) {
     
        BC.ownerPOID = 0;
        BC.personID = 0;
        BC.personTypeId = 0;
        var propertyId = 0;
        if (BC.selectCurrentPeriod != null) {
            if (BC.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {
                BC.lstReceiver_Filter = [];
                BC.lstPayable_Filter = [];
                return;
            }
            var currentDate = new Date();
            var selectDate;
            if (BC.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {
                currentDate = BC.endDate;
                selectDate = BC.startDate;
            }
            else {
                if (BC.selectCurrentPeriod.Period == 'Annual') {
                    selectDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {                   
                    selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
                    currentDate = BC.selectCurrentPeriod.EndDate === undefined ? BC.endDate : BC.selectCurrentPeriod.EndDate;
                }
            }

            BC.selectDate = selectDate;
            BC.currentDate = currentDate;

            $scope.showLoader = true;
            BC.lstReceiver_Filter = [];
            BC.lstPayable_Filter = [];
         
            var personId = $rootScope.userData.personId;
            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {

                if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {

                    var propertyId = 0;
                    BC.LoadDataForOwner(propertyId);

                } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {

                    var propertyId = 0;
                    BC.LoadDataForTenant(propertyId, selectDate, currentDate);
                }
                else { // PM is filtering bill by date
                    BC.personTypeId = $rootScope.userData.personTypeId;
                    BC.GetBillDateWise(BC.managerid, BC.personID, selectDate, currentDate, BC.isManager, propertyId, BC.personTypeId, BC.ownerPOID, function (data) {

                        BC.filterPropertyList = [];
                        var data = $filter('groupBy')(BC.lstProperty, 'propertyId');
                        angular.forEach(data, function (value, key) {
                            BC.filterPropertyList.push(value[0])
                        });
                        BC.selectProperty = BC.filterPropertyList[0];
                    }); 
                }
            }
            else {
                BC.LoadDataForOwnerTenant();
            }
        }
        else {
            BC.selectOwner = undefined;
            BC.selectProperty = undefined;
            BC.lstReceiver_Filter = [];
            BC.lstPayable_Filter = [];
        }
    }

    BC.GetBillDateWise = function (managerid, personID, selectDate, currentDate, isManager, propertyId, personTypeId, ownerPOID, callback) {
        billService.getBill_DateWise(managerid, personID, selectDate, currentDate, isManager, propertyId, personTypeId, ownerPOID).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    BC.lstReceiver = response.Data.lstReceiver;
                    BC.lstPayable = response.Data.lstPayable;
                    BC.lstReceiver_Filter = angular.copy(BC.lstReceiver);
                    BC.lstPayable_Filter = angular.copy(BC.lstPayable);

                    //if (BC.pageShowOnOwner_TenantContact == false) {
                    //    BC.selectOwner = BC.lstOwner[0];
                    //}
                    //else {
                    //    BC.selectOwner = BC.lstOwner.filter(t => t.ownerId == BC.owner_TenantIdForOwner_TenantContact && t.personTypeId == BC.personTypeForOwner_TenantContact)[0];
                    //    BC.FillProperty_dropdown(BC.selectOwner)
                    //}
                   // BC.SelectAll();

                    callback(response.Data);
                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
                $scope.showLoader = false;
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    BC.GetBillByPersonAndProperty = function (selectDate, currentDate, propertyId, ownerPOID) {
     
        $scope.showLoader = true;       
        billService.getBillByPersonAndProperty(BC.managerid, selectDate, currentDate, ownerPOID, BC.personID, propertyId, BC.personTypeId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   BC.filterPropertyList = response.Data.lstMdlPropertOwnersWithManager;
                   BC.lstReceiver = response.Data.lstReceiver;
                   BC.lstPayable = response.Data.lstPayable;
                   BC.lstReceiver_Filter = angular.copy(BC.lstReceiver);
                   BC.lstPayable_Filter = angular.copy(BC.lstPayable);

                   var addNewOption_All = angular.copy(BC.filterPropertyList[0]);
                   if (addNewOption_All != undefined) {
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       BC.filterPropertyList.unshift(addNewOption_All);

                       if (propertyId == 0) {
                           BC.selectProperty = addNewOption_All;
                       }
                       else { // when we open owner profile, in bill sub tab if we search bill against any property then make that property selected
                           BC.selectProperty = BC.filterPropertyList.filter(function (p) { return p.propertyId == propertyId })[0];
                       }
                   }

                   //if (BC.pageShowOnOwner_TenantContact == false) {
                   //    BC.selectOwner = BC.lstOwner[BC.lstOwner.length - 1];
                   //}
                   //else {
                   //    BC.selectOwner = BC.lstOwner.filter(t => t.ownerId == BC.owner_TenantIdForOwner_TenantContact && t.personTypeId == BC.personTypeForOwner_TenantContact)[0];
                   //    BC.FillProperty_dropdown(BC.selectOwner)
                   //}
                   //BC.SelectAll();
               }
               else {
                   swal("ERROR", MessageService.responseError, "error");
               }
               $scope.showLoader = false;
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    BC.GetBillForOwnerTenantByPersonIdAndPropertyId = function (selectDate, currentDate, propertyId, ownerPOID) {
        $scope.showLoader = true;
        BC.personID = BC.personID == 0 ? $rootScope.userData.personId : BC.personID;
        BC.personTypeId = BC.personTypeId == 0 ? $rootScope.userData.personTypeId : BC.personTypeId;
        billService.getBillForOwnerTenantByPersonIdAndPropertyId(BC.managerid, selectDate, currentDate, ownerPOID, BC.personID, propertyId, BC.personTypeId).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   BC.filterPropertyList = response.Data.lstMdlPropertOwnersWithManager;
                   BC.lstReceiver = response.Data.lstReceiver;
                   BC.lstPayable = response.Data.lstPayable;
                   BC.lstReceiver_Filter = angular.copy(BC.lstReceiver);
                   BC.lstPayable_Filter = angular.copy(BC.lstPayable);

                   var addNewOption_All = angular.copy(BC.filterPropertyList[0]);
                   if (addNewOption_All != undefined) {
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       BC.filterPropertyList.unshift(addNewOption_All);

                       if (propertyId == 0 || propertyId == undefined) {                          
                           BC.selectProperty = addNewOption_All;
                       }
                       else { // when we open owner profile, in bill sub tab if we search bill against any property then make that property selected
                           BC.selectProperty = BC.filterPropertyList.filter(function (p) { return p.propertyId == propertyId })[0];
                       }
                   }
               }
               else {
                   swal("ERROR", MessageService.responseError, "error");
               }
               $scope.showLoader = false;
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    BC.FillProperty_dropdown = function (model) {

        BC.currentPage_Receiver = 0;
        BC.currentPage_Payable = 0;
        BC.selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
        BC.currentDate = BC.selectCurrentPeriod.EndDate === undefined ? BC.endDate : BC.selectCurrentPeriod.EndDate;

        if (model.ownerId == 0) {
            BC.GetBillData();
        }
        else {
            BC.selectProperty = "";

            BC.personID = model.personId;
            BC.isManager = true;
            var propertyId = 0;
            BC.ownerPOID = 0;
            BC.personTypeId = model.personTypeId;
            if (model.personTypeId == $rootScope.PersonTypeIdEnum.owner) {
                BC.ownerPOID = model.ownerId;
            }
            BC.GetBillByPersonAndProperty(BC.selectDate, BC.currentDate, propertyId, BC.ownerPOID);
        }
    }

    BC.FilterByProperty = function (model) {
        

        var propertyId = 0;
        if (model.propertyId != undefined) {
            propertyId = model.propertyId;
        }

        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            if ($location.url().match("owners/") || $location.url().match("propertyOwners/")) {              
                BC.LoadDataForOwner(propertyId);

            } else if ($location.url().match("tenants/") || $location.url().match("leaseTenant/")) {                
                BC.LoadDataForTenant(propertyId, BC.selectDate, BC.currentDate);
            }
            else {  // for PM

                if (BC.selectOwner.ownerId == 0) {
                    BC.GetBillDateWise(BC.managerid, BC.personID, BC.selectDate, BC.currentDate, BC.isManager, propertyId, BC.personTypeId, BC.ownerPOID, function (data) {
                    });
                }
                else {

                    if (BC.selectOwner.personTypeId == $rootScope.PersonTypeIdEnum.owner) {
                        BC.ownerPOID = BC.selectOwner.ownerId;
                    }
                    else {
                        BC.ownerPOID = 0;
                    }                    
                    BC.personID = BC.selectOwner.personId;
                    BC.GetBillByPersonAndProperty(BC.selectDate, BC.currentDate, propertyId, BC.ownerPOID);
                }              

                //if (model.propertyId == 0) {
                //    if (BC.selectOwner.ownerId == 0) {
                //        BC.lstPayable_Filter = angular.copy(BC.lstPayable);
                //        BC.lstReceiver_Filter = angular.copy(BC.lstReceiver);
                //    }
                //    BC.SelectAll();
                //}
                //else {
                //    if (BC.selectOwner == undefined || BC.selectOwner.ownerId == 0) {
                //        BC.lstPayable_Filter = BC.lstPayable.filter(Property =>  Property.propertyId === model.propertyId);
                //        BC.lstReceiver_Filter = BC.lstReceiver.filter(Property => Property.propertyId === model.propertyId);
                //    }
                //    else {
                //        BC.lstPayable_Filter = BC.lstPayable.filter(Property => Property.from === BC.selectOwner.ownerId && Property.propertyId === model.propertyId);
                //        BC.lstReceiver_Filter = BC.lstReceiver.filter(Property => Property.to === BC.selectOwner.ownerId && Property.propertyId === model.propertyId);
                //    }
                //}
            }
        }
        else {
            BC.managerid = $rootScope.userData.personManageriD;
            BC.POID = $rootScope.userData.poid;

            var selectDate = BC.selectCurrentPeriod.Startdate === undefined ? BC.startDate : BC.selectCurrentPeriod.Startdate;
            var currentDate = BC.selectCurrentPeriod.EndDate === undefined ? BC.endDate : BC.selectCurrentPeriod.EndDate;
            BC.GetBillForOwnerTenantByPersonIdAndPropertyId(selectDate, currentDate, propertyId, BC.POID);
        }

        //if (model.propertyId == 0)
        //{                     
        //    if (BC.selectOwner.ownerId == 0)
        //    {
        //        BC.lstPayable_Filter = angular.copy(BC.lstPayable);
        //        BC.lstReceiver_Filter = angular.copy(BC.lstReceiver);
        //    }
        //    BC.SelectAll();
        //}
        //else
        //{            
        //    if (BC.selectOwner == undefined || BC.selectOwner.ownerId == 0)
        //    {
        //        BC.lstPayable_Filter = BC.lstPayable.filter(Property =>  Property.propertyId === model.propertyId);
        //        BC.lstReceiver_Filter = BC.lstReceiver.filter(Property => Property.propertyId === model.propertyId);
        //    }
        //    else
        //    {
        //        BC.lstPayable_Filter = BC.lstPayable.filter(Property => Property.from === BC.selectOwner.ownerId && Property.propertyId === model.propertyId);
        //        BC.lstReceiver_Filter = BC.lstReceiver.filter(Property => Property.to === BC.selectOwner.ownerId && Property.propertyId === model.propertyId);
        //    }          
        //}        
    }

    BC.GetListOwner = function () {
        $scope.showLoader = true;

        billService.getPropertOwnersByManager(BC.managerid).
          success(function (response) {
              if (response.Status == true) {
                  $scope.showLoader = false;
                  if (response.Data.length > 0) {

                      //var data = $filter('groupBy')(response.Data, 'ownerId');
                      //console.log('response.Data', response.Data);
                      //var data = $filter('groupBy')(response.Data, 'personId');
                      //angular.forEach(data, function (value, key) {
                      //    BC.lstOwner.push(value[0])
                      //});                    
                      //var addAll = angular.copy(BC.lstOwner[0]);
                      //addAll.ownerFullName = "All";
                      //addAll.ownerId = 0;
                      //BC.lstOwner.push(addAll);
                      BC.CreateDataForPersonDropdown(response.Data, function (data) { });
                      BC.GetBillData();
                  }
              }
              else {
                  swal("ERROR", response.Message, "error");
              }
          }).error(function (data, status) {
              if (status == 401) {
                  commonService.SessionOut();
              }
              else {
                  swal("ERROR", MessageService.serverGiveError, "error");
              }
              $scope.showLoader = false;
          }).finally(function (data) {
              $scope.showLoader = false;
          });
    }

    BC.CreateDataForPersonDropdown = function (model, callback) {

        var companyTypeOwner = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 1 });
        var individualOwner = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.owner && o.ownerIsCompany == 0});
        var Tenants = model.filter(function (o) { return o.personTypeId == $rootScope.PersonTypeIdEnum.tenant });

        //console.log('companyTypeOwner', companyTypeOwner);
        //console.log('individualOwner', individualOwner);
        //console.log('Tenants', Tenants);

        // var list = [];

        var companyTypeOwnerData = $filter('groupBy')(companyTypeOwner, 'ownerId');
        angular.forEach(companyTypeOwnerData, function (value, key) {
            BC.lstOwner.push(value[0])
        });

        var TenantsData = $filter('groupBy')(Tenants, 'personId');
        angular.forEach(TenantsData, function (value, key) {
            BC.lstOwner.push(value[0])
        });

        var individualOwnerData = $filter('groupBy')(individualOwner, 'personId');

        angular.forEach(individualOwnerData, function (value, key) {
            BC.lstOwner.push(value[0]);
        });
        var addAll = angular.copy(BC.lstOwner[0]);
        addAll.ownerFullName = "All";
        addAll.ownerId = 0;
        addAll.personId = 0;
        addAll.personTypeId = 0;
        addAll.propertyId = 0;
        addAll.propertyName = '';
        //BC.lstOwner.splice(0, 0, addAll);        
        BC.lstOwner.unshift(addAll);
        BC.selectOwner = addAll;

    }

    BC.GetListProperty = function (callback) {
        $scope.showLoader = true;
        var managerId = $rootScope.userData.personId;
        var personId = $rootScope.userData.personId;

        //if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
        //    managerId = $rootScope.userData.personManageriD;
        //    personId = $rootScope.userData.poid;
        //}
        //if ($rootScope.PersonTyp.tenant == $rootScope.userData.personType) {
        //    managerId = $rootScope.userData.personManageriD;
        //    personId = $rootScope.userData.personId;
        //}

        billService.getPropertyListByManager_Bill(managerId, BC.isManager, personId).
         success(function (response) {
             if (response.Status == true) {
                 BC.lstProperty = response.Data;
                 var addAll = angular.copy(BC.lstProperty[0]);
                 addAll.propertyName = "All";
                 addAll.ownerId = 0;
                 addAll.propertyId = 0;
                 BC.lstProperty.push(addAll);

                 callback(response.Data);

                 //if ($rootScope.PersonTyp.owner == $rootScope.userData.personType) {
                 //    //if logged-in person is owner than get the owner properties;                    
                 //    BC.filterPropertyList = BC.lstProperty.filter(Property => Property.ownerId == personId);
                 //    BC.filterPropertyList.push(addAll);
                 //}
             }
             else {
                 swal("ERROR", MessageService.responseError, "error");
             }
             $scope.showLoader = false;
         }).error(function (data, status) {
             if (status == 401) {
                 commonService.SessionOut();
             }
             else {
                 swal("ERROR", MessageService.serverGiveError, "error");
             }
             $scope.showLoader = false;

         }).finally(function (data) {
             $scope.showLoader = false;
         });
    }

    BC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    BC.CheckDropdown = function (value) {
        BC.lstReceiver_Filter = angular.copy(BC.lstReceiver);
        BC.lstPayable_Filter = angular.copy(BC.lstPayable);
    }

    BC.ButtonClick_Excel = function (id, excelName) {
        $(id).table2excel({
            filename: excelName
        });
    }

    BC.ButtonClick_Pdf = function (id, pdfName, headerName) {
        HtmlTableToPdf(id, pdfName, headerName, '', '');
    }

    BC.ShowBill = function (bill, billType) {
        $scope.billReport = bill;
        $scope.billReport.billType = billType;
        $scope.billReport.companylogo = DefaultLogo;
        $('#generateBill').modal('show');
    }

}