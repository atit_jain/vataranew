﻿
vataraApp.controller('chatController', chatController);
chatController.$inject = ['$scope', '$routeParams', 'pagingService', 'chatService', '$filter',
    '$rootScope', 'commonService', '$timeout', '$location', '$window'];
function chatController($scope, $routeParams, pagingService, chatService, $filter, $rootScope, commonService, $timeout, $location, $window) {


    $scope.PMID = $rootScope.userData.ManagerDetail.pId;
    $scope.UserData = [];
    $scope.UserUnreadMessages = [];
    $scope.Currentuser = $rootScope.userData.userName;
    $scope.ChatData = [];
    $scope.selectedEmail = null;
    $scope.mdlChatMessage = '';
    $scope.selectedUserForChat = {};
    $scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.FilterUsers = '';
    $scope.RowIndex = 0;
    //angular.element(document).ready(function () {
    //    $scope.SetScrollPosition();
    //});

    $scope.LoadChatData = function ()
    {
        ChatPageLoad();
        EmojiInit();
        document.getElementById("loggedInUser").value = $scope.Currentuser;
        document.getElementById("loggedInUserEmail").value = $scope.Currentuser;
        document.getElementById("loggedInUserName").value = $rootScope.userData.Name;

        $scope.GetAllUserListForChat(function (data) {

            if (!angular.isUndefined($routeParams.user)) {
                $scope.selectedUserForChat = {};
                var selectedUser = data.filter(function (u) { return u.EmailId == $routeParams.user })[0];
                $scope.selectedUserForChat = angular.copy(selectedUser);
                if (selectedUser != null && selectedUser != undefined)
                {
                    document.getElementById("selectedUser").value = $routeParams.user;
                    $scope.GetSelectedUserChat($scope.Currentuser, $routeParams.user, $scope.selectedUserForChat.PersonTypeId);

                    var delayInMilliseconds = 3000; //3 second        
                    setTimeout(function () {
                        $scope.CheckStatus();
                        var divId = $routeParams.user.split('@')[0];
                        var liWithIdAsEmail = $('li[id*="' + divId + '"]');
                        if (liWithIdAsEmail.length > 0) {
                            $(liWithIdAsEmail).find(".about span#UnreadMessage").text('');
                        }
                    }, delayInMilliseconds);
                   // $scope.SelectUser($scope.Currentuser, $routeParams.user, selectedUser.FirstName, selectedUser.LastName, selectedUser.PersonTypeId, selectedUser.PMID)
                }           
            }
        });        
    }

    $scope.CheckStatus = function () {       
        GetActiveUserList();
            //var toMsg = $routeParams.user;
            //var divId = toMsg.split('@')[0];
            //var liWithIdAsEmail = $('li[id*="' + divId + '"]');
            //var onlineStatusSpan = $(liWithIdAsEmail).find("div.about div.status i");
            //if (chatService.isUserOnline) {
            //    console.log('liWithIdAsEmail', liWithIdAsEmail);
            //    console.log('onlineStatusSpan', onlineStatusSpan);
            //    $(onlineStatusSpan).removeClass("offline");
            //    $(onlineStatusSpan).addClass("online");
            //}       
    }

    $scope.sendBtnClick = function () {
        document.getElementById("loggedInUser").value = $scope.Currentuser;        
        document.getElementById("sendmessage").click();
    }

    $scope.GetAllUserListForChat = function (callback) {

        $scope.showLoader = true;
        chatService.getAllUserListForChat($scope.PMID, $rootScope.userData.personTypeId, $rootScope.userData.userName).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       $scope.UserData = angular.copy(response.Data);
                       callback(response.Data);
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;                
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.GetChatUnreadNotificationMessage = function () {

        var toMsg = $rootScope.userData.userName;
        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            toMsg = $rootScope.userData.ManagerDetail.pEmail;
        }
        $scope.showLoader = true;
        chatService.getChatUnreadNotificationMessage(toMsg).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    $scope.SelectUser = function (fromMsg, toMsg, firstName, lastName, PersonTypeId, PMID) {
       
        $window.localStorage.setItem("requestedChatPage", JSON.stringify("/chat/" + toMsg));
        $location.path("/chat/" + toMsg);           
    }

    $scope.GetSelectedUserChat = function (LoggedInUser, SelectedUser, SelectedUserPersonTypeId) {  //($scope.Currentuser, $routeParams.user)

        $scope.selectedEmail = SelectedUser;
        $scope.showLoader = true;
        chatService.getChatInfo(LoggedInUser, $rootScope.userData.personTypeId, SelectedUser, SelectedUserPersonTypeId).
           success(function (response) {
               $scope.showLoader = false;
               if (response.Status == true) {
                   if (response.Data != null) {
                       $scope.ChatData = angular.copy(response.Data);

                       for (var i = 0; i < $scope.ChatData.length; i++) {
                           $scope.ChatData[i]['creationTime'] = new Date($scope.ChatData[i]['creationTime']);
                           if ($scope.ChatData[i]['isFile'] == true) {
                               $scope.ChatData[i]['messageLink'] = $scope.ChatData[i]['message'];
                               $scope.ChatData[i]['message'] = $scope.ChatData[i]['message'].substring($scope.ChatData[i]['message'].indexOf("-") + 1, $scope.ChatData[i]['message'].length);
                           }

                           if (i == $scope.ChatData.length - 1)
                           {
                               $scope.UpdateUnreadMessageCount(SelectedUser);
                           }
                       }
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
                
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }


    $scope.UpdateUnreadMessageCount = function (SelectedUserEmail) {

        var intUnreadMessageCount = 0;
        var date = moment().format("MMM D, YYYY h:mm a");
        var divId = SelectedUserEmail.split('@')[0];

        var DivWithIdAsEmail = $('div[id*="' + divId + '"]');
        var unreadMessage1 = $(DivWithIdAsEmail).find("div.cursorpointer span#unreadMessageCount"); // right side user message count 

        //var unreadMessage1 = $("#" + divId + " span#unreadMessageCount");          

        $(DivWithIdAsEmail).find(".about span#UnreadMessage").text('');

        var liWithIdAsEmail = $('li[id*="' + divId + '"]');
        if (liWithIdAsEmail.length > 0) {

            $(liWithIdAsEmail).find(".about span#UnreadMessage").text('');
        }

        //****** Set the unread message count for chat page user list***********                
        //var unreadMessage1 = $(divAbout).find("span#UnreadMessage");

        var unreadMessage = $(unreadMessage1)[0];
        var unreadMessageCount = $(unreadMessage).text();

        if (unreadMessageCount != '' && unreadMessageCount != undefined && unreadMessageCount != null) {
            intUnreadMessageCount = parseInt(unreadMessageCount);
            $(unreadMessage).text(null);
        }

        //******End Set the unread message count  for chat page user list ********
        //***********************************************

        //***** Update the total unread message count on top nav bar*******
        //*****************************************************************
        var span_Text = document.getElementById("messageCount").innerText;
        if (span_Text != '' && span_Text != undefined && span_Text != null) {
            var unreadMessageCount1 = parseInt(span_Text);
            if (unreadMessageCount1 > 0) {
                unreadMessageCount1 = unreadMessageCount1 - intUnreadMessageCount;
                if (unreadMessageCount1 <= 0) {
                    unreadMessageCount1 = null;
                }
                document.getElementById('messageCount').innerHTML = unreadMessageCount1;
            }
        }
        var delayInMilliseconds = 2000; //2 second        
        setTimeout(function () {           
            $scope.SetScrollPosition();
        }, delayInMilliseconds);
    }

    $scope.SetScrollPosition = function () {      
        var scrollHeight1 = $('div#msg_history')[0].scrollHeight;
        $("div#msg_history").scrollTop(scrollHeight1);
    }
    
    $scope.SendImage = function () {

        if (!angular.isUndefined($routeParams.user)) {
            document.getElementById('FileUpload').click();
        }
        else {
            swal("ERROR", "Please select a user", "error");
        }       
    };

    $scope.$watch("RowIndex", function (newVal, oldVal) {

        if (newVal == $scope.ChatData.length - 1) {           
            $scope.SetScrollPosition();
        }
    });

    $scope.OpenImage = function (path) {
        window.open(path, '_blank', 'height=800,width=600');
    }

    $scope.MinimiseChat = function () {
        chatService.Isminimise = true;
        chatService.MinimiseUser = angular.copy($scope.selectedUserForChat);
        $scope.ClearSelectedUser();
        $location.path("/home");
    }

    $scope.ClearSelectedUser = function () {
        $("#selectedUser").val('');
    }

}