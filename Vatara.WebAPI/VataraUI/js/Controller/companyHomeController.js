﻿vataraApp.controller('companyHomeController', companyHomeController);
companyHomeController.$inject = ['$scope', 'companyHomeService', 'loginService', '$window', '$location', '$rootScope', 'commonService', 'propayRegService'];
function companyHomeController($scope,companyHomeService, loginService, $window, $location, $rootScope, commonService, propayRegService) {
    var CHC = this;
    $scope.showLoader = false;
    CHC.CompanyDomain = $rootScope.Domain;
    CHC.CompanyData = {};


    CHC.LoadFunction = function ()
    {
        CHC.CompanyDomain = $rootScope.CompanyDomain;
        CHC.GetCompanyProfile();
        
    }
   
    CHC.GetCompanyProfile = function () {

    
        $scope.showLoader = true;
        companyHomeService.getCompanyProfile(CHC.CompanyDomain).
           success(function (response) {               
               $scope.showLoader = false;
               if (response.Status == true) {
                  
                   CHC.CompanyData = angular.copy(response.Data)

                  // CHC.CompanyData.logo = JSON.parse(CHC.CompanyData.logo);

                   var userData = {};

                   if ($window.localStorage.getItem("loginUser") != null) {
                      userData = angular.copy(JSON.parse($window.localStorage.getItem("loginUser")));
                   }
                   $window.localStorage.removeItem("loginUser");
                   userData.CompanyData = CHC.CompanyData;
                   $rootScope.userData = userData;                
                   $window.localStorage.setItem("loginUser", JSON.stringify(userData));
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    }

    //CHC.ShowLoginModal = function () {

      
    //    $("#mdllogin").modal('show');

    //}

}

