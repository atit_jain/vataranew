﻿
vataraApp.controller('ownerContactController', ownerContactController);
ownerContactController.$inject = ['$scope', '$routeParams', '$location', 'pagingService', 'ownerService', '$filter', '$rootScope', 'commonService', '$timeout', 'propayRegService', 'propertyService'];
function ownerContactController($scope, $routeParams, $location, pagingService, ownerService, $filter, $rootScope, commonService, $timeout, propayRegService, propertyService) {

    $scope.pTypeName = 'Owner';
    $scope.pTypeId = $rootScope.PersonTypeIdEnum.owner;
    $scope.personlist = [];
    $scope.ownerData = [];
    $scope.propertyId = 0;
    $scope.leaseId = 0;
    $scope.ownerPersonId = 0;
    $scope.ownerPOID = 0;
    $scope.ownerIsCompany = 0;
    $rootScope.ActiveTabName = 'Invoices';

    //$scope.ownerMasterData = {
    //    POID: 0,
    //    CompanyName: '',
    //    Email: '',
    //    ContactPerson: '',
    //    ContactNum: '',
    //    AddressId: 0,
    //    AdrsLine1: '',
    //    AdrsLine2: '',
    //    CountryId: 0,
    //    StateId: 0,
    //    CountyId: 0,
    //    CityId: 0,
    //    zipCode: '',
    //    CreatorUserId: $rootScope.userData.personId
    //};

    $scope.ownerData = {
        Id:0,
        POID: 0,
        CompanyName: '',
        OwnerType:'',
        FirstName: '',
        LastName: '',
        Email: '',
        ContactPerson: '',
        ContactNum: '',
        AddressId: 0,
        AdrsLine1: '',
        AdrsLine2: '',
        CountryId: 0,
        StateId: 0,
        CountyId: 0,
        CityId: 0,
        zipCode: '',
        CreatorUserId: $rootScope.userData.personId
    };

    $scope.mdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    $scope.CopyMdlCountryState = {
        selectedCountry: undefined,
        selectedState: undefined,
        selectedCounty: undefined,
        selectedCity: undefined
    };

    $scope.Country = [];
    $scope.State = [];
    $scope.County = [];
    $scope.City = [];
    $scope.ZipPattern = /^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] *\d[A-Z]\d)$/;
    $scope.pZipValid = true;
    $scope.IsFromPropertyProfile = false;

    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};
    $scope.selectedProperty = {};
    $scope.lstOwnersProperty = [];

    $scope.LoadAllOwners = function () {

        $scope.GetOwnersByPMID();
    };

    $scope.GetOwnersByPMID = function () {
        $scope.showLoader = true;
        ownerService.getOwnersByPMID($rootScope.userData.ManagerDetail.pId).
            success(function (response) {
                if (response.Status == true) {
                    var OwnerData = response.Data;
                    
                    var companyOwner = OwnerData.filter(function(o){return o.ownerIsCompany == 1});
                    var individualOwner = OwnerData.filter(function (o) { return o.ownerIsCompany == 0 });
                    var individualOwnerGroup = $filter('groupBy')(individualOwner, 'pId');
                    $scope.personlist = angular.copy(companyOwner);
                    angular.forEach(individualOwnerGroup, function (value, key) {
                        $scope.personlist.push(value[0])
                    });

                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
                $scope.showLoader = false;
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    $scope.LoadViewOwner = function () {

        $scope.propertyId = 0;
        $scope.leaseId = 0;
        if ($location.url().match("propertyOwners/") && !angular.isUndefined($routeParams.leaseId)) {
            $scope.leaseId = $routeParams.leaseId;
            $scope.propertyId = $routeParams.propertyId;

        }
        else if ($location.path() == "/owners" && !angular.isUndefined($routeParams.propertyId)) {
            $scope.propertyId = $routeParams.propertyId;

        }
        else if ($location.url().match("owners/") && !angular.isUndefined($routeParams.propertyId)) {
            $scope.propertyId = $routeParams.propertyId;
            $scope.IsFromPropertyProfile = true;

        }
        if (!angular.isUndefined($routeParams.ownerId)) {

            $scope.ownerPersonId = $routeParams.ownerId;
            $scope.ownerPOID = $routeParams.ownerPOID;
            $scope.ownerIsCompany = $routeParams.ownerIsCompany;
            $scope.OwnerDetails($scope.ownerPOID, $scope.ownerPersonId, function (data) { });
        }
    };

    $scope.FillOwnerModelForEdit = function (ownerData) {
        $scope.ownerData.POID = ownerData.PersonInfo.pId;
        $scope.ownerData.CompanyName = ownerData.PersonInfo.pFirstName;
        $scope.ownerData.Email = ownerData.PersonInfo.pEmail;
        $scope.ownerData.ContactNum = ownerData.PersonInfo.pPhoneNo;
        $scope.ownerData.AddressId = ownerData.PersonInfo.pAddressId;
        $scope.ownerData.AdrsLine1 = ownerData.PersonInfo.pAdrsLine1;
        $scope.ownerData.AdrsLine2 = ownerData.PersonInfo.pAdrsLine2;
        $scope.ownerData.CountryId = ownerData.PersonInfo.pCountryId;
        $scope.ownerData.StateId = ownerData.PersonInfo.pStateId;
        $scope.ownerData.CountyId = ownerData.PersonInfo.pCountyId;
        $scope.ownerData.CityId = ownerData.PersonInfo.pCityId;
        $scope.ownerData.zipCode = ownerData.PersonInfo.pZipcode;
    }

    $scope.UpdateOwnerDetail = function (frmEditOwner) {
        $scope.mdlAddress = {
            line1: $scope.ownerData.AdrsLine1,
            line2: $scope.ownerData.AdrsLine2,
            cityId: $scope.mdlCountryState.selectedCity.Id,
            countyId: 0,
            stateId: $scope.mdlCountryState.selectedState.Id,
            countryId: $scope.mdlCountryState.selectedCountry.Id,
            zipcode: $scope.ownerData.zipCode,
        }

        if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
            $scope.mdlAddress.countyId = $scope.mdlCountryState.selectedCounty.id;
        }
        MdlOwnerData = {
            ownerData: $scope.ownerData,
            mdlAddress: $scope.mdlAddress
        }
        $scope.showLoader = true;
        ownerService.updateOwnerDetail(MdlOwnerData).
            success(function (response) {
                if (response != undefined) {
                    $scope.showLoader = false;

                    $scope.OwnerDetails($scope.ownerData.POID, $scope.ownerData.Id, function (data) { });
                    frmEditOwner.$setPristine();
                    $('#mdlOwnerCompProfile').modal('hide');
                    swal("SUCCESS", "Data updated successfully", "success");
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    $scope.OwnerDetails = function (poid, PersonId, callback) {
        $scope.showLoader = true;
        ownerService.getOwnerDetails(poid, PersonId).
            success(function (response) {
                if (response.Status == true) {

                    $scope.ownerData = response.Data;
                    // $scope.ownerData.PersonInfo.pImage = defaultProfileImage;
                    $scope.ownerData.OwnerType = $scope.ownerData.PersonInfo.pOwnerType;
                    if ($scope.ownerData.OwnerType == 'INDIVIDUAL') {
                        $scope.ownerData.FirstName = $scope.ownerData.PersonInfo.pFirstName;
                        $scope.ownerData.LastName = $scope.ownerData.PersonInfo.pMiddleName;
                        $scope.ownerData.Id = $scope.ownerData.PersonInfo.pId;
                    }

                    $scope.FillOwnerModelForEdit($scope.ownerData);
                    $scope.lstOwnersProperty = angular.copy(response.Data.lstMdlPropertOwnersWithManager);
                    $scope.selectedProperty = angular.copy(response.Data.lstMdlPropertOwnersWithManager[0]);

                    output = document.getElementById('divimage2');
                    if (output != null) {
                        output.style.backgroundImage = 'url(../Uploads/Person/Photos/' + $scope.ownerData.PersonInfo.pImage + ')';
                    }
                    $scope.LoadPropertyFinancialReport($scope.selectedProperty.propertyId);
                    callback(response.Data);
                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
                $scope.showLoader = false;
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }



    $scope.LoadPropertyFinancialReport = function (PropoertyId) {
        $scope.showLoader = true;
        $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = {};
        var LeaseId = 0;            // LeaseId will always be 0 for property finance
        propertyService.getOwnerIncomeExpenseByPropertyIdLeaseId(PropoertyId, LeaseId).
            success(function (response) {
                if (response.Status == true) {
                    $scope.showLoader = false;
                    $scope.mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = angular.copy(response.Data);
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $scope.showLoader = false;
                    commonService.SessionOut();
                }
                else {
                    $scope.showLoader = false;
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
            }).finally(function (data) {
                $scope.showLoader = false;
                $scope.personId = $rootScope.userData.personId;
            });

    }


    $scope.ViewOwnerProfile = function () {

        $scope.FillOwnerModelForEdit($scope.ownerData);
        $scope.GetCountry(function (CountryData) {

            var country = CountryData.filter(function (country) { return country.Id == $scope.ownerData.CountryId });
            if (country.length > 0) {
                $scope.mdlCountryState.selectedCountry = country[0];
            }
            $scope.GetStates(function (StatesData) {
                var state = StatesData.filter(function (state) { return state.Id == $scope.ownerData.StateId });
                if (state.length > 0) {
                    $scope.mdlCountryState.selectedState = state[0];
                }
                $scope.GetCountyByState($scope.ownerData.StateId, function (data) {
                    $scope.County = angular.copy(data);
                    var county = $scope.County.filter(function (county) { return county.id == $scope.ownerData.CountyId });
                    if (county.length > 0) {
                        $scope.mdlCountryState.selectedCounty = county[0];
                    }

                    $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, $scope.ownerData.CountyId, function (data) {

                        $scope.City = angular.copy(data);
                        var city = $scope.City.filter(function (city) { return city.Id == $scope.ownerData.CityId });
                        if (city.length > 0) {
                            $scope.mdlCountryState.selectedCity = city[0];
                        }
                    });
                });


            });
        });
        $('#mdlOwnerCompProfile').modal('show');
    }

    $scope.GetCountry = function (callback) {

        $scope.showLoader = true;
        propayRegService.getCountries().
            success(function (response) {
                $scope.showLoader = false;
                if (response != undefined) {
                    $scope.showLoader = false;
                    $scope.Country = angular.copy(response);
                    callback(response);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    $scope.GetStates = function (callback) {
        //console.log('State called');
        $scope.showLoader = true;
        propayRegService.getStates().
            success(function (response) {
                if (response != undefined) {
                    $scope.showLoader = false;
                    $scope.State = angular.copy(response);
                    callback(response);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    $scope.GetCountyByStateId = function (modal) {

        $scope.mdlCountryState.selectedCounty = undefined;
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();


        $scope.GetCountyByState(modal.Id, function (data) {


            $scope.County = angular.copy(data);
            var county = $scope.County.filter(function (county) { return county.id == $scope.ownerData.CountyId });
            if (county.length > 0) {
                $scope.mdlCountryState.selectedCounty = county[0];
                $scope.CopyMdlCountryState.selectedCounty = $scope.mdlCountryState.selectedCounty;
            }

            var countyId = 0
            if ($scope.mdlCountryState.selectedCounty != undefined && $scope.mdlCountryState.selectedCounty != null && $scope.mdlCountryState.selectedCounty != '') {
                countyId = $scope.mdlCountryState.selectedCounty.id;
            }

            $scope.GetCityByStateOrCounty(modal.Id, countyId, function (data) {

                $scope.City = angular.copy(data);
                var city = $scope.City.filter(function (city) { return city.Id == $scope.ownerData.CityId });
                // console.log('city', city);

                if (city.length > 0) {
                    $scope.mdlCountryState.selectedCity = city[0];
                    $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
                }
            });
        });
    }

    $scope.ChangeCity = function (modal) {
        $scope.mdlCountryState.selectedCity = undefined;
        $scope.ChangeZip();

        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, modal.id, function (data) {

            $scope.City = angular.copy(data);
            var city = $scope.City.filter(function (city) { return city.Id == $scope.ownerData.CityId });
            // console.log('city', city);

            if (city.length > 0) {
                $scope.mdlCountryState.selectedCity = city[0];
                $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
            }

        });
    }

    $scope.GetCountyByState = function (stateId, callback) {

        $scope.showLoader = true;
        propayRegService.getCountyByState(stateId).
            success(function (response) {
                if (response != undefined) {
                    $scope.showLoader = false;
                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    $scope.GetCityByStateOrCounty = function (stateId, countyId, callback) {
        $scope.showLoader = true;
        propayRegService.getCityByStateOrCounty(stateId, countyId).
            success(function (response) {

                if (response != undefined) {
                    $scope.showLoader = false;
                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }


    $scope.clearddl = function ($event) {
        $event.stopPropagation();
        $scope.mdlCountryState.selectedCounty = undefined;
        var countyId = 0;

        $scope.GetCityByStateOrCounty($scope.mdlCountryState.selectedState.Id, countyId, function (data) {
            $scope.City = angular.copy(data);
            var city = $scope.City.filter(function (city) { return city.Id == $scope.mdlPerson.cityId });
            if (city.length > 0) {
                $scope.mdlCountryState.selectedCity = city[0];
                $scope.CopyMdlCountryState.selectedCity = $scope.mdlCountryState.selectedCity;
            }
        });
    };

    $scope.ChangeZip = function () {
        $scope.ownerData.zipCode = '';
        $scope.pZipValid = true;
    }

    $scope.checkZip = function (value) {
        return ($scope.ZipPattern).test(value);
    };

    $scope.ValidatePersonInfoZipCode = function (model, ele) {

        if (model.length == 5 || model.length == 10) {
            if ($scope.checkZip(model)) {
                $scope.ValidatePersonInfoZipCodeUsingAPI(model, ele);
            }
            else {
                swal("ERROR", 'invalid zip', "error");
            }
        }
    }

    $scope.ValidatePersonInfoZipCodeUsingAPI = function (ZipCode, ele) {


        //  console.log('ele', ele);

        var city = '';
        var County = '';
        var Country = '';
        //  $scope.pZipValid = false;
        //$scope.showLoader = true;

        var date = new Date();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + ZipCode + '&key=AIzaSyBARnZL0yQCppAcS4Vlyfp2pTDuD02e4v8', function (response) {

            if (response.status == "OK") {

                var address_components = response.results[0].address_components;

                // console.log('address_components ', address_components);

                $.each(address_components, function (index, component) {

                    var types = component.types;
                    // console.log('types ', types);

                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_1') {
                            //console.log('administrative_area_level_1 ', component.long_name);

                            state = component.long_name.toUpperCase();
                        }
                        if (type == 'administrative_area_level_2') {
                            //   console.log('administrative_area_level_2 ', component.long_name);
                            CountyFull = component.long_name;
                            County = CountyFull.replace(" County", "").toUpperCase();
                        }

                    });
                });

                if ($scope.mdlCountryState.selectedState.Name.toUpperCase().trim() == state.trim()) {
                    if ($scope.mdlCountryState.selectedCity.Name.toUpperCase().trim() == city.trim()) {

                        $scope.pZipValid = true;
                        $scope.$digest();
                        $scope.$apply();
                    }
                    else {

                        //$("#zip_code").focus();                       
                        $scope.pZipValid = false;
                        $scope.$digest();
                        $scope.$apply();
                        $("#" + ele).focus();
                    }
                }
                else {
                    // $("#zip_code").focus();                   
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
            }
            else {
                if (response.status == "ZERO_RESULTS") {
                    // $("#zip_code").focus();
                    $scope.pZipValid = false;
                    $scope.$digest();
                    $scope.$apply();
                    $("#" + ele).focus();
                }
                else {
                    $scope.pZipValid = true;
                    $scope.$digest();
                    $scope.$apply();
                }
            }
        });
    }


    $scope.ShowModel = function (EmailId) {

        var personTypeId = $rootScope.PersonTypeIdEnum.owner;

        $rootScope.$emit("ChangeContactEmail", { EmailId: EmailId, personTypeId: personTypeId });
    }


    $rootScope.$on("ChangeContactEmailSuccess", function (event, MdlParameters) {

        $scope.ownerData.PersonInfo.pEmail = MdlParameters.EmailId;

    });


    $scope.ActiveTab = function (tabName) {
        if (tabName.toUpperCase() == 'ONLINESTATEMENT' || tabName.toUpperCase() == 'TRANSACTIONS') {
            $rootScope.ShowInvoiceBillModel = false;
        }
        else {
            $rootScope.ShowInvoiceBillModel = true;
            $rootScope.ActiveTabName = '';
        }
        $rootScope.ActiveTabName = tabName;

    }


    $scope.tab1Class = "active2";
    $scope.tab2Class = "active2";
    $scope.tab3Class = "";
    $scope.tab4Class = "";
    $scope.tab5Class = "";

    $scope.setTabClass = function (tabNum) {
        $scope.tab = tabNum;
        $scope.tab1Class = "";
        $scope.tab2Class = "";
        $scope.tab3Class = "";
        $scope.tab4Class = "";
        $scope.tab5Class = "";
        if (tabNum == 1) {
            $scope.tab1Class = "active2";
            $scope.showLoader = false;
        }
        else if (tabNum == 2) {
            $scope.tab2Class = "active2";
        }
        else if (tabNum == 3) {
            $scope.tab3Class = "active2";
        }
        else if (tabNum == 4) {
            $scope.tab4Class = "active2";
        }
        else if (tabNum == 5) {
            $scope.tab5Class = "active2";
        }
    };



}


//ownerContactController