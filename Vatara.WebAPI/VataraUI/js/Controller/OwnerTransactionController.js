﻿vataraApp.controller('OwnerTransactionController', OwnerTransactionController);
OwnerTransactionController.$inject = ['$scope', 'financeService', 'serviceCategory_service', 'masterdataService',
                            'pagingService', '$filter', '$routeParams', '$location', '$rootScope', 'commonService'];
function OwnerTransactionController($scope, financeService, serviceCategory_service, masterdataService, pagingService, $filter,
                             $routeParams, $location, $rootScope, commonService) {

    var OTC = this;
    OTC.PageService = pagingService;
    OTC.selectCurrentPeriod = "";
    OTC.fromDate = new Date();
    OTC.toDate = new Date();
    OTC.lstTransections = [];
    OTC.lstTransections_Filtered = [];
    OTC.managerid = $rootScope.userData.personId;
    OTC.pmid = $rootScope.userData.ManagerDetail.pId;

    FC.personId = 0;
    FC.propoertyId = 0;
    FC.personType = $rootScope.userData.personTypeId;
    FC.isLease = false;
    FC.isManager = true;
    var currentDates = new Date();

    FC.TransectionFilterParameters = {
        owner: "",
        property: "",
        fromDate: new Date(),
        toDate: new Date(),
    }
    FC.lstPropertOwnersByManager = [];
    FC.CopylstPropertOwnersByManager = [];

    FC.lstPropertByManagerAndOwner = [];
    FC.CopylstPropertByManagerAndOwner = [];

    FC.lstTransections_OrderBy = "";
    FC.currentPage_lstTransections = 0;

    FC.groupByCategory = {};
    FC.PieChartCategoryArray = [];
    FC.IncmExpenseChartArray = [];


    FC.personId = 0;
    FC.propoertyId = 0;
    // FC.personType = 0;
    FC.current_toDate = new Date();
    FC.select_fromDate = new Date();

    OTC.POID = $rootScope.userData.poid;

    OTC.LoadFunction = function () {

        OTC.lstCurrentPeriod = angular.copy(Dateranges);
        OTC.selectCurrentPeriod = OTC.lstCurrentPeriod[0];
        OTC.pmid = $rootScope.userData.ManagerDetail.pId;
        OTC.personId = $rootScope.userData.personId;
        OTC.propoertyId = 0;
        OTC.personType = $rootScope.userData.personTypeId;

       // OTC.GetPropertByManagerAndOwner();
        OTC.GetPropertByOwnerPOID();
        OTC.FilterTransections();
    }

    OTC.GetPropertByOwnerPOID = function () {
        $scope.showLoader = true;

        financeService.getPropertByManagerAndOwner(FC.managerid, FC.isManager, FC.pmid, FC.personId, FC.propoertyId, FC.personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       FC.CopylstPropertByManagerAndOwner = response.Data;
                       var data = $filter('groupBy')(response.Data, 'propertyId');
                       angular.forEach(data, function (value, key) {

                           FC.lstPropertByManagerAndOwner.push(value[0])
                       });
                       var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerFullName = " All";
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       // FC.TransectionFilterParameters.property = addNewOption_All;

                       FC.TransectionFilterParameters.property = FC.lstPropertByManagerAndOwner[0];
                       FC.CopylstPropertByManagerAndOwner = FC.lstPropertByManagerAndOwner;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };



















    FC.TotalExpenditure = 0;
    FC.MgrExpence = 0;
    FC.MgrIncome = 0;

    FC.GetDate = function (value) {
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - value);
        return currentDate;
    }

    FC.FilterTransections = function (checkValue) {

        if (FC.selectCurrentPeriod != null) {

            if (FC.selectCurrentPeriod.date == 'custom' && checkValue == undefined) {

                FC.lstTransections = [];
                FC.lstTransections_Filtered = [];
                FC.IncmExpenseChartArray = [];
                FC.PieChartCategoryArray = [];

                if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                    // FC.PieChartLoad();
                    FC.IncmChartLoad();
                }

                return;
            }

            FC.current_toDate = new Date();
            FC.select_fromDate = new Date();

            //var currentDate = new Date().toISOString().slice(0, 10);
            //var selectDate;
            if (FC.selectCurrentPeriod.date == 'custom' && checkValue == 'custom') {

                FC.current_toDate = FC.toDate;
                FC.select_fromDate = FC.fromDate;
            }
            else {

                if (FC.selectCurrentPeriod.Period == 'Annual') {
                    FC.select_fromDate = new Date(new Date().getFullYear(), 0, 1);
                }
                else {
                    FC.select_fromDate = FC.selectCurrentPeriod.Startdate;
                    FC.current_toDate = FC.selectCurrentPeriod.EndDate;
                }
            }
            FC.lstTransections_Filtered = [];
            FC.IncmExpenseChartArray = [];
            FC.PieChartCategoryArray = [];

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                //FC.PieChartLoad();
                FC.IncmChartLoad();
            }

            FC.TransectionFilterParameters.owner = undefined;
            FC.TransectionFilterParameters.property = undefined;
            FC.GetAllTransections(FC.select_fromDate, FC.current_toDate);
        }
        else {
            FC.lstTransections = [];
            FC.IncmExpenseChartArray = [];
            FC.PieChartCategoryArray = [];
            FC.lstTransections_Filtered = [];

            if ($rootScope.PersonTyp.property_manager == $rootScope.userData.personType) {
                //FC.PieChartLoad();
                FC.IncmChartLoad();
            }

            FC.TransectionFilterParameters.owner = undefined;
            FC.TransectionFilterParameters.property = undefined;
        }
    };

    FC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    FC.ChangeCustomToDate = function () {
        FC.toDate = FC.fromDate;
    }

    FC.GetAllTransections = function (fromDate, toDate) {
        $scope.showLoader = true;
        if (PersonTypeEnum.owner == $rootScope.userData.personType) {
            FC.personId = $rootScope.userData.poid;
        }
        financeService.getAllTransections(FC.managerid, FC.isManager, fromDate, toDate, FC.pmid, FC.personId, FC.propoertyId, FC.personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   FC.lstTransections = response.Data.lstTransactionWithWriteOff;
                   FC.lstTransections_Filtered = angular.copy(FC.lstTransections);
                   FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner);
                   FC.ChartArrayForManager(response.Data.lstIncomeExpenseLiabilityChartData);

                   if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                       FC.personId = $rootScope.userData.personId;
                   }

                   if (PersonTypeEnum.tenant == $rootScope.userData.personType) {

                       FC.lstTransections_Filtered = FC.lstTransections.filter(function (transections) { return transections.propertyId === parseInt($rootScope.selectedPropertyId) });
                   }

                   FC.TransectionFilterParameters.owner = FC.CopylstPropertOwnersByManager[0];
                   FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
               }
               else {
                   swal("ERROR", response.Message, "error");
                   //FC.personId = $rootScope.userData.personId;
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
               if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
                   FC.personId = $rootScope.userData.personId;
               }
           });
    };

    FC.CreateLiabilityChart = function (model) {

        var ArrTotalInvoicedAmountToOwner = model.filter(function (transections) { return transections.amountType == 'TotalPaidAmountByOwner'});
        var ArrTotalPaidAmountByOwner = model.filter(function (transections) { return transections.amountType == 'TotalPaidAmountByOwner'});
        var ArrTotalAmountPMHasToPayToOwner = model.filter(function (transections) { return transections.amountType == 'TotalAmountPMHasToPayToOwner'});
        var ArrTotalAmountPMPaidToOwner = model.filter(function (transections) { return transections.amountType == 'TotalAmountPMPaidToOwner' });

        var TotalInvoicedAmountToOwner = $scope.sum(ArrTotalInvoicedAmountToOwner, 'amount');
        var TotalPaidAmountByOwner = $scope.sum(ArrTotalPaidAmountByOwner, 'amount');
        var TotalAmountPMHasToPayToOwner = $scope.sum(ArrTotalAmountPMHasToPayToOwner, 'amount');
        var TotalAmountPMPaidToOwner = $scope.sum(ArrTotalAmountPMPaidToOwner, 'amount');

        FC.LiabitilyChartArray = [];
        var LiabilityArr = [];
        LiabilityArr.push('Paid Liability', TotalAmountPMPaidToOwner);
        FC.LiabitilyChartArray.push(LiabilityArr);

        var NetAmountOwnerHasToPay = TotalInvoicedAmountToOwner - TotalPaidAmountByOwner;
        var NetAmountPMHasToPayToOwner = TotalAmountPMHasToPayToOwner - TotalAmountPMPaidToOwner;

        if ($rootScope.PersonTyp.property_manager != $rootScope.userData.personType) {
            var Liability = NetAmountPMHasToPayToOwner - NetAmountOwnerHasToPay;
            LiabilityArr = [];
            LiabilityArr.push('Liability', Liability);
            FC.LiabitilyChartArray.push(LiabilityArr);
            if (Liability > 0) {
                FC.LiabitilyChartLoad();
            }
            else {
                FC.LiabitilyChartArray = [];
            }
        }
        else if ($rootScope.PersonTyp.owner != $rootScope.userData.personType) {

            var Liability = NetAmountOwnerHasToPay - NetAmountPMHasToPayToOwner;
            LiabilityArr = [];
            LiabilityArr.push('Liability', Liability);
            FC.LiabitilyChartArray.push(LiabilityArr);
            if (Liability > 0) {
                FC.LiabitilyChartLoad();
            }
            else {
                FC.LiabitilyChartArray = [];
            }
        }
    }


  


    //FC.FilterProperty = function (mdl) {
    //    FC.currentPage_lstTransections = 0;
    //    if (mdl.ownerId == 0) {
    //        FC.lstTransections_Filtered = angular.copy(FC.lstTransections);
    //        FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
    //        FC.lstPropertByManagerAndOwner = FC.CopylstPropertByManagerAndOwner;
    //    }
    //    else {
    //        FC.lstTransections_Filtered = FC.lstTransections.filter(transection => transection.to == mdl.ownerId);
    //        if (FC.TransectionFilterParameters.owner != null) {
    //            if (mdl.personTypeId == 3) {
    //                console.log('FC.CopylstPropertByManagerAndOwner 111', FC.CopylstPropertByManagerAndOwner);
    //                FC.lstPropertByManagerAndOwner = FC.FilterTenantProp(FC.CopylstPropertByManagerAndOwner, FC.TransectionFilterParameters.owner.propertyId);
    //            }
    //            else {
    //                FC.lstPropertByManagerAndOwner = FC.Filter(FC.CopylstPropertByManagerAndOwner, FC.TransectionFilterParameters.owner.ownerId);
    //            }
    //            var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
    //            // addNewOption_All.ownerFullName = " All";
    //            addNewOption_All.ownerId = 0;
    //            addNewOption_All.propertyId = 0;
    //            addNewOption_All.propertyName = " All";
    //            FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
    //            FC.TransectionFilterParameters.property = addNewOption_All;
    //        }
    //        else {
    //            FC.lstPropertByManagerAndOwner = FC.CopylstPropertByManagerAndOwner;
    //            FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
    //        }
    //    }
    //    FC.ChartArray(FC.lstTransections_Filtered);
    //}


    //******* Above code is commented by Asr (OLD original FC.FilterProperty method) **********
    // It does not get the received transaction when filtering with owner/tenant because we filtering that on the basis of propertyID
    //**** Below is new method ******************// 

    FC.FilterProperty = function (mdl) {

        FC.personId = mdl.ownerId;
        FC.propoertyId = 0;
        FC.personType = mdl.personTypeId;

        FC.currentPage_lstTransections = 0;

        FC.GetTransactionByPersonAndProperty(FC.select_fromDate, FC.current_toDate, FC.personId, FC.propoertyId, FC.personType);

        //if (mdl.ownerId == 0) {
        //    FC.lstTransections_Filtered = angular.copy(FC.lstTransections);
        //    FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
        //    FC.lstPropertByManagerAndOwner = FC.CopylstPropertByManagerAndOwner;
        //}
        //else {          
        //    FC.lstTransections_Filtered = FC.lstTransections.filter(transection => transection.to == mdl.ownerId);
        //    if (FC.TransectionFilterParameters.owner != null) {
        //        if (mdl.personTypeId == 3) {
        //            console.log('FC.CopylstPropertByManagerAndOwner 111', FC.CopylstPropertByManagerAndOwner);
        //            FC.lstPropertByManagerAndOwner = FC.FilterTenantProp(FC.CopylstPropertByManagerAndOwner, FC.TransectionFilterParameters.owner.propertyId);
        //        }
        //        else {
        //            FC.lstPropertByManagerAndOwner = FC.Filter(FC.CopylstPropertByManagerAndOwner, FC.TransectionFilterParameters.owner.ownerId);
        //        }
        //        var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
        //        // addNewOption_All.ownerFullName = " All";
        //        addNewOption_All.ownerId = 0;
        //        addNewOption_All.propertyId = 0;
        //        addNewOption_All.propertyName = " All";
        //        FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
        //        FC.TransectionFilterParameters.property = addNewOption_All;
        //    }
        //    else {
        //        FC.lstPropertByManagerAndOwner = FC.CopylstPropertByManagerAndOwner;
        //        FC.TransectionFilterParameters.property = FC.CopylstPropertByManagerAndOwner[0];
        //    }
        //}
        // FC.ChartArray(FC.lstTransections_Filtered);
    }


    FC.GetTransactionByPersonAndProperty = function (fromDate, toDate, personId, propoertyId, personType) {
        $scope.showLoader = true;
        financeService.getTransactionByPersonAndProperty(FC.managerid, FC.isManager, FC.pmid, fromDate, toDate, personId, propoertyId, personType).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;

                   if (propoertyId == 0) {
                       FC.lstPropertByManagerAndOwner = response.Data.MdlPropertOwnersWithManager;
                       var addNewOption_All = angular.copy(FC.lstPropertByManagerAndOwner[0]);
                       addNewOption_All.ownerId = 0;
                       addNewOption_All.propertyId = 0;
                       addNewOption_All.propertyName = " All";
                       FC.lstPropertByManagerAndOwner.unshift(addNewOption_All);
                       FC.TransectionFilterParameters.property = addNewOption_All;
                   }
                   FC.lstTransections_Filtered = response.Data.lstMdlTransactionWithWriteOff;
                   FC.ChartArrayForManager(response.Data.lstIncomeExpenseLiabilityChartData);
                   FC.CreateLiabilityChart(response.Data.lstMdlManagerLiabilityToOwner);
               }
               else {
                   swal("ERROR", response.Message, "error");
               }
           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;
           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.Filter = function (items, propertyOwnerId) {

        var retArray = [];
        angular.forEach(items, function (obj) {

            if (obj.ownerId == propertyOwnerId) {
                retArray.push(obj);
            }
        });

        return retArray;
    }

    FC.FilterTenantProp = function (items, propertyId) {
        var retArray = [];
        angular.forEach(items, function (obj) {
            if (obj.propertyId == propertyId) {
                retArray.push(obj);
            }
        });

        return retArray;
    }

    FC.FilterTransectionByProperty = function (model) {

        FC.personId = FC.TransectionFilterParameters.owner.ownerId;
        FC.propoertyId = model.propertyId;
        FC.personType = FC.TransectionFilterParameters.owner.personTypeId;

        FC.GetTransactionByPersonAndProperty(FC.select_fromDate, FC.current_toDate, FC.personId, FC.propoertyId, FC.personType);

        //********* below code is commented by Asr ***********
        //*****************************************************


        //FC.currentPage_lstTransections = 0;
        //if (model.ownerId == 0 && FC.TransectionFilterParameters.owner != undefined && FC.TransectionFilterParameters.owner.ownerId != 0) {
        //    FC.lstTransections_Filtered = FC.lstTransections.filter(transection => transection.to == FC.TransectionFilterParameters.owner.ownerId);
        //}
        //else if (model.ownerId != 0 && FC.TransectionFilterParameters.owner != undefined && FC.TransectionFilterParameters.owner.ownerId != 0) {
        //    FC.lstTransections_Filtered = FC.lstTransections.filter(transection => transection.propertyId === model.propertyId && transection.to == FC.TransectionFilterParameters.owner.ownerId);
        //}
        //else {
        //    FC.lstTransections_Filtered = FC.lstTransections.filter(transection => transection.propertyId === model.propertyId);
        //}
        // FC.ChartArray(FC.lstTransections_Filtered);
    }

    FC.GetPropertOwnersByManager = function () {
        $scope.showLoader = true;
        financeService.getPropertOwnersByManager(FC.managerid).
           success(function (response) {
               if (response.Status == true) {
                   $scope.showLoader = false;
                   if (response.Data.length > 0) {

                       var data = $filter('groupBy')(response.Data, 'ownerId');
                       angular.forEach(data, function (value, key) {

                           FC.lstPropertOwnersByManager.push(value[0])
                       });
                       var addNewOption_All = angular.copy(FC.lstPropertOwnersByManager[0]);
                       addNewOption_All.ownerFullName = " All";
                       addNewOption_All.ownerId = 0;
                       FC.lstPropertOwnersByManager.unshift(addNewOption_All);

                       FC.TransectionFilterParameters.owner = addNewOption_All;
                       FC.CopylstPropertOwnersByManager = FC.lstPropertOwnersByManager;
                   }
               }
               else {
                   swal("ERROR", response.Message, "error");
               }

           }).error(function (data, status) {
               if (status == 401) {
                   commonService.SessionOut();
               }
               else {
                   swal("ERROR", MessageService.serverGiveError, "error");
               }
               $scope.showLoader = false;

           }).finally(function (data) {
               $scope.showLoader = false;
           });
    };

    FC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    //*****************************************************
    //Chart Methods
    //*****************************************************

    $scope.sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop];
        }, 0);
    };

    FC.ChartArrayForManager = function (modal) {

        FC.ViewIncomeCategory = 'Income';
        FC.IncomeCategory = [];
        FC.IncmChartArray = [];
        FC.EarnedIncmChartArray = [];
        FC.PendingIncmChartArray = [];

        FC.groupByCategory = $filter('groupBy')(modal, 'Category');
        angular.forEach(FC.groupByCategory, function (value, key) {

            var innergroup = $filter('groupBy')(value, 'Type');
            if (key == 'Income') {
                FC.IncomeCategory.push(key);
                angular.forEach(innergroup, function (innerValue, key2) {
                    FC.IncomeCategory.push(key2);
                    var IncomeGroup = $filter('groupBy')(innerValue, 'CategoryName');
                    angular.forEach(IncomeGroup, function (IncomeGroupValue, key3) {
                        var IncomeGroupTotal = $scope.sum(IncomeGroupValue, 'Amount');
                        if (parseFloat(IncomeGroupTotal) > 0) {
                            var IncmArr = [];
                            IncmArr.push(key3, IncomeGroupTotal);
                            if (key2 == 'Earned') {
                                FC.EarnedIncmChartArray.push(IncmArr);
                            }
                            else {
                                FC.PendingIncmChartArray.push(IncmArr);
                            }
                        }
                    });
                    var total = $scope.sum(innerValue, 'Amount');
                    var IncmArr = [];
                    IncmArr.push(key2, total);
                    FC.IncmChartArray.push(IncmArr);
                });
            }
            //else if (key == 'Liability') {
            //    var innergroup = $filter('groupBy')(value, 'Type');
            //    //console.log('Expense innergroup', innergroup);
            //    angular.forEach(innergroup, function (innerValue, key2) {
            //        //console.log('innerValue', innerValue);
            //        var LiabitilyInnergroup = $filter('groupBy')(innerValue, 'CategoryName');
            //        angular.forEach(LiabitilyInnergroup, function (innerValue2, key3) {
            //            var total = $scope.sum(innerValue2, 'Amount');
            //            //console.log('total', total);
            //            var ExpenseArr = [];
            //            ExpenseArr.push(key3, total);
            //            FC.LiabitilyChartArray.push(ExpenseArr);
            //        });
            //    });
            //    // console.log('FC.ExpenseChartArray', FC.ExpenseChartArray);
            //}
        });
        if (PersonTypeEnum.property_manager == $rootScope.userData.personType) {
            FC.IncmChartLoad();
            FC.EarnedIncmChartLoad();
            FC.PendingIncmChartLoad();
            //FC.ExpenseChartLoad();
            //FC.LiabitilyChartLoad();
        }
    };


    FC.ChartArrayForOwner = function (modal) {

        FC.ViewIncomeCategory = 'Income';
        FC.IncomeCategory = [];
        FC.IncmChartArray = [];
        FC.EarnedIncmChartArray = [];
        FC.PendingIncmChartArray = [];

        FC.ViewExpenseCategory = 'Expense';
        FC.ExpenseCategory = [];
        FC.ExpenseChartArray = [];
        FC.RealisedExpenseChartArray = [];
        FC.UnRealisedExpenseChartArray = [];

        FC.groupByCategory = $filter('groupBy')(modal, 'Category');
        angular.forEach(FC.groupByCategory, function (value, key) {

            var innergroup = $filter('groupBy')(value, 'Type');
            if (key == 'Income') {
                FC.IncomeCategory.push(key);
                angular.forEach(innergroup, function (innerValue, key2) {
                    FC.IncomeCategory.push(key2);
                    var IncomeGroup = $filter('groupBy')(innerValue, 'CategoryName');
                    angular.forEach(IncomeGroup, function (IncomeGroupValue, key3) {
                        var IncomeGroupTotal = $scope.sum(IncomeGroupValue, 'Amount');
                        if (parseFloat(IncomeGroupTotal) > 0) {
                            var IncmArr = [];
                            IncmArr.push(key3, IncomeGroupTotal);
                            if (key2 == 'Earned') {
                                FC.EarnedIncmChartArray.push(IncmArr);
                            }
                            else {
                                FC.PendingIncmChartArray.push(IncmArr);
                            }
                        }
                    });
                    var total = $scope.sum(innerValue, 'Amount');
                    var IncmArr = [];
                    IncmArr.push(key2, total);
                    FC.IncmChartArray.push(IncmArr);
                });
            }

            if (key == 'Expense') {
                FC.ExpenseCategory.push(key);
                angular.forEach(innergroup, function (innerValue, key2) {
                    FC.IncomeCategory.push(key2);
                    var ExpenseGroup = $filter('groupBy')(innerValue, 'CategoryName');
                    angular.forEach(ExpenseGroup, function (ExpenseGroupValue, key3) {
                        var ExpenseGroupTotal = $scope.sum(ExpenseGroupValue, 'Amount');
                        if (parseFloat(ExpenseGroupTotal) > 0) {
                            var ExpenseArr = [];
                            ExpenseArr.push(key3, ExpenseGroupTotal);
                            if (key2 == 'Realised') {
                                FC.RealisedExpenseChartArray.push(ExpenseArr);
                            }
                            else {
                                FC.UnRealisedExpenseChartArray.push(ExpenseArr);
                            }
                        }
                    });
                    var total = $scope.sum(innerValue, 'Amount');
                    var ExpenseArr = [];
                    ExpenseArr.push(key2, total);
                    FC.ExpenseChartArray.push(ExpenseArr);
                });
            }
        });

        FC.IncmChartLoad();
        FC.EarnedIncmChartLoad();
        FC.PendingIncmChartLoad();
        FC.ExpenseChartLoad();
        FC.RealisedExpenseChartLoad();
        FC.UnRealisedExpenseChartLoad();
    };

    FC.ExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawExpenseChart);

        function drawExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.ExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('ExpenseChart'));
            chart.draw(data, options);
        }
    }

    FC.RealisedExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawRealisedExpenseChart);

        function drawRealisedExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.RealisedExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart (Realised)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('RealisedExpenseChart'));
            chart.draw(data, options);
        }
    }

    FC.UnRealisedExpenseChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawUnRealisedExpenseChart);

        function drawUnRealisedExpenseChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.UnRealisedExpenseChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Expense Chart (Unrealised)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('UnrealisedExpenseChart'));
            chart.draw(data, options);
        }
    }


    FC.IncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawIncmChart);

        function drawIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.IncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('IncmChart'));
            chart.draw(data, options);
        }
    }

    FC.EarnedIncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawEarnedIncmChart);

        function drawEarnedIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.EarnedIncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart (Earned)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('EarnedIncmChart'));
            chart.draw(data, options);
        }
    }

    FC.PendingIncmChartLoad = function () {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawPendingIncmChart);

        function drawPendingIncmChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.PendingIncmChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Income Chart (Pending)'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('PendingIncmChart'));
            chart.draw(data, options);
        }
    }

    //FC.ExpenseChartLoad = function () {
    //    // Load the Visualization API and the corechart package.
    //    google.charts.load('current', { 'packages': ['corechart'] });
    //    // Set a callback to run when the Google Visualization API is loaded.
    //    google.charts.setOnLoadCallback(drawExpenseChart);
    //    function drawExpenseChart() {
    //        // Create the data table.
    //        var data = new google.visualization.DataTable();
    //        data.addColumn('string', 'Topping');
    //        data.addColumn('number', 'Slices');
    //        data.addRows(FC.ExpenseChartArray);
    //        var colorarr = [];
    //        // Set chart options
    //        var options = {
    //            title: 'Expense Chart'
    //        };
    //        // Instantiate and draw our chart, passing in some options.
    //        var chart = new google.visualization.PieChart(document.getElementById('ExpenseChart'));
    //        chart.draw(data, options);
    //    }
    //}

    FC.LiabitilyChartLoad = function () {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawLiabilityChart);

        function drawLiabilityChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows(FC.LiabitilyChartArray);
            var colorarr = [];

            // Set chart options
            var options = {
                title: 'Liability Chart'
            };
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('LiabitilyChart'));
            chart.draw(data, options);
        }
    }

    //*****************************************************
    //End Chart Methods
    //*****************************************************

    FC.ButtonClick_Excel = function (id, excelName) {
        $(id).table2excel({
            filename: excelName
        });
    }

    FC.ButtonClick_Pdf = function (id, pdfName, headerName) {
        HtmlTableToPdf(id, pdfName, headerName, '', '');
    }

}