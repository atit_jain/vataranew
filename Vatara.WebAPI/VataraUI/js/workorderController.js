
vataraApp.controller('workorderController', function ($scope, $http, $window, $routeParams, $location, $rootScope,
    $filter, propertyService, personService, invoiceService,
    masterdataService, mainFactoryService, workorderService, serverSidePagingService, commonService, VendorService) {

    var WOC = this;
    WOC.managerid = $rootScope.userData.personId;
    WOC.PMID = $rootScope.userData.ManagerDetail.pId;
    WOC.propertieslist = [];
    WOC.selectedProperty = '';
    WOC.lstServiceCategory = [];
    WOC.lstVendor = [];
    // WOC.lstRequestedBy = PersonWithPersonTypeId;
    WOC.lstRequestedBy = [];
    WOC.RequestedBy = '';
    WOC.wrArray = [];
    WOC.pVendor = {};
    WOC.woRequestedDate = new Date();
    WOC.woExpectedStartDate = new Date();
    WOC.woActualStartDate = new Date();
    WOC.TodayDate = new Date();
    WOC.woDescription = '';
    WOC.documentArray = [];
    WOC.WorkOrderEntryMode = 'Create'
    WOC.WOID = 0;
    WOC.DocArray = [];
    WOC.lstPropertByManagerAndOwner = [];
    WOC.WorkOrderFilterParameters = '';

    WOC.PageService = serverSidePagingService;
    WOC.itemsPerPage = WOC.PageService.itemsPerPage;
    WOC.currentPage_lstWorkOrder = 0;
    WOC.lstWorkOrder_OrderBy = "";
    WOC.lstWorkOrder = [];
    WOC.lstWorkOrder_Filtered = [];
    WOC.workOrderselectedProperty = '';
    WOC.arrWorkOrderStatus = ['All', 'Open', 'Closed', 'Cancelled'];
    WOC.workOrderStatus = WOC.arrWorkOrderStatus[1];
    WOC.TotalWorkOrderCount = 0;
    WOC.Page = 0;
    WOC.PageRange = [];
    WOC.pageNo = 0;
    WOC.CurrentSelectedPage = 0;
    WOC.mdlWorkRequestStatus = [];
    WOC.selectedWorkOrderStatus = '';
    WOC.UpdateIndex = false;
    WOC.IndexValue = '';
    WOC.isManager = false;
    WOC.personId = $rootScope.userData.personId;
    WOC.personType = $rootScope.userData.personTypeId;
    // WOC.EditCreateNew = false;

    WOC.mdlWR = {
        counter: 0,
        wrid: $scope.pWRId,
        showRemove: false,
        description: '',
        PONumber: '',
        invoiceNumber: '',
        equipmentId: '',
        //WRType: '0',
        serviceCategory: '',
        vendorId: '0',
        wrrequestedon: new Date(),
        wrexpcompldate: new Date(),
        wrnextschedule: new Date(),
        isNew: true,
        isDeleted: false
    };

    WOC.mdlWorkOrder = {

        id: 0,
        propertyId: 0,
        description: '',
        requestedDate: new Date(),
        requestedBy: 0,
        status: 0,
        isDeleted: false,
        creatorUserId: $rootScope.userData.personId,
        lstWorkrequest: []
    };

    WOC.mdlWorkRequest = {
        id: 0,
        workorderId: 0,
        description: '',
        poNumber: '',
        invoiceNumber: '',
        equipmentId: '',
        //wrType: 0,
        serviceCategory: '',
        vendor: '',
        wrStatus: '',
        expectedStartDate: new Date(),
        expectedCompletionDate: new Date(),
    }
    $scope.pId = 0;

    // WOC.wrType = [{ id: 1, value: 'On-Demand' }, { id: 2, value: 'PM' }];

    WOC.propertyId = 0;
    WOC.isSubTab = false;
    WOC.isownerSubTab = false;
    if ($location.url().match("propertyProfile/")) {

        if (!angular.isUndefined($routeParams.PropertyNum)) {
            WOC.isSubTab = true;
            WOC.propertyId = $routeParams.PropertyNum;
        }
    }
    else if ($location.url().match("leaseview/")) {
        WOC.isSubTab = true;
    }
    else if ($location.url().match("OwnerHome")) {
        WOC.isSubTab = true;
        WOC.isownerSubTab = true;
        if (WOC.managerid != 0) {
            workorderService.getPropertByManagerAndOwner(WOC.managerid, WOC.isManager, WOC.PMID, WOC.personId, WOC.propertyId, WOC.personType).
                //workorderService.getPropertByManagerAndOwner(WOC.managerid, WOC.isManager, WOC.PMID, WOC.personId, WOC.propertyId, WOC.personType).
                success(function (response) {
                    if (response.Status == true) {
                        $scope.showLoader = false;
                        if (response.Data.length > 0) {
                            var data = $filter('groupBy')(response.Data, 'propertyId');
                            angular.forEach(data, function (value, key) {
                                WOC.lstPropertByManagerAndOwner.push(value[0])
                            });
                        }
                    }
                    else {
                        swal("ERROR", response.Message, "error");
                    }
                }).error(function (data, status) {
                    if (status == 401) {
                        commonService.SessionOut();
                    }
                    else {
                        swal("ERROR", MessageService.serverGiveError, "error");
                    }
                    $scope.showLoader = false;
                }).finally(function (data) {
                    $scope.showLoader = false;
                });
        }
    }
    else if ($location.url().match("TenantHome")) {
        WOC.isSubTab = true;
        WOC.isownerSubTab = true;
        if (WOC.managerid != 0) {
            workorderService.getPropertByManagerAndOwner(WOC.managerid, WOC.isManager, WOC.PMID, WOC.personId, WOC.propertyId, WOC.personType).
                //workorderService.getPropertByManagerAndOwner(WOC.managerid, WOC.isManager, WOC.PMID, WOC.personId, WOC.propertyId, WOC.personType).
                success(function (response) {
                    if (response.Status == true) {
                        $scope.showLoader = false;
                        if (response.Data.length > 0) {
                            var data = $filter('groupBy')(response.Data, 'propertyId');
                            angular.forEach(data, function (value, key) {
                                WOC.lstPropertByManagerAndOwner.push(value[0])
                            });
                        }
                    }
                    else {
                        swal("ERROR", response.Message, "error");
                    }
                }).error(function (data, status) {
                    if (status == 401) {
                        commonService.SessionOut();
                    }
                    else {
                        swal("ERROR", MessageService.serverGiveError, "error");
                    }
                    $scope.showLoader = false;
                }).finally(function (data) {
                    $scope.showLoader = false;
                });
        }
    }

    WOC.FilterWorkOrderByProperty = function (PropertyID) {
        WOC.propertyId = PropertyID;
        WOC.Page = 1;
        WOC.PageRange = [];
        WOC.GetWorkOrderByStatus(WOC.Page, function (data) {
            WOC.currentPage_lstWorkOrder = 0;
            WOC.lstWorkOrder = angular.copy(data.lstMdlWorkOrderByStatus);
            WOC.lstWorkOrder_Filtered = angular.copy(WOC.lstWorkOrder);
            WOC.TotalWorkOrderCount = angular.copy(data.TotalWorkOrderCount);
            WOC.PageRange = serverSidePagingService.GetPageRangeValue(WOC.lstWorkOrder.length, WOC.Page);
            WOC.CurrentSelectedPage = WOC.PageRange[0];
            WOC.Page = WOC.Page + 3;
        });

    }


    WOC.LoadCreateWorkOrder = function () {
        WOC.selectedWorkOrderStatus = '';

        if (!angular.isUndefined($routeParams.workOrderId)) {

            $scope.pId = $routeParams.workOrderId;
            WOC.WorkOrderEntryMode = 'Edit';
            WOC.wrArray = [];
            WOC.documentArray = [];
            WOC.GetWorkOrderStatus(function (Statusdata) {
                WOC.GetPropertiesByPMID(WOC.PMID, function (data) {
                    WOC.GetAllVendors(function (Vendorsdata) {
                        WOC.GetVendorServiceCategory(0);
                        WOC.GetWorkOrderWithWorkRequests($routeParams.workOrderId, function (WorkOrderWithWorkRequestsData) {
                            var workorder = WorkOrderWithWorkRequestsData.Workorder;
                            WOC.GetPropertyOwnerTenantManager(WOC.selectedProperty.propertyId, function (data) {
                                WOC.RequestedBy = WOC.lstRequestedBy.filter(function (p) { return p.id == workorder.requestedBy && p.personType == workorder.requestedByPersonType })[0];
                            });
                        });
                    });
                });
            });
            WOC.GetWorkorderDocuments($routeParams.workOrderId);
        }
        else {
            WOC.GetPropertiesByPMID(WOC.PMID, function (data) {
                if ($location.url().match("property/createworkorder")) {
                    if (!angular.isUndefined($routeParams.propertyId)) {
                        WOC.selectedProperty = data.filter(function (p) { return p.propertyId == $routeParams.propertyId })[0];
                        WOC.GetPropertyOwnerTenantManagerByPropertyId($routeParams.propertyId);
                    }
                }
            });
            WOC.GetAllVendors(function (data) { });
            WOC.GetVendorServiceCategory(0);
        }
    };

    WOC.CancelCreateWorkOrder = function () {
        if ($location.url().match("property/createworkorder")) {
            if (!angular.isUndefined($routeParams.propertyId)) {
                $location.path("/propertyProfile/" + $routeParams.propertyId);
            }
            else {
                $location.path("/properties");
            }
        }
        else {
            $location.path("/workorders");
        }
    }

    WOC.GetWorkOrderWithWorkRequests = function (WorkOrderId, callback) {
        $scope.showLoader = true;
        workorderService.getWorkOrderWithWorkRequests(WorkOrderId).
            success(function (response) {
                if (response.Status == true) {

                    var workorder = response.Data.Workorder;
                    WOC.selectedWorkOrderStatus = WOC.mdlWorkRequestStatus.filter(function (p) { return p.id == workorder.status })[0];

                    // console.log(' WOC.selectedWorkOrderStatus', WOC.selectedWorkOrderStatus);
                    var lstworkrequest = angular.copy(response.Data.lstWorkrequest);
                    WOC.WOID = workorder.id;
                    WOC.woRequestedDate = workorder.requestedDate;
                    WOC.woDescription = workorder.description;
                    WOC.selectedProperty = WOC.propertieslist.filter(function (p) { return p.propertyId == workorder.propertyId })[0];

                    angular.forEach(lstworkrequest, function (value, key) {
                        //console.log('value 111', value);
                        var arrworkOrdr = angular.copy(WOC.mdlWorkRequest);
                        arrworkOrdr.id = value.id;
                        arrworkOrdr.workorderId = value.workorderId;
                        arrworkOrdr.description = value.description;
                        arrworkOrdr.poNumber = value.poNumber;
                        arrworkOrdr.invoiceNumber = value.invoiceNumber;
                        arrworkOrdr.equipmentId = value.equipmentId;
                        arrworkOrdr.status = value.status;
                        arrworkOrdr.expectedStartDate = value.expectedStartDate;
                        arrworkOrdr.expectedCompletionDate = value.expectedCompletionDate;
                        //arrworkOrdr.strStatus = WOC.mdlWorkRequestStatus.filter(s => s.id == value.status)[0].name;

                        var _status = WOC.mdlWorkRequestStatus.filter(function (p) { return p.id == value.status });
                        if (_status.length > 0) {
                            arrworkOrdr.strStatus = _status[0].name;
                        }

                        // arrworkOrdr.wrType = WOC.wrType.filter(wr =>wr.id == value.wrType)[0];;
                        var mdl = {
                            id: value.serviceCategoryId,
                            name: value.serviceCategoryName
                        };
                        arrworkOrdr.serviceCategory = mdl;
                        arrworkOrdr.vendor = WOC.lstVendor.filter(function (v) { return v.vendorId == value.vendorId })[0];
                        WOC.wrArray.push(arrworkOrdr);

                        callback(response.Data);
                    });
                    // console.log('WOC.wrArray', WOC.wrArray);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    WOC.EditWorkRequest = function (workrequestId, action) {
        WOC.mdlWorkRequest = angular.copy(WOC.wrArray.filter(function (wr) { return wr.id == workrequestId })[0]);

        if (WOC.mdlWorkRequestStatus != undefined && WOC.mdlWorkRequestStatus.length > 0) {
            WOC.GetWorkOrderStatus(function (data) {
                //WOC.mdlWorkRequest.wrStatus = angular.copy(WOC.mdlWorkRequestStatus.filter(s => s.id == WOC.mdlWorkRequest.status)[0].name);

                var _status = WOC.mdlWorkRequestStatus.filter(function (s) { return s.id == WOC.mdlWorkRequest.status });
                if (_status.length > 0) {
                    WOC.mdlWorkRequest.wrStatus = angular.copy(_status[0].name);
                }

            });
        }
        else {
            //WOC.mdlWorkRequest.wrStatus = angular.copy(WOC.mdlWorkRequestStatus.filter(s => s.id == WOC.mdlWorkRequest.status)[0].name);

            var _status = WOC.mdlWorkRequestStatus.filter(function (s) { return s.id == WOC.mdlWorkRequest.status });
            if (_status.length > 0) {
                WOC.mdlWorkRequest.wrStatus = angular.copy(_status[0].name);
            }
        }
        var vendorId = 0
        if (WOC.mdlWorkRequest.vendor != undefined) {
            vendorId = WOC.mdlWorkRequest.vendor.vendorId;
        };
        WOC.GetVendorServiceCategories(vendorId, function (data) {
            var serviceId = WOC.mdlWorkRequest.serviceCategory.id;
            WOC.mdlWorkRequest.serviceCategory = data.filter(function (c) { return c.id == serviceId })[0];
        });
        if (action == 'View') {
            WOC.WorkRequestEntrymode = 'View';
        }
        else {
            WOC.WorkRequestEntrymode = 'Update';
        }
        $('#mdlAddWorkRequest').modal('show');
    }

    WOC.GetPropertiesByPMID = function (PMID, callback) {
        $scope.showLoader = true;
        WOC.propertieslist = [];
        propertyService.getPropertiesByPMID(PMID).
            success(function (response) {
                if (response.Status == true) {

                    WOC.propertieslist = response.Data;
                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
        // WOC.addWR(this);
    }

    WOC.GetVendorServiceCategory = function (VendorId) {
        if (VendorId != undefined) {
            WOC.mdlWorkRequest.serviceCategory = '';
            WOC.GetVendorServiceCategories(VendorId, function (data) {
            });
        }
    }
    WOC.GetVendorServiceCategories = function (VendorId, callback) {
        $scope.showLoader = true;
        VendorService.getVendorServiceCategories(VendorId).
            success(function (data) {
                $scope.showLoader = false;
                WOC.lstServiceCategory = data.Data;
                callback(data.Data);

            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    WOC.GetAllVendors = function (callback) {
        $scope.showLoader = true;
        var PMID = $rootScope.userData.ManagerDetail.pId;
        VendorService.getAllVendors(PMID).
            success(function (data) {
                $scope.showLoader = false;
                WOC.lstVendor = data.Data;
                callback(data.Data);
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    WOC.LoadWorkOrder = function () {
        WOC.Page = 1;
        WOC.PageRange = [];
        workorderService.SelectedWorkOrderStatus = '';
        WOC.GetWorkOrderByStatus(WOC.Page, function (data) {

            WOC.lstWorkOrder = angular.copy(data.lstMdlWorkOrderByStatus);
            WOC.lstWorkOrder_Filtered = angular.copy(WOC.lstWorkOrder);
            WOC.TotalWorkOrderCount = angular.copy(data.TotalWorkOrderCount);
            WOC.PageRange = serverSidePagingService.GetPageRangeValue(WOC.lstWorkOrder.length, WOC.Page);
            WOC.CurrentSelectedPage = WOC.PageRange[0];
            WOC.Page = WOC.Page + 3;
        });
    };

    WOC.StatusChange = function () {
        WOC.Page = 1;
        WOC.PageRange = [];
        WOC.GetWorkOrderByStatus(WOC.Page, function (data) {
            WOC.currentPage_lstWorkOrder = 0;
            WOC.lstWorkOrder = angular.copy(data.lstMdlWorkOrderByStatus);
            WOC.lstWorkOrder_Filtered = angular.copy(WOC.lstWorkOrder);
            WOC.TotalWorkOrderCount = angular.copy(data.TotalWorkOrderCount);
            WOC.PageRange = serverSidePagingService.GetPageRangeValue(WOC.lstWorkOrder.length, WOC.Page);
            WOC.CurrentSelectedPage = WOC.PageRange[0];
            WOC.Page = WOC.Page + 3;
        });
    }

    WOC.GetPreviousRecord = function (pageNo) {

        getCurrentpage = WOC.PageRange[0];
        getCurrentpage -= 1;
        if (getCurrentpage == pageNo && pageNo > 0) {

            WOC.Page = pageNo - 2;
            WOC.currentPage_lstWorkOrder = 0;
            WOC.GetWorkOrderByStatus(WOC.Page, function (data) {

                WOC.lstWorkOrder = angular.copy(data.lstMdlWorkOrderByStatus);
                WOC.lstWorkOrder_Filtered = angular.copy(WOC.lstWorkOrder);
                WOC.TotalWorkOrderCount = angular.copy(data.TotalWorkOrderCount);
                WOC.PageRange = [];
                WOC.PageRange = serverSidePagingService.GetPageRangeValue(WOC.lstWorkOrder.length, WOC.Page);
                WOC.CurrentSelectedPage = WOC.PageRange[0];
                WOC.Page += 3;
            });
        }
    }

    WOC.GetNewRecord = function (pageNo) {

        getCurrentpage = WOC.PageRange[WOC.PageRange.length - 1];
        getCurrentpage += 1;
        if (getCurrentpage == WOC.CurrentSelectedPage && Math.ceil(WOC.TotalWorkOrderCount / WOC.itemsPerPage) >= getCurrentpage) {

            var currentPageNo = WOC.PageRange[WOC.PageRange.length - 1] + 1;
            WOC.currentPage_lstWorkOrder = 0;
            WOC.GetWorkOrderByStatus(currentPageNo, function (data) {

                WOC.lstWorkOrder = angular.copy(data.lstMdlWorkOrderByStatus);
                WOC.lstWorkOrder_Filtered = angular.copy(WOC.lstWorkOrder);
                WOC.TotalWorkOrderCount = angular.copy(data.TotalWorkOrderCount);
                WOC.PageRange = [];
                WOC.PageRange = serverSidePagingService.GetPageRangeValue(WOC.lstWorkOrder.length, WOC.Page);
                WOC.CurrentSelectedPage = WOC.PageRange[0];
                WOC.Page += 3;
            });
        }
    };

    WOC.NextClick = function ($event) {
        var maxpage = WOC.TotalWorkOrderCount / WOC.itemsPerPage;

        if (WOC.CurrentSelectedPage == Math.ceil(maxpage)) {
            return;
        }
        if (!Number.isInteger(maxpage)) {
            maxpage += 1;
        }
        WOC.CurrentSelectedPage = WOC.CurrentSelectedPage + 1;
        WOC.currentPage_lstWorkOrder = WOC.PageService.NextPage(WOC.TotalWorkOrderCount, WOC.currentPage_lstWorkOrder);
        WOC.GetNewRecord(WOC.CurrentSelectedPage);
    }

    WOC.PrevClick = function ($event) {

        if (WOC.CurrentSelectedPage == 1) {
            return;
        }
        WOC.CurrentSelectedPage = WOC.CurrentSelectedPage - 1;
        WOC.currentPage_lstWorkOrder = WOC.PageService.PrevPage(WOC.currentPage_lstWorkOrder);
        // WOC.currentPage_lstWorkOrder;
        WOC.GetPreviousRecord(WOC.CurrentSelectedPage);
    }

    WOC.GetWorkOrderByStatus = function (page, callback) {

        $scope.showLoader = true;
        if ($rootScope.userData.personType == "tenant")
        { var poid = $rootScope.userData.personId;}
        else{
            var poid=$rootScope.userData.poid;}
        workorderService.getWorkOrderByStatus(WOC.workOrderStatus, WOC.PMID, WOC.itemsPerPage, page, WOC.propertyId,poid).
            success(function (response) {
                if (response.Status == true) {
                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    $scope.getServiceCategory = function (onComplete) {
        if (angular.isUndefined($rootScope.ServiceCategoryList) || $rootScope.ServiceCategoryList == null) {
            masterdataService.getServiceCategory(function (result, data) {
                if (result) {
                    $scope.servicecategorylist = $rootScope.ServiceCategoryList;
                    for (var i = 0; i < $scope.servicecategorylist.length; i++) {
                        if ($scope.pServiceCategoryId == $scope.servicecategorylist[i].Id) {
                            $scope.pServiceCategory = $scope.servicecategorylist[i];
                            break;
                        }
                    }
                }
                else {
                    swal("ERROR", "Unable to fetch service category.", "error");
                }
            });
        } else {
            $scope.servicecategorylist = $rootScope.ServiceCategoryList;
            for (var i = 0; i < $scope.servicecategorylist.length; i++) {
                if ($scope.pServiceCategoryId == $scope.servicecategorylist[i].Id) {
                    $scope.pServiceCategory = $scope.servicecategorylist[i];
                    break;
                }
            }
        }
    }

    masterdataService.getPerson(function (result, data) {
        if (result) {
            //$scope.personlist = data; //angular.toJson(data, true);;
            $scope.vendorlist = $rootScope.PersonList.filter(function (c) {
                return c.pTypeId == 4;//For Vendor
            });
            isCall2 = true;
        }
        else {
            swal("ERROR", "Unable to fetch vendors.", "error");
        }
    });

    $scope.getWorkorderSummary = function () {
        workorderService.getWorkorderSummary(function (result, data) {
            if (result) {
                $scope.workordersummary = data;
            }
            else {
                swal("ERROR", "Unable to fetch workorder summary.", "error");
            }
        });
    }

    var wrCounter = 0;

    WOC.addWR = function (frmWorkRequest) {
        var data = angular.copy(WOC.mdlWorkRequest);
        WOC.wrArray.push(data);
        WOC.ClearMdlWorkRequest(frmWorkRequest);
    };

    WOC.removeWR = function (index) {
        WOC.wrArray.splice(index);
    };

    WOC.UpdateWR = function (frmWorkRequest) {
        var data = angular.copy(WOC.mdlWorkRequest);
        WOC.wrArray[WOC.IndexValue] = data;
        WOC.ClearMdlWorkRequest(frmWorkRequest);
    }

    WOC.EditWR = function (Id, index) {
        var data = WOC.wrArray[index];
        //WOC.mdlWorkRequest.id = 0;
        //WOC.mdlWorkRequest.workorderId = 0;
        WOC.mdlWorkRequest.description = data.description;
        WOC.mdlWorkRequest.poNumber = data.poNumber;
        WOC.mdlWorkRequest.invoiceNumber = data.invoiceNumber;
        //WOC.mdlWorkRequest.equipmentId = '';
        //WOC.mdlWorkRequest.wrType = 0;
        WOC.mdlWorkRequest.serviceCategory = data.serviceCategory;
        WOC.mdlWorkRequest.vendor = data.vendor;
        WOC.mdlWorkRequest.expectedStartDate = data.expectedStartDate;
        WOC.mdlWorkRequest.expectedCompletionDate = data.expectedCompletionDate;
        // WOC.mdlWorkRequest = angular.copy(WOC.wrArray.filter(wr => wr.id == workrequestId)[0]);
        WOC.UpdateIndex = true;
        WOC.IndexValue = index;
        $('#mdlAddWorkRequest').modal('show');
    }

    WOC.ClearWorkOrderForm = function () {
        WOC.WorkOrderEntryMode = 'Create';
        WOC.WOID = 0;
        WOC.selectedProperty = '';
        WOC.RequestedBy = '';
        WOC.woRequestedDate = new Date();
        WOC.woDescription = '';
        WOC.wrArray = [];
        // WOC.ClearMdlWorkRequest(frmWorkRequest);
    }
    WOC.ClearMdlWorkRequest = function (frmWorkRequest) {
        WOC.mdlWorkRequest.id = 0;
        WOC.mdlWorkRequest.workorderId = 0;
        WOC.mdlWorkRequest.description = '';
        WOC.mdlWorkRequest.poNumber = '';
        WOC.mdlWorkRequest.invoiceNumber = '';
        WOC.mdlWorkRequest.equipmentId = '';
        //WOC.mdlWorkRequest.wrType = 0;
        WOC.mdlWorkRequest.serviceCategory = '';
        WOC.mdlWorkRequest.vendor = '';
        WOC.mdlWorkRequest.expectedStartDate = new Date();
        WOC.mdlWorkRequest.expectedCompletionDate = new Date();
        $('#mdlAddWorkRequest').modal('hide');
        frmWorkRequest.$setPristine();
    }

    WOC.OrderBy = function (value, column) {
        if (value[0] == '-') {
            return column;
        }
        else {
            return '-' + column;
        }
    }

    //WOC.CancelWorkOrderAndRequest = function (woForm, frmWorkRequest) {
    //    WOC.ClearWorkOrderForm();
    //    woForm.$setPristine();
    //    frmWorkRequest.$setPristine();
    //}

    var counter = 0;
    $scope.documentSelected = function (element) {
        for (var i = 0; i < element.files.length; i++) {
            var index = counter;
            WOC.documentArray.push({ counter: counter++, showRemove: true, document: element.files[i], documentPath: '', documentName: element.files[i].name, documentDesc: '', documentTags: '', isNew: true, isDeleted: false });
            var reader = new FileReader();
            reader.onload = function () {
                // WOC.documentArray[index].document = reader.result;
            }
            reader.readAsDataURL(event.target.files[i]);
            $scope.$apply();
        }
    };

    WOC.removeDocument = function (index) {

        for (var i = 0; i < WOC.documentArray.length; i++) {
            if (WOC.documentArray[i].counter == index) {
                if (WOC.documentArray[i].isNew) {
                    WOC.documentArray.splice(i, 1);
                    counter = counter - 1;
                }
                else {
                    //mark it as deleted so that, services will delete the old documents.
                    WOC.documentArray[i].isDeleted = true;
                }
            }
        }
    }

    WOC.saveDocuments = function () {
        var fd = new FormData();
        for (var i = 0; i < WOC.documentArray.length; i++) {
            fd.append("file", WOC.documentArray[i].document);
            fd.append("filename", WOC.documentArray[i].documentName);
            fd.append("workorderId", $scope.pId);
            fd.append("moduleid", 'WORKORDERS');
            fd.append("fileType", 'document');
            fd.append("seqNo", WOC.documentArray[i].counter);
            fd.append("filedesc", WOC.documentArray[i].documentDesc);
            fd.append("filetags", WOC.documentArray[i].documentTags);
            fd.append("idDeleted", WOC.documentArray[i].isDeleted);
        }
        $scope.showLoader = true;
        workorderService.saveFiles(fd, '/api/workorderdocument', function (status, resp) {
            if (status) {
                swal("SUCCESS", "Documents saved successfully.", "success");
                $scope.showLoader = false;
            }
            else {
                if (status == 401) {
                    $scope.showLoader = false;
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", "Unable to add documents.", "error");
                }
                $scope.showLoader = false;
            }
        });
    }

    WOC.SaveWorkorder = function (woForm) {

        var MdlWorkOrderAndWorkRequests = {
            Workorder: {},
            lstWorkrequest: [],
            PMID: $rootScope.userData.ManagerDetail.pId
        };
        var lstWorkrequest = []
        var Workorder = {
            id: 0,
            propertyId: WOC.selectedProperty.propertyId,
            description: WOC.woDescription,
            requestedDate: WOC.woRequestedDate,
            requestedBy: WOC.RequestedBy.id,
            creatorUserId: $rootScope.userData.personId,
            requestedByPersonType: WOC.RequestedBy.personType,
        }

        for (var i = 0; i <= WOC.wrArray.length - 1; i++) {

            var mdlWorkRequest = {
                id: 0,
                workorderId: 0,
                description: WOC.wrArray[i].description,
                poNumber: WOC.wrArray[i].poNumber,
                invoiceNumber: WOC.wrArray[i].invoiceNumber,
                //equipmentId: WOC.wrArray[i].equipmentId,
                // wrType: WOC.wrArray[i].wrType.id,
                serviceCategoryId: WOC.wrArray[i].serviceCategory.id,
                vendorId: WOC.wrArray[i].vendor.vendorId,
                expectedStartDate: WOC.wrArray[i].expectedStartDate,
                expectedCompletionDate: WOC.wrArray[i].expectedCompletionDate,
            }
            lstWorkrequest.push(mdlWorkRequest);
        }
        MdlWorkOrderAndWorkRequests.Workorder = Workorder;
        MdlWorkOrderAndWorkRequests.lstWorkrequest = lstWorkrequest;
        $scope.showLoader = true;
        workorderService.saveWorkorder(MdlWorkOrderAndWorkRequests).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    swal("SUCCESS", "Data saved successfully.", "success");
                    WOC.ClearWorkOrderForm();
                    woForm.$setPristine();
                    woForm.$setUntouched();
                    $scope.pId = response.Message;

                    if (WOC.documentArray.length > 0) {
                        WOC.saveDocuments();
                    }
                    CallHubCheckNotificationMethod();
                    $location.path("/workorders");
                    // WOC.GetNotification();

                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    WOC.UpdateWorkOrder = function (frmWorkRequest) {

        var MdlWorkOrderAndWorkRequests = {
            Workorder: {},
            lstWorkrequest: []
        };
        var lstWorkrequest = []
        var Workorder = {
            id: WOC.WOID,
            propertyId: WOC.selectedProperty.propertyId,
            description: WOC.woDescription,
            requestedDate: WOC.woRequestedDate,
            requestedBy: WOC.RequestedBy.id,
            creatorUserId: $rootScope.userData.personId,
        }

        for (var i = 0; i <= WOC.wrArray.length - 1; i++) {

            var mdlWorkRequest = {
                id: WOC.wrArray[i].id,
                workorderId: WOC.WOID,
                description: WOC.wrArray[i].description,
                poNumber: WOC.wrArray[i].poNumber,
                invoiceNumber: WOC.wrArray[i].invoiceNumber,
                equipmentId: WOC.wrArray[i].equipmentId,
                //wrType: WOC.wrArray[i].wrType,
                serviceCategoryId: WOC.wrArray[i].serviceCategory.id,
                vendorId: WOC.wrArray[i].vendor.vendorId,
                expectedStartDate: OC.wrArray[i].expectedStartDate,
                expectedCompletionDate: OC.wrArray[i].expectedCompletionDate,
            }
            lstWorkrequest.push(mdlWorkRequest);
        }
        MdlWorkOrderAndWorkRequests.Workorder = Workorder;
        MdlWorkOrderAndWorkRequests.lstWorkrequest = lstWorkrequest;
        $scope.showLoader = true;
        workorderService.updateWorkOrder(MdlWorkOrderAndWorkRequests).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    swal("SUCCESS", "Data updated successfully.", "success");
                    // WOC.ClearWorkOrderForm();
                    WOC.ClearMdlWorkRequest(frmWorkRequest);
                    frmWorkRequest.$setPristine();
                    frmWorkRequest.$setUntouched();
                    // $location.path("/workorders");                   
                    $window.location.reload();
                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });

    }

    WOC.DeleteWorkOrder = function (workorderId) {
        $scope.showLoader = true;
        var mdlWorkorderIdToDelete = {
            workorderId: workorderId,
            deleteruserId: $rootScope.userData.personId
        };
        workorderService.deleteWorkOrder(mdlWorkorderIdToDelete).
            success(function (response) {
                if (response.Status == true) {
                    swal("SUCCESS", "Work order deleted successfully.", "success");
                    WOC.Page = 1;
                    WOC.GetWorkOrderByStatus(WOC.Page, function (data) {

                        WOC.lstWorkOrder = data.lstMdlWorkOrderByStatus;
                        WOC.lstWorkOrder_Filtered = WOC.lstWorkOrder;
                        WOC.TotalWorkOrderCount = data.TotalWorkOrderCount;
                        WOC.PageRange = serverSidePagingService.GetPageRangeValue(WOC.lstWorkOrder.length, WOC.Page);
                        WOC.CurrentSelectedPage = WOC.PageRange[0];
                        WOC.Page = WOC.Page + 3;
                    });
                }
                else {
                    swal("ERROR", response.Message, "error");
                }

            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
        // WOC.addWR(this);
    }

    WOC.DeleteWorkRequest = function (workRequestId) {
        $scope.showLoader = true;
        var MdlDeleteWorkRequest = {
            workRequestId: workRequestId,
            deleteruserId: $rootScope.userData.personId
        };
        workorderService.deleteWorkRequest(MdlDeleteWorkRequest).
            success(function (response) {
                if (response.Status == true) {
                    swal("SUCCESS", "Work request deleted successfully.", "success");
                    $location.path("/workorders");
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }


    WOC.AddUpdateWorkRequests = function (frmWorkRequest) {
        var vendorId = 0;
        if (WOC.mdlWorkRequest.vendor != undefined) {
            vendorId = WOC.mdlWorkRequest.vendor.vendorId;
        };
        var workRequestStatus = WOC.mdlWorkRequestStatus.filter(function (s) { return s.name == WOC.mdlWorkRequest.wrStatus })[0];
        var IntStatus = 0;
        if (workRequestStatus != undefined) {
            IntStatus = workRequestStatus.id;
        }
        var mdlWorkRequest = {

            id: WOC.mdlWorkRequest.id,
            workorderId: $routeParams.workOrderId,
            description: WOC.mdlWorkRequest.description,
            poNumber: WOC.mdlWorkRequest.poNumber,
            invoiceNumber: WOC.mdlWorkRequest.invoiceNumber,
            //wrType: WOC.mdlWorkRequest.wrType.id,
            serviceCategoryId: WOC.mdlWorkRequest.serviceCategory.id,
            vendorId: vendorId,
            creatorUserId: $rootScope.userData.personId,
            status: IntStatus,
            wrStatus: IntStatus,
            //status: WOC.mdlWorkRequestStatus.filter(s => s.name == WOC.mdlWorkRequest.status)[0].id,    // WOC.mdlWorkRequest.wrStatus.id,
            //wrStatus: WOC.mdlWorkRequestStatus.filter(s => s.name == WOC.mdlWorkRequest.status)[0].id,  //WOC.mdlWorkRequest.wrStatus.id,
            expectedStartDate: WOC.mdlWorkRequest.expectedStartDate,
            expectedCompletionDate: WOC.mdlWorkRequest.expectedCompletionDate.toGMTString(),
        }
        WOC.mdlWorkRequest.expectedCompletionDate = WOC.mdlWorkRequest.expectedCompletionDate.toGMTString();
        WOC.mdlWorkRequest = angular.copy(mdlWorkRequest);
        $scope.showLoader = true;
        workorderService.addUpdateWorkRequests(WOC.mdlWorkRequest).
            success(function (response) {
                $scope.showLoader = false;
                if (response.Status == true) {
                    swal("SUCCESS", "Work Order updated successfully.", "success");

                    //  console.log('WOC.mdlWorkRequestStatus', WOC.mdlWorkRequestStatus);

                    WOC.wrArray = [];
                    var lstworkrequest = response.Data;
                    angular.forEach(lstworkrequest, function (value, key) {
                        var arrworkOrdr = angular.copy(WOC.mdlWorkRequest);
                        arrworkOrdr.id = value.id;
                        arrworkOrdr.workorderId = value.workorderId;
                        arrworkOrdr.description = value.description;
                        arrworkOrdr.poNumber = value.poNumber;
                        arrworkOrdr.invoiceNumber = value.invoiceNumber;
                        arrworkOrdr.equipmentId = value.equipmentId;
                        arrworkOrdr.status = value.status;
                        arrworkOrdr.expectedStartDate = value.expectedStartDate;
                        arrworkOrdr.expectedCompletionDate = value.expectedCompletionDate;

                        var _status = WOC.mdlWorkRequestStatus.filter(function (s) { return s.id == value.status });
                        if (_status.length > 0) {
                            arrworkOrdr.strStatus = _status[0].name;
                        }

                        // arrworkOrdr.strStatus = WOC.mdlWorkRequestStatus.filter(s => s.id == value.status)[0].name;
                        // arrworkOrdr.wrType = WOC.wrType.filter(wr =>wr.id == value.wrType)[0];;
                        var mdl = {
                            id: value.serviceCategoryId,
                            name: value.serviceCategoryName
                        };
                        arrworkOrdr.serviceCategory = mdl;
                        arrworkOrdr.vendor = WOC.lstVendor.filter(function (v) { return v.vendorId == value.vendorId })[0];
                        WOC.wrArray.push(arrworkOrdr);
                    });

                    WOC.ClearMdlWorkRequest(frmWorkRequest);
                    frmWorkRequest.$setPristine();
                    frmWorkRequest.$setUntouched();
                    // WOC.GetNotification();
                    CallHubCheckNotificationMethod();

                    // $window.location.reload();


                    //var workRequestStatus = WOC.mdlWorkRequestStatus.filter(s => s.id == WOC.mdlWorkRequest.status)[0];
                    //var WorkRequestIndex = WOC.wrArray.findIndex((obj => obj.id == WOC.mdlWorkRequest.id));
                    //WOC.wrArray[WorkRequestIndex].description = WOC.mdlWorkRequest.description;
                    //WOC.wrArray[WorkRequestIndex].expectedCompletionDate = WOC.mdlWorkRequest.expectedCompletionDate;
                    //WOC.wrArray[WorkRequestIndex].expectedStartDate = WOC.mdlWorkRequest.expectedStartDate;
                    //WOC.wrArray[WorkRequestIndex].invoiceNumber = WOC.mdlWorkRequest.invoiceNumber;
                    //WOC.wrArray[WorkRequestIndex].poNumber = WOC.mdlWorkRequest.poNumber;
                    //WOC.wrArray[WorkRequestIndex].status = WOC.mdlWorkRequest.status;
                    //if (workRequestStatus != undefined)
                    //{
                    //    WOC.wrArray[WorkRequestIndex].strStatus = workRequestStatus.name;
                    //}
                    //var vendorMdl = WOC.lstVendor.filter(v => v.vendorId == WOC.mdlWorkRequest.vendorId)[0];
                    //if (vendorMdl != undefined) {
                    //    WOC.wrArray[WorkRequestIndex].vendor = angular.copy(vendorMdl);
                    //}


                }
                else {
                    swal("ERROR", MessageService.responseError, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    WOC.AddNewWorkRequers = function () {
        WOC.UpdateIndex = false;
        WOC.IndexValue = '';

        WOC.mdlWorkRequest.id = 0;
        WOC.mdlWorkRequest.workorderId = 0;
        WOC.mdlWorkRequest.description = '';
        WOC.mdlWorkRequest.poNumber = '';
        WOC.mdlWorkRequest.invoiceNumber = '';
        WOC.mdlWorkRequest.equipmentId = '';
        //WOC.mdlWorkRequest.wrType = 0;
        WOC.mdlWorkRequest.serviceCategory = '';
        WOC.mdlWorkRequest.vendor = '';
        WOC.mdlWorkRequest.expectedStartDate = new Date();
        WOC.mdlWorkRequest.expectedCompletionDate = new Date();

        if (WOC.WorkOrderEntryMode == 'Edit') {
            WOC.WorkRequestEntrymode = 'Save';
        }
        else {
            WOC.WorkRequestEntrymode = 'Update';
        }
    }

    WOC.GetPropertyOwnerTenantManagerByPropertyId = function (propertyId) {
        WOC.GetPropertyOwnerTenantManager(propertyId, function (data) { });
    }

    WOC.GetPropertyOwnerTenantManager = function (propertyId, callback) {

        $scope.showLoader = true;
        workorderService.getPropertyOwnerTenantManager(propertyId).
            success(function (response) {
                if (response.Status == true) {
                    WOC.lstRequestedBy = response.Data;

                    callback(response.Data);
                }
                else {
                    swal("ERROR", response.Message, "error");
                }
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }

    WOC.GetWorkorderDocuments = function (workorderId) {
        $scope.showLoader = true;
        workorderService.getWorkorderDocuments(workorderId, function (result, data) {
            if (result) {
                counter = 0;
                angular.forEach(data, function (value, key) {
                    WOC.documentArray.push({
                        counter: counter, showRemove: true, document: value.Name, documentPath: value.FileSavePath, documentCreationDate: value.CreationTime,
                        documentName: value.Name, documentDesc: value.Description, documentTags: value.Tags, isNew: false, isDeleted: false
                    });
                    counter++;
                });
            }
            else {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }
        });
    }


    WOC.GetWorkOrderStatus = function (callback) {
        $scope.showLoader = true;
        workorderService.getWorkOrderStatus().
            success(function (data) {
                $scope.showLoader = false;
                WOC.mdlWorkRequestStatus = data.Data;
                callback(data.Data);
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;

            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    WOC.ChangeWorkOrderStatus = function (workorderId, status) {
        $scope.showLoader = true;
        var MdlWorkOrderStatus = {
            workorderId: workorderId,
            status: status,
            userId: WOC.managerid
        };
        workorderService.changeWorkOrderStatus(MdlWorkOrderStatus).
            success(function (data) {
                $scope.showLoader = false;
                if (data.Status == true) {
                    swal("SUCCESS", "Data updated successfully.", "success");
                } else {
                    swal("ERROR", "Error occured while updating the data !", "error");
                }
                WOC.StatusChange();
                CallHubCheckNotificationMethod();
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    WOC.ViewWorkOrderDoc = function (workOrderId, docName) {
        WOC.DocArray = [];
        WOC.DocArray = docName.split(',');
    };

    WOC.openDocument = function (url) {
        window.open("/Uploads/Workorder/Documents/" + url, '_blank', 'height=800,width=600');
    }

    WOC.CloseModal = function () {
        $('#mdlAddWorkRequest').modal('hide');


        var mdl = {
            AddNewVendorFromWorkOrder: true,
            returnUrl: ''
        };

        if (!angular.isUndefined($routeParams.workOrderId)) {
            mdl.returnUrl = 'createworkorder/' + $routeParams.workOrderId;
        }
        else {
            mdl.returnUrl = 'createworkorder';
        }


        $window.localStorage.setItem("AddNewVendorFromWorkOrder", JSON.stringify(mdl));
    }


    WOC.PrintPdf = function (divname) {
        WOC.HTMLArr = [];
        var gridHtml = $("#" + divname).html();
        WOC.HTMLArr.push(gridHtml);
        invoiceService.Printpdf(WOC.HTMLArr, '', 'WorkOrders').
            success(function (data) {

                var pdfFile = new Blob([data], { type: 'application/pdf' });
                //var pdfFileUrl = URL.createObjectURL(pdfFile);
                //window.open(pdfFileUrl);

                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(pdfFile, 'WorkOrders.pdf');
                } else {
                    var objectUrl = URL.createObjectURL(pdfFile);
                    window.open(objectUrl);
                }

            }).error(function (data) {
                $scope.showLoader = false;
                swal("ERROR", "", "error");
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    }


    WOC.DisableInput_DTP = function () {
        $(".md-datepicker-input").prop('disabled', true);
    }

    WOC.ChangeCompletionDate = function () {
        WOC.mdlWorkRequest.expectedCompletionDate = WOC.mdlWorkRequest.expectedStartDate;
    }

    WOC.ChangeWorkRequestStatus = function (WorkRequestStatus, WorkRequestId) {

        $scope.showLoader = true;
        var mdlUpdateWorkRequest = {

            WorkRequestId: WorkRequestId,
            Status: WorkRequestStatus,
            UserId: WOC.managerid
        };

        workorderService.updateWorkRequestStatus(mdlUpdateWorkRequest).
            success(function (data) {
                $scope.showLoader = false;
                if (data.Status == true) {
                    swal("SUCCESS", "Data updated successfully.", "success");
                    // WOC.GetNotification();
                    CallHubCheckNotificationMethod();
                } else {
                    swal("ERROR", "Error occured while updating the data !", "error");
                }
                WOC.LoadCreateWorkOrder();
            }).error(function (data, status) {
                if (status == 401) {
                    commonService.SessionOut();
                }
                else {
                    swal("ERROR", MessageService.serverGiveError, "error");
                }
                $scope.showLoader = false;
            }).finally(function (data) {
                $scope.showLoader = false;
            });
    };

    WOC.GetNotification = function () {
        $rootScope.$emit("CallGetNotification", {});
    }


});