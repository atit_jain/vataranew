﻿
var form = "";
//create pdf
function createPDF(id, pdfName) {

    var ele = $(id + " .modal-dialog .modal-content");

    getCanvas(id).then(function (canvas) {
        var img = canvas.toDataURL("image/png");
        var doc = '';
        //doc.addImage(img, 'JPEG', 20, 20);
        //a4 mm (210,297), 595 pt x 842 p
        // doc.addImage(img, 'JPEG', -100, 10, 420, 160);
        if ($(id + " .modal-dialog .modal-content").width() < 600) {
            doc = new jsPDF('p', 'mm', 'a4');
            doc.addImage(img, 'JPEG', 20, 10);
        }
        else {
            doc = new jsPDF('p', 'mm', 'a3');
            doc.addImage(img, 'JPEG', 20, 20);
        }
        doc.save(pdfName);
    });
}

// create canvas object
function getCanvas(id) {

    form = $(id + " .modal-dialog .modal-content");
    var width = $(id).width();
    var height = $(id).height();

    // , cache_width = form.width(), a4 = [595.28, 841.89]; // for a4 size paper width and height
    // form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
    return html2canvas(form, {
        imageTimeout: 2000,
        removeContainer: true
    });

}



function createPDF1(id, pdfName) {
    var ele = $(id);

    getCanvas1(id).then(function (canvas) {
        var img = canvas.toDataURL("image/png");
        var doc = '';
        doc = new jsPDF('p', 'mm', 'a2');
        doc.addImage(img, 'JPEG', 20, 10);
        doc.save(pdfName);
    });
}

function getCanvas1(id) {
    form = $(id);
    var width = $(id).width();
    var height = $(id).height();
    return html2canvas(form, {
        imageTimeout: 2000,
        removeContainer: true
    });
}


//////////////////////////////////////////////////////////////////////////


function ExportToExcel(divId, fileName) {
    //var IdArr = [];
    //var NameArr = [];



    var isIE = (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0);
    if (isIE) {
        // IE > 10
        if (typeof Blob != 'undefined') {
            var fileData = new Blob([document.getElementById(divId).innerHTML.replace(/="  "/gi, "")], { type: 'application/vnd.ms-excel,' });
            window.navigator.msSaveBlob(fileData, fileName + '.xls');
        }
            // IE < 10
        else {
            myFrame.document.open("text/html", "replace");
            myFrame.document.write(document.getElementById(divId).innerHTML.replace(/="  "/gi, ""));
            myFrame.document.close();
            myFrame.focus();
            myFrame.document.execCommand('SaveAs', true, fileName + '.xls');
        }
    }
        // crome,mozilla
    else {

        var inner_html = document.getElementById(divId).innerHTML;

        exportToExcel(inner_html, fileName);

        //var uri = 'data:application/vnd.ms-excel,' + document.getElementById(divId).innerHTML.replace(/ /g, '%20');
        //var link = document.createElement("a");
        //link.href = uri;
        //link.style = "visibility:hidden";
        //link.download = fileName + ".xls";
        //document.body.appendChild(link);
        //link.click();
        //document.body.removeChild(link);       
    }
}

function exportToExcel(htmls, fileName) {
   
    var uri = 'data:application/vnd.ms-excel;base64,';

    //***********if you need lined excel sheet then uncomment the second line below and comment the first line ***********
    //********************************************************************************************************************
    var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head></head><body><table>{table}</table></body></html>';
    //var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };
    var format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    };  
    var ctx = {
        worksheet: 'Worksheet',
        table: htmls
    }
    var link = document.createElement("a");
    link.style = "visibility:hidden";
    link.download = fileName + ".xls";   
    link.href = uri + base64(format(template, ctx));
    link.click();
}



//var tablesToExcel = (function () {
//    var uri = 'data:application/vnd.ms-excel;base64,'
//    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets>'
//    , templateend = '</x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head>'
//    , body = '<body>'
//    , tablevar = '<table>{table'
//    , tablevarend = '}</table>'
//    , bodyend = '</body></html>'
//    , worksheet = '<x:ExcelWorksheet><x:Name>'
//    , worksheetend = '</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet>'
//    , worksheetvar = '{worksheet'
//    , worksheetvarend = '}'
//    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
//    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
//    , wstemplate = ''
//    , tabletemplate = '';

//    return function (table, name, filename) {
//        var tables = table;

//        console.log('tables', tables);

//        for (var i = 0; i < tables.length; ++i) {
//            wstemplate += worksheet + worksheetvar + i + worksheetvarend + worksheetend;
//            tabletemplate += tablevar + i + tablevarend;
//        }

//        var allTemplate = template + wstemplate + templateend;
//        var allWorksheet = body + tabletemplate + bodyend;
//        var allOfIt = allTemplate + allWorksheet;

//        var ctx = {};
//        for (var j = 0; j < tables.length; ++j) {
//            ctx['worksheet' + j] = name[j];
//        }

//        console.log('ctx', ctx);

//        for (var k = 0; k < tables.length; ++k) {
//            var exceltable;
//            console.log('tables[k]', tables[k]);

//            if (!tables[k].nodeType) exceltable = document.getElementById(tables[k]);
//            ctx['table' + k] = exceltable.innerHTML;

//            console.log('ctx[table+ k]', ctx['table' + k]);
//        }

//        console.log('ctx2', ctx);
//        window.location.href = uri + base64(format(allOfIt, ctx));

//    }
//})();






