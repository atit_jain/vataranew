﻿
vataraApp.filter('offset', function () {

    return function (input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});




vataraApp.service("pagingService", function ($rootScope) {

    this.itemsPerPageArr = [5, 10, 15, 20, 25, 30, 50];
 
    var rangeSize_Count = 5;
    this.Range = function (modelLength, currentPage, _itemsPerPage) {

        var rangeSize = rangeSize_Count;
        var ret = [];
        var start = 0;

        start = currentPage;
        if (this.pageCount(modelLength, _itemsPerPage) < rangeSize) {
            rangeSize = this.pageCount(modelLength, _itemsPerPage) + 1;
            start = 0;
        }
        else {
            if (start > this.pageCount(modelLength, _itemsPerPage) - rangeSize) {
                start = this.pageCount(modelLength, _itemsPerPage) - rangeSize + 1;
            }
        }
        for (var i = start; i < start + rangeSize; i++) {
            ret.push(i);
        }
        return ret;
    };

    this.PrevPage = function (currentPage) {
        if (currentPage > 0) {
            currentPage--;
        }
        return currentPage;
    };

    this.PrevPageDisabled = function (currentPage) {

        var result = currentPage === 0 ? "disabled" : "";
        return result;
    };

    this.pageCount = function (modelLength, itemsPerPage) {
        return Math.ceil(modelLength / itemsPerPage) - 1;
    };

    this.NextPage = function (modelLength, currentPage, _itemsPerPage) {
        if (this.pageCount(modelLength, _itemsPerPage) < rangeSize_Count) {
        }
        else {
            if (currentPage < this.pageCount(modelLength, _itemsPerPage)) {
                currentPage++;
            }
        }
        return currentPage;
    };

    this.NextPageDisabled = function (modelLength, currentPage, _itemsPerPage) {

        if (this.pageCount(modelLength, _itemsPerPage) < rangeSize_Count) {
            return "disabled";
        }
        else {
            return currentPage === this.pageCount(modelLength, _itemsPerPage) ? "disabled" : "";
        }
    };
})

//vataraApp.service("pagingService", function ($rootScope) {
//    this.itemsPerPageArr = [5, 10, 15, 20, 25, 30,50];
//    //this.itemsPerPage = 5;
//    this.itemsPerPage = $rootScope.itemsPerPage;
//    var rangeSize_Count = 5;
//    this.Range = function (modelLength, currentPage) {       
//        console.log('modelLength', modelLength);
//        console.log('currentPage', currentPage);      
//        var rangeSize = rangeSize_Count;
//        var ret = [];
//        var start = 0;
//        start = currentPage;
//        if (this.pageCount(modelLength) < rangeSize) {
//            rangeSize = this.pageCount(modelLength) + 1;
//            start = 0;
//        }
//        else {
//            if (start > this.pageCount(modelLength) - rangeSize) {
//                start = this.pageCount(modelLength) - rangeSize + 1;
//            }
//        }
//        for (var i = start; i < start + rangeSize; i++) {
//            ret.push(i);
//        }
//        console.log('Range', ret);
//        return ret;      
//    };

//    this.PrevPage = function (currentPage) {
//        if (currentPage > 0) {
//            currentPage--;
//        }
//        return currentPage;
//    };

//    this.PrevPageDisabled = function (currentPage) {
//        var result = currentPage === 0 ? "disabled" : "";
//        return result;
//    };

//    this.pageCount = function (modelLength) {
//        console.log('modelLength', modelLength);
//        console.log('this.itemsPerPage', this.itemsPerPage);
//        console.log('Math.ceil(modelLength / this.itemsPerPage)', Math.ceil(modelLength / this.itemsPerPage));
//        return Math.ceil(modelLength / this.itemsPerPage) - 1;
//    };

//    this.NextPage = function (modelLength, currentPage) {
//        if (this.pageCount(modelLength) < rangeSize_Count) {
//        }
//        else {
//            if (currentPage < this.pageCount(modelLength)) {
//                currentPage++;
//            }
//        }
//        return currentPage;
//    };

//    this.NextPageDisabled = function (modelLength, currentPage) {
//        if (this.pageCount(modelLength) < rangeSize_Count) {
//            return "disabled";
//        }
//        else {
//            return currentPage === this.pageCount(modelLength) ? "disabled" : "";
//        }
//    };
//})


vataraApp.service("serverSidePagingService", function ($rootScope) {

    this.itemsPerPage = 10;

    this.itemsPerPageArr = [5, 10, 15, 20, 25, 30, 50];

    var rangeSize_Count = 3;//5

    this.GetPageRangeValue = function (modelLength, startIndex) {

        var ret = [];
        var totalpage = modelLength / this.itemsPerPage;

        var start = startIndex;
        for (var i = 0; i < totalpage; i++) {
            ret.push(start);
            start = start + 1;
        }
        // console.log('ret new', ret);
        return ret;
    };


    this.Range = function (modelLength, currentPage) {
        var rangeSize = rangeSize_Count;
        var ret = [];
        var start = 0;

        start = currentPage;
        if (this.pageCount(modelLength) < rangeSize) {
            rangeSize = this.pageCount(modelLength) + 1;
            start = 0;
        }
        else {
            if (start > this.pageCount(modelLength) - rangeSize) {
                start = this.pageCount(modelLength) - rangeSize + 1;
            }
        }
        for (var i = start; i < start + rangeSize; i++) {

            ret.push(i);
        }
       // console.log('ret', ret);
        return ret;
    };

    this.PrevPage = function (currentPage) {
        if (currentPage > 0) {
            currentPage--;
        }
        return currentPage;
    };

    this.PrevPageDisabled = function (currentPage) {

        var result = currentPage <= 1 ? "disabled" : "";
        return result;
    };

    this.pageCount = function (modelLength) {        
        return Math.ceil(modelLength / this.itemsPerPage);
    };

    this.NextPage = function (modelLength, currentPage) {

        if (this.pageCount(modelLength) < currentPage) {
        }
        else {
            if (currentPage < this.pageCount(modelLength)) {
                currentPage++;
            }
        }
        return currentPage;
    };

    this.NextPageDisabled = function (modelLength, currentPage) {
        if (this.pageCount(modelLength) < rangeSize_Count) {

            return "disabled";
        }
        else {
            return currentPage === this.pageCount(modelLength) ? "disabled" : "";
        }
    };
})



