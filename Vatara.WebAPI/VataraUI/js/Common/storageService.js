﻿vataraApp.factory('storageService', ['$rootScope', '$window', function ($rootScope, $window) {

    return {
        get: function (key) {
            // return sessionStorage.getItem(key);
            return JSON.parse($window.localStorage.getItem(key));

        },
        save: function (key, data) {
            $window.localStorage.setItem(key, JSON.stringify(data));
           // sessionStorage.setItem(key, data);
        },

        remove: function (key) {
            $window.localStorage.removeItem(key);
        }
    };
}]);