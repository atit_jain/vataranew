﻿vataraApp.service("commonService", function ($window, $location, $rootScope, $http) {
    this.SessionOut = function () {
        $rootScope.userData = null;
        $rootScope.currentUserData = null;
      
        $window.localStorage.removeItem("loginUser");
        $window.localStorage.removeItem("requestedPage");
        $window.localStorage.removeItem("currentUserData");
        $rootScope.PaymentAgainstInvoice = false;       
        $window.localStorage.removeItem("PaymentAgainstInvoice");
        $window.localStorage.removeItem("InvoicePaymentDetails");       
        $window.localStorage.removeItem("paymentResult");
        $window.localStorage.removeItem("ShowPropayMsg");
        $window.localStorage.removeItem("GetErrorPropayMsg");
        $window.localStorage.removeItem("MdlPayment");
        $window.localStorage.removeItem("AddNewVendorFromWorkOrder");
        $window.localStorage.removeItem("requestedChatPage");

        var stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("localhost"));
        var arr2 = stringPath.split('/');
        var comProfile = (arr2[(arr2.length - 1)]).replace('.', '');
        if (comProfile == '' || comProfile == undefined) {
            $location.path("/login");
        }
        else {
            $location.path("/homepage");
        }
    };

    this.RemovePaymentData = function () {

        $rootScope.PaymentAgainstInvoice = false;
        $window.localStorage.removeItem("PaymentAgainstInvoice");
        $window.localStorage.removeItem("InvoicePaymentDetails");
        $window.localStorage.removeItem("paymentResult");
        $window.localStorage.removeItem("ShowPropayMsg");
        $window.localStorage.removeItem("GetErrorPropayMsg");
        $window.localStorage.removeItem("MdlPayment");
    };




    this.getCountries = function () {

        var responce = $http({
            method: "Get",
            url: "/api/Country/Get",
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getStates = function () {
        var responce = $http({
            method: "Get",
            url: "/api/State/Get",
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getCountyByState = function (stateId) {
        var responce = $http({
            method: "Get",
            url: "/api/County/GetCountyByState",
            params: {
                stateId: stateId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

    this.getCityByStateOrCounty = function (stateId, countyId) {
        var responce = $http({
            method: "Get",
            url: "/api/City/GetCityByStateOrCounty",
            params: {
                StateId: stateId,
                CountyId: countyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }

});