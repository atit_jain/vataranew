/// <reference path="D:\AST-WORK\Vatara_Project\vatara\Vatara.WebAPI\Views/_ViewStart.cshtml" />
/// <reference path="D:\AST-WORK\Vatara_Project\vatara\Vatara.WebAPI\Views/Shared/_Layout.cshtml" />
/// <reference path="D:\AST-WORK\Vatara_Project\vatara\Vatara.WebAPI\Views/Propay Payment/propayPayment.html" />
// create the module and name it vataraApp
var vataraApp = angular.module('Myapp', ['ngRoute', 'ngMaterial', 'ngMessages', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'ngMask', 'naif.base64']); //

vataraApp.directive("compareTo", function () {
    return {
        require: "ngModel",
        scope: {
            confirmPassword: "=compareTo"
        },
        link: function (scope, element, attributes, modelVal) {

            modelVal.$validators.compareTo = function (val) {
                return val == scope.confirmPassword;
            };
            scope.$watch("confirmPassword", function () {
                modelVal.$validate();
            });
        }
    };
});

vataraApp.filter('unique', function () {

    return function (arr, field) {
        var o = {}, i, l = arr.length, r = [];
        for (i = 0; i < l; i += 1) {
            o[arr[i][field]] = arr[i];
        }
        for (i in o) {
            r.push(o[i]);
        }
        return r;
    };
});

vataraApp.filter('makePositive', function () {
    return function (num) { return Math.abs(num); }
});




vataraApp.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    }
    return fallbackSrc;
});

vataraApp.directive('disallowSpaces', function () {
    return {
        restrict: 'A',

        link: function ($scope, $element) {
            $element.bind('input', function () {
                $(this).val($(this).val().replace(/ /g, ''));
            });
        }
    };
});

vataraApp.filter('groupBy', function () {
    var results = {};
    return function (data, key) {
        if (!(data && key)) return;
        var result;
        if (!this.$id) {
            result = {};
        } else {
            var scopeId = this.$id;
            if (!results[scopeId]) {
                results[scopeId] = {};
                this.$on("$destroy", function () {
                    delete results[scopeId];
                });
            }
            result = results[scopeId];
        }

        for (var groupKey in result)
            result[groupKey].splice(0, result[groupKey].length);

        for (var i = 0; i < data.length; i++) {
            if (!result[data[i][key]])
                result[data[i][key]] = [];
            result[data[i][key]].push(data[i]);
        }

        var keys = Object.keys(result);
        for (var k = 0; k < keys.length; k++) {
            if (result[keys[k]].length === 0)
                delete result[keys[k]];
        }
        return result;
    };
});



vataraApp.directive('ngModelOnblur', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1,
        link: function (scope, element, attrs, ngModelCtrl) {
            if (attrs.type === 'radio' || attrs.type === 'checkbox') { return; }
            var update = function () {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(element.val().trim());
                    ngModelCtrl.$render();
                });
            };
            element.off('input').off('keydown').off('change').on('focus', function () {
                scope.$apply(function () {
                    ngModelCtrl.$setPristine();
                });
            }).on('blur', update).on('keydown', function (e) {
                if (e.keyCode === 13) {
                    update();
                }
            });
        }
    };
});


vataraApp.directive('elemReady', function ($parse) {
    return {
        restrict: 'A',
        link: function ($scope, elem, attrs) {
            elem.ready(function () {
                $scope.$apply(function () {
                    var func = $parse(attrs.elemReady);
                    func($scope);
                })
            })
        }
    }
})

vataraApp.directive('onlyDigits', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                //  console.log('val', val);
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }
                }
                // console.log('decimalCheck', decimalCheck);
                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);

                    //console.log('decimalCheck', decimalCheck);
                    //console.log('decimalCheck[1]', decimalCheck[1]);

                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                    // console.log('clean', clean);
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


vataraApp.directive('onlyDigitsMaxHundread', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                // console.log('val', val);
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }
                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    if (decimalCheck[0] > 100) {
                        decimalCheck[0] = decimalCheck[0].slice(0, 2);
                    }
                    else {
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                }
                else {
                    if (decimalCheck[0] > 100) {
                        clean = decimalCheck[0].slice(0, 2);
                    }
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


vataraApp.directive('onlyDigitsMaxThirtyone', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                // console.log('val', val);
                if (angular.isUndefined(val)) {
                    var val = '';
                }

                var clean = val.replace(/[^0-9]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }
                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    if (decimalCheck[0] > 30) {
                        decimalCheck[0] = decimalCheck[0].slice(0, 1);
                    }
                    else {
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                }
                else {
                    if (decimalCheck[0] > 30) {
                        clean = decimalCheck[0].slice(0, 1);
                    }
                }
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


vataraApp.directive('ngConfirmClick', [
  function () {
      return {
          priority: -1,
          restrict: 'A',
          link: function (scope, element, attrs) {
              element.bind('click', function (e) {
                  var message = attrs.ngConfirmClick;
                  // confirm() requires jQuery
                  if (message && !confirm(message)) {
                      e.stopImmediatePropagation();
                      e.preventDefault();
                  }
              });
          }
      }
  }
]);

vataraApp.directive("preventTypingGreater", function () {
    return {
        link: function (scope, element, attributes) {
            var oldVal = null;
            element.on("keydown keyup", function (e) {
                if (Number(element.val()) > Number(attributes.max) &&
                      e.keyCode != 46 // delete
                      &&
                      e.keyCode != 8 // backspace
                    ) {
                    e.preventDefault();
                    element.val(oldVal);
                }
                else if (event.keyCode === 32) {
                    event.preventDefault();
                }
                else {
                    oldVal = Number(element.val());
                }
            });
        }
    };
});


vataraApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

vataraApp.directive('onlyAlphabets', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zA-Z\s]/g, '');  //"/^[a-zA-Z\s]*$/"
                //console.log(transformedInput);
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});


//directive for stop copy and paste on any element
//************************************************
vataraApp.directive('stopccp', function () {
    return {
        scope: {},
        link: function (scope, element) {
            element.on('cut copy paste', function (event) {
                event.preventDefault();
            })
        }
    };
});

//Below directive (withMaxLimit) prevents user to enter greater value 
//than the value of attribute maxLimit of any element
//*******************************************************
vataraApp.directive("withMaxLimit", function () {
    return {
        require: ["ngModel"],
        restrict: "A",
        link: function ($scope, $element, $attrs) {
            var modelName = $attrs.ngModel;
            //var maxLimit = parseInt($attrs.maxLimit, 10);
            $scope.$watch(modelName, function (newValue, oldValue) {
                var maxLimit = parseInt($attrs.maxLimit, 10);
                if (newValue && (maxLimit > 0) && (newValue > maxLimit)) {

                    swal("Alert !", "Incase GrossArea is other than 0, Rentable Area should be less than or equal to GrossArea.", "info");
                    $scope[modelName] = oldValue;
                }
            });
        }
    }
});

vataraApp.directive("checkEndDate", function () {
    return {
        require: ["ngModel"],
        restrict: "A",
        link: function ($scope, $element, $attrs) {
            var modelName = $attrs.ngModel;
            $scope.$watch(modelName, function (newValue, oldValue) {
                var isReminder = $attrs.isReminder;

                if (newValue < new Date() && (isReminder == 1 || isReminder == '1')) {

                    $scope[modelName] = new Date();

                }
            });
        }
    }
});

//vataraApp.directive('testDirective', function ($compile) {
//    return {       
//        restrict: 'EAC',        
//        template: " <div id='pop-over-link' class='cursorpointer'>" +
//            "<img src='images/notification1.png' style='height: 66px;' />"+
//            "<sub><span id='notificationCount' style='position: fixed;' class='badge totalUnreadMessage'></span></sub>"+
//            "</div> " +
//            "<div id='pop-over-content' style='display:none'>" +
//            "<div class='popup-content con'>"+
//                            "<div class='row marginnone notificatoinPopOverRow'>"+
//                                "<div class='col-md-12 col-lg-12 col-sm-12 col-xs-12 notification_col12' ng-repeat='notification in lstNotification'>"+
//                                    "<div class='col-md-2'>"+
//                                        "<img ng-if='notification.isRead == true' class='notificationImage' src='images/envelop_open.png' />"+
//                                        "<img ng-if='notification.isRead == false' class='notificationImage' src='images/close_envelope.png' />"+
//                                    "</div>"+
//                                    "<div class='col-md-10 notification-box notification-text cursorpointer' ng-click='IC.OpenPage(notification.module)'>"+
//                        "{{notification.message}}"+
//                        "</div>"+
//                    "</div>"+
//                    "</div>"+
//                    "<div class='popover-footer'>"+
//                        "<button type='button' id='btnClearNotification' ng-click='ClearNotification()' class='btn btn-default'>Clear All</button>"+
//                    "</div>"+
//                    "</div>" +
//                    "</div>",
//        replace: true,
//        scope: {          
//            lstNotification: "=",
//        },
//        link: function (scope, elements, attrs) {
//            $("#pop-over-link").popover({
//                'placement': 'bottom',
//                'trigger': 'click',
//                'html': true,
//                'container': 'body',
//                'content': function () {
//                    return $compile($("#pop-over-content").html())(scope);
//                }
//            });
//        }
//    }

//$('.popup-window').popover({
//    html: true,
//    title: '<button type="button" class="close" onclick="$(&quot;.popup-window&quot;).popover(&quot;hide&quot;);">&times;</button>',
//    trigger: 'manual',
//    content: function () {
//        return $(this).next('.popup-content').html();
//    }
//}).click(function (e) {
//    $(this).popover('toggle');
//    $('.popup-window').not(this).popover('hide');
//    e.stopPropagation();
//});
//$('body').on('click', function (e) {
//    $('.popup-window').each(function () {
//        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
//            $(this).popover('hide');
//        }
//    });
//});

//});

//vataraApp.directive('testDirective', function ($compile) {
//    return {
//        restrict: 'EAC',
//        template: "<a id='pop-over-link' style='position: fixed; top: 100px; left: 100px;'>Show pop-over</a>" +
//                  "<div id='pop-over-content' style='display:none'><button class='btn btn-primary' ng-click='ClearNotification()'>Ok</button></div>",
//        link: function (scope, elements, attrs) {
//            $("#pop-over-link").popover({
//                'placement': 'top',
//                'trigger': 'click',
//                'html': true,
//                'container': 'body',
//                'content': function () {
//                    return $compile($("#pop-over-content").html())(scope);
//                }
//            });
//            scope.testFunction = function () {
//                alert("it works");
//                console.log("maybe");
//            }
//        }
//    }
//});



vataraApp.factory('mainFactoryService', function () {
    var data = "";
    return {
        setData: function (str) {
            data = str;
        },

        getData: function () {
            return data;
        }
    }
})

// configure our routes
vataraApp.config(function ($routeProvider) {

   // console.log = () => { };        //for stopping all console.log to print

    $routeProvider

        // route for the home page
         .when('/login', {
             templateUrl: 'Views/Login/Login.html',
             scope: true,
             controller: 'loginController'
         })

        .when('/homepage', {
            templateUrl: 'CompanyHomePage.html',
            scope: true,
            controller: 'companyHomeController'
        })
        .when('/home', {
            templateUrl: 'home.html',
            scope: true,
            controller: 'homeController'
        })

    .when('/support', {
        templateUrl: 'CustomerSupport.html',
        scope: true,
        controller: 'SupportTicketController'
    })


        // route for the Financial Summary Page
        .when('/summary', {
            templateUrl: 'finanacialSummary.html',
            scope: true,
            controller: 'financialSummaryController'
        })
        .when('/Subscription', {
            templateUrl: 'Views/Subscription/Subscription.html',
            scope: true,
            controller: 'subscriptionController'
        })

         .when('/PropayProfile', {
             templateUrl: 'PropayUI/PropayRegistration.html',
             scope: true,
             controller: 'invoicesController'
         })

         .when('/liabilities', {
             templateUrl: 'Views/InvoicePayment/OutstandingPayment.html',
             scope: true,
             controller: 'invoicesController'
         })

        .when('/IncmExpnseRpt', {
            templateUrl: 'Views/Report/IncomeExpenseRpt.html',
            scope: true,
            controller: 'IncomeExpenseController'
        })
         .when('/IncmExpnseRpt/:OwnerId/:OwnerPOID/:PropertyId/:ownerIsCompany', {
             templateUrl: 'Views/Report/IncomeExpenseRpt.html',
             scope: true,
             controller: 'IncomeExpenseController'
         })
        .when('/TenantHome', {
            templateUrl: 'Views/Tenant/Home.html',
            scope: true,
            controller: 'tenantController'
        })

        .when('/OwnerHome', {
            templateUrl: 'Views/Owner/OwnerHome.html',
            scope: true,
            controller: 'ownerController'
        })

        .when('/AdminUsers', {
            templateUrl: 'Views/Users/AdminUsers.html',
            scope: true,
            controller: 'AddNewUserController'
        })

        .when('/LoggedIn', {
            templateUrl: 'Views/Login/LoggedIn.html',
            scope: true,
            controller: 'loginController'
        })

        .when('/OnlineStatement', {
            templateUrl: 'Views/Report/OnlineStatement.html',
            scope: true,
            controller: 'onlineStatementController'
        })

        // route for the Invoices
        .when('/invoices', {
            templateUrl: 'invoices.html',
            scope: true,
            controller: 'invoicesController'
        })

        // route for the Invoices
        .when('/createinvoices', {
            templateUrl: 'CreateInvoice.html',
            scope: true,
            controller: 'workorderController'
        })

         // route for the Bills
        .when('/payments', {
            templateUrl: 'bills.html',
            scope: true,
            controller: 'billsController'
        })

        // route for the properties page
        .when('/properties', {
            templateUrl: 'properties.html',
            scope: true,
            controller: 'propertiesController'
        })


         .when('/propertyProfile/:PropertyNum', {
             templateUrl: 'Views/Property/ViewProperty.html',
             scope: true,
             controller: 'propertiesController'
         })


        .when('/add_property/:PropertyNum', {
            templateUrl: 'add_property.html',
            scope: true,
            controller: 'propertiesController'
        })

        .when('/edit_property/:PropertyNum', {
            templateUrl: 'add_property.html',
            scope: true,
            controller: 'propertiesController'
        })

        .when('/leases', {
            templateUrl: 'leases.html',
            scope: true,
            controller: 'leaseController'
        })

        .when('/create_lease', {
            templateUrl: 'create_lease.html',
            scope: true,
            controller: 'leaseController'
        })
        .when('/create_lease/:PropertyNum', {
            templateUrl: 'create_lease.html',
            scope: true,
            controller: 'leaseController'
        })
        .when('/edit_lease/:LeaseNum', {
            templateUrl: 'create_lease.html',
            scope: true,
            controller: 'leaseController'
        })

        .when('/leaseview/:LeaseNum', {
            templateUrl: 'lease_docs.html',
            scope: true,
            controller: 'leaseController'
        })

         .when('/leaseview/:LeaseNum/:propertyId', {
             templateUrl: 'lease_docs.html',
             scope: true,
             controller: 'leaseController'
         })

        // route for the contact page
        .when('/person/:PersonType', {
            templateUrl: 'person.html',
            scope: true,
            controller: 'personController'
        })
        .when('/addnewcontact/:PersonType', {
            templateUrl: 'AddNewContact.html',
            controller: 'personController'
        })
        .when('/Ownerprofile/:PersonId', {
            templateUrl: 'ownerprofile.html',
            controller: 'personController'
        })
        .when('/tenantprofile/:PersonId', {
            templateUrl: 'tenantprofile.html',
            controller: 'personController'
        })
        .when('/vendorprofile/:PersonId', {
            templateUrl: 'vendorprofile.html',
            controller: 'personController'
        })
        .when('/associationsprofile/:PersonId', {
            templateUrl: 'associationsprofile.html',
            controller: 'personController'
        })
        // route for the properties page
        .when('/workorders', {
            templateUrl: 'Work_Orders.html',
            scope: true,
            controller: 'workorderController'
        })

        .when('/eventTracker', {
            templateUrl: 'Views/EventTracker/EventTracker.html',
            scope: true,
            controller: 'eventTrackerController'
        })
        .when('/createInvoice/:LeaseNum/:PropertyId/:TenantId', {
            templateUrl: 'invoices.html',
            scope: true,
            controller: 'invoiceController'
        })
        .when('/createworkorder', {
            templateUrl: 'createworkorder.html',
            scope: true,
            controller: 'workorderController'
        })
        .when('/createworkorder/:workOrderId', {
            templateUrl: 'CreateWorkOrder.html',
            scope: true,
            controller: 'workorderController'
        })

         .when('/property/createworkorder/:propertyId', {
             templateUrl: 'CreateWorkOrder.html',
             scope: true,
             controller: 'workorderController'
         })

         .when('/payment', {
             templateUrl: 'Views/Propay Payment/propayPayment.html',
             scope: true,
             controller: 'paymentController'
         })

        .when('/MobilePayment', {
            templateUrl: 'Views/Propay Payment/MobilePayment.html',
            scope: true,
            controller: 'MobilePaymentController'
        })

        .when('/MobilePayment/:PMID/:PersonId/:PersonTypeId', {
            templateUrl: 'Views/Propay Payment/MobilePayment.html',
            scope: true,
            controller: 'MobilePaymentController'
        })

        .when('/payment/:PropertyNum', {
            templateUrl: 'Views/Propay Payment/propayPayment.html',
            scope: true,
            controller: 'paymentController'
        })
        //.when('/payment/:Page', {
        //    templateUrl: 'Views/Propay Payment/propayPayment.html',
        //    scope: true,
        //    controller: 'paymentController'
        //})

        .when('/paymentDetail', {
            templateUrl: 'Views/Propay Payment/PaymentDetails.html',
            scope: true,
            controller: 'paymentController'
        })

        .when('/registration', {
            templateUrl: 'Views/Login/personRegistration.html',
            scope: true,
            controller: 'personRegistrationController'
        })

        .when('/EditProfile', {
            templateUrl: 'Views/Login/EditProfile.html',
            scope: true,
            controller: 'personRegistrationController'
        })

        .when('/CompanyProfile', {
            templateUrl: 'Views/CompanyProfile.html',
            scope: true,
            controller: 'CompanyController'
        })

         .when('/changePassword', {
             templateUrl: 'Views/Login/ChangePassword.html',
             scope: true,
             controller: 'changePasswordController'
         })

        .when('/changeEmail', {
            templateUrl: 'Views/Login/ChangeEmail.html',
            scope: true,
            controller: 'changePasswordController'
        })

        .when('/ManageOwnerTenant', {
            templateUrl: 'Views/Users/ManageOwner_Tenant.html',
            scope: true,
            controller: 'manageUsersController'
        })

        .when('/createvendor', {
            templateUrl: 'create_vendor.html',
            scope: true,
            controller: 'VendorController'
        })
        .when('/createvendor/:vendorId', {
            templateUrl: 'create_vendor.html',
            scope: true,
            controller: 'VendorController'
        })
        .when('/recurring', {
            templateUrl: 'RecurringBill.html',
            scope: true,
            controller: 'recurringBillController'
        })

        .when('/vendor', {
            templateUrl: 'Vendors.html',
            scope: true,
            controller: 'VendorController'
        })

        .when('/association', {
            templateUrl: 'Association.html',
            scope: true,
            controller: 'AssociationController'
        })

        .when('/createassociation', {
            templateUrl: 'Create_Association.html',
            scope: true,
            controller: 'AssociationController'
        })
        .when('/createassociation/:associationId', {
            templateUrl: 'Create_Association.html',
            scope: true,
            controller: 'AssociationController'
        })
         .when('/owners', {
             templateUrl: 'Owners.html',
             scope: true,
             controller: 'ownerContactController'
         })

        .when('/owners/:ownerId', {
            templateUrl: 'Views/Owner/ViewOwner.html',
            scope: true,
            controller: 'ownerContactController'
        })
         .when('/owners/:propertyId/:ownerId/:ownerPOID/:ownerIsCompany', {     //Use this for view owner profile from property(click owner Name from property profile)
             templateUrl: 'Views/Owner/ViewOwner.html',
             scope: true,
             controller: 'ownerContactController'
         })
         .when('/owners/:ownerId/:ownerPOID/:ownerIsCompany', {       // Use this for owner view in case of property manager(i.e. When PM opens the view owner page)
             templateUrl: 'Views/Owner/ViewOwner.html',
             scope: true,
             controller: 'ownerContactController'
         })

         .when('/propertyOwners/:leaseId/:propertyId/:ownerId/:ownerPOID/:ownerIsCompany', {
             templateUrl: 'Views/Owner/ViewOwner.html',
             scope: true,
             controller: 'ownerContactController'
         })

        .when('/tenants', {
            templateUrl: 'Tenants.html',
            scope: true,
            controller: 'tenantContactController'
        })
        .when('/tenants/:tenantId', {
            templateUrl: 'Views/Tenant/ViewTenant.html',
            scope: true,
            controller: 'tenantContactController'
        })
        .when('/leaseTenant/:leaseId/:propertyId/:tenantId', {
            templateUrl: 'Views/Tenant/ViewTenant.html',
            scope: true,
            controller: 'tenantContactController'
        })
        .when('/chat', {
            templateUrl: 'Chat.html',
            scope: true,
            controller: 'chatController'
        })

        .when('/chat/:user', {
            templateUrl: 'Chat.html',
            scope: true,
            controller: 'chatController'
        })
        .when('/SubscriptionPlan', {
            templateUrl: 'Views/Login/SubScriptionPlan.html',
            scope: true,
            controller: 'personRegistrationController'
        })
        .when('/SubscriptionPlan/:CustomerId/:PlanCode', {
            templateUrl: 'Views/Login/SubScriptionPlan.html',
            scope: true,
            controller: 'personRegistrationController'
        })
        .when('/SubscriptionPlan/:NewSubscription', {
            templateUrl: 'Views/Login/SubScriptionPlan.html',
            scope: true,
            controller: 'personRegistrationController'
        })
        .when('/SubscriptionHistory', {
            templateUrl: 'Views/Subscription/SubscriptionHistory.html',
            scope: true,
            controller: 'subscriptionController'
        })
    
        .when('/PaymentPage', {
            templateUrl: 'Views/Propay Payment/PaymentPage.html',
            scope: true,
            controller: 'MobilePaymentController'
        })
       .otherwise({ redirectTo: '/login' });
});


vataraApp.run(function ($rootScope, $window, $location) {

    $rootScope.emailAddessPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    $rootScope.ShowInvoiceBillModel = true;
    $rootScope.ActiveTabName = 'Transactions';
    //$rootScope.itemsPerPage = 5;

    var lastDigestRun = new Date();
    $rootScope.$watch(function detectIdle() {

        var now = new Date();
        if (now - lastDigestRun > 900000) {
            $window.localStorage.removeItem("loginUser");
            $rootScope.Islogin = true;
            $location.path("/login");
        }
        lastDigestRun = now;
    });
    if ($rootScope.checkLogin != true) {
        //var stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("localhost"));
        //var stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("vatara.biz"));

        var stringPath = '';
        if ($location.absUrl().indexOf("localhost") > -1) {
            stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("localhost"));
        }
        else {
            stringPath = $location.absUrl().substring(0, $location.absUrl().indexOf("vatara.biz"));
        }
        var arr2 = stringPath.split('/');
        var comProfile = (arr2[(arr2.length - 1)]).replace('.', '');
        if (comProfile == '' || comProfile == undefined) {
            comProfile = null
        }
        $rootScope.CompanyDomain = comProfile;
    }

    $rootScope.Islogin = false;
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        var userData = "";
        //$rootScope.PersonTyp declaration and assignment here
        $rootScope.PersonTyp = PersonTypeEnum;//PersonType;
        $rootScope.PersonRole = PersonRoleEnum;
        $rootScope.PersonTypeIdEnum = PersonTypeIdEnum;

        if ($window.localStorage.getItem("loginUser") != null) {
            userData = JSON.parse($window.localStorage.getItem("loginUser"));
        }
        if ($window.localStorage.getItem("currentUserData") != null) {
            $rootScope.currentUserData = JSON.parse($window.localStorage.getItem("currentUserData"));
        }
        
        if ($window.localStorage.getItem("requestedPage") == null) {
            $window.localStorage.setItem("requestedPage", JSON.stringify($location.path()));
        }
        if ($window.localStorage.getItem("InvoicePaymentDetails") != null) {
            $rootScope.InvoicePaymentDetails = JSON.parse($window.localStorage.getItem("InvoicePaymentDetails"));
        }
        if ((userData == null || userData == '') && $location.path() == "/registration" && $rootScope.CompanyDomain == undefined) {

            $rootScope.checkLogin = false;
            $rootScope.currentUserData = null;
            $rootScope.Islogin = true;
            $location.path("/registration");
        }
        else if ($location.path().split('?')[0] == "/MobilePayment") {

            $rootScope.checkLogin = false;
            $rootScope.currentUserData = null;
            $rootScope.Islogin = true;
            $location.path("/MobilePayment");
        }
        else if ($location.path() == "/PaymentPage") {
            $rootScope.checkLogin = false;
            $rootScope.currentUserData = null;
            $rootScope.Islogin = true;
            $location.path("/PaymentPage");
        }
        else if ((userData == null || userData == '') && $rootScope.CompanyDomain == undefined) {
            $rootScope.checkLogin = false;
            $rootScope.currentUserData = null;
            $rootScope.Islogin = true;
            $location.path("/login");
        }
        else if ((userData == null || userData == '' || userData.personType == undefined) && $rootScope.CompanyDomain != undefined) {

            $rootScope.checkLogin = false;
            $location.path("/homepage");
        }
        else {
            if (userData.personType == PersonTypeEnum.tenant) {
                var requestedChatPage = '';
                if ($window.localStorage.getItem("requestedChatPage") != null) {
                    var requestedChatPage = JSON.parse($window.localStorage.getItem("requestedChatPage"));
                }
                if ($location.path() == "/LoggedIn" || $location.path() == "/paymentDetail" || $location.path() == "/PropayProfile" || $location.path() == "/payment" || $location.path() == "/OwnerHome" || $location.path() == "/home" || $location.path() == "/chat" || $location.path() == "/chat/*") {
                }
                else if (requestedChatPage != '' && requestedChatPage != undefined && requestedChatPage == $location.path() && $location.path().indexOf('/chat/') > -1)
                { }
                else {
                    $location.path("/TenantHome");
                }
            }
            if (userData.personType == PersonTypeEnum.owner) {

                var requestedChatPage = '';
                if ($window.localStorage.getItem("requestedChatPage") != null) {
                    var requestedChatPage = JSON.parse($window.localStorage.getItem("requestedChatPage"));
                }
                if ($location.path() == "/LoggedIn" || $location.path() == "/paymentDetail" || $location.path() == "/PropayProfile" || $location.path() == "/payment" || $location.path() == "/TenantHome" || $location.path() == "/home" || $location.path() == "/chat" || $location.path() == "/chat/*") {
                }
                else if (requestedChatPage != '' && requestedChatPage != undefined && requestedChatPage == $location.path() && $location.path().indexOf('/chat/') > -1)
                { }
                else {
                    $location.path("/OwnerHome");
                }
            }
            if ($location.path() == "/login" && !(userData == null || userData == '' || userData == undefined) && ($location.search().isverification == undefined)) {

                $location.path("/home");
            }
            if (($location.path() == "/TenantHome" || $location.path() == "/OwnerHome") && (userData.personType == PersonTypeEnum.property_manager)) {
                $location.path("/home");
            }
            $rootScope.location = $location.path();
            $rootScope.userData = userData;
            $rootScope.checkLogin = true;

            ///////////////////////////////////////////////
            if ($rootScope.currentUserData != undefined) {
                $rootScope.userData.personId = $rootScope.currentUserData.personId;
                $rootScope.userData.personType = $rootScope.currentUserData.personType;
            }
            else {
                $rootScope.currentUserData = $rootScope.userData;
            }
            ///////////////////////////////////////////////
        }
    });
});
// create the controller and inject Angular's $scope
vataraApp.controller('LeftCtrl', function ($scope, mainFactoryService) {

});
vataraApp.controller('AppCtrl', function ($scope) {
    // create a message to display in our view
    $scope.message = 'In LeftCtrl Page!';
});
vataraApp.controller('homeController', function ($scope) {
    // create a message to display in our view
    $scope.message = 'In Home Page!';
});

vataraApp.controller('financialSummaryController', function ($scope) {
    // create a message to display in our view
    $scope.message = 'In Home Page!';
    $scope.myDataSource = {
        chart: {
            caption: "Age profile of website visitors",
            subcaption: "Last Year",
            startingangle: "120",
            showlabels: "0",
            showlegend: "1",
            enablemultislicing: "0",
            slicingdistance: "15",
            showpercentvalues: "1",
            showpercentintooltip: "0",
            plottooltext: "Age group : $label Total visit : $datavalue",
            theme: "fint"
        },
        data: [
            {
                label: "Teenage",
                value: "1250400"
            },
            {
                label: "Adult",
                value: "1463300"
            },
            {
                label: "Mid-age",
                value: "1050700"
            },
            {
                label: "Senior",
                value: "491000"
            }
        ]
    }
});

vataraApp.controller('invoicesController', function ($scope, $mdDialog, $mdMedia) {
    // create a message to display in our view
    $scope.message = 'In Home Page!';
    $scope.showAP = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'create-invoice.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
        .then(function (answer) {
            $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
            $scope.status = 'You cancelled the dialog.';
        });
    };
    $scope.oneAtATime = false;
    $scope.blah = ['blah'];
    $scope.view = false;


    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
    $scope.status = {
        isSecondOpen: true,
        isSecondOpen: false
    };
    $scope.status = {
        isthirdOpen: true,
        isthirdOpen: false
    };
    $scope.status = {
        isfourthOpen: true,
        isfourthOpen: false
    };
    $scope.status = {
        isseventhOpen: true,
        isseventhOpen: false
    };
});

function DialogController($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}

vataraApp.controller('billsController', function ($scope) {
    // create a message to display in our view
    $scope.message = 'In Home Page!';
});

vataraApp.controller('mainController', function ($scope) {
    // create a message to display in our view
    $scope.message = 'Everyone come and see how good I look!';
});

vataraApp.controller('aboutController', function ($scope) {
    $scope.message = 'Look! I am an about page.';
});

vataraApp.controller('contactController', function ($scope) {
    $scope.message = 'Contact us! JK. This is just a demo.';
});

vataraApp.controller('inputCtrl', function ($scope) {
    this.userState = '';
    this.states = ('select select1 select2').split(' ').map(function (state) { return { abbrev: state }; });
    $scope.ctrl = this;
});
// document.write('<script type="text/javascript" src="./js/apps.js"></script>');
// document.write('<script type="text/javascript" src="./js/script.js"></script>');
// document.write('<script type="text/javascript" src="./js/property.js"></script>');