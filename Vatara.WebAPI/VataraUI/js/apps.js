var app = angular.module('Myapp', ['ngMaterial', 'ngMessages', 'ui.bootstrap']);

app.controller('MenuCtrl', ['$scope', '$mdSidenav', function ($scope, $mdSidenav) {
    $scope.index = 0;

    $scope.toggleSidenav = function (menuId) {
        $mdSidenav(menuId).toggle();
    };
}]);

//Script for strlimit in Association Page
app.controller('sizeCtrl', function ($scope) {
    $scope.cars = ["557 S Highway", "1974 Quesda Ave", "The Presidio Residentials", "34 S Highway", "654 Quesda Ave", "The Presidio"];
});
//Script for strlimit in Association Page
app.controller('sizeCtrl1', function ($scope) {
    $scope.cars = ["557 S Highway", "1974 Quesda Ave", "The Presidio Residentials"];
});





app.controller('AppCtrl1', function ($scope, $mdDialog, $mdMedia) {
    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

    $scope.showAP = function (ev) {     
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'work-order.tmpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
        .then(function (answer) {
            $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
            $scope.status = 'You cancelled the dialog.';
        });
    };
    $scope.showAR = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'ar-template.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
        .then(function (answer) {
            $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
            $scope.status = 'You cancelled the dialog.';
        });
    };
    $scope.showPO = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'showPOdialogue.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
        .then(function (answer) {
            $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
            $scope.status = 'You cancelled the dialog.';
        });
    };
    $scope.showInvoice = function (ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'showInvoice.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
        .then(function (answer) {
            $scope.status = 'You said the information was "' + answer + '".';
        }, function () {
            $scope.status = 'You cancelled the dialog.';
        });
    };
});

function DialogController($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}

app.controller('cloneCtrl', function ($scope) {
    $scope.clonerow = function () {
        var parentElement = angular.element(document.querySelector('.units'));
        var tobeClonedElement = angular.element(document.querySelector('.row'));
        parentElement.append(tobeClonedElement.clone());
        $scope.units = "keshu";
    }
});


//Script For Owners Invoices 
app.controller('invoiceCtrl', function ($scope) {
    $scope.limit = 2;
    $scope.max = false;
    $scope.invoiceMore = function () {

        if ($scope.limit < $scope.rows1.length) {
            $scope.limit = $scope.limit + 2;
            if ($scope.limit >= $scope.rows1.length) {

                $scope.max = true;
            }

        }
        else {

            $scope.max = true;
        }

    };

    $scope.rows1 = [
      {
          rowid: 1,
          invoice_no: "I1234",
          invoice_description: "Ac Repair",
          invoice_status: "Submitted",
          invoice_amt: "$5000",
          invoice_billing: "Pending"
      },
      {
          rowid: 2,
          invoice_no: "I1235",
          invoice_description: "Ac Repair",
          invoice_status: "Submitted",
          invoice_amt: "$5000",
          invoice_billing: "Pending"
      },
      {
          rowid: 3,
          invoice_no: "I1236",
          invoice_description: "Ac Repair",
          invoice_status: "Submitted",
          invoice_amt: "$5000",
          invoice_billing: "Pending"
      }
    ]
});

//Script For Dashboard Rent Past Due 
app.controller('pastdueCtrl', function ($scope) {
    $scope.limit = 2;
    $scope.max = false;
    $scope.invoiceMore = function () {

        if ($scope.limit < $scope.rows2.length) {
            $scope.limit = $scope.limit + 2;
            if ($scope.limit >= $scope.rows2.length) {

                $scope.max = true;
            }

        }
        else {

            $scope.max = true;
        }

    };

    $scope.rows2 = [
      {
          rowid: 1,
          pty_id: "Ptydg5651, Unit",
          pty_address: "1522 Selby Ln #1, Sarasota, FL 34236",
          tenant_name: "Michael Florin (Tenant)",
          dates: "Apr 17, 2016"
      },
      {
          rowid: 2,
          pty_id: "Ptydg5652, Unit",
          pty_address: "1522 Henkis Ln #1, Margosa, GL 23425",
          tenant_name: "Michael Florin (Tenant)",
          dates: "Apr 17, 2016"
      },
      {
          rowid: 3,
          pty_id: "Ptydg5653, Unit",
          pty_address: "Ac Repair",
          tenant_name: "Michael Florin (Tenant)",
          dates: "Apr 17, 2016"
      }
    ]
});

//Script For Dashboard Tenants work orders 
app.controller('workorderCtrl', function ($scope) {
    $scope.limit = 2;
    $scope.max = false;
    $scope.invoiceMore = function () {

        if ($scope.limit < $scope.rows3.length) {
            $scope.limit = $scope.limit + 2;
            if ($scope.limit >= $scope.rows3.length) {

                $scope.max = true;
            }

        }
        else {

            $scope.max = true;
        }

    };

    $scope.rows3 = [
      {
          rowid: 1,
          work_id: "WO4535",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      },
      {
          rowid: 2,
          work_id: "WO5677",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      },
      {
          rowid: 3,
          work_id: "WO6465",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      },
      {
          rowid: 4,
          work_id: "WO6222",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      }
    ]
});

//Script For Dashboard ownerworkCtrl work orders 
app.controller('ownerworkCtrl', function ($scope) {
    $scope.limit = 2;
    $scope.max = false;
    $scope.invoiceMore = function () {

        if ($scope.limit < $scope.rows4.length) {
            $scope.limit = $scope.limit + 2;
            if ($scope.limit >= $scope.rows4.length) {

                $scope.max = true;
            }

        }
        else {

            $scope.max = true;
        }

    };

    $scope.rows4 = [
      {
          rowid: 1,
          work_id: "WO4535",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      },
      {
          rowid: 2,
          work_id: "WO5677",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      },
      {
          rowid: 3,
          work_id: "WO6465",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      },
      {
          rowid: 4,
          work_id: "WO6222",
          work_details: "Danny Kelly, AC Maintenance",
          work_date: "Apr 17, 2016"
      }
    ]
});

//Script For Owners Work Request 
app.controller('fCtrl', function ($scope) {
    $scope.limit = 1;
    $scope.max = false;
    $scope.readMore = function () {

        if ($scope.limit < $scope.rows.length) {
            $scope.limit = $scope.limit + 2;
            if ($scope.limit >= $scope.rows.length) {

                $scope.max = true;
            }

        }
        else {

            $scope.max = true;
        }

    };

    $scope.rows = [
      {
          rowid: 1,
          work_request: "#65465",
          work_order: "wo546",
          work_status: "Pending",
          work_description: "AC Repair",
          work_type: "Regular Maintenance",
          work_source: "Owner",
          labour_cost: "$450",
          material_cost: "$150",
          labour_hours: "10 hrs",
          total_cost: "$600"
      },
      {
          rowid: 2,
          work_request: "#65465",
          work_order: "wo546",
          work_status: "Pending",
          work_description: "AC Repair",
          work_type: "Regular Maintenance",
          work_source: "Owner",
          labour_cost: "$150",
          material_cost: "$750",
          labour_hours: "10 hrs",
          total_cost: "$900"
      },
      {
          rowid: 3,
          work_request: "#65465",
          work_order: "wo546",
          work_status: "Pending",
          work_description: "AC Repair",
          work_type: "Regular Maintenance",
          work_source: "Owner",
          labour_cost: "$650",
          material_cost: "$150",
          labour_hours: "10 hrs",
          total_cost: "$800"
      },
    ]
});





app.controller('MainCtrl', function ($scope) {
});

app.controller('mainController', function ($scope) {
    $scope.loggedActions = [];
    $scope.owners = [
        { id: 1, docName: 'Licence_Agrement.pdf', docType: 'Aggrement', docSize: '400kb', docDate: '12/1/2014', uploadedBy: 'Javier Levis' },
        { id: 2, docName: 'Account_change_form.pdf', docType: 'Account change form', docSize: '40kb', docDate: '15/1/2014', uploadedBy: 'Calthy Robinson' },
        { id: 3, docName: 'Licence_Agrement.doc', docType: 'Aggrement', docSize: '50kb', docDate: '19/7/2014', uploadedBy: 'Herman Dean' },
        { id: 4, docName: 'Account_change_form.pdf', docType: 'Account change form', docSize: '200kb', docDate: '12/2/2014', uploadedBy: 'Calthy Robinson' }
    ];
    $scope.deleteowner = function (owner) {
        var index = $scope.owners.indexOf(owner);
        $scope.owners.splice(index, 1);
    };
});

app.directive('readMore', function () {
    return {
        restrict: 'A',
        transclude: true,
        replace: true,
        template: '<p></p>',
        scope: {
            moreText: '@',
            lessText: '@',
            words: '@',
            ellipsis: '@',
            char: '@',
            limit: '@',
            content: '@'
        },
        link: function (scope, elem, attr, ctrl, transclude) {
            var moreText = angular.isUndefined(scope.moreText) ? ' <a class="read-more">.. + Show More</a>' : ' <a class="read-more">' + scope.moreText + '</a>',
              lessText = angular.isUndefined(scope.lessText) ? ' <a class="read-less">- Show Less</a>' : ' <a class="read-less">' + scope.lessText + '</a>',
              ellipsis = angular.isUndefined(scope.ellipsis) ? '' : scope.ellipsis,
              limit = angular.isUndefined(scope.limit) ? 60 : scope.limit;

            attr.$observe('content', function (str) {
                readmore(str);
            });

            transclude(scope.$parent, function (clone, scope) {
                readmore(clone.text().trim());
            });

            function readmore(text) {

                var text = text,
                  orig = text,
                  regex = /\s+/gi,
                  charCount = text.length,
                  wordCount = text.trim().replace(regex, ' ').split(' ').length,
                  countBy = 'char',
                  count = charCount,
                  foundWords = [],
                  markup = text,
                  more = '';

                if (!angular.isUndefined(attr.words)) {
                    countBy = 'words';
                    count = wordCount;
                }

                if (countBy === 'words') { // Count words

                    foundWords = text.split(/\s+/);

                    if (foundWords.length > limit) {
                        text = foundWords.slice(0, limit).join(' ') + ellipsis;
                        more = foundWords.slice(limit, count).join(' ');
                        markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
                    }

                } else { // Count characters

                    if (count > limit) {
                        text = orig.slice(0, limit) + ellipsis;
                        more = orig.slice(limit, count);
                        markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
                    }

                }

                elem.append(markup);
                elem.find('.read-more').on('click', function () {
                    $(this).hide();
                    elem.find('.more-text').addClass('show').slideDown();
                });
                elem.find('.read-less').on('click', function () {
                    elem.find('.read-more').show();
                    elem.find('.more-text').hide().removeClass('show');
                });

            }
        }
    };
});





app.controller('AccordionDemoCtrl', function ($scope) {
    $scope.oneAtATime = false;
    $scope.blah = ['blah'];
    $scope.view = false;


    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
    $scope.status = {
        isSecondOpen: true,
        isSecondOpen: false
    };
    $scope.status = {
        isthirdOpen: true,
        isthirdOpen: false
    };
    $scope.status = {
        isfourthOpen: true,
        isfourthOpen: false
    };
    $scope.status = {
        isseventhOpen: true,
        isseventhOpen: false
    };

});

app.controller('inputCtrl', function () {
    this.userState = '';
    this.states = ('select select1 select2').split(' ').map(function (state) { return { abbrev: state }; });
});

app.controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function () {
        return $mdSidenav('right').isOpen();
    };
    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
        var timer;
        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function () {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
        return debounce(function () {
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                  $log.debug("toggle " + navID + " is done");
              });
        }, 200);
    }
    function buildToggler(navID) {
        return function () {
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                  $log.debug("toggle " + navID + " is done");
              });
        }
    }
})
  .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
      $scope.close = function () {
          $mdSidenav('left').close()
            .then(function () {
                $log.debug("close LEFT is done");
            });
      };
  })
  .controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
      $scope.close = function () {
          $mdSidenav('right').close()
            .then(function () {
                $log.debug("close RIGHT is done");
            });
      };
  });
//Search auto completion
app.controller('DemoCtrl', DemoCtrl);
function DemoCtrl($timeout, $q, $log) {
    var self = this;
    self.simulateQuery = false;
    self.isDisabled = false;
    self.repos = loadAll();
    self.querySearch = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange = searchTextChange;
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for repos... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch(query) {
        var results = query ? self.repos.filter(createFilterFor(query)) : self.repos,
            deferred;
        if (self.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }
    function searchTextChange(text) {
        $log.info('Text changed to ' + text);
    }
    function selectedItemChange(item) {
        $log.info('Item changed to ' + JSON.stringify(item));
    }
    /**
     * Build `components` list of key/value pairs
     */
    function loadAll() {
        var repos = [
          {
              'name': 'PT3210',
              'watchers': 'in',
              'forks': 'Properties',
          },
          {
              'name': 'PT3210',
              'watchers': 'in',
              'forks': 'Maintanance',
          },
          {
              'name': 'PT3210',
              'watchers': 'in',
              'forks': 'Properties',
          },
          {
              'name': 'PT3210',
              'watchers': 'in',
              'forks': 'Maintanance',
          },
          {
              'name': 'PT3210',
              'watchers': 'in',
              'forks': 'Properties',
          }
        ];
        return repos.map(function (repo) {
            repo.value = repo.name.toLowerCase();
            return repo;
        });
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            return (item.value.indexOf(lowercaseQuery) === 0);
        };
    }
};