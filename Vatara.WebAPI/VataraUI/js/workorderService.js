//var myApp = angular.module('Myapp');

vataraApp.service('workorderService', function ($http) {

    this.lstWorkOrderStatus = [];
    

    this.getWorkorderSummary = function (onComplete) {
        $http({ method: 'GET', url: '/api/workorder' })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    //this.saveWorkorder = function (woData, onComplete) {
    //    $http({ method: 'post', data: woData, url: '/api/workorder' })
    //        .success(function (data, status, headers, config) {
    //            onComplete(true, data);
    //        })
    //        .error(function (data, status, headers, config) {
    //            onComplete(false, status);
    //        });
    //};


    this.saveWorkorder = function (MdlWorkOrderAndWorkRequests) {       
        var responce = $http.post(
            '/api/workorder/SaveWorkOrder',
            JSON.stringify(MdlWorkOrderAndWorkRequests),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.updateWorkOrder = function (MdlWorkOrderAndWorkRequests) {
        var responce = $http.post(
            '/api/workorder/UpdateWorkOrder',
            JSON.stringify(MdlWorkOrderAndWorkRequests),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.addUpdateWorkRequests = function (MdlWorkRequests) {
        var responce = $http.post(
            '/api/workorder/AddUpdateWorkRequests',
            JSON.stringify(MdlWorkRequests),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }
    
    this.changeWorkOrderStatus = function (MdlWorkOrderStatus) {
        var responce = $http.post(
            '/api/workorder/ChangeWorkOrderStatus',
            JSON.stringify(MdlWorkOrderStatus),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.saveFiles = function (files, url, onComplete) {
        $http.post(url, files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            onComplete(true, status);
        })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };



    this.getWorkOrderByStatus = function (status, pmid, pagesize, page, propertyid,poid) {
        var responce = $http({
            method: "Get",
            url: "/api/WorkOrder/GetWorkOrderByStatus",
            params: {
                status: status,
                pmid: pmid,
                pagesize: pagesize,
                page: page,
                propertyid: propertyid,
                poid:poid
            },
            headers: { 'Content-Type': JSON }

        });
        return responce;
    };


    this.getWorkOrderWithWorkRequests = function (workOrderId) {
        var responce = $http({
            method: "Get",
            url: "/api/WorkOrder/GetWorkOrderWithWorkRequests",
            params: {
                workOrderId: workOrderId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };


    this.getWorkOrderStatus = function () {
        var responce = $http({
            method: "Get",
            url: "/api/WorkOrder/GetWorkOrderStatus",            
            headers: { 'Content-Type': JSON }
        });
        this.lstWorkOrderStatus = responce;
        return responce;
    };

    this.getPropertyOwnerTenantManager = function (propertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/WorkOrder/GetPropertyOwnerTenantManager",
            params: {
                propertyId: propertyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };

    //this.getWorkorderDocuments = function (workorderId) {
    //    var responce = $http({
    //        method: "Get",           
    //        url: "/api/workorderdocument/Get?id=" + workorderId,            
    //        headers: { 'Content-Type': JSON }
    //    });
    //    return responce;
    //};

    this.getWorkorderDocuments = function (workorderId, onComplete) {
        $http({ method: 'GET', url: "/api/workorderdocument/Get?id=" + workorderId,  })
            .success(function (data, status, headers, config) {
                documentsOfselectedProperty = data;
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };


    this.deleteWorkOrder = function (mdlWorkorderIdToDelete) {
        var responce = $http.post(
            '/api/WorkOrder/DeleteWorkOrder',
            JSON.stringify(mdlWorkorderIdToDelete),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.deleteWorkRequest = function (MdlDeleteWorkRequest) {
        var responce = $http.post(
            '/api/WorkOrder/DeleteWorkRequest',
            JSON.stringify(MdlDeleteWorkRequest),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }


    this.getWorkRequestsByProperty = function (propertyId) {
        var responce = $http({
            method: "Get",
            url: "/api/WorkOrder/GetWorkRequestsByProperty",
            params: {                
                propertyId: propertyId
            },
            headers: { 'Content-Type': JSON }
        });
        return responce;
    };



    this.updateWorkRequestStatus = function (mdlUpdateWorkRequest) {
        var responce = $http.post(
            '/api/workorder/UpdateWorkRequestStatus',
            JSON.stringify(mdlUpdateWorkRequest),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        return responce;
    }

    this.getPropertByManagerAndOwner = function (managerId, isManager, pmid, personId, propertyId, personType) {

        var params = {};
        var URL = "";
        if (isManager == true) {
            URL = "/api/workorder/PropertByManagerAndOwner";
            params.managerId = managerId;
        }
        else {
            URL = "/api/workorder/PropertByTenant_Owner";
            params.PMID = pmid;
            params.PersonId = personId;
            params.PropoertyId = propertyId;
            params.PersonType = personType;
        }
        var responce = $http({
            method: "Get",
            url: URL,
            params: params,
            //params: {
            //    managerId: managerId
            //},
            headers: { 'Content-Type': JSON }
        });
        return responce;
    }
    
});