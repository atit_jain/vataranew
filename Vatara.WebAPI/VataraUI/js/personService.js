var myApp = angular.module('Myapp');

myApp.service('personService', function ($http) {

    this.getPerson = function (onComplete) {
        $http({ method: 'GET', url: '/api/person' })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getProperties = function (personId, onComplete)
    {
        $http({ method: 'GET', url: '/api/person/Get?id=' + personId })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.getPersonPhotos = function (propId, onComplete) {
        $http({ method: 'GET', url: '/api/personphoto/'+ propId })
            .success(function (data, status, headers, config) {
                //documentsOfselectedProperty = data;
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };    
    

    this.savePerson = function (personData, onComplete) {
        $http({ method: 'post', data: personData, url: '/api/person' })
            .success(function (data, status, headers, config) {
                onComplete(true, data);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.saveFiles = function (files, url, onComplete) {

        $http.post(url , files, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).success(function (data, status, headers, config) {
            //console.log(d);
            onComplete(true, status);
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };

    this.RemoveFile = function (propId, onComplete) {
        $http({ method: 'DELETE', url: '/api/personphoto/Delete?id=' + propId })
            .success(function (data, status, headers, config) {
                onComplete(true, data)
            })
            .error(function (data, status, headers, config) {
                onComplete(false, status);
            });
    };
});	
