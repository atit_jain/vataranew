﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public partial class LeaseClauseMap
    {
        public int clauseid { get; set; }

        public int pLeaseId { get; set; }

        public string clausetype { get; set; }

        public string clausedesc { get; set; }

        public bool isDeleted { get; set; }
    }

    public partial class LeaseOptionMap
    {
        public int optionid { get; set; }

        public int pLeaseId { get; set; }

        public string optiontype { get; set; }

        public string optiondesc { get; set; }

        public string optionreqdby { get; set; }

        public DateTime optioneffon { get; set; }

        public DateTime optionlastexec { get; set; }

        public bool isdeleted { get; set; }
    }

    public partial class LeaseTenantMap
    {
        public int tenantid { get; set; }

        public string tenantname { get; set; }

        public string tenantphoneno { get; set; }

        public string tenantemail { get; set; }

        public bool isDeleted { get; set; }
    }

    public partial class LeaseMap
    {
        public int pId { get; set; }

        public int pPropertyId { get; set; }

        public string pLeaseId { get; set; }

        public string pLeaseDesc { get; set; }

        public DateTime pLeaseStartDate { get; set; }

        public DateTime pLeaseEndDate { get; set; }

        public LeaseTenantMap[] pTenantArray { get; set; }

        public int pAgreementTypeId { get; set; }

        public string pLeaseType { get; set; }

        public int pPaymentFrequency { get; set; }

        public decimal pSecurityDeposit { get; set; }

        public decimal pLeaseAmount { get; set; }

        public bool pLeaseActive { get; set; }

        public int pLeaseTermId { get; set; }

        public short pLeaseTermTypeId { get; set; }

        public string pComments { get; set; }


        public decimal pLatePayamentFee { get; set; }

        public int pLatePayamentChargedId { get; set; }

        public bool pIsMoveInInspectionReqd { get; set; }

        public bool pIsMoveOutInspectionReqd { get; set; }

        public int pMaxPeopleAllowedToLive { get; set; }

        public LeaseClauseMap[] pClauseArray { get; set; }

        public LeaseOptionMap[] pOptionArray { get; set; }

        public MdlOccupant[] occupantArray { get; set; }

        public int createdId { get; set; }
        public int pParkingSpaces { get; set; }
        public decimal pRentableArea { get; set; }

        public string pTerminationDesc { get; set; }

        public string pWeekDay { get; set; }
        public int? pDayOfMonth { get; set; }
        public int? pMonthNumber { get; set; }
        public int pMID { get; set; }

    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class LeaseController : ApiController
    {
        public IEnumerable<getLeaseByManagerId_Result> GetAllLease(int id)
        {
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    return entities.getLeaseByManagerId(id).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }
        public List<getLeaseByPropertyIdOrLeaseId_Result> GetLeaseByPropertyId(int propertyId)
        {
            List<getLeaseByPropertyIdOrLeaseId_Result> lst = new List<getLeaseByPropertyIdOrLeaseId_Result>();
            using (VataraEntities entities = new VataraEntities())
            {
                try
                {
                    int leaseId = 0;
                    lst =  entities.getLeaseByPropertyIdOrLeaseId(leaseId, propertyId).ToList();
                }
                catch (Exception ex)
                {
                }
                return lst;
            }
        }
        //[HttpGet]
        //[Route("api/lease/{id}")]
        public MdlLeaseData GetLeaseById(int leaseId)
        {
            MdlLeaseData MdlLeaseData = new MdlLeaseData();
            MdlLeaseDetail MdlLeaseDetail = new MdlLeaseDetail();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    int propertyId = 0;
                  //  var leaseDtl = entities.getLeaseByPropertyIdOrLeaseId(leaseId, propertyId).FirstOrDefault();
                    MdlLeaseData.LeaseDetail = MdlLeaseDetail.GetMdl_LeaseDetail(entities.getLeaseByPropertyIdOrLeaseId(leaseId, propertyId).FirstOrDefault());
                    entities.LeaseTypes.Where(LT => LT.PropertyCategoryId == MdlLeaseData.LeaseDetail.pCategoryId).ToList().ForEach(u =>
                    {
                        MdlLeaseData.lstLeaseType.Add(new MdlLeaseType().GetMdl_LeaseType(u));
                    });
                    return MdlLeaseData;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                MdlLeaseData = null;
                MdlLeaseDetail = null;
            }
            return MdlLeaseData;
        }

        public ResponseMdl GetLeaseType(int propertyCategoryId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlLeaseType> lstLeaseType = new List<MdlLeaseType>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    try
                    {
                        entities.LeaseTypes.Where(LT => LT.PropertyCategoryId == propertyCategoryId).ToList().ForEach(u =>
                        {
                            lstLeaseType.Add(new MdlLeaseType().GetMdl_LeaseType(u));
                        });
                        response.Data = lstLeaseType;
                        response.Status = true;
                        return response;
                    }
                    catch (Exception ex)
                    {
                        response.Data = ex.Message;
                        response.Status = false;
                        return response;
                    }
                }
            }
            finally
            {
                lstLeaseType = null;
            }
        }


        public List<MdlOccupant> GetOccupantByLeaseId(int leaseId)
        {
            List<MdlOccupant> lstOccupant = new List<MdlOccupant>();
            using (VataraEntities entities = new VataraEntities())
            {
                entities.vwOccupants.Where(o => o.LeaseId == leaseId).ToList().ForEach(x =>
                lstOccupant.Add(new MdlOccupant().GetMdl_Occupant(x))
                );
                return lstOccupant;
            }
        }


        [HttpPost]
        public ResponseMdl RenewLease([FromBody] LeaseRenewMdl leaseRenewMdl)
        {
            ResponseMdl response = new ResponseMdl();
            Lease lease_New = new Lease();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    using (var dbcontextTran = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var lease = entities.Leases.FirstOrDefault(p => p.Id == leaseRenewMdl.leaseId);
                            lease.StartDate = leaseRenewMdl.leaseStartDate.ToUniversalTime();
                            lease.EndDate = leaseRenewMdl.leaseEndDate.ToUniversalTime();
                            lease.LeaseAmount = leaseRenewMdl.leaseAmount;
                            lease.CreationTime = DateTime.UtcNow;
                            lease.CreatorUserId = leaseRenewMdl.createdId;
                            entities.Entry(lease).State = System.Data.Entity.EntityState.Added;
                            entities.Leases.Add(lease);
                            entities.SaveChanges();
                            var leaseOption = entities.LeaseOptions.Where(p => p.LeaseId == leaseRenewMdl.leaseId && p.IsDeleted == false).ToArray();
                            foreach (var option in leaseOption)
                            {
                                // option.Id = 0;
                                option.LeaseId = lease.Id;
                                option.CreatorUserId = leaseRenewMdl.createdId;
                                option.CreationTime = DateTime.UtcNow;
                                entities.LeaseOptions.Add(option);
                            }
                            entities.SaveChanges();
                            var leaseClauses = entities.LeaseClauses.Where(p => p.LeaseId == leaseRenewMdl.leaseId && p.IsDeleted == false).ToArray();
                            foreach (var clauses in leaseClauses)
                            {
                                // clauses.Id = 0;
                                clauses.LeaseId = lease.Id;
                                clauses.CreationTime = DateTime.UtcNow;
                                clauses.CreatorUserId = leaseRenewMdl.createdId;
                                entities.LeaseClauses.Add(clauses);
                            }
                            entities.SaveChanges();
                            var leaseTenants = entities.LeaseTenants.Where(p => p.LeaseId == leaseRenewMdl.leaseId && p.IsDeleted == false).ToArray();
                            foreach (var tenants in leaseTenants)
                            {
                                // tenants.Id = 0;
                                tenants.LeaseId = lease.Id;
                                tenants.CreationTime = DateTime.UtcNow;
                                tenants.CreatorUserId = leaseRenewMdl.createdId;
                                entities.LeaseTenants.Add(tenants);
                            }
                            var occupants = entities.Occupants.Where(p => p.LeaseId == leaseRenewMdl.leaseId && p.IsDeleted == false).ToArray();
                            foreach (var occupant in occupants)
                            {
                                // occupant.Id = 0;
                                occupant.LeaseId = lease.Id;
                                occupant.CreationTime = DateTime.UtcNow;
                                occupant.CreatorUserId = leaseRenewMdl.createdId;
                                entities.Occupants.Add(occupant);
                            }
                            entities.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
        }

        [HttpPost]
        public ResponseMdl Savelease([FromBody] LeaseMap leasemap)
        {
            ResponseMdl response = new ResponseMdl();
            clsLeaseInvoiceScheduleHelper leasehelper = null;
            Occupant occupant = null;
            Lease lease = null;
            LeaseTenant ltenant = null;
            LeaseTenant mtenant = null;
            LeaseClaus lc = null;
            LeaseOption lo = null;
            try
            {
                DateTime dtNow = DateTime.Now;
                int pId = 0;
                lease = new Lease();
                using (VataraEntities ve = new VataraEntities())
                {
                    pId = leasemap.pId;
                    if (pId > 0)
                    {
                        var mLease = ve.Leases.FirstOrDefault(p => p.Id == pId);
                        if (mLease != null)
                            lease = mLease as Lease;
                    }
                    lease.LeaseDesc = leasemap.pLeaseDesc;
                    lease.StartDate = leasemap.pLeaseStartDate.ToUniversalTime();
                    lease.EndDate = leasemap.pLeaseEndDate.ToUniversalTime();
                    lease.AgreementTypeId = leasemap.pAgreementTypeId;
                    lease.LeaseType = leasemap.pLeaseType;
                    lease.PaymentFrequencyId = leasemap.pPaymentFrequency;
                    lease.SecurityDeposit = leasemap.pSecurityDeposit;
                    lease.LeaseAmount = leasemap.pLeaseAmount;
                    lease.LeaseActive = leasemap.pLeaseActive;
                    lease.Term = leasemap.pLeaseTermId;
                    lease.TermType = leasemap.pLeaseTermTypeId;
                    lease.ParkingSpaces = leasemap.pParkingSpaces;
                    lease.RentableArea = leasemap.pRentableArea;
                    lease.LatePaymentFee = leasemap.pLatePayamentFee;
                    lease.LatePaymentFeeChargedId = leasemap.pLatePayamentChargedId;
                    lease.IsMoveInInspectionReqd = leasemap.pIsMoveInInspectionReqd;
                    lease.IsMoveOutInspectionReqd = leasemap.pIsMoveOutInspectionReqd;
                    lease.MaxPeopleAllowed = leasemap.pMaxPeopleAllowedToLive;
                    lease.WeekDay = leasemap.pWeekDay;
                    lease.DayOfMonth = leasemap.pDayOfMonth;
                    lease.MonthNumber = leasemap.pMonthNumber;
                    if (pId == 0)
                    {
                        lease.Id = 0;
                        lease.CreationTime = dtNow.ToUniversalTime();
                        lease.CreatorUserId = leasemap.createdId;
                        lease.IsDeleted = false;
                        lease.PropertyId = leasemap.pPropertyId;
                        lease.LeaseId = "LP" + leasemap.pPropertyId.ToString() + "_" + (ve.Leases.ToList().Count() + 1).ToString();
                    }
                    //int LeaseId = 0;
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            if (pId == 0)
                            {
                                ve.Leases.Add(lease);
                            }
                            ve.SaveChanges();
                            var propStatus = ve.Properties.FirstOrDefault(p => p.Id == lease.PropertyId);
                            if (propStatus != null)
                            {
                                if (lease.LeaseActive == true)
                                    propStatus.status = ve.Status.FirstOrDefault(s => s.Name == clsConstants.Leased && s.ModuleName == clsConstants.Propertymodule).Id;
                                else
                                    propStatus.status = ve.Status.FirstOrDefault(s => s.Name == clsConstants.Vacant && s.ModuleName == clsConstants.Propertymodule).Id;
                                ve.SaveChanges();
                            }
                            foreach (var cl in leasemap.pClauseArray)
                            {
                                lc = new LeaseClaus();

                                if (cl.clauseid > 0)
                                {
                                    lc = ve.LeaseClauses.FirstOrDefault(p => p.Id == cl.clauseid && p.LeaseId == lease.Id);
                                }
                                lc.ClauseType = cl.clausetype;
                                lc.ClauseDesc = cl.clausedesc.Replace(System.Environment.NewLine, "<br />").Replace("\n", "<br />");
                                lc.LeaseId = lease.Id;
                                lc.IsDeleted = cl.isDeleted;
                                if (cl.clauseid == 0)
                                {
                                    lc.Id = 0;
                                    lc.CreationTime = dtNow.ToUniversalTime();
                                    lc.CreatorUserId = leasemap.createdId;
                                    lc.IsDeleted = false;
                                    ve.LeaseClauses.Add(lc);
                                }
                                else
                                {
                                    lc.LastModificationTime = dtNow.ToUniversalTime();
                                    lc.LastModifierUserId = leasemap.createdId;
                                }
                                ve.SaveChanges();
                                //lease.L = lease.Id;
                            }
                            foreach (var opt in leasemap.pOptionArray)
                            {
                                lo = new LeaseOption();
                                if (opt.optionid > 0)
                                {
                                    lo = ve.LeaseOptions.First(c => c.Id == opt.optionid && c.LeaseId == lease.Id);
                                }
                                lo.OptionType = opt.optiontype;
                                lo.OptionDesc = opt.optiondesc.Replace(System.Environment.NewLine, "<br />").Replace("\n", "<br />");
                                lo.RequiredBy = opt.optionreqdby;
                                lo.EffectiveOn = opt.optioneffon.ToUniversalTime();
                                lo.LastExecution = opt.optionlastexec.ToUniversalTime();
                                lo.LeaseId = lease.Id;
                                lo.IsDeleted = opt.isdeleted;
                                if (opt.optionid == 0)
                                {
                                    lo.Id = 0;
                                    lo.CreationTime = dtNow.ToUniversalTime();
                                    lo.IsDeleted = false;
                                    ve.LeaseOptions.Add(lo);
                                }
                                else
                                {
                                    lo.LastModificationTime = dtNow.ToUniversalTime();
                                    lo.LastModifierUserId = leasemap.createdId;
                                }
                                ve.SaveChanges();
                                //lease.L = lease.Id;
                            }
                            int pSeqNo = 1;
                            foreach (var lt in leasemap.pTenantArray)
                            {
                                ltenant = new LeaseTenant();
                                //LeaseTenant mtenant = null;
                                if (lt.tenantid > 0)
                                {
                                    mtenant = ve.LeaseTenants.FirstOrDefault(p => p.TenantId == lt.tenantid && p.LeaseId == lease.Id);
                                    if (mtenant != null)
                                        ltenant = mtenant as LeaseTenant;
                                }
                                ltenant.SeqNo = pSeqNo;
                                ltenant.IsDeleted = lt.isDeleted;
                                if (mtenant == null)
                                {
                                    ltenant.TenantId = lt.tenantid;
                                    ltenant.LeaseId = lease.Id;
                                    ltenant.IsPrimaryTenant = true;
                                    if (mtenant == null)
                                    {
                                        ltenant.Id = 0;
                                        ltenant.CreationTime = dtNow.ToUniversalTime();
                                        ltenant.IsDeleted = false;
                                    }
                                    else
                                    {
                                        ltenant.CreationTime = ve.LeaseTenants.FirstOrDefault(p => p.TenantId == lt.tenantid && p.LeaseId == lease.Id).CreationTime;
                                    }
                                    ve.LeaseTenants.Add(ltenant);
                                    ve.SaveChanges();
                                    //lease.L = lease.Id;
                                }
                                ve.SaveChanges();
                                pSeqNo += 1;
                            }
                            foreach (var occ in leasemap.occupantArray)
                            {
                                occupant = new Occupant();
                                if (occ.id > 0)
                                {
                                    occupant = ve.Occupants.FirstOrDefault(p => p.Id == occ.id);
                                }
                                occupant.IsDeleted = occ.isDeleted;
                                occupant.FirstName = occ.firstName;
                                occupant.LastName = occ.lastName;
                                occupant.Relation = occ.relation;
                                occupant.Detail = occ.detail;
                                occupant.LeaseId = lease.Id;
                                if (occ.id == 0)
                                {
                                    occupant.CreatorUserId = leasemap.createdId;
                                    occupant.CreationTime = DateTime.UtcNow;
                                    ve.Occupants.Add(occupant);
                                }
                                else
                                {
                                    occupant.LastModifierUserId = leasemap.createdId;
                                    occupant.LastModificationTime = DateTime.UtcNow;
                                }
                                ve.SaveChanges();
                            }
                            leasehelper = new clsLeaseInvoiceScheduleHelper();
                            leasehelper.SaveLeaseInvoiceSchedule(leasemap.pLeaseStartDate, leasemap.pLeaseEndDate, leasemap.pPaymentFrequency, leasemap.pLeaseAmount, leasemap.pDayOfMonth, leasemap.pWeekDay, leasemap.pMID, lease.Id,ve);

                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Data = lease.Id;
                            return response;

                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.errorInLeaseSaving.ToString();
                            return response;                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.errorInLeaseSaving.ToString();
                return response;
            }
            finally {
                 leasehelper = null;                
                 occupant = null;
                 lease = null;
                 ltenant = null;
                 mtenant = null;
                 lc = null;
                 lo = null;
            }
        }

        [HttpPost]
        public ResponseMdl TerminateLease([FromBody] LeaseMap leasemap)
        {
            ResponseMdl response = new ResponseMdl();
            Lease lease = new Lease();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lease = entities.Leases.FirstOrDefault(l => l.Id == leasemap.pId);
                    if (lease != null)
                    {
                        lease.LastModifierUserId = leasemap.createdId;
                        lease.EndDate = DateTime.UtcNow;
                        lease.TerminationDescription = leasemap.pTerminationDesc;
                        lease.LastModificationTime = DateTime.UtcNow;
                        lease.LeaseActive = false;
                        entities.SaveChanges();
                        //PM 21-Nov-2018
                        var bill = entities.Bills.Where(b => b.PropertyID == lease.PropertyId && b.IsRecurringPayment == true).ToList();
                        bill.ForEach(m =>
                        {
                            m.IsRecurringPayment = false;
                            m.LastModificationTime = DateTime.UtcNow;
                            m.LastModifierUserId = Convert.ToInt32(lease.LastModifierUserId);
                        });
                        entities.SaveChanges();
                        response.Status = true;
                        response.Data = "Success"; //lease;
                    }
                    else
                    {
                        response.Status = true;
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lease = null;
            }
            return response;
        }
    }
}
