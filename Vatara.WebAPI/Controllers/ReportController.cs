﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Vatara.DataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class ReportController : ApiController
    {
        [HttpGet]
        public ResponseMdl GetOwnerStatement(int managerId, int pmid, int ownerPOID, int ownerPersonId, int propId, int year)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropTenantOwnerDetail> PropTenantOwnerDetail = new List<MdlPropTenantOwnerDetail>();
            MdlPropTenantOwnerDetail MdlPropTenantOwnerDetail = new MdlPropTenantOwnerDetail();
            bool isPropIdGiven = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (ownerPOID == 0 && propId == 0)
                    {
                        isPropIdGiven = false;
                        entities.getPropertyDetailByManagerOrOwner(managerId, ownerPersonId, ownerPOID, propId).ToList().ForEach(u =>
                         {
                             MdlPropTenantOwnerDetail = null;
                             MdlPropTenantOwnerDetail = new MdlPropTenantOwnerDetail().GetMdl_PropTenantOwnerDetail(u, year, isPropIdGiven, managerId, pmid);
                             if (MdlPropTenantOwnerDetail.lstMdlTransectionInfo.Count > 0)
                                 PropTenantOwnerDetail.Add(MdlPropTenantOwnerDetail);
                         });
                    }
                    else if (ownerPOID != 0 && propId == 0)
                    {
                        isPropIdGiven = false;
                        entities.getPropertyDetailByManagerOrOwner(managerId, ownerPersonId, ownerPOID, propId).ToList().ForEach(u =>
                         {
                             PropTenantOwnerDetail.Add(new MdlPropTenantOwnerDetail().GetMdl_PropTenantOwnerDetail(u, year, isPropIdGiven, managerId, pmid));
                         });
                    }
                    else if (ownerPOID != 0 && propId != 0)
                    {
                        isPropIdGiven = true;
                        entities.getPropertyDetailByManagerOrOwner(managerId, ownerPersonId, ownerPOID, propId).ToList().ForEach(u =>
                         {
                             PropTenantOwnerDetail.Add(new MdlPropTenantOwnerDetail().GetMdl_PropTenantOwnerDetail(u, year, isPropIdGiven, managerId, pmid));
                         });
                    }
                    else if (ownerPOID == 0 && propId != 0)
                    {
                        isPropIdGiven = true;
                        entities.getPropertyDetailByManagerOrOwner(managerId, ownerPersonId, ownerPOID, propId).ToList().ForEach(u =>
                         {
                             PropTenantOwnerDetail.Add(new MdlPropTenantOwnerDetail().GetMdl_PropTenantOwnerDetail(u, year, isPropIdGiven, managerId, pmid));
                         });
                    }
                    response.Status = true;
                    response.Data = PropTenantOwnerDetail;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                MdlPropTenantOwnerDetail = null;
                PropTenantOwnerDetail = null;
            }
        }

        [HttpGet]
        public ResponseMdl OnlineStatement(int managerId, int personId, int personType, int propId, DateTime startDate, DateTime EndDate, int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            MdlOnlineStatement objMdlOnlineStatement = new MdlOnlineStatement();
            decimal? dPreviousBalance = 0, dRunningBalance = 0;
            decimal? dCurrentCR = 0, dCurrentDR = 0;
            MdlOnlineStatementReport objMdlOnlineStatementReport = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {

                    if (personType == (int)clsCommon.PersonType.Tenant)
                    {
                        ownerPOID = 0;
                    }
                    var OnlineStatementData = entities.getOnlineStatement(startDate, EndDate, personId, propId, ownerPOID).ToList();
                    dCurrentCR = OnlineStatementData.Sum(item => item.CR);
                    dCurrentDR = OnlineStatementData.Sum(item => item.DR);
                    if (OnlineStatementData.Count > 0)
                    {
                        dPreviousBalance = OnlineStatementData[0].BeginningBalance;                        
                    }
                    objMdlOnlineStatement.PreviousBalance = (decimal)dPreviousBalance;
                    objMdlOnlineStatement.CurrentCharges = (decimal)(dCurrentCR - dCurrentDR);
                    objMdlOnlineStatement.BalanceDue = (decimal)(objMdlOnlineStatement.PreviousBalance + objMdlOnlineStatement.CurrentCharges);
   
                    objMdlOnlineStatement.lstTransectionInfo = new List<MdlOnlineStatementReport>();
                    foreach (var item in OnlineStatementData)
                    {                      
                        objMdlOnlineStatementReport = new MdlOnlineStatementReport();

                        dRunningBalance += (decimal)item.CR;
                        dRunningBalance -= (decimal)item.DR;
                        objMdlOnlineStatementReport.transactionDate = (DateTime)item.TransactionDate;
                        objMdlOnlineStatementReport.refNo = item.RefNo;
                        objMdlOnlineStatementReport.billInvoiceId = item.billInvoiceId;                       
                        objMdlOnlineStatementReport.cr = (decimal)item.CR;
                        objMdlOnlineStatementReport.dr = (decimal)item.DR;
                        objMdlOnlineStatementReport.comment = item.Description;
                        objMdlOnlineStatementReport.transactionType = item.TransactionType;
                        objMdlOnlineStatementReport.runningBalance = dRunningBalance;
                        objMdlOnlineStatement.lstTransectionInfo.Add(objMdlOnlineStatementReport);                       
                    }
                    objMdlOnlineStatement.PropManager = new MdlPerson().GetMdl_Manager(entities.getManagerDetail(managerId, "PERSON").FirstOrDefault());

                    if (personType == (int)clsCommon.PersonType.Owner)
                        objMdlOnlineStatement.PropOwner = new MdlPerson().GetMdl_Owner(entities.getOwnerDetailByOwnerPersonIdAndPOID(personId, ownerPOID).FirstOrDefault());
                    else if (personType == (int)clsCommon.PersonType.Tenant)
                        objMdlOnlineStatement.PropOwner = new MdlPerson().GetMdl_Person(entities.getPersonDetailByPersonId(personId).FirstOrDefault());
                    
                   

                    response.Status = true;
                    response.Data = objMdlOnlineStatement;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objMdlOnlineStatement = null;
            }
        }



        //[HttpGet]
        //public ResponseMdl OnlineStatement(int managerId, int personId, int personType, int propId, DateTime startDate, DateTime EndDate,int ownerPOID = 0)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    MdlOnlineStatement objMdlOnlineStatement = new MdlOnlineStatement();
        //    decimal? dPreviousBalance = 0, dCurrentBalance = 0, dTotalBalance = 0;
        //    //string strPreviousBalance = "";           
        //    MdlOnlineStatementReport objMdlOnlineStatementReport = null;
        //    try
        //    {
        //        using (VataraEntities entities = new VataraEntities())
        //        {
        //            var OnlineStatementData = entities.getOnlineStatement(startDate, EndDate, managerId, personId, propId, personType).ToList();
        //            if (OnlineStatementData.Count > 0)
        //            {
        //                dTotalBalance = OnlineStatementData[0].TotalBalance;
        //                dPreviousBalance = OnlineStatementData[0].PreviousBalance;
        //                dCurrentBalance = OnlineStatementData[0].CurrentBalance;
        //                objMdlOnlineStatement.Liability = OnlineStatementData[0].Liability;
        //                objMdlOnlineStatement.IsOwner = OnlineStatementData[0].IsOwner;
        //                objMdlOnlineStatement.NetAmountOwnerHasToPay = OnlineStatementData[0].NetAmountOwnerHasToPay;
        //                objMdlOnlineStatement.NetAmountPmHasToPay = OnlineStatementData[0].NetAmountPmHasToPay;
        //            }
        //            objMdlOnlineStatement.lstTransectionInfo = new List<MdlOnlineStatementReport>();
        //            foreach (var item in OnlineStatementData)
        //            {
        //                objMdlOnlineStatementReport = new MdlOnlineStatementReport();
        //                objMdlOnlineStatementReport.amount = (decimal)item.Amount;
        //                objMdlOnlineStatementReport.date = (DateTime)item.Date;
        //                objMdlOnlineStatementReport.serviceCategory = item.ServiceCategory;
        //                objMdlOnlineStatementReport.trantype = item.Trantype;
        //                objMdlOnlineStatementReport.incmExpense = item.incmExpence;
        //                objMdlOnlineStatement.lstTransectionInfo.Add(objMdlOnlineStatementReport);
        //            }
        //            objMdlOnlineStatement.TotalInvoiceAmt = (decimal)dTotalBalance;
        //            objMdlOnlineStatement.PaidAmt = 0;// (decimal)dPreviousBalance;
        //            objMdlOnlineStatement.CurrentCharges = (decimal)dCurrentBalance;

        //            objMdlOnlineStatement.PropManager = new MdlPerson().GetMdl_Manager(entities.getManagerDetail(managerId, "PERSON").FirstOrDefault());

        //            if (personType == (int)clsCommon.PersonType.Owner)
        //                objMdlOnlineStatement.PropOwner = new MdlPerson().GetMdl_Owner(entities.getOwnerDetail(personId, "COMPANY").FirstOrDefault());
        //            else if (personType == (int)clsCommon.PersonType.Tenant)
        //                objMdlOnlineStatement.PropOwner = new MdlPerson().GetMdl_Person(entities.getPersonDetailByPersonId(personId).FirstOrDefault());

        //            objMdlOnlineStatement.SubTotal = objMdlOnlineStatement.lstTransectionInfo.Where(s => s.incmExpense == "expense").Sum(e => e.amount);
        //            objMdlOnlineStatement.PreviousBalance = (decimal)dPreviousBalance;
        //            #region OldCode

        //            //if (objMdlOnlineStatement.PreviousBalance < 0)
        //            //{
        //            //    strPreviousBalance = objMdlOnlineStatement.PreviousBalance.ToString();
        //            //    objMdlOnlineStatement.BalanceDue = (Convert.ToDecimal(strPreviousBalance) + objMdlOnlineStatement.CurrentCharges);
        //            //}
        //            //else
        //            //{
        //            //    objMdlOnlineStatement.BalanceDue = (decimal)dTotalBalance;
        //            //}
        //            #endregion
        //            objMdlOnlineStatement.BalanceDue = (decimal)dTotalBalance;
        //            //if (objMdlOnlineStatement.IsOwner == true)
        //            //    objMdlOnlineStatement.BalanceDue = Convert.ToDecimal(objMdlOnlineStatement.BalanceDue - (objMdlOnlineStatement.Liability));

        //            response.Status = true;
        //            response.Data = objMdlOnlineStatement;
        //            return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Status = false;
        //        response.Message = clsConstants.somethingwrong;
        //        return response;
        //    }
        //    finally
        //    {
        //        objMdlOnlineStatement = null;
        //    }
        //}

        [HttpPost]
        public HttpResponseMessage GeneratePDF([FromBody] MdlGeneratePDF data)  //string[] gridHtml, string pagesize = ""
        {
            string pagesize = data.pagesize;
            string[] gridHtml = data.gridHtml;
            string str = string.Empty;
            StringReader sr = null;
            Document pdfDoc = null;
            PdfWriter writer = null;
            using (MemoryStream stream = new MemoryStream())
            {
                try
                {
                    if (pagesize == "")
                    {
                        pdfDoc = new Document(PageSize.A3);
                    }
                    else
                    {
                        pdfDoc = new Document(PageSize.A4);
                    }
                    writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();
                    if (string.IsNullOrEmpty(data.logo))
                    {
                        string base64 = @"iVBORw0KGgoAAAANSUhEUgAAAKgAAAAwCAYAAACFfjGaAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAEPpJREFUeNrsXHtYVNX6ftfec+EqiiEIo4SapSmVdSqvoKbHk5migoo3rklH8VKooQSiBGJe0jS5qgMIiImoZWY3FT2mWXrMC6aJnmgURZTLDHPd6/cH7HHEmQEUqt/Tfp9n/pjZ3177m7Xe9a7v+9aaIZRSCBDwVwUjdIEAgaACBPwVCdpubMqKJ0YlhAjdLOBRQdoiBu02IMClwmXYXOMs4LSqe/vmJgndLeBPJ+hTL49myzuPWQYgxuTjBEINuqq9s5cLXS7gTyPoU6+8KS53ez22ETmNJAWA6j0R7wvdLuAPJ2i3AZNdK1x851ggJ0AbSEoEkgr4EwhqMy7jIwnVz2uG6QNK2m5sygpzRgKJBQCAqNWYrrtbDpFjc0xjQJHQbmzKCpdbX68WOb9aoRc5fGSOxAIEPFaZacKzzzlqPJ5UAoBb5fep1JRYtEmSxtzu9No9M+QUIODxCTqp93POGTV1FzQMWwMApcd3VeoIa2/xBlPCkoZXU3YCBII+6o0ptXX/BSAzEFZX0K1nMgBQTq02EqwxARu9J5wW0ppL4B66IAyKgFYgqIhyUoCCgsrGarRzAMD1zol0SkgTBfl6Bra/9xPcKg6j88390BK2XjmpoKICHoOgXYeE9PIYHNwHANRUrwIICAAtI1ICwP+O5ZTqCWNjvRUKNRGjXfV5tKMUtnW/oft1OVSs5L56CioqoKUEdR863/deh5cDbzn/YyYArLW3DwVoGUCgJ4wmtUeveagnq6PZuNJEIe2Vl0GoAUfu3MHWqiqIDSr0upqCWlYqjIiAlhNU5hvxUm27Z4aDIkZExCwAJF06/w1LqZg3maKuWwoADrVXjoAiwWxSRAAdYeFx61vYU0DFiuFFWfxwpxJ2nB59ft0MqlcCIPAYOnegMDwCmiRoF5+3vKudnh8LIAYEEFFO7dV/gjMAVBOu0hiTgpEAQNXBD7I5wkjML9MEYvUNEKrHUmUt9IQFAVDDSnCi4hZA9WB1lQBoTE273qM6D48aKQzR3xsi6+QM965q/6I/TLYvCWi0wmUoACxZaWc/dYVKvRcgMi1hlHN6e3ttuXC2VEsYOxvKPdSeHgy8yr+EHQV8dQZoCQMCApZymN6hIygRo87OC1KqB4AYpUOPBPdhC7SKb9cdaukXi49PGK436HUAcPfu3dsbP15/sal7ohYufsnW1tYOADRqjXrVqqST1uzfj102BADUarXqw1UrT1my8/PzF/fp+2z/RxmgpvyYv+Bdb0dHx/YAUFJy6dTOgjxVU22GhUd4du7s5tmUncFgMFRXV1c2p+9MERk5v1f7Du1dAOD8+QsnC3cVqNuEoHV2T74EM3vrLCMRA8DGkp9P13bpLtURFpQQ9xCVKhlAAEd1OnOZjoYAjKEOkSoldIQ1BqV2nA4/i0SodOoDCTWY3hKjlXQsBdBigoaFBcd7eHjwYYJiSuD03nm52VXWOvXjjz/aA8AdgGL37j2pACwSo7j4aMqK5cvG8PaLFkX7WSKSj++QCfPnReY9ygDV1dVdAfCUuWuhYbNkmRmpXzT4gOvXrx8CMLSpNkNDgmL793+1ued0FUqlsubMf88Wr137UeSuT3dYJdvkKdOc8vNyvuZ9Ki299g2A19pkiWc4jeqBROd+iUntMTioFwAQamB5bX1Zrx8N1O8qodF2JQcCrxt7wQKYqNGCgoKAgIDiQ3sHcISgvONAkEY1JoZqVY/yxT7euHkhAAWf470VHmp1+zT8rbB4vlPVarXKz2+sxaOBM4NCXQcNGsiTEwDcZ80KX2GxsEYevSzBMAxr6Vpw0Iw4Ex/g6enZc9Gi6JebalOt0bSkT93t7e2fHjigf9imjRu+bcq4oZ+NPnl5PdlrRULiyDZRUCuIudlxsAuAiE22tpHhGn0yQGQawiolAMqK5ZfsxqXbiEzUsI4Rw7ZOAV+tFmoiaqAmYGfQocDGEbV2nrDldK0Wu6xMSjh+9WrpuW7dvNwBwMdnyLjAwBlxublZlRbU05iUffbZfvnEieMtkzk8NIEQ4m76Wffu3frExsX7Lo+Pe0jtz507f3xX4e73KXd/8nEcZ3jppRd9u3XzGtnwvuzrr7/ZWVVVfZsQwvLJZW1N7b2goBkP+RAQEOi4fbt8RGMyvTUrLB7Av5rbT1qttvCLA19+odPq9I3SBbg84fJEv37Pv+jo6DgZANzcXD3jli33jV8Wa3ZFmxI43Wl7jnxcY5+Cg2YuBXCwbQnaSATE4FQAsOjyxdzqLj3WGkh9uSmpZ2//6F8u7NQQ1tGUoB2rToOAYnltLQyMBBQAA4p19vYwgOA311GwfXB5f2zIs7KT4pfF9gHgzjCMLCwsOB5ApBnCGdVTq9WWZmXnJFki6ISJk2x2FuSNAgCdTofKykq4uroCgHtoaHCcuXAkPW3zdZg5AJOXt0PHE5RSipEjR7zTkhBGLBZ7AkB5eTmcnZ0hFovRo3t375iYuCEJCfFHmhXjajTasW+OybCyWjhnpKfYiMXicQDc+/bpM8hSyBUeFpLAMIwMAG7cuAkXlycgEokgk3n0SE5ePWbx4qh9bVYHbQwx5dSeA6bIAEDK6R1p/aIti1ApPwIAR+WVI0B9uUlLRHC9cxKdOQoQkZHvNgYddtrYQGXrDqmZpOpxEb8s9tBvv5Vd4d/7+vqMmxI43amxenp79zWq51dffZ2/d0+hwUr8Fs8PwskfTmUsWhTtxnFcGQB4du3aMyEhaVSz1UEkEpu+H+c3Udyc+wImBToOHz7Mn48RlyyN7VJcfHQjr1jh4SHxLeimvVYn+bbMSo7jjINjY2NjZ84uMHCGs6+vD6+eiti4+CePHTv+Ce/TjBnTotusDmpWUCmN/r2TbxQAfCGVpPO7SjaoPzBy72BiLkX9UsVqb4NQHdZUV0FvjMco9kkl0AG46j4ezEP7m62znZSdnbOKj0UZhpGFh4UkWFJPg8FQtk2ebTFWnTBxks3IkSMm8YOwI79gvVy+pfzwkeIi43IWPCPmEV2VNV89Q+JZlpUBwOnTZ45kZqSWyev9VvCx6PIVHzQr7pNIJPbWrv/yy+V+UqlUwn/nkkuXTpkPe0KME/fSpV9Op6dtvi7Pyk7iferc2c2Tr3q0MkEtE4WFiAWATFu7hSzqJ5mGYZWTez/nAgBqRuykZxg8qdgLCQAvjqcxIOEMWG/vAJ3YqVVjz8ZYsuS9zxUKRak5FZ09Z97Tpup5+PCRImtlmtCQoHiRSOQJABcvlvy4YcO6cwCwbavcSA4PDw+v5OTVY5rj26McFg8ICHQcNtSXjz8UW7fKPwAAuXxL+aHDR4wTJSQ4aGlz2pNKpf0BZFp69ez51GwAbwBAdXV1ZdS7CwrNqaePzxCjem6TZ30AAFsy08qO/ef4ft6nsNDg+DYgKLXIWzHV13R7dbzTvvNndFLO0GBN3N9T1uQAgEhbcV3PacBwGsxTKqFh7oe8l1mgmhBc6zwaInDNf+4joLC+ZGRU0YbsF6GhwbEmGafi0127N1pqY/yEgAfUc9u2LKPSyuVbyk+cOPkVPxAzZ06PbqsJFxYWbJwkFy5cPMlPEgBIS814nw83ZDKPHolJyaMtzAzTdyHNeCkuXLhYNHvOvGHmmps02X8er57Xrl27mJSYcJy/lpKStoRSalT2+OUJw/+QJb7+Zhr9u4vPPW/vfrSEIaANpHraQF8BANd7P+V43dgPKYAJGq1Ri0XUgPccncCxUkDUrs13I+bMfju7trbWWAMdPHjQmJSUtJB+Lzzva5Jpf//Jpg2XLBIj9D4xrl4tPZecnPhAzXNzStoSfhK4ubl6rl273r+1v0fj2DNne+5q0+u5uVmVP5z68ZtG2XNTxfiyEydOyouLj6YWFx9N/en0mVyT8hwqKu7g7X9H+vTu3csvO2vrbXNtjPrnyOlGn3LyHvApO2vr7e++O1T4qCracoI2EjZO5AAQFqsdHIyNaQmrHv/s83bXjuUrpNrb8FNrjOpJAUg5HW4wDBQdB0JMDX/I8bqiPXvT+Y63tbXtERHxVqapem41UcSmYs/snO2rGtts25qhKC4+ymep7tOmB0a1vnrejz1v3b5dlvjBimONbVJT02NM477Va9aNt9amSqWqeeWVl4MGDx4UMXjwoIh+Lzw/tYFQCgBwdu6ALl1kT1tcnQqLYqVSqRQAbt4svx4TE33gIZ/SMpbwyu7h4eGVtHLV6LYjKDEhKgWknB51Np3xs0gEKVdfSuMI4zpfpVT29e5HCeUQpVKCNtwoohymtncGR1goHZ5+OMxtI7JOmxq4TqlU1pi7dvnylbNrVieftkYMXj0rKyvL42JjvjFnl5KSZhyITi4uspZk9E1h8pRpTsOHDfU3TdDM2W3JTCs7evSYMe4L8J8YaXU4zWwiDB3qG1lW9vuVhpAIixdFpQSHhLs3ths7bgL7xhuvBzVMdEVeXv46c8/Ykb+95sCXB3NbouytkiSB1Ncyy1x8wAE4U58zgYLC21B/Zy+9AaqGsyMUFFJOhxKWwR2nvpCaq3uS1s/keewqLNpsunzxg52enhln6Z6pU2c4/3PkiEDeNn/HzvWWbLdvz6rcv/9AdqOieatgxvSpS3j1rKi4o4iMnJ1ryXbDhk3v6PX66wDQtWuXns1N2kwRtXDxm2q1+goAsCwrS05O3N3YZtas8ESJROIFALdu3SpbsGDeTkvtpaSkLeF9cnNz9fxkc+rMNk+SjBYiJ/x89ieywd6hE0O58noyEvxLq8M7dWpjOyyliHBqDxAGFR0HmGmbtFmi1DDI6xur6JVffz1r7TBGUPDMGD4BqKqqqvz327Pk1p6RnpH5vlarLeVVdMOGTYGP67efn7/4tdeGG9WzYOenG63ZFxTk1hw8+NUOfqIEBU1vcekrPy+natMnKYv5Cd3JxUV2/Pj3mfz1if6T7Uwnbl5+wTqrxdY9hYZvv/3OGItODZwc1UoEbU7R3oCug6Z55Z0/c9uG6u14or2n0qCHgTOSz47T4bRIhIr2L8CG07dp5m4Jn32+f4uJiiqysnKSrSUlI14bbow9i4r2pjbV/p6iXYZ9n33OP8M9IiI8wXKJR2L89QHLsgpCzA/H3Lmz1/JKpVQqa5qaJACQlp4Zw4cbrq6usoyMLbPMFdvt7Ows/lb83XfmFzaECwoA7v37vzpq46bN0wFg3tw5a3lFr6mpqZo3d05+Uz6lpmUYM3onJyfnvPyCJknaKr+LJ6C46fxqBIDFNxhS2g7Uu/5zYlzyCVC2xt5h4ZmzP+azfluoPafFn4FJAf6rklauOg8AWo1WbW7v3DQ++/DDNbP1BoOOUool0Ys/b84zJoz3S0hMSj5NCIFIJBIXFx9lze1O7dn7WXpJyS+nOMpxnIEz7C4sMFsQLj56bN/3J04eYAjDlJaWXkxJ2dR0Urj7U92Cd6JGu7q6elJKuaqqauMZhJzteasOHykuopRyWq1WHRdrWWAHDRoYnpi4cjRhGGNSBQDfHTpcePTYf/YxhGF+vXr1XFrqJ02HWJ/uUL8btfgNF5cnZABw9+698imTA6xzy1qxuOPrSbN04g4pzRkUNRFv1BaFRi58pu/ApSp1AQXc66nLH6nT19iUlbbr1n+C061OI+8xlFr/6XHDNbHubsSd/dGpEPC3hFUFday+cEAndhrQnIYcUL8+fVjy87Ed3Z+Rawij4hWUgDIlrOT7BABKaac+He7++Fxz8x+RvqZCGKa/L4jwH/UC/soQ/gJcgEBQAQIEggoQCCpAgEBQAQIEggoQCCpAgEBQAQJBBQgQCCpAgEBQAf8P8X8DANT67M+MLzuIAAAAAElFTkSuQmCC";
                        //string base64 = data.logo;
                        byte[] imageBytes = Convert.FromBase64String(base64);
                        Image image = Image.GetInstance(imageBytes);
                        image.ScaleToFit(150f, 100f);
                        image.Alignment = Element.ALIGN_CENTER;
                        pdfDoc.Add(image);
                    }
                    if (!string.IsNullOrEmpty(data.headerText))
                    {
                        Chunk chunk = new Chunk(data.headerText, FontFactory.GetFont(FontFactory.TIMES_ROMAN, 22.0f, iTextSharp.text.Font.BOLD | iTextSharp.text.Font.UNDERLINE));
                        Paragraph reportHeadline = new Paragraph(chunk);
                        reportHeadline.SpacingAfter = 30;
                        pdfDoc.Add(reportHeadline);
                    }
                    for (int i = 0; i < gridHtml.Count(); i++)
                    {
                        str = string.Empty;
                        str = gridHtml[i].ToString();
                        sr = null;
                        sr = new StringReader(str);
                        if (i > 0)
                        {
                            pdfDoc.NewPage();
                        }
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    }
                    pdfDoc.Close();
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new ByteArrayContent(stream.ToArray());
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    response.Content.Headers.ContentDisposition.FileName = "Grid.pdf";
                    return response;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        [HttpGet]
        public ResponseMdl PropertByManagerAndOwner(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName });
                    });

                    //entities.vwPropertOwnersWithManagers.Where(e => e.ManagerId == managerId).Select(e => new { e.PropertyId, e.PropertyName, e.OwnerId }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName });
                    //});
                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByTenant_Owner(int PMID, int personId, int propoertyId, int personType, int managerId, int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (personType == (int)clsCommon.PersonType.Owner)
                        entities.getPropertyByOwnerPersonId(personId).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                        {
                            PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = personType });
                        });
                    else
                        PropertOwners = GetPropertyByPerson(ownerPOID, personId, managerId, personType);
                    response.Status = true;
                    response.Data = PropertOwners;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
        }

        [HttpGet]
        public ResponseMdl PropertOwnersByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, ownerFullNameForFilter = u.OwnerFullName,  ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId, ownerIsCompany = u.ownerIsCompany, personId = u.ownerPersonId });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl GetpropertyByPersonAndPersonType(int managerId, int personId = 0, int personType = 0, int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                PropertOwners = GetPropertyByPerson(ownerPOID, personId, managerId, personType);
                response.Status = true;
                response.Data = PropertOwners;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        private List<MdlPropertOwnersWithManager> GetPropertyByPerson(int ownerPOID, int personId, int managerId, int personType)
        {
            List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            //vwPersonWithType PersonWithType = new vwPersonWithType();
            using (VataraEntities entities = new VataraEntities())
            {
                entities.getPropertiesByPerson(personId, personType, ownerPOID).ToList().ForEach(u =>
                {
                    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                });
            }
            return lstMdlPropertOwnersWithManager;
        }
    }
}
