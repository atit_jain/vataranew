﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;
using System.Threading.Tasks;
using System.Web;
using System.Data;
using System.Data.Entity;

namespace Vatara.WebAPI.Controllers
{
    public partial class PropertyOwnerMap
    {
        public int ownerid { get; set; }

        public string ownername { get; set; }

        public string ownerphoneno { get; set; }

        public string owneremail { get; set; }

        public bool isprimaryowner { get; set; }

        public bool isdeleted { get; set; }
    }

    public partial class PropertyMap
    {
        public string pId { get; set; }

        public string pNum { get; set; }

        public int pCategory { get; set; }

        public int pType { get; set; }

        public string pName { get; set; }

        public string pMailBoxNo { get; set; }

        public string pAddress { get; set; }

        public int pCityId { get; set; }
        public string pCity { get; set; }

        public int pCountyId { get; set; }
        public string pCounty { get; set; }

        public int pStateId { get; set; }
        public string pState { get; set; }

        public string pZipCode { get; set; }

        public int pCountryId { get; set; }
        public string pCountry { get; set; }

        public string pPhone { get; set; }

        public string pGrossArea { get; set; }

        public string pRentableArea { get; set; }

        public string pParkingSpaces { get; set; }

        public string pUnitNo { get; set; }

        public string pBaseRent { get; set; }

        public int pHOAId { get; set; }

        public int pHoaDate { get; set; }
        public decimal pHoaAmount { get; set; }
        public decimal pManagementFee { get; set; }

        public PropertyOwnerMap[] pOwnerArray { get; set; }

        //public string pOwnerName { get; set; }

        //public string pOwnerPhone { get; set; }

        //public string pOwnerFax { get; set; }

        //public string pOwnerEmail { get; set; }

        public string pDescription { get; set; }

        public string pAirportName { get; set; }

        public string pAirportDistance { get; set; }

        public string pUOM { get; set; }

        public string pListType { get; set; }

        public string pComments { get; set; }

        public string pZoning { get; set; }

        public string pLandAcres { get; set; }

        public string pCustomField1 { get; set; }

        public int StatusId { get; set; }

        public string Status { get; set; }

        public bool pIsHomeWarrantyTaken { get; set; }
        public string pHomeWarrantyCompanyName { get; set; }
        public string pHomeWarrantyCompanyPhoneNo { get; set; }
        public string pHomeWarrantyStartDate { get; set; }
        public string pHomeWarrantyEndDate { get; set; }
        public bool pHomeWarrantySendReminder { get; set; }


        public bool pIsHomeInsuranceTaken { get; set; }
        public string pHomeInsuranceCompanyName { get; set; }
        public string pHomeInsuranceCompanyPhoneNo { get; set; }
        public string pHomeInsuranceStartDate { get; set; }
        public string pHomeInsuranceEndDate { get; set; }
        public bool pHomeInsuranceSendReminder { get; set; }

        public int pManagerId { get; set; }

    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PropertiesController : ApiController
    {
        public IEnumerable<getPropertyByManagerId_Result> GetAllProperty(int id)
        {
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var data = entities.getPropertyByManagerId(id).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        public HttpResponseMessage Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                //var entity = entities.vwProperties.FirstOrDefault(p => p.pId == id);
                var entity = entities.getPropertyByPropertyId(id).FirstOrDefault();
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Property with Id = " + id.ToString() + " Not Found");
                }
            }
        }

        [AllowAnonymous] // delete this later
        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteProperty(int id)
        {
            bool success = false;
            Property delProp = new Property();
            var photoPath = HttpContext.Current.Server.MapPath("/Uploads/Properties/Photos/");
            HttpResponseMessage retValue = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "");
            await Task.Run(() =>
            {
                // delete the property
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            delProp = ve.Properties.FirstOrDefault(p => p.Id == id);
                            delProp.status = ve.Status.FirstOrDefault(s => s.Name.ToUpper() == clsConstants.Inactive.ToUpper() && s.ModuleName == clsConstants.Propertymodule.ToUpper()).Id;
                            delProp.DeletionTime = DateTime.UtcNow;
                            delProp.IsDeleted = true;
                            ve.Entry(delProp).State = EntityState.Modified;
                            ve.Properties.Attach(delProp);                            
                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            retValue = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                        }
                    }
                    retValue = Request.CreateResponse(HttpStatusCode.OK);
                }
            });
            return retValue;
        }

        [HttpPost]
        public ResponseMdl SaveProperty([FromBody] MdlPropertyRegistration data)
        {
            ResponseMdl response = new ResponseMdl();
            clsPropertyHelper objclsPropertyHelper = new clsPropertyHelper();
            int propId = 0;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    foreach (var person in data.lstMdlOwner)
                    {
                        if (person.Id < 1)
                        {
                            person.password = new Encryption().Encrypt(new clsCommon().ReturnRandomAlphaNumericString(8));
                        }
                    }
                    data.OwnerMaster.OwnerType = data.Ownertype;
                    data.OwnerMaster.CreationTime = DateTime.Now;
                    data.OwnerMaster.LastModificationTime = DateTime.Now;
                    propId = objclsPropertyHelper.SaveProperty(data);
                    response.Data = propId;
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.errorPropertyCreation.ToString();
                return response;
            }
            finally
            {
                objclsPropertyHelper = null;
            }
        }

        [HttpPost]
        public ResponseMdl UpdateProperty([FromBody] MdlPropertyRegistration data)
        {
            ResponseMdl response = new ResponseMdl();
            clsPropertyHelper objclsPropertyHelper = new clsPropertyHelper();
            //int propId = 0;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    foreach (var person in data.lstMdlOwner)
                    {
                        if (person.Id < 1)
                        {
                            person.password = new Encryption().Encrypt(new clsCommon().ReturnRandomAlphaNumericString(8));
                        }
                    }
                    data.OwnerMaster.OwnerType = data.Ownertype;
                    //data.OwnerMaster.CreationTime = DateTime.Now;
                    data.OwnerMaster.LastModificationTime = DateTime.UtcNow;
                    response.Data = objclsPropertyHelper.UpdateProperty(data); ;
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.errorPropertyUpdation.ToString();
                return response;
            }
            finally
            {
                objclsPropertyHelper = null;
            }
        }


        [HttpPost]
        public ResponseMdl UpdatePropertyAdditionalInfo([FromBody] MdlPropertyAdditionalInfo data)
        {
            ResponseMdl response = new ResponseMdl();
            clsPropertyHelper objclsPropertyHelper = new clsPropertyHelper();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    response.Data = objclsPropertyHelper.UpdatePropertyAdditionalInfo(data);
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                objclsPropertyHelper = null;
            }
        }


        [HttpGet]
        public ResponseMdl GetPropertyDetailByPropertyId(int PropertyId)
        {
            ResponseMdl response = new ResponseMdl();
            clsPropertyHelper objclsPropertyHelper = new clsPropertyHelper();
            DataSet ds = new DataSet();
            try
            {
                ds = objclsPropertyHelper.GetPropertyDetailById(PropertyId);
                ds.Tables[0].TableName = "tblProperty";
                ds.Tables[1].TableName = "tblOwnerMaster";
                ds.Tables[2].TableName = "tblOwner";
                response.Data = ds;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objclsPropertyHelper = null;
            }
        }

        // SaveHOA

        [HttpPost]
        public ResponseMdl SaveHOA([FromBody] MdlHOA data)
        {
            ResponseMdl response = new ResponseMdl();
            HOA hoa = new HOA();
            MdlHOA objMdlHOA = new MdlHOA();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            hoa = objMdlHOA.GetEntity_HOA(data);
                            hoa.IsDeleted = false;
                            hoa.CreationTime = DateTime.UtcNow;
                            hoa.CreatorUserId = data.creatorId;
                            ve.HOAs.Add(hoa);
                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                hoa = null;
                objMdlHOA = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetHOAByPMID(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlHOA> lstHOA = new List<MdlHOA>();
            MdlHOA objMdlHOA = new MdlHOA();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.HOAs.Where(e => e.PMID == PMID).ToList().ForEach(u =>
                    {
                        lstHOA.Add(objMdlHOA.GetMdl_HOA(u));
                    });
                    response.Status = true;
                    response.Data = lstHOA;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstHOA = null;
                objMdlHOA = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl CheckEmailAvailability(string email)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    isExist = entities.HOAs.Any(e => e.Email.ToLower() == email.ToLower());
                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetPropertiesByPMID(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertyByPMID> lstMdlPropertyByPMID = new List<MdlPropertyByPMID>();
            using (VataraEntities ve = new VataraEntities())
            {
                try
                {
                    ve.getPropertyByPMID(PMID).ToList().ForEach(u =>
                    {
                        lstMdlPropertyByPMID.Add(new MdlPropertyByPMID().GetMdl_PropertyByPMID(u));
                    });
                    response.Status = true;
                    response.Data = lstMdlPropertyByPMID;
                    return response;
                }
                catch (Exception ex)
                {
                    response.Status = false;
                    response.Message = clsConstants.somethingwrong.ToString();
                    return response;
                }
                finally
                {
                    lstMdlPropertyByPMID = null;
                }
            }
        }

        [HttpGet]
        public ResponseMdl GetOwnerByPOID(int POID)
        {
            ResponseMdl response = new ResponseMdl();
            clsPropertyHelper objclsPropertyHelper = new clsPropertyHelper();
            DataTable dt = new DataTable();
            try
            {
                dt = objclsPropertyHelper.GetOwnerByPOID(POID);
                dt.TableName = "tblOwner";
                response.Data = dt;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objclsPropertyHelper = null;
            }
        }
    }
}
