﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class LeasePaymentFrequencyController : ApiController
    {
        public IEnumerable<LeasePaymentFrequency> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.LeasePaymentFrequencies.ToList();
            }
        }

        public LeasePaymentFrequency Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.LeasePaymentFrequencies.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
