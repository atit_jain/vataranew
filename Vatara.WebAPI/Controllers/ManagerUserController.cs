﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;
using Vatara.WebAPI.Class;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class ManagerUserController : ApiController
    {

        //[HttpPost]
        //public ResponseMdl RegisterNewManager(CompanyManager mdlCompanyManager)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    Person entityPerson = new Person();
        //    Address entityAddress = null;
        //    User entityUser = new User();
        //    List<MdlEmailData> lstMdlEmailData = null;
        //    PersonManager PersonManager = new PersonManager();
        //    PropertyManager PropertyManager = new PropertyManager();


        //    using (VataraEntities ve = new VataraEntities())
        //    {
        //        using (var dbcontextTran = ve.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                if (mdlCompanyManager.personId == 0)
        //                {

        //                    bool isExist = ve.People.Any(e => e.Email.ToLower() == mdlCompanyManager.email.ToLower());
        //                    if (isExist)
        //                    {
        //                        response.Status = false;
        //                        response.Message = "Email Already Exists..";
        //                        return response;
        //                    }

        //                    entityUser.Email = mdlCompanyManager.email;
        //                    entityUser.Password = new Encryption().Encrypt(new clsCommon().ReturnRandomAlphaNumericString(8));
        //                    entityUser.IsDeleted = false;
        //                    entityUser.CreationTime = DateTime.Now;
        //                    entityUser.IsVerified = false;
        //                    entityUser.VerificationId = Guid.NewGuid().ToString();
        //                    entityUser.CreatorUserId = mdlCompanyManager.creatorUserId;
        //                    ve.Users.Add(entityUser);
        //                    ve.SaveChanges();

        //                    entityAddress = new CompanyManager().GetEntity_Address(mdlCompanyManager);
        //                    entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.personAddress.ToLower()).Id;
        //                    entityAddress.IsDeleted = false;
        //                    entityAddress.CreationTime = DateTime.Now;
        //                    ve.Addresses.Add(entityAddress);
        //                    ve.SaveChanges();

        //                    entityPerson = new CompanyManager().GetEntity_Person(mdlCompanyManager);
        //                    entityPerson.AddressId = entityAddress.Id;
        //                    entityPerson.UserId = entityUser.Id;
        //                    entityPerson.PersonTypeId = ve.PersonTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.propertyManager.Trim().ToLower()).Id;
        //                    entityPerson.IsDeleted = false;
        //                    entityPerson.CreationTime = DateTime.Now;
        //                    entityPerson.CreatorUserId = mdlCompanyManager.creatorUserId;
        //                    ve.People.Add(entityPerson);
        //                    ve.SaveChanges();

        //                    //PropertyManager.ManagerId = entityPerson.Id;
        //                    //PropertyManager.PMID = ve.PersonManagers.FirstOrDefault(m => m.PersonID == mdlCompanyManager.creatorUserId).PMID;                       
        //                    //PropertyManager.IsActivated = true;
        //                    //PropertyManager.IsDeleted = false;
        //                    //PropertyManager.CreationTime = DateTime.Now;
        //                    //ve.PropertyManagers.Add(PropertyManager);

        //                    PersonManager.PersonID = entityPerson.Id;
        //                    PersonManager.PMID = ve.PersonManagers.FirstOrDefault(m => m.PersonID == mdlCompanyManager.creatorUserId).PMID;
        //                    PersonManager.IsActive = true;
        //                    PersonManager.Role = mdlCompanyManager.userRole;
        //                    PersonManager.CreationTime = DateTime.Now;
        //                    ve.PersonManagers.Add(PersonManager);
        //                }

        //                else
        //                {

        //                    entityAddress = ve.Addresses.FirstOrDefault(a => a.Id == mdlCompanyManager.addressId);
        //                    entityAddress.CountryId = mdlCompanyManager.countryId;
        //                    entityAddress.StateId = mdlCompanyManager.stateId;
        //                    entityAddress.CountyId = mdlCompanyManager.countyId;
        //                    entityAddress.CityId = mdlCompanyManager.cityId;
        //                    entityAddress.Zipcode = mdlCompanyManager.zipcode;
        //                    entityAddress.Line1 = mdlCompanyManager.line1;
        //                    entityAddress.Line2 = mdlCompanyManager.line2;
        //                    ve.Addresses.Attach(entityAddress);
        //                    ve.Entry(entityAddress).State = EntityState.Modified;

        //                    entityPerson = ve.People.FirstOrDefault(p => p.Id == mdlCompanyManager.personId);
        //                    entityPerson.Email = mdlCompanyManager.email;
        //                    entityPerson.FirstName = mdlCompanyManager.firstName;
        //                    entityPerson.MiddleName = mdlCompanyManager.middleName;
        //                    entityPerson.LastName = mdlCompanyManager.lastName;
        //                    entityPerson.PhoneNo = mdlCompanyManager.phoneNo;
        //                    entityPerson.Fax = mdlCompanyManager.fax;
        //                    entityPerson.Image = mdlCompanyManager.image;
        //                    entityPerson.LastModificationTime = DateTime.Now;
        //                    ve.People.Attach(entityPerson);
        //                    ve.Entry(entityPerson).State = EntityState.Modified;

        //                    PersonManager = ve.PersonManagers.FirstOrDefault(pm => pm.PersonID == mdlCompanyManager.personId);
        //                    PersonManager.Role = mdlCompanyManager.userRole;
        //                    //PersonManager.LastModificationTime = DateTime.Now;

        //                    ve.PersonManagers.Attach(PersonManager);
        //                    ve.Entry(PersonManager).State = EntityState.Modified;
        //                }

        //                ve.SaveChanges();
        //                dbcontextTran.Commit();
        //                response.Status = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                dbcontextTran.Rollback();
        //                response.Status = false;
        //                response.Message = clsConstants.somethingwrong.ToString();
        //                return response;
        //            }
        //            finally
        //            {
        //                entityPerson = null;
        //                entityAddress = null;
        //                entityUser = null;
        //                lstMdlEmailData = null;
        //                PersonManager = null;
        //                PropertyManager = null;
        //            }
        //        }
        //    }
        //    return response;
        //}

        //[HttpGet]
        //public ResponseMdl GetAllPropertyByPMID(int PMID)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    List<MdlPropertyByPMID> lstMdlPropertyByPMID = new List<MdlPropertyByPMID>();
        //    using (VataraEntities ve = new VataraEntities())
        //    {
        //        try
        //        {
        //            ve.getPropertyByPMID(PMID).ToList().ForEach(u =>
        //            {
        //                lstMdlPropertyByPMID.Add(new MdlPropertyByPMID().GetMdl_PropertyByPMID(u));
        //            });
        //            response.Status = true;
        //            response.Data = lstMdlPropertyByPMID;
        //            return response;
        //        }
        //        catch (Exception ex)
        //        {
        //            response.Status = false;
        //            response.Message = clsConstants.somethingwrong.ToString();
        //            return response;
        //        }
        //        finally
        //        {
        //            lstMdlPropertyByPMID = null;
        //        }
        //    }
        //}

        [HttpGet]
        public ResponseMdl GetPropertyTenantOwner(int managerId, int propertyId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertyOwnerTenantPerson> lstMdlPropertyOwnerTenantPerson = new List<MdlPropertyOwnerTenantPerson>();
            using (VataraEntities ve = new VataraEntities())
            {
                try
                {
                    ve.getPropertyTenantOwnerPersonByManagerPersonIdAndPropId(managerId, propertyId).ToList().ForEach(u =>
                    {
                        lstMdlPropertyOwnerTenantPerson.Add(new MdlPropertyOwnerTenantPerson().GetMdl_PropertyOwnerTenantPerson(u));
                    });
                    response.Status = true;
                    response.Data = lstMdlPropertyOwnerTenantPerson;
                    return response;
                }
                catch (Exception ex)
                {
                    response.Status = false;
                    response.Message = clsConstants.somethingwrong.ToString();
                    return response;
                }
                finally
                {
                    lstMdlPropertyOwnerTenantPerson = null;
                }
            }
        }

        [HttpPost]
        public ResponseMdl UpdateUserStatus([FromBody] MdlPersonStatus data)
        {
            ResponseMdl response = new ResponseMdl();
            EmailData entityEmailData = new EmailData();
            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        Int32 status = ve.Status.FirstOrDefault(s => s.Name.ToLower() == data.Action.ToLower() && s.ModuleName.ToLower() == clsConstants.Personmodule.ToLower()).Id;
                        var person = ve.People.FirstOrDefault(p => p.Id == data.PersonId);
                        person.LastModificationTime = DateTime.UtcNow;
                        person.LastModifierUserId = data.ModifierPersonId;
                        person.Status = status;
                        ve.People.Attach(person);
                        ve.Entry(person).State = EntityState.Modified;
                        ve.SaveChanges();

                        if (data.Action.ToLower() == clsConstants.PersonStatusActive.ToLower())
                        {
                            entityEmailData.From = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                            entityEmailData.To = data.ToEmail;
                            entityEmailData.IsSent = false;
                            entityEmailData.CreationTime = DateTime.UtcNow;
                            entityEmailData.ReSend = false;
                            entityEmailData.Subject = "Account Activation";
                            entityEmailData.Purpose = "Your account with email is activated.";
                            ve.EmailDatas.Add(entityEmailData);
                            ve.SaveChanges();
                        }
                        dbcontextTran.Commit();
                        response.Status = true;
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                }
            }
            return response;
        }



        [HttpPost]
        public ResponseMdl ResendLoginCredential([FromBody] UserMdl UserMdl)
        {
            ResponseMdl response = new ResponseMdl();
            string fromEmail = string.Empty;
            string toEmail = string.Empty;
            string subject = string.Empty;
            string body = string.Empty;
            Email objEmail = new Email();
            string password = string.Empty;
            User entityUser = new User();
            EmailData EmailData = null;
            string WebsiteUrl = string.Empty;
            string managerCompanyUrl = string.Empty;
            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        entityUser = ve.Users.FirstOrDefault(u => u.Email == UserMdl.Email);
                        if (entityUser != null)
                        {
                            managerCompanyUrl = ve.ManagerMasters.FirstOrDefault(m => m.PMID == UserMdl.PMID).URL;
                            WebsiteUrl = ConfigurationManager.AppSettings["ManagerWebSiteUrl"].ToString();
                            WebsiteUrl = WebsiteUrl.Replace("vatara.biz", managerCompanyUrl+ ".vatara.biz");
                            password = new Encryption().Decrypt(entityUser.Password);
                            EmailData = ve.EmailDatas.FirstOrDefault(e => e.To == UserMdl.Email && e.Subject == clsConstants.PasswordForVataraLogin);
                            if (EmailData == null)
                            {                            
                                fromEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                                toEmail = UserMdl.Email;
                                subject = clsConstants.PasswordForVataraLogin;
                                body = objEmail.CreatemailBodyForNewAccountPassword(password, UserMdl.Email, WebsiteUrl);
                                objEmail.InsertEmailData(ve, fromEmail, toEmail, subject, body, true);
                            }
                            else {
                                EmailData.Purpose = objEmail.CreatemailBodyForNewAccountPassword(password, UserMdl.Email, WebsiteUrl);
                                EmailData.ReSend = true;
                                EmailData.LastModificationTime = DateTime.UtcNow;
                                ve.EmailDatas.Add(EmailData);
                                ve.Entry(EmailData).State = EntityState.Modified;
                                ve.SaveChanges();
                            }                           

                            dbcontextTran.Commit();
                            response.Status = true;
                            return response;
                        }
                        else
                        {
                            response.Status = false;
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {
                        entityUser = null;
                        fromEmail = string.Empty;
                        toEmail = string.Empty;
                        subject = string.Empty;
                        body = string.Empty;
                        objEmail = null;
                    }
                }
            }
        }
    }


    public class UserMdl
    {
        public int PersonId { get; set; }
        public string Email { get; set; }
        public int PMID { get; set; }        
    }
}


