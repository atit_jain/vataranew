﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class AssociationController : ApiController
    {

        [HttpGet]
        public ResponseMdl GetAllAssocians(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlHOA> lstMdlHOA = new List<MdlHOA>();
            MdlHOA objMdlHOA = new MdlHOA();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getAssociationByPMID(PMID).ToList().ForEach(u =>
                    {
                        lstMdlHOA.Add(objMdlHOA.GetMdl_HOA(u));
                    });
                }
                response.Status = true;
                response.Data = lstMdlHOA;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMdlHOA = null;
                objMdlHOA = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetAssociansDetailByAssociansId(int associansId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlHOA> lstMdlHOA = new List<MdlHOA>();
            MdlHOA objMdlHOA = new MdlHOA();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getAssociationById(associansId).ToList().ForEach(u =>
                    {
                        lstMdlHOA.Add(objMdlHOA.GetMdl_HOA(u));
                    });
                }
                response.Status = true;
                response.Data = lstMdlHOA;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMdlHOA = null;
                objMdlHOA = null;
            }
            return response;
        }


        [HttpPost]
        public ResponseMdl CreateNewAssociation([FromBody] MdlAssociationRegistration mdl)
        {
            ResponseMdl response = new ResponseMdl();
            Address entityAddress = null;
            HOA entityHOA = null;           
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            entityAddress = new MdlAddress().GetEntity_Address(mdl.mdlAddress);
                            entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.personAddress.ToLower()).Id;
                            entityAddress.IsDeleted = false;
                            entityAddress.CreationTime = DateTime.UtcNow;
                            entityAddress.CreatorUserId = mdl.mdlAssociation.creatorId;
                            ve.Addresses.Add(entityAddress);
                            ve.SaveChanges();

                            mdl.mdlAssociation.addressId = entityAddress.Id;
                            entityHOA = new MdlHOA().GetEntity_HOA(mdl.mdlAssociation);
                            entityHOA.CreationTime = DateTime.UtcNow;
                            entityHOA.IsDeleted = false;
                            ve.HOAs.Add(entityHOA);                         
                           
                            ve.SaveChanges();
                            dbcontextTran.Commit();                            
                            response.Status = true;
                            response.Message = entityHOA.Id.ToString();
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                entityAddress = null;
                entityHOA = null;               
            }
        }

        [HttpPost]
        public ResponseMdl UpdateAssociation([FromBody] MdlAssociationRegistration mdl)
        {
            ResponseMdl response = new ResponseMdl();
            Address entityAddress = null;
            HOA entityHOA = null;          
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            entityAddress = ve.Addresses.FirstOrDefault(a => a.Id == mdl.mdlAddress.id);
                            entityAddress.Line1 = mdl.mdlAddress.line1;
                            entityAddress.CountryId = mdl.mdlAddress.countryId;
                            entityAddress.StateId = mdl.mdlAddress.stateId;
                            entityAddress.CountyId = mdl.mdlAddress.countyId;
                            entityAddress.CityId = mdl.mdlAddress.cityId;
                            entityAddress.Zipcode = mdl.mdlAddress.zipcode;
                            entityAddress.LastModificationTime = DateTime.UtcNow;
                            entityAddress.LastModifierUserId = mdl.mdlAssociation.creatorId;
                            ve.Addresses.Attach(entityAddress);
                            ve.Entry(entityAddress).State = EntityState.Modified;

                            entityHOA = ve.HOAs.FirstOrDefault(v => v.Id == mdl.mdlAssociation.id);
                            entityHOA.HOAName = mdl.mdlAssociation.hoaName;
                            entityHOA.PersonFirstName = mdl.mdlAssociation.personFirstName;
                            entityHOA.PersonLastName = mdl.mdlAssociation.personLastName;
                            entityHOA.Email = mdl.mdlAssociation.email;
                            entityHOA.Phone = mdl.mdlAssociation.phone;
                            entityHOA.Fax = mdl.mdlAssociation.fax;
                            entityHOA.TwitterUrl = mdl.mdlAssociation.twitterUrl;
                            entityHOA.FbUrl = mdl.mdlAssociation.fbUrl;
                            entityHOA.LastModificationTime = DateTime.UtcNow;
                            entityHOA.LastModifierUserId = mdl.mdlAssociation.creatorId;
                            ve.HOAs.Attach(entityHOA);
                            ve.Entry(entityHOA).State = EntityState.Modified;
                            ve.SaveChanges();                            
                            dbcontextTran.Commit();
                            response.Status = true;
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                entityAddress = null;
                entityHOA = null;                
            }
        }


        [HttpGet]
        public ResponseMdl CheckEmailExist(string email, int associationId)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {                  
                    if (associationId == 0)
                    {
                        isExist = entities.HOAs.Any(e => e.Email.ToLower() == email.ToLower());
                    }
                    else
                    {
                        isExist = entities.HOAs.Any(e => e.Email.ToLower() == email.ToLower() && e.Id != associationId);
                    }


                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }
    }
}
