﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class AgreementTypeController : ApiController
    {
        public IEnumerable<AgreementType> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.AgreementTypes.ToList();
            }
        }

        public AgreementType Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.AgreementTypes.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
