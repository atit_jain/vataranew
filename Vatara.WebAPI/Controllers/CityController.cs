﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class CityController : ApiController
    {
        public IEnumerable<City> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Cities.ToList();
            }
        }

        public City Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Cities.FirstOrDefault(p => p.Id == id);
            }
        }

        public IEnumerable<City> GetCity(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Cities.Where(p => p.CountyId == id).ToList();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseMdl GetCityByState(int stateId)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    List<MdlCity> cities = new List<MdlCity>();
                    entities.vwCities.Where(e => e.StateId == stateId).Select(x => new { x.Id, x.Name }).ToList().ForEach(u =>
                    {
                        cities.Add(new MdlCity { id = u.Id, name = u.Name });
                    });
                    response.Status = true;
                    response.Data = cities;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        [HttpGet]       
        public ResponseMdl GetCityByCounty(int id)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {                   
                   var cities = entities.Cities.Where(p => p.CountyId == id).ToList();
                    response.Status = true;
                    response.Data = cities;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }



        [HttpGet]
        public ResponseMdl GetCityByStateOrCounty(int StateId,int CountyId)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var cities = entities.getCityByStateOrCounty(StateId, CountyId).ToList();
                    response.Status = true;
                    response.Data = cities;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

    }
}
