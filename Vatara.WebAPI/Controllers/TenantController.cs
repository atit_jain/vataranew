﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class TenantController : ApiController
    {

        [HttpGet]
        public ResponseMdl GetTenantsByPMID(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlTenantsByPMID> lstMdlTenantsByPMID = new List<MdlTenantsByPMID>();
            MdlTenantsByPMID mdlTenantsByPMID = new MdlTenantsByPMID(); ;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getAllTenantsByPMID(PMID).ToList().ForEach(u =>
                    {
                        lstMdlTenantsByPMID.Add(mdlTenantsByPMID.Get_MdlTenantsByPMID(u));
                    });
                    response.Status = true;
                    response.Data = lstMdlTenantsByPMID;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                mdlTenantsByPMID = null;
                lstMdlTenantsByPMID = null;
            }
            return response;
        }

        [HttpGet]
        //[AllowAnonymous]        
        public ResponseMdl TenantDetail(int tenantId)
        {
            ResponseMdl response = new ResponseMdl();                  
            List<MdlEmergencyContact> lstEmergencyContact = new List<MdlEmergencyContact>();         
            MdlTenantInfo MdlTenantInfo = new MdlTenantInfo();
            MdlTenantLeaseData MdlTenantLeaseData = new MdlTenantLeaseData();
            List<MdlPendingRentInvoices> lstPendingRentInvoices = new List<MdlPendingRentInvoices>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.vwEmergencyContacts.Where(e => e.PersonId == tenantId).ToList().ForEach(ec =>
                    {
                        lstEmergencyContact.Add(new MdlEmergencyContact().getMdl_EmergencyContact(ec));
                    });                 

                    MdlTenantInfo.PersonInfo = new MdlPerson().GetMdl_Person(entities.vwPersons.Where(e => e.pId == tenantId).FirstOrDefault());

                    entities.vwLeaseTenants.Where(e => e.Tenantid == tenantId).ToList().ForEach(u =>
                    {
                        MdlTenantLeaseData = new MdlTenantLeaseData();
                        MdlTenantLeaseData.lstEmergencyContact = lstEmergencyContact;

                        entities.vwGetPendingRentInvoices.Where(e => e.To == tenantId && e.IntLeaseID == u.pLeaseId).ToList().ForEach(pr =>
                        {
                            MdlTenantLeaseData.lstPendingRentInvoices.Add(new MdlPendingRentInvoices().GetMdl_PendingRentInvoices(pr));
                        });

                        entities.vwOccupants.Where(e => e.LeaseId == u.pLeaseId).ToList().ForEach(occ =>
                        {
                            MdlTenantLeaseData.lstOccupant.Add(new MdlOccupant().GetMdl_Occupant(occ));
                        });

                        entities.vwVehicles.Where(e => e.LeaseId == u.pLeaseId).ToList().ForEach(vh =>
                        {
                            MdlTenantLeaseData.lstVehicle.Add(new MdlVehicle().GetMdl_Vehicle(vh));
                        });

                        entities.vwPetInfoes.Where(e => e.LeaseId == u.pLeaseId).ToList().ForEach(p =>
                        {
                            MdlTenantLeaseData.lstPetInfo.Add(new MdlPetInfo().GetMdl_PetInfo(p));
                        });
                        
                        entities.vwReferences.Where(e => e.LeaseId == u.pLeaseId).ToList().ForEach(rf =>
                        {
                            MdlTenantLeaseData.lstReference.Add(new MdlReference().GetMdl_Reference(rf));
                        });

                        entities.vwLeases.Where(e => e.pId == u.pLeaseId).ToList().ForEach(tl =>
                        {
                            MdlTenantLeaseData.lstTenantLease.Add(new MdlTenantLease().GetMdl_TenantLease(tl));                            
                        });

                        MdlTenantInfo.TenantLeaseData.Add(MdlTenantLeaseData);                        
                    });                 
                    response.Status = true;
                    response.Data = MdlTenantInfo;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {               
                lstEmergencyContact = null;
                MdlTenantInfo = null;
                MdlTenantLeaseData = null;
               // lstPendingRentInvoices = null;
            }
        }


        [HttpGet]
        public ResponseMdl GetInvoicePaymentHistory(int invoiceId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlPaymentDetail MdlPaymentDetail = new MdlPaymentDetail();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.vwBills.Where(e => e.InvoiceNumber == invoiceId).ToList().ForEach(u =>
                    {
                        MdlPaymentDetail.lstBills.Add(new MdlBill().GetMdl_Bill(u));
                    });

                    entities.vwPaymentModes.ToList().ForEach(u =>
                    {
                        MdlPaymentDetail.lstPaymentMode.Add(new MdlPaymentMode().GetMdl_PaymentMode(u));
                    });

                    MdlPaymentDetail.PaidAmount = MdlPaymentDetail.lstBills.Sum(o => o.amount);
                    MdlPaymentDetail.TotalAmount = entities.InvoiceDetails.Where(o => o.InvoiceNumber == invoiceId).Sum(o => o.TotalAmount);
                    response.Status = true;
                    response.Data = MdlPaymentDetail;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlPaymentDetail = null;
            }
            return response;
        }


        [HttpPost]
        public ResponseMdl UpdatetenantDetail([FromBody] MdlTenantData data)
        {
            ResponseMdl response = new ResponseMdl();
            Person objPerson = new Person();
            Address objAddress = new Address();           
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            objPerson = ve.People.FirstOrDefault(p => p.Id == data.mdlTenatData.pId);
                            objPerson.FirstName = data.mdlTenatData.pFirstName;
                            objPerson.LastName = data.mdlTenatData.pLastName;
                            objPerson.PhoneNo = data.mdlTenatData.pPhoneNo;
                            objPerson.LastModificationTime = DateTime.UtcNow;
                            objPerson.LastModifierUserId = data.mdlTenatData.CreatorUserId;
                            ve.People.Attach(objPerson);
                            ve.Entry(objPerson).State = EntityState.Modified;

                            objAddress = ve.Addresses.FirstOrDefault(a => a.Id == data.mdlTenatData.pAddressId);
                            objAddress.Line1 = data.mdlAddress.Line1;
                            objAddress.Line2 = data.mdlAddress.Line2;
                            objAddress.CityId = data.mdlAddress.CityId;
                            objAddress.CountyId = data.mdlAddress.CountyId;
                            objAddress.StateId = data.mdlAddress.StateId;
                            objAddress.CountryId = data.mdlAddress.CountryId;
                            objAddress.Zipcode = data.mdlAddress.Zipcode;
                            ve.Addresses.Attach(objAddress);
                            ve.Entry(objAddress).State = EntityState.Modified;

                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Data = objPerson;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                 objPerson = null;
                 objAddress = null;
            }
            // return response;
        }


    }

    public class MdlTenantData
    {
        public MdlPerson mdlTenatData { get; set; }
        public Address mdlAddress { get; set; }        
    }

}
