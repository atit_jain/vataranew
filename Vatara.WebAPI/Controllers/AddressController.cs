﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class AddressController : ApiController
    {
        public IEnumerable<Address> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Addresses.ToList();
            }
        }

        public Address Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Addresses.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
