﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public partial class VendorServiceCategoryClass
    {
        //public int pId { get; set; }

        public int ServiceCategoryId { get; set; }
    }

    public partial class PersonMap
    {
        public int pId { get; set; } //Person Id (Unique Id)

        public int pTypeId { get; set; } //Person Type Id

        public int pServiceCategoryId { get; set; }

        public string pCompanyName { get; set; }

        public string pFirstName { get; set; }

        public string pLastName { get; set; }

        public int pStatusId { get; set; }

        public string pMailBoxNo { get; set; }

        public int pAddressId { get; set; }

        public string pAddress { get; set; }

        public int pCountryId { get; set; }

        public int pStateId { get; set; }

        public int pCityId { get; set; }

        public int pCountyId { get; set; }

        public string pZipcode { get; set; }

        public string pEmail { get; set; }

        public string pPhoneNo { get; set; }

        public string pFaxNo { get; set; }

        public string pTwitterUrl { get; set; }

        public string pFBUrl { get; set; }

        public int[] pVendorServiceCategoriesArray { get; set; }

        public int pPropertyId { get; set; }
    }

    public class PersonProfile
    {
        public vwPerson Person { get; set; }

        public List<vwProperty> OwnedProperties { get; set; }

        public List<vwLease> OwnedLeases { get; set; }

        public List<vwProperty> TenantLeases { get; set; }

    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PersonController : ApiController
    {
        public IEnumerable<vwPerson> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.vwPersons.ToList();
            }
        }

        public vwPerson Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                PersonProfile pp = new PersonProfile();
                //vwPerson personInfo = entities.vwPerson.FirstOrDefault(p => p.pId == id);
                return entities.vwPersons.FirstOrDefault(p => p.pId == id);


                //var LambdaQuery = entities.vwProperty.Join(entities.vwPropertyOwners, e => e.pId, d => d.pPropertyId, (e, d) => new {
                //    e.ID,
                //    e.Name,
                //    d.DepartmentName
                //});

                //List<vwProperty> ownedProperties = entities.vwPropertyOwners.Where(p => p.ownerid == id);
            }
        }

        public HttpResponseMessage Post([FromBody] PersonMap personmap)
        {
            Person person = new Person();
            User user = new DataAccess.User();
            DateTime dtNow = DateTime.UtcNow;
            int pId = 0;
            Address address = new Address();
            try
            {                
                using (VataraEntities ve = new VataraEntities())
                {
                    person.Id = 0;
                    person.CreationTime = dtNow;
                    person.IsDeleted = false;

                    if (personmap.pId != 0)                    
                    {
                        pId = personmap.pId;
                        var prop = ve.People.FirstOrDefault(p => p.Id == pId);
                        if (prop != null)
                            person = prop as Person;
                    }
                    else
                    {
                        person.PersonTypeId = personmap.pTypeId;
                        person.FirstName = personmap.pFirstName;
                        person.LastName = personmap.pLastName;
                        person.ServiceCategoryId = personmap.pServiceCategoryId;
                        person.CompanyName = personmap.pCompanyName;
                        person.Status = personmap.pStatusId;
                        person.Email = personmap.pEmail;
                        person.PhoneNo = personmap.pPhoneNo;
                        person.Fax = personmap.pFaxNo;
                        person.TwitterUrl = personmap.pTwitterUrl;
                        person.FacebookUrl = personmap.pFBUrl;
                        address = ve.Addresses.FirstOrDefault(p => p.Id == personmap.pAddressId);
                        address.CreationTime = DateTime.UtcNow;
                        ve.Entry(address).State = System.Data.Entity.EntityState.Added;                        
                    }                 
                    
                    
                    int personid = 0;
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {                                                                                  
                            if (person.Id == 0)
                            {
                                ve.Addresses.Add(address);
                                ve.SaveChanges();
                                user.Email = person.Email;
                                user.IsDeleted = false;
                                user.CreationTime = DateTime.UtcNow;
                                if (personmap.pTypeId == (int)clsCommon.PersonType.Tenant)
                                {
                                    user.IsVerified = true;
                                    person.Status = ve.Status.FirstOrDefault(s => s.Name.ToUpper() == clsConstants.PersonStatusActive.ToUpper() && s.ModuleName.ToUpper() == clsConstants.Personmodule).Id;
                                }
                                user.Password = new Encryption().Encrypt(new clsCommon().ReturnRandomAlphaNumericString(8));
                                ve.Users.Add(user);
                                ve.SaveChanges();
                                person.AddressId = address.Id;
                                person.UserId = user.Id;
                                ve.People.Add(person);
                            }
                                
                            ve.SaveChanges();
                            personid = person.Id;

                            if (person.PersonTypeId == (int)clsCommon.PersonType.Vendor)
                            {
                                //int sc = 0;
                                //for (int i = 0; i < personmap.pVendorServiceCategoriesArray.Length; i++)
                                //{
                                //    VendorServiceCategory vsc = new VendorServiceCategory();
                                //    object servcatg = new object();
                                //    sc = personmap.pVendorServiceCategoriesArray[i];
                                //    if (sc > 0)
                                //    {
                                //        servcatg = ve.VendorServiceCategories.Where(p => p.PersonId == personid && p.ServiceCategoryId == sc).FirstOrDefault();
                                //        if (servcatg != null)
                                //            vsc = servcatg as VendorServiceCategory;
                                //    }

                                //    if (servcatg == null)
                                //    {
                                //        vsc.PersonId = personid;
                                //        vsc.ServiceCategoryId = personmap.pVendorServiceCategoriesArray[i];
                                //        vsc.Id = 0;
                                //        vsc.CreationTime = dtNow;
                                //        vsc.IsDeleted = false;

                                //        ve.VendorServiceCategories.Add(vsc);
                                //        ve.SaveChanges();
                                //        //lease.L = lease.Id;
                                //    }
                                //}
                            }

                            dbcontextTran.Commit();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            dbcontextTran.Rollback();
                            //throw (ex);
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                        }                        
                        var message = Request.CreateResponse(HttpStatusCode.Created, person);
                        message.Headers.Location = new Uri(Request.RequestUri + "/" + person.Id.ToString());
                        return message;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                //throw (ex);
            }
            return new HttpResponseMessage();
        }
        
        public ResponseMdl GetTenantByManagerId(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlGetTenantInfo> lstTenant = null;
            MdlGetTenantInfo mdlGetTenantInfo = new MdlGetTenantInfo();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lstTenant = new List<MdlGetTenantInfo>();
                    entities.vmGetTenantInfoes.Where(t => t.PMID == managerId).ToList().ForEach(u =>
                    {
                        lstTenant.Add(mdlGetTenantInfo.GetTenantInfoMdl(u));
                    }); 
                    response.Status = true;
                    response.Data = lstTenant;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstTenant = null;
                mdlGetTenantInfo = null;
            }
            return response;
        }
        
    }
}
