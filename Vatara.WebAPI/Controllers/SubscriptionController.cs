﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;
using Vatara.WebAPI.Models;
using ZohoSubscription;
using static ZohoSubscription.PlanApi;
using static ZohoSubscription.SubscriptionApi;

namespace Vatara.WebAPI.Controllers
{
    public class SubscriptionController : ApiController
    {
        PlanApi plan = new PlanApi();
        SubscriptionApi SubScibe = new SubscriptionApi();

        public ResponseMdl GetSubscriptionInfo(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlSubscriptionDetail MdlSubscriptionDetail = null;
            MdlSubscriptionDetail objMdlSubscriptionDetail = new MdlSubscriptionDetail(); ;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    MdlSubscriptionDetail = objMdlSubscriptionDetail.GetMdl_SubscriptionDetail(entities.getSubscriptionDetail(managerId).FirstOrDefault());
                    response.Status = true;
                    response.Data = MdlSubscriptionDetail;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlSubscriptionDetail = null;
                objMdlSubscriptionDetail = null;
            }
            return response;
        }

        public ResponseMdl GetSubscriptionTypes()
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlSubscriptionType> lstMdlSubscriptionType = new List<MdlSubscriptionType>();
            MdlSubscriptionType objMdlSubscriptionType = new MdlSubscriptionType(); ;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.SubscriptionTypes.ToList().ForEach(u =>
                    {
                        lstMdlSubscriptionType.Add(objMdlSubscriptionType.GetMdl_SubscriptionType(u));
                    });
                    response.Status = true;
                    response.Data = lstMdlSubscriptionType;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstMdlSubscriptionType = null;
                objMdlSubscriptionType = null;
            }
            return response;
        }

        public ResponseMdl GetSubscriptionPlan()
        {
            ResponseMdl response = new ResponseMdl();
            List<PlanApi.Plan> lstplan = new List<PlanApi.Plan>();
            try
            {
                lstplan = plan.GetPlans();
                response.Data = lstplan;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                response = null;
                lstplan = null;
            }
        }

        public ResponseMdl GetCustomerProfile(string userName)
        {
            ResponseMdl response = new ResponseMdl();
            CustomerApi.Customer mdlcustomer = new CustomerApi.Customer();
            CustomerApi customer = new CustomerApi();
            try
            {
                mdlcustomer = customer.GetCustomers(userName);
                response.Data = mdlcustomer;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                response = null;
                mdlcustomer = null;
                customer = null;
            }
        }

        public ResponseMdl SaveSubScription_withTrial(MdlSubScriptionPlan mdlSubScriptionPlan)
        {
            ResponseMdl response = new ResponseMdl();
            SubScriptionPlan Subscriptionplan = new SubScriptionPlan();
            mdlSubPlan Subplan = new mdlSubPlan();
            MdlPlan Splan = new MdlPlan();
            CardPlan card = new CardPlan();
            string[] strarr = new string[3];
            SubscriptionApi.Subscription lstsubscription = new SubscriptionApi.Subscription();
            try
            {
                Subscriptionplan.customer_id = mdlSubScriptionPlan.customerId;
                Subscriptionplan.status = "FUTURE";
                
                //Plan
                Subplan.plan_code = mdlSubScriptionPlan.plan.plan_code;
                Subplan.name = mdlSubScriptionPlan.plan.name;
                Subplan.price = mdlSubScriptionPlan.amount;
                Subplan.quantity = 1;
                Subscriptionplan.plan = Subplan;

                Subscriptionplan.starts_at = GetCurrentDate(DateTime.Now);
                Subscriptionplan.auto_collect = mdlSubScriptionPlan.auto_collect;

                var SubScription = SubScibe.Create(Subscriptionplan);

                if (SubScription != null)
                {
                    ZohoSubScription zohoSubscription = new ZohoSubScription();

                    using (VataraEntities ve = new VataraEntities())
                    {
                        var preZohosubscription = ve.ZohoSubScriptions.Where(m => m.CustomerId == SubScription.customer.customer_id && m.IsDeleted == false).FirstOrDefault();
                        if (preZohosubscription != null)
                        { preZohosubscription.IsDeleted = true; }

                        zohoSubscription.ActivatedAt = DateTime.Now;
                        zohoSubscription.Name = mdlSubScriptionPlan.plan.name;
                        zohoSubscription.Amount = mdlSubScriptionPlan.amount;
                        zohoSubscription.AutoCollect = mdlSubScriptionPlan.auto_collect;
                        zohoSubscription.CreationTime = DateTime.Now;
                        zohoSubscription.CreationUserId = mdlSubScriptionPlan.customer.PMID;
                        zohoSubscription.CurrencyCode = mdlSubScriptionPlan.customer.CurrencyCode;
                        zohoSubscription.CurrencySymbol = mdlSubScriptionPlan.customer.CurrencyCode;
                        zohoSubscription.CustomerId = mdlSubScriptionPlan.customer.CustomerId;
                        zohoSubscription.ExchangeRate = 1;

                        zohoSubscription.ExpiresAt = DateTime.Now.AddMonths(1);
                        zohoSubscription.Interval = mdlSubScriptionPlan.plan.interval.ToString();
                        zohoSubscription.IntervalUnit = SubScription.interval_unit;
                        zohoSubscription.IsDeleted = false;
                        zohoSubscription.Plan_Code = mdlSubScriptionPlan.planCode;
                        zohoSubscription.ProductId = mdlSubScriptionPlan.product_id;
                        zohoSubscription.SubScriptionId = SubScription.subscription_id;

                        ve.ZohoSubScriptions.Add(zohoSubscription);
                        ve.SaveChanges();
                    }
                }
                response.Data = SubScription;
                response.Status = true;
                return response;
            }

            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                response = null;
                Subscriptionplan = null;
                Subplan = null;
                Splan = null;
                card = null;
            }
        }

        public ResponseMdl SaveSubScription_withCard(MdlSubScriptionPlan_withCard mdlSubScriptionPlan)
        {
            ResponseMdl response = new ResponseMdl();
            SubScriptionPlan_withCard Subscriptionplan = new SubScriptionPlan_withCard();
            mdlSubPlan Subplan = new mdlSubPlan();
            MdlPlan Splan = new MdlPlan();
            CardPlan card = new CardPlan();
            string[] strarr = new string[3];
            DateTime? startdate;
            ZohoSubScription zohoSubscription = new ZohoSubScription();
            SubscriptionApi.Subscription lstsubscription = new SubscriptionApi.Subscription();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    var subscription = ve.ZohoSubScriptions.Where(ZS => ZS.CustomerId == mdlSubScriptionPlan.customerId).OrderByDescending(ZS => ZS.Id).FirstOrDefault();
                    if (subscription != null)
                    {
                        startdate = subscription.ExpiresAt;
                        zohoSubscription.ExpiresAt = startdate.Value.AddMonths(1);
                    }
                    else
                    {
                        startdate = DateTime.Now;
                        zohoSubscription.ExpiresAt = startdate.Value.AddMonths(1).AddDays(-1);
                    }
                }
                Subscriptionplan.customer_id = mdlSubScriptionPlan.customerId;
                Subscriptionplan.status = "Live";

                //Card Detail
                    card.customer_id = mdlSubScriptionPlan.customerId;
                    card.cvv_number = mdlSubScriptionPlan.mdlCard.cvv_number;
                    card.expiry_month = mdlSubScriptionPlan.mdlCard.expiry_month;
                    card.expiry_year = mdlSubScriptionPlan.mdlCard.expiry_year;
                    card.first_name = mdlSubScriptionPlan.mdlCard.first_name;
                    card.last4 = mdlSubScriptionPlan.mdlCard.last4;
                    card.last_name = mdlSubScriptionPlan.mdlCard.last_name;
                    card.card_number = mdlSubScriptionPlan.mdlCard.card_number;
                    card.card_type = "Visa";
                    card.street = "Chelsea and Clinton ";
                    card.state = "New York";
                    card.city = "New York";
                    card.country = "USA";
                    card.status = "valid";
                    card.gateway = "Test Gateway";
                    card.zip = 10018;
                    Subscriptionplan.card = card;

                //Plan
                Subplan.plan_code = mdlSubScriptionPlan.plan.plan_code;
                Subplan.name = mdlSubScriptionPlan.plan.name;
                Subplan.price = mdlSubScriptionPlan.amount;
                Subplan.quantity = 1;
                Subscriptionplan.plan = Subplan;

                Subscriptionplan.starts_at = GetCurrentDate(startdate.Value);
                Subscriptionplan.auto_collect = mdlSubScriptionPlan.auto_collect;

                var SubScription = SubScibe.CreatewithCard(Subscriptionplan);

                if (SubScription != null)
                {
                    

                    using (VataraEntities ve = new VataraEntities())
                    {
                        var preZohosubscription = ve.ZohoSubScriptions.Where(m => m.CustomerId == SubScription.customer.customer_id && m.IsDeleted == false).FirstOrDefault();
                        if (preZohosubscription != null)
                        { preZohosubscription.IsDeleted = true; }

                        zohoSubscription.ActivatedAt = startdate.Value.AddDays(1);
                        zohoSubscription.Name = mdlSubScriptionPlan.plan.name;
                        zohoSubscription.Amount = mdlSubScriptionPlan.amount;
                        zohoSubscription.AutoCollect = mdlSubScriptionPlan.auto_collect;
                        zohoSubscription.CreationTime = DateTime.Now;
                        zohoSubscription.CreationUserId = mdlSubScriptionPlan.customer.PMID;
                        zohoSubscription.CurrencyCode = mdlSubScriptionPlan.customer.CurrencyCode;
                        zohoSubscription.CurrencySymbol = mdlSubScriptionPlan.customer.CurrencyCode;
                        zohoSubscription.CustomerId = mdlSubScriptionPlan.customer.CustomerId;
                        zohoSubscription.ExchangeRate = 1;
                        
                        
                        zohoSubscription.Interval = mdlSubScriptionPlan.plan.interval.ToString();
                        zohoSubscription.IntervalUnit = SubScription.interval_unit;
                        zohoSubscription.IsDeleted = false;
                        zohoSubscription.Plan_Code = mdlSubScriptionPlan.planCode;
                        zohoSubscription.ProductId = mdlSubScriptionPlan.product_id;
                        zohoSubscription.SubScriptionId = SubScription.subscription_id;

                        ve.ZohoSubScriptions.Add(zohoSubscription);
                        ve.SaveChanges();
                    }
                }
                response.Data = SubScription;
                response.Status = true;
                return response;
            }

            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                response = null;
                Subscriptionplan = null;
                Subplan = null;
                Splan = null;
                card = null;
            }
        }


        //[HttpGet]
        //public ResponseMdl SavePayment(string SubScriptionId, string CustomerId, int Amount, string PaymentMode)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    Payment payment = new Payment();
        //    List<CustomField> lstcustomfield = new List<CustomField>();
        //    CustomField customfield = new CustomField();

        //    try
        //    {
        //        payment.customer_id = CustomerId;
        //        payment.payment_mode = PaymentMode;
        //        payment.amount = Convert.ToDouble(Amount);
        //        payment.date = GetCurrentDate();
        //        payment.exchange_rate = 1;

        //        customfield.label = "";
        //        customfield.value = "";
        //        lstcustomfield.Add(customfield);

        //        payment.custom_fields = lstcustomfield;
        //        var list = SubScibe.Subscriptionpayment(payment);
        //        response.Message = "Success";
        //        response.Status = true;
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Message = ex.ToString();
        //        response.Status = false;
        //        return response;
        //    }
        //    finally
        //    {
        //        response = null;
        //    }
        //}

        public ResponseMdl GetCustomerSubscriptionHistory(int pmid)
        {
            ResponseMdl response = new ResponseMdl();
            MdlSubscriptionHistory objHistory = new MdlSubscriptionHistory();
            List<MdlSubscriptionHistory> lstsubscriptionhistory = new List<MdlSubscriptionHistory>();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ve.GetSubscriptionHistory(pmid).ToList().ForEach(u =>
                     {
                         lstsubscriptionhistory.Add(objHistory.GetMdl_SubscriptionHistory(u));
                     });
                }
                response.Data = lstsubscriptionhistory;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Status = false;
                return response;
            }
            finally
            {
                response = null;
                objHistory = null;
                lstsubscriptionhistory = null;
            }
        }

        [HttpGet]
        public ResponseMdl UpdateSubscription(MdlSubscriptionPayment Payment)
        {
            ResponseMdl response = new ResponseMdl();
            RequestSubscription Subscriptionplan = new RequestSubscription();
            SubscriptionApi.Plan mdlplan = new SubscriptionApi.Plan();
            ZohoSubScription zohoSubScription = new ZohoSubScription();
            try
            {
                mdlplan.plan_code = Payment.PlanCode;
                Subscriptionplan.plan = mdlplan;
                Subscriptionplan.auto_collect = Payment.AutoRenewal;
                
                var data = SubScibe.Update(Payment.SubscriptionId, Subscriptionplan);
                if (data.code == 0)
                {
                    using (VataraEntities ve = new VataraEntities())
                    {
                        zohoSubScription = ve.ZohoSubScriptions.Where(ZS => ZS.SubScriptionId == Payment.SubscriptionId).FirstOrDefault();
                        zohoSubScription.AutoCollect = false;
                        ve.SaveChanges();
                    }
                    response.Status = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                return response;
            }
            finally
            {
                response = null;
                zohoSubScription = null;
                mdlplan = null;
                Subscriptionplan = null;
            }
        }

        [HttpGet]
        public ResponseMdl CancelSubscription(string SubscriptionId, string PlanCode)
        {
            ResponseMdl response = new ResponseMdl();
            ZohoSubScription zohoSubScription = new ZohoSubScription();
            try
            {
                var data = SubScibe.Cancel(SubscriptionId, true);
                if (data.code == 0 || data.code==107209)
                {
                    using (VataraEntities ve = new VataraEntities())
                    {
                        zohoSubScription = ve.ZohoSubScriptions.Where(ZS => ZS.SubScriptionId == SubscriptionId).FirstOrDefault();
                        zohoSubScription.AutoCollect = false;
                        zohoSubScription.IsDeleted = true;
                        ve.SaveChanges();
                    }
                    response.Status = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                return response;
            }
            finally
            {
                response = null;
                zohoSubScription = null;
            }
        }

        public string GetCurrentDate(DateTime value)
        {
            string CurrentDate = string.Empty;
            string Month = value.Month.ToString();
            string Day = value.Day.ToString();
            if (Convert.ToInt16(Day) <= 9)
            {
                Day = "0" + Day;
            }
            if (Convert.ToInt16(Month) <= 9)
            {
                Month = "0" + Month;
            }
            CurrentDate = value.Year + "-" + Month + "-" + Day;
            return CurrentDate;
        }
        
    }

    public class MdlSubScriptionPlan
    {
        public string subscriptionid { get; set; }
        public string planCode { get; set; }
        public string subscription_id { get; set; }
        public string name { get; set; }
        public int amount { get; set; }
        public string product_id { get; set; }
        public string CardID { get; set; }
        public string customerId { get; set; }
        public string CurrencyCode { get; set; }
        public string IntervalUnit { get; set; }
        public bool auto_collect { get; set; }
        public MdlPlan plan { get; set; }
     //   public MdlCard mdlCard { get; set; }
        public ZohoProfile customer { get; set; }
        public string status { get; set; }
    }

    public class MdlSubScriptionPlan_withCard
    {
        public string subscriptionid { get; set; }
        public string planCode { get; set; }
        public string subscription_id { get; set; }
        public string name { get; set; }
        public int amount { get; set; }
        public string product_id { get; set; }
        public string CardID { get; set; }
        public string customerId { get; set; }
        public string CurrencyCode { get; set; }
        public string IntervalUnit { get; set; }
        public bool auto_collect { get; set; }
        public MdlPlan plan { get; set; }
        public MdlCard mdlCard { get; set; }
        public ZohoProfile customer { get; set; }
        public string status { get; set; }
    }

    public class MdlPlan
    {
        public string plan_code { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public int quantity { get; set; }
        public int interval { get; set; }
    }

    public class MdlCard
    {
        public string customer_id { get; set; }
        public string status { get; set; }
        public string gateway { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string iin { get; set; }
        public string last4 { get; set; }
        public string card_type { get; set; }
        public int expiry_month { get; set; }
        public int expiry_year { get; set; }
        public string masked_number { get; set; }
        public string card_number { get; set; }
        public string cvv_number { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public int zip { get; set; }
        public string country { get; set; }

        public MdlCard()
        {
            customer_id = "B51XvFOKykyM84r7";
            status = "valid";
            gateway = "paypal";
            first_name = "John";
            last_name = "Wayne";
            iin = "411111";
            last4 = "1111";
            card_type = "MasterCard";
            expiry_month = 10;
            expiry_year = 2014;
            masked_number = "4111111111111111";
            city = "pune";
            street = "pune";
            state = "maharashtra";
            zip = 2043921;
            country = "India";
        }
    }

    public class MdlSubscriptionPayment
    {
        public string PaymentMode { get; set; }
        public string FirstName { get; set; }
        public string CVV { get; set; }
        public string ExpireDate { get; set; }
        public string LastDigits { get; set; }
        public bool AutoRenewal { get; set; }
        public string SubscriptionId { get; set; }
        public string PlanCode { get; set; }
    }
}