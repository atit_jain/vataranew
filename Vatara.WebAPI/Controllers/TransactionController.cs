﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class TransactionController : ApiController
    {
        [HttpGet]
        public ResponseMdl GetTransactionByManager(int managerId, bool isManager, DateTime fromDate, DateTime toDate, int pmid, int personId, int propoertyId, int personType,int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            MdlTransactionAndChartData MdlTransactionAndChartData = new MdlTransactionAndChartData();
            MdlIncomeExpenseLiabilityChartData objMdlIncomeExpenseLiabilityChartData = new MdlIncomeExpenseLiabilityChartData();
            MdlManagerLiabilityToOwner mdlManagerLiabilityToOwner = new MdlManagerLiabilityToOwner();
            MdlManagerLiabilityToTenant mdlManagerLiabilityToTenant = new MdlManagerLiabilityToTenant();
            MdlTransactionByLedgerTable mdlTransactionByLedgerTable = new MdlTransactionByLedgerTable();
            MdlIncomeChartData mdlIncomeChartData = new MdlIncomeChartData();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getTransactionByLedgerTable(fromDate, toDate, managerId, personId, propoertyId, pmid, ownerPOID).ToList().ForEach(u =>
                    {
                        MdlTransactionAndChartData.lstMdlTransactionByLedgerTable.Add(mdlTransactionByLedgerTable.GetMdl_TransactionByLedgerTable(u));
                    });
                    entities.getIncomeChartData(fromDate, toDate, personId, pmid, propoertyId, ownerPOID).ToList().ForEach(u =>
                    {
                        MdlTransactionAndChartData.lstIncomeChartData.Add(mdlIncomeChartData.GetMdl_IncomeChartData(u));
                    });
                    entities.getManagerLiabilityToOwner(toDate, personId, personType, pmid, propoertyId).ToList().ForEach(u =>
                    {
                        MdlTransactionAndChartData.lstMdlManagerLiabilityToOwner.Add(mdlManagerLiabilityToOwner.GetMdl_ManagerLiabilityToOwner(u));
                    });
                    entities.getManagerLiabilityToTenant(toDate, personId, personType, pmid, propoertyId).ToList().ForEach(u =>
                    {
                        MdlTransactionAndChartData.lstMdlManagerLiabilityToTenant.Add(mdlManagerLiabilityToTenant.GetMdl_ManagerLiabilityToTenant(u));
                    });
                    response.Status = true;
                    response.Data = MdlTransactionAndChartData;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlTransactionAndChartData = null;
                objMdlIncomeExpenseLiabilityChartData = null;               
                mdlManagerLiabilityToOwner = null;
                mdlManagerLiabilityToTenant = null;
                mdlTransactionByLedgerTable = null;
                mdlIncomeChartData = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetTransactionByPersonAndProperty(int managerId, bool isManager, int pmid, DateTime fromDate, DateTime toDate, int ownerPOID = 0, int personId = 0, int propoertyId = 0, int personType = 0)
        {
            ResponseMdl response = new ResponseMdl();
            MdlTransactionData MdlTransactionData = new MdlTransactionData();
            MdlIncomeExpenseLiabilityChartData objMdlIncomeExpenseLiabilityChartData = new MdlIncomeExpenseLiabilityChartData();
            MdlManagerLiabilityToOwner mdlManagerLiabilityToOwner = new MdlManagerLiabilityToOwner();
            MdlManagerLiabilityToTenant mdlManagerLiabilityToTenant = new MdlManagerLiabilityToTenant();
            MdlTransactionByLedgerTable mdlTransactionByLedgerTable = new MdlTransactionByLedgerTable();
            MdlIncomeChartData mdlIncomeChartData = new MdlIncomeChartData();
            int personTypeId = personType;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {                   
                    if (personId == 0) /**/
                    {
                        personTypeId = (int)clsCommon.PersonType.PropertyManager;
                    }
                    MdlTransactionData.MdlPropertOwnersWithManager = GetPropertyByPerson(ownerPOID, personId, managerId, personTypeId);
                    entities.getTransactionByLedgerTable(fromDate, toDate, managerId, personId, propoertyId, pmid, ownerPOID).ToList().ForEach(u =>
                    {
                        MdlTransactionData.lstMdlTransactionByLedgerTable.Add(mdlTransactionByLedgerTable.GetMdl_TransactionByLedgerTable(u));
                    });
                    //entities.getIncomeExpenseLiabilityChartData(fromDate, toDate, personId, pmid, propoertyId).ToList().ForEach(u =>
                    //{
                    //    if (u.Amount > 0)
                    //    {
                    //        MdlTransactionData.lstIncomeExpenseLiabilityChartData.Add(objMdlIncomeExpenseLiabilityChartData.GetMdl_IncomeExpenseLiabilityChartData(u));
                    //    }
                    //});

                    entities.getIncomeChartData(fromDate, toDate, personId, pmid, propoertyId, ownerPOID).ToList().ForEach(u =>
                    {
                        MdlTransactionData.lstIncomeChartData.Add(mdlIncomeChartData.GetMdl_IncomeChartData(u));
                    });
                    entities.getManagerLiabilityToOwner(toDate, personId, personType, pmid, propoertyId).ToList().ForEach(u =>
                    {
                        MdlTransactionData.lstMdlManagerLiabilityToOwner.Add( mdlManagerLiabilityToOwner.GetMdl_ManagerLiabilityToOwner(u));
                    });
                    entities.getManagerLiabilityToTenant(toDate, personId, personType, pmid, propoertyId).ToList().ForEach(u =>
                    {
                        MdlTransactionData.lstMdlManagerLiabilityToTenant.Add(mdlManagerLiabilityToTenant.GetMdl_ManagerLiabilityToTenant(u));
                    });

                    response.Status = true;
                    response.Data = MdlTransactionData;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlTransactionData = null;                             
                objMdlIncomeExpenseLiabilityChartData = null;
                mdlManagerLiabilityToOwner = null;
                mdlManagerLiabilityToTenant = null;
                mdlTransactionByLedgerTable = null;
                mdlIncomeChartData = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetPropertyByPersonId(int ownerPOID, int personId, int managerId, int personType)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> MdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            MdlPropertOwnersWithManager = GetPropertyByPerson(ownerPOID, personId, managerId, personType);
            response.Status = true;
            response.Data = MdlPropertOwnersWithManager;
            return response;
        }


        private List<MdlPropertOwnersWithManager> GetPropertyByPerson(int ownerPOID, int personId, int managerId, int personType)
        {
            List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
           // vwPersonWithType PersonWithType = new vwPersonWithType();
            using (VataraEntities entities = new VataraEntities())
            {
                entities.getPropertiesByPerson(personId, personType, ownerPOID).ToList().ForEach(u =>
                {
                    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                });               
            }
            return lstMdlPropertOwnersWithManager;
        }

        [HttpGet]
        public ResponseMdl PropertOwnersByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyOwnerTenantByCompanyID(managerId).Select(e => new { e.OwnerId, e.OwnerFullName, e.PropertyId, e.PropertyName, e.PersonTypeId, e.Owner_Address }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId,ownerFullNameForFilter = u.OwnerFullName, ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByManagerAndOwner(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByTenant_Owner(int PMID, int personId, int propoertyId, int personType)  //Dont change managerId variable name
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyByOwnerPersonId(personId).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                     {
                         PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = personType });
                     });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetOwnerTransaction(int managerId, bool isManager, DateTime fromDate, DateTime toDate, int pmid, int personId, int propoertyId, int personType,int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            MdlTransactionByLedgerTable mdlTransactionByLedgerTable = new MdlTransactionByLedgerTable();
            MdlTransactionAndChartData MdlTransactionAndChartData = new MdlTransactionAndChartData();
            MdlIncomeExpenseLiabilityChartData objMdlIncomeExpenseLiabilityChartData = new MdlIncomeExpenseLiabilityChartData();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    #region Old Code            
                    //entities.getTransactionWithWriteOff(fromDate, toDate, managerId, personId, propoertyId, pmid).ToList().ForEach(u =>
                    //{
                    //    mdl = new MdlTransactionWithWriteOff().GetMdl_TransactionWithWriteOff(u);
                    //    if (u.From == personId)
                    //    {
                    //        mdl.payType = clsConstants.Receive.ToLower(); //"Receive";
                    //        mdl.name = mdl.toName;
                    //    }
                    //    else
                    //    {
                    //        mdl.payType = clsConstants.Payment.ToLower(); // "Payment";
                    //        mdl.name = mdl.fromName;
                    //    }
                    //    MdlTransactionAndChartData.lstTransactionWithWriteOff.Add(mdl);
                    //});
                    #endregion

                    entities.getTransactionByLedgerTableForOwnerTenant(fromDate, toDate, personId, propoertyId, pmid, ownerPOID).ToList().ForEach(u =>
                    {
                        MdlTransactionAndChartData.lstMdlTransactionByLedgerTable.Add(mdlTransactionByLedgerTable.GetMdl_TransactionByLedgerTable(u));
                    });

                    if (personType != (int)clsCommon.PersonType.Tenant)
                    {
                        entities.getIncomeExpenseLiabilityChartDataForOwner(fromDate, toDate, personId, pmid, propoertyId).ToList().ForEach(u =>
                        {
                            MdlTransactionAndChartData.lstIncomeExpenseLiabilityChartData.Add(objMdlIncomeExpenseLiabilityChartData.GetMdl_IncomeExpenseLiabilityChartData(u));
                        });
                        entities.getOwnerLiability(toDate, personId, pmid, propoertyId).ToList().ForEach(u =>
                        {
                            MdlTransactionAndChartData.lstMdlManagerLiabilityToOwner.Add(new MdlManagerLiabilityToOwner().GetMdl_ManagerLiabilityToOwner(u));
                        });
                    }
                    response.Status = true;
                    response.Data = MdlTransactionAndChartData;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlTransactionAndChartData = null;
                objMdlIncomeExpenseLiabilityChartData = null;
                mdlTransactionByLedgerTable = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetOwnerIncomeExpenseByPropertyIdLeaseId( int PropoertyId, int LeaseId)
        {
            ResponseMdl response = new ResponseMdl();
            // List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerIncomeExpenseByPropertyIdLeaseId = new List<WebAPI.MdlOwnerIncomeExpenseByPropertyIdLeaseId>();
            MdlOwnerIncomeExpenseListByPropertyIdLeaseId mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = new MdlOwnerIncomeExpenseListByPropertyIdLeaseId();
             MdlOwnerIncomeExpenseByPropertyIdLeaseId mdlOwnerIncomeExpenseByPropertyIdLeaseId = new MdlOwnerIncomeExpenseByPropertyIdLeaseId();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getOwnerIncomeExpenseByPropertyIdLeaseId(PropoertyId, LeaseId).ToList().ForEach(u =>
                    {
                        if (u.Type.ToLower() == "income")
                        {
                            mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId.totalIncomeMTD += (decimal)u.MTD;
                            mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId.totalIncomeYTD += (decimal)u.YTD;
                            mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId.lstMdlOwnerIncomeByPropertyIdLeaseId.Add(mdlOwnerIncomeExpenseByPropertyIdLeaseId.GetMdl_OwnerIncomeExpenseByPropertyIdLeaseId(u));
                        }
                        else
                        {
                            mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId.totalExpenseMTD += (decimal)u.MTD;
                            mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId.totalExpenseYTD += (decimal)u.YTD;
                            mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId.lstMdlOwnerExpenseByPropertyIdLeaseId.Add(mdlOwnerIncomeExpenseByPropertyIdLeaseId.GetMdl_OwnerIncomeExpenseByPropertyIdLeaseId(u));
                        }                        
                    });
                    response.Status = true;
                    response.Data = mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                mdlOwnerIncomeExpenseByPropertyIdLeaseId = null;
                mdlMdlOwnerIncomeExpenseListByPropertyIdLeaseId = null;
            }
            return response;
        }

    }

    public class MdlTransactionData
    {
        public MdlTransactionData()
        {
            lstMdlTransactionWithWriteOff = new List<MdlTransactionWithWriteOff>();
            MdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            //lstIncomeExpenseLiabilityChartData = new List<MdlIncomeExpenseLiabilityChartData>();
            lstMdlManagerLiabilityToOwner = new List<MdlManagerLiabilityToOwner>();
            lstMdlManagerLiabilityToTenant = new List<MdlManagerLiabilityToTenant>();
            lstMdlTransactionByLedgerTable = new List<MdlTransactionByLedgerTable>();
            lstIncomeChartData = new List<MdlIncomeChartData>();
        }
        public List<MdlTransactionWithWriteOff> lstMdlTransactionWithWriteOff { get; set; }
        public List<MdlPropertOwnersWithManager> MdlPropertOwnersWithManager { get; set; }
      //  public List<MdlIncomeExpenseLiabilityChartData> lstIncomeExpenseLiabilityChartData { get; set; }

        public List<MdlIncomeChartData> lstIncomeChartData { get; set; }
        public List<MdlManagerLiabilityToOwner> lstMdlManagerLiabilityToOwner { get; set; }
        public List<MdlManagerLiabilityToTenant> lstMdlManagerLiabilityToTenant { get; set; }
        public List<MdlTransactionByLedgerTable> lstMdlTransactionByLedgerTable { get; set; }

    }
}
