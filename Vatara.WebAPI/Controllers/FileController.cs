﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vatara.DataAccess;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using System.IO;

namespace Vatara.WebAPI.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    //public class AttachmentsController : ApiController
    //{

    //    protected string FileType = string.Empty;
    //    protected IList<string> AllowedExtns = new List<string>();
    //    protected string FileSavePath = string.Empty;
    //    protected int MaxContentLength = 1024 * 1024 * 6; //Size = 6 MB  

    //    public AttachmentsController()
    //    {
    //        //this.FileSavePath = "\\Uploads\\Properties\\Documents\\";
    //        //this.FileType = "Documents";
    //        //this.AllowedExtns = new List<string> { ".doc", ".docx", ".txt", ".xls", ".xlsx", ".pdf", ".jpg", ".gif", ".png" };


    //    }
    //    //public FileController(string fileType, IList<string> allowedExtns, string fileSavePath)
    //    //{
    //    //    this.FileType = fileType;
    //    //    this.AllowedExtns = allowedExtns;
    //    //    this.FileSavePath = fileSavePath;
    //    //}        

    //    [AllowAnonymous]  // remove this when you implement the authentication
    //    //[HttpGet]
    //    public HttpResponseMessage Get(int LinkId)
    //    {
    //        HttpResponseMessage msg = new HttpResponseMessage();
    //        using (VataraEntities entities = new VataraEntities())
    //        {
    //            var files = entities.File.Where(f => f.LinkId == LinkId && f.FileType == this.FileType).ToList();
    //            if (files != null)
    //            {
    //                msg = Request.CreateResponse(HttpStatusCode.OK, files);

    //            }
    //        }
    //        return msg;
    //    }



    //    [AllowAnonymous]  // remove this when you implement the authentication
    //    //[HttpPost]
    //    public async Task<HttpResponseMessage> Post()
    //    {
    //        var httpRequest = System.Web.HttpContext.Current.Request;
    //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
    //        await Task.Run(() =>
    //        {
    //            DateTime dtNow = DateTime.Now;
    //            Dictionary<string, object> dict = new Dictionary<string, object>();
    //            try
    //            {
    //                for (int i = 0; i < httpRequest.Files.Count; i++)
    //                {
    //                    response = Request.CreateResponse(HttpStatusCode.Created);

    //                    var postedFile = httpRequest.Files[i];
    //                    if (postedFile != null && postedFile.ContentLength > 0)
    //                    {

    //                        //int maxContentLength = 1024 * 1024 * 4; //Size = 4 MB  


    //                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
    //                        var extension = ext.ToLower();
    //                        if (!this.AllowedExtns.Contains(extension))
    //                        {

    //                            var message = string.Format("Please upload valid file types.");

    //                            dict.Add("error" + i.ToString(), message);
    //                            response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
    //                        }
    //                        else if (postedFile.ContentLength > this.MaxContentLength)
    //                        {

    //                            var message = string.Format("Please upload a file upto 4 mb.");

    //                            dict.Add("error" + i.ToString(), message);
    //                            response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
    //                        }
    //                        else
    //                        {

    //                            char[] delimiters = new char[] { '\\' };
    //                            string[] extFilename = postedFile.FileName.Split(delimiters);
    //                            string FileName = string.Empty;
    //                            if (extFilename.Length > 0)
    //                            {
    //                                FileName = extFilename[extFilename.Length - 1];
    //                            }

    //                            string propId = httpRequest.Form.GetValues("propid")[i];
    //                            string FileId = httpRequest.Form.GetValues("filename")[i];

    //                            string fileName = this.FileSavePath + string.Concat(propId, "-", FileId, ".png");

    //                            //var filePath = HttpContext.Current.Server.MapPath(this.FileSavePath + fileName);
    //                            string filePath = HttpRuntime.AppDomainAppPath + fileName;

    //                            postedFile.SaveAs(filePath);

    //                            using (VataraEntities ve = new VataraEntities())
    //                            {
    //                                DataAccess.File fl = new DataAccess.File();
    //                                fl.LinkId = Convert.ToInt32(propId);
    //                                fl.SeqNo = Convert.ToInt32(httpRequest.Form.GetValues("seqno")[i]);
    //                                fl.Name = postedFile.FileName;
    //                                fl.Description = httpRequest.Form.GetValues("filedesc")[i];
    //                                fl.FileSavePath = filePath;
    //                                fl.FileType = this.FileType;
    //                                fl.Tags = httpRequest.Form.GetValues("filetags")[i];
    //                                byte[] fileData = null;
    //                                using (var binaryReader = new BinaryReader(postedFile.InputStream))
    //                                {
    //                                    fileData = binaryReader.ReadBytes(postedFile.ContentLength);
    //                                }
    //                                fl.FileContent = fileData;
    //                                fl.CreationTime = dtNow;
    //                                fl.IsDeleted = false;
    //                                fl.LastModificationTime = dtNow;
    //                                fl.LastModifierUserId = 0;
    //                                ve.File.Add(fl);
    //                                ve.SaveChanges();
    //                            }

    //                            var message1 = string.Format("Files Updated Successfully.");

    //                            dict.Add("success" + i.ToString(), message1);

    //                        }
    //                    }

    //                }
    //                response = Request.CreateResponse(HttpStatusCode.Created, dict); ;
    //                //var res = string.Format("Please Upload a File.");
    //                //dict.Add("error", res);
    //                //return Request.CreateResponse(HttpStatusCode.NotFound, dict);
    //            }
    //            catch (Exception ex)
    //            {
    //                var res = string.Format("Exception in UploadFile: " + ex.ToString());
    //                dict.Add("ErrorHandler", res);
    //                response = Request.CreateResponse(HttpStatusCode.NotFound, dict);
    //            }
    //        });
    //        return response;
    //    }
    //}


    [RoutePrefix("api/personphoto")]
    public class PersonPhotoController : AttachmentsController
    {
        public PersonPhotoController()
        {
            base.FileSavePath = "\\Uploads\\Person\\Photos\\";
            base.FileType = "Photos";
            base.ModuleId = clsConstants.Personmodule;
            base.AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "personid";
        }
    }

    [RoutePrefix("api/propertyphoto")]
    public class PropertyPhotoController : AttachmentsController
    {
        public PropertyPhotoController()
        {
            base.FileSavePath = "\\Uploads\\Properties\\Photos\\";
            base.FileType = "Photos";
            base.ModuleId = clsConstants.Propertymodule;
            base.AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "propid";
        }
    }

    [RoutePrefix("api/companyphoto")]
    public class CompanyPhotoController : AttachmentsController
    {
        public CompanyPhotoController()
        {
            base.FileSavePath = "\\Uploads\\Manager\\Company\\Photos\\";
            base.FileType = "Photos";
            base.ModuleId = clsConstants.ManagerCompModule;
            base.AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "companyId";
        }
    }


    [RoutePrefix("api/companysliderimage")]
    public class CompanySliderImageController : AttachmentsController
    {
        public CompanySliderImageController()
        {
            base.FileSavePath = "\\Uploads\\Manager\\Company\\SliderImages\\";
            base.FileType = "Photos";
            base.ModuleId = clsConstants.CompSliderImageModule;
            base.AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "companyId";
        }
    }

    [RoutePrefix("api/vendorphoto")]
    public class VendorPhotoController : AttachmentsController
    {
        public VendorPhotoController()
        {
            base.FileSavePath = "\\Uploads\\Contacts\\Vendors\\Photos\\";
            base.FileType = "Photos";
            base.ModuleId = clsConstants.VendorModule;
            base.AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "vendorId";
        }
    }


    [RoutePrefix("api/associationphoto")]
    public class AssociationPhotoController : AttachmentsController
    {
        public AssociationPhotoController()
        {
            base.FileSavePath = "\\Uploads\\Contacts\\Association\\Photos\\";
            base.FileType = "Photos";
            base.ModuleId = clsConstants.AssociationModule;
            base.AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "associationId";
        }
    }

    
    [RoutePrefix("api/propertydocument")]
    public class PropertyDocumentController : AttachmentsController
    {
        public PropertyDocumentController()
        {
            base.FileSavePath = "\\Uploads\\Properties\\Documents\\";
            base.FileType = "Documents";
            base.ModuleId = clsConstants.Propertymodule;
            base.AllowedExtns = new List<string> { ".doc", ".docx", ".txt", ".xls", ".xlsx", ".pdf", ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "propid";
        }
    }


    [RoutePrefix("api/workorderdocument")]
    public class WorkorderDocumentController : AttachmentsController
    {
        public WorkorderDocumentController()
        {
            base.FileSavePath = "\\Uploads\\Workorder\\Documents\\";
            base.FileType = "Documents";
            base.ModuleId = clsConstants.WorkorderModule;
            base.AllowedExtns = new List<string> { ".doc", ".docx", ".txt", ".xls", ".xlsx", ".pdf", ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "workorderId";
        }
    }


    [RoutePrefix("api/leasedocument")]
    public class LeaseDocumentController : AttachmentsController
    {
        public LeaseDocumentController()
        {
            base.FileSavePath = "\\Uploads\\Leases\\Documents\\";
            base.FileType = "Documents";
            base.ModuleId = clsConstants.LeaseModule;
            base.AllowedExtns = new List<string> { ".doc", ".docx", ".txt", ".xls", ".xlsx", ".pdf", ".jpeg", ".jpg", ".gif", ".png" };
            base.strLinkId = "propid";
        }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AttachmentsController : ApiController
    {
        protected string FileType = string.Empty;
        protected string ModuleId = string.Empty;
        protected IList<string> AllowedExtns = new List<string>();
        protected string FileSavePath = string.Empty;
        protected int MaxContentLength = 1024 * 1024 * 4; //Size = 6 MB 
        protected string strLinkId = string.Empty;

        public IEnumerable<DataAccess.File> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Files.ToList();
            }
        }

        public HttpResponseMessage Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                //var entity = entities.Files.Where(f => f.LinkId == id && f.FileType == this.FileType && f.IsDeleted == false).ToList();
                var entity = entities.Files.Where(f => f.LinkId == id && f.FileType.ToUpper() == this.FileType.ToUpper() && f.ModuleId.ToUpper() == this.ModuleId.ToUpper() && f.IsDeleted == false).ToList();
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "File with LinkId = " + id.ToString() + " Not Found");
                }
            }
        }

        //public HttpResponseMessage Post()
        //{
        //    Property p = new Property();
        //    return Request.CreateResponse(HttpStatusCode.Created, p);
        //}

        [AllowAnonymous]  // remove this when you implement the authentication
                          //[HttpPost]
        public async Task<HttpResponseMessage> Post()
        {
            var httpRequest = System.Web.HttpContext.Current.Request;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            string linkId = string.Empty;
            string filePath = string.Empty;
            string imageIndex = string.Empty;
            string FileName = string.Empty;
            DateTime dtNow = DateTime.UtcNow;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string imageNo = string.Empty;
            List<MdlFile> lstMdlFile = null;
            MdlFile mdlFile = null;
            clsSaveFileHelper objclsSaveFileHelper = null;
            int intStatus = -1;
            string[,] arrFilePath = null;
            try
            {
                var lstDocument = httpRequest.Form.GetValues("seqno");
                if (lstDocument == null)
                {
                    return response;
                }
                // var data = httpRequest.Form.GetValues("seqNo");
                arrFilePath = new string[lstDocument.Length,2];
                for (int i = 0; i < httpRequest.Files.Count; i++)
                {
                    response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[i];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        //int maxContentLength = 1024 * 1024 * 4; //Size = 4 MB  
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!this.AllowedExtns.Contains(extension))
                        {
                            var message = string.Format("Please upload valid file types.");

                            dict.Add("error" + i.ToString(), message);
                            response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                         }
                        else if (postedFile.ContentLength > this.MaxContentLength)
                        {
                            var message = string.Format("Please upload a file upto 4 mb.");

                            dict.Add("error" + i.ToString(), message);
                            response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            var seqNo = "";

                            if (httpRequest.Form.GetValues("seqNo")[i] != "undefined")
                            {
                                seqNo = httpRequest.Form.GetValues("seqNo")[i];
                            }

                            char[] delimiters = new char[] { '\\' };
                            string[] extFilename = postedFile.FileName.Split(delimiters);

                            if (extFilename.Length > 0)
                            {
                                FileName = extFilename[extFilename.Length - 1];
                            }

                            //  linkId = httpRequest.Form.GetValues("propid")[i];   //commented by ASt
                            linkId = httpRequest.Form.GetValues(strLinkId)[i];             //Added by ASt                

                            if (FileType == "Photos")
                            {
                                imageNo = seqNo;
                                filePath = HttpRuntime.AppDomainAppPath + this.FileSavePath + string.Concat(linkId, "-Image" + imageNo + ".png");
                            }
                            else
                                filePath = HttpRuntime.AppDomainAppPath + this.FileSavePath + string.Concat(linkId, "-", FileName);

                            arrFilePath[i,0] = filePath;
                            arrFilePath[i,1] = FileName;
                            postedFile.SaveAs(filePath);
                            var message1 = string.Format("Files Updated Successfully.");
                            dict.Add("success" + i.ToString(), message1);
                        }
                    }
                }

                using (VataraEntities ve = new VataraEntities())
                {
                    if (lstDocument != null)
                    {
                        lstMdlFile = new List<MdlFile>();
                        objclsSaveFileHelper = new clsSaveFileHelper();
                        for (int i = 0; i < lstDocument.Length; i++)
                        {
                            mdlFile = new MdlFile();
                            mdlFile.linkId = Convert.ToInt32(httpRequest.Form.GetValues(strLinkId)[i]);
                            mdlFile.moduleId = httpRequest.Form.GetValues("moduleid")[i];
                            mdlFile.fileType = this.FileType;
                            mdlFile.name = httpRequest.Form.GetValues("filename")[i];
                            if (mdlFile.moduleId == "PERSON")
                            {
                                mdlFile.seqNo = 1;
                            }
                            else {
                                mdlFile.seqNo = Convert.ToInt32(httpRequest.Form.GetValues("seqNo")[i]);
                            }
                            mdlFile.description = httpRequest.Form.GetValues("filedesc")[i];
                            for (int j = 0; j < lstDocument.Length; j++)
                            {
                                if (arrFilePath[j, 1] == mdlFile.name)
                                {
                                    mdlFile.fileSavePath = arrFilePath[j, 0];
                                }
                                if (FileType == "Photos")
                                {
                                    mdlFile.fileSavePath= arrFilePath[j, 0];
                                }
                            }
                                
                            mdlFile.tags = httpRequest.Form.GetValues("filetags")[i];
                            // mdlFile.lastModifierUserId = Convert.ToInt32(httpRequest.Form.GetValues("creatorUserId")[i]);
                            if (httpRequest.Form.GetValues("idDeleted") != null)
                            {
                                mdlFile.isDeleted = Convert.ToBoolean(httpRequest.Form.GetValues("idDeleted")[i]);
                            }
                            //var PreMdlFile = ve.Files.Where(f => f.Name == mdlFile.name && f.LinkId == mdlFile.linkId && f.ModuleId == mdlFile.moduleId && f.FileType == mdlFile.fileType).SingleOrDefault();

                            //if (PreMdlFile != null)
                            //{
                            //    PreMdlFile.Description = mdlFile.description;
                            //    PreMdlFile.Tags = mdlFile.tags;
                            //}
                            //else
                            //{ }

                             lstMdlFile.Add(mdlFile);
                            

                            #region Old code
                            //DataAccess.File fl = new DataAccess.File();                           
                            //fl.LinkId = Convert.ToInt32(httpRequest.Form.GetValues(strLinkId)[i]);
                            //fl.ModuleId = httpRequest.Form.GetValues("moduleid")[i];
                            //fl.FileType = this.FileType;
                            //fl.Name = httpRequest.Form.GetValues("filename")[i];

                            //Vatara.DataAccess.File fileV = ve.Files.FirstOrDefault(p => p.LinkId == fl.LinkId && p.ModuleId == fl.ModuleId && p.FileType == this.FileType && p.Name == fl.Name);
                            //if (fileV != null)
                            //{
                            //    fl = fileV;
                            //}
                            //fl.SeqNo = Convert.ToInt32(httpRequest.Form.GetValues("seqNo")[i]);
                            //// postedFile.FileName;

                            //fl.Description = httpRequest.Form.GetValues("filedesc")[i];
                            //fl.FileSavePath = filePath;
                            //fl.Tags = httpRequest.Form.GetValues("filetags")[i];

                            //fl.LastModificationTime = dtNow;
                            //fl.LastModifierUserId = 0;
                            //if (httpRequest.Form.GetValues("idDeleted") != null)
                            //    fl.IsDeleted = Convert.ToBoolean(httpRequest.Form.GetValues("idDeleted")[i]);
                            //if (fileV == null)
                            //{
                            //    fl.CreationTime = dtNow;
                            //    fl.IsDeleted = false;
                            //    ve.Files.Add(fl);
                            //}
                            //ve.SaveChanges();
                            #endregion
                        }
                        if (lstDocument.Length > 0)
                        {
                            intStatus = objclsSaveFileHelper.SaveFiles(lstMdlFile);
                        }
                    }
                }
                if (intStatus == 1)
                {
                    response = Request.CreateResponse(HttpStatusCode.Created, dict);                    
                }
                else if (intStatus == 0)
                {
                    dict.Add("ErrorHandler", "error");
                    response = Request.CreateResponse(HttpStatusCode.NotFound, dict);
                }
            }
            catch (Exception ex)
            {
                var res = string.Format("Exception in UploadFile: " + ex.ToString());
                dict.Add("ErrorHandler", res);
                response = Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            finally
            {
                lstMdlFile = null;
                mdlFile = null;
                objclsSaveFileHelper = null;
                arrFilePath = null;
            }
            return response;
        }
        
        public HttpResponseMessage Delete(int id)
        {
            using (VataraEntities ve = new VataraEntities())
            {
                var entity = ve.Files.Where(f => f.LinkId == id && f.FileType.ToUpper() == this.FileType.ToUpper() && f.ModuleId.ToUpper() == this.ModuleId.ToUpper() && f.IsDeleted == false).FirstOrDefault();
                if (entity != null)
                {
                    string filePath = entity.FileSavePath;
                    ve.Files.Remove(entity);
                    if (ve.SaveChanges() == 1)
                    {
                        FileInfo file = new FileInfo(filePath);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                    }
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "File Remove LinkId = " + id.ToString() + " Successfully.");
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "File with LinkId = " + id.ToString() + " Not Found");
                }
            }
        }
    }
}

