﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;
using System.Data.Entity;
using Propay_Manage;
using BusinessLayer;
using System.Configuration;
using Vatara.WebAPI.Class;

namespace Vatara.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class InvoiceController : ApiController
    {
        public class MdlInvoiceData
        {
            public int invoiceId { get; set; }
            public int statusId { get; set; }
            public string deleteCommand { get; set; }
            public string currentStatus { get; set; }
        }

        #region invoices

        //[HttpGet]
        //public ResponseMdl PropertOwnersByManager(int managerId)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
        //    try
        //    {
        //        using (VataraEntities entities = new VataraEntities())
        //        {
        //            entities.vwPropertOwnersWithManagers.Where(e => e.ManagerId == managerId).Select(e => new { e.OwnerId, e.OwnerFullName, e.PropertyId, e.PropertyName, e.PersonTypeId, e.Owner_Address }).Distinct().ToList().ForEach(u =>
        //                   {
        //                       PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
        //                   });

        //            response.Status = true;
        //            response.Data = PropertOwners;
        //            //  return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Status = false;
        //        response.Message = clsConstants.somethingwrong;
        //        return response;
        //    }
        //    finally
        //    {
        //        PropertOwners = null;
        //    }
        //    return response;
        //}

        [HttpGet]
        public ResponseMdl PropertOwnersByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyOwnerTenantByCompanyID(managerId).Select(e => new { e.OwnerId, e.OwnerFullName, e.PropertyId, e.PropertyName, e.PersonTypeId, e.Owner_Address,e.ownerIsCompany,e.personId }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId,ownerFullNameForFilter = u.OwnerFullName, ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId,ownerIsCompany = u.ownerIsCompany, personId = u.personId });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByManagerAndOwner(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                     {
                         PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName,ownerIsCompany = u.ownerIsCompany, personId = u.ownerPersonId });
                     });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByTenant_Owner(int PMID, int personId, int propoertyId, int personType)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyByOwnerPersonId(personId).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = personType });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl PropertByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    ///  ********** below lines are commented by asr, because the view throwes error
                    ///  *******************************************************************************
                    //entities.vwPropertOwnersWithManagers.Where(e => e.ManagerId == managerId).Select(e => new { e.PropertyId, e.PropertyName }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { propertyId = u.PropertyId, propertyName = u.PropertyName });
                    //});


                    response.Status = true;
                    response.Data = PropertOwners;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetAllInvoicesByManager(DateTime StartDate, DateTime EndDate, int ManagerId, int PersonId, int PropertyId, int PersonType, int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
        //    DateTime newToDate = (EndDate.Date).AddDays(1);
            List<MdlGetAllInvoice> AllInvoice = new List<MdlGetAllInvoice>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getInvoiceByDate(StartDate, EndDate, ManagerId, PersonId, PropertyId, PersonType, ownerPOID).ToList().ForEach(u =>
                     {
                         AllInvoice.Add(new MdlGetAllInvoice().GetMdl_GetAllInvoiceUsingproc(u, true));
                     });
                    response.Status = true;
                    response.Data = AllInvoice;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                AllInvoice = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetInvoicesByPersonAndProperty(int managerId, DateTime fromDate, DateTime toDate, int ownerPOID = 0, int personId = 0, int propertyId = 0, int personType = 0)
        {
            ResponseMdl response = new ResponseMdl();
            DateTime newToDate = (toDate.Date).AddDays(1);
            MdlInvoicesData MdlInvoicesData = new MdlInvoicesData();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (propertyId == 0)   /*personId > 0 &&*/
                    {
                        MdlInvoicesData.MdlPropertOwnersWithManager = GetPropertyByPerson(personId, managerId, personType, ownerPOID);
                    }
                    entities.getInvoiceByDate(fromDate, newToDate, managerId, personId, propertyId, personType, ownerPOID).ToList().ForEach(u =>
                    {
                        MdlInvoicesData.lstMdlGetAllInvoice.Add(new MdlGetAllInvoice().GetMdl_GetAllInvoiceUsingproc(u, true));
                    });
                    response.Status = true;
                    response.Data = MdlInvoicesData;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                MdlInvoicesData = null;
            }
            return response;
        }

        private List<MdlPropertOwnersWithManager> GetPropertyByPerson(int personId, int managerId, int personType, int ownerPOID)
        {
            List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            //vwPersonWithType PersonWithType = new vwPersonWithType();
            using (VataraEntities entities = new VataraEntities())
            {
                entities.getPropertiesByPerson(personId, personType, ownerPOID).ToList().ForEach(u =>
                {
                    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                });
            }
            return lstMdlPropertOwnersWithManager;
        }


        [HttpGet]
        public ResponseMdl GetInvoiceWithDetail(int invoiceId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlInvoice_Detail mdlInvoice_Detail = new MdlInvoice_Detail();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.proc_GetInvoiceByID(invoiceId).ToList().ForEach(u =>
                    {
                        mdlInvoice_Detail.invoice = (new MdlInvoice().GetMdl_Invoice(u));
                    });
                    entities.Proc_GetInvoiceDetailByInvoiceID(invoiceId).ToList().ForEach(u =>
                    {
                        mdlInvoice_Detail.lstInvoiceDetail.Add(new MdlInvoiceDetail().GetMdl_InvoiceDetail(u));
                    });
                    response.Status = true;
                    response.Data = mdlInvoice_Detail;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlInvoice_Detail = null;
            }
            return response;
        }

        #endregion

        [HttpGet]
        public ResponseMdl GetInvoicePaymentHistory(int invoiceId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlPaymentDetail MdlPaymentDetail = new MdlPaymentDetail();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.vwBills.Where(e => e.InvoiceNumber == invoiceId).ToList().ForEach(u =>
                  {
                      MdlPaymentDetail.lstBills.Add(new MdlBill().GetMdl_Bill(u));
                  });
                    entities.vwPaymentModes.ToList().ForEach(u =>
                   {
                       MdlPaymentDetail.lstPaymentMode.Add(new MdlPaymentMode().GetMdl_PaymentMode(u));
                   });
                    MdlPaymentDetail.PaidAmount = MdlPaymentDetail.lstBills.Sum(o => o.amount);
                    MdlPaymentDetail.TotalAmount = entities.InvoiceDetails.Where(o => o.InvoiceNumber == invoiceId).Sum(o => o.TotalAmount);
                    response.Status = true;
                    response.Data = MdlPaymentDetail;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlPaymentDetail = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertyWithPropertyType(int id)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlProperty> properties = new List<MdlProperty>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.PropertyWithTypeByManager(id).Select(x => new { x.Id, x.Name }).ToList().ForEach(u =>
                    {
                        properties.Add(new MdlProperty { id = u.Id, name = u.Name });
                    });
                    response.Status = true;
                    response.Data = properties;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                properties = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetCurrentInvoiceNumber(int managerId)
        {
            string invoiceNo = string.Empty;
            string[] arr = new string[2];
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    invoiceNo = (entities.getCurrentInvoiceNumber(managerId).FirstOrDefault());
                    if (!string.IsNullOrEmpty(invoiceNo))
                    {
                        arr = invoiceNo.Split('-');
                        arr[1] = (Convert.ToInt64(arr[1]) + 1).ToString("00000");
                    }
                    response.Data = arr;
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                invoiceNo = string.Empty;
                arr = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertyTenantOwner(int id, bool isManager, int propertyId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertyTenantOwner> PropertyTenantOwner = new List<MdlPropertyTenantOwner>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (isManager)
                    {
                        entities.getPropertyTenantOwner(id, propertyId).Select(x => new { x.PersonId, x.PersonName, x.PersonType, x.PropertyId, x.Email }).Distinct().ToList().ForEach(u =>
                         {
                             PropertyTenantOwner.Add(new MdlPropertyTenantOwner { personId = u.PersonId, PersonName = u.PersonName, PersonType = u.PersonType, PropertyId = u.PropertyId, Email = u.Email });
                         });
                    }
                    else
                    {
                        entities.getPropertyTenantOwner(id, propertyId).Select(x => new { x.PersonId, x.PersonName, x.PersonType, x.PropertyId, x.ManagerId, x.ManagerName, x.Email }).Distinct().ToList().ForEach(u =>
                          {
                              PropertyTenantOwner.Add(new MdlPropertyTenantOwner { personId = u.PersonId, PersonName = u.PersonName, PersonType = u.PersonType, PropertyId = u.PropertyId, ManagerId = Convert.ToInt32(u.ManagerId), ManagerName = u.ManagerName, Email = u.Email });
                          });
                    }
                    response.Status = true;
                    response.Data = PropertyTenantOwner;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertyTenantOwner = null;
            }
            // return response;
        }

        [HttpPost]
        public ResponseMdl SaveInvoice([FromBody] MdlInvoice_Detail data)
        {
            Invoice entityInvoice = null;
            InvoiceDetail entityInvoiceDetail = null;
            ResponseMdl response = new ResponseMdl();
            Status entityStatus = null;
            Int32 detailId = 0;
            ServiceCategory rentSrvCat = null;
            Lease entityLease = null;
            bool IsSendToClient = false;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            if (data.invoice.statusId == 1) //save as draft
                            {
                                entityStatus = ve.Status.AsEnumerable().FirstOrDefault(s => s.ModuleName == clsConstants.invoiceModule.Trim() && s.Name == clsConstants.savedAsDraft.Trim());
                                data.invoice.statusId = entityStatus.Id;
                            }
                            else if (data.invoice.statusId == 2) //send to client
                            {
                                entityStatus = ve.Status.AsEnumerable().FirstOrDefault(s => s.ModuleName == clsConstants.invoiceModule.Trim() && s.Name == clsConstants.sentToClient.Trim());
                                data.invoice.statusId = entityStatus.Id;
                                IsSendToClient = true;
                            }
                            if (data.invoice.id == 0) //create invoice
                            {
                                entityInvoice = new MdlInvoice().GetEntity_Invoice(data.invoice);
                                if (data.invoice.recipientPersonTypeName.ToUpper() == clsConstants.Owner.ToUpper())
                                {
                                    entityInvoice.RecipientPersonType = (int)clsCommon.PersonType.Owner;
                                }
                                else if (data.invoice.recipientPersonTypeName.ToUpper() == clsConstants.Tenant.ToUpper())
                                {
                                    entityInvoice.RecipientPersonType = (int)clsCommon.PersonType.Tenant;
                                }
                                entityInvoice.IsDeleted = false;
                                entityInvoice.CreationTime = DateTime.Now;
                                entityInvoice.IsAutoInv = false;
                                ve.Invoices.Add(entityInvoice);
                            }
                            else //edit invoice
                            {
                                entityInvoice = ve.Invoices.FirstOrDefault(s => s.Id == data.invoice.id);
                                entityInvoice.StatusId = data.invoice.statusId;
                                entityInvoice.Note = data.invoice.note;
                                entityInvoice.LastModificationTime = DateTime.UtcNow;
                                entityInvoice.LastModifierUserId = entityInvoice.From;
                                ve.Invoices.Attach(entityInvoice);
                                ve.Entry(entityInvoice).State = EntityState.Modified;
                            }
                            ve.SaveChanges();
                            rentSrvCat = ve.ServiceCategories.FirstOrDefault(s => s.Name.ToLower() == clsConstants.rentServiceCategory.Trim());
                            for (int i = 0; i < data.lstInvoiceDetail.Count; i++) //Invoice Details 
                            {
                                data.lstInvoiceDetail[i].invoiceNumber = entityInvoice.Id;
                                if (data.lstInvoiceDetail[i].id == 0) //create detail
                                {
                                    entityInvoiceDetail = new MdlInvoiceDetail().GetEntity_InvoiceDetail(data.lstInvoiceDetail[i]);
                                    entityInvoiceDetail.PaidAmount = 0;
                                    entityInvoiceDetail.IsDeleted = false;
                                    entityInvoiceDetail.CreationTime = DateTime.UtcNow;
                                    ve.InvoiceDetails.Add(entityInvoiceDetail);

                                    if (entityInvoiceDetail.CategoryID == rentSrvCat.Id)
                                    {
                                        var entityRes = ve.Leases.Join(ve.LeaseTenants,
                                             le => le.Id,
                                             lt => lt.LeaseId,
                                             (le, lt) => new { leaseId = le.Id, propId = le.PropertyId, tenantId = lt.TenantId }).FirstOrDefault(le => le.propId == entityInvoice.PropertyID && le.tenantId == entityInvoice.To);

                                        if (entityRes != null)
                                        {
                                            entityLease = ve.Leases.FirstOrDefault(l => l.Id == entityRes.leaseId);
                                            entityLease.LastInvoiceDate = DateTime.UtcNow;
                                            ve.Leases.Attach(entityLease);
                                            ve.Entry(entityLease).State = EntityState.Modified;
                                        }
                                    }
                                }
                                else //edit detail row
                                {
                                    detailId = data.lstInvoiceDetail[i].id;
                                    entityInvoiceDetail = ve.InvoiceDetails.FirstOrDefault(e => e.Id == detailId);
                                    if (data.lstInvoiceDetail[i].isDeleted == true)
                                    {
                                        entityInvoiceDetail.DeletionTime = DateTime.UtcNow;
                                        entityInvoiceDetail.DeleterUserId = entityInvoice.From;
                                    }
                                    else
                                    {
                                        entityInvoiceDetail.Amount = data.lstInvoiceDetail[i].amount;
                                        entityInvoiceDetail.Markup = data.lstInvoiceDetail[i].markup;
                                        entityInvoiceDetail.Markup_type = data.lstInvoiceDetail[i].markup_type;
                                        entityInvoiceDetail.Tax = data.lstInvoiceDetail[i].tax;
                                        entityInvoiceDetail.TotalAmount = data.lstInvoiceDetail[i].totalAmount;
                                        entityInvoiceDetail.LastModificationTime = DateTime.UtcNow;
                                        entityInvoiceDetail.LastModifierUserId = entityInvoice.From;
                                        entityInvoiceDetail.WRNumber = data.lstInvoiceDetail[i].wRNumber.ToString();
                                        entityInvoiceDetail.VendorInvRefNo = data.lstInvoiceDetail[i].vendorInvRefNo;
                                        entityInvoiceDetail.Description = data.lstInvoiceDetail[i].description;
                                        entityInvoiceDetail.CategoryID = data.lstInvoiceDetail[i].categoryID;
                                    }
                                    ve.InvoiceDetails.Attach(entityInvoiceDetail);
                                    ve.Entry(entityInvoiceDetail).State = EntityState.Modified;
                                }
                            }
                            ve.SaveChanges();
                            //Insert  Ledger Transaction  || SEND null for billid
                            if (IsSendToClient) //sent to client
                            {
                                ve.sp_SaveLedgerTransaction(entityInvoice.To, entityInvoice.RecipientPersonType, entityInvoice.From, (int)clsCommon.PersonType.PropertyManager, entityInvoice.Id, null, entityInvoice.PropertyID);
                            }
                            dbcontextTran.Commit();
                            response.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            ve.sp_DeleteLedgerTransaction(entityInvoice.Id, null);  // SEND null for billid     
                            response.Status = false;
                            response.Message = clsConstants.errorInvoiceCreation.ToString() + " " + ex.Message;
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                entityInvoice = null;
                entityInvoiceDetail = null;
                entityStatus = null;
                rentSrvCat = null;
                entityLease = null;
            }
            // return response;
        }
        
        [HttpPost]
        public ResponseMdl DeleteInvoice([FromBody] MdlInvoiceData MdlInvoiceData)
        {
            Invoice entityInvoice = null;
            List<InvoiceDetail> invoiceDetail = new List<InvoiceDetail>();
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            entityInvoice = ve.Invoices.FirstOrDefault(e => e.Id == MdlInvoiceData.invoiceId);
                            entityInvoice.StatusId = MdlInvoiceData.statusId;
                            if (MdlInvoiceData.deleteCommand.Trim() == clsConstants.invoiceCancelled.Trim())
                            {
                                entityInvoice.LastModificationTime = DateTime.UtcNow;
                                entityInvoice.LastModifierUserId = entityInvoice.From;
                            }
                            else
                            {
                                entityInvoice.IsDeleted = true;
                                entityInvoice.DeletionTime = DateTime.UtcNow;
                                entityInvoice.DeleterUserId = entityInvoice.From;
                                invoiceDetail = ve.InvoiceDetails.Where(s => s.InvoiceNumber == MdlInvoiceData.invoiceId).ToList();
                                for (int i = 0; i < invoiceDetail.Count; i++)
                                {
                                    invoiceDetail[i].IsDeleted = true;
                                    invoiceDetail[i].DeletionTime = DateTime.UtcNow;
                                    ve.InvoiceDetails.Attach(invoiceDetail[i]);
                                    ve.Entry(invoiceDetail[i]).State = EntityState.Modified;
                                }
                            }
                            ve.Invoices.Attach(entityInvoice);
                            ve.Entry(entityInvoice).State = EntityState.Modified;
                            ve.SaveChanges();
                            ve.sp_DeleteLedgerTransaction(entityInvoice.Id, null);  // SEND null for billid   
                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Message = clsConstants.invoiceDeletedSuccess;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                entityInvoice = null;
                invoiceDetail = null;
            }
        }

        [HttpGet]
        public bool ValidInvoiceNumber(string invoceNumber, int mangerID)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Invoices.Any(e => e.From == mangerID && e.Number.ToLower() == invoceNumber.ToLower());
            }
        }

        [HttpPost]
        public ResponseMdl Payment(MdlPayment mdlPayment)
        {
            ResponseMdl response = new ResponseMdl();
            decimal? remain = 0;
            decimal paidAmount = mdlPayment.amount;
            Vatara.DataAccess.Transaction transaction = null;
            PaymentMode payMode = null;
            Bill bill = new Bill();
            List<InvoiceDetail> invoiceDetails = null;
            Invoice invoce = null;
            decimal paidAmountFordetail = 0;
            Status entityStatus = null;
            TransactionHistory TransactionHistory = null;
            PorpayProfile PorpayProfile = null;
            HostedTransactionInfo hostedTransactionInfo = null;
            RecurringPaymentInstruction recurringPaymentInstraction = null;
            Notification notification = null;
            ServiceCategory srviceCategory = null;
            string strHostedTransaction = string.Empty;
            //bool IsPaymentUsingPropay = false;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            if (mdlPayment.writeOff == true)
                            {
                                ve.proc_WriteOffInvoiceAmount(mdlPayment.invoiceId, mdlPayment.amount, mdlPayment.description, mdlPayment.manageId);
                            }
                            else
                            {
                                payMode = ve.PaymentModes.Where(e => e.Name.ToLower() == mdlPayment.paymentMode.ToLower()).FirstOrDefault();
                                bill = new clsInvoice().FillBillModel(mdlPayment, payMode.Name);
                                ve.Bills.Add(bill);
                                ve.SaveChanges();
                                invoiceDetails = ve.InvoiceDetails.Where(e => e.InvoiceNumber == mdlPayment.invoiceId && e.IsFullPaid != true).ToList();
                                foreach (var invoiceDetail in invoiceDetails)
                                {
                                    if (invoiceDetail.IsFullPaid == false || invoiceDetail.IsFullPaid == null)
                                    {
                                        invoiceDetail.PaidAmount = invoiceDetail.PaidAmount == null ? 0 : invoiceDetail.PaidAmount;
                                        invoiceDetail.LastModificationTime = DateTime.UtcNow;
                                        invoiceDetail.LastModifierUserId = mdlPayment.manageId;
                                        remain = invoiceDetail.TotalAmount - (invoiceDetail.PaidAmount);
                                        if (remain <= paidAmount)
                                        {
                                            if (remain == paidAmount)
                                            {
                                                paidAmountFordetail = paidAmount;
                                                invoiceDetail.PaidAmount = invoiceDetail.PaidAmount + paidAmount;
                                                invoiceDetail.IsFullPaid = true;
                                                invoiceDetail.BillNo = new clsInvoice().invoiceBill(invoiceDetail.BillNo, bill.BillId);
                                                paidAmount = 0;
                                            }
                                            else
                                            {
                                                invoiceDetail.PaidAmount = invoiceDetail.TotalAmount;
                                                invoiceDetail.IsFullPaid = true;
                                                invoiceDetail.BillNo = new clsInvoice().invoiceBill(invoiceDetail.BillNo, bill.BillId);
                                                paidAmountFordetail = Convert.ToDecimal(remain);
                                                paidAmount = paidAmount - Convert.ToDecimal(remain);
                                            }
                                        }
                                        else
                                        {
                                            paidAmountFordetail = paidAmount;
                                            invoiceDetail.PaidAmount = invoiceDetail.PaidAmount + paidAmount;
                                            invoiceDetail.BillNo = new clsInvoice().invoiceBill(invoiceDetail.BillNo, bill.BillId);
                                            paidAmount = 0;
                                        }
                                        ve.InvoiceDetails.Attach(invoiceDetail);
                                        ve.Entry(invoiceDetail).State = EntityState.Modified;
                                        transaction = new clsInvoice().FillTransactionModel(bill, invoiceDetail, payMode, paidAmountFordetail);
                                        ve.Transactions.Add(transaction);

                                        if (paidAmount == 0)
                                        {
                                            break;
                                        }
                                    }
                                }
                                ve.SaveChanges();
                                invoiceDetails = ve.InvoiceDetails.Where(e => e.InvoiceNumber == mdlPayment.invoiceId).ToList();
                                decimal TotalAmount = invoiceDetails.Sum(o => o.TotalAmount);
                                Nullable<decimal> TotalPaidamount = invoiceDetails.Sum(o => o.PaidAmount);
                                invoce = ve.Invoices.First(e => e.Id == mdlPayment.invoiceId);
                                if (TotalAmount == TotalPaidamount)
                                {
                                    entityStatus = ve.Status.AsEnumerable().FirstOrDefault(s => s.ModuleName == clsConstants.invoiceModule.Trim() && s.Name == clsConstants.Paid.Trim());
                                }
                                else if (TotalAmount > TotalPaidamount)
                                {
                                    entityStatus = ve.Status.AsEnumerable().FirstOrDefault(s => s.ModuleName == clsConstants.invoiceModule.Trim() && s.Name == clsConstants.PartialPymt.Trim());
                                }
                                invoce.StatusId = entityStatus.Id;
                                ve.Invoices.Attach(invoce);
                                ve.Entry(invoce).State = EntityState.Modified;
                                TransactionHistory = ve.TransactionHistories.FirstOrDefault(t => t.Id == mdlPayment.transactionHistoryId);
                                if (TransactionHistory != null)
                                {
                                    TransactionHistory.IsPaid = true;
                                    ve.Entry(TransactionHistory).State = EntityState.Modified;
                                    ve.TransactionHistories.Attach(TransactionHistory);
                                }
                                ve.SaveChanges();

                                ///Save Notification
                                var ClientName = ve.vwUserPersons.Where(u => u.Email.ToLower() == mdlPayment.clientEmail.ToLower()).Select(u => u.Person_Name).SingleOrDefault();
                                srviceCategory = new ServiceCategory();
                                srviceCategory = ve.ServiceCategories.Where(S => S.Id == mdlPayment.paymentCategoryId).FirstOrDefault();
                                notification = new Notification();
                                notification.Module = "Payment";
                                notification.IsRead = false;
                                notification.Message = "Payment Receive for Property Id P" + mdlPayment.propertyId +" by "+ ClientName;
                                notification.CreationTime = DateTime.Now;
                                notification.PersonId = mdlPayment.PersonId;
                                notification.PersonTypeId = mdlPayment.RecipientType;
                                notification.PropertyId = mdlPayment.propertyId;
                                if (mdlPayment.RecipientType == 1)
                                    notification.PMID = mdlPayment.clientId;
                                else
                                    notification.PMID = mdlPayment.manageId;
                                ve.Notifications.Add(notification);
                                ve.SaveChanges();

                                // SEND null for InvoiceId
                                if (mdlPayment.clientId != 0)
                                {
                                    ve.sp_SaveLedgerTransaction(mdlPayment.clientId, mdlPayment.RecipientType, mdlPayment.manageId, mdlPayment.PayerType, null, bill.BillId, mdlPayment.propertyId);
                                }
                                else
                                {
                                    ve.sp_SaveLedgerTransaction(bill.From, bill.RecipientType, bill.To, bill.PayerType, null, bill.BillId, bill.PropertyID);
                                }
                            }                            
                            dbcontextTran.Commit();
                            response.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            ve.sp_DeleteLedgerTransaction(null, bill.BillId);  // SEND null for InvoiceId     
                            SendEmailForTransactionEntryFailuar(ve, mdlPayment);
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                transaction = null;
                payMode = null;
                bill = null;
                invoiceDetails = null;
                invoce = null;
                TransactionHistory = null;
                notification = null;
                srviceCategory = null;
            }
        }

        [HttpPost]
        public ResponseMdl Payment_WithoutInvoice(MdlPayment mdlPayment)
        {
            ResponseMdl response = new ResponseMdl();
            Vatara.DataAccess.Transaction transaction = new DataAccess.Transaction();
            PaymentMode payMode = null;
            Bill bill = null;
            PorpayProfile PorpayProfile = null;
            TransactionHistory TransactionHistory = null;
            HostedTransactionInfo hostedTransactionInfo = null;
            RecurringPaymentInstruction recurringPaymentInstraction = null;
            Notification notification = null;
            ServiceCategory srviceCategory = null;
            string strHostedTransaction = string.Empty;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            payMode = ve.PaymentModes.Where(e => e.Name.ToLower() == mdlPayment.paymentMode.ToLower()).FirstOrDefault();
                            bill = new clsInvoice().FillBillModel(mdlPayment, payMode.Name);
                            ve.Bills.Add(bill);
                            ve.SaveChanges();

                            ///Save Notification
                            ///
                            var ClientName = ve.vwUserPersons.Where(u => u.Email.ToLower() == mdlPayment.clientEmail.ToLower()).Select(u => u.Person_Name).SingleOrDefault();
                            srviceCategory = new ServiceCategory();
                            srviceCategory = ve.ServiceCategories.Where(S => S.Id == mdlPayment.paymentCategoryId).FirstOrDefault();
                            notification = new Notification();
                            notification.Module = "Payment";
                            notification.IsRead = false;
                            notification.Message = "Payment Receive for Property Id P" + mdlPayment.propertyId + " by " + ClientName;
                            notification.CreationTime = DateTime.Now;
                            notification.PersonId = mdlPayment.PersonId;
                            notification.PersonTypeId = mdlPayment.RecipientType;
                            notification.PropertyId = mdlPayment.propertyId;
                            if (mdlPayment.RecipientType == 1)
                                notification.PMID = mdlPayment.clientId;
                            else
                                notification.PMID = mdlPayment.manageId;
                            ve.Notifications.Add(notification);
                            ve.SaveChanges();

                            transaction.BillNo = bill.BillId;
                            transaction.InvoiceNo = 0;
                            transaction.TranRefNo = 0;
                            transaction.TranDate = DateTime.UtcNow;
                            transaction.Amount = mdlPayment.amount;
                            transaction.PayMode = payMode.Id;
                            transaction.Category = 0;
                            transaction.IsDeleted = false;
                            transaction.CreationTime = DateTime.UtcNow;
                            transaction.Category = mdlPayment.paymentCategoryId;
                            ve.Transactions.Add(transaction);

                            if (!(clsConstants.CashPayment.ToLower() == mdlPayment.paymentMode.ToLower()))
                            {
                                PorpayProfile = ve.PorpayProfiles.FirstOrDefault(e => e.PayerId == mdlPayment.payerId);
                                PorpayProfile.LastPaymentType = mdlPayment.selectPaymentType;
                                ve.PorpayProfiles.Attach(PorpayProfile);
                                ve.Entry(PorpayProfile).State = EntityState.Modified;

                                TransactionHistory = ve.TransactionHistories.FirstOrDefault(t => t.Id == mdlPayment.transactionHistoryId);
                                TransactionHistory.IsPaid = true;
                                ve.TransactionHistories.Attach(TransactionHistory);
                                ve.Entry(TransactionHistory).State = EntityState.Modified;
                                ve.SaveChanges();

                                if (TransactionHistory.HostedTransactionIdentifier.Trim() != string.Empty)
                                {
                                    strHostedTransaction = TransactionHistory.HostedTransactionIdentifier;
                                    hostedTransactionInfo = new MdlHostedTransactionInfo().GetEntity_HostedTransactionInfo(new PropayTransaction().GetHostedTransactionInfo(strHostedTransaction));
                                    hostedTransactionInfo.TransactionId = transaction.Id;
                                    hostedTransactionInfo.TransactionHistoryId = TransactionHistory.Id;
                                    ve.HostedTransactionInfoes.Add(hostedTransactionInfo);
                                    ve.SaveChanges();
                                    recurringPaymentInstraction = new MdlRecurringPaymentInstruction().GetEntity_RecurringPaymentInstruction(mdlPayment, hostedTransactionInfo);
                                    recurringPaymentInstraction.CreationTime = DateTime.UtcNow;
                                    recurringPaymentInstraction.CreatorUserId = mdlPayment.manageId;
                                    ve.RecurringPaymentInstructions.Add(recurringPaymentInstraction);
                                    ve.SaveChanges();
                                }
                            }
                            else
                            {
                                ve.SaveChanges();
                            }
                            // SEND null for InvoiceId
                            ve.sp_SaveLedgerTransaction(mdlPayment.clientId, mdlPayment.RecipientType, mdlPayment.manageId, mdlPayment.PayerType, null, bill.BillId, mdlPayment.propertyId);
                            dbcontextTran.Commit();
                            response.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            ve.sp_DeleteLedgerTransaction(null, bill.BillId);  // SEND null for InvoiceId  
                            SendEmailForTransactionEntryFailuar(ve, mdlPayment);
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                transaction = null;
                payMode = null;
                bill = null;
                PorpayProfile = null;
                TransactionHistory = null;
                notification = null;
                srviceCategory = null;
            }
        }

        private void SendEmailForTransactionEntryFailuar(VataraEntities ve, MdlPayment mdlPayment)
        {
            List<PayerRecipientDetail> lstPayerRecipientDetail = null;
            PayerRecipientDetail objPayerDetail = null;
            PayerRecipientDetail objReceiverDetail = null;
            string fromEmail = string.Empty;
            string toEmail = string.Empty;
            string subject = string.Empty;
            string body = string.Empty;
            Email objEmail = null;
            MdlTransactionHistory objMdlTransactionHistory = null;

            try
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    lstPayerRecipientDetail = new List<PayerRecipientDetail>();
                    objPayerDetail = new PayerRecipientDetail();
                    objReceiverDetail = new PayerRecipientDetail();
                    objEmail = new Email();
                    ve.getPayerRecipientDetail(mdlPayment.PayerType, mdlPayment.RecipientType, mdlPayment.manageId, mdlPayment.clientId).ToList().ForEach(u =>
                    {
                        lstPayerRecipientDetail.Add(new PayerRecipientDetail().GetMdl_PayerRecipientDetail(u));
                    });
                    objMdlTransactionHistory = new MdlTransactionHistory().GetMdl_TransactionHistory(ve.TransactionHistories.FirstOrDefault(h => h.Id == mdlPayment.transactionHistoryId));
                    objPayerDetail = lstPayerRecipientDetail.FirstOrDefault(p => p.type.ToUpper() == clsConstants.Payer);
                    objReceiverDetail = lstPayerRecipientDetail.FirstOrDefault(p => p.type.ToUpper() == clsConstants.Receiver);
                    fromEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                    toEmail = ConfigurationManager.AppSettings["VataraEmail"].ToString();
                    subject = clsConstants.TransactionEntryintoBill;
                    body = objEmail.CreatEmailBodyForTransactionHistory(objPayerDetail, objReceiverDetail, objMdlTransactionHistory);
                    objEmail.InsertEmailData(ve, fromEmail, toEmail, subject, body, false);
                    dbcontextTran.Commit();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                lstPayerRecipientDetail = null;
                fromEmail = string.Empty;
                toEmail = string.Empty;
                subject = string.Empty;
                body = string.Empty;
                objEmail = null;
                objPayerDetail = null;
                objReceiverDetail = null;
            }
        }

        [HttpPost]
        public ResponseMdl PropayPayment_GetUrl(MdlPayment mdlPayment)
        {
            ResponseMdl response = new ResponseMdl();
            MdlTransactionHistory objMdlTransactionHistory = new MdlTransactionHistory();
            TransactionHistory TransactionHistory = new TransactionHistory();
            Dictionary<string, object> dicData = null;
            try
            {
                dicData = new Dictionary<string, object>();
                if (mdlPayment.merchantProfileId == "0")
                {
                    mdlPayment.merchantProfileId = GetPropayProfileByPersonID(mdlPayment.PersonId);
                }
                if (mdlPayment.merchantProfileId != "")
                {
                    string[] value = new PropayTransaction().PropayTransactionProcess(mdlPayment);
                    if (value[1].ToUpper() == "SUCCESS")
                    {
                        using (VataraEntities ve = new VataraEntities())
                        {
                            using (var dbcontextTran = ve.Database.BeginTransaction())
                            {
                                try
                                {
                                    TransactionHistory = objMdlTransactionHistory.GetEntityTransactionHistory(mdlPayment, value[2]);
                                    ve.TransactionHistories.Add(TransactionHistory);
                                    ve.SaveChanges();
                                    dbcontextTran.Commit();
                                    response.Status = true;
                                }
                                catch (Exception ex)
                                {
                                    dbcontextTran.Rollback();
                                }
                            }
                        }
                    }
                    dicData.Add("transctionResult", value);
                    dicData.Add("TransactionHistoryId", TransactionHistory.Id);
                    response.Data = dicData;
                }
                else
                {
                    response.Data = "Merchant Profile Not Correct.";
                }
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
            }
            finally
            {
                objMdlTransactionHistory = null;
                TransactionHistory = null;
            }
            return response;
        }


        [HttpPost]
        public ResponseMdl PropayPayment_WithCard(MdlPayment mdlPayment)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                PayWithCard.ProcessTransaction(mdlPayment);
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl GetPropayProfile(string EmailId) //From Id
        {
            ResponseMdl response = new ResponseMdl();
            MdlPorpayProfile mdlPropayProfile = new MdlPorpayProfile();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    var proPayProfile = ve.PorpayProfiles.FirstOrDefault(e => e.Email == EmailId);
                    if (proPayProfile != null)
                    {
                        mdlPropayProfile = mdlPropayProfile.GetMdl_PorpayProfile(proPayProfile);
                        response.Data = mdlPropayProfile;
                    }
                    else
                    {
                        response.Data = null;
                    }
                    response.Status = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                mdlPropayProfile = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetAllInvoiceWithTotalAmt(int clientID, int propertyId) //From Id
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlGetInvoice_WithAmount> lstInvoice_WithAmount = new List<MdlGetInvoice_WithAmount>();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ve.vmGetInvoice_WithAmount.Where(e => e.To == clientID && e.PropertyID == propertyId && e.WrtOff == "false"
                    && e.TotalAmount != e.PaidAmount)
                    .ToList().ForEach(u =>
                    {
                        lstInvoice_WithAmount.Add(new MdlGetInvoice_WithAmount().GetMdl_GetAllInvoice_WithAmount(u));
                    });
                    response.Data = lstInvoice_WithAmount;
                    response.Status = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                lstInvoice_WithAmount = null;
            }

        }

        [HttpGet]
        public ResponseMdl GetInvoiceNumberByPropertyAndPerson(int propertyId,int personId, int personType ) //From Id
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlInvoiceNumberByPropertyAndPersonId> lstMdlInvoiceNumberByPropertyAndPersonId = new List<MdlInvoiceNumberByPropertyAndPersonId>();
            MdlInvoiceNumberByPropertyAndPersonId mdlInvoiceNumberByPropertyAndPersonId = new MdlInvoiceNumberByPropertyAndPersonId();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ve.getInvoiceNumberByPropertyAndPersonId(propertyId, personId, personType).ToList().ForEach(u =>
                    {
                        lstMdlInvoiceNumberByPropertyAndPersonId.Add(mdlInvoiceNumberByPropertyAndPersonId.GetMdl_InvoiceNumberByPropertyAndPersonId(u));
                    });
                    response.Data = lstMdlInvoiceNumberByPropertyAndPersonId;
                    response.Status = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                lstMdlInvoiceNumberByPropertyAndPersonId = null;
            }

        }

        public string GetPropayProfileByPersonID(int Id) //From Id
        {
            string marchantprofileId = "";
            MdlPorpayProfile mdlPropayProfile = new MdlPorpayProfile();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    var proPayProfile = ve.PorpayProfiles.FirstOrDefault(e => e.PersonId == Id);
                    if (proPayProfile != null)
                    {
                        mdlPropayProfile = mdlPropayProfile.GetMdl_PorpayProfile(proPayProfile);
                        marchantprofileId = mdlPropayProfile.marchantProfileId;
                    }
                }
                return marchantprofileId;
            }
            catch (Exception ex)
            {
                return marchantprofileId;
            }
            finally
            {
                mdlPropayProfile = null;
            }
        }
    }

    public class MdlInvoicesData
    {
        public MdlInvoicesData()
        {
            lstMdlGetAllInvoice = new List<MdlGetAllInvoice>();
            MdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
        }
        public List<MdlGetAllInvoice> lstMdlGetAllInvoice { get; set; }
        public List<MdlPropertOwnersWithManager> MdlPropertOwnersWithManager { get; set; }
    }
}
