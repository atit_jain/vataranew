﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class BillController : ApiController
    {
        public ResponseMdl GetOwnerListByManager_Bill(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlBill_Owner> lstOwner = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lstOwner = new List<MdlBill_Owner>();
                    entities.getPropertyTenantOwner(managerId, 0).Select(x => new { x.PersonId, x.PersonName }).Distinct().ToList().ForEach(u =>
                    {
                        lstOwner.Add(new MdlBill_Owner { ownerId = u.PersonId, ownerFullName = u.PersonName });
                    });
                    response.Status = true;
                    response.Data = lstOwner;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstOwner = null;
            }
            return response;
        }

        public ResponseMdl GetPropertyListByManager_Bill(int managerId, bool isManager, int personId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlBill_Owner> lstProperty = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lstProperty = new List<MdlBill_Owner>();
                    if (isManager)
                    {
                        entities.getPropertyTenantOwner(managerId, 0).Select(x => new { x.PropertyId, x.PropertyName, x.PersonId }).Distinct().ToList().ForEach(u =>
                          {
                              lstProperty.Add(new MdlBill_Owner { propertyId = u.PropertyId, propertyName = u.PropertyName, ownerId = u.PersonId });

                          });
                    }
                    else
                    {
                        entities.getPropertyTenantOwner(managerId, 0).Where(e => e.PersonId == personId)
                          .Select(x => new { x.PropertyId, x.PropertyName, x.PersonId }).Distinct().ToList().ForEach(u =>
                          {
                              lstProperty.Add(new MdlBill_Owner { propertyId = u.PropertyId, propertyName = u.PropertyName, ownerId = u.PersonId });
                          });
                    }
                    response.Status = true;
                    response.Data = lstProperty;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstProperty = null;
            }
            return response;
        }

        public ResponseMdl GetBill_DateWise(int managerID, int personID, DateTime startDate, DateTime endDate, bool isManager, int propertyID, int personTypeId ,int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlBill_Owner> lstReceiver = null;
            List<MdlBill_Owner> lstPayable = null;
            IDictionary<string, List<MdlBill_Owner>> dict = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lstReceiver = new List<MdlBill_Owner>();
                    lstPayable = new List<MdlBill_Owner>();
                    dict = new Dictionary<string, List<MdlBill_Owner>>();

                    if (isManager)
                    {
                        personTypeId = (int)clsCommon.PersonType.PropertyManager;
                    }
                    var List = entities.getAllBill(startDate, endDate, managerID, personID, propertyID).ToList();
                    foreach (var item in List)
                    {
                        // this seperation of receivable and payable is with respect to PM
                        if (item.RecipientType == personTypeId)
                        {
                            lstReceiver.Add(new MdlBill_Owner().GetMdl(item));
                        }
                        else
                        {
                            lstPayable.Add(new MdlBill_Owner().GetMdl(item));
                        }
                    }
                    dict.Add("lstReceiver", lstReceiver);
                    dict.Add("lstPayable", lstPayable);
                    response.Status = true;
                    response.Data = dict;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstReceiver = null;
                lstPayable = null;
                dict = null;
            }
        }

        public ResponseMdl GetBillById(int BillId, int personTypeId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlBill_Owner mdlBill_Owner = new MdlBill_Owner();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var item = entities.getBillById(BillId).FirstOrDefault();
                    mdlBill_Owner = mdlBill_Owner.GetMdl(item);

                    if (item.RecipientType == personTypeId)
                    {
                        mdlBill_Owner.payerReceiver = "receive";
                    }
                    else {
                        mdlBill_Owner.payerReceiver = "pay";
                    }
                    response.Status = true;
                    response.Data = mdlBill_Owner;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlBill_Owner = null;
            }

        }

        [HttpGet]
        public ResponseMdl PropertOwnersByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId,ownerFullNameForFilter = u.OwnerFullName, ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId, ownerIsCompany = u.ownerIsCompany, personId = u.ownerPersonId });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetBillByPersonAndProperty(int managerId, DateTime startDate, DateTime endDate, int ownerPOID = 0, int personId = 0, int propoertyId = 0, int personType = 0)
        {
            ResponseMdl response = new ResponseMdl();
            MdlBillByPersonAndProperty mdlBillByPersonAndProperty = new MdlBillByPersonAndProperty();
            MdlBill_Owner mdlBill_Owner = new MdlBill_Owner();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (personId > 0)    /* && propoertyId == 0*/
                    {
                        mdlBillByPersonAndProperty.lstMdlPropertOwnersWithManager = GetPropertyByPerson(personId, managerId, personType, ownerPOID);
                    }
                    var List = entities.getBillByPersonIdAndPropertyId(startDate, endDate, managerId, personId, propoertyId, personType, ownerPOID).ToList();
                    foreach (var item in List)
                    {
                        // this seperation of receivable and payable is with respect to PM
                        if (item.RecipientType == personType)
                        {
                            mdlBillByPersonAndProperty.lstPayable.Add(mdlBill_Owner.GetMdl(item));
                        }
                        else
                        {
                            mdlBillByPersonAndProperty.lstReceiver.Add(mdlBill_Owner.GetMdl(item));
                        }
                    }
                    response.Status = true;
                    response.Data = mdlBillByPersonAndProperty;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlBillByPersonAndProperty = null;
                mdlBill_Owner = null;
            }
            return response;
        }

        public ResponseMdl GetBillForOwnerTenantByPersonIdAndPropertyId(int managerId, DateTime startDate, DateTime endDate, int ownerPOID = 0, int personId = 0, int propoertyId = 0, int personType = 0)
        {
            ResponseMdl response = new ResponseMdl();
            MdlBillByPersonAndProperty mdlBillByPersonAndProperty = new MdlBillByPersonAndProperty();
            MdlBill_Owner mdlBill_Owner = new MdlBill_Owner();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (propoertyId == 0)
                    {
                        mdlBillByPersonAndProperty.lstMdlPropertOwnersWithManager = GetPropertyByPerson(personId, managerId, personType, ownerPOID);
                    }
                    var List = entities.getBillByPersonIdAndPropertyId(startDate, endDate, managerId, personId, propoertyId, personType, ownerPOID).ToList();
                    foreach (var item in List)
                    {
                        // this seperation of receivable and payable is with respect to PM
                        if (item.RecipientType == personType)
                        {
                            mdlBillByPersonAndProperty.lstReceiver.Add(mdlBill_Owner.GetMdl(item));
                        }
                        else
                        {
                            mdlBillByPersonAndProperty.lstPayable.Add(mdlBill_Owner.GetMdl(item));
                        }
                    }
                    response.Status = true;
                    response.Data = mdlBillByPersonAndProperty;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlBillByPersonAndProperty = null;
                mdlBill_Owner = null;
            }
            return response;
        }

        private List<MdlPropertOwnersWithManager> GetPropertyByPerson(int personId, int managerId, int personType, int ownerPOID)
        {
            List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            using (VataraEntities entities = new VataraEntities())
            {
                entities.getPropertiesByPerson(personId, personType, ownerPOID).ToList().ForEach(u =>
                {
                    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                });
            }
            return lstMdlPropertOwnersWithManager;
        }

        //private List<MdlPropertOwnersWithManager> GetPropertyByPerson(int personId, int managerId, int personType)
        //{
        //    List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
        //    vwPersonWithType PersonWithType = new vwPersonWithType();
        //    using (VataraEntities entities = new VataraEntities())
        //    {
        //        PersonWithType.Type = clsConstants.propertyManager;
        //        if (personId > 0)
        //        {
        //            PersonWithType = entities.vwPersonWithTypes.FirstOrDefault(e => e.Id == personId);
        //        }
        //        if (PersonWithType.Type.ToLower() == clsConstants.Owner)
        //        {
        //            ///commented By ASR Because  PropManagr is giving error CHECK if This method is in use
        //            /// *********************************************************************************
        //            //entities.vwPropTenantOwnerDetails.GroupBy(p => new { p.PropId, p.PropManagr, p.PropName, p.PropAddress, p.OwnerId }).Select(m => new { m.Key.PropId, m.Key.PropName, m.Key.PropManagr, m.Key.PropAddress, m.Key.OwnerId }).Where(e => e.OwnerId == personId && e.PropManagr == managerId).ToList().ForEach(u =>
        //            //{
        //            //    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
        //            //});
        //        }
        //        else if (PersonWithType.Type.ToLower() == clsConstants.Tenant)
        //        {
        //            //entities.vwPropTenantOwnerDetails.GroupBy(p => new { p.PropId, p.PropManagr, p.PropName, p.PropAddress, p.OwnerId, p.TenantId }).Select(m => new { m.Key.PropId, m.Key.PropName, m.Key.PropManagr, m.Key.PropAddress, m.Key.TenantId }).Where(e => e.TenantId == personId && e.PropManagr == managerId).ToList().ForEach(u =>
        //            //{
        //            //    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
        //            //});
        //        }
        //        else
        //        {
        //            //entities.vwPropTenantOwnerDetails.GroupBy(p => new { p.PropId, p.PropManagr, p.PropName, p.PropAddress }).Select(m => new { m.Key.PropId, m.Key.PropName, m.Key.PropManagr, m.Key.PropAddress }).Where(e => e.PropManagr == managerId).ToList().ForEach(u =>
        //            //{
        //            //    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
        //            //});
        //        }
        //    }
        //    return lstMdlPropertOwnersWithManager;
        //}

    }

    public class MdlBillByPersonAndProperty
    {
        public MdlBillByPersonAndProperty()
        {           
            lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            lstReceiver = new List<MdlBill_Owner>();
            lstPayable = new List<MdlBill_Owner>();
        }

        public List<MdlBill_Owner> lstReceiver { get; set; }
        public List<MdlBill_Owner> lstPayable { get; set; }
        public List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager { get; set; }
    }
}
