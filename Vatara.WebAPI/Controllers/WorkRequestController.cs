﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public partial class WorkrequestMap
    {
        public string workrequestid { get; set; }

        public string workorderid { get; set; }

        public string ponumber { get; set; }

        public string invoicenumber { get; set; }

        public string equipmentid { get; set; }

        public string wrtype { get; set; }

        public string servicecategoryid { get; set; }

        public string vendorname { get; set; }

        public string requestedon { get; set; }

        public string estdateofcompl { get; set; }

        public string nextschedule { get; set; }        

    }    

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WorkrequestController : ApiController
    {
        public IEnumerable<Workrequest> Get()
        {
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    return entities.Workrequests.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        public HttpResponseMessage Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                var entity = entities.Workrequests.FirstOrDefault(p => p.Id == id);
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workrequest with Id = " + id.ToString() + " Not Found");
                }
            }
        }
    }
}
