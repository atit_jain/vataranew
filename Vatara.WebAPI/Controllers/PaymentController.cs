﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class PaymentController : ApiController
    {
        [HttpGet]
        public ResponseMdl GetPropayProfile(string EmailId,int PersonId,int PersonTypeId) //From Id
        {
            ResponseMdl response = new ResponseMdl();
            //MdlPorpayProfile mdlPropayProfile = new MdlPorpayProfile();
            MdlGetPropayProfile mdlPropayProfile = new MdlGetPropayProfile();
            
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    //var proPayProfile = ve.PorpayProfiles.FirstOrDefault(e => e.Email == EmailId);
                    //if (proPayProfile != null)
                    //{
                    //    mdlPropayProfile = mdlPropayProfile.GetMdl_PorpayProfile(proPayProfile);
                    //    response.Data = mdlPropayProfile;
                    //}
                    //else
                    //{
                    //    response.Data = null;
                    //}
                    var proPayProfile = ve.getPropayProfileForMarchantAndPayerId(PersonId, PersonTypeId).FirstOrDefault();
                    if (proPayProfile != null)
                    {
                        mdlPropayProfile = mdlPropayProfile.GetMdl_PorpayProfileForMarchantAndPayerId(proPayProfile);
                        response.Data = mdlPropayProfile;
                    }
                    else
                    {
                        response.Data = null;
                    }                    
                    response.Status = true;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                mdlPropayProfile = null;
            }
        }


        [HttpGet]
        public ResponseMdl PropertByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { propertyId = u.PropertyId, propertyName = u.PropertyName });
                    });

                    //entities.vwPropertOwnersWithManagers.Where(e => e.ManagerId == managerId).Select(e => new { e.PropertyId, e.PropertyName }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { propertyId = u.PropertyId, propertyName = u.PropertyName });
                    //});
                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl PropertByOwner(int ownerId) 
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    ///Below lines commented by asr because view throws error
                    ///*******************************************************
                    //entities.vwPropertOwnersWithManagers.Where(e => e.OwnerId == managerId).Select(e => new { e.PropertyId, e.PropertyName, e.OwnerId, e.PersonTypeId }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
                    //});

                    entities.getPropertyByOwnerPersonId(ownerId).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = (int)clsCommon.PersonType.Owner });
                    });

                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl PropertByTenant(int TenantId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyByTenantId(TenantId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.TenantId, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
                    });

                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertyTenantOwner(int id, bool isManager, int propertyId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertyTenantOwner> PropertyTenantOwner = new List<MdlPropertyTenantOwner>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {

                    if (isManager)
                    {
                        entities.getPropertyTenantOwner(id, propertyId).Select(x => new { x.PersonId, x.PersonName, x.PersonType, x.PropertyId, x.Email,x.PersonTypeId }).Distinct().ToList().ForEach(u =>
                        {
                            PropertyTenantOwner.Add(new MdlPropertyTenantOwner { personId = u.PersonId, PersonName = u.PersonName, PersonType = u.PersonType, PropertyId = u.PropertyId, Email = u.Email, PersonTypeId = (int)u.PersonTypeId });
                        });
                    }
                    else
                    {
                        entities.getPropertyTenantOwner(id, propertyId).Select(x => new { x.PersonId, x.PersonName, x.PersonType, x.PropertyId, x.ManagerId, x.ManagerName, x.Email,x.PersonTypeId }).ToList().ForEach(u =>
                        {
                            PropertyTenantOwner.Add(new MdlPropertyTenantOwner { personId = u.PersonId, PersonName = u.PersonName, PersonType = u.PersonType, PropertyId = u.PropertyId, ManagerId = Convert.ToInt32(u.ManagerId), ManagerName = u.ManagerName, Email = u.Email,PersonTypeId = (int)u.PersonTypeId });
                        });
                    }

                    //if (isManager)
                    //{
                    //    entities.vwPropertyTenantOwners.Where(e => e.ManagerId == id && e.PersonId != id).Select(x => new { x.PersonId, x.PersonName, x.PersonType, x.PropertyId, x.Email }).ToList().ForEach(u =>
                    //    {
                    //        PropertyTenantOwner.Add(new MdlPropertyTenantOwner { personId = u.PersonId, PersonName = u.PersonName, PersonType = u.PersonType, PropertyId = u.PropertyId, Email = u.Email });
                    //    });
                    //}
                    //else
                    //{
                    //    entities.vwPropertyTenantOwners.Where(e => e.PropertyId == propertyId && e.PersonId != id).Select(x => new { x.PersonId, x.PersonName, x.PersonType, x.PropertyId, x.ManagerId, x.ManagerName, x.Email }).ToList().ForEach(u =>
                    //    {
                    //        PropertyTenantOwner.Add(new MdlPropertyTenantOwner { personId = u.PersonId, PersonName = u.PersonName, PersonType = u.PersonType, PropertyId = u.PropertyId, ManagerId = u.ManagerId, ManagerName = u.ManagerName, Email = u.Email });
                    //    });
                    //}

                    response.Status = true;
                    response.Data = PropertyTenantOwner;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertyTenantOwner = null;
            }
            // return response;
        }

       
        
    }
}