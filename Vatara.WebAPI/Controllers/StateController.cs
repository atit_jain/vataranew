﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class StateController : ApiController
    {
        public IEnumerable<State> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.States.ToList();
            }
        }

       
        public State Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.States.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
