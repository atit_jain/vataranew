﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;
using Vatara.WebAPI.Class;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class AddNewUserController : ApiController
    {
        [HttpPost]
        public ResponseMdl RegisterNewManager(CompanyManager mdlCompanyManager)
        {
            ResponseMdl response = new ResponseMdl();
            Person entityPerson = new Person();
            Address entityAddress = null;            
            User entityUser = new User();        
            PersonManager PersonManager = new PersonManager();
            PropertyManager PropertyManager = new PropertyManager();
            string fromEmail = string.Empty;
            string toEmail = string.Empty;
            string subject = string.Empty;
            string body = string.Empty;
            Email objEmail = new Email();
            string password = string.Empty;
            //string WebsiteUrl = string.Empty;
            //string managerCompanyUrl = string.Empty;

            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        if (mdlCompanyManager.personId == 0)
                        {
                          bool  isExist = ve.People.Any(e => e.Email.ToLower() == mdlCompanyManager.email.ToLower() && e.IsDeleted == false);
                            if (isExist)
                            {
                                response.Status = false;
                                response.Message = "Email Already Exists..";
                                return response;
                            }

                            entityUser.Email = mdlCompanyManager.email;
                            password =  new clsCommon().ReturnRandomAlphaNumericString(8);
                            entityUser.Password = new Encryption().Encrypt(password);
                            entityUser.IsDeleted = false;
                            entityUser.CreationTime = DateTime.UtcNow;
                            entityUser.IsVerified = true;
                            entityUser.VerificationId = Guid.NewGuid().ToString();
                            entityUser.CreatorUserId = mdlCompanyManager.creatorUserId;
                            ve.Users.Add(entityUser);
                            ve.SaveChanges();

                            entityAddress = new CompanyManager().GetEntity_Address(mdlCompanyManager);
                            entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.personAddress.ToLower()).Id;
                            entityAddress.IsDeleted = false;
                            entityAddress.CreationTime = DateTime.UtcNow;
                            ve.Addresses.Add(entityAddress);
                            ve.SaveChanges();

                            entityPerson = new CompanyManager().GetEntity_Person(mdlCompanyManager);
                            entityPerson.AddressId = entityAddress.Id;
                            entityPerson.UserId = entityUser.Id;
                            entityPerson.PersonTypeId = ve.PersonTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.propertyManager.Trim().ToLower()).Id;
                            entityPerson.IsDeleted = false;
                            entityPerson.Status = 1;
                            entityPerson.CreationTime = DateTime.UtcNow;
                            entityPerson.CreatorUserId = mdlCompanyManager.creatorUserId;
                            ve.People.Add(entityPerson);
                            ve.SaveChanges();

                            //PropertyManager.ManagerId = entityPerson.Id;
                            //PropertyManager.PMID = ve.PersonManagers.FirstOrDefault(m => m.PersonID == mdlCompanyManager.creatorUserId).PMID;                       
                            //PropertyManager.IsActivated = true;
                            //PropertyManager.IsDeleted = false;
                            //PropertyManager.CreationTime = DateTime.Now;
                            //ve.PropertyManagers.Add(PropertyManager);

                            PersonManager.PersonID = entityPerson.Id;
                            PersonManager.PMID = ve.PersonManagers.FirstOrDefault(m => m.PersonID == mdlCompanyManager.creatorUserId).PMID;
                            PersonManager.IsActive = true;
                            PersonManager.Role = mdlCompanyManager.userRole;
                            PersonManager.CreationTime = DateTime.UtcNow;
                            ve.PersonManagers.Add(PersonManager);

                            fromEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                            toEmail = mdlCompanyManager.email;
                            subject = clsConstants.PasswordForVataraLogin;
                           // var path = HttpRuntime.AppDomainAppPath;
                            body = objEmail.CreatemailBodyForNewAccountPasswordForAdmin(password,mdlCompanyManager.email);
                            objEmail.InsertEmailData(ve, fromEmail, toEmail, subject, body,false);
                        }

                        else {

                            entityAddress = ve.Addresses.FirstOrDefault(a => a.Id == mdlCompanyManager.addressId);
                            entityAddress.CountryId = mdlCompanyManager.countryId;
                            entityAddress.StateId = mdlCompanyManager.stateId;
                            entityAddress.CountyId = mdlCompanyManager.countyId;
                            entityAddress.CityId = mdlCompanyManager.cityId;
                            entityAddress.Zipcode = mdlCompanyManager.zipcode;
                            entityAddress.Line1 = mdlCompanyManager.line1;
                            entityAddress.Line2 = mdlCompanyManager.line2;
                            ve.Addresses.Attach(entityAddress);
                            ve.Entry(entityAddress).State = EntityState.Modified;

                            entityPerson = ve.People.FirstOrDefault(p => p.Id == mdlCompanyManager.personId);
                            entityPerson.Email = mdlCompanyManager.email;
                            entityPerson.FirstName = mdlCompanyManager.firstName;
                            entityPerson.MiddleName = mdlCompanyManager.middleName;
                            entityPerson.LastName = mdlCompanyManager.lastName;
                            entityPerson.PhoneNo = mdlCompanyManager.phoneNo;
                            entityPerson.Fax = mdlCompanyManager.fax;
                            entityPerson.Image = mdlCompanyManager.image;
                            entityPerson.LastModificationTime = DateTime.UtcNow;
                            ve.People.Attach(entityPerson);
                            ve.Entry(entityPerson).State = EntityState.Modified;

                            PersonManager = ve.PersonManagers.FirstOrDefault(pm => pm.PersonID == mdlCompanyManager.personId);
                            PersonManager.Role = mdlCompanyManager.userRole;
                            //PersonManager.LastModificationTime = DateTime.Now;

                            ve.PersonManagers.Attach(PersonManager);                           
                            ve.Entry(PersonManager).State = EntityState.Modified;                     
                        }

                        ve.SaveChanges();
                        dbcontextTran.Commit();
                        response.Status = true;
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {                       
                        entityPerson = null;
                        entityAddress = null;
                        entityUser = null;                     
                        PersonManager = null;
                        PropertyManager = null;                        
                    }
                }
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl GetAllManagers(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
           List<CompanyManager> lstCompanyManager = new List<CompanyManager>();
            int PMID = 0;

            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        var managerData = ve.PersonManagers.FirstOrDefault(e => e.PersonID == managerId);
                        if (managerData != null)
                        {
                            PMID = Convert.ToInt32(managerData.PMID);

                            ve.vwCompanyManagers.Where(m => m.PMID == PMID && m.UserRole.ToLower() != clsConstants.propertyManager_Short).ToList().ForEach(u =>
                            {
                                lstCompanyManager.Add(new CompanyManager().GetMdl_CompanyManager(u));

                            });

                            response.Status = true;
                            response.Data = lstCompanyManager;
                        }                       
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {
                        lstCompanyManager = null;                       
                    }
                }
            }
            return response;
        }


        [HttpPost]
        public ResponseMdl DeleteUser(DeleteUserModel DeleteUserModel)
        {
            ResponseMdl response = new ResponseMdl();
            Person entityPerson = new Person();          
            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        entityPerson = ve.People.FirstOrDefault(p => p.Id == DeleteUserModel.personId);

                        entityPerson.IsDeleted = true;
                        entityPerson.LastModificationTime = DateTime.UtcNow;
                        entityPerson.DeleterUserId = DeleteUserModel.deleterId;

                        ve.People.Attach(entityPerson);
                        ve.Entry(entityPerson).State = EntityState.Modified;

                        ve.SaveChanges();
                        dbcontextTran.Commit();
                        response.Status = true;
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {                        
                    }
                }
            }
            return response;
        }
    }


    public class DeleteUserModel
    {
        public int personId { get; set; }
        public int deleterId { get; set; }
    }
}
