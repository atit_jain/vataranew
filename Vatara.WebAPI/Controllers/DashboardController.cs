﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class DashboardController : ApiController
    {
        [HttpGet]
        public ResponseMdl GetChartDataForDashboard(int PMID, string Duration)
        {
            ResponseMdl response = new ResponseMdl();
            DashboardChartModel dashboardChartModel = new DashboardChartModel();
            MdlLeaseChartData mdlLeaseChartData = new MdlLeaseChartData();
            MdlPropertyStatusChartDataForDashboard mdlPropertyStatusChartDataForDashboard = null;
            MdlWorkOrderChartDataForDashboard mdlWorkOrderChartDataForDashboard = new MdlWorkOrderChartDataForDashboard();
            clsDashboardHelper clsDashboardHelper = new clsDashboardHelper();
            MdlExpiringLeases mdlExpiringLeases = new MdlExpiringLeases();
            MdlRentPaidUnpaidAmountForDashboardChart mdlRentPaidUnpaidAmountForDashboardChart = new MdlRentPaidUnpaidAmountForDashboardChart();
            MdlPastDueRentInvoices mdlPastDueRentInvoices = new MdlPastDueRentInvoices();
            MdlIncomeExpenseChartData mdlIncomeExpenseChartData = new MdlIncomeExpenseChartData();
            try
            {
                dashboardChartModel.lstIncomeExpenseLiabilityChartData = clsDashboardHelper.GetIncomeExpenseDataForDashboard(PMID, Duration);
                using (VataraEntities entities = new VataraEntities())
                {
                    mdlPropertyStatusChartDataForDashboard = new MdlPropertyStatusChartDataForDashboard();
                    entities.getPropertyStatusChartDataForDashboard(PMID, Duration).ToList().ForEach(u =>
                    {
                        dashboardChartModel.lstMdlPropertyStatusChartDataForDashboard.Add(mdlPropertyStatusChartDataForDashboard.Get_MdlPropertyStatusChartDataForDashboard(u));
                    });
                    entities.getWorkOrderChartDataForDashboard(PMID, Duration).ToList().ForEach(u =>
                    {
                        dashboardChartModel.lstMdlWorkOrderChartDataForDashboard.Add(mdlWorkOrderChartDataForDashboard.Get_WorkOrderChartDataForDashboard(u));
                    });
                    #region Lease Chart Data
                    entities.getLeaseChartDataForDashboard(PMID).ToList().ForEach(u =>
                    {
                        dashboardChartModel.mdlLeaseChartDataForDashboard.mdlLeaseChartData = mdlLeaseChartData.Get_MdlLeaseChartData(u);
                    });
                    entities.getExpiringLeasesByPMID(PMID).ToList().ForEach(u =>
                    {
                        dashboardChartModel.mdlLeaseChartDataForDashboard.lstMdlExpiringLeases.Add(mdlExpiringLeases.GetMdl_ExpiringLeases(u));
                    });
                    #endregion

                    #region Rent Chart Data
                    entities.getRentPaidUnpaidAmountForDashboardChart(PMID).ToList().ForEach(u =>
                    {
                        dashboardChartModel.mdlRentChartDataForDeshboard.mdlRentPaidUnpaidAmountForDashboardChart = mdlRentPaidUnpaidAmountForDashboardChart.GetMdl_RentPaidUnpaidAmountForDashboardChart(u);
                    });
                    entities.getPastDueRentInvoicesByPMID(PMID).ToList().ForEach(u =>
                    {
                        dashboardChartModel.mdlRentChartDataForDeshboard.lstMdlPastDueRentInvoices.Add(mdlPastDueRentInvoices.GetMdl_PastDueRentInvoices(u));
                    });
                    #endregion

                    #region Income OR Expense Chart Data

                    //Income Chart
                    entities.GetIncomeDataChart(PMID, Duration).ToList().ForEach(u =>
                    {
                        dashboardChartModel.lstIncomeChartData.Add(mdlIncomeExpenseChartData.GetMdl_IncomeChartData(u));
                    });

                    //Income and Expense Chart
                    entities.GetIncomeExpenseChartData(PMID, Duration).ToList().ForEach(u =>
                    {
                        dashboardChartModel.lstIncomeExpenseChartData.Add(mdlIncomeExpenseChartData.GetMdl_IncomeExpenseChartData(u));
                    });

                    #endregion
                }
                response.Data = dashboardChartModel;
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                dashboardChartModel = null;
                clsDashboardHelper = null;
                mdlPropertyStatusChartDataForDashboard = null;
                mdlLeaseChartData = null;
                mdlWorkOrderChartDataForDashboard = null;
                mdlExpiringLeases = null;
                mdlRentPaidUnpaidAmountForDashboardChart = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetIncomeExpenseChartData(int PMID, string Duration)
        {
            ResponseMdl response = new ResponseMdl();
            clsDashboardHelper clsDashboardHelper = new clsDashboardHelper();
            List<MdlIncomeExpenseLiabilityDataForDashboard> lstIncomeExpenseLiabilityChartData = new List<MdlIncomeExpenseLiabilityDataForDashboard>();
            try
            {
                lstIncomeExpenseLiabilityChartData = clsDashboardHelper.GetIncomeExpenseDataForDashboard(PMID, Duration);
                response.Data = lstIncomeExpenseLiabilityChartData;
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                clsDashboardHelper = null;
                lstIncomeExpenseLiabilityChartData = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetIncomeChartData(int PMID, string Duration)
        {
            ResponseMdl response = new ResponseMdl();
            DashboardChartModel dashboardChartModel = new DashboardChartModel();
            MdlIncomeExpenseChartData mdlIncomeExpenseChartData = new MdlIncomeExpenseChartData();
            try {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.GetIncomeDataChart(PMID, Duration).ToList().ForEach(u =>
                    {
                        dashboardChartModel.lstIncomeChartData.Add(mdlIncomeExpenseChartData.GetMdl_IncomeChartData(u));
                    });
                }
                response.Data = dashboardChartModel.lstIncomeChartData;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally {
                response = null;
                dashboardChartModel = null;
                mdlIncomeExpenseChartData = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetIncomeandExpenseChartData(int PMID, string Duration)
        {
            ResponseMdl response = new ResponseMdl();
            DashboardChartModel dashboardChartModel = new DashboardChartModel();
            MdlIncomeExpenseChartData mdlIncomeExpenseChartData = new MdlIncomeExpenseChartData();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.GetIncomeExpenseChartData(PMID, Duration).ToList().ForEach(u =>
                    {
                        dashboardChartModel.lstIncomeExpenseChartData.Add(mdlIncomeExpenseChartData.GetMdl_IncomeExpenseChartData(u));
                    });
                }
                response.Data = dashboardChartModel.lstIncomeExpenseChartData;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                response = null;
                dashboardChartModel = null;
                mdlIncomeExpenseChartData = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetPropertyChartData(int PMID, string Duration)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertyStatusChartDataForDashboard> lstMdlPropertyStatusChartDataForDashboard = new List<MdlPropertyStatusChartDataForDashboard>();
            MdlPropertyStatusChartDataForDashboard mdlPropertyStatusChartDataForDashboard = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    mdlPropertyStatusChartDataForDashboard = new MdlPropertyStatusChartDataForDashboard();
                    entities.getPropertyStatusChartDataForDashboard(PMID, Duration).ToList().ForEach(u =>
                    {
                        lstMdlPropertyStatusChartDataForDashboard.Add(mdlPropertyStatusChartDataForDashboard.Get_MdlPropertyStatusChartDataForDashboard(u));
                    });
                }
                response.Data = lstMdlPropertyStatusChartDataForDashboard;
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMdlPropertyStatusChartDataForDashboard = null;
                mdlPropertyStatusChartDataForDashboard = null;
            }
            return response;
        }
    }
}