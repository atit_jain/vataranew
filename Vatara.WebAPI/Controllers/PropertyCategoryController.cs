﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class PropertyCategoryController : ApiController
    {
        public IEnumerable<PropertyCategory> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.PropertyCategories.ToList();
            }
        }

        public PropertyCategory Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.PropertyCategories.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
