﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class ServiceCategoryController : ApiController
    {
        public ResponseMdl Get()
        {
            ResponseMdl response = new ResponseMdl();
            try
            {               
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            var varlist = ve.ServiceCategories.ToList();
                            response.Data = varlist;
                            response.Status = true;
                            return response;
                        }
                        catch (Exception ex)
                        {
                            response.Data = ex.Message;
                            response.Status = false;
                            return response;
                        }
                    }
                }              
            }
            catch (Exception ex)
            {
                response.Data = ex.Message;
                response.Status = false;
                return response;
            }
            finally
            {               
            }
        }


        [HttpGet]
        public ResponseMdl GetServiceCategory(int personTypeId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlServiceCategory> lstServiceCategory = new List<MdlServiceCategory>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getServiceCategory(personTypeId).ToList().ForEach(u =>
                    {
                        lstServiceCategory.Add(new MdlServiceCategory().GetMdl_ServiceCategory(u));
                    });
                    response.Status = true;
                    response.Data = lstServiceCategory;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
            }           
        }


        public ServiceCategory Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.ServiceCategories.FirstOrDefault(p => p.Id == id);
            }
        }

        [HttpPost]
        public HttpResponseMessage SaveCategory([FromBody] MdlServiceCategory category)
        {
            ServiceCategory entityServiceCategory = null;           
            try
            {
                category.isDeleted = false;
                category.creationTime = DateTime.UtcNow;
                entityServiceCategory = new MdlServiceCategory().GetEntity_ServiceCategory(category);

                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            if (entityServiceCategory.Id == 0)
                            {
                                ve.ServiceCategories.Add(entityServiceCategory);
                            }                            
                            ve.SaveChanges();                           
                            dbcontextTran.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                        }
                    }
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, entityServiceCategory);
                message.Headers.Location = new Uri(Request.RequestUri + "/" + entityServiceCategory.Id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
            finally
            {
                entityServiceCategory = null;               
            }
        }

       
    }
}
