﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class LeaseOptionController : ApiController
    {
        public IEnumerable<LeaseOption> Get()
        {
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    return entities.LeaseOptions.ToList();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                throw;
            }
        }

        public HttpResponseMessage GetLeaseOptionByLeaseId(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                var entity = entities.LeaseOptions.Where(p => p.LeaseId == id && p.IsDeleted == false).ToList(); //PM 28-9-2018
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Option for Lease with Id = " + id.ToString() + " Not Found");
                }
            }
        }
    }
}
