﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class CountyController : ApiController
    {
        public IEnumerable<County> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Counties.ToList();
            }
        }

        public County Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Counties.FirstOrDefault(p => p.Id == id);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl GetCountyByState(int stateId)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    List<MdlCounty> cities = new List<MdlCounty>();
                    entities.vwCounties.Where(e => e.StateId == stateId).Select(x => new { x.Id, x.Name,x.StateId }).ToList().ForEach(u =>
                    {
                        cities.Add(new MdlCounty { id = u.Id, name = u.Name,stateId = u.StateId });
                    });
                    response.Status = true;
                    response.Data = cities;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }


    }
}
