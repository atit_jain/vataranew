﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class CompanyController : ApiController
    {
        [Authorize]
        [HttpPost]
        public ResponseMdl SaveCompany([FromBody] MdlManagerMaster data)
        {
            ResponseMdl response = new ResponseMdl();
            Address entityAddress = null;
            ManagerMaster ManagerMaster = null;
            getPropertyManagerDetailForDisplayInfo_Result PropertyManagerDetail = null;


            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            if (data.pmid > 0)
                            {
                                entityAddress = ve.Addresses.FirstOrDefault(a => a.Id == data.addressId);
                                entityAddress.CountryId = (int)(data.countryId == null ? 0 : data.countryId);
                                entityAddress.StateId = (int)(data.stateId == null ? 0 : data.stateId);
                                entityAddress.CountyId = (int)(data.countyId == null ? 0 : data.countyId);
                                entityAddress.CityId = (int)(data.cityId == null ? 0 : data.cityId);
                                entityAddress.Zipcode = data.zipCode;
                                entityAddress.Line1 = data.line1;
                                entityAddress.Line2 = data.line2;
                                entityAddress.LastModificationTime = DateTime.UtcNow;
                                ve.Addresses.Attach(entityAddress);
                                ve.Entry(entityAddress).State = EntityState.Modified;

                                ManagerMaster = ve.ManagerMasters.FirstOrDefault(m => m.PMID == data.pmid);
                                ManagerMaster.Company_Name = data.companyName;
                                ManagerMaster.Company_Email = data.email;
                                ManagerMaster.Company_Fax = data.fax;
                                ManagerMaster.Company_Phone = data.phone;
                                ManagerMaster.Logo = data.logo;
                                ManagerMaster.ContactUs = data.contactUs;
                                ManagerMaster.AboutUs = data.aboutUs;

                                if (data.useCompanyInfoForDisplay)
                                {
                                    ManagerMaster.DisplayAddressId = data.addressId;
                                    ManagerMaster.DisplayEmail = data.email;
                                    ManagerMaster.DisplayFax = data.fax;
                                    ManagerMaster.DisplayName = data.companyName;
                                    ManagerMaster.DisplayPhone = data.phone;
                                    ManagerMaster.useCompanyInfoForDisplay = true;
                                }
                                else
                                {
                                    PropertyManagerDetail = ve.getPropertyManagerDetailForDisplayInfo(data.pmid).FirstOrDefault();
                                    ManagerMaster.DisplayAddressId = PropertyManagerDetail.AddressId;
                                    ManagerMaster.DisplayEmail = PropertyManagerDetail.Email;
                                    ManagerMaster.DisplayFax = PropertyManagerDetail.Fax;
                                    ManagerMaster.DisplayName = PropertyManagerDetail.NAME;
                                    ManagerMaster.DisplayPhone = PropertyManagerDetail.PhoneNo;
                                    ManagerMaster.useCompanyInfoForDisplay = false;
                                }
                                ManagerMaster.SliderImage1 = data.sliderImage1;
                                ManagerMaster.SliderImage2 = data.sliderImage2;
                                ManagerMaster.SliderImage3 = data.sliderImage3;
                                ManagerMaster.LastModificationTime = DateTime.UtcNow;
                                ve.ManagerMasters.Attach(ManagerMaster);
                                ve.Entry(ManagerMaster).State = EntityState.Modified;
                            }
                            else
                            {
                                entityAddress = new MdlManagerMaster().GetAddressModel(data);
                                entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.companyAddress.ToLower()).Id;
                                entityAddress.IsDeleted = false;
                                entityAddress.CreationTime = DateTime.UtcNow;
                                entityAddress.LastModificationTime = DateTime.UtcNow;
                                ve.Addresses.Add(entityAddress);
                                ve.SaveChanges();

                                ManagerMaster = new MdlManagerMaster().Get_Entity_ManagerMaster(data);
                                // ManagerMaster.URL = ManagerMaster.CompanyName.Replace(" ", "").ToLower().Substring(0, 8);
                                ManagerMaster.IsDeleted = false;
                                // ManagerMaster.AddressId = entityAddress.Id;
                                ManagerMaster.LastModificationTime = DateTime.UtcNow;
                                ManagerMaster.CreationTime = DateTime.UtcNow;
                            }
                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                entityAddress = null;
                PropertyManagerDetail = null;
            }
            // return response;
        }

      
        [HttpGet]
        public ResponseMdl GetCompanyData(string EmailId, int personId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlManagerMaster MdlManagerMaster = null;
            vwManagerMaster vwManagerMaster = null;
            PersonManager PersonManager = null;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    PersonManager = ve.PersonManagers.FirstOrDefault(p => p.PersonID == personId);
                    if (PersonManager != null)
                    {
                        vwManagerMaster = ve.vwManagerMasters.FirstOrDefault(e => e.PMID == PersonManager.PMID);

                        if (vwManagerMaster != null)
                        {
                            MdlManagerMaster = new MdlManagerMaster().GetMdl_ManagerMaster(vwManagerMaster);
                            response.Data = MdlManagerMaster;
                        }
                        else { response.Data = null; }
                        response.Status = true;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                MdlManagerMaster = null;
                vwManagerMaster = null;
                PersonManager = null;
            }
        }

       
        [HttpGet]
        public ResponseMdl GetCompanyProfile(string CompanyDomain)
        {
            ResponseMdl response = new ResponseMdl();
            MdlManagerMaster MdlManagerMaster = null;
            vwManagerMaster vwManagerMaster = null;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    vwManagerMaster = ve.vwManagerMasters.FirstOrDefault(e => e.URL.ToLower() == CompanyDomain.ToLower());

                    if (vwManagerMaster != null)
                    {
                        MdlManagerMaster = new MdlManagerMaster().GetMdl_ManagerMaster(vwManagerMaster);
                        response.Data = MdlManagerMaster;
                        response.Status = true;
                    }
                    else
                    {
                        response.Data = null;
                        response.Status = false;
                    }


                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                MdlManagerMaster = null;
                vwManagerMaster = null;
            }

        }

        
        [HttpGet]
        public ResponseMdl validateCompanyURL(string url, int PMID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (PMID == 0)
                        isExist = entities.ManagerMasters.Any(e => e.URL.ToLower() == url.ToLower());
                    else
                        isExist = entities.ManagerMasters.Any(e => e.URL.ToLower() == url.ToLower() && e.PMID != PMID);

                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }

            return response;
        }


        [AllowAnonymous]  // remove this when you implement the authentication
                          //[HttpPost]
        public async Task<HttpResponseMessage> SaveCompanySliderImages()
        {
            string FileType = "Photos";
            IList<string> AllowedExtns = new List<string> { ".jpeg", ".jpg", ".gif", ".png" };
            string FileSavePath = "\\Uploads\\Manager\\Company\\";
            int MaxContentLength = 1024 * 1024 * 4; //Size = 6 MB  

            var httpRequest = System.Web.HttpContext.Current.Request;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            string companyId = string.Empty;
            string filePath = string.Empty;
            string imageIndex = string.Empty;
            string FileName = string.Empty;
            DateTime dtNow = DateTime.UtcNow;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string imageNo = string.Empty;

            try
            {
                var lstDocument = httpRequest.Form.GetValues("seqno");
                // var data = httpRequest.Form.GetValues("seqNo");

                for (int i = 0; i < httpRequest.Files.Count; i++)
                {
                    response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[i];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        //int maxContentLength = 1024 * 1024 * 4; //Size = 4 MB  
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedExtns.Contains(extension))
                        {
                            var message = string.Format("Please upload valid file types.");

                            dict.Add("error" + i.ToString(), message);
                            response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please upload a file upto 4 mb.");

                            dict.Add("error" + i.ToString(), message);
                            response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            var seqNo = "";

                            if (httpRequest.Form.GetValues("seqNo")[i] != "undefined")
                            {
                                seqNo = httpRequest.Form.GetValues("seqNo")[i];
                            }

                            char[] delimiters = new char[] { '\\' };
                            string[] extFilename = postedFile.FileName.Split(delimiters);

                            if (extFilename.Length > 0)
                            {
                                FileName = extFilename[extFilename.Length - 1];
                            }

                            companyId = httpRequest.Form.GetValues("companyId")[i];

                            if (FileType == "Photos")
                            {
                                imageNo = seqNo;
                                filePath = HttpRuntime.AppDomainAppPath + FileSavePath + string.Concat(companyId, "-Image" + imageNo + ".png");
                            }
                            else
                                filePath = HttpRuntime.AppDomainAppPath + FileSavePath + string.Concat(companyId, "-", postedFile.FileName);

                            postedFile.SaveAs(filePath);
                            var message1 = string.Format("Files Updated Successfully.");
                            dict.Add("success" + i.ToString(), message1);

                        }
                    }
                }

                #region
                //using (VataraEntities ve = new VataraEntities())
                //{
                //    if (lstDocument != null)
                //    {
                //        for (int i = 0; i < lstDocument.Length; i++)
                //        {
                //            DataAccess.File fl = new DataAccess.File();
                //            fl.LinkId = Convert.ToInt32(httpRequest.Form.GetValues("propid")[i]);
                //            fl.ModuleId = httpRequest.Form.GetValues("moduleid")[i];
                //            fl.FileType = FileType;
                //            fl.Name = httpRequest.Form.GetValues("filename")[i];
                //            Vatara.DataAccess.File fileV = ve.Files.FirstOrDefault(p => p.LinkId == fl.LinkId && p.ModuleId == fl.ModuleId && p.FileType == p.FileType && p.Name == fl.Name);
                //            if (fileV != null)
                //            {
                //                fl = fileV;
                //            }
                //            fl.SeqNo = Convert.ToInt32(httpRequest.Form.GetValues("seqNo")[i]);
                //            // postedFile.FileName;
                //            fl.Description = httpRequest.Form.GetValues("filedesc")[i];
                //            fl.FileSavePath = filePath;
                //            fl.Tags = httpRequest.Form.GetValues("filetags")[i];
                //            fl.LastModificationTime = dtNow;
                //            fl.LastModifierUserId = 0;
                //            if (httpRequest.Form.GetValues("idDeleted") != null)
                //                fl.IsDeleted = Convert.ToBoolean(httpRequest.Form.GetValues("idDeleted")[i]);
                //            if (fileV == null)
                //            {
                //                fl.CreationTime = dtNow;
                //                fl.IsDeleted = false;
                //                ve.Files.Add(fl);
                //            }
                //            ve.SaveChanges();
                //        }
                //    }
                //}

                #endregion

                response = Request.CreateResponse(HttpStatusCode.Created, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("Exception in UploadFile: " + ex.ToString());
                dict.Add("ErrorHandler", res);
                response = Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            return response;
        }


    }
}