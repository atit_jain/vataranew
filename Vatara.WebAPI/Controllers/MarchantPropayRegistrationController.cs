﻿using Propay_Manage;
using System;
using System.Linq;
using System.ServiceModel;
using System.Web.Http;
using System.Xml;
using Vatara.DataAccess;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class MarchantPropayRegistrationController : ApiController
    {
        [HttpPost]
        public ResponseMdl RegisterMarchant([FromBody] MdlPropay_Profile data)
        {
            ResponseMdl response = new ResponseMdl();
            SignupMarchant bojSignupMarchant = new SignupMarchant();
            BasicHttpBinding httpBinding = new BasicHttpBinding();
            httpBinding.MaxReceivedMessageSize = 2147483647;
            httpBinding.MaxBufferSize = 2147483647;
            PorpayProfile PorpayProfile = null;
            Address Address = null;
            AddressType AddressType = null;
            string[] strResponse = new string[2];
            XmlDocument doc = new XmlDocument();
            string xmlRequest = string.Empty;
            string obj = string.Empty;
            XmlNodeList status = null;
            XmlNodeList accntNum = null;
            XmlNodeList tier = null;
            XmlNodeList transType = null;
            XmlNodeList password = null;
            long profileid = 0;
            SPSServices service = new SPSServices();
            try
            {
                if (!string.IsNullOrEmpty(data.PorpayProfile.marchantProfileId))
                {

                   // string CertStr = GetBase64Cert(Propay_Manage.Configuration.CertStr);
                    //xmlRequest = string.Format(bojSignupMarchant.GetPropayProfile(),
                    //                    Propay_Manage.Configuration.CertStr, Propay_Manage.Configuration.TermId,
                    //                   data.PorpayProfile.email, data.PorpayProfile.firstName, data.PorpayProfile.lastName,
                    //                   data.PorpayProfile.phone, data.PorpayProfile.marchantProfileId, data.PorpayProfile.marchantAccntNum);

                    xmlRequest = string.Format(bojSignupMarchant.GetXMLRequestforEdit(),
                                       Propay_Manage.Configuration.CertStr, Propay_Manage.Configuration.TermId,
                                       data.PorpayProfile.email, data.PorpayProfile.firstName, data.PorpayProfile.lastName,
                                       data.PorpayProfile.phone, data.PorpayProfile.marchantProfileId, data.PorpayProfile.marchantAccntNum);

                    obj = service.SignupMerchantXML(Propay_Manage.Configuration.xmlMerchantSignup, xmlRequest);

                    //SubmitRequest(xmlRequest);
                    //if (obj != null)
                    //{
                    //    doc.LoadXml(obj);
                    //    status = doc.GetElementsByTagName("status");
                    //    if (status != null && status[0].InnerText == "00")
                    //    {

                    //    }
                    //}
                    //else {

                    //}
                }
                else
                {
                    xmlRequest = string.Format(bojSignupMarchant.GetXMLRequest(),
                                       Propay_Manage.Configuration.CertStr, Propay_Manage.Configuration.TermId,
                                       data.PorpayProfile.email, data.PorpayProfile.firstName, data.PorpayProfile.lastName,
                                       data.Address.line1, data.Address.city, data.Address.state, data.Address.zipcode,
                                       data.PorpayProfile.phone, data.PorpayProfile.ssn, Convert.ToDateTime(data.PorpayProfile.dob.ToString("MM-dd-yyyy")),
                                       data.PorpayProfile.bankName, data.PorpayProfile.accountName, data.PorpayProfile.accountNumber,
                                       data.PorpayProfile.routingNumber, data.PorpayProfile.accountType, data.PorpayProfile.accountOwnershipType);

                    //obj = service.SignupMerchantXML(API.xmlMerchantSignup, xmlRequest);
                    obj = service.SignupMerchantXML(Propay_Manage.Configuration.xmlMerchantSignup, xmlRequest);
                    //bojSignupMarchant.FindPropayMerchantIdAndPayerId("sheethal.kumar@gmail.com", "Sheetal Kumar");

                    if (obj != null)
                    {
                        doc.LoadXml(obj);
                        status = doc.GetElementsByTagName("status");
                        if (status != null && status[0].InnerText == "00")
                        {
                            accntNum = doc.GetElementsByTagName("accntNum");
                            tier = doc.GetElementsByTagName("tier");
                            transType = doc.GetElementsByTagName("transType");
                            password = doc.GetElementsByTagName("password");
                            profileid = bojSignupMarchant.CreateProfile(accntNum[0].InnerText, data.PorpayProfile.firstName + data.PorpayProfile.lastName);
                            if (profileid != 0)
                            {
                                strResponse = bojSignupMarchant.CreatePayer(data.PorpayProfile.email, data.PorpayProfile.firstName + " " + data.PorpayProfile.lastName);
                                if (strResponse[0] == "success")
                                {
                                    using (VataraEntities ve = new VataraEntities())
                                    {
                                        using (var dbcontextTran = ve.Database.BeginTransaction())
                                        {
                                            try
                                            {
                                                AddressType = ve.AddressTypes.FirstOrDefault(s => s.Name == clsConstants.personAddress);
                                                data.Address.creationTime = DateTime.UtcNow;
                                                data.Address.isDeleted = false;
                                                data.Address.addressTypeId = AddressType.Id;
                                                data.Address.line2 = "";
                                                Address = new MdlAddress().GetEntity_Address(data.Address);
                                                ve.Addresses.Add(Address);
                                                ve.SaveChanges();

                                                data.PorpayProfile.addressId = Address.Id;
                                                data.PorpayProfile.marchantProfileId = profileid.ToString();
                                                data.PorpayProfile.marchantPassword = password[0].InnerText;
                                                data.PorpayProfile.marchantAccntNum = accntNum[0].InnerText;
                                                data.PorpayProfile.payerId = strResponse[1];
                                                PorpayProfile = new MdlPorpayProfile().GetEntity_PorpayProfile(data.PorpayProfile);
                                                ve.PorpayProfiles.Add(PorpayProfile);
                                                ve.SaveChanges();
                                                dbcontextTran.Commit();
                                                response.Status = true;
                                                return response;
                                            }
                                            catch (Exception ex)
                                            {
                                                dbcontextTran.Rollback();
                                                response.Status = false;
                                                response.Message = clsConstants.somethingwrong.ToString();
                                                return response;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            response.Status = false;
                            if (status[0].InnerText == "41")
                            {
                                response.Message = clsConstants.propayErrorCode_41;
                            }
                            else if (status[0].InnerText == "46")
                            {
                                response.Message = clsConstants.propayErrorCode_46;
                            }
                            else if (status[0].InnerText == "53")
                            {
                                response.Message = clsConstants.propayErrorCode_53;
                            }
                            else if (status[0].InnerText == "54")
                            {
                                response.Message = clsConstants.propayErrorCode_54;
                            }
                            else if (status[0].InnerText == "87")
                            {
                                response.Message = clsConstants.propayErrorCode_87;
                            }
                            else if (status[0].InnerText == "40")
                            {
                                response.Message = clsConstants.propayErrorCode_40;
                            }
                            else
                            {
                                response.Message = clsConstants.somethingwrong;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
            }
            finally
            {
                bojSignupMarchant = null;
                PorpayProfile = null;
                Address = null;
                AddressType = null;
                strResponse = null;
                doc = null;
                xmlRequest = string.Empty;
                service = null;
                status = null;
                accntNum = null;
                tier = null;
                transType = null;
                password = null;
                profileid = 0;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetPropayProfile(int personId, int personTypeId) //From Id
        {
            ResponseMdl response = new ResponseMdl();
            // MdlPorpayProfile mdlPropayProfile = new MdlPorpayProfile();
            MdlGetPropayProfile mdlPropayProfile = new MdlGetPropayProfile();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    //var proPayProfile = ve.PorpayProfiles.FirstOrDefault(e => e.PersonId == id);
                    //if (proPayProfile != null)
                    //{
                    //    mdlPropayProfile = mdlPropayProfile.GetMdl_PorpayProfile(proPayProfile);
                    //    response.Data = mdlPropayProfile;
                    //}
                    //else
                    //{
                    //    mdlPropayProfile = new MdlPorpayProfile().GetMdl_PorpayProfileFromPerson(ve.vwPersons.FirstOrDefault(p => p.pId == id));
                    //}
                    mdlPropayProfile = new MdlGetPropayProfile().GetMdl_PorpayProfile(ve.getPropayProfile(personId, personTypeId).FirstOrDefault());


                    response.Status = true;
                    response.Data = mdlPropayProfile;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                mdlPropayProfile = null;
            }
            return response;
        }


        public ResponseMdl GetMerchantProfile(long MarchantId)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    SPSServices service = new SPSServices();
                    var profile = service.GetMerchantProfiles().Profiles.Where(p => p.ProfileId == MarchantId);
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            { }
        }

        
        private static void SubmitRequest(string request)
        {
            byte[] dataToSend = Encoding.UTF8.GetBytes(request);
            // Change the following URL to point to production instead of integration
            WebRequest webRequest = WebRequest.Create("https://xmltest.propay.com/API/PropayAPI.aspx");
            webRequest.Method = "POST";
            webRequest.ContentLength = dataToSend.Length;
            webRequest.ContentType = "text/xml";
            webRequest.Headers.Add("X509Certificate", GetBase64Cert(Propay_Manage.Configuration.CertStr));
           // webRequest.ProtocolVersion = HttpVersion.Version10;
            Stream dataStream = webRequest.GetRequestStream();
            dataStream.Write(dataToSend, 0, dataToSend.Length);
            dataStream.Close();
            string response = string.Empty;
            try
            {
                WebResponse apiResponse = webRequest.GetResponse();
                using (StreamReader sr = new StreamReader(apiResponse.GetResponseStream()))
                {
                    response += sr.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;
                using (Stream responseStream = httpResponse.GetResponseStream())
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    response = reader.ReadToEnd();
                }
            }
            ParseResponse(response);
        }
        private static void ParseResponse(string response)
        {
            var load = XDocument.Parse(response);
            var transType = Convert.ToInt32(load.Descendants().First(p => p.Name.LocalName == "transType").Value);
            var status = load.Descendants().First(p => p.Name.LocalName == "status").Value;
        }
        public static string GetBase64Cert(string certificateThumbprint)
        {
            using (X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine))
            {
                store.Open(OpenFlags.ReadOnly);
                var foundCertificates = store.Certificates.Find(X509FindType.FindByThumbprint, certificateThumbprint, false);
                if (foundCertificates.Count != 1)
                {
                    return null;
                }
                var certByteArray = foundCertificates[0].Export(X509ContentType.Cert);
                store.Close();
                return Convert.ToBase64String(certByteArray);
            }
        }


    }

}
