﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class LeaseClauseController : ApiController
    {
        public IEnumerable<LeaseClaus> Get()
        {
           // LeaseClauses
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    return entities.LeaseClauses.ToList();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                throw;
            }
        }


        public HttpResponseMessage GetLeaseClauseByLeaseId(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                var entity = entities.LeaseClauses.Where(p => p.LeaseId == id && p.IsDeleted == false).ToList(); //PM 28-9-2018
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Clause for Lease with Id = " + id.ToString() + " Not Found");
                }
            }
        }
     
    }
}
