﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class OutStandingPayController : ApiController
    {
        //[HttpGet]
        //public ResponseMdl VendorRemainPayment(int managerId, DateTime fromDate, DateTime toDate)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    DateTime newToDate = (toDate.Date).AddDays(1);
        //    List<MdlVendorRemainAMT> vendorRemainAMT = new List<MdlVendorRemainAMT>();
        //    try
        //    {
        //        using (VataraEntities entities = new VataraEntities())
        //        {
        //            entities.getVendorRemainAMTByManager(fromDate, toDate, managerId, 0, (int)clsCommon.PersonType.PropertyManager).ToList().ForEach(u =>
        //            {
        //                vendorRemainAMT.Add(new MdlVendorRemainAMT().GetMdl_VendorRemainAMT(u));
        //            });

        //            //entities.vwVendorRemainAMTs.Where(e => e.ManagerId == managerId && e.Date >= fromDate.Date && e.Date <= newToDate).ToList().ForEach(u =>
        //            //{
        //            //    vendorRemainAMT.Add(new MdlVendorRemainAMT().GetMdl_VendorRemainAMT(u));
        //            //});
        //            response.Status = true;
        //            response.Data = vendorRemainAMT;
        //            //  return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Status = false;
        //        response.Message = clsConstants.somethingwrong;
        //        return response;
        //    }
        //    finally
        //    {
        //        vendorRemainAMT = null;
        //    }
        //    return response;
        //}

        [HttpPost]
        public ResponseMdl VendorPaid([FromBody]MdlVendorRemainAMT mdlVendor)
        {
            ResponseMdl response = new ResponseMdl();
            InvoiceDetail invoiceDetail = new InvoiceDetail();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            invoiceDetail = ve.InvoiceDetails.FirstOrDefault(e => e.Id == mdlVendor.invoiceDetailId);
                            invoiceDetail.VendorPaid = true;
                            invoiceDetail.Vendor_Notes = mdlVendor.Vendor_Notes;
                            invoiceDetail.VendorPaymentDate = DateTime.UtcNow;
                            invoiceDetail.LastModificationTime = DateTime.UtcNow;
                            invoiceDetail.LastModifierUserId = mdlVendor.managerId;
                            ve.InvoiceDetails.Attach(invoiceDetail);
                            ve.Entry(invoiceDetail).State = EntityState.Modified;
                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                invoiceDetail = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl VendorRemainPayment(int managerId,int PMID, DateTime fromDate, DateTime toDate, string filterCategory, int personType, int personId = 0,int ownerPOID = 0)
        {
            ResponseMdl response = new ResponseMdl();
          //  DateTime newToDate = (toDate.Date).AddDays(1);
            MdlOutstandingData MdlOutstandingData = new MdlOutstandingData();
            int propertyId = 0;
           
            try
            {
                if (personId > 0)
                {
                    MdlOutstandingData.MdlPropTenantOwnerDetail = GetPropertyByPerson(ownerPOID,personId, managerId, personType);
                }
                using (VataraEntities entities = new VataraEntities())
                {
                    if (filterCategory.ToUpper() == clsConstants.UnPaidLiabilities.ToUpper())
                    {
                        entities.getVendorRemainAMTByManager(fromDate, toDate, managerId, personId, personType).ToList().ForEach(u =>
                        {
                            MdlOutstandingData.vendorRemainAMT.Add(new MdlVendorRemainAMT().GetMdl_VendorRemainAMT(u));
                        });
                        entities.getManagerLiability(fromDate, toDate, personId, PMID, propertyId, filterCategory).ToList().ForEach(u =>
                        {
                            MdlOutstandingData.lstMdlManagerLiability.Add(new MdlManagerLiability().GetMdl_ManagerLiability(u));
                        });
                    }
                    else if (filterCategory.ToUpper() == clsConstants.PaidLiabilities.ToUpper())
                    {
                        entities.getVendorPaidAMTByManager(fromDate, toDate, managerId, personId, personType).ToList().ForEach(u =>
                        {
                            MdlOutstandingData.vendorRemainAMT.Add(new MdlVendorRemainAMT().GetMdl_VendorRemainAMT(u));
                        });
                        entities.getManagerLiability(fromDate, toDate, personId, PMID, propertyId, filterCategory).ToList().ForEach(u =>
                         {
                             MdlOutstandingData.lstMdlManagerLiability.Add(new MdlManagerLiability().GetMdl_ManagerLiability(u));
                         });
                    }                   
                    response.Status = true;
                    response.Data = MdlOutstandingData;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                MdlOutstandingData = null;
            }
        }


        private List<MdlPropertOwnersWithManager> GetPropertyByPerson(int ownerPOID, int personId, int managerId, int personType)
        {

            List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
           // vwPersonWithType PersonWithType = new vwPersonWithType();

            using (VataraEntities entities = new VataraEntities())
            {
              //  PersonWithType.Type = clsConstants.propertyManager;


                entities.getPropertiesByPerson(personId, personType, ownerPOID).ToList().ForEach(u =>
                {
                    lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                });

                //if (personId > 0)
                //{
                //    PersonWithType = entities.vwPersonWithTypes.FirstOrDefault(e => e.Id == personId);
                //}

                //if (PersonWithType.Type.ToLower() == clsConstants.Owner)
                //{
                //    entities.vwPropTenantOwnerDetails.GroupBy(p => new { p.PropId, p.PropManagr, p.PropName, p.PropAddress,p.OwnerId }).Select(m => new { m.Key.PropId, m.Key.PropName, m.Key.PropManagr, m.Key.PropAddress, m.Key.OwnerId }).Where(e => e.OwnerId == personId && e.PropManagr == managerId).ToList().ForEach(u =>
                //    {
                //        lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName =  u.PropName +" | "+ u.PropAddress, propertyId = u.PropId });
                //    });
                //}

                //else if (PersonWithType.Type.ToLower() == clsConstants.Tenant)
                //{
                //    entities.vwPropTenantOwnerDetails.GroupBy(p => new { p.PropId, p.PropManagr, p.PropName, p.PropAddress,p.OwnerId,p.TenantId }).Select(m => new { m.Key.PropId, m.Key.PropName, m.Key.PropManagr, m.Key.PropAddress, m.Key.TenantId }).Where(e => e.TenantId == personId && e.PropManagr == managerId).ToList().ForEach(u =>
                //    {
                //        //lstMdlPropTenantOwnerDetail.Add(new MdlPropTenantOwnerDetail().GetMdl_PropTenantOwnerDetail1(u));
                //        lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                //    });
                //}
                //else
                //{

                //    entities.vwPropTenantOwnerDetails.GroupBy(p => new {p.PropId,p.PropManagr,p.PropName,p.PropAddress }).Select(m => new {m.Key.PropId, m.Key.PropName, m.Key.PropManagr, m.Key.PropAddress }).Where(e => e.PropManagr == managerId).ToList().ForEach(u =>
                //    {
                //        //  lstMdlPropTenantOwnerDetail.Add(new MdlPropTenantOwnerDetail().GetMdl_PropTenantOwnerDetail1(u));
                //        lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                //    });
                //}
            }
            return lstMdlPropertOwnersWithManager;
        }


        [HttpGet]
        public ResponseMdl PropertOwnersByManager(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId,ownerFullNameForFilter = u.OwnerFullName, ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
                    });

                    //entities.vwPropertOwnersWithManagers.Where(e => e.ManagerId == managerId).Select(e => new { e.OwnerId, e.OwnerFullName, e.PropertyId, e.PropertyName, e.PersonTypeId, e.Owner_Address }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, ownerFullName = u.OwnerFullName, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
                    //});

                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByManagerAndOwner(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {

                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName });
                    });

                    //entities.vwPropertOwnersWithManagers.Where(e => e.ManagerId == managerId).Select(e => new { e.PropertyId, e.PropertyName, e.OwnerId }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName });
                    //});
                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl PropertByTenant_Owner(int managerId)  //Dont change managerId variable name
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    ///********** below lines commented by asr because the view throwes error 
                    /// *************************************************************************
                    //entities.vwPropertOwnersWithManagers.Where(e => e.OwnerId == managerId).Select(e => new { e.PropertyId, e.PropertyName, e.OwnerId, e.PersonTypeId }).Distinct().ToList().ForEach(u =>
                    //{
                    //    PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName, personTypeId = u.PersonTypeId });
                    //});
                    response.Status = true;
                    response.Data = PropertOwners;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }           
        }
    }

    public class MdlOutstandingData
    {
        public MdlOutstandingData()
        {
            vendorRemainAMT = new List<MdlVendorRemainAMT>();
            MdlPropTenantOwnerDetail = new List<MdlPropertOwnersWithManager>();
            lstMdlManagerLiability = new List<MdlManagerLiability>();
            //lstMdlManagerLiabilityToOwner = new List<MdlManagerLiabilityToOwner>();
        }

        public List<MdlVendorRemainAMT> vendorRemainAMT { get; set; }
        public List<MdlPropertOwnersWithManager> MdlPropTenantOwnerDetail { get; set; }
        public List<MdlManagerLiability> lstMdlManagerLiability { get; set; }
        //public List<MdlManagerLiabilityToOwner> lstMdlManagerLiabilityToOwner { get; set; }       
    }
}
