﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PropertyOwnersController : ApiController
    {

        public IEnumerable<vwPropertyOwner> Get()
        {
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    return entities.vwPropertyOwners.ToList();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                throw;
            }
        }

        public HttpResponseMessage Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                List<vwPropertyOwner> entity = entities.vwPropertyOwners.Where(p => p.pPropertyId == id).ToList();
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "OWners for Property with Id = " + id.ToString() + " Not Found");
                }
            }
        }

    }
}
