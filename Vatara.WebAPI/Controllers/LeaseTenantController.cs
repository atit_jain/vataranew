﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LeaseTenantsController : ApiController
    {
        public IEnumerable<vwLeaseTenant> Get()
        {
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    return entities.vwLeaseTenants.ToList();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                throw;
            }
        }

        public vwLeaseTenant GetLeaseById(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                var data = entities.vwLeaseTenants.Where(p => p.pLeaseId == id && p.IsDeleted == false).FirstOrDefault();
                return data;
            }
        }

    }
}
