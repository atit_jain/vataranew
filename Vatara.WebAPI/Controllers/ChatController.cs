﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using Vatara.DataAccess;


namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class ChatController : ApiController
    {

        [HttpGet]
        public ResponseMdl GetChatUnreadNotificationMessage(string toMsg)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlChatUnreadNotificationMessage> lstMdlChatUnreadNotificationMessage = new List<MdlChatUnreadNotificationMessage>();
            MdlChatUnreadNotificationMessage mdlChatUnreadNotificationMessage = new MdlChatUnreadNotificationMessage();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getChatUnreadNotificationMessage(toMsg).ToList().ForEach(u =>
                    {
                        lstMdlChatUnreadNotificationMessage.Add(mdlChatUnreadNotificationMessage.GetMdl_ChatUnreadNotificationMessage(u));
                    });                 
                    response.Status = true;
                    response.Data = lstMdlChatUnreadNotificationMessage;                  
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMdlChatUnreadNotificationMessage = null;
                mdlChatUnreadNotificationMessage = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetAllUserListForChat(int PMID,int loggedInPersonType,string loggedInPersonEmail)
        {
            ResponseMdl response = new ResponseMdl();          
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    //  var userList = entities.MessageUsers.Where(s => s.PMID == PMID && s.IsDeleted == false).ToList();
                    var userList = entities.getUserListForChat(loggedInPersonType, PMID, loggedInPersonEmail).ToList();
                    response.Status = true;
                    response.Data = userList;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }            
            return response;
        }

        [HttpGet]
        public ResponseMdl GetChatInfo(string LoggedInUser,int LoggedInUserPersonTypeId, string SelectedUser,int SelectedUserPersonTypeId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlMessage> lstMessage = new List<MdlMessage>();
            MdlMessage mdlMessage = new MdlMessage();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.Sp_GetChatInfo(LoggedInUser, LoggedInUserPersonTypeId, SelectedUser, SelectedUserPersonTypeId).ToList().ForEach(u => {
                        lstMessage.Add(mdlMessage.GetMdl_Message(u));
                    });
                    response.Status = true;
                    response.Data = lstMessage;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMessage = null;
                mdlMessage = null;
            }
            return response;
        }

        public int getUserID(string Email)
        {
            int userId = 0;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var entity = entities.getMessageUserIdByEmail(Email).FirstOrDefault();
                    if (entity != null)
                    {
                        userId = (int)entity;
                    }
                }
                return userId;
            }
            catch (Exception ex)
            {
                return userId;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ResponseMdl UploadFiles()
        {
            string path = "";
            string localPath = "";
            ResponseMdl response = new ResponseMdl();
            var httpRequest = System.Web.HttpContext.Current.Request;
            // Checking no of files injected in Request object  
            if (httpRequest.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollection files = httpRequest.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        string fname;
                        // Checking for Internet Explorer  
                        if (httpRequest.Browser.Browser.ToUpper() == "IE" || httpRequest.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        fname = DateTime.Now.ToString("MM-dd-yyyy hh:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "")+"-" + fname;
                        // Get the complete folder path and store the file inside it.  
                        path = "/Uploads/Chat/" + fname;
                        localPath = "../Uploads/Chat/" + fname;
                       // fname = "http://vatara.biz/" + localPath;
                        fname = HttpContext.Current.Server.MapPath(path);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    response.Data = localPath;
                    response.Status = true;
                    return response;
                    //return Json(path);
                }
                catch (Exception ex)
                {                    
                    response.Status = false;
                    return response;
                }
            }
            else
            {
                response.Data = "No files selected.";
                response.Status = false;
                return response;               
            }
        }
        
        [HttpGet]
        public ResponseMdl GetNotification(int personId,int personTypeId,int PMID)
        {
            ResponseMdl response = new ResponseMdl();
          //  List<MdlNotification> lstNotification = new List<MdlNotification>();
            MdlNotificationData mdlNotificationData = new MdlNotificationData();
            MdlNotification mdlNotification = new MdlNotification();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getNotificationData(PMID, personId, personTypeId).ToList().ForEach(u =>
                    {
                        mdlNotificationData.lstNotification.Add(mdlNotification.GetMdl_Notification(u));
                    });
                    mdlNotificationData.unreadNotificationCount = mdlNotificationData.lstNotification.Where(x => x.isRead == false).Count();
                    response.Status = true;
                    response.Data = mdlNotificationData;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlNotificationData = null;
                mdlNotification = null;
            }
            return response;
        }

        [HttpPost]
        public ResponseMdl ClearNotification([FromBody] MdlParameters mdl)
        {
            ResponseMdl response = new ResponseMdl();
            IQueryable<Notification> notification;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    notification = entities.Notifications.Where(x => x.PersonId == mdl.personId && x.PersonTypeId == mdl.personTypeId);
                    entities.Notifications.RemoveRange(notification);
                    entities.SaveChanges();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally {
                notification = null;
            }          
            return response;
        }


        [HttpPost]
        public ResponseMdl DeleteNOtification([FromBody] int notificationId)
        {
            ResponseMdl response = new ResponseMdl();
            Notification notification;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    notification = entities.Notifications.FirstOrDefault(x => x.Id == notificationId);
                    entities.Notifications.Remove(notification);
                    entities.SaveChanges();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                notification = null;
            }
            return response;
        }


        [HttpGet]
        public ResponseMdl UpdateNotificationStatus(int notificationId)
        {
            ResponseMdl response = new ResponseMdl();
            Notification notification = new Notification();         
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    notification = entities.Notifications.FirstOrDefault(n => n.Id == notificationId);
                    notification.IsRead = true;
                    entities.Notifications.Attach(notification);
                    entities.Entry(notification).State = EntityState.Modified;
                    entities.SaveChanges();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally {              
                notification = null;
            }
            return response;
        }

    }
    public class MdlParameters {
        public int personId { get; set; }
        public int personTypeId { get; set; }
    }

    public class MdlNotificationData
    {
        public MdlNotificationData()
        {
            lstNotification = new List<MdlNotification>();
        }
        public int unreadNotificationCount { get; set; }
        public List<MdlNotification> lstNotification { get; set; }
    }
}
