﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class PropertyTypeController : ApiController
    {
        public IEnumerable<PropertyType> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.PropertyTypes.ToList();
            }
        }

        public PropertyType Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.PropertyTypes.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
