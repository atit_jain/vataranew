﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class StatusController : ApiController
    {
        public IEnumerable<Status> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Status.ToList();
            }
        }

        public Status Get(string moduleName)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Status.FirstOrDefault(p => p.ModuleName == moduleName);
            }
        }
    }
}
