﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;
using System.Data.Entity;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class VendorController : ApiController
    {
        //[HttpGet]
        //public ResponseMdl GetAllVendors(int PMID)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    List<MdlVendor> lstMdlVendor = new List<MdlVendor>();
        //    MdlVendor objMdlVendor = new MdlVendor();
        //    try
        //    {
        //        using (VataraEntities entities = new VataraEntities())
        //        {
        //            entities.Vendors.Where(v => v.PMID == PMID).ToList().ForEach(u =>
        //            {
        //                lstMdlVendor.Add(objMdlVendor.GetMdl_Vendor(u));
        //            });
        //        }
        //        response.Status = true;
        //        response.Data = lstMdlVendor;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Status = false;
        //        response.Message = clsConstants.somethingwrong;
        //        return response;
        //    }
        //    finally
        //    {
        //        objMdlVendor = null;
        //        lstMdlVendor = null;
        //    }
        //    return response;
        //}

        [HttpGet]
        public ResponseMdl GetAllVendors(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlVendor> lstMdlVendor = new List<MdlVendor>();
            MdlVendor objMdlVendor = new MdlVendor();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getVendorByPMID(PMID).ToList().ForEach(u =>
                    {
                        lstMdlVendor.Add(objMdlVendor.GetMdl_Vendor(u));
                    });
                }
                response.Status = true;
                response.Data = lstMdlVendor;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objMdlVendor = null;
                lstMdlVendor = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetVendorDetailByVendorId(int vendorId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlVendor> lstMdlVendor = new List<MdlVendor>();
            MdlVendor objMdlVendor = new MdlVendor();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getVendorById(vendorId).ToList().ForEach(u =>
                    {
                        lstMdlVendor.Add(objMdlVendor.GetMdl_Vendor(u));
                    });
                }
                response.Status = true;
                response.Data = lstMdlVendor;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objMdlVendor = null;
                lstMdlVendor = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetVendorServiceCategoryMaster()
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlVendorServiceCategoriesMaster> lstVendorServiceCategoriesMaster = new List<MdlVendorServiceCategoriesMaster>();
            MdlVendorServiceCategoriesMaster objMdlVendorServiceCategoriesMaster = new MdlVendorServiceCategoriesMaster();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.VendorServiceCategoriesMasters.ToList().ForEach(u =>
                    {
                        lstVendorServiceCategoriesMaster.Add(objMdlVendorServiceCategoriesMaster.GetMdl_VendorServiceCategoriesMaster(u));
                    });
                }
                response.Status = true;
                response.Data = lstVendorServiceCategoriesMaster;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objMdlVendorServiceCategoriesMaster = null;
                lstVendorServiceCategoriesMaster = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetVendorServiceCategories(int VendorId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlVendorServiceCategory> lstVendorServiceCategory = new List<MdlVendorServiceCategory>();
            MdlVendorServiceCategory objMdlVendorServiceCategory = new MdlVendorServiceCategory();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getVendorServiceCategories(VendorId).ToList().ForEach(u =>
                    {
                        lstVendorServiceCategory.Add(objMdlVendorServiceCategory.GetMdl_VendorServiceCategory(u));
                    });
                }
                response.Status = true;
                response.Data = lstVendorServiceCategory;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                objMdlVendorServiceCategory = null;
                lstVendorServiceCategory = null;
            }
            return response;
        }

        [HttpPost]
        public ResponseMdl CreateNewVendor([FromBody] MdlVendorRegistration mdl)
        {
            ResponseMdl response = new ResponseMdl();
            Address entityAddress = null;
            Vendor entityVendor = null;
            VendorServiceCategory entityVendorServiceCategory = null;
            MdlVendorServiceCategory objMdlVendorServiceCategory = new MdlVendorServiceCategory();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            entityAddress = new MdlAddress().GetEntity_Address(mdl.mdlAddress);
                            entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.personAddress.ToLower()).Id;
                            entityAddress.IsDeleted = false;
                            entityAddress.CreationTime = DateTime.UtcNow;
                            entityAddress.CreatorUserId = mdl.mdlVendor.creatorUserId;
                            ve.Addresses.Add(entityAddress);
                            ve.SaveChanges();

                            mdl.mdlVendor.addressId = entityAddress.Id;
                            entityVendor = new MdlVendor().GetEntity_Vendor(mdl.mdlVendor);
                            entityVendor.CreationTime = DateTime.UtcNow;
                            entityVendor.IsDeleted = false;
                            ve.Vendors.Add(entityVendor);
                            ve.SaveChanges();

                            for (int i = 0; i < mdl.mdlServiceCategories.Count; i++)
                            {
                                entityVendorServiceCategory = new VendorServiceCategory();
                                mdl.mdlServiceCategories[i].vendorId = entityVendor.VendorId;
                                entityVendorServiceCategory = objMdlVendorServiceCategory.GetEntity_VendorServiceCategory(mdl.mdlServiceCategories[i]);
                                entityVendorServiceCategory.CreatorUserId = mdl.mdlVendor.creatorUserId;
                                entityVendorServiceCategory.CreationTime = DateTime.UtcNow;
                                entityVendorServiceCategory.IsDeleted = false;
                                ve.VendorServiceCategories.Add(entityVendorServiceCategory);
                            }
                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Message = entityVendor.VendorId.ToString();
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                entityAddress = null;
                entityVendor = null;
                entityVendorServiceCategory = null;
                objMdlVendorServiceCategory = null;
            }
        }

        [HttpPost]
        public ResponseMdl UpdateVendor([FromBody] MdlVendorRegistration mdl)
        {
            ResponseMdl response = new ResponseMdl();
            Address entityAddress = null;
            Vendor entityVendor = null;
           // VendorServiceCategory entityVendorServiceCategory = null;
            MdlVendorServiceCategory objMdlVendorServiceCategory = new MdlVendorServiceCategory();
            clsVendorHelper objclsVendorHelper = new clsVendorHelper();
            List<MdlUpdateVendorServiceCategory> lstMdlUpdateVendorServiceCategory = new List<MdlUpdateVendorServiceCategory>();
            MdlUpdateVendorServiceCategory mdlUpdateVendorServiceCategory = null;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            entityAddress = ve.Addresses.FirstOrDefault(a => a.Id == mdl.mdlAddress.id);
                            entityAddress.Line1 = mdl.mdlAddress.line1;
                            entityAddress.CountryId = mdl.mdlAddress.countryId;
                            entityAddress.StateId  = mdl.mdlAddress.stateId;
                            entityAddress.CountyId = mdl.mdlAddress.countyId;
                            entityAddress.CityId = mdl.mdlAddress.cityId;
                            entityAddress.Zipcode = mdl.mdlAddress.zipcode;
                            entityAddress.LastModificationTime = DateTime.UtcNow;
                            entityAddress.LastModifierUserId = mdl.mdlVendor.creatorUserId;
                            ve.Addresses.Attach(entityAddress);                           
                            ve.Entry(entityAddress).State = EntityState.Modified;
                           
                            entityVendor = ve.Vendors.FirstOrDefault(v =>v.VendorId == mdl.mdlVendor.vendorId);
                            entityVendor.CompanyName = mdl.mdlVendor.companyName;
                            entityVendor.FirstName = mdl.mdlVendor.firstName;
                            entityVendor.LastName = mdl.mdlVendor.lastName;
                            entityVendor.Email = mdl.mdlVendor.email;
                            entityVendor.Phone = mdl.mdlVendor.phone;
                            entityVendor.Fax = mdl.mdlVendor.fax;
                            entityVendor.TwitterUrl = mdl.mdlVendor.twitterUrl;
                            entityVendor.FbUrl = mdl.mdlVendor.fbUrl;
                            entityVendor.LastModificationTime = DateTime.UtcNow;
                            entityVendor.LastModifierUserId = mdl.mdlVendor.creatorUserId;
                            ve.Vendors.Attach(entityVendor);
                            ve.Entry(entityVendor).State = EntityState.Modified;
                           // ve.SaveChanges();

                            for (int i = 0; i < mdl.mdlServiceCategories.Count; i++)
                            {                               
                                mdlUpdateVendorServiceCategory = new MdlUpdateVendorServiceCategory();
                                mdlUpdateVendorServiceCategory.serviceCategoryId = Convert.ToInt32(mdl.mdlServiceCategories[i].id);
                                mdlUpdateVendorServiceCategory.vendorId = Convert.ToInt32(mdl.mdlVendor.vendorId);
                                mdlUpdateVendorServiceCategory.creatorUserId = Convert.ToInt32(mdl.mdlVendor.creatorUserId);
                                lstMdlUpdateVendorServiceCategory.Add(mdlUpdateVendorServiceCategory);                               
                            }
                            objclsVendorHelper.SaveVendorServiceCategories(lstMdlUpdateVendorServiceCategory, mdl.mdlVendor.vendorId);
                            ve.SaveChanges();
                            //dbcontextTran.Rollback();
                            dbcontextTran.Commit();
                            response.Status = true;
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                entityAddress = null;
                entityVendor = null;
               // entityVendorServiceCategory = null;
                objMdlVendorServiceCategory = null;
                objclsVendorHelper = null;
                lstMdlUpdateVendorServiceCategory = null;
                mdlUpdateVendorServiceCategory = null;
            }
        }

        [HttpGet]
        public ResponseMdl CheckEmailExist(string email, int vendorId)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (vendorId == 0)
                    {
                        isExist = entities.Vendors.Any(e => e.Email.ToLower() == email.ToLower());
                    }
                    else
                    {
                        isExist = entities.Vendors.Any(e => e.Email.ToLower() == email.ToLower() && e.VendorId != vendorId);
                    }
                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }


        //[HttpPost]
        //public async Task<HttpResponseMessage> SaveFiles()
        //{
        //    var httpRequest = System.Web.HttpContext.Current.Request;
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
        //    string workorderId = string.Empty;
        //    string filePath = string.Empty;
        //    string imageIndex = string.Empty;
        //    string FileName = string.Empty;
        //    DateTime dtNow = DateTime.Now;
        //    Dictionary<string, object> dict = new Dictionary<string, object>();
        //    string imageNo = string.Empty;
        //    int MaxContentLength = 1024 * 1024 * 4; //Size = 6 MB 
        //    IList<string> AllowedExtns = new List<string>();
        //    string FileSavePath = "\\Uploads\\Workorder\\Documents\\";
        //    string FileType = "Documents";
        //    string ModuleId = clsConstants.WorkorderModule;
        //    AllowedExtns = new List<string> { ".doc", ".docx", ".txt", ".xls", ".xlsx", ".pdf", ".jpeg", ".jpg", ".gif", ".png" };

        //    try
        //    {
        //        var lstDocument = httpRequest.Form.GetValues("seqno");
        //        // var data = httpRequest.Form.GetValues("seqNo");

        //        for (int i = 0; i < httpRequest.Files.Count; i++)
        //        {
        //            response = Request.CreateResponse(HttpStatusCode.Created);
        //            var postedFile = httpRequest.Files[i];
        //            if (postedFile != null && postedFile.ContentLength > 0)
        //            {
        //                //int maxContentLength = 1024 * 1024 * 4; //Size = 4 MB  
        //                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //                var extension = ext.ToLower();
        //                if (!AllowedExtns.Contains(extension))
        //                {
        //                    var message = string.Format("Please upload valid file types.");

        //                    dict.Add("error" + i.ToString(), message);
        //                    response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
        //                }
        //                else if (postedFile.ContentLength > MaxContentLength)
        //                {
        //                    var message = string.Format("Please upload a file upto 4 mb.");

        //                    dict.Add("error" + i.ToString(), message);
        //                    response = Request.CreateResponse(HttpStatusCode.BadRequest, dict);
        //                }
        //                else
        //                {
        //                    var seqNo = "";

        //                    if (httpRequest.Form.GetValues("seqNo")[i] != "undefined")
        //                    {
        //                        seqNo = httpRequest.Form.GetValues("seqNo")[i];
        //                    }

        //                    char[] delimiters = new char[] { '\\' };
        //                    string[] extFilename = postedFile.FileName.Split(delimiters);

        //                    if (extFilename.Length > 0)
        //                    {
        //                        FileName = extFilename[extFilename.Length - 1];
        //                    }

        //                    workorderId = httpRequest.Form.GetValues("workorderId")[i];

        //                    if (FileType == "Photos")
        //                    {
        //                        imageNo = seqNo;
        //                        filePath = HttpRuntime.AppDomainAppPath + FileSavePath + string.Concat(workorderId, "-Image" + imageNo + ".png");
        //                    }
        //                    else
        //                        filePath = HttpRuntime.AppDomainAppPath + FileSavePath + string.Concat(workorderId, "-", postedFile.FileName);

        //                    postedFile.SaveAs(filePath);
        //                    var message1 = string.Format("Files Updated Successfully.");
        //                    dict.Add("success" + i.ToString(), message1);

        //                }
        //            }
        //        }

        //        using (VataraEntities ve = new VataraEntities())
        //        {
        //            if (lstDocument != null)
        //            {
        //                for (int i = 0; i < lstDocument.Length; i++)
        //                {
        //                    DataAccess.File fl = new DataAccess.File();
        //                    fl.LinkId = Convert.ToInt32(httpRequest.Form.GetValues("workorderId")[i]);
        //                    fl.ModuleId = httpRequest.Form.GetValues("moduleid")[i];
        //                    fl.FileType = FileType;
        //                    fl.Name = httpRequest.Form.GetValues("filename")[i];
        //                    Vatara.DataAccess.File fileV = ve.Files.FirstOrDefault(p => p.LinkId == fl.LinkId && p.ModuleId == fl.ModuleId && p.FileType == p.FileType && p.Name == fl.Name);
        //                    if (fileV != null)
        //                    {
        //                        fl = fileV;
        //                    }

        //                    fl.SeqNo = Convert.ToInt32(httpRequest.Form.GetValues("seqNo")[i]);
        //                    // postedFile.FileName;

        //                    fl.Description = httpRequest.Form.GetValues("filedesc")[i];
        //                    fl.FileSavePath = filePath;
        //                    fl.Tags = httpRequest.Form.GetValues("filetags")[i];

        //                    fl.LastModificationTime = dtNow;
        //                    fl.LastModifierUserId = 0;
        //                    if (httpRequest.Form.GetValues("idDeleted") != null)
        //                        fl.IsDeleted = Convert.ToBoolean(httpRequest.Form.GetValues("idDeleted")[i]);
        //                    if (fileV == null)
        //                    {
        //                        fl.CreationTime = dtNow;
        //                        fl.IsDeleted = false;
        //                        ve.Files.Add(fl);
        //                    }
        //                    ve.SaveChanges();
        //                }
        //            }
        //        }
        //        response = Request.CreateResponse(HttpStatusCode.Created, dict);
        //    }
        //    catch (Exception ex)
        //    {
        //        var res = string.Format("Exception in UploadFile: " + ex.ToString());
        //        dict.Add("ErrorHandler", res);
        //        response = Request.CreateResponse(HttpStatusCode.NotFound, dict);
        //    }
        //    finally
        //    {
        //        AllowedExtns = null;
        //    }
        //    return response;
        //}


        //public HttpResponseMessage GetVendorDocuments(int venderId)
        //{
        //    string FileType = "Documents";
        //    string ModuleId = clsConstants.WorkorderModule;
        //    using (VataraEntities entities = new VataraEntities())
        //    {
        //        var entity = entities.Files.Where(f => f.LinkId == venderId && f.FileType.ToUpper() == FileType.ToUpper() && f.ModuleId.ToUpper() == ModuleId.ToUpper() && f.IsDeleted == false).ToList();
        //        if (entity != null)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, entity);
        //        }
        //        else
        //        {
        //            return Request.CreateErrorResponse(HttpStatusCode.NoContent, "File with LinkId = " + venderId.ToString() + " Not Found");
        //        }
        //    }
        //}
    }


    public class MdlUpdateVendorServiceCategory
    {
        public int vendorId { get; set; }
        public int serviceCategoryId { get; set; }
        public int creatorUserId { get; set; }
        
    }
}
