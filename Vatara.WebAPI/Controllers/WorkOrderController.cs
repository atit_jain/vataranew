﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;


namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WorkorderController : ApiController
    {
        public HttpResponseMessage Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                var entity = entities.Workorders.FirstOrDefault(p => p.Id == id);
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NoContent, "Workorder with Id = " + id.ToString() + " Not Found");
                }
            }
        }
        [HttpGet]
        public ResponseMdl GetWorkOrderByStatus(string status, int pmid, int pagesize, int page, int propertyid = 0,int poid=0)
        {
            ResponseMdl response = new ResponseMdl();
            // List<MdlWorkOrderByStatus> lstMdlWorkOrderByStatus = new List<MdlWorkOrderByStatus>();
            MdlWorkOrderWithtotalCount mdlWorkOrderWithtotalCount = new MdlWorkOrderWithtotalCount();
            MdlWorkOrderByStatus mdlWorkOrderByStatus = new MdlWorkOrderByStatus();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var workorder = entities.GetWorkOrderByStatus(pmid, status, propertyid,poid).ToList();
                    mdlWorkOrderWithtotalCount.TotalWorkOrderCount = workorder.Count();
                    workorder.Skip(pagesize * (page - 1)).Take(pagesize * 3).ToList().ForEach(u =>
                          {
                              mdlWorkOrderWithtotalCount.lstMdlWorkOrderByStatus.Add(mdlWorkOrderByStatus.GetMdl_WorkOrderByStatus(u));
                          });
                }
                response.Data = mdlWorkOrderWithtotalCount;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlWorkOrderByStatus = null;
                mdlWorkOrderWithtotalCount = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetWorkOrderWithWorkRequests(int workOrderId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlWorkOrderAndWorkRequests mdl = new MdlWorkOrderAndWorkRequests();
            MdlWorkorder mdlWorkorder = new MdlWorkorder();
            MdlWorkrequest mdlWorkrequest = new MdlWorkrequest();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var workorder = entities.Workorders.FirstOrDefault(wo => wo.Id == workOrderId);
                    if (workorder != null)
                    {
                        mdl.Workorder = mdlWorkorder.GetMdl_Workorder(workorder);
                        entities.getWorkRequestsByWorkorderId(workOrderId).ToList().ForEach(u =>
                        {
                            mdl.lstWorkrequest.Add(mdlWorkrequest.GetMdl_Workrequest(u));
                        });
                    }
                }
                response.Data = mdl;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdl = null;
                mdlWorkorder = null;
                mdlWorkrequest = null;
            }
        }


        [HttpGet]
        public ResponseMdl GetWorkRequestsByProperty(int propertyId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlWorkRequestsByProperty> lstWorkRequestsByProperty = new List<MdlWorkRequestsByProperty>();
            MdlWorkRequestsByProperty MdlWorkRequestsByProperty = new MdlWorkRequestsByProperty();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getWorkRequestsByPropertyId(propertyId).ToList().ForEach(u =>
                    {
                        lstWorkRequestsByProperty.Add(MdlWorkRequestsByProperty.GetMdl_WorkRequestsByProperty(u));
                    });
                }
                response.Data = lstWorkRequestsByProperty;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                MdlWorkRequestsByProperty = null;
                lstWorkRequestsByProperty = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetWorkOrderStatus()
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlStatus> lstMdlStatus = new List<MdlStatus>();
            MdlStatus mdlStatus = new MdlStatus();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.Status.Where(s => s.ModuleName.ToUpper() == clsConstants.WorkorderModule.ToUpper() && s.Name.ToUpper() != clsConstants.stsNew.ToUpper()).ToList().ForEach(u =>
                    {
                        lstMdlStatus.Add(mdlStatus.GetMdl_Status(u));
                    });
                };
                response.Data = lstMdlStatus;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMdlStatus = null;
                mdlStatus = null;
            }
        }

        [HttpPost]
        public ResponseMdl SaveWorkOrder([FromBody] MdlWorkOrderAndWorkRequests mdl)
        {
            ResponseMdl response = new ResponseMdl();
            Workorder workorder = new Workorder();
            MdlWorkorder mdlWorkorder = new MdlWorkorder();
            MdlWorkrequest mdlWorkrequest = new MdlWorkrequest();
            Workrequest objWorkrequest = null;            
            
            int status = 0;           
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            var entityEtatus = ve.Status.FirstOrDefault(s => s.Name.ToUpper() == clsConstants.open.ToUpper() && s.ModuleName.ToUpper() == clsConstants.WorkorderModule.ToUpper());
                            if (entityEtatus != null)
                            {
                                status = entityEtatus.Id;
                            }
                            workorder = mdlWorkorder.GetEntityWorkorder(mdl.Workorder);
                            workorder.IsDeleted = false;
                            workorder.CreationTime = DateTime.UtcNow;
                            workorder.Status = status;
                            ve.Workorders.Add(workorder);
                            ve.SaveChanges();

                            for (int i = 0; i < mdl.lstWorkrequest.Count; i++)
                            {                               
                                objWorkrequest = new Workrequest();
                                objWorkrequest = mdlWorkrequest.GetEntityWorkrequest(mdl.lstWorkrequest[i]);
                                objWorkrequest.WorkorderId = workorder.Id;
                                objWorkrequest.Status = status;
                                objWorkrequest.IsDeleted = false;
                                objWorkrequest.CreationTime = DateTime.UtcNow;
                                objWorkrequest.CreatorUserId = mdl.Workorder.creatorUserId;
                                ve.Workrequests.Add(objWorkrequest);
                                objWorkrequest = null;
                            }
                            ve.SaveChanges();
                            ve.proc_SetWorkOrderStartAndEndDate(workorder.Id);                          
                            ve.insertWorkOrderCreationNotification(mdl.PMID, workorder.PropertyId,workorder.RequestedByPersonType,workorder.Id, "INSERT");
                            
                            dbcontextTran.Commit();                         
                            response.Status = true;
                            response.Message = workorder.Id.ToString();
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                workorder = null;
                mdlWorkorder = null;
                mdlWorkrequest = null;
                objWorkrequest = null;
            }
        }

        [HttpPost]
        public ResponseMdl UpdateWorkOrder([FromBody] MdlWorkOrderAndWorkRequests mdl)
        {
            ResponseMdl response = new ResponseMdl();
            Workorder workorder = new Workorder();
            Workrequest objWorkrequest = null;
            int workorderid = 0;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            workorder = ve.Workorders.FirstOrDefault(wo => wo.Id == mdl.Workorder.id);
                            workorder.Description = mdl.Workorder.description;
                            workorder.RequestedDate = mdl.Workorder.requestedDate.Value.ToUniversalTime();
                            workorder.RequestedBy = mdl.Workorder.requestedBy;
                            workorder.RequestedByPersonType = mdl.Workorder.requestedByPersonType;
                            workorder.LastModificationTime = DateTime.UtcNow;
                            workorder.LastModifierUserId = mdl.Workorder.creatorUserId;
                            ve.Workorders.Attach(workorder);
                            ve.Entry(workorder).State = EntityState.Modified;
                            ve.SaveChanges();
                            for (int i = 0; i < mdl.lstWorkrequest.Count; i++)
                            {
                                workorderid = mdl.lstWorkrequest[i].id;
                                objWorkrequest = new Workrequest();
                                objWorkrequest = ve.Workrequests.FirstOrDefault(wr => wr.WorkorderId == mdl.Workorder.id && wr.Id == workorderid);
                                objWorkrequest.Description = mdl.lstWorkrequest[i].description;                               
                                objWorkrequest.InvoiceNumber = mdl.lstWorkrequest[i].invoiceNumber;
                                objWorkrequest.PONumber = mdl.lstWorkrequest[i].poNumber;
                                objWorkrequest.ServiceCategoryId = mdl.lstWorkrequest[i].serviceCategoryId;
                                objWorkrequest.LastModificationTime = DateTime.UtcNow;
                                objWorkrequest.LastModifierUserId = mdl.Workorder.creatorUserId;
                                ve.Workrequests.Attach(objWorkrequest);
                                ve.Entry(objWorkrequest).State = EntityState.Modified;
                                objWorkrequest = null;
                            }
                            ve.SaveChanges();
                            ve.proc_SetWorkOrderStartAndEndDate(workorder.Id);
                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Message = workorder.Id.ToString();
                            return response;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                workorder = null;
                objWorkrequest = null;
            }
        }

        //[HttpPost]
        //public ResponseMdl DeleteWorkOrder([FromBody] MdlWorkOrderStatus mdl)
        //{
        //    Workorder entityWorkorder = null;
        //    List<Workrequest> lstWorkrequest = new List<Workrequest>();
        //    ResponseMdl response = new ResponseMdl();
        //    int status = 0;
        //    try
        //    {
        //        using (VataraEntities ve = new VataraEntities())
        //        {
        //            using (var dbcontextTran = ve.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    entityWorkorder = ve.Workorders.FirstOrDefault(wo => wo.Id == mdl.workorderId);
        //                    var Workorderstatus = ve.Status.FirstOrDefault(s => s.Name.ToUpper() == clsConstants.WorkOrderCancelled && s.ModuleName.ToUpper() == clsConstants.WorkorderModule);
        //                    if (Workorderstatus != null)
        //                    {
        //                        status = Workorderstatus.Id;
        //                    }
        //                    entityWorkorder.Status = status;
        //                    entityWorkorder.DeleterUserId = mdl.userId;
        //                    entityWorkorder.DeletionTime = DateTime.Now;
        //                    entityWorkorder.IsDeleted = true;

        //                    lstWorkrequest = ve.Workrequests.Where(wr => wr.WorkorderId == mdl.workorderId).ToList();
        //                    for (int i = 0; i < lstWorkrequest.Count; i++)
        //                    {
        //                        lstWorkrequest[i].IsDeleted = true;
        //                        lstWorkrequest[i].DeletionTime = DateTime.Now;
        //                        lstWorkrequest[i].Status = status;
        //                        lstWorkrequest[i].DeleterUserId = mdl.userId;
        //                        ve.Workrequests.Attach(lstWorkrequest[i]);
        //                        ve.Entry(lstWorkrequest[i]).State = EntityState.Modified;
        //                    }
        //                    ve.Workorders.Attach(entityWorkorder);
        //                    ve.Entry(entityWorkorder).State = EntityState.Modified;
        //                    ve.SaveChanges();
        //                    dbcontextTran.Commit();
        //                    response.Status = true;
        //                    response.Message = clsConstants.WorkorderDeletedSuccess;
        //                }
        //                catch (Exception ex)
        //                {
        //                    dbcontextTran.Rollback();
        //                    response.Status = false;
        //                    response.Message = clsConstants.somethingwrong.ToString();
        //                    return response;
        //                }
        //            }
        //        }
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Status = false;
        //        response.Message = clsConstants.somethingwrong;
        //        return response;
        //    }
        //    finally
        //    {
        //        entityWorkorder = null;
        //        lstWorkrequest = null;
        //    }
        //}

        [HttpPost]
        public ResponseMdl AddUpdateWorkRequests([FromBody] MdlWorkrequest mdl)
        {
            ResponseMdl response = new ResponseMdl();
            MdlWorkrequest mdlWorkrequest = new MdlWorkrequest();
            List<MdlWorkrequest> lstWorkrequest = new List<MdlWorkrequest>();
            int result = 0;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    
                    ObjectParameter returnId = new ObjectParameter("Output", typeof(int));
                    ve.addUpdateWorkRequest(mdl.id, mdl.workorderId, mdl.description, mdl.poNumber, mdl.invoiceNumber,
                         mdl.serviceCategoryId, mdl.vendorId, mdl.status,
                         mdl.creatorUserId,mdl.expectedStartDate,mdl.expectedCompletionDate.Value.ToLocalTime(), returnId).ToList().ForEach(u =>
                         {
                            lstWorkrequest.Add(mdlWorkrequest.GetMdl_Workrequest(u));
                         }); 

                    result = Convert.ToInt32(returnId.Value);
                    if (result == 1)
                    {
                        response.Status = true;
                        response.Data = lstWorkrequest;
                    }
                    else
                        response.Status = false;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                mdlWorkrequest = null;
                lstWorkrequest = null;
            }
        }

        [HttpPost]
        public ResponseMdl DeleteWorkRequest([FromBody] MdlDeleteWorkRequest mdl)
        {
            Workrequest workrequest = null;
            ResponseMdl response = new ResponseMdl();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            workrequest = ve.Workrequests.FirstOrDefault(wr => wr.Id == mdl.workRequestId);
                            if (workrequest != null)
                            {
                                workrequest.IsDeleted = true;
                                workrequest.DeletionTime = DateTime.UtcNow;
                                workrequest.DeleterUserId = mdl.deleteruserId;
                                ve.Workrequests.Attach(workrequest);
                                ve.Entry(workrequest).State = EntityState.Modified;
                                ve.SaveChanges();
                                dbcontextTran.Commit();
                                response.Status = true;
                                response.Message = clsConstants.WorkrequestDeletedSuccess;
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = clsConstants.WorkrequestDeleteError;
                                return response;
                            }
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                workrequest = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetPropertyOwnerTenantManager(int propertyId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertyOwnerTenantManager> lstMdlPropertyOwnerTenantManager = new List<MdlPropertyOwnerTenantManager>();
            MdlPropertyOwnerTenantManager mdlPropertyOwnerTenantManager = new MdlPropertyOwnerTenantManager();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyOwnerTenantManager(propertyId).ToList().ForEach(u =>
                    {
                        lstMdlPropertyOwnerTenantManager.Add(mdlPropertyOwnerTenantManager.GetMdl_PropertyOwnerTenantManager(u));
                    });
                }
                response.Data = lstMdlPropertyOwnerTenantManager;
                response.Status = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                lstMdlPropertyOwnerTenantManager = null;
                mdlPropertyOwnerTenantManager = null;
            }
        }

        [HttpPost]
        public ResponseMdl ChangeWorkOrderStatus([FromBody] MdlWorkOrderStatus mdl)
        {
            ResponseMdl response = new ResponseMdl();
            int status = 0;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ObjectParameter returnId = new ObjectParameter("output", typeof(int));
                    status = ve.changeWorkOrderStatus(mdl.workorderId, mdl.status, mdl.userId, returnId);
                    status = Convert.ToInt32(returnId.Value);
                    if (status == 1)
                    {
                        response.Status = true;
                    }                 
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
        }


        [HttpPost]
        public ResponseMdl UpdateWorkRequestStatus([FromBody] mdlUpdateWorkRequest mdl)
        {
            ResponseMdl response = new ResponseMdl();
            int result = 0;
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ObjectParameter returnId = new ObjectParameter("Output", typeof(int));
                    result = ve.proc_ChangeWorkRequestStatus(mdl.WorkRequestId, mdl.Status, mdl.UserId, returnId);                   
                    result = Convert.ToInt32(returnId.Value);
                    if (result == 1)
                        response.Status = true;
                    else
                        response.Status = false;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }            
        }

        [HttpGet]
        public ResponseMdl PropertByManagerAndOwner(int managerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertOwnersWithManager(managerId).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = u.OwnerId, propertyId = u.PropertyId, propertyName = u.PropertyName, ownerIsCompany = u.ownerIsCompany, personId = u.ownerPersonId });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByTenant_Owner(int PMID, int personId, int propoertyId, int personType)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyByOwnerPersonId(personId).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = personType });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

    }

    public class mdlUpdateWorkRequest
    {
        public int WorkRequestId { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }       
    }

    public class MdlWorkOrderWithtotalCount
    {
        public MdlWorkOrderWithtotalCount()
        {
            lstMdlWorkOrderByStatus = new List<MdlWorkOrderByStatus>();
        }
        public int TotalWorkOrderCount { get; set; }
        public List<MdlWorkOrderByStatus> lstMdlWorkOrderByStatus { get; set; }
    }

    public class MdlWorkOrderStatus
    {
        public int workorderId { get; set; }
        public int userId { get; set; }
        public string status { get; set; }
    }

    public class MdlDeleteWorkRequest
    {
        public int workRequestId { get; set; }
        public int deleteruserId { get; set; }
    }

    //public class MdlChangeWorkRequestStatus
    //{
    //    public int workRequestId { get; set; }
    //    public string status { get; set; }
    //    public int modifierId { get; set; }
    //}
}
