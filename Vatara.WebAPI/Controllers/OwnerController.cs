﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class OwnerController : ApiController
    {
        [HttpGet]
        public ResponseMdl GetOwnerDetail(int POID,int PersonId)
        {
            ResponseMdl response = new ResponseMdl();
            MdlOwnerInfo MdlOwnerInfo = new MdlOwnerInfo();
            MdlProperty mdlProperty = new MdlProperty();
            MdlPerson mdlPerson = new MdlPerson();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    // MdlOwnerInfo.PersonInfo = new MdlPerson().GetMdl_Owner(entities.getOwnerDetail(POID, "POID").FirstOrDefault());/* vwPersons.Where(e => e.pId == ownerId).FirstOrDefault()*/

                    MdlOwnerInfo.PersonInfo = mdlPerson.GetMdl_Owner(entities.getOwnerDetailByOwnerPersonIdAndPOID(PersonId, POID).FirstOrDefault());
                    entities.getPropertyByOwnerPOID(PersonId,POID).ToList().ForEach(u =>
                    {
                        MdlOwnerInfo.lstMdlProperty.Add(mdlProperty.GetMdl_PropertyByOwnerPOID(u));
                    });
                    entities.getPropertiesByPerson(PersonId, (int)clsCommon.PersonType.Owner, POID).ToList().ForEach(u =>
                    {
                        MdlOwnerInfo.lstMdlPropertOwnersWithManager.Add(new MdlPropertOwnersWithManager { propertyName = u.PropName + " | " + u.PropAddress, propertyId = u.PropId });
                    });
                    response.Status = true;
                    response.Data = MdlOwnerInfo;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                MdlOwnerInfo = null;
                mdlProperty = null;
                mdlPerson = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetOwnersByPMID(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlOwnersByPMID> lstMdlOwnersByPMID = new List<MdlOwnersByPMID>();
            MdlOwnersByPMID mdlOwnersByPMID = new MdlOwnersByPMID(); ;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {                  
                    entities.getAllOwnersByPMID(PMID).ToList().ForEach(u =>
                    {
                        lstMdlOwnersByPMID.Add(mdlOwnersByPMID.GetMdl_OwnersByPMID(u));
                    });
                    response.Status = true;
                    response.Data = lstMdlOwnersByPMID;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                mdlOwnersByPMID = null;
                lstMdlOwnersByPMID = null;
            }
            return response;
        }

        [HttpPost]
        public ResponseMdl SaveOwner([FromBody] MdlPropertyOwnerDetail data)
        {
            ResponseMdl response = new ResponseMdl();
            OwnerMaster objOwnerMaster = new OwnerMaster();
            User objUser = null;
            Person objperson = null;
            PersonOwner objPersonOwner = null;
            int personTypeId = 0;
            int statusId = 0;
            try
            {                           
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            //ownerMaster Table
                            objOwnerMaster.CompanyName = data.OwnerMaster.CompanyName;
                            objOwnerMaster.Email = data.OwnerMaster.Email;
                            objOwnerMaster.ContactPerson = data.OwnerMaster.ContactPerson;
                            objOwnerMaster.ContactPerson = data.OwnerMaster.ContactPerson;
                            objOwnerMaster.IsActive = true;
                            objOwnerMaster.CreationTime = DateTime.UtcNow;
                            objOwnerMaster.IsDeleted = false;
                            if (data.Ownertype.ToLower() == clsConstants.individualOwner.ToLower())
                                objOwnerMaster.OwnerType = clsConstants.individualOwner.ToLower();
                            else
                                objOwnerMaster.OwnerType = clsConstants.companyOwner.ToLower();

                            ve.OwnerMasters.Add(objOwnerMaster);
                            ve.SaveChanges();
                            personTypeId = ve.PersonTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.Owner.Trim().ToLower()).Id;
                            statusId = ve.Status.FirstOrDefault(s => s.Name.ToLower() == clsConstants.PersonStatusInActive.ToLower() && s.ModuleName.ToLower() == clsConstants.Personmodule.ToLower()).Id;

                            foreach (var person in data.lstMdlOwner)
                            {
                                 objUser = new User();
                                 objperson = new Person();
                                 objPersonOwner = new PersonOwner();

                                //userTable
                                objUser.Email = person.pEmail;
                                objUser.Password = new Encryption().Encrypt(new clsCommon().ReturnRandomAlphaNumericString(8));
                                objUser.IsDeleted = false;
                                objUser.CreationTime = DateTime.UtcNow;
                                objUser.IsVerified = false;
                                objUser.VerificationId = Guid.NewGuid().ToString();
                                objUser.CreatorUserId = data.creatorId;
                                ve.Users.Add(objUser);
                                ve.SaveChanges();

                                //personTable

                                objperson.FirstName = person.pFirstName;
                                objperson.LastName = person.pLastName;
                                objperson.Email = person.pEmail;
                                objperson.PhoneNo = person.pPhoneNo;
                                objperson.Status = statusId;
                                objperson.UserId = objUser.Id;
                                objperson.PersonTypeId = personTypeId;
                                objperson.AddressId = 0;
                                objperson.IsDeleted = false;
                                objperson.CreationTime = DateTime.UtcNow;
                                objperson.CreatorUserId = data.creatorId;
                                ve.People.Add(objperson);
                                ve.SaveChanges();

                                //personOwner table

                                objPersonOwner.POID = objOwnerMaster.POID;
                                objPersonOwner.PersonID = objperson.Id;
                                objPersonOwner.IsActive = true;
                                objPersonOwner.IsPrimaryOwner = person.Isprimary;
                                //objPersonOwner.Role = person.Isprimary == true ? "PO" : "SO";
                                ve.PersonOwners.Add(objPersonOwner);
                                ve.SaveChanges();
                            }
                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Data = objOwnerMaster;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                objOwnerMaster = null;
                objUser = null;
                objperson = null;
                objPersonOwner = null;
            }
            // return response;
        }

        [HttpPost]
        public ResponseMdl UpdateOwnerDetail([FromBody] MdlOwnerData data)
        {
            ResponseMdl response = new ResponseMdl();
            OwnerMaster objOwnerMaster = new OwnerMaster();
            Address objAddress = new Address();
            Person objPerson = new Person();
            //User objUser = new User();
            //Person objPerson = new Person();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    using (var dbcontextTran = ve.Database.BeginTransaction())
                    {
                        try
                        {
                            //  objPerson = ve.People.FirstOrDefault(p => p.Id == data.personId);
                            ////  objPerson.FirstName
                            if (data.ownerData.OwnerType == "INDIVIDUAL")
                            {
                                objPerson = ve.People.FirstOrDefault(p => p.Id == data.ownerData.Id);
                                objPerson.FirstName = data.ownerData.FirstName;
                                objPerson.LastName = data.ownerData.LastName;
                                objPerson.PhoneNo = data.ownerData.ContactNum;
                                ve.People.Attach(objPerson);
                                ve.Entry(objPerson).State = EntityState.Modified;
                            }
                            else
                            {
                                objOwnerMaster = ve.OwnerMasters.FirstOrDefault(o => o.POID == data.ownerData.POID);
                                objOwnerMaster.CompanyName = data.ownerData.CompanyName;
                                objOwnerMaster.ContactNum = data.ownerData.ContactNum;
                                objOwnerMaster.LastModificationTime = DateTime.UtcNow;
                                objOwnerMaster.LastModifierUserId = data.ownerData.CreatorUserId;
                                ve.OwnerMasters.Attach(objOwnerMaster);
                                ve.Entry(objOwnerMaster).State = EntityState.Modified;
                            }
                            objAddress = ve.Addresses.FirstOrDefault(a => a.Id == data.ownerData.AddressId);
                            objAddress.Line1 = data.mdlAddress.line1;
                            objAddress.Line2 = data.mdlAddress.line2;
                            objAddress.CityId = data.mdlAddress.cityId;
                            objAddress.CountyId = data.mdlAddress.countyId;
                            objAddress.StateId = data.mdlAddress.stateId;
                            objAddress.CountryId = data.mdlAddress.countryId;
                            objAddress.Zipcode = data.mdlAddress.zipcode;
                            ve.Addresses.Attach(objAddress);
                            ve.Entry(objAddress).State = EntityState.Modified;

                            ve.SaveChanges();
                            dbcontextTran.Commit();
                            response.Status = true;
                            response.Data = objOwnerMaster;
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong.ToString();
                            return response;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                objOwnerMaster = null;
                objAddress = null;
                objPerson = null;
            }
            // return response;
        }

        [HttpGet]
        public ResponseMdl CheckEmailAvailability(string email,string OwnerType)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (OwnerType.ToUpper() == "COMPANY")
                    {
                        isExist = entities.OwnerMasters.Any(e => e.Email.ToLower() == email.ToLower());
                        response.Status = true;
                        response.Data = isExist;
                    }
                    else
                    {
                        isExist = entities.People.Any(e => e.Email.ToLower() == email.ToLower());
                        response.Status = true;
                        response.Data = isExist;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }

            return response;
        }

        [HttpGet]
        public ResponseMdl GetOwnerMasterByPOID(int POID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            List<getOwnerMasterByPOID_Result> lstownerMaster = new List<getOwnerMasterByPOID_Result>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lstownerMaster = entities.getOwnerMasterByPOID(POID).ToList();
                    response.Status = true;
                    response.Data = lstownerMaster;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstownerMaster = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetPersonOwnerByPMID(int PMID = 0)
        {
            ResponseMdl response = new ResponseMdl();
            List<getPersonOwnerByPMID_Result> lstownerPerson = new List<getPersonOwnerByPMID_Result>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    lstownerPerson = entities.getPersonOwnerByPMID(PMID).ToList();
                    response.Status = true;
                    response.Data = lstownerPerson;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstownerPerson = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByOwner(int ownerId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {                    
                    entities.getPropertyByOwnerPersonId(ownerId).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = (int)clsCommon.PersonType.Owner });
                    });

                    response.Status = true;
                    response.Data = PropertOwners;
                    //  return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl PropertByOwnerPOID(int POID,int PersonId = 0) // change PersonId from optional to mendatory
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> PropertOwners = new List<MdlPropertOwnersWithManager>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.getPropertyByOwnerPOID(PersonId,POID).Select(e => new { e.pId, e.pName, e.pOwnerId }).Distinct().ToList().ForEach(u =>
                    {
                        PropertOwners.Add(new MdlPropertOwnersWithManager { ownerId = (int)u.pOwnerId, propertyId = u.pId, propertyName = u.pName, personTypeId = (int)clsCommon.PersonType.Owner });
                    });
                    response.Status = true;
                    response.Data = PropertOwners;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                PropertOwners = null;
            }
            return response;
        }

        [HttpGet]
        public ResponseMdl GetCompanyByMangerID(int PMID)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlCompany> lstCompany = new List<MdlCompany>();
            MdlCompany mdlCompany = new MdlCompany();
            try {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.GetCompanyByManagerID(PMID).Distinct().ToList().ForEach(u =>
                    {
                        lstCompany.Add(mdlCompany.GetMdl_Company(u));
                    });
                    response.Status = true;
                    response.Data = lstCompany;
                    return response;
                }
            }
            catch (Exception ex) {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                response = null;
            }
        }
    }
}
