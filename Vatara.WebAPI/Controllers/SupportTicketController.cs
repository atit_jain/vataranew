﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class SupportTicketController : ApiController
    {
        [HttpPost]
        public ResponseMdl SaveSupportTicket([FromBody] MdlSupportTicket data)
        {
            ResponseMdl response = new ResponseMdl();
            SupportTicket SupportTicket = new SupportTicket();
            EmailData EmailData = new EmailData();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    int status = entities.Status.Where(x => x.ModuleName == clsConstants.SupportTicket && x.Name == clsConstants.open).FirstOrDefault().Id;
                    SupportTicket.Description = data.Description;
                    SupportTicket.UserId = data.UserId;
                    SupportTicket.Status = status;
                    SupportTicket.CompanyMgrId = data.CompanyMgrId;
                    SupportTicket.TicketNo = new clsCommon().ReturnRandomAlphaNumericString(8);
                    SupportTicket.Response = null;
                    SupportTicket.CreatedDate = DateTime.UtcNow;
                    entities.SupportTickets.Add(SupportTicket);
                    EmailData.From = data.FromEmail;
                    EmailData.To = ConfigurationManager.AppSettings["VataraEmail"].ToString();
                    EmailData.Subject = "Customer Support";
                    EmailData.Purpose = "SupportTicket";
                    EmailData.IsSent = false;
                    EmailData.ReSend = false;
                    EmailData.CreationTime = DateTime.UtcNow;
                    entities.EmailDatas.Add(EmailData);
                    entities.SaveChanges();
                    response.Status = true;
                }
            }

            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
            }
            finally
            {
                SupportTicket = null;
                EmailData = null;
            }

            return response;

        }
        [HttpGet]
        public ResponseMdl GetAllSupportTicket(int UserId, bool IncludeClosedStatus = false)
        {
            ResponseMdl response = new ResponseMdl();
            List<vwSupportTicket> lstSupportTicket = new List<vwSupportTicket>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    if (!IncludeClosedStatus)
                    {
                        entities.vwSupportTickets.Where(x => x.UserId == UserId && x.ModuleName == clsConstants.SupportTicket && x.statusName == clsConstants.open).ToList().ForEach(u =>
                       {
                           vwSupportTicket SupportTicket = new vwSupportTicket();
                           SupportTicket.TicketId = u.TicketId;
                           SupportTicket.TicketNo = u.TicketNo;
                           SupportTicket.Description = u.Description;
                           SupportTicket.statusName = u.statusName;
                           SupportTicket.CreatedDate = u.CreatedDate.Date.ToUniversalTime();
                           lstSupportTicket.Add(SupportTicket);
                       });
                    }
                    else
                    {
                        entities.vwSupportTickets.Where(x => x.UserId == UserId && x.ModuleName == clsConstants.SupportTicket && x.statusName == clsConstants.open || x.statusName == clsConstants.closed).ToList().ForEach(u =>
                         {
                             vwSupportTicket SupportTicket = new vwSupportTicket();
                             SupportTicket.TicketId = u.TicketId;
                             SupportTicket.TicketNo = u.TicketNo;
                             SupportTicket.Description = u.Description;
                             SupportTicket.statusName = u.statusName;
                             SupportTicket.CreatedDate = u.CreatedDate.ToUniversalTime();
                             lstSupportTicket.Add(SupportTicket);
                         });
                    }
                    response.Status = true;
                    response.Data = lstSupportTicket;
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            { lstSupportTicket = null; }

            return response;
        }

        [HttpGet]
        public ResponseMdl UpdateTicketStatus(int TicketId)
        {
            ResponseMdl response = new ResponseMdl();
            SupportTicket SupportTicket = new SupportTicket();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    int status = entities.Status.Where(x => x.ModuleName == clsConstants.SupportTicket && x.Name == clsConstants.closed).FirstOrDefault().Id;
                    SupportTicket = entities.SupportTickets.Where(x => x.TicketId == TicketId).FirstOrDefault();
                    SupportTicket.Status = status;
                    entities.Entry(SupportTicket).State = EntityState.Modified;
                    entities.SaveChanges();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            { SupportTicket = null; }

            return response;
        }
        public ResponseMdl GetAllFaq(string Section)
        {
            ResponseMdl response = new ResponseMdl();
            List<FAQ> Faq = new List<FAQ>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    Faq = entities.FAQs.Where(x => x.Section == "Property").ToList();
                    response.Status = true;
                    response.Data = Faq;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            { Faq = null; }

            return response;

        }
    }
}
