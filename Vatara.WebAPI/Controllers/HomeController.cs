﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Vatara.WebAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        [HttpPost]       
        public FileResult Export(string[] gridHtml, string pagesize = "")
        {
            var obj = gridHtml;
            string str = string.Empty;
            StringReader sr = null;
            Document pdfDoc = null;
            PdfWriter writer = null;

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                try
                {
                    if (pagesize == "")
                    {
                        pdfDoc = new Document(PageSize.A3);
                    }
                    else
                    {
                        pdfDoc = new Document(PageSize.A4);
                    }

                    writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();

                    for (int i = 0; i < gridHtml.Count(); i++)
                    {
                        str = string.Empty;
                        str = gridHtml[i].ToString();

                        sr = null;
                        sr = new StringReader(str);
                        if (i > 0)
                        {
                            pdfDoc.NewPage();
                        }
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    }
                    pdfDoc.Close();                    
                    return File(stream.ToArray(), "application/pdf", "Grid.pdf");

                    //return stream.ToArray();                   
                }
                catch (Exception ex)
                {
                    throw;
                    // return File(stream.ToArray(), "application/pdf", "Grid.pdf");
                }
            }


        }

        [HttpGet]
        public ActionResult RedirectToMobileSchema(string result,string message)
        {
           

            string url = string.Empty;
            if (result == "Success")
            {
                url = "vataramobileschema://vatara?status=success";
            }
            else if (result == "Cancel")
            {
                url = "vataramobileschema://vatara?status=Cancel";
            }
            else
            {
                url = "vataramobileschema://vatara?status=failure&&message=" + message;
            }
            return Redirect(url);
        }


    }
}
