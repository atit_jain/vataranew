﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Security;
using Vatara.DataAccess;
using System.Data.Entity;
using Vatara.WebAPI.Class;
using System.Configuration;
using System.Text.RegularExpressions;
using Vatara.WebAPI.Helper;
using Vatara.WebAPI.Models;

namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class LoginController : ApiController
    {
        public int GetMdl_SubscriptionDetail { get; private set; }

        [AllowAnonymous]
        [HttpPost]
        public ResponseMdl SignIn(MdlUser user)
        {
            ResponseMdl response = new ResponseMdl();
            string encrypt = string.Empty;
            vwUserPerson vwUserPerson = null;
            bool IsManager = false;
            vwCompanyPropertyOwner Owner = null;
            vwCompanyPropertyTenant tenant = null;
            vwManagerMaster ManagerMaster = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    encrypt = new Encryption().Encrypt(user.password);
                    user.password = new Encryption().Encrypt(user.password);
                    vwUserPerson = entities.vwUserPersons.FirstOrDefault(u => u.Email.ToLower() == user.userName.ToLower() && u.Password == encrypt);

                    if (vwUserPerson != null)
                    {

                        user.userName = vwUserPerson.Email;   // if user types his email in any case and if that email and password is correct 
                                                              //irrespective of case in email then replace the email with correct case email so that this not create any problem to chat.
                        if (!string.IsNullOrEmpty(user.comProfile))
                        {
                            user.comProfile = Regex.Replace(user.comProfile, ".vatara.com", "", RegexOptions.IgnoreCase); // user.comProfile.Replace(".vatara.com", "");
                            ManagerMaster = entities.vwManagerMasters.FirstOrDefault(e => e.URL.ToLower() == user.comProfile.ToLower());
                            if (ManagerMaster == null)
                            {
                                response.Status = false;
                                response.Data = clsConstants.WrongUrl;
                                response.Message = clsConstants.WrongUrl;
                                return response;
                            }
                            //if (vwUserPerson != null)
                            //{
                            user.pmid = ManagerMaster.PMID;
                            if (vwUserPerson.Type.ToLower() == clsConstants.propertyManager.ToLower())
                            {
                                var personManager = entities.PersonManagers.FirstOrDefault(p => p.PersonID == vwUserPerson.PersonId);
                                if (personManager != null)
                                {
                                    if (user.pmid == personManager.PMID)
                                    {
                                        IsManager = true;
                                        response = SignInProcess(encrypt, user, entities, IsManager, vwUserPerson);
                                    }
                                }
                                else
                                {
                                    response.Status = false;
                                    response.Data = clsConstants.EmailAndCompanyNotMatched;
                                }
                            }
                            else if (vwUserPerson.Type.ToLower() == clsConstants.Owner.ToLower() || vwUserPerson.Type.ToLower() == clsConstants.Tenant.ToLower())
                            {
                                if (vwUserPerson.Type.ToLower() == clsConstants.Owner.ToLower())
                                {
                                    Owner = entities.vwCompanyPropertyOwners.FirstOrDefault(o => o.OwnerId == vwUserPerson.PersonId);
                                }
                                else
                                {
                                    tenant = (entities.vwCompanyPropertyTenants.FirstOrDefault(o => o.TenantId == vwUserPerson.PersonId));
                                }
                                if (user.pmid == (tenant == null ? 0 : tenant.PMID) || user.pmid == (Owner == null ? 0 : Owner.PMID))
                                {
                                    IsManager = false;
                                    response = SignInProcess(encrypt, user, entities, IsManager, vwUserPerson);
                                }
                                else
                                {
                                    response.Status = false;
                                    response.Data = clsConstants.EmailAndCompanyNotMatched;
                                }
                            }
                            //}
                            //else
                            //{
                            //    response.Status = false;
                            //    response.Data = clsConstants.wrongUserNameAndPassword;
                            //}
                        }
                        else
                        {
                            IsManager = true;
                            response = SignInProcess(encrypt, user, entities, IsManager, vwUserPerson);
                        }
                    }
                    else
                    {
                        response.Status = false;
                        response.Data = clsConstants.wrongUserNameAndPassword;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
                return response;
            }
            finally
            {
                vwUserPerson = null;
                Owner = null;
                tenant = null;
                ManagerMaster = null;
            }
        }

        private ResponseMdl SignInProcess(string encrypt, MdlUser user, VataraEntities entities, bool IsManager, vwUserPerson vwUserPerson)
        {
            ResponseMdl response = new ResponseMdl();
            int managerID = 0;
            if (vwUserPerson.IsVerified == true)
            {
                if (String.Compare(vwUserPerson.Password, encrypt) == 0)
                {
                    FormsAuthentication.SetAuthCookie(user.userName, true);
                    if (IsManager)
                    {
                        #region
                        var person = entities.People.Where(e => e.Email.ToLower() == user.userName.ToLower()).ToList();
                        if (person != null)
                        {
                            if ((int)clsCommon.PersonType.PropertyManager != person[0].PersonTypeId)
                            {
                                response.Status = false;
                                response.Data = clsConstants.UnknownUser;
                                response.Message = clsConstants.UnknownUser;
                                return response;
                            }
                            else
                            {
                                user.personId = person[0].Id;
                                user.personTypeId = person[0].PersonTypeId;
                                user.personType = new clsCommon().ReturnPersonType(person[0].PersonTypeId);
                                user.Name = person[0].FirstName + " " + person[0].MiddleName + " " + person[0].LastName;
                                user.PhoneNo = person[0].PhoneNo;
                                int addId = Convert.ToInt32(person[0].AddressId);
                                var address = entities.vwAddresses.FirstOrDefault(e => e.AdrsID == addId);
                                if (address != null)
                                    user.Address = address.FullAdrs;

                                user.role = "pm";
                                user.access = new clsCommon().ReturnManagerRole(entities.PersonManagers.FirstOrDefault(m => m.PersonID == user.personId).Role);

                                person.ForEach(x =>
                                {
                                    user.lstPerson.Add(new MdlPersonRegistraion().GetPersonModel(x));
                                });
                            }
                        }
                        else
                        {
                            response.Status = false;
                            response.Data = clsConstants.wrongUserNameAndPassword;
                            response.Message = clsConstants.wrongUserNameAndPassword;
                            return response;
                        }
                        #endregion
                    }
                    else
                    {
                        var person = entities.getPersonByEmail(user.userName).ToList();
                        user.personId = person[0].Id;
                        user.personTypeId = person[0].PersonTypeId;
                        user.personType = new clsCommon().ReturnPersonType(person[0].PersonTypeId);
                        user.Name = person[0].FirstName + " " + person[0].MiddleName + " " + person[0].LastName;
                        user.PhoneNo = person[0].PhoneNo;
                        int addId = Convert.ToInt32(person[0].AddressId);
                        var address = entities.vwAddresses.FirstOrDefault(e => e.AdrsID == addId);
                        if (address != null)
                            user.Address = address.FullAdrs;
                        user.poid = person[0].POID;
                        user.personManageriD = (int)person[0].MANGERID;
                        person.ForEach(x =>
                        {
                            user.lstPerson.Add(new MdlPersonRegistraion().GetPersonModelUsingSP(x, user.poid));
                        });
                    }
                    if (user.pmid == 0)
                    {
                        managerID = Convert.ToInt32(entities.PersonManagers.FirstOrDefault(pm => pm.PersonID == vwUserPerson.PersonId).PMID);
                    }
                    else
                    {
                        managerID = user.pmid;
                    }
                    if (managerID > 0)
                    {
                        var managerDetail = entities.vwManagerMasters.FirstOrDefault(e => e.PMID == managerID);
                        user.ManagerDetail.pFirstName = managerDetail.CompanyName;
                        user.ManagerDetail.pLastName = managerDetail.CompanyName;
                        user.ManagerDetail.pId = managerDetail.PMID;
                        user.ManagerDetail.pFullName = managerDetail.CompanyName;
                        user.ManagerDetail.pPhoneNo = managerDetail.Phone;
                        user.ManagerDetail.pEmail = managerDetail.Email;
                        user.ManagerDetail.pAddress = managerDetail.FullAdrs;
                        //user.ManagerDetail.pImage = managerDetail.Logo;
                        user.ManagerDetail.pTypeId = (int)clsCommon.PersonType.PropertyManager;
                    }
                    user.mdlSubscription = new MdlSubscriptionExipryDate().GetMdl_SubscriptionExipryDate(entities.getSubscriptionExipryDate(managerID).FirstOrDefault());
                    //if (user.mdlSubscription.remainingDaysToExpire < 0)
                    //    user.isSubscriptionExpired = true;
                    user.isSubscriptionExpired = CheckSubscrtptionExist(managerID);
                    response.Data = user;
                    response.Status = true;
                }
                else
                {
                    response.Status = false;
                    response.Data = clsConstants.wrongUserNameAndPassword;
                    response.Message = clsConstants.wrongUserNameAndPassword;
                }
            }
            else
            {
                response.Status = false;
                response.Data = clsConstants.verifyEmail;
                response.Message = clsConstants.verifyEmail;
            }
            return response;
        }

        #region Original SignIn method

        //public ResponseMdl SignIn(MdlUser user)
        //{
        //    ResponseMdl response = new ResponseMdl();
        //    string encrypt = string.Empty;
        //    try
        //    {
        //        using (VataraEntities entities = new VataraEntities())
        //        {
        //            encrypt = new Encryption().Encrypt(user.password);
        //            // string Encrypt = new Encryption().Decrypt(user.password);
        //            var userData = entities.Users.FirstOrDefault(e => e.Email.ToLower() == user.userName.ToLower() && e.Password == encrypt
        //                           && e.IsDeleted == false);
        //            int managerID = 0;
        //            if (userData != null)
        //            {
        //                if (userData.IsVerified == true)
        //                {
        //                    if (String.Compare(userData.Password, encrypt) == 0)
        //                    {
        //                        FormsAuthentication.SetAuthCookie(user.userName, true);
        //                        var person = entities.People.Where(e => e.Email.ToLower() == user.userName.ToLower()).ToList();
        //                        user.personId = person[0].Id;
        //                        user.personType = new clsCommon().ReturnPersonType(person[0].PersonTypeId);
        //                        user.Name = person[0].FirstName + " " + person[0].MiddleName + " " + person[0].LastName;
        //                        user.PhoneNo = person[0].PhoneNo;
        //                        int addId = Convert.ToInt32(person[0].AddressId);
        //                        var address = entities.vwAddresses.FirstOrDefault(e => e.AdrsID == addId);
        //                        user.Address = address.FullAdrs;
        //                        //user.personId = 0;
        //                        //user.personType = "";
        //                        if (user.personType.ToLower() != clsConstants.propertyManager_Short)
        //                        {
        //                            vwPropertyTenantOwner vwPropertyTenantOwners = entities.vwPropertyTenantOwners.FirstOrDefault(e => e.PersonId == user.personId);
        //                            managerID = vwPropertyTenantOwners.ManagerId;
        //                        }
        //                        else
        //                        {
        //                            managerID = user.personId;
        //                        }
        //                        if (managerID > 0)
        //                        {
        //                            var managerDetail = entities.vwPersons.FirstOrDefault(e => e.pId == managerID);
        //                            user.ManagerDetail.pFirstName = managerDetail.pFirstName;
        //                            user.ManagerDetail.pLastName = managerDetail.pLastName;
        //                            user.ManagerDetail.pId = managerDetail.pId;
        //                            user.ManagerDetail.pFullName = managerDetail.pFullName;
        //                            user.ManagerDetail.pPhoneNo = managerDetail.pPhoneNo;
        //                            user.ManagerDetail.pEmail = managerDetail.pEmail;
        //                            user.ManagerDetail.pAddress = managerDetail.pAddress + " " + managerDetail.pStateName + " " + managerDetail.pCountyName + " " + managerDetail.pCityName + " (" + managerDetail.pMailBoxNo + ")";
        //                        }
        //                        person.ForEach(x =>
        //                        {
        //                            user.lstPerson.Add(new MdlPersonRegistraion().GetPersonModel(x));
        //                        });

        //                        response.Data = user;
        //                        response.Status = true;
        //                    }
        //                    else
        //                    {
        //                        response.Status = false;
        //                        response.Data = clsConstants.wrongUserNameAndPassword;
        //                    }
        //                }
        //                else
        //                {
        //                    response.Status = false;
        //                    response.Data = clsConstants.verifyEmail;
        //                }
        //            }
        //            else
        //            {
        //                response.Status = false;
        //                response.Data = clsConstants.wrongUserNameAndPassword;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Status = false;
        //        response.Message = clsConstants.somethingwrong;
        //    }
        //    return response;
        //}

        #endregion


        [HttpPost]
        public ResponseMdl SignOut(MdlUser user)
        {
            ResponseMdl response = new ResponseMdl();
            try
            {
                FormsAuthentication.SignOut(); //2
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        [AllowAnonymous]
        [HttpPost]
        public ResponseMdl PersonRegistraion([FromBody] MdlPersonRegistraion mdlPersonReg)
        {
            ResponseMdl response = new ResponseMdl();
            Person entityPerson = new Person();
            Address entityAddress = null;
            Address companyAddress = null;
            ManagerMaster ManagerMaster = null;
            User entityUser = new User();
            PersonManager PersonManager = new PersonManager();
            MdlPersonRegistraion objMdlPersonRegistraion = new MdlPersonRegistraion();
            MdlManagerMaster objMdlManagerMaster = new MdlManagerMaster();
            Status activeStatus = null;
            Email objEmail = new Email();
            ZohoProfile zohoProfile = null;
            clszohoHelper clshelper = new clszohoHelper();
            string fromEmail = string.Empty;
            string toEmail = string.Empty;
            string subject = string.Empty;
            string body = string.Empty;
            bool isExist = false;
            string stringforCompanyUrl = string.Empty;
            int status = 1;
            #region
            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        if (mdlPersonReg.id == 0)
                        {
                            activeStatus = ve.Status.FirstOrDefault(s => s.ModuleName.ToUpper() == clsConstants.Personmodule.ToUpper() && s.Name.ToUpper() == clsConstants.Active.ToUpper());
                            if (activeStatus != null)
                            {
                                status = activeStatus.Id;
                            }
                            isExist = ve.People.Any(e => e.Email.ToLower() == mdlPersonReg.email.ToLower());
                            if (isExist)
                            {
                                response.Status = false;
                                response.Message = clsConstants.EmailAlreadyExists;
                                return response;
                            }
                            if (mdlPersonReg.personType == clsConstants.propertyManager_Short)
                            {
                                mdlPersonReg.personType = clsConstants.propertyManager;
                            }
                            mdlPersonReg.personTypeId = ve.PersonTypes.FirstOrDefault(e => e.Name.ToLower() == mdlPersonReg.personType).Id;

                            #region Method for saving data in ManagerMaster Table

                            /// Save manager company address

                            if (mdlPersonReg.haveRegisteredCompany == false)
                            {
                                stringforCompanyUrl = mdlPersonReg.firstName + mdlPersonReg.lastName;
                                companyAddress = objMdlPersonRegistraion.GetAddressModel(mdlPersonReg);
                            }
                            else
                            {
                                stringforCompanyUrl = mdlPersonReg.company_Name;
                                companyAddress = objMdlPersonRegistraion.GetCompanyAddressModel(mdlPersonReg);
                            }
                            companyAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.companyAddress.ToLower()).Id;
                            companyAddress.IsDeleted = false;
                            companyAddress.CreationTime = DateTime.UtcNow;
                            ve.Addresses.Add(companyAddress);

                            /// Save person address
                            entityAddress = objMdlPersonRegistraion.GetAddressModel(mdlPersonReg);
                            entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.personAddress.ToLower()).Id;
                            entityAddress.IsDeleted = false;
                            entityAddress.CreationTime = DateTime.UtcNow;
                            ve.Addresses.Add(entityAddress);
                            ve.SaveChanges();
                            mdlPersonReg.company_addressId = companyAddress.Id;
                            mdlPersonReg.addressId = entityAddress.Id;
                            ManagerMaster = objMdlPersonRegistraion.Get_Entity_ManagerMaster(mdlPersonReg);
                            ManagerMaster.IsDeleted = false;
                            ManagerMaster.CreationTime = DateTime.UtcNow;
                            ManagerMaster.LastModificationTime = DateTime.UtcNow;
                            ManagerMaster.URL = objMdlManagerMaster.GetManagerCompanyURL(stringforCompanyUrl, ve);
                            ve.ManagerMasters.Add(ManagerMaster);
                            ve.SaveChanges();

                            #endregion                           

                            entityUser.Email = mdlPersonReg.email;
                            entityUser.Password = new Encryption().Encrypt(mdlPersonReg.password);
                            entityUser.PersonId = 0;
                            entityUser.PersonTypeId = 0;
                            entityUser.IsDeleted = false;
                            entityUser.CreationTime = DateTime.UtcNow;
                            // entityUser.CreatorUserId = entityPerson.Id;
                            entityUser.IsVerified = true;
                            entityUser.VerificationId = Guid.NewGuid().ToString();
                            ve.Users.Add(entityUser);
                            ve.SaveChanges();

                            entityPerson = objMdlPersonRegistraion.GetPersonModel(mdlPersonReg);
                            entityPerson.AddressId = entityAddress.Id;
                            entityPerson.UserId = entityUser.Id;
                            entityPerson.IsDeleted = false;
                            entityPerson.Status = status;
                            entityPerson.CreationTime = DateTime.UtcNow;
                            ve.People.Add(entityPerson);
                            ve.SaveChanges();

                            #region Method for saving data in personManager Table

                            PersonManager.PersonID = entityPerson.Id;
                            PersonManager.PMID = ManagerMaster.PMID;
                            PersonManager.IsActive = true;
                            PersonManager.Role = "PM";
                            PersonManager.CreationTime = DateTime.UtcNow;
                            ve.PersonManagers.Add(PersonManager);

                            #endregion

                            //********************************************

                            #endregion

                            fromEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                            toEmail = mdlPersonReg.email;
                            subject = clsConstants.EmailVarificationSubject;
                            body = objEmail.CreatemailBody(ConfigurationManager.AppSettings["EmailVerificationUrl"].ToString() + entityUser.VerificationId + "&email=" + toEmail + "&isverification=true");
                            objEmail.InsertEmailData(ve, fromEmail, toEmail, subject, body, false);

                            zohoProfile = clshelper.CreateCustomer(ve, mdlPersonReg, ManagerMaster.PMID);
                        }
                        else
                        {
                            entityPerson = ve.People.FirstOrDefault(e => e.Id == mdlPersonReg.id);
                            entityPerson = objMdlPersonRegistraion.GetPersonModel(entityPerson, mdlPersonReg);
                            entityPerson.LastModificationTime = DateTime.UtcNow;
                            entityPerson.LastModifierUserId = mdlPersonReg.id;
                            ve.People.Attach(entityPerson);
                            ve.Entry(entityPerson).State = EntityState.Modified;

                            entityAddress = ve.Addresses.FirstOrDefault(e => e.Id == mdlPersonReg.addressId);
                            entityAddress = objMdlPersonRegistraion.GetAddressModel(entityAddress, mdlPersonReg);
                            entityAddress.LastModificationTime = DateTime.UtcNow;
                            entityAddress.LastModifierUserId = mdlPersonReg.id;
                            ve.Addresses.Attach(entityAddress);
                            ve.Entry(entityAddress).State = EntityState.Modified;

                            entityUser = ve.Users.FirstOrDefault(e => e.Email.ToLower() == mdlPersonReg.email.ToLower());
                            // entityUser.Email = mdlPersonReg.email;
                            entityUser.LastModificationTime = DateTime.UtcNow;
                            entityUser.LastModifierUserId = mdlPersonReg.id;
                            ve.Users.Attach(entityUser);
                            ve.Entry(entityUser).State = EntityState.Modified;

                            if (mdlPersonReg.personTypeId == (int)clsCommon.PersonType.PropertyManager)
                            {
                                ManagerMaster = ve.ManagerMasters.FirstOrDefault(m => m.PMID == mdlPersonReg.pmid);
                                ManagerMaster.URL = mdlPersonReg.company_url;
                                ManagerMaster.LastModificationTime = DateTime.UtcNow;
                                ManagerMaster.LastModifierUserId = mdlPersonReg.id;
                                ve.ManagerMasters.Attach(ManagerMaster);
                                ve.Entry(ManagerMaster).State = EntityState.Modified;
                            }
                            // ManagerMaster =
                        }
                        ve.SaveChanges();
                        dbcontextTran.Commit();
                        response.Status = true;
                        if (mdlPersonReg.personType == clsConstants.propertyManager_Short || mdlPersonReg.personType == clsConstants.propertyManager)
                        {
                            response.Message = ManagerMaster.PMID.ToString();
                        }
                        response.Data = zohoProfile;
                        return response;
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {
                        entityPerson = null;
                        entityAddress = null;
                        entityUser = null;
                        companyAddress = null;
                        ManagerMaster = null;
                        PersonManager = null;
                        fromEmail = string.Empty;
                        toEmail = string.Empty;
                        subject = string.Empty;
                        body = string.Empty;
                        objEmail = null;
                        objMdlPersonRegistraion = null;
                        objMdlManagerMaster = null;
                        activeStatus = null;
                        zohoProfile = null;
                    }
                }
            }

        }


        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl companyEmailExist(string email)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    isExist = entities.ManagerMasters.Any(e => e.DisplayEmail.ToLower() == email.ToLower() || e.Company_Email.ToLower() == email.ToLower());
                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }

            return response;
        }

        [HttpGet]
        public ResponseMdl GetRegistrationData(int personId)
        {
            ResponseMdl response = new ResponseMdl();
            Person entityPerson = new Person();
            Address entityAddress = new Address();
            string personType = string.Empty;
            MdlPersonRegistraion mdlPersonReg = new MdlPersonRegistraion();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entityPerson = entities.People.FirstOrDefault(e => e.Id == personId);
                    entityAddress = entities.Addresses.FirstOrDefault(e => e.Id == entityPerson.AddressId);
                    mdlPersonReg = new MdlPersonRegistraion().GetMdl_PersonRegistraion(entityPerson, entityAddress);
                    personType = entities.PersonTypes.FirstOrDefault(e => e.Id == entityPerson.PersonTypeId).Name;
                    if (personType.ToLower() == clsConstants.propertyManager)
                    {
                        mdlPersonReg.personType = clsConstants.propertyManager_Short;
                        mdlPersonReg.company_url = entities.getManagerDetail(personId, clsConstants.Person).FirstOrDefault().URL;
                    }
                    else
                    {
                        mdlPersonReg.personType = personType.ToLower();
                    }
                    response.Status = true;
                    response.Data = mdlPersonReg;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                entityPerson = null;
                entityAddress = null;
                mdlPersonReg = null;
            }
            return response;
        }

        [HttpPost]
        public ResponseMdl ChangePassword(MdlChangePassword changePassword)
        {
            ResponseMdl response = new ResponseMdl();
            string encrypt = string.Empty;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    encrypt = new Encryption().Encrypt(changePassword.oldPassword);
                    // string Encrypt = new Encryption().Decrypt(user.password);
                    var userData = entities.Users.FirstOrDefault(e => e.Email.ToLower() == changePassword.email.ToLower() && e.Password == encrypt
                                   && e.IsDeleted == false);
                    if (userData != null)
                    {
                        encrypt = new Encryption().Encrypt(changePassword.newPassword);
                        userData.Password = encrypt;
                        entities.Users.Attach(userData);
                        entities.Entry(userData).State = EntityState.Modified;
                        entities.SaveChanges();
                        response.Status = true;
                    }
                    else
                    {
                        response.Data = "wrong Old passowrd";
                        response.Status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        [HttpPost]
        public ResponseMdl ChangeEmail(MdlChangeEmail mdlChangeEmail)
        {
            ResponseMdl response = new ResponseMdl();
            string encrypt = string.Empty;
            OwnerMaster entityOwnerMaster = null;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    using (var dbcontextTran = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            if (mdlChangeEmail.personTypeId == (int)clsCommon.PersonType.Owner)
                            {
                                entityOwnerMaster = entities.OwnerMasters.FirstOrDefault(e => e.Email.ToLower() == mdlChangeEmail.oldemail.ToLower() && e.IsDeleted == false);
                            }
                            if (mdlChangeEmail.personTypeId != (int)clsCommon.PersonType.Owner || (entityOwnerMaster != null && entityOwnerMaster.OwnerType == "individual"))
                            {
                                var userData = entities.Users.FirstOrDefault(e => e.Email.ToLower() == mdlChangeEmail.oldemail.ToLower() && e.IsDeleted == false);
                                if (userData != null)
                                {
                                    userData.Email = mdlChangeEmail.newemail;
                                    userData.LastModificationTime = DateTime.UtcNow;
                                    entities.Users.Attach(userData);
                                    entities.Entry(userData).State = EntityState.Modified;
                                    entities.SaveChanges();
                                }
                                var Person = entities.People.FirstOrDefault(e => e.Email.ToLower() == mdlChangeEmail.oldemail.ToLower() && e.IsDeleted == false);
                                if (Person != null)
                                {
                                    Person.Email = mdlChangeEmail.newemail;
                                    Person.LastModificationTime = DateTime.UtcNow;
                                    entities.People.Attach(Person);
                                    entities.Entry(Person).State = EntityState.Modified;
                                    entities.SaveChanges();
                                }
                                var MangerMaster = entities.ManagerMasters.FirstOrDefault(e => e.DisplayEmail.ToLower() == mdlChangeEmail.oldemail.ToLower() && e.IsDeleted == false);
                                if (MangerMaster != null)
                                {
                                    MangerMaster.DisplayEmail = mdlChangeEmail.newemail;
                                    MangerMaster.LastModificationTime = DateTime.UtcNow;
                                    entities.ManagerMasters.Attach(MangerMaster);
                                    entities.Entry(MangerMaster).State = EntityState.Modified;
                                    entities.SaveChanges();
                                }

                                var PropayProfile = entities.PorpayProfiles.FirstOrDefault(e => e.Email.ToLower() == mdlChangeEmail.oldemail.ToLower());
                                if (PropayProfile != null)
                                {
                                    PropayProfile.Email = mdlChangeEmail.newemail;
                                    entities.PorpayProfiles.Attach(PropayProfile);
                                    entities.Entry(PropayProfile).State = EntityState.Modified;
                                    entities.SaveChanges();
                                }

                                entityOwnerMaster = entities.OwnerMasters.FirstOrDefault(e => e.Email.ToLower() == mdlChangeEmail.oldemail.ToLower() && e.IsDeleted == false);
                                if (entityOwnerMaster != null)
                                {
                                    entityOwnerMaster.Email = mdlChangeEmail.newemail;
                                    entityOwnerMaster.LastModificationTime = DateTime.UtcNow;
                                    entities.OwnerMasters.Attach(entityOwnerMaster);
                                    entities.Entry(entityOwnerMaster).State = EntityState.Modified;
                                    entities.SaveChanges();
                                }
                            }
                            else
                            {
                                entityOwnerMaster.Email = mdlChangeEmail.newemail;
                                entityOwnerMaster.LastModificationTime = DateTime.UtcNow;
                                entities.OwnerMasters.Attach(entityOwnerMaster);
                                entities.Entry(entityOwnerMaster).State = EntityState.Modified;
                                entities.SaveChanges();
                            }
                            response.Status = true;
                            dbcontextTran.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbcontextTran.Rollback();
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl CheckEmailExist(string email)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    isExist = entities.Users.Any(e => e.Email.ToLower() == email.ToLower());
                    if (!isExist)
                    {
                        isExist = entities.ManagerMasters.Any(e => e.Company_Email.ToLower() == email.ToLower());
                    }
                    if (!isExist)
                    {
                        isExist = entities.OwnerMasters.Any(e => e.Email.ToLower() == email.ToLower());
                    }
                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }

            return response;
        }

        private List<MdlEmailData> GetEmailToSend()
        {
            List<MdlEmailData> lstMdlEmailData = new List<MdlEmailData>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.EmailDatas.Where(e => (e.IsSent == false || e.ReSend == true)).ToList().ForEach(u =>
                    {
                        lstMdlEmailData.Add(new MdlEmailData().Get_MdlEmailData(u));
                    });
                }
            }
            catch (Exception ex)
            {
            }
            return lstMdlEmailData;
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl EmailExist(string email, int id, string personType)
        {
            ResponseMdl response = new ResponseMdl();
            bool isExist = false;
            //int personTypeId = 0;
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    isExist = entities.People.Any(e => e.Email.ToLower() == email.ToLower() && e.IsDeleted==false);
                    response.Status = true;
                    response.Data = isExist;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl VerifyEmail(string email, string Guid, string comProfile)
        {
            ResponseMdl response = new ResponseMdl();
            User entityUser = new User();
            EmailData EmailData = new EmailData();
            vwCompanyWithClient CompanyWithClient = new vwCompanyWithClient();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    using (var dbcontextTran = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(comProfile))
                            {
                                CompanyWithClient = entities.vwCompanyWithClients.FirstOrDefault(e => e.CompanyProfileName == comProfile && e.ClientEmailId == email);
                                if (CompanyWithClient != null)
                                {
                                    response = VerifyAndUpdateTable(entityUser, email, Guid, EmailData, entities, response);
                                }
                                else
                                {
                                    response.Status = false;
                                    response.Message = clsConstants.EmailAndCompanyNotMatched;
                                    dbcontextTran.Rollback();
                                }
                            }
                            else
                            {
                                response = VerifyAndUpdateTable(entityUser, email, Guid, EmailData, entities, response);
                            }

                            dbcontextTran.Commit();
                        }
                        catch (Exception ex)
                        {
                            response.Status = false;
                            response.Message = clsConstants.somethingwrong;
                            dbcontextTran.Rollback();
                        }
                        finally
                        {
                            entityUser = null;
                            EmailData = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            return response;
        }

        private ResponseMdl VerifyAndUpdateTable(User entityUser, string email, string Guid, EmailData EmailData, VataraEntities entities, ResponseMdl response)
        {
            entityUser = entities.Users.FirstOrDefault(e => e.Email == email && e.VerificationId == Guid);
            if (entityUser != null)
            {
                entityUser.IsVerified = true;
                entities.Users.Attach(entityUser);
                entities.Entry(entityUser).State = EntityState.Modified;

                EmailData = entities.EmailDatas.FirstOrDefault(e => e.To == email);
                EmailData.IsSent = true;
                EmailData.ReSend = false;
                entities.EmailDatas.Attach(EmailData);
                entities.Entry(EmailData).State = EntityState.Modified;

                entities.SaveChanges();
                response.Status = true;
                response.Data = "";
            }
            else
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }

            return response;
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl ResendEmail(string email)
        {
            ResponseMdl response = new ResponseMdl();
            User entityUser = new User();
            EmailData EmailData = new EmailData();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    using (var dbcontextTran = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            //entityUser = entities.Users.FirstOrDefault(e => e.Email == email && e.VerificationId.ToString() == Guid);
                            //if (entityUser != null)
                            //{
                            //    entityUser.IsVerified = true;
                            //    entities.Users.Attach(entityUser);
                            //    entities.Entry(entityUser).State = EntityState.Modified;

                            //    EmailData = entities.EmailDatas.FirstOrDefault(e => e.To == email);
                            //    EmailData.IsSent = true;
                            //    EmailData.ReSend = false;
                            //    entities.EmailDatas.Attach(EmailData);
                            //    entities.Entry(EmailData).State = EntityState.Modified;

                            //    dbcontextTran.Commit();
                            //    response.Status = true;
                            //    response.Data = "";
                            //}
                            //else
                            //{
                            //    response.Status = false;
                            //    response.Data = "Somthing went wrong";
                            //    dbcontextTran.Rollback();
                            //}
                        }
                        catch (Exception ex)
                        {
                            //response.Data = isExist;
                            dbcontextTran.Rollback();
                        }
                        finally
                        {

                            entityUser = null;
                            EmailData = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }

            return response;
        }

        [HttpPost]
        public ResponseMdl SaveAdmin(MdlPersonRegistraion mdlPersonReg)
        {
            ResponseMdl response = new ResponseMdl();
            Person entityPerson = new Person();
            Address entityAddress = null;
            Address companyAddress = null;
            ManagerMaster ManagerMaster = null;
            User entityUser = new User();
            vwPerson vwPerson = null;
            List<MdlEmailData> lstMdlEmailData = null;
            vwManagerMaster vwManagerMaster = null;
            PersonManager PersonManager = new PersonManager();

            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        if (mdlPersonReg.id == 0)
                        {
                            vwPerson = new vwPerson();
                            if (mdlPersonReg.personType == clsConstants.propertyManager_Short)
                            {
                                mdlPersonReg.personType = clsConstants.propertyManager;
                            }
                            mdlPersonReg.personTypeId = ve.PersonTypes.FirstOrDefault(e => e.Name.ToLower() == mdlPersonReg.personType).Id;
                            vwPerson = ve.vwPersons.FirstOrDefault(e => e.pEmail.ToLower() == mdlPersonReg.email.ToLower());
                            if (vwPerson == null)
                            {
                                #region Method for saving data in ManagerMaster Table

                                companyAddress = new MdlPersonRegistraion().GetAddressModel(mdlPersonReg);
                                companyAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.companyAddress.ToLower()).Id;
                                companyAddress.IsDeleted = false;
                                companyAddress.CreationTime = DateTime.UtcNow;
                                ve.Addresses.Add(companyAddress);
                                ve.SaveChanges();

                                ManagerMaster = new MdlPersonRegistraion().Get_Entity_ManagerMaster(mdlPersonReg);
                                // Commented By asr 2018-10-03   
                                // ManagerMaster.AddressId = companyAddress.Id;
                                //END Commented By asr 2018-10-03   
                                //ManagerMaster.IsActive = true;
                                ManagerMaster.IsDeleted = false;
                                ManagerMaster.CreationTime = DateTime.UtcNow;
                                ManagerMaster.LastModificationTime = DateTime.UtcNow;

                                // Commented By asr 2018-10-03   
                                // ManagerMaster.URL = ManagerMaster.CompanyName.Replace(" ", "").ToLower().Substring(0, 8);
                                //END Commented By asr 2018-10-03   
                                vwManagerMaster = ve.vwManagerMasters.FirstOrDefault(e => e.URL.Trim().ToLower() == ManagerMaster.URL.Trim().ToLower());
                                if (vwManagerMaster != null)
                                {
                                    // Url already exists 
                                    // make unique URL  (ManagerMaster.URL)
                                }
                                ve.ManagerMasters.Add(ManagerMaster);
                                ve.SaveChanges();

                                #endregion

                                entityAddress = new MdlPersonRegistraion().GetAddressModel(mdlPersonReg);
                                entityAddress.AddressTypeId = ve.AddressTypes.FirstOrDefault(e => e.Name.ToLower() == clsConstants.personAddress.ToLower()).Id;
                                entityAddress.IsDeleted = false;
                                entityAddress.CreationTime = DateTime.UtcNow;
                                ve.Addresses.Add(entityAddress);
                                ve.SaveChanges();

                                entityUser.Email = mdlPersonReg.email;
                                entityUser.Password = new Encryption().Encrypt(mdlPersonReg.password);
                                entityUser.PersonId = 0;
                                entityUser.PersonTypeId = 0;
                                entityUser.IsDeleted = false;
                                entityUser.CreationTime = DateTime.UtcNow;
                                // entityUser.CreatorUserId = entityPerson.Id;
                                entityUser.IsVerified = false;
                                entityUser.VerificationId = Guid.NewGuid().ToString();
                                ve.Users.Add(entityUser);
                                ve.SaveChanges();

                                entityPerson = new MdlPersonRegistraion().GetPersonModel(mdlPersonReg);
                                entityPerson.AddressId = entityAddress.Id;
                                entityPerson.UserId = entityUser.Id;
                                entityPerson.IsDeleted = false;
                                entityPerson.CreationTime = DateTime.UtcNow;
                                ve.People.Add(entityPerson);
                                ve.SaveChanges();


                                #region Method for saving data in personManager Table

                                PersonManager.PersonID = entityPerson.Id;
                                PersonManager.PMID = ManagerMaster.PMID;
                                PersonManager.IsActive = true;
                                PersonManager.Role = null;
                                ve.PersonManagers.Add(PersonManager);

                                #endregion

                                //********************************************
                                lstMdlEmailData = GetEmailToSend();

                                foreach (var item in lstMdlEmailData)
                                {
                                    if (item.purpose.ToLower() == "new registration verification")
                                    {
                                        entityUser = null;
                                        entityUser = ve.Users.FirstOrDefault(e => e.Email == item.to);
                                        if (entityUser.IsVerified == false)
                                        {
                                            new Email().SendMail(item.from, "FromPassword", item.to, item.subject, "http://52.15.240.188/VataraUI/#/login?id=" + entityUser.VerificationId + "&email=" + item.to);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = "Email already exists!";
                                return response;
                            }
                        }
                        else
                        {
                            entityPerson = ve.People.FirstOrDefault(e => e.Id == mdlPersonReg.id);
                            entityPerson = new MdlPersonRegistraion().GetPersonModel(entityPerson, mdlPersonReg);
                            entityPerson.LastModificationTime = DateTime.UtcNow;
                            entityPerson.LastModifierUserId = mdlPersonReg.id;
                            ve.People.Attach(entityPerson);
                            ve.Entry(entityPerson).State = EntityState.Modified;

                            entityAddress = ve.Addresses.FirstOrDefault(e => e.Id == mdlPersonReg.addressId);

                            entityAddress = new MdlPersonRegistraion().GetAddressModel(entityAddress, mdlPersonReg);
                            entityAddress.LastModificationTime = DateTime.UtcNow;
                            entityAddress.LastModifierUserId = mdlPersonReg.id;
                            ve.Addresses.Attach(entityAddress);
                            ve.Entry(entityAddress).State = EntityState.Modified;

                            entityUser = ve.Users.FirstOrDefault(e => e.Email.ToLower() == mdlPersonReg.email.ToLower());
                            // entityUser.Email = mdlPersonReg.email;
                            entityUser.LastModificationTime = DateTime.UtcNow;
                            entityUser.LastModifierUserId = mdlPersonReg.id;
                            ve.Users.Attach(entityUser);
                            ve.Entry(entityUser).State = EntityState.Modified;
                        }
                        ve.SaveChanges();
                        dbcontextTran.Commit();
                        response.Status = true;
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {
                        vwPerson = null;
                        entityPerson = null;
                        entityAddress = null;
                        entityUser = null;
                        lstMdlEmailData = null;
                        vwManagerMaster = null;
                        companyAddress = null;
                        ManagerMaster = null;
                        PersonManager = null;
                    }
                }
            }
            return response;
        }

        [AllowAnonymous]
        [HttpPost]
        public ResponseMdl ForgotPasswordReset(forgotPasswordReset mdlforgotPasswordReset)
        {
            ResponseMdl response = new ResponseMdl();
            User entityUser = new User();
            string fromEmail = string.Empty;
            string toEmail = string.Empty;
            string subject = string.Empty;
            string body = string.Empty;
            Email objEmail = new Email();
            string password = string.Empty;

            using (VataraEntities ve = new VataraEntities())
            {
                using (var dbcontextTran = ve.Database.BeginTransaction())
                {
                    try
                    {
                        if (mdlforgotPasswordReset.userEmail != null)
                        {
                            entityUser = ve.Users.FirstOrDefault(u => u.Email == mdlforgotPasswordReset.userEmail);
                            if (entityUser != null)
                            {
                                password = new clsCommon().ReturnRandomAlphaNumericString(8);
                                entityUser.Password = new Encryption().Encrypt(password);
                                ve.Users.Attach(entityUser);
                                ve.Entry(entityUser).State = EntityState.Modified;
                                ve.SaveChanges();

                                fromEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                                toEmail = mdlforgotPasswordReset.userEmail;
                                subject = clsConstants.PasswordForVataraLogin;
                                body = objEmail.CreatEmailBodyForNewPassword(password, mdlforgotPasswordReset.userEmail);
                                objEmail.InsertEmailData(ve, fromEmail, toEmail, subject, body, false);
                            }

                            dbcontextTran.Commit();
                            response.Status = true;
                            return response;
                        }
                        else
                        {
                            response.Status = false;
                            response.Data = "Invalid Email";
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        dbcontextTran.Rollback();
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                    finally
                    {
                        entityUser = null;
                        fromEmail = string.Empty;
                        toEmail = string.Empty;
                        subject = string.Empty;
                        body = string.Empty;
                        objEmail = null;
                    }
                }
            }
        }

        public bool CheckSubscrtptionExist(int pmid)
        {
            string customerid = string.Empty;
            List<MdlGetSubscriptionDetail> lstSubscriptionDetail = new List<MdlGetSubscriptionDetail>();
            MdlGetSubscriptionDetail mdlGetSubscriptionDetail = new MdlGetSubscriptionDetail();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ve.sp_GetSubScrpitiondetail(pmid).ToList().ForEach(u =>
                    {
                        lstSubscriptionDetail.Add(mdlGetSubscriptionDetail.GetMdl_SubscriptionDetail(u));
                    });

                    customerid = ve.ZohoProfiles.Where(zp => zp.PMID == pmid).Select(zp => zp.CustomerId).SingleOrDefault();
                    if (!string.IsNullOrEmpty(customerid))
                    {
                        var Subscription = ve.ZohoSubScriptions.Where(C => C.CustomerId == customerid).FirstOrDefault();
                        if (Subscription == null)
                        {
                            return true;
                        }
                        else if (lstSubscriptionDetail.Count != 0)
                        {
                                return true;
                        }
                        else
                        {
                            var subcriptionlist= ve.ZohoSubScriptions.Where(C => C.CustomerId == customerid && C.IsDeleted==false).FirstOrDefault();
                            if (subcriptionlist == null)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {

            }
        }

        [HttpGet]
        public ResponseMdl GetZohoProfile(string Email)
        {
            ResponseMdl response = new ResponseMdl();
            MdlZohoProfile mdlZohoProfile = new MdlZohoProfile();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    ve.ZohoProfiles.Where(m => m.Email == Email).ToList().ForEach(u =>
                    {
                        mdlZohoProfile = mdlZohoProfile.getZohoProfile(u);
                    });
                    response.Data = mdlZohoProfile;
                }
                response.Status = true;
                response.Message = "Success";
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.ToString();
                return response;
            }
            finally
            {
                response = null;
                mdlZohoProfile = null;
            }
        }


        [AllowAnonymous]
        [HttpGet]
        public ResponseMdl GetUserInfo(int PMID,int PersonId,int PersonTypeId)
        {
            ResponseMdl response = new ResponseMdl();
            string encrypt = string.Empty;
            vwUserPerson vwUserPerson = null;
            bool IsManager = false;
            vwCompanyPropertyOwner Owner = null;
            vwCompanyPropertyTenant tenant = null;
            vwManagerMaster ManagerMaster = null;
            MdlUser user = new MdlUser();
            try {
                using (VataraEntities entities = new VataraEntities())
                {
                    vwUserPerson = entities.vwUserPersons.FirstOrDefault(u => u.PersonId==PersonId && u.PersonTypeId == PersonTypeId);
                    encrypt = vwUserPerson.Password;
                    if (vwUserPerson != null)
                    {

                        user.userName = vwUserPerson.Email;   // if user types his email in any case and if that email and password is correct 
                        user.pmid = PMID;                                     //irrespective of case in email then replace the email with correct case email so that this not create any problem to chat.
                        if (PersonTypeId!=1)
                        {
                            user.comProfile = entities.ManagerMasters.Where(m => m.PMID == user.pmid).Select(m => m.URL).SingleOrDefault();
                            //user.comProfile = Regex.Replace(user.comProfile, ".vatara.com", "", RegexOptions.IgnoreCase); // user.comProfile.Replace(".vatara.com", "");
                            ManagerMaster = entities.vwManagerMasters.FirstOrDefault(e => e.URL.ToLower() == user.comProfile.ToLower());
                            if (ManagerMaster == null)
                            {
                                response.Status = false;
                                response.Data = clsConstants.WrongUrl;
                                response.Message = clsConstants.WrongUrl;
                                return response;
                            }
                            //if (vwUserPerson != null)
                            //{
                            user.pmid = ManagerMaster.PMID;
                            if (vwUserPerson.Type.ToLower() == clsConstants.propertyManager.ToLower())
                            {
                                var personManager = entities.PersonManagers.FirstOrDefault(p => p.PersonID == vwUserPerson.PersonId);
                                if (personManager != null)
                                {
                                    if (user.pmid == personManager.PMID)
                                    {
                                        IsManager = true;
                                        response = SignInProcess(encrypt, user, entities, IsManager, vwUserPerson);
                                    }
                                }
                                else
                                {
                                    response.Status = false;
                                    response.Data = clsConstants.EmailAndCompanyNotMatched;
                                }
                            }
                            else if (vwUserPerson.Type.ToLower() == clsConstants.Owner.ToLower() || vwUserPerson.Type.ToLower() == clsConstants.Tenant.ToLower())
                            {
                                if (vwUserPerson.Type.ToLower() == clsConstants.Owner.ToLower())
                                {
                                    Owner = entities.vwCompanyPropertyOwners.FirstOrDefault(o => o.OwnerId == vwUserPerson.PersonId);
                                }
                                else
                                {
                                    tenant = (entities.vwCompanyPropertyTenants.FirstOrDefault(o => o.TenantId == vwUserPerson.PersonId));
                                }
                                if (user.pmid == (tenant == null ? 0 : tenant.PMID) || user.pmid == (Owner == null ? 0 : Owner.PMID))
                                {
                                    IsManager = false;
                                    response = SignInProcess(encrypt, user, entities, IsManager, vwUserPerson);
                                }
                                else
                                {
                                    response.Status = false;
                                    response.Data = clsConstants.EmailAndCompanyNotMatched;
                                }
                            }
                            //}
                            //else
                            //{
                            //    response.Status = false;
                            //    response.Data = clsConstants.wrongUserNameAndPassword;
                            //}
                        }
                        else
                        {
                            IsManager = true;
                            response = SignInProcess(encrypt, user, entities, IsManager, vwUserPerson);
                        }
                    }
                    
                }
                
            }
            catch(Exception ex) {

            }
            finally {
            }
            return response;
        }
    }

    public class forgotPasswordReset
    {
        public string userEmail { get; set; }
    }

}

