﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

using System.Web.Http.Cors;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class RecurringBillController : ApiController
    {
        public ResponseMdl GetRecurringBill(int personTypeId,int personId,int propertyId)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlRecurringBill> lstRecurring = new List<MdlRecurringBill>();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    if (propertyId == 0)
                    {
                        ve.RecurringBillByPersonId(personTypeId, personId).ToList().ForEach(u =>
                       {
                           lstRecurring.Add(new MdlRecurringBill().GetMdl_RecurringBill(u));
                       });
                    }
                    else
                    {
                        ve.RecurringBillByPersonIdAndPropertyID(personTypeId, personId, propertyId).ToList().ForEach(u =>
                        {
                            lstRecurring.Add(new MdlRecurringBill().GetMdl_RecurringBillByProperty(u));
                        });
                    }
                    
                    response.Status = true;
                    response.Data = lstRecurring;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstRecurring = null;
            }
            return response;
        }

        [HttpPost]
        public ResponseMdl StopRecurringPayment([FromBody] int billId)
        {
            ResponseMdl response = new ResponseMdl();            
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    var bill = entities.Bills.FirstOrDefault(b => b.BillId == billId);
                    if (bill != null)
                    {
                        bill.IsRecurringPayment = false;
                        entities.Bills.Attach(bill);
                        entities.Entry(bill).State = EntityState.Modified;
                        entities.SaveChanges();
                        response.Status = true;

                    }
                    else
                    {
                        response.Status = true;
                        response.Data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            
            return response;
        }

    }
}
