﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{
    public class PersonTypeController : ApiController
    {
        public IEnumerable<PersonType> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.PersonTypes.ToList();
            }
        }

        public PersonType Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.PersonTypes.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
