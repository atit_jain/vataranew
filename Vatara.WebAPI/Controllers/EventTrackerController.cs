﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;


namespace Vatara.WebAPI.Controllers
{
    [Authorize]
    public class EventTrackerController : ApiController
    {
        [HttpGet]
        public ResponseMdl GetPropertyByPerson(int ownerPOID, int personId, int personType)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlPropertOwnersWithManager> lstMdlPropertOwnersWithManager = new List<MdlPropertOwnersWithManager>();
            MdlPropertOwnersWithManager mdlPropertOwnersWithManager = new MdlPropertOwnersWithManager();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    try
                    {
                        entities.getPropertiesByPerson(personId, personType, ownerPOID).ToList().ForEach(u =>
                        {
                            lstMdlPropertOwnersWithManager.Add(mdlPropertOwnersWithManager.GetMdl_PropertOwnersWithManager(u));
                        });
                        response.Status = true;
                        response.Data = lstMdlPropertOwnersWithManager;
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                lstMdlPropertOwnersWithManager = null;
                mdlPropertOwnersWithManager = null;
            }
        }

        [HttpPost]
        public ResponseMdl SaveNewEvent([FromBody] MdlEventTracker eventData)
        {
            ResponseMdl response = new ResponseMdl();
            EventTracker eventTracker = new EventTracker();
            MdlEventTracker mdlEventTracker = new MdlEventTracker();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    try
                    {
                        var EventTracker = ve.EventTrackers.Where(Event => Event.EventID == eventData.eventID).SingleOrDefault();
                        if (EventTracker != null)
                        {
                            EventTracker.Description = eventData.description;                          
                            EventTracker.EventDate = eventData.eventDate.Value.ToUniversalTime();
                            EventTracker.EventType = eventData.eventType;
                            EventTracker.LastModifiactionTime = DateTime.UtcNow;
                            EventTracker.LastModificationUserId = eventData.creatorUserId;
                            ve.SaveChanges();
                            response.Data = eventData;
                        }
                        else
                        {
                            eventTracker = mdlEventTracker.GetEntity_EventTracker(eventData);
                            eventTracker.CreationTime = DateTime.UtcNow;
                            eventTracker.EventDate = eventData.eventDate.Value.ToUniversalTime();
                            ve.EventTrackers.Add(eventTracker);
                            ve.SaveChanges();
                            eventData.eventID = eventTracker.EventID;
                            response.Data = eventData;
                        }
                        response.Status = true;
                        
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        response.Message = clsConstants.somethingwrongSaveEvent.ToString();
                        return response;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrongSaveEvent.ToString();
                return response;
            }
            finally
            {
                eventTracker = null;
                mdlEventTracker = null;
            }
        }

        [HttpGet]
        public ResponseMdl GetEventList(int Propertyid, bool ShowArchive)
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlEventTracker> lstmdleventtracker = new List<MdlEventTracker>();
            MdlEventTracker mdlEventTracker = new MdlEventTracker();
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    try
                    {
                        ve.Sp_GetEventList(Propertyid, ShowArchive).ToList().ForEach(u =>
                        {
                            lstmdleventtracker.Add(mdlEventTracker.GetMdl_EventTracker(u));
                        });
                        response.Status = true;
                        response.Data = lstmdleventtracker;
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        response.Message = clsConstants.somethingwrong.ToString();
                        return response;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong.ToString();
                return response;
            }
            finally
            {
                lstmdleventtracker = null;
                mdlEventTracker = null;
            }
        }

        [HttpPost]
        public ResponseMdl DeleteEvent([FromBody] MdlEventTracker eventData)
        {
            ResponseMdl response = new ResponseMdl();           
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    try
                    {
                        var EventTracker = ve.EventTrackers.Where(Event => Event.EventID == eventData.eventID && Event.EventType == eventData.eventType).FirstOrDefault();
                        ve.EventTrackers.Remove(EventTracker);
                        ve.SaveChanges();
                        response.Status = true;
                        response.Data = eventData;
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        response.Message = clsConstants.somethingwrongDeleteEvent.ToString();
                        return response;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrongDeleteEvent.ToString();
                return response;
            }            
        }

        [HttpPost]
        public ResponseMdl ArchiveEvent([FromBody] MdlEventTracker eventData)
        {
            ResponseMdl response = new ResponseMdl();          
            try
            {
                using (VataraEntities ve = new VataraEntities())
                {
                    try
                    {
                        var EventTracker = ve.EventTrackers.Where(Event => Event.EventID == eventData.eventID && Event.EventType == eventData.eventType).SingleOrDefault();
                        if (EventTracker != null)
                        {
                            EventTracker.IsDeleted = true;
                            EventTracker.DeletionTime = DateTime.Now.ToUniversalTime();
                            EventTracker.DeleterUserId = eventData.creatorUserId;
                            ve.SaveChanges();
                            response.Data = eventData;
                        }
                        response.Status = true;
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        response.Message = clsConstants.somethingwrongArchiveEvent.ToString();
                        return response;
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrongArchiveEvent.ToString();
                return response;
            }            
        }
    }
}