﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vatara.DataAccess;
using System.Data.Entity.Core.Objects;

namespace Vatara.WebAPI.Controllers
{
    public class PrintFileController : Controller
    {
        VataraEntities entities = new VataraEntities();
        protected string FileType = string.Empty;
        protected string ModuleId = string.Empty;
        // GET: PrintFile
        public ActionResult PrintLeaseDeatails()
        {
            
            return View();
        }

        /// <summary>
        /// Print file for lease Module 
        /// </summary>
        /// <param name="propertyId"> PropertyId</param>
        /// <returns> pdf file return </returns>
        public ActionResult PrintLeaseDeatailsPdf(int leaseId, int propertyId)
        {
            FileType = "Documents";
            ModuleId = "LEASE";
            PrintLease data = new PrintLease();
            LeaseController lease = new LeaseController();
            PropertyPhotoController PropertyPhotoController = new PropertyPhotoController();
            LeaseClauseController LeaseClauseController = new LeaseClauseController();
            TransactionController TransactionController = new TransactionController();
            AgreementTypeController AgreementTypeController = new AgreementTypeController();
            LeaseOptionController LeaseOptionController = new LeaseOptionController();
            LeasePaymentFrequencyController leasePaymentFrequencyController = new LeasePaymentFrequencyController();
            PropertiesController PropertiesController = new PropertiesController();
            LeaseDocumentController LeaseDocumentController = new LeaseDocumentController();
            data.ResponseMdl = GetOwnerIncomeExpenseByPropertyIdLeaseId(propertyId, leaseId);
            data.MdlLeaseData = lease.GetLeaseById(leaseId);
            data.lstAgreementType = AgreementTypeController.Get();
            data.LeaseClause = entities.LeaseClauses.Where(p => p.LeaseId == leaseId && p.IsDeleted == false).ToList(); 
            data.LeaseOption = entities.LeaseOptions.Where(p => p.LeaseId == leaseId && p.IsDeleted == false).ToList(); ;
            data.LeasePaymentFrequency = leasePaymentFrequencyController.Get();
            data.Photos = entities.Files.Where(f => f.LinkId == propertyId && f.FileType.ToUpper() == this.FileType.ToUpper() && f.ModuleId.ToUpper() == this.ModuleId.ToUpper() && f.IsDeleted == false).ToList();
            data.propertyInfo = entities.getPropertyByPropertyId(propertyId).FirstOrDefault();
            data.lstMdlOccupant = lease.GetOccupantByLeaseId(leaseId);
            data.documentArray = entities.Files.Where(f => f.LinkId == leaseId && f.FileType.ToUpper() == this.FileType.ToUpper() && f.ModuleId.ToUpper() == this.ModuleId.ToUpper() && f.IsDeleted == false).ToList();
            return new Rotativa.ViewAsPdf("PrintLeaseDeatails", data);
        }
        public OwnerIncomeExpense GetOwnerIncomeExpenseByPropertyIdLeaseId(int propertyId, int leaseId)
        {
            MdlOwnerIncomeExpenseByPropertyIdLeaseId mdlOwnerIncomeExpenseByPropertyIdLeaseId = new MdlOwnerIncomeExpenseByPropertyIdLeaseId();
            OwnerIncomeExpense OwnerIncomeExpense = new OwnerIncomeExpense();
            using (VataraEntities entities = new VataraEntities())
            {
               
                entities.getOwnerIncomeExpenseByPropertyIdLeaseId(propertyId, leaseId).ToList().ForEach(item => {
                    if (item.Type.ToLower() == "income")
                    {
                        OwnerIncomeExpense.totalIncomeYTD += (decimal)item.MTD;
                        OwnerIncomeExpense.totalIncomeYTD += (decimal)item.YTD;
                        OwnerIncomeExpense.lstMdlOwnerIncomeByPropertyIdLeaseId.Add(mdlOwnerIncomeExpenseByPropertyIdLeaseId.GetMdl_OwnerIncomeExpenseByPropertyIdLeaseId(item));
                    }
                    else
                    {
                        OwnerIncomeExpense.totalExpenseMTD += (decimal)item.MTD;
                        OwnerIncomeExpense.totalExpenseYTD += (decimal)item.YTD;
                        OwnerIncomeExpense.lstMdlOwnerExpenseByPropertyIdLeaseId.Add(mdlOwnerIncomeExpenseByPropertyIdLeaseId.GetMdl_OwnerIncomeExpenseByPropertyIdLeaseId(item));
                    }
                });
              
            }
            return OwnerIncomeExpense;
        }
    }
    public class PrintLease
    {
        public MdlLeaseData MdlLeaseData { get; set; }
        public OwnerIncomeExpense ResponseMdl { get; set; }
        public IEnumerable<LeasePaymentFrequency> LeasePaymentFrequency { get; set; }
        public IEnumerable<AgreementType> lstAgreementType { get; set; }
        public List<LeaseClaus> LeaseClause { get; set; }
        public List<LeaseOption> LeaseOption { get; set; }
        public List<MdlOccupant> lstMdlOccupant { get; set; }
        public getPropertyByPropertyId_Result propertyInfo { get; set; }
        public List<File> documentArray { get; set; }
        public List<File> Photos { get; set; }
        public MdlOwnerIncomeExpenseListByPropertyIdLeaseId mdlOwnerIncomeExpenseListByPropertyIdLeaseId { get; set; }

    }

    public class OwnerIncomeExpense
    {
        public decimal totalIncomeMTD { get; set; }
        public decimal totalIncomeYTD { get; set; }
        public decimal totalExpenseMTD { get; set; }
        public decimal totalExpenseYTD { get; set; }
        public OwnerIncomeExpense()
        {
            lstMdlOwnerIncomeByPropertyIdLeaseId = new List<MdlOwnerIncomeExpenseByPropertyIdLeaseId>();
            lstMdlOwnerExpenseByPropertyIdLeaseId = new List<MdlOwnerIncomeExpenseByPropertyIdLeaseId>();
        }
        public List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerIncomeByPropertyIdLeaseId { get; set; }
        public List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerExpenseByPropertyIdLeaseId { get; set; }
        //public List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerIncomeByPropertyIdLeaseId { get; set; }
        //public List<MdlOwnerIncomeExpenseByPropertyIdLeaseId> lstMdlOwnerExpenseByPropertyIdLeaseId { get; set; }
    }
}