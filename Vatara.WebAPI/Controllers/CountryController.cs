﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vatara.DataAccess;

namespace Vatara.WebAPI.Controllers
{

    public class CountryController : ApiController
    {
        [HttpGet]
        public IEnumerable<Country> Get()
        {
            using (VataraEntities entities = new VataraEntities())
            {                
                return entities.Countries.ToList();
            }
        }

        [HttpGet]
        public IEnumerable<Country> GetAllCountries()
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Countries.ToList();
            }
        }



        public ResponseMdl GetAllCountry()
        {
            ResponseMdl response = new ResponseMdl();
            List<MdlCountry> lstCountry = new List<MdlCountry>();
            try
            {
                using (VataraEntities entities = new VataraEntities())
                {
                    entities.Countries.ToList().ForEach(u =>
                    {
                        lstCountry.Add(new MdlCountry());
                    });
                    response.Data = lstCountry;
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = clsConstants.somethingwrong;
            }
            finally
            {
                lstCountry = null;
            }
            return response;
        }



        public Country Get(int id)
        {
            using (VataraEntities entities = new VataraEntities())
            {
                return entities.Countries.FirstOrDefault(p => p.Id == id);
            }
        }
    }
}
