﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Vatara.DataAccess;
using Vatara.WebAPI.Controllers;

namespace Vatara.WebAPI.Hubs
{
    public class ChatHubForMobile : Hub
    {

        clsChat db = new clsChat();
        ChatController objChat = new ChatController();
        // Context.QueryString["EmailID"]="";

        public void sendToIndividual(string User, string who, string message, string messageType, string fileExtension, string SenderName, string EmailID)
        {
            var ActiveUsersList = MdlActiveUsersList.getActiveUsersList();
            
            string name = EmailID;
            Message mdl = new Message();
            mdl.FromMsg = User;
            mdl.ToMsg = who;
            mdl.Message1 = message;
            mdl.FileExtension = fileExtension;
            mdl.IsFile = messageType == "file" ? true : false;
            mdl.IsDelivered = false;
            mdl.Id = db.saveChat(mdl);
            Clients.Group(who).addNewMessageToPage(User, name, message, mdl.IsFile, fileExtension, mdl.Id, ActiveUsersList, SenderName);
            Clients.Group(name).selfUpdate(User, name, message, mdl.IsFile, fileExtension, mdl.Id, SenderName);
        }

        public void messageDelivered(string who, long messageID, string EmailID)
        {

            if (who.ToUpper() == "CHECKNOTIFICATION")
            {
                Clients.All.CheckNotification();
            }
            else
            {
                db.messageDelivered(messageID);
               
                string name = EmailID;
                Clients.Group(who).messageDelivered(name, messageID);
            }
        }

        public void userTyping(string user, string selectedUser,string EmailID)
        {

            //string name = EmailID;

            Clients.Group(selectedUser).userTyping(user);
        }

        public void getActiveUserList(string EmailID)
        {
           
            string name = EmailID;
            var ActiveUsersList = MdlActiveUsersList.getActiveUsersList();
            Clients.Group(name).getActiveUserList(ActiveUsersList);
        }

        public override Task OnConnected()
        {

            var EmailID = Context.QueryString["EmailID"];
            string name = EmailID;
            var userIDChat = objChat.getUserID(name);
            var ActiveUsersList = MdlActiveUsersList.addActiveUser(EmailID, Context.ConnectionId, userIDChat);
            Groups.Add(Context.ConnectionId, name);
            Clients.All.updateUserStatus(name, true, userIDChat, ActiveUsersList);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var EmailID = Context.QueryString["EmailID"];
            if (EmailID != null)
            {
                var ActiveUsersList = MdlActiveUsersList.removeActiveUser(EmailID, Context.ConnectionId);
                string name = EmailID;
                Groups.Remove(Context.ConnectionId, name);
                Clients.All.updateUserStatus(name, false, objChat.getUserID(name), ActiveUsersList);
               
            }
            return base.OnDisconnected(stopCalled);
        }

        public void disconnectUser(string EmailID)
        {
            var ActiveUsersList = MdlActiveUsersList.removeActiveUser(EmailID, Context.ConnectionId);
            string name = EmailID;
            Groups.Remove(Context.ConnectionId, name);
            Clients.All.updateUserStatus(name, false, objChat.getUserID(name), ActiveUsersList);
        }

        public override Task OnReconnected()
        {
            
            var EmailID = Context.QueryString["EmailID"];
            if (EmailID != null)
            {
                string name = EmailID;
                var userID = objChat.getUserID(name);
                var ActiveUsersList = MdlActiveUsersList.addActiveUser(EmailID, Context.ConnectionId, userID);
                Groups.Add(Context.ConnectionId, name);
                Clients.All.updateUserStatus(name, true, userID, ActiveUsersList);
            }
            return base.OnReconnected();
        }

    }
}