﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Vatara.DataAccess;
using Vatara.WebAPI.Controllers;

namespace Vatara.WebAPI.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        clsChat db = new clsChat();
        ChatController objChat = new ChatController();
    
        public void sendToIndividual(string User ,string who, string message, string messageType, string fileExtension,string SenderName)
        {
            var ActiveUsersList = MdlActiveUsersList.getActiveUsersList();
            string name = Context.User.Identity.Name;
            Message mdl = new Message();
            mdl.FromMsg = User;
            mdl.ToMsg = who;
            mdl.Message1 = message;
            mdl.FileExtension = fileExtension;
            mdl.IsFile = messageType == "file" ? true : false;
            mdl.IsDelivered = false;
            mdl.Id = db.saveChat(mdl);
            Clients.Group(who).addNewMessageToPage(User,name, message, mdl.IsFile, fileExtension, mdl.Id, ActiveUsersList, SenderName);
            Clients.Group(name).selfUpdate(User,name, message, mdl.IsFile, fileExtension, mdl.Id, SenderName);
        }

        public void messageDelivered(string who, long messageID)
        {
            if (who.ToUpper() == "CHECKNOTIFICATION")
            {
                Clients.All.CheckNotification();
            }
            else
            {
                db.messageDelivered(messageID);
                string name = Context.User.Identity.Name;
                Clients.Group(who).messageDelivered(name, messageID);
            }
        }

        public void userTyping(string user, string selectedUser)
        {
            Clients.Group(selectedUser).userTyping(user);
        }

        public void getActiveUserList()
        {
            var ActiveUsersList = MdlActiveUsersList.getActiveUsersList();
            string name = Context.User.Identity.Name;
            Clients.Group(name).getActiveUserList(ActiveUsersList);
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            var userID =  objChat.getUserID(name);
            var ActiveUsersList = MdlActiveUsersList.addActiveUser(Context.User.Identity.Name, Context.ConnectionId, userID);            
            Groups.Add(Context.ConnectionId, name);
            Clients.All.updateUserStatus(name, true, userID, ActiveUsersList);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var ActiveUsersList = MdlActiveUsersList.removeActiveUser(Context.User.Identity.Name, Context.ConnectionId);
            string name = Context.User.Identity.Name;
            Groups.Remove(Context.ConnectionId, name);
            Clients.All.updateUserStatus(name, false, objChat.getUserID(name), ActiveUsersList);
            return base.OnDisconnected(stopCalled);
         }

        public void disconnectUser()
        {
            var ActiveUsersList = MdlActiveUsersList.removeActiveUser(Context.User.Identity.Name, Context.ConnectionId);
            string name = Context.User.Identity.Name;
            Groups.Remove(Context.ConnectionId, name);
            Clients.All.updateUserStatus(name, false, objChat.getUserID(name), ActiveUsersList);
        }

        public override Task OnReconnected()
        {
            string name = Context.User.Identity.Name;
            var userID = objChat.getUserID(name);
            var ActiveUsersList = MdlActiveUsersList.addActiveUser(Context.User.Identity.Name, Context.ConnectionId, userID);
            Groups.Add(Context.ConnectionId, name);
            Clients.All.updateUserStatus(name, true, userID, ActiveUsersList);
            return base.OnReconnected();
        }
        
    }
}