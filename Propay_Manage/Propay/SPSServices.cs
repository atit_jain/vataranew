﻿using Propay_Manage.SPSService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;



namespace Propay_Manage
{
    public class SPSServices
    {        
        private SPSService.SPSServiceClient client = new SPSService.SPSServiceClient();

        private SPSService.ID GetUserIdentity()
        {
            SPSService.ID identity = new SPSService.ID();
            //identity.AuthenticationToken = "0671454c-cba6-4cfa-90a9-ab4b3af15975";
            //identity.BillerAccountId = "1916271013067342";
            identity.AuthenticationToken = Configuration.AuthenticationToken;
            identity.BillerAccountId = Configuration.BillerAccountId;
            return identity;
        }

        private SPSService.UserRight[] GetRights()
        {
            SPSService.UserRight[] rights = new SPSService.UserRight[6];
            rights[0] = SPSService.UserRight.ProcessPayments;
            rights[1] = SPSService.UserRight.BatchProcess;
            rights[2] = SPSService.UserRight.EditPayers;
            rights[3] = SPSService.UserRight.RefundPayments;
            rights[4] = SPSService.UserRight.ViewPayers;
            rights[5] = SPSService.UserRight.ViewReports;
            return rights;
        }

        public string SignupMerchantXML(string destinationUrl, string requestXml)
        {
           
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }


        public string SignupMerchantXML_test(string destinationUrl, string requestXml)
        {
            var Cert = new X509Certificate2("C:\\Users\\Admin\\Downloads\\Kazoo.pfx", "test123");
            var certBytes = Cert.RawData;
            var certString = Convert.ToBase64String(certBytes);            
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);            
            request.Headers.Add("Authorization",certString);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }

        public SPSService.CreateUserResult CreateUser(SPSService.UserInfo info)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.CreateUser(this.GetUserIdentity(), info, this.GetRights());
        }

        public SPSService.CreateAccountInformationResult CreatePayerID(SPSService.PayerData info)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.CreatePayerWithData(this.GetUserIdentity(), info);
        }

        public SPSService.CreateMerchantProfileResult CreateMerchantProfile(SPSService.MerchantProfileData info)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.CreateMerchantProfile(this.GetUserIdentity(), info);
        }

        public SPSService.CreateHostedTransactionResult CreateHostedTransaction(SPSService.HostedTransactionDetail info)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.CreateHostedTransaction(this.GetUserIdentity(), info);
        }

        public SPSService.GetHostedResult GetHostedTransaction(string info)
        {                       
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.GetHostedResult(this.GetUserIdentity(), info);
        }

        public SPSService.TransactionResult ProcessTransaction(SPSService.Transaction transaction, Guid id, SPSService.PaymentInfoOverrides overri)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.ProcessPaymentMethodTransaction(this.GetUserIdentity(), transaction, id, overri);
        }

        public SPSService.PaymentMethodsResult ProcessTransactionMethod(string payerid, Guid id)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.GetPayerPaymentMethod(this.GetUserIdentity(), payerid, id);
        }

        public SPSService.TempTokenResult GetTempToken(SPSService.TempTokenRequest request)
        {
            request.Identification = this.GetUserIdentity();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.GetTempToken(request);
        }

        public SPSService.GetMerchantProfilesResult GetMerchantProfiles()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.GetMerchantProfiles(this.GetUserIdentity());
        }

        public System.Threading.Tasks.Task<Propay_Manage.SPSService.GetMerchantProfilesResult> GetMerchantProfilesAsync()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.GetMerchantProfilesAsync(this.GetUserIdentity());
        }

        public SPSService.GetPayersResult GetPayers(SPSService.PayerData info)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.GetPayers(this.GetUserIdentity(), info);
        }

        public SPSService.TransactionResult ProcessPaymentMethodTransaction(SPSService.Transaction transaction, Guid id, SPSService.PaymentInfoOverrides overri)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            return client.ProcessPaymentMethodTransaction(this.GetUserIdentity(), transaction, id, overri);
        }

    }
}
