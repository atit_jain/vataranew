﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Propay_Manage
{
    public static class Configuration
    {
        private static string _certStr = ConfigurationManager.AppSettings["certStr"].ToString();
        private static string _billerAccountId = ConfigurationManager.AppSettings["BillerAccountId"].ToString(); 
        private static string _termId = ConfigurationManager.AppSettings["termId"].ToString();
        private static string _authenticationToken = ConfigurationManager.AppSettings["AuthenticationToken"].ToString();
        private static string _propayRedirectUrl = ConfigurationManager.AppSettings["PropayRedirectUrl"].ToString();
        private static string _propayUrl = ConfigurationManager.AppSettings["PropayUrl"].ToString();
        private static string _propayCSS = ConfigurationManager.AppSettings["PropayCSS"].ToString();

        private static string _senderEmail = ConfigurationManager.AppSettings["SenderEmail"].ToString();

        private static string _hostedTransactionInfoApi = ConfigurationManager.AppSettings["HostedTransactionInfoApi"].ToString();

        private static string _xmlMerchantSignup = ConfigurationManager.AppSettings["xmlMerchantSignup"].ToString();

        //  xmlMerchantSignup

        public static string xmlMerchantSignup
        {
            get { return _xmlMerchantSignup; }
            set { _xmlMerchantSignup = value; }
        }

        public static string HostedTransactionInfoApi
        {
            get { return _hostedTransactionInfoApi; }
            set { _hostedTransactionInfoApi = value; }
        }

        public static string CertStr
        {
            get { return _certStr; }
            set { _certStr = value; }
        }

        public static string TermId
        {
            get { return _termId; }
            set { _termId = value; }
        }

        public static string AuthenticationToken
        {
            get { return _authenticationToken; }
            set { _authenticationToken = value; }
        }

        public static string BillerAccountId
        {
            get { return _billerAccountId; }
            set { _billerAccountId = value; }
        }

        public static string PropayRedirectUrl
        {
            get { return _propayRedirectUrl; }
            set { _propayRedirectUrl = value; }
        }

        public static string PropayUrl
        {
            get { return _propayUrl; }
            set { _propayUrl = value; }
        }

        public static string PropayCSS
        {
            get { return _propayCSS; }
            set { _propayCSS = value; }
        }

        public static string SenderEmail
        {
            get { return _senderEmail; }
            set { _senderEmail = value; }
        }

    }

    public static class API
    {
        public static string xmlMerchantSignup = "https://xmltest.propay.com/api/propayapi.aspx";
    }
}