﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Propay_Manage
{
    public class SignupMarchant
    {
        #region Old GetXMLRequest Method
        //public string GetXMLRequest()
        //{
        //    return "<?xml version='1.0'?><!DOCTYPE Request.dtd><XMLRequest>                 " +
        //    "<username></username>                                                          " +
        //    "<sourceip></sourceip>                                                          " +
        //    "<certStr>{0}</certStr>                                                         " +
        //    "<termid>{1}</termid>                                                           " +
        //    "<class>partner</class>                                                         " +
        //    "<XMLTrans>                                                                     " +
        //    "<transType>01</transType>                                                      " +
        //    "<country>USA</country>                                                         " +
        //    "<sourceEmail>{2}</sourceEmail>    " +
        //    "<firstName>{3}</firstName>                                               " +
        //    "<mInitial>F</mInitial>                                                          " +
        //    "<lastName>{4}</lastName>                                                  " +
        //    "<addr>{5} Main Street</addr>                                                   " +
        //    "<aptNum></aptNum>                                                              " +
        //    "<city>{6}</city>                                                              " +
        //    "<state>{7}</state>                                                              " +
        //    "<zip>{8}</zip>                                                               " +
        //    "<dayPhone>{9}</dayPhone>                                                " +
        //    "<evenPhone>{9}</evenPhone>                                              " +
        //    "<ssn>{10}</ssn>                                                           " +
        //    "<externalId></externalId>                                                      " +
        //    "<dob>{11}</dob>                                                          " +
        //    "<phonePin></phonePin>                                                          " +
        //    "<ccNum></ccNum>                                                                " +
        //    "<expDate></expDate>                                                        " +
        //    "<password></password>                                                          " +
        //    "<acceptedTermsAndConditions>False</acceptedTermsAndConditions>                 " +
        //    "<CurrencyCode></CurrencyCode>                                                  " +
        //    "<paymentMethodId></paymentMethodId>                                            " +
        //    "<userId></userId>                                                              " +
        //    "<BankName>{12}</BankName>                                                          " +
        //    "<accountName>{13}</accountName>                                                    " +
        //    "<AccountNumber>{14}</AccountNumber>                                                " +
        //    "<RoutingNumber>{15}</RoutingNumber>                                                " +
        //    "<accountType>{16}</accountType>                                                    " +
        //    "<AccountOwnershipType>{17}</AccountOwnershipType>                                  " +
        //    "<SecondaryAccountName></SecondaryAccountName>                                  " +
        //    "<SecondaryAccountType></SecondaryAccountType>                                  " +
        //    "<SecondaryRoutingNumber></SecondaryRoutingNumber>                              " +
        //    "<SecondaryAccountNumber></SecondaryAccountNumber>                              " +
        //    "<SecondaryBankName></SecondaryBankName>                                        " +
        //    "<SecondaryAccountOwnershipType></SecondaryAccountOwnershipType>                " +
        //    "<SecondaryAccountCountryCode></SecondaryAccountCountryCode>                    " +
        //    "<GrossSettleAddress></GrossSettleAddress>                                      " +
        //    "<GrossSettleCity></GrossSettleCity>                                            " +
        //    "<GrossSettleState></GrossSettleState>                                          " +
        //    "<GrossSettleZipCode></GrossSettleZipCode>                                      " +
        //    "<GrossSettleCountry></GrossSettleCountry>                                      " +
        //    "<GrossSettleNameOnCard></GrossSettleNameOnCard>                                " +
        //    "<GrossSettleCreditCardNumber></GrossSettleCreditCardNumber>                    " +
        //    "<GrossSettleCreditCardExpDate></GrossSettleCreditCardExpDate>                  " +
        //    "<GrossSettleAccountType>Unknown</GrossSettleAccountType>                       " +
        //    "<GrossSettleAccountCountryCode></GrossSettleAccountCountryCode>                " +
        //    "<GrossSettleRoutingNumber></GrossSettleRoutingNumber>                          " +
        //    "<GrossSettleAccountNumber></GrossSettleAccountNumber>                          " +
        //    "<GrossSettleAccountHolderName></GrossSettleAccountHolderName>                  " +
        //    "<tier></tier>                                                                  " +
        //    "<EIN></EIN>                                                                    " +
        //    "<BusinessLegalName></BusinessLegalName>                                        " +
        //    "<DoingBusinessAs></DoingBusinessAs>                                            " +
        //    "<BusinessAddress ></BusinessAddress>                                           " +
        //    "<BusinessCity ></BusinessCity>                                                 " +
        //    "<BusinessState ></BusinessState>                                               " +
        //    "<BusinessZip></BusinessZip>                                                    " +
        //    "<BusinessCountry></BusinessCountry>                                            " +
        //    "<BusinessAddress2></BusinessAddress2>                                          " +
        //    "<WebsiteURL></WebsiteURL>                                                      " +
        //    "<MerchantLegalName></MerchantLegalName>                                        " +
        //    "<BusinessDesc></BusinessDesc>                                                  " +
        //    "<MonthlyBankCardVolume>0</MonthlyBankCardVolume>                               " +
        //    "<AverageTicket>0</AverageTicket>                                               " +
        //    "<HighestTicket>0</HighestTicket>                                               " +
        //    "<mailApt></mailApt>                                                            " +
        //    "<mailAddr></mailAddr>                                                          " +
        //    "<mailCity></mailCity>                                                          " +
        //    "<mailState></mailState>                                                        " +
        //    "<mailZip></mailZip>                                                            " +
        //    "<MerchantSourceip></MerchantSourceip>                                          " +
        //    "<ThreatMetrixPolicy></ThreatMetrixPolicy>                                      " +
        //    "<ThreatMetrixSessionid></ThreatMetrixSessionid>                                " +
        //    "<intlID></intlID>                                                              " +
        //    "<driversLicenseVersion></driversLicenseVersion>                                " +
        //    "<documentType></documentType>                                                  " +
        //    "<documentExpDate></documentExpDate>                                            " +
        //    "<documentIssuingState></documentIssuingState>                                  " +
        //    "<medicareReferenceNumber></medicareReferenceNumber>                            " +
        //    "<medicareCardColor></medicareCardColor>                                        " +
        //    "<AuthorizedSignerFirstName></AuthorizedSignerFirstName>                        " +
        //    "<AuthorizedSignerLastName></AuthorizedSignerLastName>                          " +
        //    "<AuthorizedSignerTitle></AuthorizedSignerTitle>                                " +
        //    "<SignificantOwnerFirstName></SignificantOwnerFirstName>                        " +
        //    "<SignificantOwnerLastName></SignificantOwnerLastName>                          " +
        //    "<SignificantOwnerSSN></SignificantOwnerSSN>                                    " +
        //    "<SignificantOwnerDateOfBirth></SignificantOwnerDateOfBirth>                    " +
        //    "<SignificantOwnerStreetAddress></SignificantOwnerStreetAddress>                " +
        //    "<SignificantOwnerCityName></SignificantOwnerCityName>                          " +
        //    "<SignificantOwnerRegionCode></SignificantOwnerRegionCode>                      " +
        //    "<SignificantOwnerPostalCode></SignificantOwnerPostalCode>                      " +
        //    "<SignificantOwnerCountryCode></SignificantOwnerCountryCode>                    " +
        //    "<SignificantOwnerTitle></SignificantOwnerTitle>                                " +
        //    "<SignificantOwnerPercentage></SignificantOwnerPercentage>                      " +
        //    "<PaymentBankAccountNumber>1234567</PaymentBankAccountNumber>                   " +
        //    "<PaymentBankRoutingNumber>011000015</PaymentBankRoutingNumber>                 " +
        //    "<PaymentBankAccountType>Checking</PaymentBankAccountType>                      " +
        //    "</XMLTrans>                                                                    " +
        //    "</XMLRequest>";
        //}

        #endregion


        public string GetXMLRequest()
        {
            return "<?xml version='1.0'?><!DOCTYPE Request.dtd><XMLRequest>                 " +
            "<username></username>                                                          " +
            "<sourceip></sourceip>                                                          " +
            "<certStr>{0}</certStr>                                                         " +
            "<termid>{1}</termid>                                                           " +
            "<class>partner</class>                                                         " +
            "<XMLTrans>                                                                     " +
            "<transType>01</transType>                                                      " +
            "<country>USA</country>                                                         " +
            "<sourceEmail>{2}</sourceEmail>    " +
            "<firstName>{3}</firstName>                                               " +
            "<mInitial>F</mInitial>                                                          " +
            "<lastName>{4}</lastName>                                                  " +
            "<addr>{5} Main Street</addr>                                                   " +
            "<aptNum></aptNum>                                                              " +
            "<city>Los Angeles</city>                                                              " +
            "<state>CA</state>                                                              " +
            "<zip>83204</zip>                                                               " +
            "<dayPhone>{9}</dayPhone>                                                " +
            "<evenPhone>{9}</evenPhone>                                              " +
            "<ssn>{10}</ssn>                                                           " +
            "<externalId></externalId>                                                      " +
            "<dob>{11}</dob>                                                          " +
            "<phonePin></phonePin>                                                          " +
            "<ccNum>4747474747474747</ccNum>                                                                " +
            "<expDate>12-21</expDate>                                                        " +
            "<password></password>                                                          " +
            "<acceptedTermsAndConditions>False</acceptedTermsAndConditions>                 " +
            "<CurrencyCode>USD</CurrencyCode>                                                  " +
            "<paymentMethodId></paymentMethodId>                                            " +
            "<userId></userId>                                                              " +
            "<BankName>{12}</BankName>                                                          " +
            "<accountName>{13}</accountName>                                                    " +
            "<AccountNumber>{14}</AccountNumber>                                                " +
            "<RoutingNumber>{15}</RoutingNumber>                                                " +
            "<accountType>{16}</accountType>                                                    " +
            "<AccountOwnershipType>{17}</AccountOwnershipType>                                  " +
            "<SecondaryAccountName></SecondaryAccountName>                                  " +
            "<SecondaryAccountType></SecondaryAccountType>                                  " +
            "<SecondaryRoutingNumber></SecondaryRoutingNumber>                              " +
            "<SecondaryAccountNumber></SecondaryAccountNumber>                              " +
            "<SecondaryBankName></SecondaryBankName>                                        " +
            "<SecondaryAccountOwnershipType></SecondaryAccountOwnershipType>                " +
            "<SecondaryAccountCountryCode></SecondaryAccountCountryCode>                    " +
            "<GrossSettleAddress>down town Torrance</GrossSettleAddress>                                      " +
            "<GrossSettleCity>Torrance</GrossSettleCity>                                            " +
            "<GrossSettleState>CA</GrossSettleState>                                          " +
            "<GrossSettleZipCode>90501</GrossSettleZipCode>                                      " +
            "<GrossSettleCountry>USA</GrossSettleCountry>                                      " +
            "<GrossSettleNameOnCard></GrossSettleNameOnCard>                                " +
            "<GrossSettleCreditCardNumber></GrossSettleCreditCardNumber>                    " +
            "<GrossSettleCreditCardExpDate></GrossSettleCreditCardExpDate>                  " +
            "<GrossSettleAccountType>Unknown</GrossSettleAccountType>                       " +
            "<GrossSettleAccountCountryCode></GrossSettleAccountCountryCode>                " +
            "<GrossSettleRoutingNumber></GrossSettleRoutingNumber>                          " +
            "<GrossSettleAccountNumber></GrossSettleAccountNumber>                          " +
            "<GrossSettleAccountHolderName></GrossSettleAccountHolderName>                  " +
            "<tier></tier>                                                                  " +
            "<EIN></EIN>                                                                    " +
            "<BusinessLegalName>ArnaTech</BusinessLegalName>                                        " +
            "<DoingBusinessAs>software company</DoingBusinessAs>                                            " +
            "<BusinessAddress>down town Torrance</BusinessAddress>                                           " +
            "<BusinessCity>Torrance</BusinessCity>                                                 " +
            "<BusinessState>CA</BusinessState>                                               " +
            "<BusinessZip>90501</BusinessZip>                                                    " +
            "<BusinessCountry>USA</BusinessCountry>                                            " +
            "<BusinessAddress2>down town Torrance</BusinessAddress2>                                       " +
            "<WebsiteURL>ArnaTech.com</WebsiteURL>                                                         " +
            "<MerchantLegalName></MerchantLegalName>                                                       " +
            "<BusinessDesc>Test business</BusinessDesc>                                                    " +
            "<MonthlyBankCardVolume>2000</MonthlyBankCardVolume>                                           " +
            "<AverageTicket>300</AverageTicket>                                                            " +
            "<HighestTicket>2000</HighestTicket>                                                           " +
            "<mailApt></mailApt>                                                                           " +
            "<mailAddr></mailAddr>                                                                         " +
            "<mailCity></mailCity>                                                                         " +
            "<mailState></mailState>                                                                       " +
            "<mailZip></mailZip>                                                                           " +
            "<MerchantSourceip></MerchantSourceip>                                                         " +
            "<ThreatMetrixPolicy></ThreatMetrixPolicy>                                                     " +
            "<ThreatMetrixSessionid>01f50c4d1430a620a3b50005ffe98546</ThreatMetrixSessionid>               " +
            "<intlID></intlID>                                                                             " +
            "<driversLicenseVersion></driversLicenseVersion>                                               " +
            "<documentType></documentType>                                                                 " +
            "<documentExpDate></documentExpDate>                                                           " +
            "<documentIssuingState></documentIssuingState>                                                 " +
            "<medicareReferenceNumber></medicareReferenceNumber>                                           " +
            "<medicareCardColor></medicareCardColor>                                                       " +
            "<AuthorizedSignerFirstName>{3}</AuthorizedSignerFirstName>                                       " +
            "<AuthorizedSignerLastName>{4}</AuthorizedSignerLastName>                                         " +
            "<AuthorizedSignerTitle></AuthorizedSignerTitle>                                               " +
            "<SignificantOwnerFirstName></SignificantOwnerFirstName>                                       " +
            "<SignificantOwnerLastName></SignificantOwnerLastName>                                         " +
            "<SignificantOwnerSSN></SignificantOwnerSSN>                                                   " +
            "<SignificantOwnerDateOfBirth></SignificantOwnerDateOfBirth>                                   " +
            "<SignificantOwnerStreetAddress></SignificantOwnerStreetAddress>                               " +
            "<SignificantOwnerCityName></SignificantOwnerCityName>                                         " +
            "<SignificantOwnerRegionCode></SignificantOwnerRegionCode>                                     " +
            "<SignificantOwnerPostalCode></SignificantOwnerPostalCode>                                     " +
            "<SignificantOwnerCountryCode></SignificantOwnerCountryCode>                                   " +
            "<SignificantOwnerTitle></SignificantOwnerTitle>                                               " +
            "<SignificantOwnerPercentage></SignificantOwnerPercentage>                                     " +
            "<PaymentBankAccountNumber>1234567</PaymentBankAccountNumber>                                  " +
            "<PaymentBankRoutingNumber>011000015</PaymentBankRoutingNumber>                                " +
            "<PaymentBankAccountType>Checking</PaymentBankAccountType>                                     " +
            "</XMLTrans>                                                                                   " +
            "</XMLRequest>";
        }


        public string GetXMLRequest_Edit()

        {
            return "<?xml version='1.0'?><!DOCTYPE Request.dtd><XMLRequest>                 " +

            "<certStr>33ef8664b354779afbb273c910d7c1</certStr>                              " +
            "<termid>10d7c1</termid>                                                        " +
            "<class>partner</class>                                                         " +
            "<XMLTrans>                                                                     " +
            "<transType>42</transType>                                                      " +
            "<country>USA</country>                                                         " +
            "<sourceEmail>ravi@gmail.com</sourceEmail>                                   " +
            "<firstName>Ashish</firstName>                                               " +
            "<mInitial>F</mInitial>                                                          " +
            "<lastName>Manager</lastName>                                                  " +
            "<addr>MOG line</addr>                                                   " +
            "<aptNum></aptNum>                                                              " +
            "<city>Los Angeles</city>                                                                            " +
            "<state>California</state>                                                                           " +
            "<zip>1234</zip>                                                                                     " +
            "<dayPhone>9876543234</dayPhone>                                                                     " +
            "<evenPhone>9876543234</evenPhone>                                                                   " +
            "<ssn>789654321</ssn>                                                                                " +
            "<externalId>HK11243</externalId>                                                                    " +
            "<dob>09-13-1983</dob>                                                                               " +
             "<phonePin></phonePin>                                                                              " +
            "<ccNum>4747474742456787</ccNum>                                                                     " +
            "<expDate>1220</expDate>                                                                             " +
            "<password>maHJ8Yz%V3</password>                                                                     " +
            "<acceptedTermsAndConditions>False</acceptedTermsAndConditions>                                      " +
            "<CurrencyCode></CurrencyCode>                                                                       " +
            "<paymentMethodId></paymentMethodId>                                                                 " +
            "<userId></userId>                                                                                   " +
            "<BankName>Test bank</BankName>                                                                      " +
            "<accountName>drftg</accountName>                                                                    " +
            "<AccountNumber>drftg</AccountNumber>                                                                " +
            "<RoutingNumber>123</RoutingNumber>                                                                  " +
            "<accountNum>32396326</accountNum>                                                                   " +
            "<accountType>Saving</accountType>                                                                   " +
            "<AccountCountryCode>USA</AccountCountryCode>                                                        " +
            "<AccountOwnershipType>Business</AccountOwnershipType>                                               " +
            "<SecondaryAccountName>second account</SecondaryAccountName>                                         " +
            "<SecondaryAccountType>Savings</SecondaryAccountType>                                                " +
            "<SecondaryRoutingNumber></SecondaryRoutingNumber>                                                   " +
            "<SecondaryAccountNumber></SecondaryAccountNumber>                                                   " +
            "<SecondaryBankName></SecondaryBankName>                                                             " +
            "<SecondaryAccountOwnershipType></SecondaryAccountOwnershipType>                                     " +
            "<SecondaryAccountCountryCode>USA</SecondaryAccountCountryCode>                                      " +
            "<GrossSettleAddress>dxcsgdf</GrossSettleAddress>                                                    " +
            "<GrossSettleCity>Los Angeles</GrossSettleCity>                                                      " +
            "<GrossSettleState>California</GrossSettleState>                                                     " +
            "<GrossSettleZipCode>1234</GrossSettleZipCode>                                                       " +
            "<GrossSettleCountry>USA</GrossSettleCountry>                                                        " +
            "<GrossSettleNameOnCard></GrossSettleNameOnCard>                                                     " +
            "<GrossSettleCreditCardNumber>474747474747474747</GrossSettleCreditCardNumber>                       " +
            "<GrossSettleCreditCardExpDate>1220</GrossSettleCreditCardExpDate>                                   " +
            "<GrossSettleAccountType>Unknown</GrossSettleAccountType>                                            " +
            "<GrossSettleAccountCountryCode>USA</GrossSettleAccountCountryCode>                                  " +
            "<GrossSettleRoutingNumber>011000015</GrossSettleRoutingNumber>                                      " +
            "<GrossSettleAccountNumber>12345678912</GrossSettleAccountNumber>                                    " +
            "<GrossSettleAccountHolderName>ravi</GrossSettleAccountHolderName>                                   " +
            "<tier>Merchant</tier>                                                                               " +
            "<EIN></EIN>                                                                                         " +
            "<BusinessLegalName></BusinessLegalName>                                                             " +
            "<DoingBusinessAs></DoingBusinessAs>                                                                 " +
            "<BusinessAddress ></BusinessAddress>                                                                " +
            "<BusinessCity ></BusinessCity>                                                                      " +
            "<BusinessState ></BusinessState>                                                                    " +
            "<BusinessZip></BusinessZip>                                                                         " +
            "<BusinessCountry></BusinessCountry>                                                                 " +
            "<BusinessAddress2></BusinessAddress2>                                                               " +
            "<WebsiteURL></WebsiteURL>                                                                           " +
            "<MerchantLegalName></MerchantLegalName>                                                             " +
            "<BusinessDesc></BusinessDesc>                                                                       " +
            "<MonthlyBankCardVolume>0</MonthlyBankCardVolume>                                                    " +
            "<AverageTicket>300</AverageTicket>                                                                  " +
            "<HighestTicket>1500</HighestTicket>                                                                 " +
            "<mailApt></mailApt>                                                                                 " +
            "<mailAddr></mailAddr>                                                                               " +
            "<mailCity></mailCity>                                                                               " +
            "<mailState></mailState>                                                                             " +
            "<mailZip></mailZip>                                                                                 " +
            "<MerchantSourceip></MerchantSourceip>                                                               " +
            "<ThreatMetrixPolicy></ThreatMetrixPolicy>                                                           " +
            "<ThreatMetrixSessionid>01f50c4d1430a620a3b50005f6fe98546</ThreatMetrixSessionid>                    " +
            "<intlID></intlID>                                                                                   " +
            "<driversLicenseVersion></driversLicenseVersion>                                                     " +
            "<documentType></documentType>                                                                       " +
            "<documentExpDate></documentExpDate>                                                                 " +
            "<documentIssuingState></documentIssuingState>                                                       " +
            "<medicareReferenceNumber></medicareReferenceNumber>                                                 " +
            "<medicareCardColor></medicareCardColor>                                                             " +
            "<AuthorizedSignerFirstName></AuthorizedSignerFirstName>                                             " +
            "<AuthorizedSignerLastName></AuthorizedSignerLastName>                                               " +
            "<AuthorizedSignerTitle></AuthorizedSignerTitle>                                                     " +
            "<SignificantOwnerFirstName></SignificantOwnerFirstName>                                             " +
            "<SignificantOwnerLastName></SignificantOwnerLastName>                                               " +
            "<SignificantOwnerSSN></SignificantOwnerSSN>                                                         " +
            "<SignificantOwnerDateOfBirth></SignificantOwnerDateOfBirth>                                         " +
            "<SignificantOwnerStreetAddress></SignificantOwnerStreetAddress>                                     " +
            "<SignificantOwnerCityName></SignificantOwnerCityName>                                               " +
            "<SignificantOwnerRegionCode></SignificantOwnerRegionCode>                                           " +
            "<SignificantOwnerPostalCode></SignificantOwnerPostalCode>                                           " +
            "<SignificantOwnerCountryCode></SignificantOwnerCountryCode>                                         " +
            "<SignificantOwnerTitle></SignificantOwnerTitle>                                                     " +
            "<SignificantOwnerPercentage></SignificantOwnerPercentage>                                           " +
            "<PaymentBankAccountNumber>1234567</PaymentBankAccountNumber>                                        " +
            "<PaymentBankRoutingNumber>011000015</PaymentBankRoutingNumber>                                      " +
            "<PaymentBankAccountType>Checking</PaymentBankAccountType>                                           " +
            "</XMLTrans>                                                                                         " +
            "</XMLRequest>";
        }

        public string GetXMLRequestforEdit()
        {
            return "<?xml version = '1.0' ?>" +
            "<!DOCTYPE Request.dtd>" +
            "<XMLRequest>" +
            "<certStr>{0}</certStr>" +
            "<termid>{1}</termid> "+
            "<class>partner</class>" +
            "<XMLTrans>" +
            "<transType>42</transType>" +
            "<accountNum>{7}</accountNum> " +
            "<dayPhone>{5}</dayPhone> " +
            "<evenPhone>{5}</evenPhone> " +
            "<externalId></externalId> " +
            "<firstName>{3}</firstName> " +
            "<lastName>{4}</lastName> " +
            "<mInitial>F</mInitial> " +
            "<sourceEmail>{2}</sourceEmail> " +
            "</XMLTrans> " +
            "</XMLRequest>";
        }
        public string GetPropayProfile()
        {
            return "<?xml version = '1.0' ?>" +
            "<!DOCTYPE Request.dtd>" +
            "<XMLRequest>" +
            "<certStr>{0}</certStr>" +
            "<termid>{1}</termid> " +
            "<class>partner</class>" +
            "<XMLTrans>" +
            "<transType>13</transType>" +
            "<accountNum>{7}</accountNum>" +
            "</XMLTrans>" +
            "</XMLRequest>";
        }


        public string GetAccountPingXmlRequest()
        {
            return "<?xml version = '1.0'?>" +
                    "<!DOCTYPE Request.dtd>" +
                    "<XMLRequest>" +
                    "<certStr>33ef8664b354779afbb273c910d7c1</certStr>" +
                    "<termid>10d7c1</termid>" +
                    "<class>partner</class>" +
                    "<XMLTrans>" +
                    "<transType>13</transType>" +
                    "<sourceEmail>ravi121994@gmail.com</sourceEmail>" +
                    "</XMLTrans>" +
                    "</XMLRequest>";
        }



        public long CreateProfile(string accntNum, string profileName)
        {
            SPSService.MerchantProfileData profile = new SPSService.MerchantProfileData();
            profile.PaymentProcessor = "LegacyProPay";
            SPSService.ProcessorDatum[] processor = new SPSService.ProcessorDatum[3];
            SPSService.ProcessorDatum cert = new SPSService.ProcessorDatum();
            cert.ProcessorField = "certStr";
            cert.Value = Configuration.CertStr;
            processor[0] = cert;
            SPSService.ProcessorDatum termId = new SPSService.ProcessorDatum();
            termId.ProcessorField = "termId";
            termId.Value = Configuration.TermId;
            processor[1] = termId;
            SPSService.ProcessorDatum accountNum = new SPSService.ProcessorDatum();
            accountNum.ProcessorField = "accountNum";
            accountNum.Value = accntNum;
            processor[2] = accountNum;
            profile.ProcessorData = processor;
            profile.ProfileName = profileName;
            SPSService.CreateMerchantProfileResult result = new SPSServices().CreateMerchantProfile(profile);
            return result.ProfileId;
        }

        public string[] CreatePayer(string email, string name)
        {
            string[] strResponse = new string[2];
            SPSService.PayerData info = new SPSService.PayerData();
            SPSServices service = new SPSServices();
            info.EmailAddress = email;
            info.Name = name;

            SPSService.CreateAccountInformationResult result = service.CreatePayerID(info);
            if (result.RequestResult.ResultCode == "00")
            {
                strResponse[0] = "success";
                strResponse[1] = result.ExternalAccountID;
            }
            else
            {
                strResponse[0] = "error";
                strResponse[1] = result.RequestResult.ResultMessage;
            }
            return strResponse;
        }

        public void FindPropayMerchantIdAndPayerId(string EmailAddress, string Name)
        {
            string[] strResponse = new string[2];
            SPSService.PayerData info = new SPSService.PayerData();
            SPSServices service = new SPSServices();

           // SPSService.GetMerchantProfilesResult result = service.GetMerchantProfiles();
            SPSService.PayerData payerData = new SPSService.PayerData();
            payerData.EmailAddress = EmailAddress;// "sheethal.kumar@gmail.com";
            payerData.Name = Name; // "Sheetal Kumar";
            var result2 = service.GetPayers(payerData);


        }


    }
}
