﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace Propay_Manage
{
    public class PropayTransaction
    {
        public string[] PropayTransactionProcess(MdlPayment mdlPayment)
        {
            string[] str = new string[3];                                  
            SPSServices service = new SPSServices();
            SPSService.HostedTransactionDetail info = new SPSService.HostedTransactionDetail();
           
            info.Address1 = mdlPayment.address1;
            info.Address2 = mdlPayment.address2;
            info.Amount = Convert.ToInt32(mdlPayment.amount * 100);
            info.AuthOnly = false;
            info.AvsRequirementType = SPSService.RequirementType.Required;
            info.BillerAccountId = Convert.ToInt64(Configuration.BillerAccountId);
            info.BillerAuthToken = Configuration.AuthenticationToken;
            info.CardHolderNameRequirementType = SPSService.RequirementType.Required;
            info.City = mdlPayment.city;
            info.Comment1 = "";
            info.Comment2 = "";
            info.Country = "";
            info.CssUrl = Configuration.PropayCSS;//"https://protectpaytest.propay.com/hpp/css/pmi.css";
            info.CurrencyCode = SPSService.ISOCurrencyCode.USD;
            info.Description = mdlPayment.description;
            info.InvoiceNumber = mdlPayment.invoiceNumber;
            info.MerchantProfileId = Convert.ToInt64(mdlPayment.merchantProfileId);//Convert.ToInt64(2246750);
            info.Name = mdlPayment.name;//"asdsad";
            info.OnlyStoreCardOnSuccessfulProcess = false;
            info.PayerId = Convert.ToInt64(mdlPayment.payerId);//Convert.ToInt64(1047174440247345);
            info.PaymentTypeId = mdlPayment.selectPaymentType;
            info.ProcessCard = true;
            info.Protected = false;
            info.ReturnUrl = mdlPayment.currentUrl; // "http://localhost:55452/vataraUI/#/MobilePayment";//"http://localhost:55452/Home/RedirectToMobileSchema"; //mdlPayment.currentUrl;//Configuration.PropayRedirectUrl;//"http://localhost:55452/vataraUI/#/payment";
            info.SecurityCodeRequirementType = SPSService.RequirementType.Required;
            info.State = mdlPayment.state;
            info.StoreCard = true;
            info.ZipCode = mdlPayment.zipCode;
            
            SPSService.CreateHostedTransactionResult result = service.CreateHostedTransaction(info);
            var temp = result.ExtensionData;
            if (result.Result.ResultCode == "00")
            {
                //PropayEntities entity = new PropayEntities();
                //Propay_With_DB.Entity.Transaction transaction = new Entity.Transaction();
                //transaction.Address1 = Address1.Text;
                //transaction.Address2 = Address2.Text;
                //transaction.Amount = Amount.Text;
                //transaction.AuthOnly = "false";
                //transaction.City = City.Text;
                //transaction.Comment1 = Comment1.Text;
                //transaction.Comment2 = Comment2.Text;
                //transaction.Country = Country.Text;
                //transaction.Description = Description.Text;
                //transaction.InvoiceNumber = InvoiceNumber.Text;
                //transaction.MerchantProfileId = MerchantProfileId.Text;
                //transaction.Name = Name.Text;
                //transaction.PayerId = PayerId.Text;
                //transaction.State = State.Text;
                //transaction.ZipCode = ZipCode.Text;
                // transaction.HostedTransactionIdentifier = result.HostedTransactionIdentifier;
                // entity.Transactions.Add(transaction);
                // entity.SaveChanges();
                //Response.Redirect("https://protectpaytest.propay.com/hpp/v2/" + result.HostedTransactionIdentifier);

                // str[0]= "https://protectpaytest.propay.com/hpp/v2/" + result.HostedTransactionIdentifier;
                str[0] = Configuration.PropayUrl + result.HostedTransactionIdentifier;
                str[1] = "success";
                str[2] = result.HostedTransactionIdentifier;
            }
            else
            {               
                str[0] = result.Result.ResultMessage;
                str[1] = "error";
                str[2] = "";
            }

            return str;
        }

        public HostedTransactionModel GetHostedTransactionInfo(string hostedTransactionId)
        {
            string authorization = GetCredentials();
            string jsonResponse = string.Empty;
            HostedTransactionModel hostedTransactionModel = new HostedTransactionModel();
            string url = string.Format(Configuration.HostedTransactionInfoApi, hostedTransactionId);
            try
            {
                var webRequest = WebRequest.CreateHttp(url);
                if (webRequest != null)
                {
                    webRequest.Method = "GET";                   
                    webRequest.Headers.Add("Authorization", authorization);
                 
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            jsonResponse = sr.ReadToEnd();
                            return JsonConvert.DeserializeObject<HostedTransactionModel>(jsonResponse);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return hostedTransactionModel;
        }
        
        private  string GetCredentials()
        {
            var billerAccountId = Configuration.BillerAccountId; //"1916271013067342"; // biller account id
            var authToken = Configuration.AuthenticationToken; //"0671454c-cba6-4cfa-90a9-ab4b3af15975"; // authentication token of biller

            var encodedCredentials = Convert.ToBase64String(Encoding.Default.GetBytes(billerAccountId + ":" + authToken));

            var credentials = string.Format("Basic {0}", encodedCredentials);
            return credentials;
        }
    }
}
